package com.happypeople.codeforces.c1138

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1138().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1138 {
    val NONE=0
    val CLOWN=1
    val ACROB=2
    val BOTH=3
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val c=(1..n).map { sc.nextInt() }
        val a=(1..n).map { sc.nextInt() }

        val N=0
        val A=1
        val B=2
        val NONE=0
        val CLOWN=1
        val ACROB=2
        val BOTH=3
        val sets=arrOf(3) { IntArray(4) }
        for(i in c.indices) {
            if(c[i]==0 && a[i]==0)
                sets[N][NONE]++
            else if(c[i]==1 && a[i]==0)
                sets[N][CLOWN]++
            else if(c[i]==0 && a[i]==1)
                sets[N][ACROB]++
            else
                sets[N][BOTH]++
        }

        for( i in 0..sets[N][ACROB]) {      // possible counts for sets[A][ACROB]
            for(j in 0..sets[N][BOTH]) {    // possible counts for  sets[A][BOTH]
                val Ac=sets[N][ACROB] - i + sets[N][BOTH] - 2*j
                // TODO check if possible
            }
        }


    }

    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
