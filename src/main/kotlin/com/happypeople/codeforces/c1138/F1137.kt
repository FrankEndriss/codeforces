package com.happypeople.codeforces.c1138

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        F1137().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class F1137 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val q = sc.nextInt()
        val tree = arrOf(n + 1) { mutableListOf<Int>() }
        for (i in (1..n)) {
            val u = sc.nextInt()
            val v = sc.nextInt()
            tree[u].add(v)
            tree[v].add(u)
        }

        val prio = tree.indices.map { it + 1 }.toIntArray()

        // create the sequence of nodes in order of burning
        val leaves = tree.indices.filter { tree[it].size == 1 }
        // TODO ...

        for (i in (1..q)) {
            val word = sc.next().trim()
            if (word == "up")
                solveUp(sc.nextInt())
            else if (word == "when")
                solveWhen(sc.nextInt())
            else
                solveCompare(sc.nextInt(), sc.nextInt())
        }

    }

    private fun solveCompare(nextInt: Int, nextInt1: Int) {

    }

    private fun solveWhen(nextInt: Int) {

    }

    private fun solveUp(nextInt: Int) {

    }

    inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
        val a = arrayOfNulls<E>(size)
        for (i in a.indices)
            a[i] = init(i)
        return a as Array<E>
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
