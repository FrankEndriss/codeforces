package com.happypeople.codeforces.c1138

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        C1138().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1138 {
    fun dump(a:Array<List<Int>>) {
        for(i in a.indices) {
            for (j in a[i].indices) {
                print("${a[i][j]} ")
            }
            println()
        }
    }

    val tsini=System.currentTimeMillis()
    fun logts(s:String) {
        val ts=System.currentTimeMillis()-tsini
        log("$ts $s")
    }

    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()
        val city=arrOf(n) { IntArray(m) }
        for(r in 0..n-1) {
            for(c in 0..m-1) {
                city[r][c]=sc.nextInt()
            }
        }

        // every single row city[i] sorted by ci
        val dummy=IntArray(1)
        val cByRow=arrOf(n) { dummy }
        val cByRowMax=IntArray(n)
        for(i in 0..n-1) {
            val row=city[i]
            val idx=(0..m-1).sortedWith(Comparator<Int> { i1, i2 -> row[i1]-row[i2] }).toIntArray()
            val decrA=IntArray(m)
            var pos=0
            for(j in 1..m-1) {
                if(row[idx[j]]!=row[idx[j-1]]) {
                    pos++
                }
                decrA[idx[j]]=pos
            }
            /*
            if(i==0) {
                log("cByRow[0]=${idx.toList()} wo/ decr")
                log("decrA    =${decrA.toList()}")
            }
            */
            //for(j in 1..m-1)
            //    idx[j]-=decrA[idx[j]]

            cByRow[i]=decrA
            //if(i==0) {
            //    log("cByRow[0]=${cByRow[i].toList()}")
            //}
            cByRowMax[i]=pos
        }
        //logts("after cByRow")
        //log("cByRow: $cByRow")
        //dump(cByRow)

        val cByCol=arrOf(m) { dummy }
        val cByColMax=IntArray(m)
        for(j in 0..m-1) {
            val idx=(0..n-1).sortedWith(Comparator<Int> { i1, i2 -> city[i1][j]-city[i2][j] }).toIntArray()
            //if(j==0)
            //    log("before decr idx=${idx.toList()}")

            val decrA=IntArray(n)
            var pos=0
            for(i in 1..n-1) {
                if(city[idx[i-1]][j]!=city[idx[i]][j]) {
                    pos++
                }
                decrA[idx[i]]=pos
            }
            //for(i in 1..n-1)
            //    idx[i]-=decrA[idx[i]]
            //if(j==0)
            //    log("after decr idx=${idx.toList()}")

            cByCol[j]=decrA
            //if(j==0) {
            //    log("cByCol[0]=${cByCol[j].toList()}")
            //}
            cByColMax[j]=pos
        }
        //logts("after cByCol")
        //log("cByCol: $cByCol")
        //dump(cByCol)


        val sb=StringBuffer()
        //log("---------")
        for(i in 0..n-1) {
            for(j in 0..m-1) {
                val diff=cByRow[i][j]-cByCol[j][i]

                /*
                if(i==0 && j==0) {
                    log("diff=$diff @ i=$i j=$j")
                    log("cByRow[i][j]=${cByRow[i][j]}")
                    log("cByCol[j][i]=${cByCol[j][i]}")
                    log("cByRowMax[i]=${cByRowMax[i]}")
                    log("cByColMax[j]=${cByColMax[j]}")
                }
                */

                var ans=0
                if(diff==0) {
                    ans = max(ans, cByRowMax[i])
                    ans = max(ans, cByColMax[j])
                } else if(diff<0) {
                    ans=max(ans, cByRowMax[i]-diff)
                    ans=max(ans, cByColMax[j])
                } else {
                    ans=max(ans, cByColMax[j]+diff)
                    ans=max(ans, cByRowMax[i])
                }
                sb.append("${ans+1} ")
            }
            sb.append("\n")
        }
        //logts("after creating output")
        println(sb)
        //logts("after output, fini")
    }

    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
