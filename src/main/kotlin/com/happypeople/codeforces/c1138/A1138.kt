package com.happypeople.codeforces.c1138

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        A1138().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1138 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val t=(1..n).map { sc.nextInt() }

        val list=mutableListOf<Int>()
        var lastT=t[0]
        var count=1
        for(i in 1..t.size-1) {
            if(t[i]==lastT)
                count++
            else {
                list.add(if(lastT==1) count else -count)
                count=1
            }
            lastT=t[i]
        }
        list.add(if(lastT==1) count else -count)

        var ans=0
        for(i in 1..list.size-1) {
            ans=max(ans, min(abs(list[i-1]), abs(list[i]))*2)
        }
        println("$ans")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
