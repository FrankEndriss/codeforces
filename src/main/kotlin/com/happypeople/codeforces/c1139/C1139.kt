package com.happypeople.codeforces.c1139

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs

fun main(args: Array<String>) {
    try {
        C1139().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1139 {

    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val k=sc.nextInt()
        val graph=arrOf(n) { mutableListOf<Int>() }
        (1..n-1).forEach {
            val u=sc.nextInt()-1
            val v=sc.nextInt()-1
            var c=sc.nextInt()
            if(c==0) { // red, ignore black edges
                graph[u].add(v)
                graph[v].add(u)
            }
        }

        val sum=0
        // first find all red pairs/pathes
        val badPathes=arrOf(n) { mutableSetOf<Int>(it) }

        for(i in 0..n-1) {
                calcRedPathes(i, badPathes, graph)
        }

        // second
        // Count all permutations of len K with only bad Pairs in it.
        // Find groups of red connected nodes.
        // Any one group can build sizeof(group) pow k permutations
        // Add this numbers.
        var ans=0
        val checked=mutableSetOf<Int>()
        for(i in badPathes.indices) {
            if(badPathes[i].size==0)
                continue
            if(checked.contains(i))
                continue

            val inGroup=mutableSetOf(i)
            do {
                val next=inGroup.flatMap { badPathes[it] }.filter { !inGroup.contains(it) }
                inGroup.addAll(next)
            }while(!next.isEmpty())

            ans=Inv.plus(ans, Inv.toPower(inGroup.size, k))
            checked.addAll(inGroup)
        }

        val allPerms=Inv.toPower(n, k)
        ans=Inv.plus(allPerms, -ans)
        println("$ans")

    }

    /** traverse the whole tree rooting at i, mark all found pathes */
    private fun calcRedPathes(i: Int, badPathes: Array<MutableSet<Int>>, graph: Array<MutableList<Int>>) {
        var next=listOf(i)
        var visited=next.toMutableSet()
        while(!next.isEmpty()) {
            val nextnext=next.flatMap { graph[it] }.filter { !visited.contains(it) }
            // not sure if can filter more nodes...
            for(u in nextnext) {
                if(badPathes[u].size>0) {
                    val list=badPathes[u].toList()
                    for (v in list) {
                        badPathes[i].add(v)
                        badPathes[v].add(i)
                    }
                    badPathes[u].add(i)
                    badPathes[i].add(u)
                    return
                }
            }
            visited.addAll(nextnext)
            next=nextnext
        }
    }

    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }
    class Inv {
        companion object {
            val defaultMod = 1000000007
            var MOD = defaultMod

            fun toPower(a: Int, p: Int, mod: Int=MOD): Int {
                var a = a
                var p = p
                var res = 1
                while (p != 0) {
                    if (p and 1 == 1)
                        res = mul(res, a)
                    p = p shr 1
                    a = mul(a, a)
                }
                return res
            }

            fun inv(x: Int, mod: Int = MOD): Int {
                return toPower(x, mod - 2)
            }

            fun fraction(zaehler: Int, nenner: Int): Int {
                return mul(zaehler, inv(nenner))
            }

            fun mul(v1: Int, v2: Int): Int {
                return ((1L * v1 * v2) % Inv.MOD).toInt()
            }

            fun plus(v1: Int, v2: Int): Int {
                var res = v1 + v2

                if (res < 0)
                    res += Inv.MOD

                if(res>=Inv.MOD)
                    res-=Inv.MOD

                return res
            }
        }
    }
    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
