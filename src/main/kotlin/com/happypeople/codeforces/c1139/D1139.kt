package com.happypeople.codeforces.c1139

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1139().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1139 {
    fun run() {
        val sc = Scanner(systemIn())
        val M=sc.nextInt()
        log("M=$M")

        val primes=Primes(100000)

        val ans=primes.pr[99999]
        log("third prime=$ans")

        // first find the number of primes up to M
        var numPrimes=0
        while(primes.pr[numPrimes]<M)
            numPrimes++
        log("first prime bgt m: ${primes.pr[numPrimes]}")

        // calculate the "expected length", which is the sum of
        // an endless sequence of fractions with increasing potentions.
        // sic mathematics...

    }
    class Primes(val MAXN:Int, val primeAproxFactor:Int=30) {
        val pr = IntArray(MAXN)

        init {
            val cmp = BooleanArray(MAXN * primeAproxFactor)

            var c = 0
            var i = 2
            // approx pr[MAXN] to be less than MAXN*primeAproxFactor, which is true for about MAXN < ~200000
            while (i < MAXN * primeAproxFactor && c < MAXN) {
                if (!cmp[i]) {
                    pr[c] = i
                    c++
                    var j = 1L*i * i
                    while (j < primeAproxFactor * MAXN) {
                        cmp[j.toInt()] = true
                        j += i
                    }
                }
                i++
            }
        }

        fun asSeq(): Sequence<Int> {
            var idx = 0
            return generateSequence {
                if (idx >= pr.size)
                    null
                else
                    pr[idx++]
            }
        }

        /*
    const int MAXN = 100000;
    int pr[MAXN], c = 0;
    bool cmp[MAXN * 30];

    void precalc() {
    c = 0;
    memset(cmp, false, sizeof cmp);
    for (int i = 2; i< 30 * MAXN && c < MAXN; ++i) {
        if (!cmp[i]) {
            pr[c++] = i;
            for (long long j = i*1ll*i; j < 30 * MAXN; j += i) {
                cmp[j] = true;
            }
        }
    }
    }
         */
    }
    class Inv {
        companion object {
            val defaultMod = 1000000007
            var MOD = defaultMod

            fun toPower(a: Int, p: Int, mod: Int=MOD): Int {
                var a = a
                var p = p
                var res = 1
                while (p != 0) {
                    if (p and 1 == 1)
                        res = mul(res, a)
                    p = p shr 1
                    a = mul(a, a)
                }
                return res
            }

            fun inv(x: Int, mod: Int = MOD): Int {
                return toPower(x, mod - 2)
            }

            fun fraction(zaehler: Int, nenner: Int): Int {
                return mul(zaehler, inv(nenner))
            }

            fun mul(v1: Int, v2: Int): Int {
                return ((1L * v1 * v2) % Inv.MOD).toInt()
            }

            fun plus(v1: Int, v2: Int): Int {
                var res = v1 + v2

                if (res < 0)
                    res += Inv.MOD

                if(res>=Inv.MOD)
                    res-=Inv.MOD

                return res
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
