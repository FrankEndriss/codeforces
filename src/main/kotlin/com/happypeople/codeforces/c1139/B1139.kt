package com.happypeople.codeforces.c1139

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1139().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1139 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val A=(1..n).map { sc.nextInt() }
        var m=Int.MAX_VALUE
        var ans=0L
        for(i in n-1 downTo 0) {
            m=min(m-1, A[i])
            if(m<0)
                m=0
            ans+=m
        }
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
