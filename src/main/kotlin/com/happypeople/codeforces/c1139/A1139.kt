package com.happypeople.codeforces.c1139

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1139().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1139 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val s=sc.next().trim()
        var ans=0
        for(i in 0..n-1) {
            val c="${s[i]}".toInt()
            if(c%2==0) { // even
                ans+=(i+1)
            }
        }
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
