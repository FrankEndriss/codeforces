package com.happypeople.codeforces.c1106

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1106().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1106 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val m = sc.nextInt()
        val edges = arrOf(n + 1) { mutableSetOf<Int>() }
        for (i in 1..m) {
            val from = sc.nextInt()
            val to = sc.nextInt()
            edges[from].add(to)
            edges[to].add(from)
        }

        val path = mutableSetOf<Int>(1)
        val reacheable=TreeSet<Int>()

        var currNode = 1
        reacheable.addAll(edges[currNode])

        while (path.size < n) {
            val nextNode = reacheable.first()
            path.add(nextNode)
            reacheable.remove(nextNode)
            reacheable.addAll(edges[nextNode].filter {  !path.contains(it) } )
        }
        val ans = path.joinToString(" ")
        println("$ans")
    }

    inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
        val a = arrayOfNulls<E>(size)
        for (i in a.indices)
            a[i] = init(i)
        return a as Array<E>
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
