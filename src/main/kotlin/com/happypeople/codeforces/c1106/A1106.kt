package com.happypeople.codeforces.c1106

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1106().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1106 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val M=arrOf(n) { BooleanArray(n) }
        for(i in M.indices) {
            val line=sc.next().trim()
            for (j in M.indices)
                M[i][j] = line[j]=='X'
        }

        var ans=0
        for(i in 1..M.size-2) {
            for (j in 1..M.size - 2) {
                if(M[i][j] && M[i-1][j-1] && M[i-1][j+1] && M[i+1][j-1] && M[i+1][j+1])
                    ans++
            }
        }
        println("$ans")

    }

    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }
    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
