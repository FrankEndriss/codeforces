package com.happypeople.codeforces.c1106

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1106().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1106 {
    fun run() {
        val sc = Scanner(systemIn())
        val foodKinds=sc.nextInt()
        val customers=sc.nextInt()
        val remainingFood=(1..foodKinds).map { sc.nextInt() }.toMutableList()
        val foodCosts=(1..foodKinds).map { sc.nextInt() }
        val costIdx=(0..foodKinds-1).sortedWith(compareBy( { foodCosts[it] }, { it }))
        var cheapestIdx:Int?=0

        for(i in 1..customers) {    // orders
            val food=sc.nextInt()-1
            var orderded=sc.nextInt()
            //log("food: $food ordered: $orderded cost per dish: ${foodCosts[food]}")
            //log("avail food: $remainingFood")
            var costs=0L

            if(remainingFood[food]>0) {
                val sold=min(remainingFood[food], orderded)
                remainingFood[food]-=sold
                orderded-=sold
                costs+=1L*foodCosts[food]*sold
                //log("m1, sold=$sold costs=$costs remaining count=$orderded")
            }

            if(orderded>0 && cheapestIdx!=null) { // find cheapest
                cheapestIdx=findCheapest(costIdx, remainingFood, cheapestIdx!!)
                while(cheapestIdx!=null && orderded>0) {
                    val sold = min(remainingFood[costIdx[cheapestIdx]], orderded)
                    costs+=1L*foodCosts[costIdx[cheapestIdx]]*sold
                    //log("m2, sold:$sold of:$orderded food=${costIdx[cheapestIdx]} cost per dish=${foodCosts[costIdx[cheapestIdx]]} cust-cost=$costs")
                    orderded-=sold
                    remainingFood[costIdx[cheapestIdx]]-=sold
                    if(orderded>0)
                        cheapestIdx=findCheapest(costIdx, remainingFood, cheapestIdx!!)
                }
            }
            //log("m3, $orderded")

            if(orderded>0) // customer unsatified
                costs=0

            println("$costs")
        }
    }

    private fun findCheapest(costIdx: List<Int>, remainingFood: MutableList<Int>, startSearchAt:Int): Int? {
        for(i in startSearchAt..costIdx.size-1)
            if(remainingFood[costIdx[i]]>0)
                return i
        return null
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
