package com.happypeople.codeforces.c1105

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        println("")
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val k=sc.nextInt()
        val s=sc.next().trim().toCharArray()

        val az=('a'..'z').map { 0 }.toMutableList()
        var currChr='-'
        var currLen=0
        s.forEach {
            if(it==currChr) {
                currLen++
            } else {
                currChr=it
                currLen=1
            }

            if(currLen==k) {
                az['z'-currChr]++
                currLen=0
            }
        }
        val ans=az.max()
        println("$ans")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}