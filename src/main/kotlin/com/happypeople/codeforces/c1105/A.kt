package com.happypeople.codeforces.c1105

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        println("")
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }

        var gOptCost=Int.MAX_VALUE
        var gOptVal=-1
        // brute force
        for( i in 1..99 ) {
            var costs=0
            for(j in a.indices) {
                var len=abs(a[j]-i)
                if(len>=1)
                    len--
                costs+=len
            }
            if(gOptCost>costs) {
                gOptCost=costs
                gOptVal=i
            }
        }

        println("${gOptVal} ${gOptCost}")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}