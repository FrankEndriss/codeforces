package com.happypeople.codeforces.c1105

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        println("")
    }
}

class C {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val l = sc.nextInt()
        val r = sc.nextInt()

        // possible count of numbers to get remeinder idx: c[idx]
        val c = (0..2).map {
            if (l % 3 != it && r % 3 != it && (l%3)==(r%3))
                (r - l) / 3
            else
                1+ (r - l) / 3
        }
        log("c=$c")

        /*
        // possible count of a[l], a[l+1], a[l+2]
        c[0] * (c[0] * c[0]) +
                c[0] * (c[1] * c[2]) +
                c[0] * (c[2] * c[1]) +
                c[1] * (c[0] * c[2]) +
                c[1] * (c[1] * c[1]) +
                c[1] * (c[2] * c[0]) +
                c[2] * (c[0] * c[1]) +
                c[2] * (c[1] * c[0]) +
                c[2] * (c[2] * c[2])
                */

        val MOD = 1000000007
        var a3 = 1L
        var ans=0L
        for (i in 0..2)
            for (j in 0..2) {
                var v=1L * c[i]
                v %= MOD
                v *= c[j]
                v %= MOD
                v *= c[(3-((i + j) % 3))%3]
                v %= MOD
                ans+=v
            }

        ans%=MOD
        ans = ipow(ans, n / 3, MOD)
        log("ans after a3: $ans")

        if (n % 3 == 2) {
            var a2=1L*c[l%3] * c[2-(l%3)] +
                    1L*c[(l+1)%3] * c[2-(l+1)%3] +
                    1L*c[(l+2)%3] * c[2-(l+2)%3]

            a2 %= MOD
            ans *= a2
        } else if (n % 3 == 1) {
            ans *= c[0]
        }
        ans %= MOD
        println("${ans}")
    }

    fun ipow(base: Long, exp: Int, mod: Int): Long {
        var base = base
        var exp = exp
        var result = 1L
        while (true) {
            if ((exp and 1) == 1) {
                result *= base
                result %= mod
            }
            exp = exp shr 1
            if (exp == 0)
                break
            base *= base
            base %= mod
        }

        return result
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}