package com.happypeople.codeforces.c1101

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        A.log("" + e)
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val q=sc.nextInt()
        for(i in 1..q) {
            val l=sc.nextInt()
            val r=sc.nextInt()
            val d=sc.nextInt()

            if(d>r || d<l)
                println("$d")
            else if(r%d==0)
                println("${r+d}")
            else {
                val ans=r+(d-r%d)
                println("$ans")
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}