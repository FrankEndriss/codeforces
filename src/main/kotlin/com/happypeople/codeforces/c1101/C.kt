package com.happypeople.codeforces.c1101

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        println("$e")
    }
}

class C {

    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        for (i in 1..T) {
            val s = sc.nextInt()

            val list = mutableListOf<Pair<Pair<Int, Int>, Int>>()
            val segs = (0..s - 1).map {
                val l = sc.nextInt()
                val r = sc.nextInt()
                Pair(Pair(l, r), it)
            }.sortedBy { it.first.first }

            var maxR = segs[0].first.second
            var gapFoundAt = -1
            for (i in 1..segs.size - 1) {
                if (segs[i].first.first > maxR) {
                    gapFoundAt = i
                    break
                } else
                    maxR = max(maxR, segs[i].first.second)
            }

            if (gapFoundAt <= 0)
                println("-1")
            else {
                val out = IntArray(segs.size)
                for (i in segs.indices)
                    out[segs[i].second] = if (i < gapFoundAt) 1 else 2
                for (i in out.indices)
                    print("${out[i]} ")
                println()
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}