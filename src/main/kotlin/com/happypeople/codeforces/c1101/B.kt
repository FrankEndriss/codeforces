package com.happypeople.codeforces.c1101

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        B.log("" + e)
    }
}

enum class S {
    start,
    open,
    firstColon,
    pipes,
    secondColon,
    close
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val line=sc.nextLine().trim()

        val firstOpen=line.indexOf('[')
        if(firstOpen<0) {
            println("-1")
            return
        }
        val firstColon=line.indexOf(':', firstOpen)
        if(firstColon<0) {
            println("-1")
            return
        }

        val lastOpen=line.lastIndexOf(']')
        if(lastOpen<0) {
            println("-1")
            return
        }
        val lastColon=line.lastIndexOf(':', lastOpen)
        if(lastColon<0) {
            println("-1")
            return
        }

        if(lastColon==firstColon || lastOpen<=firstOpen) {
            println("-1")
            return
        }

        val pipes=line.substring(firstColon, lastColon).filter { it=='|' }.count()

        val ans=pipes+4
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}