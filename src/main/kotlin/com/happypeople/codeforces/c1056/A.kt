package com.happypeople.codeforces.c1056

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    A().run()
}

/* https://codeforces.com/contest/1056/problem/A */
class A {
    fun run() {
        val sc = Scanner(systemIn())
        log("needs to be implemented")
        val n=sc.nextInt()
        var trams=mutableSetOf<Int>()
        for(i in 0 until n) {
            val r=sc.nextInt()
            val stopList=mutableSetOf<Int>()
            for(j in 0 until r) {
                stopList.add(sc.nextInt())
            }
            if(trams.isEmpty())
                trams.addAll(stopList)
            else
                trams=trams.map { if(stopList.contains(it)) it else null }.filterNotNull().toMutableSet()

        }
        trams.forEach { print("$it ") }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}