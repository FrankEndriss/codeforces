package com.happypeople.codeforces.c1056

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        E().run()
    } catch (e: Throwable) {
        E.log("" + e)
    }
}

/*
TODO https://codeforces.com/contest/1056/problem/E
Brute force, recursive:
-type1 s=0*1
-type2 s=0*0
-type1
--add chars to r0 and r1 and check after each char if match, allways two posibilities: left or right
-type2
--find possible values for r0 (common pre/post-fixes of t)
--for all possible r0 find possible values for r1, multiply posibilities

 */
class E {
    fun run() {
        val sc = Scanner(systemIn())
        // val s=sc.nextLine()!!.map { if (it=='1') 1 else 0 }
        val s = sc.nextLine()!!
        val t = sc.nextLine()!!.toCharArray()

        var r0 = "".toCharArray()
        var r1 = "".toCharArray()

        r0 += t.first()
        if (s.first() == s.last()) {     // 1..0..1 || 0..1..0
            r0 += t.last()
        } else                        // 1..0 || 0..1
            r1 += t.last()

        var r0L = 1        // if taken from left of t insert into r0 idx
        var r0R = 1       // if taken from right of t insert into r0 idx
        var r1L = 0        // if taken from left of t insert into r1 idx
        var r1R = 0       // if taken from right of t insert into r1 idx

        // indexes of next chars in t
        var tLidx = 1
        var tRidx = t.size - 2

        var count=0
        while (tLidx < tRidx-1) {
            if (t[tLidx] == r0[0])
                count+=checkMatch(r0, r1, s, t)
            else {

            }
        }

    }

    fun checkMatch(r0:CharArray, r1: CharArray, s: String, t: CharArray):Int {
        if(r0==r1 || r1.size==0)
            return 0    // not allowed per definition

        val r0mark=s[0]
        var tIdx=0
        for(it in s) {
            if(it==r0mark) {
                for(c in r0) {
                    if(c!=t[tIdx++])
                        return 0
                }
            } else {
                for(c in r1) {
                    if(c!=t[tIdx++])
                        return 0
                }
            }
        }
        return 1
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}