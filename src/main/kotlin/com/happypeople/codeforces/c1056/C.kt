package com.happypeople.codeforces.c1056

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    }catch(e:Throwable) {
        // ignore
    }
}

class C {
    val sc = Scanner(systemIn())
    val power= mutableListOf<Int>(-1) // heros are 1 based
    val specialPairs= mutableListOf<Pair<Int, Int>>()

    fun run() {
        val numPlayers=sc.nextInt()
        val numSpecialPairs=sc.nextInt()
        for(i in 0 until 2*numPlayers)
            power.add(sc.nextInt())
        for(i in 0 until numSpecialPairs)
            specialPairs.add(Pair(sc.nextInt(), sc.nextInt()))

        // team one or two, one begins
        val t=sc.nextInt()

        var otherInput=-2
        if(t==2) {
            while (react(sc.nextInt()))
            ;
        } else {
            act()
        }
    }

    fun act() {
        while(power.filter { it>=0 }.size>0) {
            specialPairs.forEach {
                if(power[it.first]>power[it.second])
                    output(it.first)
                else
                    output(it.second)

                power[it.first]=-1
                power[it.second]=-1
                val otherInput = sc.nextInt()
                // println("other: $otherInput")
            }
            specialPairs.clear()

            if(power.filter { it>=0 }.size>0) {
                val next = power.mapIndexed { hero, power ->
                    Pair(hero, power) // hero, power
                }.filter {
                    it.second >= 0
                }.sortedBy {
                    -it.second
                }.first()

                output(next.first)
                power[next.first] = -1

                if(power.filter { it>=0 }.size>0) {
                    val otherInput = sc.nextInt()
                    // println("other: $otherInput")
                    power[otherInput] = -1
                }
            }
        }
    }

    fun react(otherInput:Int):Boolean {
        // println("otherInput $otherInput")
        power[otherInput]=-1
        val pair=specialPairs.filter { it.first==otherInput || it.second==otherInput }.firstOrNull()
        if(pair!=null) {
            output(if(pair.first==otherInput) pair.second else pair.first)
            specialPairs.remove(pair)
            power[pair.first]=-1
            power[pair.second]=-1
            return true;
        } else {
            act();
        }
        return false
    }

    fun output(i:Int) {
        println("$i")
        System.`out`.flush()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}