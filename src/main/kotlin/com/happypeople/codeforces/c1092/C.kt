package com.happypeople.codeforces.c1092

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        println("")
    }
}

class C {
    val UNDEF = -1
    val BOTH = 0
    val PRE = 1
    val SUF = 2

    fun isPrefixOf(str1: String, str2: String, n: Int): Int {
        //log("isPrefixOf, str1=$str1 str2=$str2 n=$n")
        val c = n - str2.length
        var ret = UNDEF

        if (str1.length <= n / 2)    // cannot tell if PRE or SUF
            return BOTH

        if (str1.endsWith(str2.substring(str2.length - c)))
            ret = PRE

        if (str2.endsWith(str1.substring(str1.length - c))) {
            if (ret == UNDEF)
                ret = SUF
            else
                ret = BOTH
        }
        return ret
    }

    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val fixes = mutableListOf<String>()
        for (i in 1..(2 * n - 2)) {
            fixes.add(sc.next().trim())
        }

        // construct the string
        val lenIdx = (0..fixes.size - 1).sortedBy { -fixes[it].length }
        var nstr = ""
        for (i in 0..(2 * n - 3) step 2) {
            val str1 = fixes[lenIdx[i]]
            val str2 = fixes[lenIdx[i + 1]]

            val type = isPrefixOf(str1, str2, n)
            if (PRE == type) {
                val c = n - str2.length
                nstr = str1 + str2.substring(str2.length - c)
            } else if (SUF == type) {
                val c = n - str1.length
                nstr = str2 + str1.substring(str1.length - c)
            } else if(str1==str2 && str1.length>=(n+1)/2) {
                val c = n - str1.length
                nstr = str2 + str1.substring(str1.length - c)
            }
            if(nstr.length>0)
                break
        }

        if (nstr.length == 0) { // corner case: most likely nstr starts end ends with the same char
            if (fixes[lenIdx[0]].startsWith(fixes[lenIdx[lenIdx.size - 1]]))
                nstr = fixes[lenIdx[0]] + fixes[lenIdx[lenIdx.size - 1]]
            else
                nstr = fixes[lenIdx[1]] + fixes[lenIdx[lenIdx.size - 1]]
        }

        // nstr should contain the string now
        val outMap = mutableMapOf<Int, Char>()
        for (s in fixes) {
            if (outMap.containsKey(s.length)) {
                val c = outMap[s.length]
                if (c == 'P')
                    print('S')
                else
                    print('P')
            } else {
                if (nstr.startsWith(s)) {
                    print('P')
                    outMap[s.length] = 'P'
                } else {
                    print('S')
                    outMap[s.length] = 'S'
                }
            }
        }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}