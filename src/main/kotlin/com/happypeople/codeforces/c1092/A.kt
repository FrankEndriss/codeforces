package com.happypeople.codeforces.c1092

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        println("")
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        for(i in 1..t) {
            val len=sc.nextInt()
            val count=sc.nextInt()
            for(i in 0..len-1) {
                val c='a'+(i%count)
                print(c)
            }
            println()
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}