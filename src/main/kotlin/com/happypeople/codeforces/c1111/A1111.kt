//package com.happypeople.codeforces.c1111

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1111().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1111 {
    fun run() {
        val sc = Scanner(systemIn())
        val s=sc.next().trim()
        val t=sc.next().trim()

        val v=setOf('a', 'e', 'i', 'o', 'u')

        if(s.length!=t.length) {
            println("No")
        } else {
            for (i in 0..s.length-1) {
                val sc=v.contains(s[i])
                val tc=v.contains(t[i])
                if(sc!=tc) {
                    println("No")
                    return
                }
            }
            println("Yes")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
