package com.happypeople.codeforces.c1111

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        C1111().run()
    } catch (e: Throwable) {
        println("$e")
        e.printStackTrace(System.out)
    }
}

class C1111 {
    var a = listOf(0)
    var spos = FenwickSparse(2, 0) { e1, e2 -> e1 + e2 }
    var B = 0L
    var A = 0L
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val k = sc.nextInt()
        A = sc.nextLong()
        B = sc.nextLong()
        a = (1..k).map { sc.nextInt() }.sorted()
        spos = FenwickSparse((2 shl (n-1))+1, 0) { e1, e2 -> e1 + e2 }
        for (i in a.indices)
            spos.updateCumul(a[i], 1)

        val ans = calc(1, (2 shl (n - 1))+1)
        println("$ans")
    }

    /** @return true if there is at least one superhero in interval lInkl:rExkl */
    var searchIdx=-1
    fun hasSuperheros(l:Int, r:Int):Boolean {
        // TODO does not work correct
        while(searchIdx<a.size-2 && a[searchIdx+1]<=l) {
           searchIdx++
        }
        return searchIdx>=0 && searchIdx<a.size && a[searchIdx]<r
    }

    fun calc(l: Int, r: Int): Long {
        //if(!hasSuperheros(l, r))
        //    return A

        val superheros = spos.readCumul(r - 1) - spos.readCumul(l - 1)
        if(superheros==0)
            return A

        val calced = B * superheros * (r - l)

        // println("calc for l=$l r=$r -> $calced")
        var ret = 0L
        if (r - l == 1)
            ret = calced
        else
            ret = min(
                    calced,
                    calc(l, l + ((r - l) / 2)) + calc(l + ((r - l) / 2), r))
        //println("ret for l=$l r=$r -> $ret")
        return ret
    }

    class Lsb {
        companion object {
            fun lsb(i: Int) = i and -i
        }
    }

    class FenwickSparse<E>(val sizeofTree: Int, val neutral: E, val cumul: (E, E) -> E) : FenwickIf<E> {
        val tree = mutableMapOf<Int, E>()
        /** TODO find a way to dynamically grow the tree to new sizes. */

        /** Updates the value at pIdx cumulative, i.e. "adds" value to the value stored at pIdx. */
        override fun updateCumul(pIdx: Int, value: E) {
            if (pIdx < 1 || pIdx>sizeofTree)
                throw IllegalArgumentException("bad idx1, min=1, max=${sizeofTree}, you sent: $pIdx")
            var idx = pIdx
            while (idx < sizeofTree) {
                tree[idx] = cumul(tree.getOrDefault(idx, neutral), value)
                idx += Lsb.lsb(idx)
            }
        }

        /** @return the cumul of range 0:pIdx inclusive. */
        override fun readCumul(pIdx: Int): E {
            var idx = pIdx
            var sum = neutral
            while (idx > 0) {
                sum = cumul(sum, tree.getOrDefault(idx, neutral))
                idx -= Lsb.lsb(idx)
            }
            return sum
        }

    }

    interface FenwickIf<E> {
        /** Updates the value at pIdx cumulative, i.e. "adds" value to the value stored at pIdx.
         * @param pIdx 1 based index into structure.
         * @param value the delta for the Update. */
        fun updateCumul(pIdx: Int, value: E)

        /** @return the cumul of range 0:pIdx inclusive. */
        fun readCumul(pIdx: Int): E

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
