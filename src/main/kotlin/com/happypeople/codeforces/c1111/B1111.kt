package com.happypeople.codeforces.c1111

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1111().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1111 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val k = sc.nextInt()
        var m = sc.nextInt()
        val a = (1..n).map { sc.nextLong() }.toMutableList()
        val aIdx = (0..n - 1).map { it }.sortedBy { a[it] }
        var aSum = a.sum().toDouble()

        val maxInc = k.toLong() * n

        var i = 0
        var removed = 0
        var ops = 0
        var incIdx = aIdx.size - 1
        var opIdx = BooleanArray(n)
        while (i < n && ops < m) {
            /*
            if(printLog) {
                val avg=aSum / (a.size - removed)
                val curA=a[aIdx[i]]
                log("currentA: $curA avg=$avg")
            }
            */
            if (a[aIdx[i]] < (aSum / (a.size - removed)) - 1) {
                //log("removing at ${aIdx[i]}")
                aSum -= a[aIdx[i]]
                opIdx[i] = true
                removed++
                ops++
                i++
            } else {
                while (incIdx >= 0 && opIdx[incIdx])
                    incIdx--
                if (incIdx >= 0) {
                    val inc = min(m - ops, k)
                    //log("incrementing at ${aIdx[incIdx]} by $inc")
                    opIdx[incIdx]=true
                    a[aIdx[incIdx]]+=inc.toLong()
                    aSum = aSum + inc
                    ops += inc
                } else {    // remove without -1 check
                    if (a[aIdx[i]] < aSum / (a.size - removed)) {
                        //log("removing at ${aIdx[i]} wo/ -1-check")
                        aSum -= a[aIdx[i]]
                        removed++
                        opIdx[i]=true
                        ops++
                        i++
                    } else {
                        i=n // break
                    }
                }
            }
        }
        val ans = aSum / (a.size - removed)
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
