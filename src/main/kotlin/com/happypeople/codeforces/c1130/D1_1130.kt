package com.happypeople.codeforces.c1130

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        D1_1130().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1_1130 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val m = sc.nextInt()
        val s = arrOf(n) { mutableListOf<Int>() }
        for (i in (1..m)) {
            val station = sc.nextInt() - 1
            val target = sc.nextInt() - 1
            s[station].add(target)
        }

        // At every Station we pack one candy, the farthest first, the nearest last.
        // So we need numCandies-1 full rounds, plus the nearest.
        // The stationNumber plus the nearest candy of that station is the farthest way we need to travel
        // The sum increases / decreases for every one station if starting at the next station.

        // brute force
        for(start in 0..n-1) {
            var maxTravel = 0
            for(station in 0..n-1) {
                var first=station-start
                if(first<0)
                    first+=n

                var last=if(s[station].size==0) 0 else {
                    s[station].map {
                        val x=it-station
                        if(x<0)
                            x+n
                        else
                            x
                    }.min()!!
                }

                val dist = max(0, s[station].size-1)*n + first + last

                maxTravel=max(maxTravel, dist)
            }
            print("$maxTravel ")
        }
        println()
    }

    inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
        val a = arrayOfNulls<E>(size)
        for (i in a.indices)
            a[i] = init(i)
        return a as Array<E>
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
