package com.happypeople.codeforces.c1130

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        C1130().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

var grid=listOf(mutableListOf(false))
var n=0

class C1130 {
    fun run() {
        val sc = Scanner(systemIn())
        n=sc.nextInt()
        val y1=sc.nextInt()-1
        val x1=sc.nextInt()-1
        val y2=sc.nextInt()-1
        val x2=sc.nextInt()-1
        grid=(1..n).map { sc.next().trim().map { it=='0' }.toMutableList() }
        //log("grid: $grid")

        val set1=fillSet(x1, y1)
        val set2=fillSet(x2, y2)

        var minCost=Int.MAX_VALUE
        for(p1 in set1) {
            for(p2 in set2) {
                   minCost=min(minCost, tunnelCost(p1.first, p1.second, p2.first, p2.second) )
            }
        }
        println("$minCost")
    }

    val dirs=setOf(Pair(1, 0), Pair(-1, 0), Pair(0, 1), Pair(0, -1))

    fun fillSet(x1: Int, y1: Int): Set<Pair<Int, Int>> {
        var set=mutableSetOf(Pair(x1, y1))
        var next=setOf(Pair(x1, y1))
        while(!next.isEmpty()) {
            next=next.flatMap { p ->
                dirs.map {
                Pair(it.first+p.first, it.second+p.second)
            } }.filter {
                it.first>=0 && it.first<n && it.second>=0 && it.second<n && grid[it.second][it.first] && (!set.contains(it))
            }.toSet()
            //log("next: $next")
            //log("all : $set")
            set.addAll(next)
        }
        return set
    }

    fun tunnelCost(x1:Int, y1:Int, x2:Int, y2:Int):Int {
        val x=abs(x1-x2)
        val y=abs(y1-y2)
        return x*x + y*y
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
