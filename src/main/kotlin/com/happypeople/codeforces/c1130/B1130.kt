package com.happypeople.codeforces.c1130

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1130().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}
class B1130 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n*2).map { sc.nextInt() }

        val pos=arrOf(2) { IntArray(n+1) }
        for(i in pos[0].indices) {
            pos[0][i]=-1
            pos[1][i]=-1
        }

        for(i in a.indices) {
            if(pos[0][a[i]]==-1)
                pos[0][a[i]]=i
            else
                pos[1][a[i]]=i
        }

        var sumSteps=0
        pos[0][0]=0
        pos[1][0]=0
        for(i in 1..n) {
            sumSteps+=min(
                    abs(pos[0][i-1]-pos[0][i]) + abs(pos[1][i-1]-pos[1][i]),
                    abs(pos[1][i-1]-pos[0][i]) + abs(pos[0][i-1]-pos[1][i])
            )
        }
        println("$sumSteps")
    }

    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
