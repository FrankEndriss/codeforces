package com.happypeople.codeforces.c1130

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1130().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1130 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }

        val pos=a.filter { it>0 }.count()
        val neg=a.filter { it<0 }.count()
        val c=(n+1)/2
        if(pos>=c)
            println("1")
        else if(neg>=c)
            println("-1")
        else
            println("0")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
