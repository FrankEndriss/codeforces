package com.happypeople.codeforces.c1114

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1114().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1114 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val c=(1..n).map { sc.nextInt() }

        val ans=solve(c, 0, n-1)
        println("$ans")
    }

    // fuck this shit... does not work
    fun solve(c:List<Int>, pL:Int, pR:Int):Int {
        //log("solve l=$pL r=$pR")
        // find biggest segment with left/right same number in range l..r
        val lMap=mutableSetOf<Int>()
        val rMap=mutableSetOf<Int>()
        var l=pL
        var r=pR
        var x=0
        while(l<r) {
            if(lMap.contains(c[r])) {
                val fitL=findNext(c, pL, c[r], 1)
                //log("l contains r, fitL=$fitL r=$r x=$x")
                return x+solve(c, fitL+1, r-1)
            }
            if(rMap.contains(c[l])) {
                val fitR=findNext(c, pR, c[l], -1)
                //log("r contains l, fitR=$fitR l=$l")
                return x+solve(c, l+1, fitR-1)
            }
            val cl=c[l]
            l++
            while(c[l]==cl && l<c.size)
                l++
            x++
            if(r>l) {
                val cr=c[r]
                r--
                while(cr==c[r] && r>0)
                    r--
                x++
            }
            lMap.add(c[l])
            rMap.add(c[r])
            //log("inc")
        }
        //log("solve ret=$x")
        return x
    }

    fun findSeg(c:List<Int>, offs:Int, v:Int, dir:Int):Int {
        var offs=offs
        while(offs+dir>=0 && offs+dir<=c.size && c[offs+dir]==v)
            offs+=dir
        return offs
    }

    fun findNext(c:List<Int>, offs:Int, v:Int, dir:Int):Int {
        var offs=offs
        while(c[offs]!=v)
            offs+=dir
        return findSeg(c, offs, v, dir)
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
