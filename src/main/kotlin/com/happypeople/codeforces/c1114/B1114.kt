package com.happypeople.codeforces.c1114

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1114().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1114 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt() // len of pretty vals and min len of subarrays
        val k=sc.nextInt() // num of sub arrays
        val A=(1..n).map { sc.nextInt() }
        val aIdx=(0..n-1).sortedBy { -A[it] }
        val took=BooleanArray(n)
        var sum=0L
        for(i in 0..m*k-1) {
            sum+=A[aIdx[i]]
            took[aIdx[i]] = true
        }

        println("$sum")
        //log("$took")
        var j=0
        for(i in 0..k-2) {
            //log("$i $j")
            var c=0
            while(c<m) {
                if (took[j++])
                    c++
            }
            print("${j} ")
        }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
