package com.happypeople.codeforces.c1114

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1114().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1114 {
    fun run() {
        val sc = Scanner(systemIn())
        log("needs to be implemented")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
