package com.happypeople.codeforces.c1114

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1114().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1114 {
    fun run() {
        val sc = Scanner(systemIn())
        val hGreen=sc.nextInt()
        val hNotBlack=sc.nextInt()
        val hAny=sc.nextInt()
        var green=sc.nextInt()
        var purple=sc.nextInt()
        var black=sc.nextInt()

        val m1=hGreen<=green
        green-=hGreen
        val m2=hNotBlack<=green+purple
        val m3=hAny<=green+purple+black-hNotBlack
        val ans =m1 and m2 and m3
        if(ans)
            println("YES")
        else
            println("NO")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
