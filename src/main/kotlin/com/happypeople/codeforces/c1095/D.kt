package com.happypeople.codeforces.c1095

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D().run()
    } catch (e: Throwable) {
        D.log("" + e)
    }
}

class D {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()

        // maps child to its two rembbered
        val childs=mutableMapOf<Int, List<Int>>()
        (1..n).map { childs[it]=listOf(sc.nextInt(), sc.nextInt()) }

        val rememberedBy=mutableMapOf<Int, MutableSet<Int>>()
        childs.forEach { mapEnt ->
            rememberedBy.computeIfAbsent(mapEnt.value[0]) { mutableSetOf(mapEnt.key) }
            rememberedBy.computeIfAbsent(mapEnt.value[1]) { mutableSetOf(mapEnt.key) }
            rememberedBy[mapEnt.value[0]]!!.add(mapEnt.key)
            rememberedBy[mapEnt.value[1]]!!.add(mapEnt.key)
        }

        if(childs.size==3) { // special case, any order is ok
            println(childs.keys.joinToString(" "))
            return
        }
        // Start at any child x. There are two childs remembering x. One of them remembers
        // x.first or x.second, too. That is the pre of x
        // Collect until x is found again. Use indexe to run in O(log n).

        val childsOrdered= mutableListOf<Int>()
        var x=childs.keys.first()
        var remsOfX=childs[x]
        childsOrdered.add(x)
        while(childsOrdered.size<childs.size) {
            val remsOfX=childs[x]!!
            // this query finds two entries
            val preOfX=rememberedBy[x]!!.toList()
            val remembOfPre0=childs[preOfX[0]]!!

            if(remsOfX.contains(remembOfPre0[0]) || remsOfX.contains(remembOfPre0[1])) {
                x=preOfX[0]
            } else {
                x=preOfX[1]
            }
            childsOrdered.add(x)
        }
        childsOrdered.reversed().forEach { print("$it ") }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}