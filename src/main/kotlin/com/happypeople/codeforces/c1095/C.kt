package com.happypeople.codeforces.c1095

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        C.log("" + e)
    }
}

/** Accepted https://codeforces.com/problemset/submission/1095/47842619 */
class C {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val k=sc.nextInt()

        var n2=n
        val powers= mutableListOf<Int>()
        while(n2>0) {
            val x=maxp2(n2)
            powers.add(x)
            n2-=x
        }

        if(powers.size>k || n<k )
            println("NO")
        else {
            println("YES")
            var neededDevs=k-powers.size
            powers.forEach { neededDevs=devides(it, neededDevs) }
            println()
        }
    }

    fun devides(n:Int, pDevs:Int):Int {
        var devs=pDevs
        if(devs==0 || n==1) {
            print("$n ")
            return devs
        } else {
            devs=devides(n/2, devs-1)
            devs=devides(n/2, devs)
        }
        return devs
    }

    fun maxp2(i:Int):Int {
        var p2=1
        while(p2*2<=i)
            p2*=2

        return p2
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}