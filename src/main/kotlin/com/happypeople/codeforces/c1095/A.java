// package com.happypeople.codeforces.c1095;

import java.util.Scanner;

public class A {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        s.nextLine();
        String str = s.nextLine();

        int inc = 1;
        String res = "";
        for (int idx = 0; idx < str.length(); idx += inc) {
            res = res + str.charAt(idx);
            inc++;
        }

        System.out.println(res);

    }
}
