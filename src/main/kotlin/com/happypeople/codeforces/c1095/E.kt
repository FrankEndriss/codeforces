package com.happypeople.codeforces.c1095

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        E().run()
    } catch (e: Throwable) {
        E.log("" + e)
    }
}

/** for solution see https://codeforces.com/problemset/problem/1095/E */
class E {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val str=sc.next()!!

        val prefBal= mutableListOf<Int>()
        val prefCan=mutableListOf<Boolean>()
        var currOpen=0
        str.forEachIndexed { index, it ->
            if (it == '(') {
                currOpen++
            } else {
                currOpen--
            }
            prefBal.add(currOpen)
            if(prefCan.size==0)
                prefCan.add(currOpen>=0)
            else
                prefCan.add(prefCan[index-1] && currOpen>=0)
        }

        val sufBal= mutableListOf<Int>()
        val sufCan=mutableListOf<Boolean>()
        var currClose=0
        str.reversed().forEachIndexed { index, it ->
            if (it == ')') {
                currClose++
            } else {
                currClose--
            }
            sufBal.add(currClose)
            if(sufCan.size==0)
                sufCan.add(currClose>=0)
            else
                sufCan.add(sufCan[index-1] && currClose>=0)
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}