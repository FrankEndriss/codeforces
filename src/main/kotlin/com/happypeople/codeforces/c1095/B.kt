package com.happypeople.codeforces.c1095

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        B.log("" + e)
    }
}

class B {
    data class Entry(val id:Int, val v:Int)

    fun run() {
        val sc = Scanner(systemIn())
        val len=sc.nextInt()

        var maxA=mutableListOf(Entry(-1, Int.MIN_VALUE), Entry(-1, Int.MIN_VALUE))
        var minA=mutableListOf(Entry(-1, Int.MAX_VALUE), Entry(-1, Int.MAX_VALUE))

        (1..len).forEach{
            val e=Entry(it, sc.nextInt())
            if(e.v>maxA[0].v) {
                maxA.add(0, e)
            } else if(e.v>maxA[1].v)
                maxA.add(1, e)

            if(e.v<minA[0].v) {
                minA.add(0, e)
            } else if(e.v<minA[1].v)
                minA.add(1, e)
        }
        val diffMax=abs(maxA[0].v-maxA[1].v)
        val diffMin=abs(minA[0].v-minA[1].v)

        if(diffMax>diffMin)
            println(maxA[1].v-minA[0].v)
        else
            println(maxA[0].v-minA[1].v)
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}