package com.happypeople.codeforces.c1095

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        F().run()
    } catch (e: Throwable) {
        F.log({ "" + e })
    }
}

class F {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val m = sc.nextInt()

        var minVertexVal = Long.MAX_VALUE
        var minVertex = -1
        val edgeCosts = LongArray(n + m)
        for (it in 0..n - 1) {
            edgeCosts[it] = sc.nextLong()
            if (minVertexVal > edgeCosts[it]) {
                minVertexVal = edgeCosts[it]
                minVertex = it
            }
        }

        val edgeX = IntArray(n + m)
        val edgeY = IntArray(n + m)
        for (it in 0..n - 1) {
            if (it != minVertex) {
                edgeX[it] = minVertex
                edgeY[it] = it
                edgeCosts[it] = edgeCosts[it] + minVertexVal
            } else {
                edgeX[it] = -1
                edgeY[it] = -1
            }
        }

        // the offers
        for (it in 0..m - 1) {
            edgeX[n + it] = sc.nextInt() - 1
            edgeY[n + it] = sc.nextInt() - 1
            edgeCosts[n + it] = sc.nextLong()
        }

        val sortedEdges = (0..n + m - 1).sortedBy { edgeCosts[it] }

        val graph = object : Kruskal.Graph<Long> {
            override fun forEachEdge(cb: (w: Long, x: Int, y: Int) -> Boolean) {
                for (it in sortedEdges.indices) {
                    if (edgeX[sortedEdges[it]] >= 0) {     // minValue is marked with -1
                        if (!cb(edgeCosts[sortedEdges[it]], edgeX[sortedEdges[it]], edgeY[sortedEdges[it]]))
                            break
                    }
                }
            }
        }

        var ans = 0L
        Kruskal.minimumSpanningTree(graph, n) { w, x, y ->
            // log { "result: w=$w x=$x y=$y" }
            ans += w
        }

        println("$ans")
    }

    class Kruskal {
        interface Graph<W> {
            fun forEachEdge(cb: (w: W, x: Int, y: Int) -> Boolean)
        }

        companion object {

            private fun merge(p: IntArray, pX: Int, pY: Int): Boolean {
                var x = getLeader(p, pX)
                var y = getLeader(p, pY)
                if (x == y)
                    return false
                p[x] = y
                return true
            }

            private fun getLeader(p: IntArray, pX: Int): Int {
                if (pX == p[pX])
                    return pX
                p[pX] = getLeader(p, p[pX])
                return p[pX]
            }

            public fun <W> minimumSpanningTree(graph: Graph<W>, maxNodes: Int, result: (w: W, x: Int, y: Int) -> Unit) {

                val p = IntArray(maxNodes)
                for (i in p.indices)
                    p[i] = i

                var nodeCount = 0

                graph.forEachEdge { w, x, y ->
                    if (merge(p, x, y)) {
                        nodeCount++
                        result(w, x, y)
                    }
                    nodeCount < maxNodes
                }
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: () -> String) {
            if (printLog)
                println(str())
        }
    }
}