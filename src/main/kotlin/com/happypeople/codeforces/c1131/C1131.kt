package com.happypeople.codeforces.c1131

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1131().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1131 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }.sorted()

        val eves=a.filterIndexed { index, i ->  index%2==0 }
        val odds=a.filterIndexed { index, i ->  index%2==1 }

        for(i in 0..eves.size-1)
            print("${eves[i]} ")
        for(i in odds.size-1 downTo 0)
            print("${odds[i]} ")

        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
