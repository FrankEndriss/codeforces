package com.happypeople.codeforces.c1131

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1131().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1131 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val a = (1..n).map {
            Pair(sc.nextInt(), sc.nextInt())
        }.toSet().toList().sortedBy { it.first + it.second }.filter { it.first + it.second > 0 }

        var ans = 1
        var pOld = Pair(0, 0)
        a.forEach {
            val r1 = pOld.first..it.first
            val r2 = pOld.second..it.second
            //log("$r1 $r2")
            ans += inters(r1, r2)
            if(pOld.first==pOld.second)
                ans--
            pOld = it
        }
        if(ans==0)
            ans=1
        println("$ans")
    }

    private fun inters(r1: IntRange, r2: IntRange): Int {
        val s1=Pair(min(r1.first, r1.last), max(r1.first, r1.last))
        val s2=Pair(min(r2.first, r2.last), max(r2.first, r2.last))
        val res=min(s1.second, s2.second)-max(s1.first, s2.first)+1
        return if(res<0) 0 else res
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
