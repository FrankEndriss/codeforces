package com.happypeople.codeforces.c1131

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1131().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1131 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()
        val s=(1..n).map { sc.next().trim() }
        val p=mutableListOf<Pair<Int, Int>>()
        for(i in s.indices)
            p.add(Pair(0, i))
        for(j in 0..s[0].length-1)
            p.add(Pair(1, j))

        // 1. build a tree by connecting every dish (parent) with any dish better than itself (childs)
        // 2. The one without childs is the best, remove it from all others
        // repeat 2.
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
