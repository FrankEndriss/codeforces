package com.happypeople.codeforces.c1131

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1131().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1131 {
    fun run() {
        val sc = Scanner(systemIn())
        val w1=sc.nextInt()
        val h1=sc.nextInt()
        val w2=sc.nextInt()
        val h2=sc.nextInt()

        val lb1=Pair(1, 1)
        val rt1=Pair(w1, h1)
        val lb2=Pair(1, h1+1)
        val rt2=Pair(w2, h1+h2)

        var ans=(w1+2)+(h1*2) + (h2*2) + 2 + w2 + (w1-w2)
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
