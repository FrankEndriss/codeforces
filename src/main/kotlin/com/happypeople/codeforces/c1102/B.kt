package com.happypeople.codeforces.c1102

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        B.log("" + e)
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val k = sc.nextInt()
        val A=IntArray(n+1)
        var aCount=IntArray(5001)

        for(i in 1..n) {
            A[i]=sc.nextInt()
            aCount[A[i]]=aCount[A[i]]+1
        }

        log("${A.toList()}")
        log("${aCount.toList()}")

        var lMax=aCount.max()!!
        val pos=lMax<=k && k<=n
        log("$lMax <= $k")

        if(!pos) {
            println("NO")
        } else {
            println("YES")

            var color=1
            val aColors=IntArray(n+1)
            aCount=IntArray(5001)
            for(i in 1..n) {
                aColors[i] = aCount[A[i]]+1
                aCount[A[i]]=aCount[A[i]]+1
            }

            log("lMax=$lMax k=$k")
            for(extraC in lMax+1..k) {
                // find any color used twice and set the first of the usages to extraC
                val countDoubles=IntArray(k+1)
                for(i in 1..n) {
                    countDoubles[aColors[i]]=countDoubles[aColors[i]]+1
                    if(countDoubles[aColors[i]]>1) {
                        aColors[i] = extraC
                        break
                    }
                }
            }
            log("${aColors.toList()}")
            for(i in 1..n)
                print("${aColors[i]} ")
            println()
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}