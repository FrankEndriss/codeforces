package com.happypeople.codeforces.c1102

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        F1102().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
    val a= arrayOfNulls<E>(size)
    for(i in a.indices)
        a[i]=init(i)
    return a as Array<E>
}

class F1102 {
    /** max K of one column */
    /*
    fun maxK(a:IntArray):Int {
        var ret=0
        a.sort()
        val half=(a.size+1)/2
        for(i in 0..half-1) {
            ret=max(ret, abs(a[i*2]-a[i*2+half]))
            if((i+1)*2<half)
                ret=max(ret, abs(a[i*2+half]-a[(i+1)*2]))
        }
        return ret
    }
    */

    /** Calculate the K of a for permutation p */
    fun calcK(a:IntArray, p:List<Int>):Int {
        var ret=Int.MAX_VALUE
        for(i in 0..p.size-2) {
            ret=min(ret, abs(a[p[i]]-a[p[i+1]]))
            if(ret==0)
                return ret
        }
        return ret
    }

    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()
        val a=arrOf(m) { IntArray(n)}
        for(i in 0..n-1)
            for(j in 0..m-1)
                a[j][i]=sc.nextInt()

        val p=(0..n-1).map { it }
        val seq=Permutations.permutations(p).iterator()

        // brute force
        var maxK=Int.MIN_VALUE
        while(seq.hasNext()) {
            val p=seq.next()
            var minK=Int.MAX_VALUE
            for(i in a.indices) {
                val k=calcK(a[i], p)
                minK=min(k, minK)
                if(minK<=0)
                    break
            }
            for(i in 0..a.size-2) {
                val k=abs(a[i][p[n-1]]-a[i+1][p[0]])
                minK=min(k, minK)
                if(minK<=0)
                    break
            }
            maxK=max(maxK, minK)
        }
        println("$maxK")
    }

    class Permutations {
        companion object {
            /** @return Sequence of all permutations of List<T>, including List<T> itself.
             * The order is so that it "counts up",
             * ie [1,2,3] -> [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1]
             */
            fun <T> permutations(col: Iterable<T>): Sequence<List<T>> {
                val list = col.toList()
                var idx:IntArray? = null

                return generateSequence {
                    if (idx==null) {
                        idx = (0..list.size - 1).toList().toIntArray()
                        list
                    } else {
                        if (nextPermutation(idx!!))
                            idx!!.map { list[it] }
                        else
                            null
                    }
                }
            }

            fun nextPermutation(numList: IntArray): Boolean {
                /*
                by Sani Singh Huttunen
             Knuths
             1. Find the largest index j such that a[j] < a[j + 1]. If no such index exists, the permutation is the last permutation.
             2. Find the largest index l such that a[j] < a[l]. Since j + 1 is such an index, l is well defined and satisfies j < l.
             3. Swap a[j] with a[l].
             4. Reverse the sequence from a[j + 1] up to and including the final element a[n].
             */
                var largestIndex = -1
                for (i in numList.size - 2 downTo 0) {
                    if (numList[i] < numList[i + 1]) {
                        largestIndex = i
                        break
                    }
                }

                if (largestIndex < 0)
                    return false

                var largestIndex2 = -1
                for (i in numList.size - 1 downTo 0) {
                    if (numList[largestIndex] < numList[i]) {
                        largestIndex2 = i
                        break
                    }
                }

                var tmp = numList[largestIndex]
                numList[largestIndex] = numList[largestIndex2]
                numList[largestIndex2] = tmp

                var i = largestIndex + 1
                var j = numList.size - 1
                while (i < j) {
                    tmp = numList[i]
                    numList[i] = numList[j]
                    numList[j] = tmp
                    i++
                    j--
                }

                return true
            }
        }
    }
    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
