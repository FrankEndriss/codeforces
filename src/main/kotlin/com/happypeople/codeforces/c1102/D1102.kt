package com.happypeople.codeforces.c1102

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1102().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1102 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val s=sc.next().trim().toCharArray()
        val count=IntArray(3)
        for(c in s)
            count[c-'0']++

        var idx=0
        while(count[0]<n/3) {
            if(count[2]>n/3 && s[idx]=='2') {
                s[idx]='0'
                count[0]++
                count[2]--
            } else if(count[1]>n/3 && s[idx]=='1') {
                s[idx]='0'
                count[0]++
                count[1]--
            }
            idx++
            //log("1 $count")
        }

        idx=n-1
        while(count[2]<n/3) {
            if(count[0]>n/3 && s[idx]=='0') {
                s[idx]='2'
                count[0]--
                count[2]++
            } else if(count[1]>n/3 && s[idx]=='1') {
                s[idx]='2'
                count[2]++
                count[1]--
            }
            idx--
            //log("2 $count")
        }

        idx=0
        while(count[1]<n/3 && idx<n) {
            if(count[2]>n/3 && s[idx]=='2') {
                s[idx]='1'
                count[1]++
                count[2]--
            }
            idx++
            //log("3 $count")
        }

        idx=n-1
        while(count[1]<n/3) {
            if(s[idx]=='0') {
                s[idx]='1'
                count[1]++
                count[0]--
            }
            idx--
            //log("4 $count")
        }

        val ans=String(s)
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
