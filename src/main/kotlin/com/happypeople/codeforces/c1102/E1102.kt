package com.happypeople.codeforces.c1102

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        E1102().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class E1102 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()

        val a=IntArray(n)
        // maps a[i] to i of first occurence
        val aMinIdx=mutableMapOf<Int, Int>()
        val aMin=IntArray(n)
        val aMax=IntArray(n)
        for(it in 0..n-1) {
            a[it]=sc.nextInt()
            val firstIt=aMinIdx.getOrPut(a[it]) { it }
            aMin[it]=firstIt
        }
        aMinIdx.clear()

        // maps a[i] to i of last occurence
        val aMaxIdx=mutableMapOf<Int, Int>()
        for(it in n-1 downTo 0) {
            val lastIt=aMaxIdx.getOrPut(a[it]) { it }
            aMax[it]=lastIt
        }
        aMaxIdx.clear()

        var count=0
        var idx=0
        var maxPos=0
        while(idx<n) {
            count++
            maxPos=aMax[idx]
            while(idx<maxPos) {
                idx++
                maxPos=max(maxPos, aMax[idx])
            }
            idx++
        }

        val MOD=998244353
        var ans=1
        for(i in 1..count-1)
            ans=(ans*2)%MOD
        println("$ans")
    }

    class Multiset<E> {
        private val map = mutableMapOf<E, Int>()

        public fun add(e: E) {
            val c = map.getOrDefault(e, 0)
            map[e] = c + 1
        }

        public fun remove(e: E) {
            val c = map.getOrDefault(e, 0)
            if (c > 1)
                map[e] = c - 1
            else if (c == 1)
                map.remove(e)
        }

        public fun contains(e: E) =
                map.contains(e)

        public fun count(e: E) =
                map.getOrDefault(e, 0)

        public fun toList(): List<E> {
            return map.flatMap { ent -> (1..ent.value).map { ent.key } }
        }

        public fun toSet(): Set<E> = map.keys
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
