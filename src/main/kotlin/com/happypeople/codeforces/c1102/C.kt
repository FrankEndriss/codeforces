//package com.happypeople.codeforces.c1102

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        C.log("" + e)
    }
}

class C {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val x=sc.nextInt()
        val y=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }

        if(x>y)
            println("$n")
        else {
            val ans=(a.filter { it<=x }.count()+1) / 2
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}