package com.happypeople.codeforces.c1140

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        A1140().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1140 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        var maxDay=1
        var days=0
        (1..n).forEach {
            maxDay=max(maxDay, sc.nextInt())
            if(maxDay==it)
                days++
        }
        println("$days")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
