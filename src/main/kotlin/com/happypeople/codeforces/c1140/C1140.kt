package com.happypeople.codeforces.c1140

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        C1140().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1140 {
    var T = LongArray(1)
    var B = LongArray(1)
    var Pidx = listOf(0)

    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val k = sc.nextInt()
        T = LongArray(n)
        B = LongArray(n)
        for (i in 0..n - 1) {
            T[i] = sc.nextLong()
            B[i] = sc.nextLong()
        }
        Pidx = (0..n - 1).sortedBy { -B[it] * T[it] }

        //val pl=BooleanArray(n)
        //pl[Pidx[0]]=true
        var plMinB = B[Pidx[0]]
        var plSumT = T[Pidx[0]]

        var plPleasure = plMinB * plSumT    // pleasure of playlist

        val allSongs = (0..n - 1).toMutableSet()
        allSongs.remove(Pidx[0])
        var i = 1
        while (i < k) {    // add at most k-1 more songs
            var maxResult = -1L
            var maxResultAt = -1
            val iterset = allSongs.toList()
            for (j in iterset) { // find max increaser
                val nextResult = (plSumT + T[j]) * min(plMinB, B[j])
                if (nextResult > maxResult) {
                    maxResult = nextResult
                    maxResultAt = j
                }
            }
            if (plPleasure <= maxResult) {  // sum increases by adding maxResultAt to playlist
                plMinB = min(plMinB, B[maxResultAt])
                plSumT += T[maxResultAt]
                plPleasure = plSumT * plMinB
                allSongs.remove(maxResultAt)
                i++

                // and add all other songs with beauties bigger that B[maxResultAt], sorted by T[i] desc
                if (i < k) {
                    val byT = allSongs.filter { B[it] >= plMinB }.sortedBy { -T[it] }
                    for (j in byT) {
                        plSumT += T[j]
                        allSongs.remove(j)
                        i++
                        if (i == k)
                            break
                    }
                    plPleasure = plSumT * plMinB
                }
            } else
                break
        }
        println("$plPleasure")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
