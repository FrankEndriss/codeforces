package com.happypeople.codeforces.c1140

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1140().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1140 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        var ans=0L
        for(i in 3..n)
            ans+=(i*(i-1))

        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
