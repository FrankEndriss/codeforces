package com.happypeople.codeforces.c1140

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1140().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1140 {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        for (i in (1..t)) {
            val n = sc.nextInt()
            var s = sc.next().trim()

            var start = 0
            var end = 0
            while (s.startsWith("<")) {
                s = s.substring(1)
                start++
            }

            while (s.endsWith(">")) {
                s = s.substring(0, s.length-1)
                end++
            }

            var ans = min(start, end)
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
