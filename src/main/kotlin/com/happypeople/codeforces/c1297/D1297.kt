package com.happypeople.codeforces.c1297

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1297().run()
    } catch (e: Throwable) {
        println("")
    }
}

class D1297 {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        (1..t).forEach { solve(sc) }
        val mp=mutableMapOf<Int,Int>()
        mp[10]=2
        mp[10]=mp.getOrDefault(10, 0)-1
        if(mp.getOrDefault(10, 0)>0) print("yes")
        else print("no")
    }

    fun solve(sc : Scanner) {
        val n=sc.nextInt()
        val k=sc.nextInt()
        val a=(1..n).map { Pair(sc.nextInt(), it) }.sortedBy { -it.first }
        /* Pay the second one max possible d, the third one max possible...
        * if still d available, distribute equal among all, starting with higest
        **/

        val ans=IntArray(n)
        var d=0
        (1..n-1).forEach {
            val diff=a[it-1].first+ans[it-1] - a[it].first
            if(diff>1) {
                if(diff <= k-d)
                    ans[it]=diff-1
                else
                    ans[it]=k-d
            } else
                ans[it]=0

            d+=ans[it]
        }

        val v=(k-d)/n
        if(v>0) {
            (0..n - 1).forEach {
                ans[it] +=v
                d+=v
            }
        }
        (1..(k-d)).forEach {
            ans[it-1]++
        }

        (0..n-1).map {
            Pair(ans[it], a[it].second)
        }.sortedBy { it.second }.forEach {
            print("${it.first} ")
        }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}