package com.happypeople.codeforces.c1297

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1297().run()
    } catch (e: Throwable) {
        println("")
    }
}

class C1297 {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        (1..t).forEach {
            solve(sc);
        }
    }
    fun solve(sc: Scanner) {
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }
        /* remove smallest positive or add smallest negative
        * that for we want to know the index of the smallest
        * positve or biggest negative, whichevers abs value is less.
         */

        var idx=-1
        var v=1000000000
        a.forEachIndexed { index, element ->
           if(element>0 && element<v) {
               v=element
               idx=index
           } else if(element<0 && -element<v) {
               v=-element
               idx=index
           }
        }
        assert(idx>=0)

        var ans=a.filter { it>0 }.sum()
        ans-=v

        println("$ans")
        a.forEachIndexed { index, element ->
            if(index!=idx) {
                if(element<=0)
                    print("0")
                else
                    print("1")
            } else {
                if(element<0)
                    print("1")
                else
                    print("0")
            }
        }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}