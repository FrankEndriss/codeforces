package com.happypeople.codeforces.c1297

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1297().run()
    } catch (e: Throwable) {
        println("")
    }
}

class A1297 {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        (1..t).forEach {
            val n=sc.nextInt()
            var ans=n
            var ext=""
            if(n>=1000000) {
                ans=(n+500000)/1000000
                ext="M"
            } else if(n>=1000) {
                ans=(n+500)/1000
                ext="K"
                if(ans==1000) {
                    ans=1
                    ext="M"
                }
            }
            println("$ans$ext")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}