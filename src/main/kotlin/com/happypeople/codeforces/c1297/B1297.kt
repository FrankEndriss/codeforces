package com.happypeople.codeforces.c1297

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1297().run()
    } catch (e: Throwable) {
        println("")
    }
}

class B1297 {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        (1..t).forEach {
            val n=sc.nextInt()
            val list=mutableListOf<Pair<Int,Int>>()
            (1..n).forEach {
                val from=sc.nextInt()
                val to=sc.nextInt()+1
                list.add(Pair(from, 1))
                list.add(Pair(to,-1))
            }
            var idx=0
            var cnt=0
            var done=false
            list.sortedBy { it.first }.forEach {
                if (!done) {
                    if(cnt==1 && it.first!=idx && idx!=0) {
                        val ans=it.first-1
                        println("$ans")
                        done=true
                    } else {
                        cnt += it.second
                        idx=it.first
                    }
                }
            }
            if(!done)
                println("-1")
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}