package com.happypeople.codeforces.c1080

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        B.log("" + e)
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val q=sc.nextInt()
        for(i in 0 until q) {
            val l=sc.nextInt()
            val r=sc.nextInt()

            val res=
                if((l%2)==1 && (r%2)==1)
                    -((r/2)-(l/2))-l
                else if(l%2==0 && r%2==1)
                    -(r/2 - l/2 +1)
                else if(l%2==1 && r%2==0)
                    r/2 - l/2
                else if(l%2==0 && r%2==0)
                    -(r/2 - l/2)+r
                else
                    4242

            println("$res")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}