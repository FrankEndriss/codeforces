package com.happypeople.codeforces.c1133

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1133().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1133 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val k=sc.nextInt()
        val d=(1..n).map { sc.nextInt() }

        val freq=IntArray(k)
        for(i in d)
            freq[i%k]++

        var ans=(freq[0]/2)*2
        // log("1 ans=$ans")

        var i=1
        var j=k-1
        while(i<=j) {
            if(i<j)
                ans+=(min(freq[i], freq[j])*2)
            else
                ans+=(freq[i]/2)*2
            // log("i=$i j=$j ans=$ans")
            i++
            j--
        }
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
