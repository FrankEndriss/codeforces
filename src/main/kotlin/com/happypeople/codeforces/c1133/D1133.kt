package com.happypeople.codeforces.c1133

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.math.BigDecimal
import java.util.*

fun main(args: Array<String>) {
    try {
        D1133().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1133 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }
        val b=(1..n).map { sc.nextInt() }
        val freq=mutableMapOf<String, Int>()
        freq[BigDecimal.ZERO.toString()]=0

        var extra=0
        for(i in 0..n-1) {
            if(a[i]!=0) {
                var bd = BigDecimal(b[i])
                bd=bd.setScale(20)
                bd=bd.divide(BigDecimal(a[i]), BigDecimal.ROUND_HALF_UP)
                val bdStr=bd.setScale(18, BigDecimal.ROUND_HALF_UP).toString()
                val c=freq[bdStr]
                if(c==null)
                    freq[bdStr]=1
                else
                    freq[bdStr]=freq[bdStr]!!+1
            } else if(b[i]==0) {
                extra++
            }
        }
        var ans=freq.values.max()!!+extra
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
