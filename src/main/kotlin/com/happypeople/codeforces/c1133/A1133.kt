package com.happypeople.codeforces.c1133

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1133().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1133 {
    fun run() {
        val sc = Scanner(systemIn())
        var s1=sc.next().trim()
        var s2=sc.next().trim()

        var h1str=s1.substring(0, 2)
        if(h1str.startsWith("0"))
            h1str=h1str.substring(1, 2)
        var m1str=s1.substring(3, 5)
        if(m1str.startsWith("0"))
            m1str=m1str.substring(1, 2)

        var h2str=s2.substring(0, 2)
        if(h2str.startsWith("0"))
            h2str=h2str.substring(1, 2)
        var m2str=s2.substring(3, 5)
        if(m2str.startsWith("0"))
            m2str=m2str.substring(1, 2)


        val h1=h1str.toInt()
        val m1=m1str.toInt()
        val h2=h2str.toInt()
        val m2=m2str.toInt()

        var h=h2-h1
        var m=m2-m1

        val mSum=(h*60+m)/2

        h=h1+mSum/60
        m=m1+mSum%60
        if(m>=60) {
            h++
            m-=60
        }
        if(h<10)
            print("0")
        print("$h:")
        if(m<10)
            print("0")
        println("$m")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
