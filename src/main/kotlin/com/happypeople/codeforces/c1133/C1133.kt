package com.happypeople.codeforces.c1133

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        C1133().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1133 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }.sorted()

        var maxS=0
        var l=0
        var r=0
        while(r<a.size) {
            //log("l=$l r=$r")
            //log("ar=${a[r]} al=${a[l]}")
            while(a[r]-a[l]>5)
                l++
            maxS=max(maxS, r-l+1)
            r++
        }

        println("$maxS")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
