package com.happypeople.codeforces.c1133

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        F2_1133().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class F2_1133 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()
        val D=sc.nextInt()
        val graph=mutableMapOf<Int, MutableList<Int>>()

        var maxV=0
        for(i in (1..m)) {
            val u=sc.nextInt()
            val v=sc.nextInt()
            graph.getOrPut(u) { mutableListOf<Int>() }.add(v)
            graph.getOrPut(v) { mutableListOf<Int>() }.add(u)
            maxV= max(maxV, max(u, v))
        }

        var DVertex=-1
        // find the vertex with D connections
        for(i in graph) {
            val gS=i.value.size
            if(gS==D) {
                DVertex=i.key
                break
            }
        }
        if(DVertex==-1) {
            println("NO")
            return
            // sorry, thats wrong.
            // We could still use a vertex as first, but would have to remove
            // some edges from that vertex.
            // BUT that makes the construction of the tree complecated...
            // It must be still a spanning tree :/
        }
        println("YES")

        // then construct a tree from those connections.
        val connected=BooleanArray(maxV+1)
        connected[DVertex]=true
        // val connected= mutableListOf<Int>(maxVertex)
        var next=graph[DVertex]!!
        for(i in next)
            connected[i]=true

        // val pairs= mutableListOf<Pair<Int, Int>>()
        val pairsV=IntArray(m)
        val pairsU=IntArray(m)

        var pIdx=0
        for(i in next) {
            pairsV[pIdx]=DVertex
            pairsU[pIdx]=i
            pIdx++
        }

        while(!next.isEmpty()) {
            val nextnext=mutableListOf<Int>()
            for(v in next) {
                for(c in graph[v]!!) {
                    if(!connected[c]) {
                        connected[c]=true
                        nextnext.add(c)
                        pairsV[pIdx]=v
                        pairsU[pIdx]=c
                        pIdx++
                    }
                }
            }
            next=nextnext
        }

        var i=0
        for(i in 0..pIdx-1) {
            println("${pairsV[i]} ${pairsU[i]}")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
