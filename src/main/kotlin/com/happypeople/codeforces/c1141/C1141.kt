package com.happypeople.codeforces.c1141

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1141().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1141 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val q=(1..n-1).map { sc.nextInt() }

        val q2=IntArray(n)
        for(i in 1..n-1)
            q2[i]=q2[i-1]+q[i-1]

        val minQ=q2.min()!!
        for(i in q2.indices)
            q2[i]-=minQ
        val freq=IntArray(n)
        for(i in q2.indices) {
            if(q2[i]<0 || q2[i]>=freq.size) {
                println("-1")
                return
            }
            freq[q2[i]] = 1
        }

        if(freq.min()==1) {
            for(i in q2.indices)
                q2[i]++

            val s= q2.toList().joinToString(" ")
            println("$s")
        } else
            println("-1")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
