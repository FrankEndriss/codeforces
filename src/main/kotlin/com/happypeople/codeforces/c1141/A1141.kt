package com.happypeople.codeforces.c1141

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        A1141().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1141 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()

        val x=minmoves(n, m)
        val ans=if(x>9999) -1 else x
        println("$ans ($c)")
    }

    var c=0
    fun minmoves(n:Int, m:Int):Int {
        c++
        //log("minmoves $n $m")
        if(n==m)
            return 0
        else if(n>m)
            return 10000
        else
            return min(minmoves(n*2, m), minmoves(n*3, m))+1
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
