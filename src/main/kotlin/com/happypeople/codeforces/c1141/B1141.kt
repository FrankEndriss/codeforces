package com.happypeople.codeforces.c1141

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        B1141().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1141 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val A=(1..n).map { sc.nextInt() }
        // find longest sequence of 1s in a
        var len=0
        var maxlen=0
        for(a in A) {
            if(a==1) {
                len++
                maxlen=max(maxlen, len)
            } else
                len=0
        }
        // find prefix 1s
        var idx=0
        while(idx<A.size && A[idx]==1) {
            idx++
        }
        val prelen=idx
        var postlen=0
        var idx2=0
        if(prelen<A.size) {
            while(idx2<A.size && A[A.size-1-idx2]==1) {
                idx2++
            }
        }
        val ans=max(maxlen, idx+idx2)
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
