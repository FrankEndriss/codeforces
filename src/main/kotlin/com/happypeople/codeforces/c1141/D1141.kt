package com.happypeople.codeforces.c1141

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        D1141().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1141 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val l=sc.next().trim()
        val r=sc.next().trim()

        // freqs as maps of color to indices
        val fL=mutableMapOf<Char, MutableList<Int>>()
        val fR=mutableMapOf<Char, MutableList<Int>>()
        for(i in 0..n-1) {
            fL.getOrPut(l[i]) { mutableListOf<Int>() }.add(i)
            fR.getOrPut(r[i]) { mutableListOf<Int>() }.add(i)
        }

        val usedBootsL=Multiset<Char>()
        val usedBootsR=Multiset<Char>()
        var ans=0
        val sb=StringBuffer()
        for(lBoot in fL.keys) {
            if(lBoot!='?') {
                fR.getOrPut(lBoot) { mutableListOf<Int>() }
                for(i in 0..min(fL[lBoot]!!.size, fR[lBoot]!!.size)-1) {
                    sb.append("${fL[lBoot]!![i]+1} ${fR[lBoot]!![i]+1}\n")
                    ans++
                    usedBootsL.add(lBoot)
                    usedBootsR.add(lBoot)
                }
            }
        }

        val rKeys=fR.keys
        fL.getOrPut('?') { mutableListOf<Int>() }
        for(i in 0..fL['?']!!.size-1) {
            for(key in rKeys) { // find unused right boot
                val idx=usedBootsR.count(key)
                if(key!='?' && fR[key]!!.size>idx) {
                    sb.append("${fL['?']!![i]+1} ${fR[key]!![idx]+1}\n")
                    usedBootsL.add('?')
                    usedBootsR.add(key)
                    ans++
                    break
                }
            }
        }

        val lKeys=fL.keys
        fR.getOrPut('?') { mutableListOf<Int>() }
        for(i in 0..fR['?']!!.size-1) {
            for(key in lKeys) { // find unused right boot
                val idx=usedBootsL.count(key)
                if(fL[key]!!.size>idx) {
                    sb.append("${fL[key]!![idx]+1} ${fR['?']!![i]+1}\n")
                    usedBootsL.add(key)
                    usedBootsR.add('?')
                    ans++
                    break
                }
            }
        }

        println("$ans")
        println("$sb")
    }

    class Multiset<E> {
        private val map = mutableMapOf<E, Int>()

        public fun add(e: E) {
            val c = map.getOrDefault(e, 0)
            map[e] = c + 1
        }

        public fun remove(e: E) {
            val c = map.getOrDefault(e, 0)
            if (c > 1)
                map[e] = c - 1
            else if (c == 1)
                map.remove(e)
        }

        public fun contains(e: E) =
                map.contains(e)

        public fun count(e: E) =
                map.getOrDefault(e, 0)

        public fun toList(): List<E> {
            return map.flatMap { ent -> (1..ent.value).map { ent.key } }
        }

        public fun toSet(): Set<E> = map.keys
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
