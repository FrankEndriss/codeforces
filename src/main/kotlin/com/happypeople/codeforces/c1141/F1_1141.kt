package com.happypeople.codeforces.c1141

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        F1_1141().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class F1_1141 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }.toIntArray()

        // brute force
        val sum1=0
        for(i in 0..a.size-2) {
            val sum2=0
            for(j in i+1..a.size-1) {

            }
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
