package com.happypeople.codeforces.c1141

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        E1141().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class E1141 {
    fun run() {
        val sc = Scanner(systemIn())
        val H=sc.nextLong()
        val n=sc.nextInt()
        val d=(1..n).map { sc.nextInt() }

        val sumRound=1L*d.sum()
        var minRound=0L
        var sumR=0L
        for(l in d) {
            sumR+=l
            minRound=min(minRound, sumR)
        }

        if(-minRound<H && -sumRound<=0) {
            println("-1")
        } else {
            log("minRound=$minRound sumRound=$sumRound")
            var ans=0L
            if(sumRound!=0L)
                ans=((H+minRound)/-sumRound)*n   // full rounds
            var hRest=H+(sumRound*(ans/n))
            log("fullRounds=$ans hRest=$hRest")
            if(hRest>-minRound) {
                hRest+=sumRound
                ans+=n
            }
            log("hRest=$hRest ans=$ans")
            if(hRest>0) {
                for(l in d) {
                    hRest+=l
                    ans++
                    if(hRest<=0) {
                        println("$ans")
                        return
                    }
                }
            }
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
