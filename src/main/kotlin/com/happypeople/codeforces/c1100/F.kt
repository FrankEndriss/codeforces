package com.happypeople.codeforces.c1100

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        F().run()
    } catch (e: Throwable) {
        println("")
    }
}

/* One needs the "Gaussian Elemenation" with this.
see
https://math.stackexchange.com/questions/48682/maximization-with-xor-operator
https://stackoverflow.com/questions/27470592/maximum-xor-among-all-subsets-of-an-array
 */
class F {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val c=(1..n).map { sc.nextInt() }
        val q=sc.nextInt()
        for(i in 1..q) {
            val li=sc.nextInt()-1
            val ri=sc.nextInt()-1

            var costs=0L
            // Note: simply sorting is not enough, one needs the gaussian elimination
            val sublist=(li..ri).toMutableList().sortedBy { -c[it] }


            log("$li $ri")
            for(i in li..ri) {
                val costsAtI=c[sublist[i-li]]
                val ncosts=costs xor c[sublist[i-li]].toLong()
                if(ncosts>costs)
                    costs=ncosts
                log("ncosts: $ncosts ${Integer.toBinaryString(ncosts.toInt())} costs: $costs costsAtI: $costsAtI ${Integer.toBinaryString(costsAtI)}")
            }
            println("$costs")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}