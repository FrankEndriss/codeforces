package com.happypeople.codeforces.c1100

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        println("")
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val k=sc.nextInt()
        val tabs=(1..n).map { sc.nextInt() }

        val sums=IntArray(k)
        for(i in tabs.indices)
            sums[i%k]+=tabs[i]

        val sumAll=sums.sum()
        val ans=max(
                abs(sumAll - sums.max()!!),
                abs(sumAll - sums.min()!!)
        )
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}