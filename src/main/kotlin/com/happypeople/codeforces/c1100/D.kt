package com.happypeople.codeforces.c1100

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D().run()
    } catch (e: Throwable) {
        println("")
    }
}

class D {
    val rooksX=IntArray(666)
    val rooksY=IntArray(666)
    fun run() {
        val sc = Scanner(systemIn())
        var kX=sc.nextInt()
        var kY=sc.nextInt()

        for(i in 0..666-1) {
            rooksX[i]=sc.nextInt()
            rooksY[i]=sc.nextInt()
        }

        val CENTER=1
        var move=1
        var target=CENTER
        while(move<2000) {
            if(target==CENTER) {
                val dX=if(kX>999/2) -1 else if(kX<999/2) 1 else 0
                val dY=if(kY>999/2) -1 else if(kY<999/2) 1 else 0
                kX+=dX
                kY+=dY
                if(isRook(kX, kY)) {

                }
                println("$kX $kY")
            }
        }
    }
    fun isRook(x:Int, y:Int):Boolean {
        for(i in rooksX.indices) {
            if(rooksX[i]==x && rooksY[i]==y)
                return true
        }
        return false
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}