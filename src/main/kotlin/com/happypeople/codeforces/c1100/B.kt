package com.happypeople.codeforces.c1100

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        println("")
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val numLevels=sc.nextInt()
        val numProbs=sc.nextInt()

        var levelsAvail=0
        var probsLevels=IntArray(numLevels)

        for(i in 1..numProbs) {
            val level=sc.nextInt()-1
            if(probsLevels[level]==0) {
                levelsAvail++
            }
            probsLevels[level]++
            if(levelsAvail==numLevels) {
                print("1")
                levelsAvail=0
                for(j in probsLevels.indices) {
                    probsLevels[j]--
                    if(probsLevels[j]>0)
                        levelsAvail++
                }
            } else
                print("0")
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}