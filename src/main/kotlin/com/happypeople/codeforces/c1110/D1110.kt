package com.happypeople.codeforces.c1110

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        D1110().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1110 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()
        val tiles=IntArray(m)
        for(i in 1..n)
            tiles[sc.nextInt()-1]++

        for(i in tiles.indices) {
            if(tiles[i]>=3) {
                val mi=min3(tiles[i], tiles[i+1], tiles[i+2])
            }
        }


    }

    fun min3(i1:Int, i2:Int, i3:Int):Int {
        return min(min(i1, i2), i3)
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
