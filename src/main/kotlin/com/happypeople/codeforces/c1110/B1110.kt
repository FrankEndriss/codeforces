package com.happypeople.codeforces.c1110

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1110().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1110 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()  // num broken segments
        val m=sc.nextInt()  // num segments
        val k=sc.nextInt()  // max number of tape pieces
        val b=(1..n).map { sc.nextInt() }

        if(n<=k) { // piece per broken
            var ans=k*2
            for(i in 0..b.size-2)
                if(b[i]==b[i+1]-1)
                    ans--

            println("$ans")
        } else {
            // list of len to next segement sorted by lengths
            var dIdx=mutableListOf<Int>()
            for(i in dIdx.indices)
                dIdx.add(i)
            dIdx=dIdx.sortedBy { b[it+1]-b[it] }.toMutableList()
            val coveredSegments=mutableSetOf<Int>()

            // first cover consecutive broken segments
            val c=IntArray(b.size)
            var count=0
            for(i in c.indices) {
                c[i]=count
                if((b[i]==b[i-count]-count))
                    count++
                else
                    count=0
            }

            var cIdx=mutableListOf<Int>()
            for(i in cIdx.indices)
                cIdx.add(i)
            dIdx=dIdx.sortedByDescending { b[it] }.toMutableList()

            // TODO

        }



    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
