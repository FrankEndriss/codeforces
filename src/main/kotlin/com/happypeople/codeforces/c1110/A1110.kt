package com.happypeople.codeforces.c1110

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1110().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1110 {
    fun run() {
        val sc = Scanner(systemIn())
        val b=sc.nextInt()
        val k=sc.nextInt()
        val a=(1..k).map { sc.nextInt() }

        var countOdd=0
        for(i in 0..a.size-2)
            if(b%2!=0 && (a[i]%2)!=0)
                countOdd++
        if(a[a.size-1]%2==1)
            countOdd++

        if(countOdd%2==1)
            println("odd")
        else
            println("even")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
