package com.happypeople.codeforces.c1110

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1110().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1110 {
    fun run() {
        val sc = Scanner(systemIn())
        val q=sc.nextInt()
        for(i in 1..q) {
            val a=sc.nextInt()
            // find b so that (a and b) == 0
            var j=0
            val unsetBits=mutableListOf<Int>()
            while((1 shl j) <=a) {
                if (a and (1 shl j) == 0)
                    unsetBits.add(j)
                j++
            }
            //log("a=$a unsetBits=$unsetBits")

            // find the number build by only bits
            // which xors with a to the biggest possible number
            // So, use all bits which are not in a
            var b=0
            if(unsetBits.size>0) {
                for(j in unsetBits.indices)
                    b+=(1 shl unsetBits[j])

                //log("a=$a ans=$ans")
                // ans=(ans xor a)
            } else {
                b=2
            }
            //log("a=$a b=$b")
            val ans=Gcd.gcd((a xor b).toLong(), (a and b).toLong())
            println("$ans")
        }
    }

    class Gcd {
        companion object {

            fun lcm(a: Long, b: Long): Long {
                return a * (b / gcd(a, b))
            }

            fun lcm(input: List<Long>): Long {
                var result = input[0]
                for (i in input.indices)
                    result = lcm(result, input[i])
                return result
            }

            fun gcd(pA: Long, pB: Long): Long {
                var b = pB
                var a = pA
                while (b > 0) {
                    val temp = b
                    b = a % b
                    a = temp
                }
                return a
            }

            fun gcd(input: List<Long>): Long {
                var result = input[0]
                for (i in input.indices)
                    result = gcd(result, input[i])
                return result
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
