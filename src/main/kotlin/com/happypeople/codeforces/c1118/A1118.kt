package com.happypeople.codeforces.c1118

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1118().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1118 {
    fun run() {
        val sc = Scanner(systemIn())
        val q=sc.nextInt()
        for (i in (1..q)) {
            val n=sc.nextLong()
            val a=sc.nextLong()
            val b=sc.nextLong()
            val bIsCheaper=a*2>b;

            if(!bIsCheaper) {
                val aa=a*n
                println("$aa")
            } else {
                var bb=b*(n/2)
                if(n%2!=0L)
                    bb+=a
                println("$bb")
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
