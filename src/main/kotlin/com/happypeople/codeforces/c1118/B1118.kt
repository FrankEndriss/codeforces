package com.happypeople.codeforces.c1118

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1118().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1118 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }
        val oddPre=a.filterIndexed { index, i ->  index%2==1 }.toMutableList()
        for(i in oddPre.indices) {
            if(i>0)
                oddPre[i]+=oddPre[i-1]
        }
        val evePre=a.filterIndexed { index, i ->  index%2==0 }.toMutableList()
        for(i in evePre.indices) {
            if(i>0)
                evePre[i]+=evePre[i-1]
        }

        var ans=0
        for(i in a.indices) {
            val removedIdx=i/2
            if(i%2==0) { // even
                var leftEve=if(removedIdx>0) evePre[removedIdx-1] else 0
                val righEve=evePre[evePre.size-1] - evePre[removedIdx]

                val leftOdd=if(removedIdx>0) oddPre[removedIdx-1] else 0
                val righOdd=oddPre[oddPre.size-1] - if(removedIdx>0) oddPre[removedIdx-1] else 0

                log("i=$i leftEve=$leftEve righEve=$righEve leftOdd=$leftOdd righOdd=$righOdd")
                if(leftEve+righOdd==righEve+leftOdd)
                    ans++
            } else { // odd
                var leftEve=evePre[removedIdx]
                val righEve=evePre[evePre.size-1] - evePre[removedIdx]

                val leftOdd=oddPre[removedIdx]
                val righOdd=oddPre[oddPre.size-1] - oddPre[removedIdx]
                log("i=$i leftEve=$leftEve righEve=$righEve leftOdd=$leftOdd righOdd=$righOdd")
                if(leftEve+righOdd==righEve+leftOdd)
                    ans++
            }
        }
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
