package com.happypeople.codeforces.c1118

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1118().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
    val a = arrayOfNulls<E>(size)
    for (i in a.indices)
        a[i] = init(i)
    return a as Array<E>
}

class C1118 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val a = (1..n * n).map { sc.nextInt() }

        val m = (n + 1) / 2
        val freqs = IntArray(1000)
        for(i in a.indices)
            freqs[a[i]-1]++

        var pic=IntArray(m*m)
        var pic4Idx=0
        var pic2Idx=0

        if(n%2==0) {    // even
            if(freqs.filter { it%4!=0 }.count()>0) {
                println("NO")
                return
            }

        } else { // odd
            log("${freqs.toList()}")
            var count2=n*2-2
            var count1=1
            var count4=n*n-count2-count1

            for(i in freqs.indices) {
                while(freqs[i]>0) {
                    log("i=$i freqs[i]=${freqs[i]} count1=$count1 count2=$count2 count4=$count4")
                    if (freqs[i] % 4 == 0 && count4>0) {
                        count4-=4
                        freqs[i] -= 4
                        while(pic4Idx/m==m-1 || pic4Idx%m==m-1)
                            pic4Idx++
                        pic[pic4Idx]=i+1
                        pic4Idx++
                    } else if (freqs[i] % 2 == 0 && count2>0) {
                        count2 -=2
                        freqs[i] -=2
                        if(pic2Idx<n)
                            pic[n/2*n+pic2Idx]=i+1
                        else
                            pic[n/2*pic2Idx+n]=i+1
                        pic2Idx++
                    } else {
                        count1--
                        freqs[i] --
                        pic[(n/2)*n + (n/2)]=i+1
                    }
                }
            }
            if(count4!=0 || count2!=0 || count1!=0) {
                println("NO")
                return
            } else {
                for(i in 0..n-1) {
                    for (j in 0..n - 1) {
                        print("${pic[j*n+i]} ")
                    }
                    println()
                }
            }
        }

        // val xUsed = IntArray(m * m)
        // val xPic = IntArray(m * m)

        /*
        var xFree = 0
        val mIsOdd=m%2==1
        for (i in a.indices) {
            val idx = xVals[a[i]]
            val middleCol=mIsOdd && (idx%m)==m/2+1
            val middleRow=mIsOdd && (idx/m)==m/2+1
            val middleOne=middleCol || middleRow
            val middleBoth=middleCol && middleRow
            if (idx < 0 || xUsed[idx] == 4 || (middleOne && xUsed[idx]==2) || middleBoth) {
                log("xFree=$xFree idx=$idx")
                xVals[a[i]] = xFree
                xFree++
                if (xFree > (m * m + 1)) {
                    println("NO")
                    return
                }
                xUsed[xVals[a[i]]] = 1
                xPic[xVals[a[i]]] = a[i]
            } else {
                xUsed[idx]++
                log("xUsed[idx]=${xUsed[idx]}")
            }
        }

        println("YES")
        val k=n/2
        log("k=$k")
        for (i in (0..m - 1)) {
            for (j in (0..m - 1)) {
                print("${xPic[j * m + i]}")
            }
            for (j in k-1 downTo 0) {
                print("${xPic[j * k + i]}")
            }
            println()
        }
        for (i in k-1 downTo 0) {
            for (j in (0..m - 1)) {
                print("${xPic[j * m + i]}")
            }
            for (j in k-1 downTo 0) {
                print("${xPic[j * k + i]}")
            }
            println()
        }
        */
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
