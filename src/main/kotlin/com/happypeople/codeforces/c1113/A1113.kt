package com.happypeople.codeforces.c1113

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        A1113().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1113 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val v=sc.nextInt()

        var ans=minFuel(n, v)
        println("$ans")
    }

    fun minFuel(n:Int, v:Int):Int {
        var ans=min(v, n-1)
        var x=2
        while(x<=n-v) {
            ans += x
            x++ // tank one liter in every city
        }
        return ans
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
