package com.happypeople.codeforces.c1113

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1113().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1113 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }.sortedDescending()
        // find the biggest x, so that a big number is reduced by a[i]-(a[i]/x)
        // and the smalles number is increased by a[min]*(x-1)
        val ma=a.max()
        val amin=a[a.size-1]

        val totalPower=a.sum()
        var ans=totalPower    // choose x=1 to not change anything
        for(i in a.indices) {
            for(k in 2..50) {
                if(a[i]%k==0)
                    ans=min(ans, totalPower+(a[a.size-1]*(k-1))-(a[i]-(a[i]/k)))
            }
        }
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
