package com.happypeople.codeforces.c1113

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1113().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1113 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val a = (1..n).map { sc.nextInt() }
        val psum = IntArray(n + 1)
        psum[0] = 0
        for (i in a.indices) {
            psum[i + 1] = psum[i] xor a[i]
        }

        val fset=mutableMapOf<Int, MutableList<Int>>()
        for(i in 1..psum.size-1) {
            val list=fset.getOrPut(psum[i]) { mutableListOf<Int>() }
            list.add(i)
        }

        log("psum=${psum.toList()}")
        var ans = 0
        fset.filter { it.value.size>1 }.forEach {
            val list=it.value
            log("list.size=${list.size} list=$list")
            for(r in 1..list.size-1) {
                for(l in 1..r)
                    if((list[r]-list[l])%2==0)
                        ans++
            }
        }
        println("$ans")




        /*
        var ans = 0
        for (r in 2..a.size) {
            var l =
                    if (r % 2 == 0)
                        1
                    else
                        2
            var mid = (l + r - 1) / 2

            while (l <= r - 1) {
                // log("checking l=$l r=$r")
                if ((psum[mid] xor psum[l - 1]) == (psum[mid] xor psum[r])) {
                    ans++
                    //log("funny: l=$l r=$r mid=$mid")
                }
                l += 2
                mid++
            }
        }
        */
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
