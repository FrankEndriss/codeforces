package com.happypeople.codeforces.c1113

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1113().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1113 {
    fun run() {
        val sc = Scanner(systemIn())
        val s=sc.next().trim()

        val ans=minK(s)
        if(ans==-1)
            println("Impossible")
        else
            println("$ans")

    }

    fun minK(s:String):Int {
        if(s.length%2==1) { // odd
            val l=s.substring(0, s.length/2)
            val r=s.substring(s.length/2+1)
            if(l==r)
                return -1
            else
                return 2
        } else { // s.length is even
            val l=s.substring(0, s.length/2)
            val r=s.substring(s.length/2)
            if(l!=r)
                return 1
            else if(isPalin(l))
                return minK(l)
            else
                return -1
        }
    }

    fun isPalin(s:String):Boolean {
        var l=0
        var r=s.length-1
        while(l<=r) {
            if(s[l]!=s[r])
                return false
            l++
            r--
        }
        return true
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
