package com.happypeople.codeforces.c1121

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        B1121().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1121 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val a = (1..n).map { sc.nextInt() }.sorted().toIntArray()
        val sums = IntArray(200001)

        for (i in 0..n - 2) {
            for (j in i + 1..n - 1) {
                sums[a[i]+a[j]]++
            }
        }

        val ans=sums.max()!!
        println("$ans")
    }

    fun countpairs(a:IntArray, s:Int):Int {
        val half=s/2
        var count=0
        for(i in 0..a.size-2) {
            if(a[i]>half)
                return count
            val idx=a.binarySearch(s-a[i], i)
            if(idx>i)
                count++
        }
        return count
    }

    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }

    class Multiset<E> {
        private val map = mutableMapOf<E, Int>()

        public fun add(e: E) {
            val c = map.getOrDefault(e, 0)
            map[e] = c + 1
        }

        public fun remove(e: E) {
            val c = map.getOrDefault(e, 0)
            if (c > 1)
                map[e] = c - 1
            else if (c == 1)
                map.remove(e)
        }

        public fun contains(e: E) =
                map.contains(e)

        public fun count(e: E) =
                map.getOrDefault(e, 0)

        public fun toList(): List<E> {
            return map.flatMap { ent -> (1..ent.value).map { ent.key } }
        }

        public fun toSet(): Set<E> = map.keys
    }
    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
