package com.happypeople.codeforces.c1121

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1121().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1121 {
    fun run() {
        val sc = Scanner(systemIn())
        val students=sc.nextInt()
        val schools=sc.nextInt()
        val choosen=sc.nextInt()
        val p=(1..students).map { sc.nextInt() }    // student powers
        val s=(1..students).map { sc.nextInt() }    // school numbers
        val c=(1..choosen).map { sc.nextInt() }     // IDs of choosen ones


    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
