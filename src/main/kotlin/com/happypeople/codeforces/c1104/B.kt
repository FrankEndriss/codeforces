package com.happypeople.codeforces.c1104

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        println("")
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val s = sc.next().trim().map { it }.toList()
        val ll1 = LinkedList<Char>()
        val ll2 = LinkedList<Char>()
        for (c in s) {
            ll1.push(c)
        }

        // Remove at most possible, count
        var count = 0
        while (!ll1.isEmpty()) {
            if (ll2.peek() == ll1.peek()) {
                ll1.pop()
                ll2.pop()
                count++
            } else {
                ll2.push(ll1.pop())
            }
        }
        if (count % 2 == 1)
            println("Yes")
        else
            println("No")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}