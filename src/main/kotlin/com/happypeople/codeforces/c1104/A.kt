package com.happypeople.codeforces.c1104

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        println("")
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()

        println(n)
        (1..n).forEach {
            print("1 ")
        }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}