package com.happypeople.codeforces.c1104

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        println("")
    }
}

class C {
    fun out(r: Int, c: Int) {
        println("${r + 1} ${c + 1}")
    }

    fun run() {
        val sc = Scanner(systemIn())
        val s = sc.next().trim()

        // map of free cells
        val map = listOf(
                mutableListOf(true, true, true, true),
                mutableListOf(true, true, true, true),
                mutableListOf(true, true, true, true),
                mutableListOf(true, true, true, true)
        )

        s.map { it == '1' }.forEach {
            val horz = it
            doMap(horz, map)
            for (i in 0..3) {
                if (!map[i][0] && !map[i][1] && !map[i][2] && !map[i][3]) {
                    map[i][0] = true
                    map[i][1] = true
                    map[i][2] = true
                    map[i][3] = true
                } else if (!map[0][i] && !map[1][i] && !map[2][i] && !map[3][i]) {
                    map[0][i] = true
                    map[1][i] = true
                    map[2][i] = true
                    map[3][i] = true
                }
            }
        }
    }

    private fun doMap(horz: Boolean, map: List<MutableList<Boolean>>) {
        if (horz) {
            for (i in 0..3) {
                if (map[i][0] && map[i][1]) {
                    map[i][0] = false
                    map[i][1] = false
                    out(i, 0)
                    return
                } else if (map[i][2] && map[i][3]) {
                    map[i][2] = false
                    map[i][3] = false
                    out(i, 2)
                    return
                } else if (map[i][1] && map[i][2]) {
                    map[i][1] = false
                    map[i][2] = false
                    out(i, 1)
                    return
                }
            }
        } else {
            for (i in 0..3) {
                if (map[0][i] && map[1][i]) {
                    map[0][i] = false
                    map[1][i] = false
                    out(0, i)
                    return
                } else if (map[2][i] && map[3][i]) {
                    map[2][i] = false
                    map[3][i] = false
                    out(2, i)
                    return
                } else if (map[1][i] && map[2][i]) {
                    map[1][i] = false
                    map[2][i] = false
                    out(1, i)
                    return
                }
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}