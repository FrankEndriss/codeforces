package com.happypeople.codeforces.c1087;

import java.util.*;

public class C {
    static class P {
        int x;
        int y;
        P(int x, int y) {
            this.x=x;
            this.y=y;
        }

        int manh(P other) {
            return Math.abs(x-other.x) + Math.abs(y-other.y);
        }
    }

    static String ex1="0 0\n" +
            "2 0\n" +
            "1 1\n";

    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner(ex1);
        P[] p={ new P(s.nextInt(), s.nextInt()),
            new P(s.nextInt(), s.nextInt()),
            new P(s.nextInt(), s.nextInt()) };

        P root=new P(0, 0);

        Arrays.sort(p, new Comparator<P>() {
            @Override
            public int compare(P o1, P o2) {
                return o1.manh(root)-o2.manh(root);
            }
        });

        List<P> res=new ArrayList<P>();

        {
            int incX = p[0].x < p[1].x ? 1 : -1;
            for (int x = p[0].x; x != p[1].x; x += incX) {
                if (x != p[1].x && x!=p[0].x)
                    res.add(new P(x, p[0].y));
            }
        }

        {
            int incY = p[0].y < p[1].y ? 1 : -1;
            for (int y = p[0].y; y != p[1].y; y += incY) {
                if (y != p[1].y && y!=p[0].y)
                    res.add(new P(p[1].x, y));
            }
        }

        {
            int incX = p[1].x < p[2].x ? 1 : -1;
            for (int x = p[1].x; x != p[2].x; x += incX) {
                if (x != p[2].x && x!=p[1].x)
                    res.add(new P(x, p[1].y));
            }
        }

        {
            int incY = p[1].y < p[2].y ? 1 : -1;
            for (int y = p[1].y; y != p[2].y; y += incY) {
                if (y != p[2].y && y!=p[1].y)
                    res.add(new P(p[2].x, y));
            }
        }

        System.out.println(""+res.size());
        for(P p1 : res) {
            System.out.println(""+p1.x+" "+p1.y);
        }
    }
}
