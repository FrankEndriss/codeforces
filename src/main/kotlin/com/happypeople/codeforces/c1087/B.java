package com.happypeople.codeforces.c1087;

import java.util.Scanner;

public class B {
    static String ex1="999983 1000";
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        //Scanner s = new Scanner(ex1);
        int n = s.nextInt();
        int k = s.nextInt();

        int x=1;
        while(!fit(n, k, x) && x<=(n*10))
            x++;
        if(x>n*10)
            x=n*k+1;

        System.out.println(""+x);
    }

    static boolean fit(int n, int k, int x) {
        return n==(x/k) * (x%k);
    }
}
