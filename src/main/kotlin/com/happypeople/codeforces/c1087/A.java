package com.happypeople.codeforces.c1087;

import java.util.Scanner;

public class A {
    //static String ex1="ncteho\n";
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String str = s.nextLine();

        String res = "";

        while(str.length()>0) {
            if(str.length()%2==1) {
                res=str.charAt(0)+res;
                str=str.substring(1);
            } else {
                res=str.charAt(str.length()-1)+res;
                str=str.substring(0, str.length()-1);
            }
        }
        System.out.println(res);

    }
}
