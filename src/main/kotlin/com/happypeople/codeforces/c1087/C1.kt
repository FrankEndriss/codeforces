package com.happypeople.codeforces.c1087

import java.util.*
import kotlin.math.abs

fun main(args: Array<String>) {
    C1().run()
}

class C1() {
    val ex6 =
            "0 0\n" +
                    "1 2\n" +
                    "2 1\n"
    val ex7 = "0 0\n" +
            "5 0\n" +
            "0 6\n"

    data class P(val x: Int, val y: Int) {
        fun manh(p: P): Int = abs(x - p.x) + abs(y - p.y)
        override fun toString() = "P " + x + ":" + y
        fun steps(): Set<P> {
            return setOf(
                    P(x - 1, y),
                    P(x + 1, y),
                    P(x, y - 1),
                    P(x, y + 1)
            )
        }
    }

    fun sumManh(p: P, list: List<P>): Int {
        return list.map {
            p.manh(it)
        }.sum()
    }

    fun minManh(p: P, list: List<P>): Int {
        return list.map {
            p.manh(it)
        }.min()!!
    }

    fun run() {
        val s = Scanner(System.`in`)
        // val s = Scanner(ex7)
        val p = mutableListOf(P(s.nextInt(), s.nextInt()), P(s.nextInt(), s.nextInt()), P(s.nextInt(), s.nextInt()))
        val targets = p.toMutableList()

        // start at p0
        // opt next step by min(sum(manh(s, pN))) tie min(manh(s, pN))
        // then find nearest
        // and repeat for nearest and (p1 or p2), wichever still not reached

        val res = mutableSetOf<P>()

        var currentPoint = p[0]
        targets.remove(p[0])
        res.add(currentPoint)

        // println("starting at: $currentPoint")
        while (!targets.contains(currentPoint)) {
            currentPoint = currentPoint.steps()
                    .sortedWith(compareBy({ sumManh(it, targets) }, { minManh(it, targets) }))
                    .first()
            // println("next: $currentPoint")
            res.add(currentPoint)
        }
        targets.removeAll(res)

        val target = targets.first() // the only one left
        val nearest = res.sortedBy { it.manh(target) }.first()

        // println("nearest then: $nearest")
        currentPoint = nearest
        res.add(nearest)
        while (currentPoint != target) {
            res.add(currentPoint)
            currentPoint = currentPoint.steps()
                    .sortedWith(compareBy({ it.manh(target) })).first()
            // println("next: $currentPoint")
            res.add(currentPoint)
        }

        println("${res.size}")
        res.sortedWith(compareBy({ it.x }, { it.y }))
                .forEach {
                    println("${it.x} ${it.y}")
                }
    }

    // step the bigger diff first
    fun nextTo(p1: P, p2: P): P {
        if (abs(p1.x - p2.x) > abs(p1.y - p2.y)) {
            return P(p1.x + (if (p1.x < p2.x) 1 else -1), p1.y)
        } else {
            return P(p1.x, p1.y + (if (p1.y < p2.y) 1 else -1))
        }
    }

}
