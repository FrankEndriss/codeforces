package com.happypeople.codeforces.c1087

import java.util.*


fun main(args: Array<String>) {
    E().run()
}

class E {
val ex = "4\n" +
        "4\n" +
        "bbcb\n" +
        "aada\n" +
        "aada\n" +
        "3\n" +
        "abc\n" +
        "bbb\n" +
        "bbb \n" +
        "5\n" +
        "dbeedcbcbaccdddbdcbcabacecbe\n" +
        "dcaeaabaadacddcdecbbebcbadcb\n" +
        "ddbacaaabdaeedaacbabebccebbd\n" +
        "26\n" +
        "b\n" +
        "a\n" +
        "a\n"

    fun run() {
        val sc = Scanner(System.`in`)
        // val sc = Scanner(ex)
        val t = sc.nextInt()
        for (i in 1..t) {
            val k = sc.nextInt()
            val s = sc.next()
            val a = sc.next()
            val b = sc.next()

            // println("$k $s $a $b")

            val template = mutableListOf<Char>()
            for (j in 1..k)
                template.add('a' - 1 + j) // [a, b, c,...]

            val perm1 = (0..(k - 1)).toMutableList()

            // map permutation to substitution map
            val perms = permute(perm1, 0);
            // println("${perms.toList()}")

            val matches = mutableSetOf<String>()
            for (perm in perms) {
                val subst = String(s.map { template[perm[it - 'a']] }.toCharArray())
                // println("${perm} $s -> $subst")
                if (subst.compareTo(a) >= 0 && subst.compareTo(b) <= 0) {
                    val res = String(template.map { template[perm[it - 'a']] }.toCharArray())
                    matches.add(res)
                    if (matches.size == 1) {
                        println("YES")
                        println(res)
                        break
                    }
                }
            }
            if (matches.size == 0)
                println("NO")
        }
    }

    fun <T> permute(list: List<T>, k: Int): Sequence<List<T>> {
        if (k == list.size - 1) {
            return sequence { yield(list.toMutableList()) }
        }
        return sequence {
            val ret = mutableListOf<Sequence<List<T>>>()
            // zip all Seq in ret
            var i = 0
            for (i in k..list.size - 1) {
                val mlist = list.toMutableList()
                swap(mlist, i, k)
                ret.add(permute(mlist.toList(), k + 1))
                swap(mlist, k, i)
            }

            val iret = ret.map { it.iterator() }

            var fini = false
            while (!fini) {
                fini = true
                for (s in iret) {
                    if (s.hasNext()) {
                        yield(s.next())
                        fini = false
                    }
                }
            }
        }
    }

    fun <T> swap(list: MutableList<T>, i: Int, k: Int) {
        val t = list[i]
        list[i] = list[k]
        list[k] = t
    }
}
