package com.happypeople.codeforces.c1087

import java.util.*

fun main(args: Array<String>) {
    D().run()
}

data class Node(val id: Int) {
    var childs = mutableSetOf<Node>()
    var weight: Double = 1.0
}

val nodes = mutableMapOf<Int, Node>()

class D() {
    val ex1 = "4 3\n" +
            "1 2\n" +
            "1 3\n" +
            "1 4\n"

    val ex2 = "6 1\n" +
            "2 1\n" +
            "2 3\n" +
            "2 5\n" +
            "5 4\n" +
            "5 6\n"

    fun run() {
        // val sc = Scanner(System.`in`)
        val sc = Scanner(ex2)
        val n = sc.nextInt()
        val s = sc.nextInt()

        while (sc.hasNextInt()) {
            val id = sc.nextInt()
            val chi = sc.nextInt()
            val parent = nodes.getOrElse(id) { Node(id) }
            nodes[id] = parent
            val child = nodes.getOrElse(chi) { Node(chi) }
            nodes[chi] = child
            parent.childs.add(child)
            child.childs.add(parent)
            println("$id $chi")
        }


        val roots = findRoots()
        println("roots: $roots")

        if (roots.size == 1)
            dealW(roots[0], s.toDouble())
    }

    fun dealW(n: Node, s: Double) {
        // TODO...

    }

    // basically the root(s) are that nodes with the shortest path to any other node
    // so sort all nodes by longest path to any other node
    // first is then root
    fun findRoots(): List<Node> {
        val list = nodes.values.map { node ->
            nodes.map {
                Pair(cheapestPath(it.value, node), node)
            }.sortedBy {
                -it.first
            }.first()
        }

        val rootW = list.first().first           // the one with the shotest longest path
        return list.filter { it.first == rootW }.map { it.second }
    }

    fun cheapestPath(from: Node, to: Node, pathSoFar: List<Node> = listOf<Node>()): Double {
        if (from.id == to.id)
            return 0.0

        val toExamine = from.childs.minus(pathSoFar)
        if (toExamine.isEmpty()) // dead end
            return 1000000.0

        return from.childs.minus(pathSoFar).map {
            cheapestPath(it, to, pathSoFar.plus(it))
        }.min()!! + from.weight
    }
}