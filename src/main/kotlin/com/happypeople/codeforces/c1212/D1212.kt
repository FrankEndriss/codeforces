package com.happypeople.codeforces.c1212

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1212().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1212 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextLong() }.toSet()

        /* find the one element with no input */
        val start=a.filter { (!a.contains(it*3)) && ((it%2==1L) || !a.contains(it/2)) }
        //println("start= $start")
        assert(start.size==1)
        var next=start.first()
        while(true) {
            print("$next ")
            if(a.contains(next*2))
                next=next*2
            else if(next%3==0L && a.contains(next/3))
                next=next/3
            else
                break
        }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
