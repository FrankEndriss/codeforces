package com.happypeople.codeforces.c1212

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1212().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1212 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val k=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }.sorted()
        if(k==0) {
            if(a[0]==1)
                println(-1)
            else
                println(a[0]-1)
        } else {
            if (a.size < k)
                println(-1)
            else if (a.size > k && a[k] == a[k - 1])
                println(-1)
            else
                println(a[k - 1]);
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
