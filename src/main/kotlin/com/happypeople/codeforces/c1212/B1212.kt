package com.happypeople.codeforces.c1212

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1212().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1212 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val str=sc.next()
        val dmap = mutableMapOf<String, Int>()
        for(i in 0..str.length-2) {
            val twogram=str.substring(i..i+1)
            val old=dmap.getOrDefault(twogram, 0);
            dmap.set(twogram, old+1);
        }
        println(dmap.toList().sortedByDescending { (_, value) -> value }.first().first)
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
