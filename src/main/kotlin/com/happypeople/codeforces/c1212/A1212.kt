package com.happypeople.codeforces.c1212

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1212().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1212 {
    fun run() {
        val sc = Scanner(systemIn())
        var n=sc.nextInt()
        val k=sc.nextInt()

        for(i in 1..k) {
            if(n%10>0)
                n--;
            else
                n/=10;
        }

        println(n)
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
