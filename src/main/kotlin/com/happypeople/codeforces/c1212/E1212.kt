//package com.happypeople.codeforces.c1212

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        E1212().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class E1212 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val cp=(1..n).map { Triple(sc.nextInt(), sc.nextInt(), it)  }
                .sortedWith(compareBy({ -it.second }, { it.first }))

        val k=sc.nextInt()
        val r=(1..k).map { Pair(sc.nextInt(), it) }.sortedBy{it.first}
        val rUsed=(1..k).map { false }.toMutableList()

        var ansM=0
        var ansS=0L
        val ansL=mutableListOf<Pair<Int, Int>>()
        cp.forEach {
            /* Find if an unused table of min size it.first.first and mark it used. */

            /* simply brute force instead of binsearch */
            for(i in r.indices) {
                if(r[i].first>=it.first && !rUsed[i]) {
                    ansL.add(Pair(it.third, r[i].second))
                    ansM++
                    ansS+=it.second
                    rUsed[i]=true
                    break
                }
            }
        }
        println("$ansM $ansS")
        ansL.forEach {
            println("${it.first} ${it.second}")
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
