package com.happypeople.codeforces.c1108

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        E1().run()
    } catch (e: Throwable) {
        println("")
    }
}


// for solution see https://codeforces.com/blog/entry/64751
class E1 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()

        val a=LongArray(n+1)
        for(i in 1..n) { a[i]=sc.nextLong() }
        val l=IntArray(m+1)
        val r=IntArray(m+1)
        for(j in 1..m) {
            l[j] = sc.nextInt()
            r[j] = sc.nextInt()
        }

        var maxdiff=0L
        var maxindexes=mutableListOf<Int>()
        for(i in 1..n) {
            a[0]=a[i]
            // assume one a after the other being max value, bruteforce
            // apply all ranges not includnig a[i], find minimum
            val fenwick=Fenwick<Long>(n, 0) { i1, i2 -> i1+i2}
            val indexes=mutableListOf<Int>()
            for(j in 1..m) {
                if(l[j]>i || r[j]<i) {  // add range if j is not in range
                    //log("adding idx: $j l=${l[j]} r=${r[j]}")
                    fenwick.updateCumul(l[j], -1)
                    fenwick.updateCumul(r[j]+1, 1)
                    indexes.add(j)
                }
            }

            val minval=a.mapIndexed { index, l -> l+fenwick.readCumul(index) }.min()!!
            val diff=a[i]-minval
            //log("diff at idx=$i: $diff min:$minval max:${a[i]}")
            if(diff>maxdiff) {
                maxdiff=diff
                maxindexes=indexes
            }
        }

        println("$maxdiff")
        println("${maxindexes.size}")
        println("${maxindexes.joinToString(" ")}")
    }

    class Lsb { companion object { fun lsb(i: Int) = i and -i } }
    class Fenwick<E>(size: Int, val neutral: E, val cumul: (E, E) -> E) : FenwickIf<E> {
        val tree = (0..size).map { neutral }.toMutableList()

        override fun updateCumul(pIdx: Int, value: E) {
            if (pIdx < 1 || pIdx>tree.size)
                throw IllegalArgumentException("bad idx1, min=1, max=${tree.size}, you sent: $pIdx")
            var idx = pIdx
            while (idx < tree.size) {
                tree[idx] = cumul(tree[idx], value)
                idx += Lsb.lsb(idx)
            }
        }

        override fun readCumul(pIdx: Int): E {
            var idx = pIdx
            var sum = neutral
            while (idx > 0) {
                sum = cumul(sum, tree[idx])
                idx -= Lsb.lsb(idx)
            }
            return sum
        }
    }

    interface FenwickIf<E> {
        /** Updates the value at pIdx cumulative, i.e. "adds" value to the value stored at pIdx.
         * @param pIdx 1 based index into structure.
         * @param value the delta for the Update. */
        fun updateCumul(pIdx: Int, value: E)

        /** @return the cumul of range 0:pIdx inclusive. */
        fun readCumul(pIdx: Int): E

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}