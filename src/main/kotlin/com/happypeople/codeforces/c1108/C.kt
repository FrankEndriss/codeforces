package com.happypeople.codeforces.c1108

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        println("")
    }
}

class C {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()

        val pat1 = listOf('R', 'G', 'B')
        val pat2 = listOf('R', 'B', 'G')
        val s = sc.next().trim().toCharArray()
        val a1 = (1..3).map { 0 }.toMutableList()
        val a2 = (1..3).map { 0 }.toMutableList()

        for (i in s.indices) {
            val c=s[i]
            for (j in 0..2) {
                if (c != pat1[(i+j)%3])
                    a1[j]++
                if (c != pat2[(i+j)%3])
                    a2[j]++
            }
        }
        // log("${a1} ${a2}")

        val min = listOf(
                a1[0], a1[1], a1[2],
                a2[0], a2[1], a2[2]
        ).mapIndexed { idx, c -> Pair(idx, c) }
                .sortedBy { it.second }.first()

        println("${min.second}")
        val pat = if (min.first < 3) pat1 else pat2
        val idx = min.first % 3
        for (i in 0..s.size - 1) {
            val c = pat[(idx + i) % 3]
            print("$c")
        }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}