package com.happypeople.codeforces.c1108

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        println("")
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val mset=Multiset<Int>()
        for(i in 1..n) {
            mset.add(sc.nextInt())
        }
        val biggest=mset.toSet().max()!!

        var other=Int.MIN_VALUE
        val set=mset.toSet().toList()
        for(i in mset.toSet())
            if((biggest%i)!=0 || mset.count(i)>1)
                other=max(other, i)

        println("$other $biggest")
    }

    class Multiset<E> {
        private val map = mutableMapOf<E, Int>()

        public fun add(e: E) {
            val c = map.getOrDefault(e, 0)
            map[e] = c + 1
        }

        public fun remove(e: E) {
            val c = map.getOrDefault(e, 0)
            if (c > 1)
                map[e] = c - 1
            else if (c == 1)
                map.remove(e)
        }

        public fun contains(e: E) =
                map.contains(e)

        public fun count(e: E) =
                map.getOrDefault(e, 0)

        public fun toList(): List<E> {
            return map.flatMap { ent -> (1..ent.value).map { ent.key } }
        }

        public fun toSet(): Set<E> = map.keys
    }
    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}