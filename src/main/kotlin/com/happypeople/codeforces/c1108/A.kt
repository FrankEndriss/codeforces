package com.happypeople.codeforces.c1108

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        println("")
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val q=sc.nextInt()
        for(i in 1..q) {
            var r1=Pair(sc.nextInt(), sc.nextInt());
            var r2=Pair(sc.nextInt(), sc.nextInt());
            var changed=false
            if(r1.first>r2.first) {
                changed=true
                val tmp=r1;
                r1=r2
                r2=tmp
            }
            if(changed)
                print("${r2.first} ")
            else
                print("${r1.first} ")

            if(r1.first!=r2.first) {
                if(changed)
                    println("${r1.first}")
                else
                    println("${r2.first}")
            } else {
                if(changed)
                    println("${r1.second}")
                else
                    println("${r2.second}")
            }

        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}