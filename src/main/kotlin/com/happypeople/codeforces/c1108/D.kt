package com.happypeople.codeforces.c1108

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D().run()
    } catch (e: Throwable) {
        println("")
    }
}

class D {
    fun unused(c1:Char, c2:Char):Char {
        return listOf('R', 'G', 'B').minus(c1).minus(c2).first()
    }
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val s = sc.next().trim().toCharArray()

        var prev='-'
        var c=0
        for(i in s.indices) {
            if(s[i]==prev) {
                c++
                if(i<s.size-1)
                    s[i]=unused(prev, s[i+1])
                else
                    s[i]=unused(prev, prev)
            }
            prev=s[i]
        }

        println("$c")
        val str=String(s)
        println("$str")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}