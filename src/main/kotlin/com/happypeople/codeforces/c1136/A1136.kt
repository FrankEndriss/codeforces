package com.happypeople.codeforces.c1136

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1136().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1136 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val p=(1..n).map { sc.nextInt(); sc.nextInt(); }
        val x=sc.nextInt()

        val idx=p.binarySearch(x)
        val ans=
        if(idx>=0)
            n-idx
        else
            n+(idx+1)
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
