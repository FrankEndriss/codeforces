package com.happypeople.codeforces.c1136

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1136().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1136 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()

        val p=(1..n).map { sc.nextInt() }.toMutableList()
        val c=arrOf(300001) { mutableSetOf<Int>() }
        val cRev=arrOf(300001) { mutableSetOf<Int>() }
        for(i in 1..m) {
            val u=sc.nextInt()-1
            val v=sc.nextInt()-1
            c[u].add(v)
            cRev[v].add(u)
        }
        // NOTE if u may change pos with v it will do this
        // unlimited times if neccesary.

        val nastya=p[n-1]
        val naspos=n-1
        val stack=LinkedList<Set<Int>>()
        var ans=0
        while(naspos>0) {
        }
        println("$ans")

    }
    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }

    class Multiset<E> {
        private val map = mutableMapOf<E, Int>()

        public fun add(e: E) {
            val c = map.getOrDefault(e, 0)
            map[e] = c + 1
        }

        public fun remove(e: E) {
            val c = map.getOrDefault(e, 0)
            if (c > 1)
                map[e] = c - 1
            else if (c == 1)
                map.remove(e)
        }

        public fun contains(e: E) =
                map.contains(e)

        public fun count(e: E) =
                map.getOrDefault(e, 0)

        public fun toList(): List<E> {
            return map.flatMap { ent -> (1..ent.value).map { ent.key } }
        }

        public fun toSet(): Set<E> = map.keys
    }
    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
