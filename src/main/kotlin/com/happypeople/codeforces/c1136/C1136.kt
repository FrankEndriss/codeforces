package com.happypeople.codeforces.c1136

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        C1136().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1136 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()
        val A=mutableListOf<List<Int>>()
        val B=mutableListOf<List<Int>>()
        for (i in (1..n)) {
            A.add( (1..m).map { sc.nextInt() })
        }
        for (i in (1..n)) {
            B.add( (1..m).map { sc.nextInt() })
        }

        for(k in 0..m-1) {
            val listA=mutableListOf<Int>()
            val listB=mutableListOf<Int>()
            var r=0
            var c=k
            while(r<=n-1 && c>=0) {
                listA.add(A[r][c])
                listB.add(B[r][c])
                r++
                c--
            }
            listA.sort()
            listB.sort()
            //log("listA=$listA listB=$listB")
            if(listA!=listB) {
                println("NO")
                return
            }
        }
        for(k in 1..n-1) {
            val listA= mutableListOf<Int>()
            val listB= mutableListOf<Int>()
            var r=k
            var c=m-1
            while(r<n && c>=0) {
                listA.add(A[r][c])
                listB.add(B[r][c])
                r++
                c--
            }
            listA.sort()
            listB.sort()

            //log("listA=$listA listB=$listB")
            if(listA!=listB) {
                println("NO")
                return
            }
        }
        println("YES")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
