package com.happypeople.codeforces.c1136

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1136().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1136 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val k=sc.nextInt()

        val ans=
                if(n==2)
                    6
                else if(k==1 || k==n)
                    n*3
                else {
                    val m=min(k, n-k+1)
                    m * 2 + (m - 1) * 2 + 1 + (n - m) * 3
                }

        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
