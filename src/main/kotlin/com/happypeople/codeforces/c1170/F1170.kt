package com.happypeople.codeforces.c1170

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.lang.Math.abs
import java.lang.Math.min
import java.util.*

fun main(args: Array<String>) {
    try {
        F1170().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class F1170 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()
        val k=sc.nextInt()
        val a=(1..n).map { sc.nextLong() }.sorted()
        /* find m consecutives in a with min dist to median of them. ans =sum(dist)*/
        var sumL=0L;
        var sumR=0L;
        var med=m/2
        for(i in 0..med-1)
            sumL+=a[i]
        for(i in med+1..m-1)
            sumR+=a[i]

        //log("sumL=$sumL sumR=$sumR")
        var ans=a[med]*(med)-sumL + sumR-a[med]*(m-med-1)
        //log("ans[0]=$ans")
        for(i in 1..n-m) {
            med++
            sumL-=a[i-1]
            sumL+=a[med-1]
            sumR-=a[med]
            sumR+=a[i+m-1]
            ans=min(ans, a[med]*(m/2)-sumL + sumR-a[med]*(m-m/2-1))
            //log("sumL=$sumL sumR=$sumR ans[i]=$ans")
        }
        // what if ans>k ???
        if(ans>k)
            ans=k.toLong()

        println("$ans")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
