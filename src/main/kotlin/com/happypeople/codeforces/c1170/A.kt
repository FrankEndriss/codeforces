package com.happypeople.codeforces.c1170

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val q=sc.nextInt();

        for(i in 1..q) {
            val a=sc.nextInt();
            val b=sc.nextInt();
            val x1=max(a, b);
            val x2=min(a, b);
            println("${x1-x2+1} ${x2-1} 1")
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
