package com.happypeople.codeforces.c1170

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import java.lang.StringBuilder as StringBuilder1

fun main(args: Array<String>) {
    try {
        C1170().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1170 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        for(i in 1..n) {
            val s=sc.next()
            val t=sc.next()
            var x=StringBuilder()
            var sIdx=0
            var tIdx=0
            while(sIdx<s.length && tIdx<t.length) {
                if(s[sIdx]==t[tIdx]) {
                    x.append(s[sIdx])
                    sIdx++
                    tIdx++
                } else {
                    if(s[sIdx]=='-' && sIdx<s.length-1 && s[sIdx+1]=='-') {
                        x.append('+')
                        sIdx+=2
                        tIdx++
                    } else
                        break
                }
            }
            if(t==x.toString() && sIdx==s.length)
                println("YES")
            else
                println("NO")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
