package com.happypeople.codeforces.c1170

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        B1170().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1170 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        if (n == 1) {
            println("0")
            return
        }
        val a=(1..n).map { sc.nextInt() }
        var m1=max(a[0], a[1]);
        var m2=min(a[0], a[1]);
        var ans=0;
        for(i in (2..n-1)) {
            if(a[i]>m1) {
                    m2=m1;
                    m1=a[i];
            } else if(a[i]>m2) {
                m2 = a[i];
            } else if(a[i]<m2)
                ans++;
        }
        println("$ans")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
