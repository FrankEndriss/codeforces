package com.happypeople.codeforces.c1170

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1170().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class D1170 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt();
        val a=(1..n).map { sc.nextInt() }

        val s=a.filter { it==-1 }.count();
        //log("s=$s")

        val ans=Array(s) { mutableListOf<Int>() }
        var id=(0..s-1).map { it }.toMutableList()
        var next=(0..s-1).map { it }.toMutableList()

        val fini=Array(s) { false }
        var idx=0;
        while(idx<a.size) {
            var next= mutableListOf<Int>()
            for(row in id) {
                if(!fini[row]) {
                    if(a[idx]==-1)
                        fini[row] = true
                    else {
                        ans[row].add(a[idx])
                        next.add(row)
                    }
                    idx++
                }
            }
            id=next
        }

        println("$s")
        for(i in (0..s-1)) {
            print("${ans[i].size}")
            for(j in ans[i].indices) {
                print(" ${ans[i][j]}")
            }
            println()
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
