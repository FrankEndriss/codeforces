package com.happypeople.codeforces.c1117

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        A1117().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1117 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }
        val aMax=a.max()

        // log("aMax=$aMax")
        var count=0
        var maxCount=1
        for(i in a.indices) {
            //log("a[i]=${a[i]}")
            if(a[i]==aMax) {
                //log("count++")
                count++
                maxCount=max(maxCount, count)
            } else {
                count = 0
            }
        }
        println("$maxCount")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
