package com.happypeople.codeforces.c1117

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs

fun main(args: Array<String>) {
    try {
        C1117().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1117 {
    fun run() {
        val sc = Scanner(systemIn())
        val xS=sc.nextInt()
        val yS=sc.nextInt()
        val xD=sc.nextInt()
        val yD=sc.nextInt()
        val len=sc.nextInt()
        val s=sc.next().trim()

        val destination=Pair(xD, yD)
        val dist=manh(Pair(xS, yS), destination)   // distance start to destination
        //log("dist=$dist")

        val pos=mutableListOf(Pair(xS,yS))
        for(i in 1..s.length) {
            val x=if(s[i-1]=='L') -1 else if(s[i-1]=='R') 1 else 0
            val y=if(s[i-1]=='D') -1 else if(s[i-1]=='U') 1 else 0
            pos.add(Pair(pos[i-1].first+x, pos[i-1].second+y))
        }
        //log("pos=$pos")

        //val winddist=pos.map { manh(pos[0], it) }   // distance to destination in first round
        //log("winddist=$winddist")
        val mindistX=pos.mapIndexed { index, p ->  abs(p.first-xD)-index } // minimum distance X we can reach on day index
        val mindistY=pos.mapIndexed { index, p ->  abs(p.second-yD)-index } // minimum distance Y we can reach on day index
        //log("mindist=$mindist")

        // ... need to take into account that we have to find the longer of both distances X and Y...

        /*

        val maxTravelInOneRoundX=xD-mindistX[mindist.size-1]
        val maxTravelInOneRoundY=dist-mindist[mindist.size-1]
        //log("maxTravelOneRound=$maxTravelInOneRound")
        if(maxTravelInOneRound<1) {
            println("-1")
            return
        }

        if(maxTravelInOneRound>=dist) {
            // need to find first value in mindist equals 0
            for(i in mindist.indices) {
                if(mindist[i]<=0) {
                    println("$i")
                    return
                }
            }
        } else {
            val fullRounds=dist/maxTravelInOneRound
            val rest=dist%maxTravelInOneRound
            var ans=fullRounds*s.length
            for(i in pos.indices) {
                if(rest<=manh(pos[0], pos[i])+i) {
                    println("${ans+i}")
                    return
                }
            }
        }
        */
    }

    fun manh(p1:Pair<Int, Int>, p2:Pair<Int, Int>):Int {
        return abs(p1.first-p2.first) + abs(p1.second-p2.second)
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
