package com.happypeople.codeforces.c1117

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1117().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1117 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextLong()
        val m=sc.nextLong()
        val k=sc.nextLong()
        val a=(1..n).map { sc.nextLong() }.sortedDescending()

        val p=k+1
        if(m<=k) {
            println("${a[0]*m}")
        } else {
            var ans = (m / p) * a[1] + (m / p) * (p - 1) * a[0]
            ans += (m % p) * a[0]
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
