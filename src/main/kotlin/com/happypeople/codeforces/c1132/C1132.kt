package com.happypeople.codeforces.c1132

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        C1132().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class C1132 {
    var pL=IntArray(1)
    var pR=IntArray(1)
    var preSum=IntArray(1)
    var counts1=IntArray(1) // count of 1 in range i
    var counts2=IntArray(1) // count of 2 in range i
    var c1Sort=IntArray(1)

    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val q=sc.nextInt()
        pL=IntArray(q)
        pR=IntArray(q)
        preSum=IntArray(n+1)
        for(i in 0..q-1) {
            pL[i]=sc.nextInt()-1
            pR[i]=sc.nextInt()-1
            preSum[pL[i]]++
            preSum[pR[i]+1]--
        }
        for(i in 1..preSum.size-1)
            preSum[i]+=preSum[i-1]

        log("preSum: ${preSum.toList()}")
        counts1=IntArray(q) // count of 1 in range i
        for(i in counts1.indices) {
            for(j in pL[i]..pR[i]) {
                if(preSum[j]==1)
                    counts1[i]++
            }
        }

        var ans=Int.MIN_VALUE
        c1Sort=(counts1.indices).sortedBy { counts1[it] }.toIntArray()
        log("c1Sort: ${c1Sort.toList()}")

        for(i in counts1.indices) {
            for(j in 0..i) {
                if(j!=i) {
                    var x=calc(i, j)
                    log("calc($i, $j)=$x")
                    //x=calc(j, i)
                    //log("calc($j, $i)=$x")
                    ans = max(ans, calc(i, j))
                    //ans = max(ans, calc(j, i))
                }
            }
        }
        println("${n-ans}")

        // find that two i where the least 1s and 2s turn to 0s
    }

    private fun intersect(i:Int, j:Int):Boolean {
        return (pL[i]<=pL[j] && pR[i]>=pL[j]) || (pL[i]<=pR[j] && pR[i]>=pR[j])
    }
    // calc the number of not painted sections if removed i and j
    private fun calc(i: Int, j: Int): Int {
        val sI=c1Sort[i]
        val sJ=c1Sort[j]
        log("calc $sI $sJ")
        log("counts: ${counts1.toList()}")
        if(!intersect(i, j))
            return counts1[sI] + counts1[sJ]
        else {
            val interL=max(pL[sI], pL[sJ])
            val interR=min(pR[sI], pR[sJ])
            var c2=0
            for(i in interL..interR)
                if(preSum[sI]==2 || preSum[sI]==1)
                    c2++
            for(i in min(sI, sJ)..interL-1)
                if(preSum[sI]==1)
                    c2++
            for(i in interR+1..max(pR[sI], pR[sJ]))
                if(preSum[sI]==1)
                    c2++
            return c2
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
