package com.happypeople.codeforces.c1132

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1132().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class B1132 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        var sumA=0L
        val a=(1..n).map {
            val x=sc.nextInt()
            sumA+=x
            x
        }.sorted()
        val m=sc.nextInt()
        val q=(1..m).map { sc.nextInt() }

        val sb=StringBuffer("")
        for(i in q) {
            var ans=sumA-a[a.size-i]
            sb.append("$ans\n")
        }
        println(sb.toString())
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
