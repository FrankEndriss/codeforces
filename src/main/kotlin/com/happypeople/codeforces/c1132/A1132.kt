package com.happypeople.codeforces.c1132

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1132().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1132 {
    fun run() {
        val sc = Scanner(systemIn())
        val c=(1..4).map { sc.nextInt() }

        if(chk(c))
            println("1")
        else
            println("0")
    }

    fun chk(cnt:List<Int>):Boolean {
        if(cnt[2]>0 && ( cnt[0]==0 || cnt[3]==0))
             return false

        return cnt[0]==cnt[3]
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
