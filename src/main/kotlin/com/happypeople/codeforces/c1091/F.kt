package com.happypeople.codeforces.c1091

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        F().run()
    } catch (e: Throwable) {
        F.log("" + e)
    }
}

class F {
    val Gras=0
    val Water=1
    val Lava=2

    fun run() {
        val sc = Scanner(systemIn())
        val numSegments=sc.nextInt()
        val slengs=(1..numSegments).map { sc.nextInt() }
        val typesString=sc.next()!!
        val types=typesString.map {
            when(it) {
            'G' -> 0
            'W' -> 1
            'L' -> 2
                else -> { throw RuntimeException(":/") // notreached
                }
            } }
        // G W L
        val timePerMeter=listOf(5, 3, 1) // walk, swim, fly

        // simply simulate the run
        var stam=0
        var currPos=0
        var time=0
        while(currPos<types.size) {     // must reach other end
            val currType=types[currPos]
            if(currType==Water) {
                time+=slengs[currPos]*timePerMeter[Water]
                stam+=slengs[currPos]
            } else { // need to walk or fly. So calc how much stam we need to gain in (last) water, so we do not need to gain extra stam ono gras
                var neededStam=calcNeededStamAhead(slengs, currPos, types, stam)
                log("neededStam: $neededStam")
                if(neededStam>0) {  // gain stam by swim or walk backwards, the forward again
                    // note that if stam is needed, then last segment is Water
                    // just check this. :/ NOTE if all starts with Gras and Lava, there might be no Water
                    if(currPos>0 && types[currPos-1]==Water) {
                        time += neededStam * timePerMeter[Water]
                        stam += neededStam
                    } else { // do the same, but on Gras
                        time += neededStam * timePerMeter[Gras]
                        stam += neededStam
                    }
                }

                // TODO if we have enough stam, we can fly instead of walk or swim
                // thats important on the last segment(s).
                calculateIfStamIsEnoughToFlyAfterCurrentSegement(slengs, currPos, types, stam)

                // so, now just walk or fly
                if(currType==Lava) {
                    time+=slengs[currPos]
                    stam-=slengs[currPos]
                } else { // currType==Gras
                    time+=slengs[currPos]*timePerMeter[Gras]
                    stam+=slengs[currPos]
                }
            }
            currPos++
        }
        println("$time")
    }

    private fun calculateIfStamIsEnoughToFlyAfterCurrentSegement(slengs: List<Int>, currPos: Int, types: List<Int>, stam: Int) {
        throw RuntimeException("implement me!!!")
    }

    private fun calcNeededStamAhead(slengs: List<Int>, currPos: Int, types: List<Int>, stam:Int): Int {
        // run until end or next water, allowing negativ stamina
        // report min stamina as neededStamina
        var i=0
        var pos=currPos
        var minStam=stam
        var lStam=stam
        while(pos<slengs.size && (types[pos]==Gras || types[pos]==Lava)) {
            if(types[pos]==Gras) {
                lStam+=slengs[pos]
            } else { //fly
                lStam-=slengs[pos]
                minStam=min(minStam, lStam)
            }
            pos++
        }
        return if(minStam<0) -minStam else 0
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}