package com.happypeople.codeforces.c1091

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.lang.RuntimeException
import java.math.BigInteger
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        C.log("" + e)
    }
}

// there is something with the prime factors of n... i dont know
class C {
    fun fakt(i:Long):Long {
        if(i==1L)
            return 1
        else
            return i*fakt(i-1)
    }

    fun gauss(n:Int):Int {
        return ((n + 1) * n) / 2       // sum(1..n)
    }

    val primes=listOf(2,3,5,7,11,13,17,19,23,29)
    fun primefactorsOf(n:Int):List<Int> {
        if(n==1)
            return listOf<Int>()

        for(p in primes) {
            if(n%p==0)
                return primefactorsOf(n/p).plus(p)
        }
        throw RuntimeException(":/") // notreached
    }

    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()

        val funValues= mutableListOf(1) // for k=n


        print("1 ")
        for(p in primefactorsOf(n)) {
        }

        /*
        (2..n).forEach { k ->
            val bi=BigInteger.valueOf(k.toLong())

            if(n%k==0) {
                val res=gauss(n)/k
                print("$k:$res ")
            } else if(n%k==1 || n%k==n-1){
                val res=gauss(n)
                print("$k:$res ")
            } else {
                for(p in primefactorsOf(n))
                print("$k:??? ")
            }
        }
        */


        funValues.forEach { print("$it ") }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}