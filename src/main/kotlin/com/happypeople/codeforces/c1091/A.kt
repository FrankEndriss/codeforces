package com.happypeople.codeforces.c1091

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        A.log("" + e)
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val y=sc.nextInt()
        val b=sc.nextInt()
        val r=sc.nextInt()

        // b==y+1
        // r==b+1

        var m=min(b, y+1)
        m=min(m+1, r)
        m=3*m-3
        println("$m")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}