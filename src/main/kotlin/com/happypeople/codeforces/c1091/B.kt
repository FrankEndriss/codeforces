package com.happypeople.codeforces.c1091

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        B.log("" + e)
    }
}

class B {
    data class P(val x:Int, val y:Int) {
        fun add(other:P):P {
            return P(x+other.x, y+other.y)
        }
    }

    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val obelisks=(1..n).map { P(sc.nextInt(), sc.nextInt()) }
        val clues=(1..n).map { P(sc.nextInt(), sc.nextInt()) }

        var tPositions= setOf<P>()
        obelisks.forEach { obel ->
            val cMap=clues.map { it.add(obel) }.toSet()
            if(tPositions.isEmpty())
                tPositions=cMap
            else
                tPositions=tPositions.intersect(cMap)
            if(tPositions.size==1) {
                val res=tPositions.first()
                println("${res.x} ${res.y}")
                return
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}