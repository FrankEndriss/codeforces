package com.happypeople.codeforces.codejam0

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.math.BigInteger
import java.util.*

fun main(args: Array<String>) {
    try {
        Ccodejam0().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class Ccodejam0 {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        for(ca in 1..t) {
            sc.next()   // ignore n
            val l=sc.nextInt()
            val L=(1..l).map { BigInteger(sc.next().trim()) }.toMutableList()

            var extra=0
            while(L[0].equals(L[1])) {
                extra++
                L.removeAt(0)
            }
            //log("extra=$extra")

            val P=mutableListOf<BigInteger>()
            val secondP=gcd(L[0], L[1])
            val firstP=L[0].divide(secondP)
            //log("firstP=$firstP secondP=$secondP")
            P.add(firstP)
            P.add(secondP)
            for(i in 1..l-1-extra) {
                P.add(L[i] / P[i])
                //log("P=$P")
            }

            val s=P.toSet().toList().sorted().mapIndexed { idx, it ->
                it to 'A'+idx
            }.toMap()

            print("Case #$ca: ")
            for(i in 1..extra) {
                P.add(0, L[0]/P[0])
                //print("${s[P[1]]}")
            }
            for(bd in P)
                print("${s[bd]}")
            println()
        }
    }

    fun gcd(pA: BigInteger, pB: BigInteger): BigInteger {
        var b = pB
        var a = pA
        while (b.compareTo(BigInteger.ZERO)>0) {
            val temp = b
            b = a % b
            a = temp
        }
        return a
    }

    companion object {

        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
