package com.happypeople.codeforces.c1298

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        E1298().run()
    } catch (e: Throwable) {
        println("")
    }
}

// sic array initialization
inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
    val a= arrayOfNulls<E>(size)
    for(i in a.indices)
        a[i]=init(i)
    return a as Array<E>
}

/* sort programmers by skill.
* count per programmer the number of querrel programmer with lower skill
* for all programmers, binsearch idx in list sorted by skill, and
* subtract querrels.
*
* TODO implement binary search by hand
 **/
class E1298 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val k=sc.nextInt()
        val r=(1..n).map { sc.nextInt() }
        val q= arrOf(n){ 0 }
        (1..k).forEach {
            val x=sc.nextInt()-1
            val y=sc.nextInt()-1
            if(r[x]<r[y])
                q[y]++
            if(r[y]<r[x])
                q[x]++
        }
        val r2=r.map { it }.sorted()

        r.forEachIndexed { index, element ->
            /* binary search how much elements in r2 are smaller than element.
            * set lo to max(idx)<element */
            var lo=0
            var up=n
            while(lo+1<up) {
                val mid=(lo+up)/2
                if(r2[mid]>=element)
                    up=mid
                else
                    lo=mid
            }
            while(lo<n && r2[lo]<element)
                lo++

            val ans=lo-q[index]
            print("$ans ")
        }
        println()
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}