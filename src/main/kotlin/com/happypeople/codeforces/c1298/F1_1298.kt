package com.happypeople.codeforces.c1298

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        F1_1298().run()
    } catch (e: Throwable) {
        println("")
    }
}

/** for F2_1298 we need to consider that there are more than one element
 * equal to m, and we need to loop over them.
 * Then, for every one we need to loop over all positions
 * 0..idx.
 * Which is O(n^2) unfortunatly.
 */
class F1_1298 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=sc.nextInt()-1
        val p=(1..n).map { sc.nextInt()-1 }
        val pos=IntArray(n)
        p.forEachIndexed { index, element ->
            pos[element]=index
        }

        log("m=$m pos[m]=${pos[m]}")

        /* let pos[i]==position of p[i]
        * find number of pairs (l,r) where
        * 1. l<= pos[m] <=r because to be median m must be in interval
        * 2. let num elements>m== cntbg; num elements<m cntlt
        *   if(r-l is odd) then cntbg==cntlt
        *   else then cntlt+1==cntbg
        *
        * For every 1..L we need to find the Rs which make the element at pos[m]
        * (which is m) the median.
        * At every position we find the number of position on the right
        * which match to make them equal (diff==1 for even r-l)
        * To find the number of positions we index the part
        * pos[m]..n
        **/

        val sz=n // number of positions  pos[m]..n
        val dpRodd= IntArray(sz*2+1) // dpRodd[i]==number of odd positions right of pos[m] with bal=i. Note -sz<bal<sz
        val dpReven=IntArray(sz*2+1)// dpReven[i]==number of even positions right of pos[m] with bal=i

        var bal=0
        dpReven[bal+sz]++       // zero positions counts as even

        (pos[m]+1..n-1).forEach { idx ->
            if(p[idx]>m-1)
                bal++
            else
                bal--

            log("indexing position=$idx bal=$bal")

            val npos=idx-pos[m] // number positions right of pos[m], _not_ including pos[m]
            if(npos %2 == 0)
                dpReven[bal+sz]++
            else
                dpRodd[bal+sz]++
        }

        log("sz=$sz dpReven: ")
        dpReven.forEachIndexed { index, i ->  log("idx=$index i=$i")}
        log("dpRodd: ")
        dpRodd.forEachIndexed { index, i ->  log("idx=$index i=$i")}

        var ans=0L+dpRodd[sz+1]   // interval starting at pos[m] of even length
        ans+=dpReven[sz]  // invervals starting at pos[m] of odd length
        log("number of intervals starting at pos[m]==$ans")

        bal=0
        var idx=pos[m]-1
        while(idx>=0) {
            log("second loop, idx=$idx")
            if(p[idx]>m)
                bal++
            else
                bal--

            /* ans+= number of matching values in dpR.
            * A matching value is one where
            * let len=dpR.idx-idx+1
            * len is odd && bal+dpR.bal==0 || len is even && bal+dpR.bal==1
            * This again is tricky index fiddling.
            * For even sizes we need to find number of positons with bal==-1,
            * for odd sizes we need to find number of positions with bal==0
            * */
            val cnt=pos[m]-idx+1 // number of positions left of pos[m] up to including pos[m]

            log("bal=$bal cnt=$cnt")
            if(cnt%2==0) {
                ans+=dpReven[sz-bal+1]    // even plus even is even, so bal has to eq -1
                ans+=dpRodd[sz-bal]   // even plus odd is odd, so bal has to eq 0
            } else {
                ans+=dpReven[sz-bal]    // odd plus even is odd, so bal has to eq 0
                ans += dpRodd[sz-bal+1]
            }
            idx--
        }

        log("ans=$ans")
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}