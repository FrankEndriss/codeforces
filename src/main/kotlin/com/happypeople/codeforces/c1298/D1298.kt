package com.happypeople.codeforces.c1298

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D1298().run()
    } catch (e: Throwable) {
        println("")
    }
}

class D1298 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val w=sc.nextInt()
        var sum=0
        var mi=0
        var ma=0
        (1..n).forEach {
            sum += sc.nextInt()
            if (sum < mi)
                mi = sum
            if (sum > ma)
                ma = sum
        }
        log("mi=$mi ma=$ma w=$w")
        mi=-mi
        ma+=mi

        var ans=0
        if(ma>w)
            ans=0
        else
            ans=w+1-ma

        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}