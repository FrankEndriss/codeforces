package com.happypeople.codeforces.c1298

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C1298().run()
    } catch (e: Throwable) {
        println("")
    }
}

class C1298 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val s=sc.next()

        var cnt=0
        var ans=0

        s.forEach {
            if(it=='x')
                cnt++
            else
                cnt=0

            if(cnt>=3)
                ans++
        }
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}