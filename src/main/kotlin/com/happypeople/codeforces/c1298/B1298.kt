package com.happypeople.codeforces.c1298

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1298().run()
    } catch (e: Throwable) {
        println("")
    }
}

class B1298 {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val m=mutableMapOf<Int,Int>()
        val a=(1..n).forEach { m[sc.nextInt()]=it }

        val l=m.map { Pair(it.value, it.key ) }
               .sortedWith(compareBy({it.first}, {it.second}))
               .map { it.second }
        println(l.size)
        l.forEach { print("$it ") }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}