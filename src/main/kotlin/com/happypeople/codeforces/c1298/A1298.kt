package com.happypeople.codeforces.c1298

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A1298().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class A1298 {
    fun run() {
        val sc = Scanner(systemIn())
        val a=(1..4).map { sc.nextInt() }.sorted()
        val ans1=a[3]-a[0]
        val ans2=a[3]-a[1]
        val ans3=a[3]-a[2]
        println("$ans1 $ans2 $ans3")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}