package com.happypeople.codeforces.c1096

import java.util.*

fun main(args: Array<String>) {
    D().run()
}

class D {
    val ex1 = "6\n" +
            "hhardh\n" +
            "3 2 9 11 7 1\n"

    fun run() {
        //val sc = Scanner(System.`in`)
        val sc = Scanner(ex1)
        val len = sc.nextInt()
        val str = sc.next()
        val a = mutableListOf<Int>()
        for (i in 1..len)
            a.add(sc.nextInt())

        if (!isHard(str)) {
            println("0")
        } else {
            val lPerms=perms(a)
            perms(a).forEach {
                    // take(1000).sortedBy {
                println("sorted by $it")
                it.map { a[it] }.sum()
            }

            // println("$len $str $a $b")
            for (removals in lPerms) {
                println("removals $removals")
                val transformed = removeThem(str, removals)
                if (!isHard(transformed)) {
                    val price = removals.map { a[it] }.sum()
                    println("$transformed $price")
                }
            }
        }

        //* calc and sort all sublists of a so that sum(a[i]) is min.
        // this starts with the list of one ent, the min ai
        // sequence of permutations....
        // then apply that sublists of a in that order
        // first transformed string which is not hard is answer
    }

    /** removes the chars at pos rms */
    fun removeThem(str: String, rms: List<Int>): String {
        val arr = str.toCharArray()
        for (i in rms)
            arr[i] = '#'
        return String(arr).replace("#", "")
    }

    fun incIdx(idx: MutableList<Int>, lenA: Int) {
        // lowest idx digit right
        println("incIdx: $idx")
        var uebertrag = false
        var i = 0
        do {
            do {
                uebertrag = false
                if (i == idx.size)
                    idx.add(0)
                else if (idx[i] == lenA - 1) {
                    idx[i] = 0
                    i++
                    uebertrag = true
                } else
                    idx[i]++
                // println("inner loop $idx")
            } while (uebertrag)
        } while (idx.size != idx.toSet().size) // no doubles
    }

    fun perms(a: List<Int>): Sequence<List<Int>> {
        val sortedA = (0..a.size-1).toMutableList().sortedBy { a[it] }
        var idx = mutableListOf(0)
        return generateSequence {
            if(idx.size==a.size)
                null
            else {
                val ret = idx.map { sortedA[it] }
                incIdx(idx, a.size)
                ret
            }
        }
    }

    fun isHard(str: String): Boolean {
        return Scanner(str).hasNext(".*h.*a.*r.*d.*")
    }

}