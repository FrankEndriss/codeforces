package com.happypeople.codeforces.c1096

import java.util.*

fun main(args: Array<String>) {
    A().run()
}

class A {
    val ex=
    "3\n"+
    "1 10\n"+
    "3 14\n"+
    "1 10\n"

    fun run() {
        val sc = Scanner(System.`in`)
        // val sc = Scanner(ex)
        val t=sc.nextInt()
        for(i in 1..t) {
            val l=sc.nextInt()
            val r=sc.nextInt()

            val primes= mutableListOf<Int>()
            // find pseudo primes
            for(p in l..r) {
                if(primes.filter { p%it ==0 }.firstOrNull()==null)
                    primes.add(p)
            }
            // println("$l $r $primes")

            var fini=false
            var x=0
            while(x<=primes.size && !fini) {
                var y=primes[x]+1
                while(y<=r && !fini) {
                    if(y!=primes[x] && y%primes[x]==0) {
                        println("${primes[x]} $y")
                        fini = true
                    }
                    y++
                }
                x++
            }
        }
    }
}
