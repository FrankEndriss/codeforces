package com.happypeople.codeforces.c1099

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        C.log("" + e)
    }
}

// worst code ever :/
class C {
    fun run() {
        val sc = Scanner(systemIn())
        var str = sc.nextLine()!!.trim()
        val k = sc.nextInt()

        if(str.length==0) {
            if(k>0)
                println("Impossible")
            else
                println()
            return
        }

        val specialCount = str.filter { it == '*' || it == '?' }.count()
        val minLen = str.length - (2 * specialCount)
        if (k < minLen) {
            println("Impossible")
            return
        }

        if(!str.contains("*")) {
            val maxLen=str.length-specialCount
            if(maxLen<k) {
                println("Impossible")
                return
            }
        }

        var len = str.length - (specialCount * 2) // len if all removed
        log("before, len=$len k=$k : $str")
        while (len < k) {
            val idx = str.indexOf('?')
            if (idx > 0) {
                val repl = str[idx - 1]
                val repl1 = "" + repl + "?"
                val repl2 = "" + repl
                str = str.replaceFirst(repl1, repl2)
                len++
            } else
                break
        }
        log("after1: $str")

        if (len < k) { // replace first * by as much as needed chars
            val idx = str.indexOf('*')
            // idx allways >=0
            val repl = str[idx - 1]
            val repl1 = "" + repl + "*"
            val repl2 = buildString(repl, k - len)
            log("seconds repl2: $repl2")
            str = str.replaceFirst(repl1, repl2)
        }
        log("after2: $str")

        while (str.contains("*")) {
            val idx = str.indexOf('*')
            val repl = str[idx - 1]
            val repl1 = "" + repl + "*"
            str = str.replace(repl1, "")
        }
        log("removed *: $str")
        while (str.contains("?")) {
            val idx = str.indexOf('?')
            val repl = str[idx - 1]
            val repl1 = "" + repl + "?"
            str = str.replace(repl1, "")
        }
        log("len result: ${str.length}")

        println("$str")
    }

    fun buildString(c: Char, len: Int): String {
        var s = "" + c
        while (s.length < len)
            s = s + c
        return s
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}