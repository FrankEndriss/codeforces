package com.happypeople.codeforces.c1099

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        A.log("" + e)
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        var w=sc.nextInt()
        var h=sc.nextInt()
        val u1=sc.nextInt()
        val d1=sc.nextInt()
        val u2=sc.nextInt()
        val d2=sc.nextInt()

        while(h>0) {
            w+=h
            if(h==d1)
                w-=u1
            if(w<0)
                w=0
            if(h==d2)
                w-=u2
            if(w<0)
                w=0
            h--
        }
        println("$w")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}