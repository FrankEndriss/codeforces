package com.happypeople.codeforces.c1099

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.sqrt

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        B.log("" + e)
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val s=sqrt(n.toDouble()).toInt()
        var r=n-s*s

        var ans=s+s
        var nextS=s
        var flip=true
        val seq= generateSequence {
            val tmp=nextS
            if(flip)
                nextS++
            flip=!flip
            tmp
        }.iterator()

        while(r>0) {
            ans++
            r-=seq.next()
        }

        if(ans<2)
            ans=2
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}