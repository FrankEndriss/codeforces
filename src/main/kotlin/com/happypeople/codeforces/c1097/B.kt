package com.happypeople.codeforces.c1097

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        B.log("" + e)
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val a=(1..n).map { sc.nextInt() }

        val res=binsearch(a, 0, 0)
        if(res)
            println("YES")
        else
            println("NO")
    }

    fun binsearch(a:List<Int>, next:Int, sum:Int):Boolean {
        if(next==a.size)
            return sum%360==0
        else
            return binsearch(a, next+1, sum+a[next]) ||
                binsearch(a, next+1, sum-a[next])
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}