package com.happypeople.codeforces.c1097

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        C.log("" + e)
    }
}

class C {
    data class S(val needOpenAtLeft: Int, val needCloseAtRight: Int)

    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()

        var ans = 0
        val multiset = Multiset<Int>()
        for (i in 1..n) {
            val seq = sc.next()!!.trim()

            var needOpenAtLeft = 0
            var count = 0

            for (c in seq) {
                if (c == '(') {
                    count--
                } else {
                    count++
                    needOpenAtLeft = max(needOpenAtLeft, count)
                }
            }

            var needCloseAtRight = 0
            count = 0
            for (c in seq.reversed()) {
                if (c == '(') {
                    count++
                    needCloseAtRight = max(needCloseAtRight, count)
                } else {
                    count--
                }
            }

            if (needOpenAtLeft > 0 && needCloseAtRight > 0) // cant build pair from such seq
                continue

            val seqVal = needOpenAtLeft - needCloseAtRight
            if (multiset.contains(-seqVal)) {
                multiset.remove(-seqVal)
                ans++
            } else
                multiset.add(seqVal)
        }

        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }

    class Multiset<E> {
        private val map = mutableMapOf<E, Int>()

        public fun add(e: E) {
            val c = map.getOrDefault(e, 0)
            map[e] = c + 1
        }

        public fun remove(e: E) {
            val c = map.getOrDefault(e, 0)
            if (c > 1)
                map[e] = c - 1
            else if (c == 1)
                map.remove(e)
        }

        public fun contains(e: E) =
                map.contains(e)

        public fun count(e: E) =
                map.getOrDefault(e, 0)

        public fun toList(): List<E> {
            return map.flatMap { ent -> (1..ent.value).map { ent.key } }
        }

        public fun toSet(): Set<E> = map.keys
    }
}