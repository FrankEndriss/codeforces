package com.happypeople.codeforces.c1097

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        Ax().run()
    } catch (e: Throwable) {
        Ax.log("" + e)
    }
}

class Ax {
    fun run() {
        val sc = Scanner(systemIn())
        val card=sc.next().trim()
        val hand=(1..5).map { sc.next().trim() }
        log("card=$card, hand=$hand")
        val c=hand.filter { it[0]==card[0]  || it[1]==card[1] }.count()
        if(c>0)
            println("YES")
        else
            println("NO")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}