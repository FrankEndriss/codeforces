package com.happypeople.codeforces.c1093

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        C().run()
    } catch (e: Throwable) {
        C.log("" + e)
    }
}

class C {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        val b=(1..(n/2)).map {
            sc.nextLong()
        }

        val ans=LongArray(n)
        ans[0]=0
        ans[n-1]=b[0]
        for(i in 1..n/2-1) {
            var increment=0L
            if(b[i]>ans[n-i])     // right side constraint
                increment=b[i]-ans[n-i]

            if(increment<ans[i-1]) // left side constraint
                increment=ans[i-1]

            ans[i]=increment
            ans[n-i-1]=b[i]-increment
        }

        val str=ans.joinToString(" ")
        println("$str")

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}