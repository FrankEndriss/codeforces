package com.happypeople.codeforces.c1093

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        A().run()
    } catch (e: Throwable) {
        A.log("" + e)
    }
}

class A {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        (1..n).forEach {
            val i=sc.nextInt()
            val rolls=if(i==2) 1 else i/3
            println("$rolls")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}