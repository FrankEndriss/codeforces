package com.happypeople.codeforces.c1093

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        D().run()
    } catch (e: Throwable) {
        D.log("" + e)
    }
}

class D {
    val MOD = 998244353
    val GREY = 0
    val WHITE = 1
    val BLACK = 2
    val NMAX = 300004

    fun run() {
        // use a table of pows to speed up testcases with a lot of subgraphs
        val p2 = IntArray(NMAX)
        p2[0] = 1;
        for (i in 1..p2.size - 1)
            p2[i] = (2 * p2[i - 1]) % MOD

        val sc = Scanner(systemIn())
        // Build one tree of 1s and 2s.
        // Since all 1s can be changed to 3s this is 2^(num1s) possibilities.
        // And we could have used 1s and 2s vice versa.
        // That is another 2^(nums2s) possibilities. Add them.
        // So, if beautiful tree can be constructed, it is
        // 2^((n+1)/2) + 2^(n/2) possibilities.
        // This number has to be calculated for every single subgraph, and
        // all that numbers have to be multiplicated.

        val t = sc.nextInt()
        val out=StringBuffer("")
        (1..t).forEach {
            val n = sc.nextInt()
            val m = sc.nextInt()
            // log("n=$n m=$m")
            val edges = (1..n).map { mutableListOf<Int>() }
            (1..m).forEach {
                val x = sc.nextInt() - 1
                val y = sc.nextInt() - 1
                edges[x].add(y)
                edges[y].add(x)
            }

            val vColor = IntArray(n)   // vertex values: 0=grey, 1/2
            val stack = ArrayDeque<Int>()

            var bad = false
            var ans = 1L
            for (vertex in vColor.indices) {
                if (vColor[vertex] != GREY)
                    continue
                var colorCount = IntArray(3)

                stack.push(vertex)
                vColor[vertex] = WHITE
                colorCount[WHITE]++

                // recursiv mark all connected vertices
                while (!stack.isEmpty()) {
                    val currVertex = stack.pop()!!
                    for (child in edges[currVertex]) {
                        if (vColor[currVertex] == vColor[child]) {
                            // graph is not bipartit
                            stack.clear()
                            bad = true
                            break
                        }
                        if (vColor[child] == GREY) {   // recurse
                            vColor[child] = if (vColor[currVertex] == WHITE) BLACK else WHITE
                            colorCount[vColor[child]]++
                            stack.push(child)
                        }
                    }
                }

                val bi = (p2[colorCount[WHITE]] + p2[colorCount[BLACK]]) % MOD
                ans = (ans * bi) % MOD
            }

            if (!bad) {
                out.append(ans)
                out.append("\n")
            } else {
                out.append("0\n")
            }
        }
        print(out.toString())
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}