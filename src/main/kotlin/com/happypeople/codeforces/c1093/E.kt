package com.happypeople.codeforces.c1093

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs

fun main(args: Array<String>) {
    try {
        E().run()
    } catch (e: Throwable) {
        E.log("" + e)
    }
}

/** This implementation is naive slow, see https://codeforces.com/problemset/problem/1093/E
 * The tutorial solution uses a Fenwick tree and some fancy modifications of the data.
 **/
class E {
    val N = 200002
    val out = StringBuffer("")
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val m = sc.nextInt()
        var a = IntArray(N)
        var b = IntArray(N)
        for (i in (0..n - 1)) {
            a[i] = sc.nextInt()
        }
        for (i in (0..n - 1)) {
            b[i] = sc.nextInt()
        }
        log("a.size=${a.size} n=$n m=$m")

        for (i in (1..m)) {
            val q = sc.nextInt()
            if (q == 1) {
                query(sc.nextInt() - 1, sc.nextInt() - 1, a, sc.nextInt() - 1, sc.nextInt() - 1, b, n)
            } else
                swap(sc.nextInt() - 1, sc.nextInt() - 1, b)
        }
        print(out.toString())
    }

    val bIndex = BooleanArray(N) // all false

    /** Sets the values in indexIntervals of indexValues in bIndex to value setV.
     * Use setV=true before search, and setV=false after search. */
    private fun setIndex(indexIntervals: IntArray, indexValues: IntArray, setV: Boolean) {
        for (idx in indexIntervals[0]..indexIntervals[1]) {
            bIndex[indexValues[idx]] = setV
        }
        if (indexIntervals.size > 2) {
            for (idx in indexIntervals[2]..indexIntervals[3]) {
                bIndex[indexValues[idx]] = setV
            }
        }
    }

    /** Searches the bIndex for values searchV given by searchIntervals and seachValues. */
    private fun searchIndex(searchIntervals: IntArray, searchValues: IntArray, searchV: Boolean): Int {
        var c = 0
        for (idx in searchIntervals[0]..searchIntervals[1])
            if (bIndex[searchValues[idx]] == searchV)
                c++

        if (searchIntervals.size > 2) {
            for (idx in searchIntervals[2]..searchIntervals[3])
                if (bIndex[searchValues[idx]] == searchV)
                    c++
        }

        return c
    }

    val indexInterval2 = IntArray(2)
    val indexInterval4 = IntArray(4)
    val searchInterval2 = IntArray(2)
    val searchInterval4 = IntArray(4)

    private fun query(raL: Int, raR: Int, pA: IntArray, rbL: Int, rbR: Int, pB: IntArray, n: Int) {
        // Two optimizations:
        // 1. Use the smaller interval for the index.
        // 2. If the bigger interval is bigger than n/2 check the values _not_ in the interval.

        // The "smaller" interval is the one farther away from the middle. Use that one as index.
        val sizeofA=raR-raL
        val sizeofB=rbR-rbL
        val aCosts=n/2-abs(n/2-sizeofA)
        val bCosts=n/2-abs(n/2-sizeofB)

        if (aCosts > bCosts) { // use smaller range as index range
            query(rbL, rbR, pB, raL, raR, pA, n) // change a and b
            return
        }

        var c = 0
        if (sizeofA < n / 2) { // use positive indexInterval
            indexInterval2[0] = raL
            indexInterval2[1] = raR
            setIndex(indexInterval2, pA, true)
            searchInterval2[0] = rbL
            searchInterval2[1] = rbR
            c = searchIndex(searchInterval2, pB, true)
            setIndex(indexInterval2, pA, false) // cleanup
        } else {        // use negative indexIntervals
            indexInterval4[0] = 0
            indexInterval4[1] = raL - 1
            indexInterval4[2] = raR + 1
            indexInterval4[3] = n - 1
            setIndex(indexInterval4, pA, true)
            searchInterval2[0] = rbL
            searchInterval2[1] = rbR
            c = searchIndex(searchInterval2, pB, false)
            setIndex(indexInterval4, pA, false) // cleanup
        }
        out.append(c)
        out.append("\n")
    }

    private fun swap(i1: Int, i2: Int, b:IntArray) {
        val t = b[i1]
        b[i1] = b[i2]
        b[i2] = t
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}