package com.happypeople.codeforces.c1093

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B().run()
    } catch (e: Throwable) {
        B.log("" + e)
    }
}

class B {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        for (i in (1..n)) {
            val str=sc.next().toCharArray()
            str.sort()
            val s1=String(str)
            val s2=String(str.reversed().toCharArray())
            if(s1==s2)
                println("-1")
            else
                println(s1)
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}