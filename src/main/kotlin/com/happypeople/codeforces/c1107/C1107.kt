package com.happypeople.codeforces.c1107

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        C1107().run()
    } catch (e: Throwable) {
        println("")
    }
}
class C1107 {
    fun run() {
        val sc = Scanner(systemIn())
        val hits = sc.nextInt()
        val k=sc.nextInt()  // max press button in a row
        val a = (1..hits).map { sc.nextLong() }      // damages of hit i
        val s=sc.next().trim()  // buttons a-z

        var idx=0
        var ans=0L
        for(len in split(s)) {
            ans += a.subList(idx, idx + len).sortedByDescending { it }.take(min(k, len)).sum()
            idx+=len
        }
        println("$ans")
    }

    /** split the string into lens of substrings of same char. */
    fun split(s:String):List<Int> {
        val ret=mutableListOf<Int>()
        var oldC=s[0]
        var count=1
        for(i in 1..s.length-1) {
            val c=s[i]
            if(c==oldC)
                count++
            else {
                ret.add(count)
                count=1
            }
            oldC=c
        }
        ret.add(count)
        return ret
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}