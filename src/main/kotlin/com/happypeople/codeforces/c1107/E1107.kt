package com.happypeople.codeforces.c1107

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        E1107().run()
    } catch (e: Throwable) {
        println("")
    }
}

// see
class E1107 {
    val NMAX = 205
    var n = 0
    var S = ""
    var A = LongArray(1)
    val dp = arrOf(NMAX) { arrOf(NMAX) { LongArray(NMAX) }}

    inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
        val a= arrayOfNulls<E>(size)
        for(i in a.indices)
            a[i]=init(i)
        return a as Array<E>
    }

    fun run() {
        val sc = Scanner(systemIn())
        n = sc.nextInt()
        S = sc.next().trim()
        A = LongArray(n + 1)
        for (i in 1..n)
            A[i] = sc.nextLong()

        val ans = solve(0, n, 1)
        println("$ans")
    }

    private fun solve(start: Int, end: Int, prefix: Int): Long {
        if (end - start <= 0)
            return 0

        if (end - start == 1)
            return A[prefix]

        var answer = dp [start][end][prefix]

        if (answer != 0L)
            return answer

        answer = A[prefix] + solve(start + 1, end, 1)

        for (i in (start + 1)..(end-1)) {
            if (S[i] == S[start])
                answer = max(answer, solve(start + 1, i, 1) + solve(i, end, prefix + 1))
        }

        dp [start][end][prefix]=answer
        return answer
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}