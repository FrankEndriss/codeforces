package com.happypeople.codeforces.c1107

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.math.BigInteger
import java.util.*

fun main(args: Array<String>) {
    try {
        A1107().run()
    } catch (e: Throwable) {
        println("")
    }
}

class A1107 {
    fun run() {
        val sc = Scanner(systemIn())
        val q=sc.nextInt()
        for(i in 1..q) {
            val n=sc.nextInt()
            val s=sc.next().trim()
            val first=s.substring(0, 1)
            val second=s.substring(1, s.length)
            val bi1=BigInteger(first)
            val bi2=BigInteger(second)
            // log("$bi1 $bi2")
            if(bi1.compareTo(bi2)>=0) {
                println("NO")
            } else  {
                println("YES")
                println("2")
                println("$first $second")
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}