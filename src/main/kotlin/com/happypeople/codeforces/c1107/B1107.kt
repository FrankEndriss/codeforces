package com.happypeople.codeforces.c1107

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        B1107().run()
    } catch (e: Throwable) {
        println("")
    }
}

class B1107 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        for (i in 1..n) {
            val k = sc.nextLong() // 1 to 10e12
            val x = sc.nextInt()  // 1 to 9
            val ans = (k - 1) * 9 + x
            println("$ans")
        }
    }

    fun x(s: Int): Int {
        log("x($s)")
        var s = s
        if (s < 10)
            return s;
        else {
            var x = 0
            while (s > 0) {
                log("$s")
                x += s % 10
                s /= 10
            }
            return x(x)
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}