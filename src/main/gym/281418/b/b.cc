/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* What is the question?
 * That statement is even more confusing than A.
 *
 * Every day there is a number of lectures.
 * no d[i]>k1
 * no d[i]+d[i+1]> k2
 * some d[i] must be ==0
 *
 * Find distribution, sum=m
 * greedy
 *
 * Divide days into 2 categories:
 * 0-adj  and others
 * put k1 into every 0adj
 * put max(k1, k2/2) into every other
 */
void solve() {
    cini(n);
    cini(m);
    cini(k1);
    cini(k2);
    cins(s);
    assert(s.size()==n);

    vi ans(n);
    int prev=0;
    for(int i=0; i<n; i++)  {
        if(s[i]=='0') {
            prev=0;
            continue;
        }
        ans[i]=min(m, min(k1, k2-prev));
        m-=ans[i];
        prev=ans[i];
    }

    if(m) {
        cout<<"NO"<<endl;
    } else {
        cout<<"YES"<<endl;
        for(int i : ans) 
            cout<<i<<" ";
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
