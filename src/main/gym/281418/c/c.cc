/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* ...and again much text.
 * What is a storage?
 * What mean "assing (set)ai:=0"???
 * Assing what to what? What is (set)?
 *
 * Op 1 x seconds.
 * Op 2 y seconds. Evenly. Evenly seems to be simply evenly.
 *
 * End: Each bucket not more than k books.
 *
 * I do not understand, what happens to the books in the storage???
 * --> ok, according to the examples these books are simply deleted,
 *  ant that is what the problem is all about.
 *  Greedy, remove biggest shelfs until sum(book)<=n*k.
 *  if any shelf>k
 *  then find what is cheaper, removing more shelfs or distributing 
 *  once.
 *  (Needed aprox 20 minutes to understand the text, 2 minutes for solution idea).
 *  ...turns out something is wrong. Overlookes some detail?
 *  Got it completly wrong? Some stupid bug? idk :/
 */
void solve() {
    cini(n);
    cini(k);
    cini(x);
    cini(y);
    cinai(a,n);

    int sum=accumulate(a.begin(), a.end(),0LL);
//cerr<<"n="<<n<<" k="<<k<<" sum="<<sum<<endl;
    sort(a.begin(), a.end(), greater<int>());
    int ans=0;
    int idx=0;
    for(int i=0; i<n; i++) {
        if(sum>n*k) {
            ans+=x;
            sum-=a[i];
            idx++;
        } else {
            break;
        }
    }

    int ans2=0;
    while(idx<n && a[idx]>k) {
        idx++;
        ans2+=x;
    }
    ans+=min(y, ans2);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
