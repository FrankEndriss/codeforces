/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* What is the goal of the game, what do the players want?
 * blah, blah...
 * -> Find subarray with sum-max biggest possible.
 *
 *  How?
 *  The values are small. How does this help?
 *  We can just brute force 60 times that subarray
 *  assuming we do not use bigger values than x ant all.
 *  We can allways make score==0;
 *
 *  D is simpler that C and B.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans=0;
    for(int x=1; x<=30; x++) {
        int sum=0;
        for(int i=0; i<n; i++) {
            if(a[i]<=x && sum+a[i]>0) {
                sum+=a[i];
                ans=max(ans, sum-x);
            } else {
                sum=0;
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
