/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We cannt bruteforce on !15
 * So we need to analyse digit by digit.
 * Starting with lowest significant digit,
 * we need to add s[0]-a[0] to b[0];
 * if b[0]<0 maintain a carry.
 *
 * on last digit we might have to add a leading zero.
 */
void solve() {
    cini(a);
    cini(s);

    int diff=s-a;
    if(diff<0) {
        cout<<"NO"<<endl;
        return;
    }

    vi va;
    while(a>0) {
        va.push_back(a%10);
        a/=10;
    }

    vi vd;
    while(diff>0) {
        vd.push_back(diff%10);
        diff/=10;
    }

    sort(all(va));
    sort(all(vd));

    if(va==vd) {
        cout<<"YES"<<endl;
    } else {
        cout<<"NO"<<endl;
    }



}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
