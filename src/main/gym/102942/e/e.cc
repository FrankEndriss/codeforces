/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=1e9+7;
/* If s[i] is a digit there are 0 or 1 possibilities.
 * Else foreach digit there are the number of possibiblities
 * according to the previous biggest number.
 *
 * dp[i][j]== possibilities at len==i if max digit is j
 */
void solve() {
    cini(n);
    cins(s);

    vvi dp(10, vi(n+1, 0));
    dp[1][0]=1;

    for(int i=1; i<=n; i++) {
        if(s[i-1]!='-') {
            for(int j=1; j<=s[i-1]-'0'; j++) {
                dp[s[i-1]-'0'][i]+=dp[j][i-1];
                dp[s[i-1]-'0'][i]%=MOD;
            }
        } else {
            for(int j=1; j<10; j++) {
                for(int k=0; k<=j; k++) {
                    dp[j][i]+=dp[k][i-1];
                    dp[j][i]%=MOD;
                }
            }
        }
    }
    int ans=0;
    for(int i=1; i<10; i++) 
        ans=(ans+dp[i][n])%MOD;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
