/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Count how often each number is right side of the pairs.
 * This is, foreach numer is a[], we find the count of
 * numbers beeing in a left, and in b[] right of that number.
 * To do so, go from left to right, maintain two segtrees,
 * and insert a 1 at position of each number.
 * Also, foreach number count the 1 in left and right segments.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    stree seg(n);

    vi pos(n);
    for(int i=0; i<n; i++) 
        pos[b[i]-1]=i;

    ll ans=0;
    for(int i=0; i<n; i++) {
        ans+=seg.prod(pos[a[i]-1], n);
        seg.set(pos[a[i]-1], 1);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
