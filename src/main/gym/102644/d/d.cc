/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+7;

/* matrix multiplication */
vvi mulM(vvi &m1, vvi &m2, int mod=MOD) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vvi ans(sz, vi(sz));
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            for(int k=0; k<sz; k++) {
                ans[i][j]+=m1[i][k]*m2[k][j];
                ans[i][j]%=mod;
            }
        }
    }
    return ans;
}

/* matrix pow */
vvi toPower(vvi &a, int p, int mod=MOD) {
    const int sz=a.size();
    vvi res = vvi(sz, vi(sz));
    for(int i=0; i<sz; i++)
        res[i][i]=1;

    while (p != 0) {
        if (p & 1)
            res = mulM(a, res, mod);
        p >>= 1;
        a = mulM(a, a, mod);
    }
    return res;
}

/* 
 * make matrix with 1 at all positions (i,j) where there is a path
 * between i and j.
 * Then the quad of this matrix has a 2 in all positions where
 * there is a path of len 2 between i and j.
 * ****
 * see https://cp-algorithms.com/graph/fixed_length_paths.html
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    vvi g(n, vi(n));

    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        g[u][v]++;
    }

    g=toPower(g, k);
    int ans=0;
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++)
            ans=(ans+g[i][j])%MOD;

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
