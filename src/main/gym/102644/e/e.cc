/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1LL<<32;

/* matrix multiplication */
vvi mulM(vvi &m1, vvi &m2) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vvi ans(sz, vi(sz));
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            for(int k=0; k<sz; k++) {
                ans[i][j]+=(m1[i][k]*m2[k][j])%MOD;
            }
                /* WTF???
                 * Here some wired things happen. If this mod is done
                 * one line aboe within the loop we get WA. :/
                */
                ans[i][j]%=MOD;
        }
    }
    return ans;
}

/* matrix pow */
vvi toPower(vvi &a, int p) {
    const int sz=a.size();
    vvi res = vvi(sz, vi(sz));
    for(int i=0; i<sz; i++)
        res[i][i]=1;

    while (p != 0) {
        if (p & 1)
            res = mulM(a, res);
        p >>= 1;
        a = mulM(a, a);
    }
    return res;
}

/*
 * Use the generalization trick from 
 * https://cp-algorithms.com/graph/fixed_length_paths.html
 *
 * This is, we use the standard fixed length solution
 * (distance matrix to the kth pow), and extend by 
 * 'special' vertex.
 * for each normal vertex v[i] we add one special vs[i] with an
 * adge from v[i] to vs[i], and an self edge to vs[i].
 *
 * Then, the pathes to v[i] sum up in vs[i], so vs[i+1] is 
 * the number of all length paths ending in v[i].
 */
void solve() {
    cini(K);

    if(K==0) {
        cout<<"1"<<endl;
        return;
    }

    vvi g(2*64, vi(2*64));
    const vi di= { -1, -1, 1, 1, -2, -2, 2, 2 };
    const vi dj= { -2, 2, -2, 2, -1, 1, -1, 1 };
    /* construct the matrix */
    for(int i1=0; i1<8; i1++)  {
        for(int j1=0; j1<8; j1++) {
            for(int k=0; k<8; k++) {
                int i2=i1+di[k];
                int j2=j1+dj[k];
                if(i2>=0 && i2<8 && j2>=0 && j2<8) {
                    g[i1*8+j1][i2*8+j2]=1;
                }
            }
        }
    }

    /* add extra edges */
    for(int i=0; i<64; i++)  {
        g[i][i+64]=1;
        g[i+64][i+64]=1;
    }

    g=toPower(g, K+1);

    int ans=0;
    for(int i=0; i<64; i++)
        ans=(ans+g[0][i+64])%MOD;

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
