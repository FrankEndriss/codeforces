/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* matrix multiplication */
vvi mulM(vvi &m1, vvi &m2, int mod=MOD) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vvi ans(sz, vi(sz));
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            for(int k=0; k<sz; k++) {
                ans[i][j]=pl(ans[i][j], mul(m1[i][k], m2[k][j]));
            }
        }
    }
    return ans;
}

/*
 * make matrix with 1 at all positions (i,j) where there is a path
 * between i and j.
 * Then the quad of this matrix has a 2 in all positions where
 * there is a path of len 2 between i and j.
 * ****
 * see https://cp-algorithms.com/graph/fixed_length_paths.html
 *
 * This sol optimzes the potentiation, but still
 * there are up to 200*31 matrix multiplications.
 * Each mm costs 200^3 operations wich is way to much.
 *
 * We need 31*200^3 instead of 31*200^4
 * Sorting the queries does not help a lot :/
 * ***
 * We do kind of binary lifting, once all powers of 2 of the
 * matrix are calculated
 */
void solve() {
    cini(n);
    cini(m);
    cini(q);
    vvi g(n, vi(n));

    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        g[u][v]=1;
    }

    vvvi gg(32);
    gg[0]=g;
    for(int i=1; i<32; i++)
        gg[i]=mulM(gg[i-1], gg[i-1]);

    for(int i=0; i<q; i++) {
        cini(s);
        s--;
        cini(t);
        t--;
        cini(k);

        int ans=0;
        for(int j=0; j<32; j++) {
            if(k&(1LL<<j)) {
                // THIS DOES NOT WORK, simple addition is to simple :/
                ans+=gg[j][s][t];
                ans%=MOD;
            }
        }
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
