
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


const int INF=2e18+7;

/* matrix multiplication meets floyd_warshal */
vvi mulM(vvi &m1, vvi &m2) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vvi ans(sz, vi(sz, INF));
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            for(int k=0; k<sz; k++) {
                if(m1[i][k]!=INF && m2[k][j]!=INF)
                    ans[i][j]=min(ans[i][j], m1[i][k]+m2[k][j]);
            }
        }
    }
    return ans;
}

/* matrix pow */
vvi toPower(vvi a, int p) {
    assert(p>0);
    vvi res=a;
    p--;

    while (p) {
        if (p & 1)
            res = mulM(res, a);
        p >>= 1;
        a = mulM(a, a);
    }
    return res;
}

void dump(vvi& a) {
    for(size_t i=0; i<a.size(); i++) {
        for(size_t j=0; j<a[i].size(); j++) {
            if(a[i][j]==INF)
                cerr<<"INF ";
            else
                cerr<<a[i][j]<<" ";
        }
        cerr<<endl;
    }
}
/*
 * fixed length path
 * see https://cp-algorithms.com/graph/fixed_length_paths.html
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vvi mat(n, vi(n, INF));

    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        cini(w);
        mat[u][v]=min(mat[u][v], w);
    }

    vvi ans=toPower(mat, k);

    int a=INF;
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            a=min(a, ans[i][j]);

    if(a==INF)
        cout<<"IMPOSSIBLE"<<endl;
    else
        cout<<a<<endl;

}

signed main() {
    solve();
}

