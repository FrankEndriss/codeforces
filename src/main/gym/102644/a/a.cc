/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(10);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vvd= vector<vd>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* matrix multiplication */
template<typename T>
vector<vector<T>> mulM(vector<vector<T>> &m1, vector<vector<T>> &m2) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vector<vector<T>> ans=m1;
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            ans[i][j]=0;
            for(int k=0; k<sz; k++) {
                ans[i][j]+=m1[i][k]*m2[k][j];
            }
        }
    }
    return ans;
}

/* matrix pow */
template<typename T>
vector<vector<T>> toPower(vector<vector<T>> &a, int p) {
    const int sz=a.size();
    vector<vector<T>> res = vector<vector<T>>(sz, vector<T>(sz));
    for(int i=0; i<sz; i++)
        res[i][i]=1;

    while (p != 0) {
        if (p & 1)
            res = mulM(a, res);
        p >>= 1;
        a = mulM(a, a);
    }
    return res;
}

/* let h(n) be the happy,sad probs after step n,
 * let p0 be notswitch, p1 1-p0
 * so h(0)=(1,0)
 * h(1)= (p0, p1)
 * h(2)= (p0*p0 + p1*p1, p0*p1 * p1*p0)
 * ...
 * h(n)= (h(n-1).first*p0 + h(n-1).second*p1, h(n-1).second*p0 + h(n-1).second*p1)
 *
 * [1,0]
 *
 * [1-p1, 0]
 * [0, 1-p1]
 */
void solve() {
    cini(n);
    cind(p);

    vvd m={ { 1-p, p }, 
            { p, 1-p }};
    vvd ans=toPower(m, n);

//cerr<<"{ "<<ans[0][0]<<" "<<ans[0][1]<<"}\n";
//cerr<<"{ "<<ans[1][0]<<" "<<ans[1][1]<<"}\n";

    cout<<ans[0][0]<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
