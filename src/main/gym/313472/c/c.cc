/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* some knapsack variant
 * ***
 * no...
 * Is it some greedy, points per app?
 *
 * We can greedy use the mem-most apps until we remove the last app.
 * If the last app is worth cost 2, then there could be a cost 1
 * app with enough a[i], too, we need to check this.
 * ...
 * We need to greedy find the solution using the biggest apps,
 * then try to subst one of the 2-cost apps (the smallest one)
 * with any 1-cost app.
 * Alternative we can not-uninstall any one of the uninstalled
 * 1cost apps.
 * ...not sure if this works.
 */
const int INF=1e9+7;
void solve() {
    cini(n);
    cini(m);    /* mem we need to save */

    vector<pii> ab(n);
    for(int i=0; i<n; i++)
        cin>>ab[i].first;
    for(int i=0; i<n; i++)
        cin>>ab[i].second;

    sort(all(ab), [](pii p1, pii p2) {  /* biggest save first */
        return p1.first*(2/p1.second) > p2.first*(2/p2.second);
    });

    int ans=0;
    int sum=0;
    int min2=INF;   /* memory save of smallest 2cost app */
    int vis=-1;
    for(int i=0; sum<m && i<n; i++) {
        sum+=ab[i].first;
        //cerr<<"used="<<i+1<<" sum="<<sum<<endl;;
        ans+=ab[i].second;
        if(ab[i].second==2)
            min2=min(min2, ab[i].first);
        vis=i;
    }

    if(sum<m) {
        cout<<-1<<endl;
        return;
    }

    if(min2<INF) {
        for(int j=vis+1; j<n; j++) {
            if(ab[j].second==1 && sum-min2+ab[j].first>=m) {
                ans--;
                cout<<ans<<endl;
                return;
            }
        }
        for(int j=0; j<=vis; j++) {
            if(ab[j].second==1 && sum-ab[j].first>=m) {
                ans--;
                cout<<ans<<endl;
                return;
            }
        }
    }

    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
