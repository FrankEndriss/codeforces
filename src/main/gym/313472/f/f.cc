/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* The order of operations does not matter.
 * Each operation is done or not done, never done more than once.
 * So,
 * if a cell is flipped, then it is flipped by using the row XOR using the col.
 * if a cell is not flipped, then none of the row/col is flipped, or both.
 *
 * Consider first row,
 * consider it not flipped.
 *  Then foreach cell that must be flipped, that col must be flipped.
 * consider it flipped
 *  Then foreach cell that must not be flipped, that col must be flipped.
 *
 * After that first row, we need to check if second row must be flipped or not.
 * Then third row...etc.
 */
const int N=1003;
void solve() {
    cini(n);
    vvb a(n,vb(n));
    vvb b(n,vb(n));
    for(int i=0; i<n; i++)  {
        cins(s);
        for(int j=0; j<n; j++)
            a[i][j]=(s[j]=='1');
    }
    for(int i=0; i<n; i++)  {
        cins(s);
        for(int j=0; j<n; j++)
            b[i][j]=(s[j]=='1');
    }

    vb cols(n); /* cols[i]==true -> col i must be flipped */

    /* Consider first row not flipped */
    for(int i=0; i<n; i++) {
        if(a[0][i]!=b[0][i]) {
            cols[i]=true;
        }
    }

    for(int f=0; f<2; f++) {
        bool ok=true;   /* check if we can create all other rows */
        for(int i=1; ok && i<n; i++) {
            vb aa=a[i];
            vb flip=a[i];
            for(int j=0; j<n; j++)
                flip[j]=!flip[j];

            for(int j=0; j<n; j++)
                if(cols[j]) {
                    aa[j]=!aa[j];
                    flip[j]=!flip[j];
                }

            if(aa!=b[i] && flip!=b[i])
                ok=false;
        }
        if(ok) {
            cout<<"YES"<<endl;
            return;
        }


        for(int j=0; j<n; j++)
            cols[j]=!cols[j];
    }

    cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
