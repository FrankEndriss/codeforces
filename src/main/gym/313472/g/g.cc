/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Construct the biggest possible subsets.
 * This is, by adding a number, we can further
 * add only multiples of lcm(choosenNumbers)
 * Note that elements can be multiple times
 * in the array.
 *
 * Since we can only add multiples of lcm,
 * the next lcm allways equals the last 
 * choosen number.
 */
const int N=2e5+7;
void solve() {
    cini(n);
    cinai(a,n);
    vi f(N);
    for(int i=0; i<n; i++) 
        f[a[i]]++;

    vi aa=a;
    sort(all(aa));
    auto it=unique(all(aa));
    aa.resize(distance(aa.begin(), it));
    const int nn=aa.size();

    vi memo(N, -1);
    /* @return max number of chooseable elements if starting at number num */
    function<int(int)> go=[&](int num) {
        if(memo[num]>=0)
            return memo[num];

        //cerr<<"go, num="<<num<<endl;

        int ans=0;
        for(int j=num*2; j<N; j+=num) {
            if(f[j]) {
                ans=max(ans, f[j]+go(j));
            }
        }
        return memo[num]=ans;
    };

    int ans=0;
    for(int i=0; i<nn; i++)
        ans=max(ans, f[aa[i]]+go(aa[i]));
    cout<<n-ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
