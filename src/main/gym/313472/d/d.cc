/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to somehow construct a graph from the pairs.
 * Say max-flow.
 * S -> boys
 * boys -> girls if contained in k
 * girls -> T
 *
 * But that would not give us the number of ways, but the the max pairs we
 * can create.
 **** 
 * Number of ways is if we choose the first pair, how much ways are there
 * to choose the other ones.
 * And same for not choose the first pair.
 * ...But there is no need to construct all pairs.
 * Foreach pair we need to know how much other pairs are there with
 * none of the both parts of the pair.
 * Since each pair exists only once we can simply count them.
 */
void solve() {
    cini(cntA);
    cini(cntB);

    vi cnt(cntA+cntB);
    cini(k);
    cinai(a,k);
    cinai(b,k);
    for(int i=0; i<k; i++) {
        cnt[a[i]-1]++;
        cnt[cntA+b[i]-1]++;
    }

    int ans=0;
    for(int i=0; i<k; i++) {
        ans+=k-(cnt[a[i]-1]+cnt[cntA+b[i]-1]-1);
    }
    ans/=2;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
