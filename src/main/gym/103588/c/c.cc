/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Poor statement text:
 * -Numbers are not in scifi notation, bad readeable
 * -What is the meaning of "Find the minimal number of infected houses". Minimum of what?
 *  -> Seems to be "What is the minimum number of infected houses after x days?"
 *     However, X is missing :/
 *     So it is one day?
 * -Rules of quarantine people are not clear explained. It seems
 *  -We can quarantine each house, infected or not
 *  -But only one house
 *  -If qurantine house is infected, it does not infect adj houses
 *  -If qurantine house if not infected, it does not get infected
 *
 *  Assume problem asks for best strategy for one day.
 *
 *  Each adj house gets infected, some by only one source.
 *  That infections can be eliminated by qua the source of infection.
 *  If no such single source exists, we can save one house by
 *  quarantining it.
 *
 *  If there is an infected house with two adj that are not adj to another
 *  infected house, we can save those two.
 *  Else we can save one.
 *  If all are infected we can save none.
 *  Consider edge cases for first and last house.
 *
 *  -> But that asumptions is not consistent with first sample.
 *  How is ans==7?
 *  If we put house 3 in quarantine, then infected houses are 3,5,6,7,8,9, which are 6. Not 7.
 *  ???
 *  So problem is unclear, also no explanation of samples :/
 *  **********************************
 *  After some announcements:
 *  Quarantine is permanent, so X is the number of days until there are no new
 *  infections, so nothing changes any more.
 *  -We can save a range of houses if they are all uninfected, and we place a qua at
 *   first and last of the range
 *  -Consider also edgecases first range and last range.
 *  -> So translage the list of infected houses to a list of uninfected ranges,
 *     then save one range after the other, sorted by size.
 *
 *  -> Consider a "double" range, where an infected house is the border of two
 *     ranges. Qua such house saves more.
 *  So, on first day, qua some house that is border of two ranges, then on subseq
 *  days try to fix biggest ranges possible, each with two quas.
 *
 *  ...better solve D, it is less complecated.
 *  ************************************
 *  After some more changes:
 *  Now the houses are in a cirlce of size up to 1e9.
 *  Initially each range of uninfected houses shrinks by 2 units per day.
 *  On first and second day we can save a range len-1
 *  third and fourth day, range len-5
 *  ...len-9
 *  etc
 */
void solve() {
    cini(n);
    cini(m);

    cinai(a,m);
    sort(all(a));
    vi d(m);
    for(int i=0; i+1<m; i++)
        d[i]=a[i+1]-a[i]-1;

    d.back()=n-a.back()+(a[0]-1);
    sort(all(d));

    int dec=1;
    int ans=0;
    while(d.size() && d.back()>=dec) {
        if(d.back()==0)
            break;
        int v=max(1LL, d.back()-dec);
        ans+=v;
        dec+=4;
        if(v==1)
            dec--;
        d.pop_back();
    }

    cout<<n-ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
