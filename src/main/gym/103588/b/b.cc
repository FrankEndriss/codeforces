/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Binary search?
 * No.
 * At each position where Luke needs to transform
 * he transforms into a number that allows him
 * to eat as much elements as possible.
 * Since the elements are fixed, just go right
 * until diff of bigest-smallest becomes gt 2*x.
 */
void solve() {
    cini(n);
    cini(x);

    cinai(a,n);
    set<int> v;
    int ans=0;
    for(int i=0; i<n; i++) {
        v.insert(a[i]);
        int d=(*v.rbegin())-(*v.begin());
        if(d>2*x) {
            ans++;
            v.clear();
            v.insert(a[i]);
        } 
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
