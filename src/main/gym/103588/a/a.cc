/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * If all a[i] are prime, all pairs are coprime.
 * Choose first k primes.
 * Subst any number by another, bigger prime.
 * ...But we would need to have all primes up 
 * to 1e9 available :/
 *
 * We must not use any primefactor twice, because then
 * the two numbers using that primefactor would not 
 * be coprime.
 * So, the sol using only primes is correct.
 * How to implement this? 
 * We cannot calc all primes up to 1e9, TLE.
 *
 * Some kind of brute force?
 * Consider k==2, n==7e8+333
 * How to proceed?
 * -> Ok, for k==2 we can brute force gcd() all pairs of two numbers with sum=n.
 *  But how does this work out for k=100, we got like 5000 pairs.
 *
 * Also: parity of n and k must match. But how does this help?
 *
 * ....
 * Can we choose first k-1 primes, then brute force the last number
 * by decrement it...somehow?
 * ****
 * Just use 1 foreach number, and the last the diff to n?
 * -> If this is the answer, it is a badly stupid problem. The only way to not solve it 
 *    is to misunderstand the problem statement.
 */
void solve() {
    cini(n);
    cini(k);

    if(n<k)
        cout<<-1<<endl;
    else {
        for(int i=0; i+1<k; i++) 
            cout<<"1 ";
        cout<<n-(k-1)<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
