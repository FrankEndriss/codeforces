/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * In both operations a[i] and a[j] get decremented.
 * The only difference is the increment 
 * of  a[j+1] if type1,
 * and a[j+2] in type2.
 *
 * Consider the first element of each array.
 * We can tell how much operations each array received on leftmost position,
 * since only a decrement is possible.
 * We could undo that operations. The do the same on next position, and on third 
 * position.
 * And then?
 *
 * Also we can consider the last element, here more or less same rule applies.
 * Last element can only be incremented, so we can tell how much
 * operations where done on it. 
 * But how does this help?
 */
const int INF=1e9+7;
void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));

    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++)
            cin>>a[i][j];

    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
