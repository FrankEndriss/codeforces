/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It seems to be simply the max possible sum of a[i] in all paths.
 * So do kind of dijkstra.
 * ...
 * No.
 * Consider a vertex with two incoming edges.
 * It will be incremented by both.
 *
 * So, each vertex 
 * -if no incoming decrements
 * -if one incoming stays same
 * -if more incoming increments
 *
 * So, operate in rounds.
 * In each round, update each vertex according to its inputs and outputs.
 * Note that vertex with no input get 0 by this.
 * Remove all 0 vertex.
 *
 * From all vertex that are leafs, we need to find the one with smallest a[i].
 * Then simulate a[i] steps, which makes that vertex get removed.
 * Repeat.
 *
 * ***
 * But something wrong :/
 * -> There can be vertex with initail a[i]=0
 *  How to handle that?
 *  -> Just skip decrement and increment while simulate.
 *  No...
 *  As long as there are vertex with a[i]=0 we need to simulate a single step at a time.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    vector<int> a(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        a[i]=aux;
    }

    vvi adj(n);
    vi icnt(n);  /* incoming count */
    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        adj[u].push_back(v);
        icnt[v]++;
    }

    vb vis(n);
    int ans=0;
    while(true) {
        /* find smallest leaf */
        int mia=INF;
        int l=-1;
        int mialla=INF;
        for(int j=0; j<n; j++) {
            if(!vis[j]) {
                mialla=min(mialla, a[j]);
            }
            if(!vis[j] && icnt[j]==0 && a[j]<mia) {
                mia=a[j];
                l=j;
            }
        }

        if(l<0)
            break;

        if(mialla==0)
            mia=1;

        assert(l>=0);
        ans=(ans+mia)%998244353;

        /* simulate mia steps */
        auto aa=a;
        for(int j=0; j<n; j++) {
            if(vis[j] || aa[j]==0)
                continue;

            a[j]-=mia;
            for(int chl : adj[j])
                a[chl]+=mia;
        }

        if(a[l]==0) {
            for(int chl : adj[l])
                icnt[chl]--;
            vis[l]=true;
        }
    }

    int lans=(int)ans;
    cout<<lans<<endl;

}

signed main() {
    solve();
}
