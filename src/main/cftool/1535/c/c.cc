/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* Find foreach position the number of beautiful substrings ending at that pos.
 * How?
 * At each position we can add all previous strings that do not include
 * two '0' on different parity indexes or two '1' on different parity indexes.
 */
void solve() {
    cins(s);
    string t=s;

    int ans=0;
    vvi dp(2, vi(2, -1));   /* dp[i][j][k]=index of prev occ of i=0 even i=1 odd j='0' or j='1'
                               k=0 last occ, k=1 prev of last occ
    */
    int prev=-1;
    for(int i=0; i<(int)s.size(); i++) {
        if(s[i]=='0'||s[i]=='1') {
            dp[i%2][s[i]-'0']=i;
            prev=max({prev, dp[i%2][!(s[i]-'0')], dp[!(i%2)][s[i]-'0']});
        }

        ans+=abs(i-prev);
    }
    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
