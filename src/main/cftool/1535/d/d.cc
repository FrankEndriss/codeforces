/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* complecated construction...
 * Each team without a 0 on its path to root can win.
 * So count them initially, and then?
 *
 * We can solve it with a lazy segment tree. Store foreach position
 * the number of 0s in the path to root, and a 1 if the number of
 * 0s eq 0.
 * On update increment the number of 0s on each covered position,
 * and set the flag...no.
 * nonono...
 *
 * Consider a 0 to be 01 and a 1 to be 10, a '?' to be 11.
 * So a 0 "removes" all teams down that subtree, a 1 "adds" them.
 *
 * So, on each update we need to recalc from that vertex up to root.
 *
 * We need to map indexes to an adj matrix, kind of like in a segment tree,
 * but we need to reverse s to do so...
 */
void solve() {
    cini(k);
    cins(s);
    assert(s.size()==(1<<k)-1);

    vvi adj(1<<k);

    for(int 

    vi dp
}

signed main() {
    solve();
}
