/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* It seems obvious to sort a[] non descending.
 * Then simply count the pairs?
 * Does not work.
 *
 * So, what?
 * Move all even element left, then sort the two
 * even/odd halfs non descending.
 * How to prove?
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi e;
    vi o;
    for(int i=0; i<n; i++) 
        if(a[i]%2)
            o.push_back(a[i]);
        else
            e.push_back(a[i]);

    sort(all(e));
    sort(all(o));

    a.clear();
    for(int i : e)
        a.push_back(i);
    for(int i : o)
        a.push_back(i);

    int ans=0;
    for(int i=0; i<n; i++) 
        for(int j=i+1; j<n; j++)
            if(gcd(a[i],2*a[j])>1)
                ans++;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
