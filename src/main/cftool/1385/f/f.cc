/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* It is just "implement simulation",
 * and count.
 */
void solve() {
    cini(n);
    cini(k);

    vector<set<int>> adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--; 

        adj[u].insert(v);
        adj[v].insert(u);
    }

    vvi leafs(n);

    for(int i=0; i<n; i++) {
        if(adj[i].size()==1)  {
            int p=*adj[i].begin();
            leafs[p].push_back(i);
        }
    }

    queue<int> q; 
    for(int p=0; p<n; p++) {
        if(leafs[p].size()>=k)
            q.push(p);
    }

    int ans=0;
    while(q.size()) {
        int p=q.front();
        q.pop();
        if(leafs[p].size()<k || adj[p].size()<k)
            continue;
        
        ans++;
        for(int i=0; i<k; i++) {
            adj[p].erase(leafs[p].back());
            adj[leafs[p].back()].erase(p);
            leafs[p].pop_back();
        }

        if(leafs[p].size()>=k)
            q.push(p);
        else if(leafs[p].size()==0 && adj[p].size()==1) {
            int pp=*adj[p].begin();
            leafs[pp].push_back(p);
            if(leafs[pp].size()==k)
                q.push(pp);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
