/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* permutation is possible if one end of
 * the pairs can create a permutation,
 * and the other end would do, too.
 * Then count the cheaper of both posible configurations.
 * -> Which does not work, see second example:
 *  5 3 5 1 4
 *  1 2 3 2 4
 *  The point is, the existence of pair 5,3 determines that exactly of
 *  the pairs 5,1 or 3,2 must be swapped.
 *
 *  So, we can create chains of dependecies:
 *  Start with first pair:
 *  the 5 in first determines that the other pair 5,3 is swapped. So we
 *  get a 3 there. The 3 determines that the pair 2,3 is swapped, so we
 *  get a 2 there. The 2 determines that the pair 1,2 is not swapped, so we
 *  get a 1 there. The 1 was the last number.
 *  Just count (and maintain) the swaps, ans=min(cnt,n-cnt).
 */
void solve() {
    cini(n);
    vector<pii> a(n);
    vvi pos(n);

    for(int i=0; i<n; i++) {
        cin>>a[i].first;
        a[i].first--;
        pos[a[i].first].push_back(i);
    }

    for(int i=0; i<n; i++) {
        cin>>a[i].second;
        a[i].second--;
        pos[a[i].second].push_back(i);

    }

    for(int i=0; i<n; i++) {
        if(pos[i].size()!=2) {
            cout<<"-1"<<endl;
            return;
        }
    }

    function<int(int,int)> otherPos=[&](int lpos, int num) {
        if(pos[num][0]==lpos) {
            return pos[num][1];
        } else {
            assert(pos[num][1]==lpos);
            return pos[num][0];
        }
    };

    vi sw;  // indexes of swapped pairs
    vb vis(n);  // fixed numbers in first

    for(int i=0; i<n; i++) {
        if(a[i].first==a[i].second)
            vis[a[i].first]=true;
    }

    /* now fix all numbers in first, swap where it is needed to swap */
    for(int i=0; i<n; i++) {
        if(vis[a[i].first])
            continue;

        int ii=i;
        set<int> sw1;
        set<int> sw2;
        sw2.insert(ii);

        while(!vis[a[ii].first]) {
            vis[a[ii].first]=true;
            int nextPos=otherPos(ii, a[ii].first);
            if(a[nextPos].first==a[ii].first) {
                sw1.insert(nextPos);
                swap(a[nextPos].first, a[nextPos].second);
            } else
                sw2.insert(nextPos);
            ii=nextPos;
        }

//cerr<<"sw1.size()="<<sw1.size()<<" sw2.size()="<<sw2.size()<<endl;
        if(sw2.size()<sw1.size())
            sw1.swap(sw2);

        copy(all(sw1), back_inserter(sw));
    }

    sort(all(sw));
    cout<<sw.size()<<endl;
    for(int i : sw)
        cout<<i+1<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
