/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We would have to find if there are loops in the graph with 
 * directed edges.
 * if yes, ans=no;
 *
 * Find some root in the tree by traversing 'backwards'.
 * Then dfs from these roots, directing the undirected edges.
 * Then check for cycles.
 *
 * ...idk :/
 */
void solve() {
    cini(n);
    cini(m);

    vector<set<int>> undir(n);
    vvi adj(n);
    vvi adjr(n);

    for(int i=0; i<m; i++) {
        cini(t);
        cini(u);
        cini(v);
        u--;
        v--;
        if(t==1) {
            adj[u].push_back(v);
            adjr[v].push_back(u);
        } else {
            undir[u].insert(v);
            undir[v].insert(u);
        }
    }
cerr<<"parsed"<<endl;

    vb vis(n);
    function<bool(int)> dfs1=[&](int v) {
cerr<<"dfs1, v="<<v<<endl;
        for(int u : undir[v]) {
            adj[v].push_back(u);
            undir[u].erase(v);
        }
cerr<<"dfs1, after undir"<<endl;

        vis[v]=true;

        for(int idx=0; idx<adj[v].size(); idx++) {
            int c=adj[v][idx];
            if(vis[c])
                return true;

            if(dfs1(c))
                return true;
        }
        return false;
    };

    for(int i=0; i<n; i++) {
        if(adjr[i].size()==0) {
            vis.assign(n, false);
            if(dfs1(i)) {
                cout<<"NO"<<endl;
                return;
            };
        }
    }

    cout<<"YES"<<endl;
    for(int i=0; i<n; i++) {
        for(int c : adj[i]) 
            cout<<i+1<<" "<<c+1<<endl;
    }
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
