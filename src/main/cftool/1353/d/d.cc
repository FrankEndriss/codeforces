/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

void solve() {
    cini(n);

    vi a(n);

    int i=1;

    struct mycmp {
        bool operator()(const pii p1, const pii p2) {
            if(p2.second-p2.first!=p1.second-p1.first)
                return p1.second-p1.first < p2.second-p2.first;
            return p2.first<p1.first;
        }
    };

//cerr<<"n="<<n<<endl;
    priority_queue<pii, vector<pii>, mycmp> q;
    q.push({1,n});
    while(q.size()) {
        int l=q.top().first;
        int r=q.top().second;
        q.pop();
//cerr<<"l="<<l<<" r="<<r<<endl;
        if(l>r)
            continue;

        if(l==r)  {
            a[l-1]=i;
            i++;
            continue;
        }

        if((r-l+1)&1) {
            int idx=(r+l)/2;
            a[idx-1]=i;
            if(l<=idx-1)
                q.push({l,idx-1});
            if(idx+1<=r)
                q.push({idx+1,r});
        } else {
            int idx=(l+r-1)/2;
            a[idx-1]=i;
            if(idx+1<=r)
                q.push({idx+1,r});
            if(l<=idx-1)
                q.push({l,idx-1});
        }
        i++;

    }

    for(int j=0; j<n; j++)
        cout<<a[j]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);    
    while(t--)
        solve();
}

// DO NOT JUMP BETWEEN PROBLEMS
