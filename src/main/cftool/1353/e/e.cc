/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* 
 * A garland is completly defined by index of first
 * and last burning lamp.
 *
 * ********
 * Consider the k-position separate.
 *
 * 1. Each lamp not in the current k-position must be turned 
 * off, so each k-position has an initial cost.
 *
 * 2. In each k-position we can remove (switch off) lamps
 * from both ends, or turn on some lamps.
 *
 * 3. At each position (from left) there is a number of lamps we
 * would have to turn on to make the seq correct, and a number we would
 * have to turn off to make the seq correct.
 * Same from right.
 *
 * 4. Ans is at the position where sum of numbers from both ends is 
 * minimum.
 *
 * Consider the dp:
 * Going from left to right we have 3 parts of the garland:
 * leading 0, middle 1, tail 0
 * At each position consider it as start of one of the three parts.
 * Then final ans=min(dp[n]);
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);
    cins(s);

    vi f1(k);
    for(int i=0; i<n; i++)  {
        if(s[i]=='1')
            f1[i%k]++;
    }

    int sumF1=accumulate(f1.begin(), f1.end(), 0LL);
    int ans=INF;
    for(int i=0; i<k; i++) {
        vi dp(3);
        for(int j=i; j<n; j+=k) {
            vi dp0(3);
            if(s[j]=='0') {
                dp0[0]=dp[0];
                dp0[1]=min(dp[0]+1, dp[1]+1);
                dp0[2]=min({dp[0], dp[1], dp[2]});
            } else if(s[j]=='1') {
                dp0[0]=dp[0]+1;
                dp0[1]=min(dp[0], dp[1]);
                dp0[2]=min({dp[0], dp[1], dp[2]})+1;
            }
            dp=dp0;
        }

        ans=min(ans, min({dp[0], dp[1], dp[2]})+sumF1-f1[i]);
    }

    cout<<ans<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

