/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* the grid builds components.
 * there are costs to connect one
 * component with the other.
 * We have to find the min connection
 * cost path from componentStart to componentEnd.
 *
 * nonono...
 * We need to implement some dijkstra.
 * To step into a cell we need to pay.
 * if cell is higher than ours, the diff of hights, else
 * the diff mult by current pathlen.
 * But how to consider the change of hights?
 */
void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));
    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++) {
            cin>>a[i][j];
        }
    }
    int a0=a[0][0];
    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++) {
            a[i][j]-=a0;
        }
    }

    const int INF=1e18;
    vvvi dp(n, vvi(m, vi(n+m, INF))); /* dp[i][j][k]=min const to get to dp[i][j] of hight k */

    queue<pair<pii, pii>> q;
    q.push({{0,0},{0,0}});
    dp[0][0][0]=0;

    while(q.size()) {
        pii p=q.front().first;
        int h=q.front().second.first;
        int c0=q.front().second.second;
        q.pop();

        for(pii p1 :  {
                    make_pair( p.first+1, p.second), make_pair( p.first, p.second+1)
                }) {

            if(p1.first<n && p1.second<n) {
                int c1=0;
                int h1=h+1;

                if(a[p1.first][p1.second]>=h+1)
                    c1=a[p1.first][p1.second]-(h+1)+c0;
                else {
                    c1=((h+1)-a[p1.first][p1.second])*(p.first+p.second);
                    h1=a[p1.first][p1.second];
                }

                if(c1<dp[p1.first][p1.second][h1]) {
                    dp[p1.first][p1.second][h1]=c1;
                    q.push({{p1.first,p1.second},{h1,c1}});
                }
            }
        }

    }

    int ans=1e18;
    for(int i=0; i<n+m; i++) {
        ans=min(ans, dp[n-1][m-1][i]);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// DO NOT JUMP BETWEEN PROBLEMS
