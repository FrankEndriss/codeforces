/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cini(k);
    cinai(a,n); 
    cinai(b,n);
    sort(a.begin(), a.end());
    sort(b.begin(), b.end(), greater<int>());

    int idx=0;
    int ans=0;
    for(int i=0; i<n; i++) {
        if(a[i]<b[idx] && k) {
            a[i]=b[idx];
            k--;
            idx++;
        }
        ans+=a[i];
    }
    cout<<ans<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// DO NOT JUMP BETWEEN PROBLEMS
