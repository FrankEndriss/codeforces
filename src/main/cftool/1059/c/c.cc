/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we want to remove elements so 
 * that asap gcd changes.
 * So remove all odd numbers, and it changes to 2.
 * Repeat.
 * On second iteration remove all not div by 4.
 * Then 8 etc.
 *
 * n==3 is edge case
 */
void solve() {
    cini(n);

    vi a(n);
    iota(a.begin(), a.end(), 1);

    int mul=1;
    while(a.size()) {
        if(a.size()==3) {
            cout<<mul<<" "<<mul<<" "<<a[2];
            break;
        }

        vi b;
        for(int i : a) {
            if(i%(mul*2))
                cout<<mul<<" ";
            else
                b.push_back(i);
        }
        mul*=2;
        a.swap(b);
    }
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

