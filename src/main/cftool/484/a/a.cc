/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * If r has more bits than l then it
 * is the biggest number with all bits set.
 * This is r if l==2^y-1, else 2^(y-1)-1
 *
 * If l and r have same highest bit, that bit
 * is set in ans, and further ignore it.
 */
void solve() {
    cini(n);
    for(int i=0; i<n; i++) {
        cini(l);
        cini(r);

        int ans=0;
        for(int j=61; j>=0; j--) {
            int b=1LL<<j;
            if((r&b) && !(l&b)) {
                if(r==(b<<1)-1)
                    ans|=r;
                else if(b>1)
                    ans|=(b-1);
                else
                    ans|=1;
                break;
            } else if((r&b) && (l&b)) {
                ans|=b;
                r-=b;
                l-=b;
            }
        }

        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
