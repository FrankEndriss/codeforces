/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;


/* 
 * The problem text is bad english.
 * So, the string should become RBS, but in one action
 * we can only replace open by open bracket, and close by close.
 * So we need to count open/close, and every close matches
 * exactly one open bracket.
 * If those open/close-pairs do not match we need to add on
 * replace action.
 */
void solve() {
    cins(s);

    stack<char> st;
    int ans=0;
    for(char c : s) {
        if(c=='{' || c=='(' || c=='[' || c=='<')
            st.push(c);
        else {
            if(st.size()==0) {
                cout<<"Impossible"<<endl;
                return;
            }
            char cl=st.top();
            st.pop();
            if(cl=='{' && c!='}')
                ans++;
            else if(cl=='(' && c!=')')
                ans++;
            else if(cl=='<' && c!='>')
                ans++;
            else if(cl=='[' && c!=']')
                ans++;
        }
    }
    if(st.size()>0) {
        cout<<"Impossible"<<endl;
        return;
    }
    cout<<ans<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

