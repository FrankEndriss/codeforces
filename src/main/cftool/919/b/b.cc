/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void inc(vi& a) {   
    int carry=1;
    for(int i=0; carry && i<a.size(); i++) {
        a[i]+=carry;
        if(a[i]==10)
            a[i]=0;
        else
            carry=0;
    }
    if(carry)
        a.push_back(1);
}

/*
 * We can simply brute force that number.
 */
void solve() {
    cini(n);

    int cnt=0;
    vi a(1);
    int d=0;

    while(cnt<n) {
        inc(a);
        
        d=accumulate(all(a), 0);
        if(d<=10) {
            cnt++;
/*
cerr<<cnt<<" ";
for(int i=a.size()-1; i>=0; i--)
    cerr<<a[i];
cerr<<" "<<10-d<<endl;
*/
        } else
            a[0]=9; // speedup incrementation
    }

    
    reverse(all(a));
    a.push_back(10-d);
    int ans=0;
    for(int i : a) {
        ans*=10;
        ans+=i;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
