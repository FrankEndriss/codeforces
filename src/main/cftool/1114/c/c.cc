/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*  We need to find the primefactors of b.
 *  The biggest primefactor is the limiting one, the number
 *  of 0 is the number of times that factor is used in n!
 *
 *  Wrong! It is not the biggest one. It can be any one. We need to find
 *  how much of the primefactors "are" in n!, then devide by the 
 *  number of times it is needed for one b.
 *  So anyone of the primefactors can be the limiting one, ans is smallest
 *  of those numbers.
 *
 *  Note that we need to care for 32-bit overflow.
 */
void solve() {
    cini(n);
    cini(b);

    int bb=b;
    map<int,int> d;
    for(int i=2; i*i<=bb; i++) {
        while(bb%i==0)  {
            d[i]++;
            bb/=i;
        }
    }
    if(bb>1)
        d[bb]++;

    int ans=1e18;
    for(auto ent : d) {
        int nn=n;
        int lans=0;
        while(nn) {
            nn/=ent.first;
            lans+=nn;
        }
        ans=min(ans, lans/ent.second);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
