/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int INF=1e18;
/* dp
 * dp[i][j]=min moves to ff interval (i,j)
 * ...TLE
 *
 * We need to find the longest subsequence-palindrome, and
 * start from middle of it.
 * So we have plen/2 moves where two gems instead of one are integrated.
 * How to find that polindrome?
 * We can dp from "outsides" find(i,j)
 */
void solve() {
    cini(n);
    cinai(aa,n);
    vi a;   /* compressed */
    a.push_back(aa[0]);
    for(int i=1; i<n; i++) 
        if(aa[i]!=aa[i-1])
            a.push_back(aa[i]);

/* dp[i][j]= max half palin len for subarray (i,j); */
    vvi dp(a.size()+1, vi(a.size()+1, -1));

    function<int(int,int)> calc=[&](int i, int j) {
        if(i>=j)
            return 0LL;

        if(dp[i][j]>=0)
            return dp[i][j];

        if(a[i]==a[j])
            dp[i][j]=max(dp[i][j], 1+calc(i+1, j-1));

        dp[i][j]=max(dp[i][j], calc(i+1, j));
        dp[i][j]=max(dp[i][j], calc(i, j-1));
        
        return dp[i][j];
    };

    int ans=calc(0,a.size()-1);
    cout<<a.size()-ans-1<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
