/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int idx(vector<pii>& v, pii p) {
    int ans=-1;
    for(size_t i=0; i<v.size(); i++)
        if(v[i]==p)
            ans=i;
    return ans;
}

void norm(int& x, int& y) {
        if(x==0)
            y/=abs(y);
        else if(y==0)
            x/=abs(x);
        else {
            int g=gcd(x,y);
            x/=g;
            y/=g;
        }
}

bool isRect(vector<pii>& v) {
    sort(v.begin(), v.end());

    do {
        int dx=v[1].first-v[0].first;
        int dy=v[1].second-v[0].second;
        norm(dx,dy);
//cerr<<"isRect, dx="<<dx<<" dy="<<dy<<endl;

        int dx2=v[2].first-v[1].first;
        int dy2=v[2].second-v[1].second;
        
        int dx3=v[3].first-v[0].first;
        int dy3=v[3].second-v[0].second;

        if(dx2==dx3 && dy2==dy3) {
            norm(dx2,dy2);
            if(dx2==-dy && dy2==dx) {
                return true;
            }
        }
        
    }while(next_permutation(v.begin(), v.end()));

    return false;
}

/* If any 4 points form a square
 * it is sufficient to check if
 * the other 4 form a rect.
 */
const int INF=1e9;
void solve() {
    vector<pii> p(8);
    for(int i=0; i<8; i++)
        cin>>p[i].first>>p[i].second;
    
    for(int i=0; i<8; i++)
        for(int j=i+1; j<8; j++) {
            int dx=p[j].first-p[i].first;
            int dy=p[j].second-p[i].second;

            int idx1=idx(p, { p[j].first-dy, p[j].second+dx});
            int idx2=idx(p, { p[i].first-dy, p[i].second+dx});

            if(idx1>=0 && idx2>=0) {
//cerr<<"found sq, i="<<i<<" j="<<j<<" idx1="<<idx1<<" idx2="<<idx2<<endl;
                vector<pii> v2=p;
                v2[i]={INF,INF};
                v2[j]={INF,INF};
                v2[idx1]={INF,INF};
                v2[idx2]={INF,INF};
                sort(v2.begin(), v2.end());
                v2.resize(4);
                if(isRect(v2)) {
                    cout<<"YES"<<endl;
                    cout<<i+1<<" "<<j+1<<" "<<idx1+1<<" "<<idx2+1<<endl;
                    set<int> ans;
                    for(int k=1; k<=8; k++) 
                        ans.insert(k);
                    ans.erase(i+1);
                    ans.erase(j+1);
                    ans.erase(idx1+1);
                    ans.erase(idx2+1);
                    for(int k : ans)
                        cout<<k<<" ";
                    cout<<endl;
                    return;
                }
            }
        }
    

    cout<<"NO"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
