/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* No need to consider unliked books.
 * From liked books 3 kinds
 * A, B, AB
 * cnt(A)+cnt(AB)>k && cnt(B)+cnt(AB)>k
 *
 * dp all ab and find the time for according a and b with binsearch.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    vi t(n);
    vi a(n);
    vi b(n);

    vi bab;  /* books liked by both */
    vi ba;  /* only a */
    vi bb; /* only b */

    for(int i=0; i<n; i++) {
        cin>>t[i]>>a[i]>>b[i];
        if(a[i]&&b[i]) {
            bab.push_back(i);
        } else if(a[i])
            ba.push_back(i);
        else if(b[i])
            bb.push_back(i);
    }

    auto cmp=[&](int i1, int i2) {
        return t[i1]<t[i2];
    };

    sort(all(bab), cmp);
    sort(all(ba), cmp);
    sort(all(bb), cmp);

    vi prebab(bab.size()+1);  /* prefix time bab */
    for(int i=0; i<bab.size(); i++)
        prebab[i+1]=prebab[i]+t[bab[i]];

    vi preba(ba.size()+1);  /* prefix time ba */
    for(int i=0; i<ba.size(); i++)
        preba[i+1]=preba[i]+t[ba[i]];

    vi prebb(bb.size()+1);  /* prefix time bb */
    for(int i=0; i<bb.size(); i++)
        prebb[i+1]=prebb[i]+t[bb[i]];

    const int INF=1e18;
    int ans=INF;
    vi anslist;

    for(int i=0; i<prebab.size(); i++) {
        vi lanslist;
        int t1=prebab[i];

        int need=m-i;
        if(k-i<preba.size() && k-i<prebb.size() && k-i>=0) {
            t1+=preba[k-i]+prebb[k-i];
            need-=(k-i)*2;
        }

        /* find min time to read need more books from all 3 stacks.
         * use "triple" pointer. */
        size_t ibab=i;
        size_t iba=k-i;
        size_t ibb=k-i;
        cerr<<"need="<<need<<endl;
        while(need>0) {
            if(ibab<bab.size() &&
                    (iba==ba.size() || t[bab[ibab]]<=t[ba[iba]]) &&
                    (ibb==bb.size() || t[bab[ibab]]<=t[bb[ibb]])) {
                t1+=t[bab[ibab]];
                ibab++;
                lanslist.push_back(bab[ibab]);

            } else if(iba<ba.size() &&
                      (ibab==bab.size() || t[ba[iba]]<=t[bab[ibab]]) &&
                      (ibb==bb.size() || t[ba[iba]]<=t[bb[ibb]])) {
                t1+=t[ba[iba]];
                iba++;
                lanslist.push_back(ba[iba]);
            } else if(ibb<bb.size() &&
                      (ibab==bab.size() || t[bb[ibb]]<=t[bab[ibab]]) &&
                      (iba==ba.size() || t[bb[ibb]]<=t[ba[iba]])) {
                t1+=t[bb[ibb]];
                ibb++;
                lanslist.push_back(bb[ibb]);
            } else {
                t1=INF;
                break;
            }
            need--;
        }
        if(t1<ans) {
            ans=t1;
            anslist.clear();
            for(int j=0; j<i; j++)
                anslist.push_back(bab[j]);
            for(int j : lanslist)
                anslist.push_back(j);
        }
    }

    if(ans==INF)
        cout<<-1<<endl;
    else {
        cout<<ans<<endl;
        for(int i : anslist)
            cout<<i<<" ";
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
