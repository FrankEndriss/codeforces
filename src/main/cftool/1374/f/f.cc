/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* call it a triple swap.
 * can we do a swap with two (or more) triple swap?
 * think not.
 * so simple greedy :/
 * note that the a[i] are not distinct!
 *
 * So if we do not end well, we can somewhere in between the
 * swaps do one swap with two equal elements.
 * Which will in the end make the last two elements switched.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi aa=a;
    sort(all(aa));

    int ans=0;
    vi ansv;
    function<void(int)> tswap=[&](int i) {
        ans++;
        ansv.push_back(i);
        int tmp=a[i+2];
        a[i+2]=a[i+1];
        a[i+1]=a[i];
        a[i]=tmp;
    };

    for(int i=0; i+2<n; i++) {
        /* find index of next element */
        int idx=i;
        while(aa[i]!=a[idx])
            idx++;

//cerr<<"next element at idx="<<idx<<endl;

        /* move a[idx] to position i */
        while(idx-i>=2) {
            tswap(idx-2);
            idx-=2;
        }
        if(idx==i+1) {
            tswap(i);
            tswap(i);
        }
    }
//cerr<<"fini"<<endl;

    if(a[n-2]<=a[n-1]) {
        cout<<ans<<endl;
        for(int i : ansv)
            cout<<i+1<<" ";
        cout<<endl;
    } else 
        cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
