/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* elevator is two times at s:
 * (2*m)*y + s
 * (2*m)*y + m + m-s
 * Find the smallest one which is bgeq t.
 */
void solve() {
    cini(n);
    cini(m);    // floors

    for(int i=0; i<n; i++) {
        cini(s);
        cini(f);
        cini(t);
        s--;
        f--;

        if(s==f) {
            cout<<t<<endl;
            continue;
        }

        int t1=t%(2*(m-1));
        int t2=t-t1;
        bool up=true;
        int ans=t2;

        if(t1<=s) {     // way up
            ans+=s;
            up=true;
        } else if(t1<=m-1+m-1-s) {        // way down
            ans+=m-1+m-1-s;
            up=false;
        } else {        // way up
            ans+=m-1+m-1+s;
            up=true;
        }

        if(up) {
            if(f>s)
                ans+=f-s;
            else
                ans+=m-s-1+m-f-1;
        } else {
            if(f<s)
                ans+=s-f;
            else
                ans+=s+f;
        }
        cout<<ans<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

