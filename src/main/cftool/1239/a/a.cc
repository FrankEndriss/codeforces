/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it)
{}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<vvvi> vvvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

/* for some reason answer is fib(n)+fib(m)-1
 * see https://codeforces.com/blog/entry/70720
 * for a poor explanation.
 */
void solve() {
    cini(n);
    cini(m);

    int nval=1;
    if(n==2)
        nval=2;

    int mval=1;
    if(m==2)
        mval=2;

    int prev=2;
    int pprev=1;
    for(int i=3; i<=max(n,m); i++) {
        int val=(prev+pprev)%MOD;
        if(i==n)
            nval=val;
        if(i==m)
            mval=val;

        pprev=prev;
        prev=val;
    }

    nval+=mval;
    nval%=MOD;

    nval--;
    if(nval<0)
        nval+=MOD;

    nval+=nval;
    nval%=MOD;

    cout<<nval<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

