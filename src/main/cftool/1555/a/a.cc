/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 8 slice and 10 sclice is same, 5/2 minute per slice.
 *
 * 15/6
 * 10/6
 * So 6 slices is fastest, not 10.
 */
const int INF=1e18;
void solve() {
    cini(n);

    int ans=INF;
    for(int i=0; i<100; i++) {   /* cnt of 8slc pizzas */
        for(int j=0; j<5; j++) { /* cnt of 10slc pizzas */
            int lans=i*20+j*25;
            int nn=n-i*8-j*10;
            if(nn>0) {
                lans+=((nn+5)/6)*15;
            }
            if(lans<ans) {
                ans=lans;
                //cerr<<"n="<<n<<" i="<<i<<" j="<<j<<" lans="<<lans<<endl;
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
