/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to consider both axes separate.
 * Then move so that the bigger gap gets big enough.
 *
 * But there are cases:
 * We can move only x
 * We can move only y
 * We cannot move enough
 * Note that we never have to move both axes.
 */
const int INF=1e9;
void solve() {
    cini(w);
    cini(h);

    cini(x1);
    cini(y1);
    cini(x2);
    cini(y2);

    cini(xx);
    cini(yy);

    if(xx>w || yy>h) {  /* does not fit at all */
        cout<<-1<<endl;
        return;
    }

    int dy1=h-max(y1,y2);   /* gap at top */
    int dy2=min(y1,y2);     /* gap at bottom */
    int dx1=w-max(x1,x2);   /* gap right */
    int dx2=min(x1,x2);     /* gap left */

    if(dy1>=yy || dy2>=yy || dx1>=xx || dx2>=xx) {
        cout<<0<<endl;
        return;
    }

    int ans=INF;
    if(xx-dx1<=dx2)
        ans=min(ans, xx-dx1);

    if(xx-dx2<=dx1)
        ans=min(ans, xx-dx2);

    if(yy-dy1<=dy2)
        ans=min(ans, yy-dy1);

    if(yy-dy2<=dy1)
        ans=min(ans, yy-dy2);

    if(ans==INF)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
