/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* not palindrome -> beautiful
 * only 3 letters, so there are 6 permutations
 *
 * How long can a beautiful string be?
 * Not two consecutive chars
 * Consider the first both chars, all pairs possible.
 * The third char must be the other one.
 * Then the first must follow
 * Then the second must follow
 * Then the third must follow
 * ...ans so on
 * abcabc
 *
 * So we simply count the hits/misses in the 6 permutations,
 * min of that is ans.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cins(s);

    string abc="abc";

    vvi dp(6, vi(n+1));   /* dp[i][j]=ith perm prefix sum of wrong chars */
    int i=0;
    do {
        for(int j=0; j<n; j++) 
            if(s[j]==abc[j%3])
                dp[i][j+1]=dp[i][j];
            else
                dp[i][j+1]=dp[i][j]+1;

        i++;
    }while(next_permutation(all(abc)));

    for(i=0; i<m; i++) {
        cini(l);
        l--;
        cini(r);
        int ans=INF;
        for(int j=0; j<6; j++)
            ans=min(ans, dp[j][r]-dp[j][l]);
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}
