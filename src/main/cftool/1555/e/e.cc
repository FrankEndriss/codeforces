/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}


/* This is the neutral element */
const int INF=1e9;
S st_e() {
    return INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return x+f;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**  Order segments by w[i]
 *  foreach i remove the prefix left of i.
 *  Remove the max postfix right of some j
 *  ans=min(ans, w[j]-w[i])
 *
 *  How to find the j foreach i?
 *
 *  We need to precalc foreach point the w[j] of segments covering it.
 *  So the state of each point is the list of w[j] of the segments at that point, and
 *  on query we return the min of them (or -1 if empty).
 *
 *  Then we need to remove increasing w[i], and find foreach not removed w[i] the
 *  min w[j] so that all points are still covered.
 *
 *  ****
 *  We can act like two pointer:
 *  Sort segments by w[i]
 *  Add segments until all are covered, then
 *  while(true) 
 *     remove segments from left until not covered
 *     add segemnts from right until covered
 */
void solve() {
    cini(n);
    cini(m);

    //cerr<<"n="<<n<<" m="<<m<<endl;

    vi dp(m-1);
    stree seg(dp);   /* seg[i]=number of segments at point i */   

    vector<signed> l(n);
    vector<signed> r(n);
    vector<signed> w(n);
    for(int i=0; i<n; i++) {
        cin>>l[i]>>r[i]>>w[i];
        l[i]--;
        r[i]--;
    }


    vi id(n);
    iota(all(id), 0LL);
    sort(all(id), [&](int i1, int i2) {
            return w[i1]<w[i2];
    });

    signed idxL=0;
    signed idxR=0;
    signed ans=INF;

    while(true) {

        while(idxR<n && seg.all_prod()<1)  {
            seg.apply(l[id[idxR]], r[id[idxR]], 1LL);
            idxR++;
        }

        if(seg.all_prod()<1)
            break;

        //cerr<<"idxL="<<idxL<<" idxR="<<idxR<<endl;
        assert(idxR>idxL);
        ans=min(ans, w[id[idxR-1]]-w[id[idxL]]);

        seg.apply(l[id[idxL]], r[id[idxL]], -1LL);
        idxL++;
    }

    if(ans==INF)
        ans=-1;

    cout<<ans<<endl;
}

signed main() {
    solve();
}
