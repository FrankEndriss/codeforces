/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Alice leaves a prefix in row 2, and a postfix in row1.
 *
 * Bob can collect whole row1, or whole row2.
 * So Alice optimizes by leaving the min max(row1,row2)
 */
const int INF=1e18;
void solve() {
    cini(m);

    vvi a(2, vi(m));
    for(int i=0; i<2; i++) 
        for(int j=0; j<m; j++) 
            cin>>a[i][j];

    vi post(m+1);
    for(int i=m-1; i>=0; i--)
        post[i]=post[i+1]+a[0][i];

    vi pre(m+1);
    for(int i=1; i<=m; i++) 
        pre[i]=pre[i-1]+a[1][i-1];

    int ans=INF;
    for(int i=0; i<m; i++) {
        ans=min(ans, max(pre[i], post[i+1]));
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
