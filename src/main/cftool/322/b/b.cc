/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Greedy: Create mixed bouqets while
 * every color is available.
 * Then single color bouqs.
 * Then a last mixed one if possible.
 */
void solve() {
    cinai(a,3);
    sort(a.begin(), a.end());

    int ans=max(0LL, a[0]-1);
    a[0]-=ans;
    a[1]-=ans;
    a[2]-=ans;
    ans+=a[1]/3;
    a[1]%=3;
    ans+=a[2]/3;
    a[2]%=3;
    ans+=min(a[0], min(a[1], a[2]));
    
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
