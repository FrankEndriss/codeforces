/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;

#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

const int N=3e5+7;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/*
 * Inclusion/Exclusion
 * We need to know the number of perms where first _and_ second
 * are sorted/bad.
 * How to get that?
 * -> It is the same-pairs number.
 *  Let sp[] be the sizes of same-pair blocks, then it is
 *  x(sp[])=!s[0] * !s[1] * ... * !s[m]
 *
 *  Note that we explicitly have to check if ab[].second is sorted after
 *  haveing sorted ab[].
 *
 *  ans=N! - x(spFirst[]) - x(spSecond) + x(sp[])
 */
void solve() {
    cini(n);
    vector<pii> ab(n);
    for(int i=0; i<n; i++)
        cin>>ab[i].first>>ab[i].second;

    sort(all(ab));
    int prev=ab[0].first;
    pii prevab=ab[0];
    int cnt=1;
    int cntab=1;
    int xf=1;
    int xab=1;
    for(int i=1; i<n; i++) {
        if(ab[i].first==prev)
            cnt++;
        else {
            if(cnt>1) 
                xf=mul(xf, fac[cnt]);
            cnt=1;
        }
        prev=ab[i].first;

        if(ab[i]==prevab)
            cntab++;
        else {
            if(cntab>1) 
                xab=mul(xab, fac[cntab]);
            cntab=1;
        }
        prevab=ab[i];
    }

/* check if ab[].second is sorted */
    bool sorted=true;
    for(int i=1;i<n; i++) 
        if(ab[i].second<ab[i-1].second)
            sorted=false;

    if(cnt>1)
        xf=mul(xf, fac[cnt]);
    if(cntab>1)
        xab=mul(xab, fac[cntab]);

    sort(all(ab), [](pii p1, pii p2) {
        return p1.second<p2.second;
    });
    prev=ab[0].second;
    cnt=1;
    int xs=1;
    for(int i=1; i<n; i++) {
        if(ab[i].second==prev) {
            cnt++;
        } else {
            if(cnt>1)
                xs=mul(xs, fac[cnt]);
            cnt=1;
        }
        prev=ab[i].second;
    }
    if(cnt>1)
        xs=mul(xs, fac[cnt]);

    if(!sorted)
        xab=0;

 //  ans=N! - x(spFirst[]) - x(spSecond) + x(sp[])
 
//cerr<<"fac[n]="<<fac[n]<<" xf="<<xf<<" xs="<<xs<<" xab="<<xab<<endl;
    int ans=fac[n];
    ans=pl(ans, -xs);
    ans=pl(ans, -xf);
    ans=pl(ans, xab);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
