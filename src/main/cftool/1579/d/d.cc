/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each talk decrements two numbers in the array.
 * We cannot simulate since to much.
 *
 * ...We can simulate, since sum(a[]) over all testcases
 * <2e5, and we must simulate since we need to output
 * the pairs.
 * Use a priority_queue and allways pair the two max elements of a[].
 */
void solve() {
    cini(n);
    cinai(a,n);

    priority_queue<pii> q;  /* <a,id> */
    for(int i=0; i<n; i++) {
        if(a[i]>0)
            q.emplace(a[i],i);
    }

    vector<pii> ans;
    while(q.size()>=2) {
        pii p1=q.top();
        q.pop();
        pii p2=q.top();
        q.pop();
        assert(p1.second!=p2.second);
        ans.emplace_back(p1.second,p2.second);

        if(p1.first>1)
            q.emplace(p1.first-1, p1.second);
        if(p2.first>1)
            q.emplace(p2.first-1, p2.second);
    }

    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++) 
        cout<<ans[i].first+1<<" "<<ans[i].second+1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
