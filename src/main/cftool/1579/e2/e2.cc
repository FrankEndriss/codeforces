/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** stolen from tourist, Problem D, https://codeforces.com/contest/1208/standings/page/1 */
template <typename T>
struct fenwick {
    vector<T> fenw;
    int n;

    fenwick(int _n) : n(_n) {
        fenw.resize(n);
    }

    /* update add */
    void update(int x, T v) {
        while (x < n) {
            fenw[x] += v;
            x |= (x + 1);
        }
    }

    /* get sum of range (0,x), including x */
    T query(int x) {
        T v{};
        while (x >= 0) {
            v += fenw[x];
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};
/**
 * Process input from left to right.
 * Note that for the current number it does not
 * make any diff in which order the previous numbers where put into the deque.
 * So we want to place them where it is cheaper.
 * if number of smaller numbers is bigger index/2 then
 * to the end, else to the beginning. 
 * Just add up the number of inversions.
 * Find the number of smaller numbers with a segtree.
 *
 * Before all compress input array.
 */
void solve() {
    cini(n);
    cinai(a,n);

    map<int,int> aa;
    for(int i=0; i<n; i++)
        aa[a[i]]=0;

    int id=0;
    for(auto it=aa.begin(); it!=aa.end(); it++) {
        it->second=id;
        id++;
    }

    for(int i=0; i<n; i++)
        a[i]=aa[a[i]];


    fenwick<int> fen(id);

    int ans=0;
    for(int i=0; i<n; i++)  {
        int smaller=fen.query(a[i]-1);
        int bigger=fen.query(id-1)-fen.query(a[i]);
        ans+=min(smaller, bigger);
        fen.update(a[i], 1);
    }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
