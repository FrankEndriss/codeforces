/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * After each step we have a l,r total coverage, and a ll left end of current seq
 * which is inside l,r, also ll+size is inside l,r.
 *
 * Note that it never makes sense to l,r cover more than two times the biggest
 * segement, beause once we have covered such an area we can allways put the 
 * next seg completly into the covered area.
 *
 * Max size is 1e3, so max covered area is less than 2e3.
 *
 * Note that the l of l,r does not matter, just the size of l,r, so r0, and
 * the offset of ll within l,r
 *
 * So we have a 2D-dp. But still to slow.
 *
 * ...However, implement the brute force and see.
 */
const int N=1e3+3;
const int M=2e3+3;
void solve() {
    cini(n);

    vvb dp(M, vb(M));
    /* dp[i][j]=true -> area of size i is possible with left end of last placed seg at pos j */
    dp[0][0]=true;  /* start with the empty area */

    int prev=0; /* size of previous placed segment */

    for(int idx=0; idx<n; idx++) {
        cini(a);    /* size of current segment */

        for(int i=0; i<M; i++) {    /* all possible area sizes */
            for(int j=0; j<i; j++) {    /* all possible offsets of last seg */
                if(!dp[i][j])
                    continue;

                /* left end to left */
                int left=j-a;
                if(a<0) {
                    if(i-a<M) 
                        dp[i-a][0]=true;
                } else {
                    if(j-a<N) 
                        dp[i][j-a]=true;
                }

                /* left end to right */
                left=j;
                if(left+a>i && left+a<M)
                    dp[left+a][left]=true;

                /* right end to left */
                left=j+prev-a;
                if(a<0) {
                    if(i-a<M)
                        dp[i-a][0]=true;
                } else
                    if(j-a<N)
                        dp[i][j-a]=true;

                /* right end to right */
                left=j+prev;
                if(left+a<M)
                    dp[left+a][j]=true;
            }
        }
    }

    int ans=1e9;
    for(int i=0; i<M; i++)
        for(int j=0; j<N; j++) 
            if(dp[i][j])
                ans=min(ans, i);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
