/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Some positios form a component.
 * If at least one element of the component is 0 then all become 0.
 * Count the longest seq of 1 in each component.
 */
void solve() {
    cini(n);
    cini(d);
    cinai(a,n);

    vvi comp;
    vb vis(n);
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;

        vi e;
        int ii=i;
        bool has0=false;
        while(!vis[ii]) {
            e.push_back(a[ii]);
            if(a[ii]==0)
                has0=true;
            vis[ii]=true;
            ii=(ii+d)%n;
        }
        if(!has0) {
            cout<<-1<<endl;
            return;
        }
        comp.push_back(e);
    }

    /* count longest seq of 1 */
    int ans=0;
    for(size_t i=0; i<comp.size(); i++) {
        int cnt=0;
        for(size_t j=0; j<2*comp[i].size(); j++) {
            if(comp[i][j%comp[i].size()]==0)
                cnt=0;
            else
                cnt++;
            ans=max(ans, cnt);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
