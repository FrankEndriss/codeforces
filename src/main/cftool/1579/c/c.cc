/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Every black cell with at least k black cells on 
 * left and right diagonal can be a tick origin.
 *
 * Each black cell must be a tick origin, or beeing
 * created by some tick to the left or right with
 * the needed min size.
 *
 * So, check each cell to be a tick origin of max size, maybe 0.
 * Then check each black cell if there is a corresponding 
 * tick origin.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    cinas(s,n);

    vvi dp(n, vi(m));

    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++) {
            if(s[i][j]!='*')
                continue;

            int cnt=0;
            for(int ii=i-1; ii>=0; ii--)  {
                if(j-(i-ii)>=0 && j+(i-ii)<m && s[ii][j-(i-ii)]=='*' && s[ii][j+(i-ii)]=='*')
                    cnt++;
                else
                    break;
            }
            if(cnt>=k)
                dp[i][j]=cnt;
        }
    }

    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) {
            if(s[i][j]!='*')
                continue;

            if(dp[i][j]>0)
                continue;

            bool ok=false;
            for(int ii=1; i+ii<n; ii++) {
                if(j+ii<m && dp[i+ii][j+ii]>=ii)
                    ok=true;
                if(j-ii>=0 && dp[i+ii][j-ii]>=ii)
                    ok=true;
            }

            if(!ok) {
                cout<<"NO"<<endl;
                return;
            }
        }

    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
