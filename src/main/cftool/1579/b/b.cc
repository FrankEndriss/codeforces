/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * One shift per position.
 * Brute force.
 *
 * Why not correct?
 */
void solve() {
    cini(n);
    cinai(a,n);
    //cerr<<"next, n="<<n<<endl;

    vi aa=a;
    sort(all(aa));

    vvi ans;
    for(int i=0; i<n; i++) {

        if(aa[i]!=a[i]) {
            int done=false;
            for(int j=i+1; !done && j<n; j++) {
                if(aa[i]==a[j]) {
                    rotate(a.begin()+i, a.begin()+j, a.end());
                    ans.push_back({i+1,n,j-i});
                    done=true;
                }
            }
            assert(done);
        }

        /*
        cerr<<"a=";
        for(int j=0; j<n; j++)
            cerr<<a[j]<<" ";
        cerr<<endl;
        */
    }

    cout<<ans.size()<<endl;

    for(size_t i=0; i<ans.size(); i++)
        cout<<ans[i][0]<<" "<<ans[i][1]<<" "<<ans[i][2]<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
