/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}
();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, map<int,int> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization[d]++;
            n /= d;
        }
    }
    if (n > 1)
        factorization[n]++;
}

/**
 * X2 has some prime factors, and one of them was the choosen prime in second
 * choice. Considering these, we get (a lot) possible x1 values.
 *
 * Here, we do the same, and find foreach primefactor the min possible x0, and collect
 * min of all.
 *
 * ...
 * Lets try greedy aproach:
 * Give a number x[i], and we want to increase as much as possible, we would
 * probably use the next smaller prime, resulting in x[i+1]=pr*2
 *
 * So most likely the smallest prime factor fmin in x2 is the multiple, and
 * x2/fmin is the choosen prime.
 *
 * Same again in first step.
 * Note that a choosen prime cannot be as multiple prime factor in x[i].
 */
void solve() {
    cini(x2);

    int ans=x2;
    map<int,int> f;
    trial_division4(x2, f);

    //cerr<<"x2="<<x2<<" max prime factor="<<f.rbegin()->first<<endl;
    for(int j=x2-f.rbegin()->first+1; j<=x2; j++) {
        ans=min(ans, j);
        map<int,int> f2;
        trial_division4(j, f2);
        if(f2.rbegin()->first<j) {
            //cerr<<"j="<<j<<" f2.rbegin()->first="<<f2.rbegin()->first<<endl;
            ans=min(ans, j-f2.rbegin()->first+1);
        }
    }

    cout<<ans<<endl;
}


signed main() {
    assert(pr.back()>(int)1e6);
    solve();
}
