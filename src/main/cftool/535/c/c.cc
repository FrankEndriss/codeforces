/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * We got sequence of numbers, and need to
 * find sum quickly.
 *
 * Sequence is as follows:
 * s[0]=a+b*l
 * s[1]=a+b*(l+1)
 * ...
 *
 * Binary search.
 *
 */
void solve() {
    cini(A);
    cini(B);
    cini(n);

    /* @return true if karafs from l to r can be eaten with max t mbites */
    function<bool(int,int,int,int)> check=[&](int l, int r, int m, int t) {
/* copied from https://codeforces.com/contest/536/submission/10707215
 * This simple formular drives me insane :/
 */
        ll sum = A * (r - l) + B * ((r - l) * (r + l - 1) / 2LL);
        if (sum > m * t)
            return false;
        ll val = A + B * (r - 1);
        if (val > t)
            return false;
        return true;
    };

    for(int i=0; i<n; i++) {
        cini(l);
        cini(t);
        cini(m);

        
        int first=A+(l-1)*B;
        if(first>t) {
            cout<<-1<<endl;
            continue;
        }

        l--;
        int mi=l;
        int ma=l+t+3;
        while(mi+1<ma) {
            int mid=(mi+ma)/2;
            if(check(l,mid,m,t))
                mi=mid;
            else
                ma=mid;
        }
        cout<<mi<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
