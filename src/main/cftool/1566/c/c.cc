/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to put each 1/1 together with a 0/1,00 or 10
 * So there are 4 types
 *
 * We put each 0 into one part, get 1 point.
 * We put each 1 into prev (or next) zero part, get 1 point.
 */
void solve() {
    cini(n);
    cins(s);
    cins(t);

    vi a(n);
    for(int i=0; i<n; i++) {
        if(s[i]=='1' && t[i]=='1')
            a[i]=2;
        else if(s[i]=='0' && t[i]=='0')
            a[i]=0;
        else
            a[i]=1;
    }

    int ans=0;
    int z=0;    /* unmatched zeros */
    int o=0;    /* unmatched ones */

    for(int i=0; i<n; i++) {
        if(a[i]==0) {
            ans++;
            z=1;
            if(o) {
                ans++;
                z=0;
                o=0;
            }
        } else if(a[i]==2) {
            if(z) {
                ans++;
                z=0;
            } else {
                o=1;
            }
        } else if(a[i]==1) {
            ans+=2;
            o=0;
            z=0;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
