/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Makeing one substring gives MEX=2, or less.
 * Can we make less?
 *
 * If there is only 1 consecutive part of 0s, then we can.
 */
void solve() {
    cins(s);

    int zeroParts=0;
    if(s[0]=='0')
        zeroParts=1;
    for(size_t i=1; i<s.size(); i++) {
        if(s[i]=='0' && s[i-1]=='1')
            zeroParts++;
    }

    if(zeroParts==0)
        cout<<"0"<<endl;
    else if(zeroParts==1)
        cout<<"1"<<endl;
    else
        cout<<2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
