/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Still not understood... lengthy text :/
 * ...
 *
 * Easy version:
 * One row of seats.
 *
 * We want to fill rows from right to left, since that
 * makes best contribution.
 * But we cannot allways choose the best row, since the
 * number of rows is limited.
 *
 * Consider the case only 1 row.
 * How can we place the people optimally?
 * Consider the first person. We need to place him at
 * the rightmost position so that all others with a bigger
 * a[j] can still be placed right of him.
 * Those contribute 1.
 * So, foreach person i find/add the number of persons with a[j]>a[i].
 * But subtract the ones that where placed before person i.
 *
 * Thats annoying complecated implementation...
 * Use a segtree for the subtraction, and a sorted list for the adding ones.
 ******
 * So, now hard version.
 * We still need to put the persons to the right places,
 * but the contribution is limited to the persons in the same row,
 * ie up to the next M-block.
 * How to find the index of each persons seat?
 * -> It is the position in the sorted list, minus the number of persons
 *  with same a[i] allready processed.
 *
 * ...it is an of by one nightmare :/
 */
 
void solve() {
    cini(n);
    cini(m);
    //cerr<<"n="<<n<<" m="<<m<<endl;

    cinai(a,n*m);

    /* compress a[i] */
    vi aa=a;
    sort(all(aa));
    map<int,int> aamap;
    int ma=0;
    for(int i=0; i<n*m; i++) {
        auto it=aamap.find(aa[i]);
        if(it==aamap.end()) {
            aamap[aa[i]]=ma;
            ma++;
        }
    }
    for(int i=0; i<n*m; i++) 
        a[i]=aamap[a[i]];

    stree occ(n*m);
    stree occ2(n*m);
    for(int i=0; i<n*m; i++) {
        occ.set(a[i], occ.get(a[i])+1);
        occ2.set(a[i], occ2.get(a[i])+1);
    }
    stree occ3(n*m);

    int ans=0;
    for(int i=0; i<n*m; i++) {
        int idx=occ2.prod(0,a[i]+1)-1 - occ3.get(a[i]); /* zero based index of seat */
        int rowend=idx+1;   /* first seat of next row */
        if(rowend%m!=0) {
            rowend+=m;
            rowend-=(rowend%m);
        }

        //...and here we would need to consider in which row the allready 
        // processed people are placed...
        // TODO
        
        ans+=occ.prod(a[i]+1, rowend)-1;
        occ.set(a[i], occ.get(a[i])-1);
        //cerr<<"i="<<i<<" a[i]="<<a[i]<<" ans="<<ans<<endl;
        occ3.set(a[i], occ3.get(a[i])+1);
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
