/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to find the longest subarray (continous) with s+sum>=0
 * and each prefix s+sum>=0
 *
 * We can use two pointer.
 * Go with right-end until sum is bad,
 * go with left-end until it becomes good.
 *
 * Why so difficult to implement?
 * Dont get it :/
 * *****
 * Invariant is that the choosen subarray is good.
 * Then advance R until it becomes bad.
 * Advance L until L==R+1 or it becomes good.
 */
void solve() {
    cini(n);
    cini(s);
    cinai(a,n);


    int sum=0;

    /* start with empty but good subarray */
    int l=0; /* first used */
    int r=0; /* first unused */

    int ans=0;
    int ansL=-1;
    int ansR=-1;

    while(r<n) {

        /* make array bad */
        while(r<n && sum+s>=0) {
            sum+=a[r];
            r++;

            if(sum+s>=0 && ans<r-l) {
                ans=r-l;
                ansL=l;
                ansR=r-1;
            }
        }

        /* make array good */
        while(l<n && sum+s<0) {
            sum-=a[l];
            l++;
        }

        if(sum+s>=0 && ans<r-l) {
            ans=r-l;
            ansL=l;
            ansR=r-1;
        }
    }

    if(ans>0) {
        cout<<ansL+1<<" "<<ansR+1<<endl;
    } else 
        cout<<-1<<endl;
        
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
