/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * e1:
 * Start at 1, reach any leaf.
 * Multi bfs at all friends vertex, mark all vertex with its friends dist.
 * dfs start at 1 find a path on vertex notreached by his friends into a leaf.
 *
 * e2:
 * Each friend cuts some number of leafs, that is if that friend
 * reaches a leaf lteq vlad. (bfs,dfs)
 *
 * So we need to find the size of min subset of friends covering all leafs (except root).
 * How to find this subset? 
 * MaxFlow...MinCost somehow?
 *
 * S -> Friends, cap=INF, cost=0
 * Friends -> reacheableLeaf, cap=INF, cost=1
 * Leaf -> T, cap=1, cost=0
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);

    vi x(k);
    vi dp(n,INF);
    queue<int> q;

    for(int i=0; i<k; i++) {
        cin>>x[i];
        x[i]--;
        dp[x[i]]=0;
        q.push(x[i]);
    }

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    while(q.size()) {
        int v=q.front();
        q.pop();
        for(int chl : adj[v]) {
            if(dp[chl]>dp[v]+1) {
                dp[chl]=dp[v]+1;
                q.push(chl);
            }
        }
    }

    vi dp1(n,INF);
    dp1[0]=0;
    function<bool(int,int)> dfs=[&](int v, int p) {
        if(adj[v].size()==1 && v!=0)
            return true;    /* reached leaf */

        if(v!=0)
            dp1[v]=dp1[p]+1;

        for(int chl : adj[v]) {
            if(chl==p)
                continue;

            if(dp[chl]>dp1[v]+1 && dfs(chl, v)) {
                return true;
            }
        }
        return false;
    };

    if(dfs(0,-1)) {
        cout<<"YES"<<endl;
    } else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
