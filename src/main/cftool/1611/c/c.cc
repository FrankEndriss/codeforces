/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider l and r of p[]
 * The smaller of both numbers was added to p[] before
 * the bigger.
 *
 * Does not work...idk :/
 *
 * *****
 * Since the smaller of both ends gets removed first,
 * the bigger of both ends was added later.
 * So remove the bigger end first.
 * *****
 *
 * It turns out that one possible solution is to simply
 * reverse the input string.
 * This works, since the biggest element allways comes last,
 * so all elements are taken from the other end.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    if(a[0]==a.back() && n>1) {
        a.pop_back();
        n--;
    }

    vi p(2*n+1,INF);
    int pl=n;
    int pr=n+1;

    int l=0;
    int r=a.size()-1;
    while(l<=r) {
        if(a[l]>a[r]) {
            if(p[pr-1]<a[l]) {
                cout<<-1<<endl;
                return;
            }

            p[pl]=a[l];
            l++;
            pl--;

        } else {
            if(p[pl+1]<a[r]) {
                cout<<-1<<endl;
                return;
            }
            p[pr]=a[r];
            r--;
            pr++;
        }
    }

    for(int i=pl+1; i<pr; i++)
        cout<<p[i]<<" ";
    cout<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
