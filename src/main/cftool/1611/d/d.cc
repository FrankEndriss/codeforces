/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that the weights must be positive.
 * Iterate p;
 * Note that p[0] must be root, p[1] one of its children...
 * ans so on.
 * Each vertex must have an edge to an pre-choosen vertex,
 * else we would need to use negative weights.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(p,n);

    for(int i=0; i<n; i++) {
        a[i]--;
        p[i]--;
    }

    if(a[p[0]]!=p[0]) { /* p[0] must be root */
        cout<<-1<<endl;
        return;
    }

    vi w(n,-1);
    int maW=0;
    vi ans(n);
    for(int j=0; j<n; j++) {
        int i=p[j];
        assert(w[i]<0);

        if(a[i]==i) {
            ans[i]=0;
            w[i]=0;
        } else {
            int pw=w[a[i]];
            if(pw<0) {
                cout<<-1<<endl;
                return;
            }
            int mi=(maW+1)-pw;
            ans[i]=mi;
            w[i]=maW+1;
            maW++;
        }
    }

    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
