/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Lets use a map and brute force.
 */
void solve() {
    cini(n);

    cinas(ss,n);
    set<string> s;
    for(int i=0; i<n; i++)
        s.insert(ss[i]);

    string ans(n, '0');
    for(int j=0; j<n; j++) {
        for(size_t i=1; i<ss[j].size(); i++) {
            string l=ss[j].substr(0,i);
            string r=ss[j].substr(i);
            //cerr<<"l="<<l<<" r="<<r<<endl;

            auto itL=s.find(l);
            if(itL!=s.end()) {
                auto itR=s.find(r);
                if(itR!=s.end()) {
                    ans[j]='1';
                }
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
