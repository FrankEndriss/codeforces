/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We got three classes of a[i]:
 * 1: a[i]<i
 * 2: a[i]==i
 * 3: a[i]>i
 *
 * foreach class 1 we want to know
 * how much 1s are there right of it
 * Or how much left of it.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans=0;
    vi pre(n+1);
    for(int j=0; j<n; j++) {
        if(a[j]<j+1) {
            a[j]=max(1LL,a[j]);
            ans+=pre[a[j]-1];
            pre[j+1]=pre[j]+1;
        } else
            pre[j+1]=pre[j];
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
