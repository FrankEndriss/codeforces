/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The 4 cells of a rotation build an aequivalence class.
 * Count the symbols in that class,
 * and make them all same.
 */
void solve() {
    cini(n);
    cinas(s,n);

    int ans=0;
    for(int i=0; i<n/2; i++) {
        for(int j=0; j<(n+1)/2; j++) {
            if((n&1) && i==n/2 && j==n/2)
                continue;

            int cnt=0;
            cnt+=s[i][j]-'0';
            //cerr<<"i="<<i<<" j="<<j<<endl;
            int ii=n-1-j;
            int jj=i;
            //cerr<<"ii="<<ii<<" jj="<<jj<<endl;
            cnt+=s[ii][jj]-'0';
            int iii=n-1-jj;
            int jjj=ii;
            //cerr<<"iii="<<iii<<" jjj="<<jjj<<endl;
            cnt+=s[iii][jjj]-'0';
            ii=n-1-jjj;
            jj=iii;
            //cerr<<"iiii="<<ii<<" jjjj="<<jj<<endl;
            cnt+=s[ii][jj]-'0';

            ans+=min(cnt, 4-cnt);
        }
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
