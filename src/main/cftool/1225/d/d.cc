/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

bool init=[]() {

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/*
 * let a[i]=p[] where p[j] is the number of jth primefactor
 * then we need to find the number of pairs a[i]*a[j] where
 * each element of p_i[]+p_j[] is divisable by k.
 * Note that there is the exception of 1, since 1 is in all
 * primefactors. So a[i]*a[j]==1 always counts as a pair.
 *
 * First find all primefactors. Remove all multiples of k.
 * Then, for each number, find the fitting ones.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    map<vector<pii>,int> f;
    
    int ans=0;
    for(int i=0; i<n; i++) {
        map<int,int> ff;
        for(int j=0; pr[j]*pr[j]<=a[i]; j++) {
            while(a[i]%pr[j]==0) {
                ff[pr[j]]++;
                a[i]/=pr[j];
            }
        }
        if(a[i]>1)
            ff[a[i]]++;

        vector<pii> v;
        vector<pii> r;
        for(auto ent : ff) {
            if(ent.second%k!=0) {
                v.emplace_back(ent.first,ent.second%k);
                r.emplace_back(ent.first,k-(ent.second%k));
            }
        }


        ans+=f[r];
        f[v]++;
    }
    cout<<ans<<endl;
    
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
