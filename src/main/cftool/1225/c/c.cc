/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* let q be the number of numbers, then
 * q*p+sum(numbers)==n
 * can we iterate over all possible q?
 * So, for q==1 n-p must be some 2^k
 * for q==2 n-2*p must be some 2^k_1 + 2^k_2
 * for q==3 n-3*p must be some 2^k_1 + 2^k_2 + 2^k_3
 * ...
 * We can greedy check for increasing q if it is
 * possible to create n with q summands.
 * Calc n-q*p, then create smallest possible number of
 * summands. If that number is smaller q, then q is possible.
 * If n-q*p is negative no solution is possible.
 */
void solve() {
    cini(n);
    cini(p);

    
    vi ans(41);    
    int q=1;
    while(true) {
        int pp=n-q*p;
        if(pp<q)
            break;

        int cnt=0;
        for(int i=40; i>=0; i--) {
            int b=1LL<<i;
            if(pp-b>=0) {
                ans[i]++;
                cnt++;
                pp-=b;
            }
        }

        for(int i=40; i>0; i--) {
            if(cnt<q && ans[i]) {
                int ma=min(q-cnt, ans[i]);
                ans[i-1]+=2*ma;
                ans[i]-=ma;
                cnt+=ma;
            }
        }
        if(cnt==q) {
            cout<<q<<endl;
            return;
        }
        q++;
    }
    cout<<-1<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

