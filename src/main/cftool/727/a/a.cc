/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* Note:
 * Better solution would be starting from B, since
 * we can see for every B wich was the last operation.
 * If even it was mul2, if odd it was mul10+1
 *
 * Second, if implemented brute force (like below) 
 * we should do all mul2 before all mul10, (or 
 * vice versa), because ordering does not matter.
 */
void solve() {
    cinll(a);
    cinll(b);

    vll ans;
    function<bool(ll)> calc=[&](ll aa) {
        ans.push_back(aa);
        if(aa==b)
            return true;
        else if(aa>b) {
            ans.pop_back();
            return false;
        }

        aa*=2;
        if(calc(aa))
            return true;

        aa/=2;
        aa=aa*10+1;
        if(calc(aa))
            return true;

        ans.pop_back();
        return false;
    };

    if(calc(a)) {
        cout<<"YES"<<endl;
        cout<<ans.size()<<endl;
        for(auto i : ans)
            cout<<i<<" ";
        cout<<endl;
    } else {
        cout<<"NO"<<endl;
    }
 

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

