/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll

#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int ask(int i1, int i2) {
    cout<<"? "<<i1<<" "<<i2<<endl;
    cini(aux);
    return aux;
}

void solve() {
    cini(n);
    int ab=ask(1,2);
    int ac=ask(1,3);
    int bc=ask(2,3);
    vi a(n);
    a[0]=(ab+ac-bc)/2;
    a[1]=ab-a[0];
    a[2]=bc-a[1];
    for(int i=3; i<n; i++) {
        int aux=ask(i, i+1);
        a[i]=aux-a[i-1];
    }
    cout<<"! ";
    for(int i : a)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

