/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * s must not contain LLL or RRR
 * how to dp that?
 *
 * It can be optimum to change the second position, or
 * the third position.
 * -> If fourth position exists, and is same, then
 *  change third position, else change second position.
 *
 * Note that we need to rotate until some LR is found.
 * Note also that if all letters are same then ans=roundup(n/3);
 *
 * I do not get what is the intuition with this problem.
 * Isn't it a super messy casework?
 */
char flip(char c) {
    if(c=='L')
        return 'R';
    else if(c=='R')
        return 'L';
    else
        assert(false);
}

void solve() {
    cini(n);
    cins(s);

    bool dfound=false;
    for(int i=1; i<n; i++) {
        if(s[i]!=s[i-1]) {
            rotate(s.begin(), s.begin()+i, s.end());
            dfound=true;
            break;
        }
    }
    if(!dfound) {
        cout<<(n+2)/3<<endl;
        return;
    }

    s+='$';

    int ans=0;
    int cnt=1;
    for(size_t i=1; i<s.size(); i++) {
        if(s[i]==s[i-1])
            cnt++;
        else {
            ans+=cnt/3;
            cnt=1;
        }
    }
    //cerr<<"ans="<<ans<<" s="<<s<<endl;
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
