/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

//#define endl "\n"

/* dp
 * h[n-1] is highest mountain, it slides until mountain h[n-2]
 * is high enough to stop it. This is...
 *
 * h[0] will never slide, so h[1] slide until it reaches
 * size of h[0]. This hight can be h[0], or h[0]+1
 *
 * How would we simulate?
 * Slide all from h[1] to h[0]
 * Slide all from h[2] to h[1], and h[1] to h[0]
 * Slide all from h[3] to h[2], and h[2] to h[1], and h[1] to h[0]
 * ... O(n^2)
 *
 * Note that the sum of numbers does not change.
 * We need to find in which positions the hight finally increases,
 * and in which it is equals to the next one.
 *
 * Simulate the first diff, it is (h[1]-h[0])%2
 * So, h[1] looses (h[1]-h[0])/2 of hight (rounded down)
 * ...
 *
 * If an amount slides, than if downward hight diff==1 it simply slides
 * through.
 * On h[1] is simply halfes (rounded up).
 *
 * Let a plateau be two consecutive same hights.
 * One unit slides until the next plateau.
 * Then the right of that plateau is one higher.
 * If there is no plateau it slides until h[0], where
 * then h[0] and h[1] build a plateau. So next one stops at h[1]
 * And so on.
 *
 * So we need to keep track of the hight of h[0] and the plateau
 * positions. Denote them by their right half.
 *
 * We calc the plateaus after each h[i].
 */
void solve() {
    cini(n);
    cinai(h,n);

    int d=(h[1]-h[0])/2;
    h[0]+=d;
    h[1]-=d;
    assert(h[1]>=h[0]);

    stack<int> plat;
    if(h[0]==h[1])
        plat.push(1);

    for(int i=2; i<n; i++) {
        int hi1=h[0]+i-1-plat.size(); // hight of h[i-1]
        cerr<<"i="<<i<<" h[0]="<<h[0]<<" hi1="<<hi1<<" plat.size()="<<plat.size()<<endl;

        while(h[i]-hi1>1) {
            cerr<<"h[i]="<<h[i]<<" hi1="<<hi1<<" diff="<<h[i]-hi1<<endl;

            if(plat.size()>0) {     // there is a plateau
                int pl=plat.top();
                plat.pop();
                int units=min(i-pl, h[i]-hi1);
                pl+=units;
                if(pl<i)
                    plat.push(pl);
                hi1=h[0]+i-1-plat.size();
                continue;
            }

            /* now all hight diffs starting at h[0]/h[1] are +1 */
            int fullUnits=(h[i]-hi1)/i;
            cerr<<"doint fullUnits="<<fullUnits<<endl;
            h[0]+=fullUnits;
            hi1+=fullUnits;
            h[i]-=fullUnits*i;

            int diff=h[i]-hi1;
            if(diff==0)
                plat.push(i);
            else if(diff==1)
                ; 
            else if(diff>1) {
                h[0]++;
                plat.push(diff);
            }
        }
    }
    cerr<<"after for, plat.size()="<<plat.size()<<endl;

    stack<int> st2;
    while(plat.size()) {
        st2.push(plat.top());
        plat.pop();
    }

    cout<<h[0]<<" ";
    int ss=h[0];
    for(int i=1; i<n; i++) {
        if(st2.size() && st2.top()==i) {
            cout<<ss<<" ";
            st2.pop();
        } else {
            ss++;
            cout<<ss<<" ";
        }
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
