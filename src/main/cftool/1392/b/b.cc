/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * 1 2 3
 * 2 1 0
 * 0 1 2
 * 2 1 0
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(k);
    int mi=INF;
    int ma=-INF;
    vi a(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        a[i]=aux;
        mi=min(mi, aux);
        ma=max(ma, aux);
    }

    /* after 1:
     * ma=0
     * mi=ma-mi
     * mi -> ma
     * after 2:
     * ma=ma-mi-0
     * mi=0;
     */

    if(k==1) {
        for(int i=0; i<n; i++) 
            cout<<ma-a[i]<<" ";
        cout<<endl;
    } else if((k&1)==0) {
        for(int i=0; i<n; i++) 
            cout<<a[i]-mi<<" ";
        cout<<endl;
    } else {
        for(int i=0; i<n; i++) 
            cout<<ma-a[i]<<" ";
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
