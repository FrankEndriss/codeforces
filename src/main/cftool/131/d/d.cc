/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* see https://cp-algorithms.com/graph/finding-cycle.html
 * Note that this finds a cylce of size 2. To skip cylces of size 2
 * add a parent parameter to dfs(v,p).
 *
 * For directed graph modification see https://cses.fi/problemset/task/1678/
 * It adds a check so that in first place the dfs is started for 'root'
 * vertex only, that are vertex without an incoming edge.
 **/

vector<vector<int>> adj;
vector<char> color;
vector<int> parent;
int cycle_start, cycle_end;
vector<int> cycle;

bool dfs(int v, int p) {
    color[v] = 1;
    for (int u : adj[v]) {
        if(u==p)
            continue;

        if (color[u] == 0) {
            parent[u] = v;
            if (dfs(u,v))
                return true;
        } else if (color[u] == 1) {
            cycle_end = v;
            cycle_start = u;
            return true;
        }
    }
    color[v] = 2;
    return false;
}

/* set graph in adj[][], result cycle found in cycle[] */
void find_cycle(int n) {
    color.assign(n, 0);
    parent.assign(n, -1);
    cycle_start = -1;

    for (int v = 0; v < n; v++) {
        if (color[v] == 0 && dfs(v,-1))
            break;
    }

    if (cycle_start != -1) {
        cycle.push_back(cycle_start);
        for (int v = cycle_end; v != cycle_start; v = parent[v])
            cycle.push_back(v);
        cycle.push_back(cycle_start);
        reverse(cycle.begin(), cycle.end());
    }
}

/**
 * We need to find the (single) cycle, then
 * multi-dfs the distance of all other vertex
 * to the cycles elements.
 */
const int INF=1e9;
void solve() {
    cini(n);
    adj.resize(n);
    for(int i=0; i<n; i++) {
        cini(x); x--;
        cini(y); y--;
        adj[x].push_back(y);
        adj[y].push_back(x);
    }

    find_cycle(n);
    vi dp(n, INF);
    queue<int> q;
    for(int v : cycle) {
        dp[v]=0;
        q.push(v);
    }

    while(q.size()) {
        int v=q.front();
        q.pop();
        for(int chl : adj[v]) {
            if(dp[chl]>dp[v]+1) {
                dp[chl]=dp[v]+1;
                q.push(chl);
            }
        }
    }
    for(int i=0; i<n; i++) 
        cout<<dp[i]<<" ";
    cout<<endl;
}

signed main() {
        solve();
}
