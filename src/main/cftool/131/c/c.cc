/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s)
        c = c == ','?  ' ': c;
    stringstream ss;
    ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) {
    cerr << endl;
}
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0)
        cerr << ", ";
    stringstream ss;
    ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void d(int n, map<int,int> &divs) {
    for(int i=2; i*i<=n; i++) {
        while(n%i==0) {
            divs[i]++;
            n/=i;
        }
    }
    if(n>1)
        divs[n]++;

}
void solve() {
    cini(n);
    cini(m);
    cini(t);

    if(m<1 || n<4) {
        cout<<0<<endl;
        return;
    }

    /* iterate over possible number of b */
    int ans=0;
    for(int b=4; b<=n; b++) {
        int g=t-b;
        if(g<1 || g>m)
            continue;

        map<int,int> divs;
        vi muls;

        for(int i=0; i<b; i++) {
            muls.push_back(n-i);
            d(i+1, divs);
        }

        for(int i=0; i<g; i++) {
            muls.push_back(m-i);
            d(i+1, divs);
        }

        for(auto &ent : divs) {
            for(int i=0; i<muls.size(); i++) {
                if(muls[i]<2)
                    continue;

                while(ent.second && muls[i]%ent.first==0) {
                    muls[i]/=ent.first;
                    ent.second--;
                }
            }
        }

        int lans=1;
        for(int i=0; i<muls.size(); i++) {
            if(muls[i]==1)
                continue;
            lans*=muls[i];
        }
        ans+=lans;
    }

    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

