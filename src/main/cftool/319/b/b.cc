/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

struct node {
    int val=-1;
    int next=-1;
    int prev=-1;
    int cnt=0;
};

/* simulate.
 * use vector, erase in place by copy, count copy times.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vector<node> li(n);
    for(int i=0; i<n; i++) {
        li[i].val=a[i];
        li[i].next=i+1;
        li[i].prev=i-1;
    }
    li.back().next=-1;

    int idx=n-2;
    int ans=0;
    while(idx>=0) {
        while(li[idx].next>=0 && li[idx].val > li[li[idx].next].val) {
            /* remove li[idx].next */
            li[idx].cnt=max(li[idx].cnt+1, li[li[idx].next].cnt);
            li[idx].next=li[li[idx].next].next;
        }
        ans=max(ans, li[idx].cnt);
        idx=li[idx].prev;
    }
    
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
