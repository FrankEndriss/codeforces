/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

/* 
 * 0 : 0^x
 * 1 : 1^x
 * ...
 * (2^n)-1 : (2^(n-1)-1)^x
 *
 * # pairs of a,c = n*(n-1)/2
 *
 * How to find # b,d?
 * And, if we had #b,d, how to combine?
 *
 * Brute force would be sweep line counting intervals within other
 * intervals. How to use this?
 *
 * How to solve this bit by bit?
 * Say, 000...0001
 * Every a with 0 at last position gets a0=b0+1, the others a0=b0-1
 * So there is one a1=b1-1
 *
 * see tutorial:
 * Starting with lowest significant bit,
 * adding a bit multiplies ans by two.
 * adding a one-bit adds 2^((n-1)*2) pairs where
 * n is current position
 */
void solve() {
    cins(s);
    reverse(s.begin(), s.end());
    int ans=0;
    int two=1;
    for(char c : s) {
        ans=mul(ans, 2);
        if(c=='1') {
            ans+=mul(two,two);
            ans%=MOD;
        }
        two=mul(two, 2);   
    }
    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
