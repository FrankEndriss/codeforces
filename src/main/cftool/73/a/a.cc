/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need to cut each axis
 * the same times. We get
 * 100  2   : 2*1*1
 * 110  4   : 2*2*1
 * 111  8   : 2*2*2
 * 211 12   : 3*2*2
 * 221 18   : 3*3*2
 * 222 27   : 3*3*3
 * n,n,n    : (n+1)*(n+1)*(n+1)
 *
 * Additionally there is some casework
 * if xyz is smaller k/3.
 */
void solve() {
    cinai(a,3);
    cini(k);
    sort(all(a));

    int n=k/3;
    vi w= { n, n, n};
    int nn=k%3;
    if(nn) {
        w[2]++;
        nn--;
        if(nn)
            w[1]++;
    }

    int unused=max(0LL, w[0]-(a[0]-1));
    w[0]=min(a[0], w[0]+1);

    w[2]+=unused/2;
    w[1]+=unused-unused/2;
    while(w[1]>w[2]) {
        w[1]--;
        w[2]++;
    }

    unused=max(0LL, w[1]-(a[1]-1));
    w[1]=min(a[1], w[1]+1);
    w[2]+=unused;
    w[2]=min(a[2], w[2]+1);

    int ans=w[0]*w[1]*w[2];
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
