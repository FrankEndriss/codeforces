/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* find worst place x+y can make.
 * find max number of participants can be better/equal than x+y
 * 1+(x+y-1)
 * 2+(x+y-2)
 * ...
 * (x+y)/2 - 1
 **/

int smax(int n, int x, int y) {
    return min(n, x+y-1);
}

/* Best place x+y can make.
 * min cnt of pairs less/eq x+y
 * if first==1, min(second)==x+y
 * if first==2, min(second)==x+y-1
 *
 * 1+(x+y)
 * 2+(x+y-1)
 * 3+(x+y-2)
 * ...
 * k==x+y -> x+y+(x+y+1-(x+y))
 *
 * */
int smin(int n, int x, int y) {
    return max(1, min(n, x+y-n+1));
}

void solve() {
    cini(n);
    cini(x);
    cini(y);
    if(x>y)
        swap(x,y);
    cout<<smin(n,x,y)<<" "<<smax(n,x,y)<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

