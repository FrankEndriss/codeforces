/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* There are N*N/2 subarrays.
 * How to find the best one?
 *
 * Consider a position i, that can be swapped/reversed with a position j.
 * The contribution of that swap is:
 * c= b[i]*(a[j]-a[i]) + b[j]*(a[i]-a[j])
 *
 * And that contribution contributes for all arrays with the midpoint
 * in the mid between i and j, and starting at i or left of it.
 * That is: ans[i][j], ans[i-1][j+1],...
 * ...but that is O(n^3) :/
 *
 * How can we update a diagonal in ans[][] quickly?
 * Consider each diagonal be a segtree, then we can update all positions
 * from 0 to i in O(logn)
 *
 * We must consider 2*n diagonals, each of size n
 * ***
 * No...it is so trival :/
 * We need to fix the mid of the reversed array, and foreach 
 * such mid iterate all lengths of it.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    int sum=0;  /* sum without swap */
    for(int i=0; i<n; i++) 
        sum+=a[i]*b[i];

    int ans=sum;
    for(int i=0; i<n; i++) {
        int lans=sum;
        for(int l=i-1, r=i+1; l>=0 && r<n; l--,r++) {
            lans+= b[l]*(a[r]-a[l]) + b[r]*(a[l]-a[r]);
            ans=max(ans, lans);
        }

        lans=sum;
        for(int l=i, r=i+1; l>=0 && r<n; l--,r++) {
            lans+= b[l]*(a[r]-a[l]) + b[r]*(a[l]-a[r]);
            ans=max(ans, lans);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
