/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We can go one coordinate until the correct row/col,
 * then the other coordinate for the price of
 * n*(n+1)/2
 * How to know if a sum can be built?
 * Brute force?
 * Calc once.
 */
const int N=102;
vector<vector<set<int>>> a(N, vector<set<int>>(N));
void init() {
    a[1][1].insert(0);
    for(int i=1; i<N; i++) {
        for(int j=1; j<N; j++) {
            for(int k : a[i-1][j])
                a[i][j].insert(k+j);

            for(int k : a[i][j-1])
                a[i][j].insert(k+i);
        }
    }

}
void solve() {
    cini(n);
    cini(m);
    cini(k);
    /*
    for(int i=1; i<=n; i++)  {
        for(int j=1; j<=m; j++) {
            cerr<<"i="<<i<<" j="<<j<<" a[i][j]=";
            for(int kk : a[i][j])
                cerr<<kk<<" ";
            cerr<<endl;
        }
    }
    */

    if(a[n][m].count(k)>0)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
