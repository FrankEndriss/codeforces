/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/* We need to find number of subarray-sums
 * with value k^y.
 * How to find number of subarray-sum for
 * a single kk?
 * Find number of prefix sum diffs.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vi aa(1);
    partial_sum(all(a), back_inserter(aa));

    const int INF=1e15;
    set<int> kset;
    if(abs(k)>1) {
        int kk=1;
        while(abs(kk)<INF) {
            kset.insert(kk);
            kk*=k;
        }
    } else {
        kset.insert(k);
        kset.insert(k*k);
    }

    int ans=0;
    map<int,int> f;
    for(int i=0; i<=n; i++) {
        for(int j : kset) {
            auto it=f.find(aa[i]-j);
            if(it!=f.end())
                ans+=it->second;
        }
        f[aa[i]]++;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
