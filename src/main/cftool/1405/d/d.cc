/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* alice hunts bob 
 * Alice wins if
 * - db<=da*2
 * - hit in first move
 * - longest path <=a*2
 *
 * What else?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(a);
    cini(b);
    cini(da);
    cini(db);

    a--;
    b--;

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    if(db<=da*2) {
        cout<<"Alice"<<endl;
        return;
    }

    vi dist1(n, INF);
    dist1[a]=0;
    function<void(int,int)> dfs1=[&](int v, int p) {
        if(p>=0)
            dist1[v]=dist1[p]+1;

        for(int chl : adj[v])
            if(chl!=p)
                dfs1(chl, v);
    };

    dfs1(a,-1);
    if(dist1[b]<=da) {
        cout<<"Alice"<<endl;
        return;
    }

    /* find a leaf and use as root */
    int root=a;
    int mad=0;
    for(int i=0; i<n; i++) {
        if(dist1[i]>mad) {
            mad=dist1[i];
            root=i;
        }
    }

    vi dist2(n, INF);
    dist2[root]=0;
    function<void(int,int)> dfs2=[&](int v, int p) {
        if(p>=0)
            dist2[v]=dist2[p]+1;

        for(int chl : adj[v])
            if(chl!=p)
                dfs2(chl, v);
    };

    dfs2(root, -1);
    int mapl=*max_element(all(dist2));
    if(mapl<=2*da) {
        cout<<"Alice"<<endl;
        return;
    }

    cout<<"Bob"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
