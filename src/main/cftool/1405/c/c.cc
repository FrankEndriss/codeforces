/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* even k
 * s has three parts
 * left, overlap, right
 *
 * left and right must be alternating, and fit together. 0101 or 1010
 * overlap must have correct amount of 01 in any order.
 *
 * fuck, with k==4
 * also possible 11001100...
 * k==6
 * 111000111000
 *
 * Every kth position must be same or '?'!
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);

    bool ans=true;

    vi sym(k);
    for(int i=0; i<n; i++) {
        int idx=i%k;
        if(s[i]!='?')
            sym[idx]=s[i];
    }

    for(int i=0; i<n; i++) {
        if(s[i]=='?') 
            s[i]=sym[i%k];
    }

    if(ans)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
