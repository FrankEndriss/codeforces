/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * The games build a cycle (of max length 9).
 * There is a path leading from start into the cylce.
 * We need to find the scores of that path,
 * and the sum score of the cycle.
 *
 * Maintain a list of {a,b} after the ith game (0=initial)
 * until some previous seen {a,b} is found again. That is the 
 * cycle.
 */
void solve() {
    cini(k);
    cini(a);
    cini(b);
    vvi A(3, vi(3));
    vvi B(3, vi(3));
    for(int i=0; i<3; i++)
        for(int j=0; j<3; j++) {
            cin>>A[i][j];
            A[i][j]--;
        }
    for(int i=0; i<3; i++)
        for(int j=0; j<3; j++) {
            cin>>B[i][j];
            B[i][j]--;
        }

    a--;
    b--;

    map<pii,int> s; /* {a,b} -> ith game */

    vector<pii> scores;
    pii score;
    scores.push_back(score);    /* first -> zero; prefixsum 1 based */

    int game=1;
    while(s.find({a,b})==s.end()) {
        s[{a,b}]=game;
        game++;

        if(a==b)
            ;
        else if((a==2 && b==1) || (a==1 && b==0) || (a==0 && b==2))
            score.first++;
        else
            score.second++;

        scores.push_back(score);

        int na=A[a][b];
        int nb=B[a][b];
        a=na;
        b=nb;
    }
    
    int lstart=s[{a,b}];
    pii cycsc;

    cycsc.first =scores.back().first  - scores[lstart-1].first;
    cycsc.second=scores.back().second - scores[lstart-1].second;

    if(k<scores.size()) {
        cout<<scores[k].first<<" "<<scores[k].second<<endl;
        return;
    }

    pii bev;
    bev.first +=scores[lstart-1].first;
    bev.second+=scores[lstart-1].second;

    int ansA=bev.first;
    int ansB=bev.second;
    k-=(lstart-1);

    int cyc=scores.size()-lstart;
    ansA+=(k/cyc)*cycsc.first;  
    ansB+=(k/cyc)*cycsc.second;  
    if(k%cyc>0) {
        ansA+=scores[lstart-1+k%cyc].first - bev.first;
        ansB+=scores[lstart-1+k%cyc].second - bev.second;
    }

    cout<<ansA<<" "<<ansB<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
