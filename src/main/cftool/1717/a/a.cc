/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * math :/
 *
 * So, 
 * if a*2==b then lcm(a,b)=b, and gcd(a,b)=a, lcm/gcd=2
 * if a*3==b then lcm(a,b)=b, and gcd(a,b)=a, lcm/gcd=3
 * Are there other cases?
 * I guess no.
 * ...but that does not work.
 * What, exactly, is a pair here?
 * Consider n=2, ans=4. It seems
 * 1,1; 1,2; 2,1; 2,2; all contribute
 * Consider n=3, ans=7.
 * 1,1; 1,2; 1,3;
 * 2,1; 1,2; 2,3;
 * 3,1; 3,2; 3,3;
 * Which do not contribute? -> 3,2; 2,3;
 */
void solve() {
    cini(n);

    int ans=(n/2+n/3)*2+n;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
