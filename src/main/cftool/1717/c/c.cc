/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can increase numbers up to a[i]+1=a[i+1]
 * But also there is some special number n.
 * We actually can increate every number to n
 * after increaseing a[0] to n.
 *
 * So there are several cases:
 *
 * if any b[i]<a[i] then ans=NO
 * ...
 * We can increase the a[i] to like any value, but 
 * we can never make a[i]>a[i+1]+1, so if there is 
 * some b[i]>b[i+1]+1, and they do not fit before, then ans=No.
 *
 * Maybe some edgecases?
 * -> fairly unsure about that.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    for(int i=0; i<n; i++) 
        if(b[i]<a[i]) {
            cout<<"NO"<<endl;
            return;
        }

    for(int i=0; i<n; i++) {
        if(b[i]>b[(i+1)%n]+1 && a[i]<b[i]) {
            cout<<"NO"<<endl;
            return;
        }
    }

    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
