/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Observe that it is not n partitipants, but instead 2^n.
 * So 
 * if k==0 then we can make 1 the winner.
 * if k==1 then we can make last match 1,2, 
 *    and the first match of 1,3; so smallest winner is 3
 * if k==2 then we can make prelast match left 1,3, so smallest winner is 3.
 * ...and so on?
 * Consider second example, since n==2 there are 4 partitipants
 * 1
 * 3 - 1 - 1
 * 2 - 2
 * 4
 *
 * So hows the formular here?
 * It seems some wiered recursive definition...
 */
void solve() {
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
