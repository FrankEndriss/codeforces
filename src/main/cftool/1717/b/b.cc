/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Ok, we need to output some picture where
 * no k consecutive cells are '.'.
 * Also right/lower cell must be 'x'.
 *
 * What is n?
 * -> r,c is not the size of the table, it is some arbitrary cell, so n is size of table,
 *  also multiple of k. (why multiple of k?)
 *
 * I guess if we fill diagonals with 'X' it would be optimum.
 * How to nicely construct diagonals?
 */
void solve() {
    cini(n);
    cini(k);
    cini(r); r--;
    cini(c); c--;

    vs ans(n, string(n, '.'));
    ans[r][c]='X';

    r-=k*500;
    for(int f=0; f<1000; f++) {
        for(int i=0; r-i>=0; i++)
            if(r-i<n && i+c<n && r-i>=0 && i+c>=0)
                ans[r-i][i+c]='X';
        for(int i=0; r+i<n; i++)
            if(r+i>=0 && c-i>=0 && r+i<n && c-i<n)
                ans[r+i][c-i]='X';

        r+=k;
    }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
