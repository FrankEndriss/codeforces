/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Any substring between some W must alternate in some position.
 */
void solve() {
    cini(n);
    cins(s);
    s="W"+s+"W";

    vi w;
    for(size_t i=0; i<s.size(); i++) 
        if(s[i]=='W')
            w.push_back(i);

    bool ok=true;
    for(size_t i=0; i+1<w.size(); i++) {
        if(w[i]+1==w[i+1])
            continue;

        vb cnt(2);
        for(int j=w[i]+1; j<w[i+1]; j++) {
            cnt[s[j]=='R']=true;
        }
        if(!(cnt[0]&&cnt[1]))
            ok=false;
    }

    if(ok)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
