/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    vector<map<pii,int>> v1('k'-'a'+1);
    vector<map<pii,int>> v2('k'-'a'+1);
    vi c1('k'-'a'+1);
    vi c2('k'-'a'+1);

    int ans=0;
    for(int i=0; i<n; i++) {
        cins(s);
        pii p={ s[0]-'a', s[1]-'a' };

        int cnt1=c1[p.first];
        cnt1-=v1[p.first][p];

        int cnt2=c2[p.second];
        cnt2-=v2[p.second][p];

        ans+=cnt1;
        ans+=cnt2;

        v1[p.first][p]=v1[p.first][p]+1;
        c1[p.first]++;
        v2[p.second][p]=v2[p.second][p]+1;
        c2[p.second]++;
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
