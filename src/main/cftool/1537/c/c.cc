/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to have increasing order.
 * But we must choose some particular element for
 * first and last.
 * So there are 0, 1 or 2 non increasing positions.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(h,n);

    if(n==1) {
        cout<<h[0]<<endl;
        return;
    } else if(n==2) {
        cout<<h[0]<<" "<<h[1]<<endl;
        return;
    }

    sort(all(h));
    int minD=INF;
    for(int i=0; i+1<n; i++)
        minD=min(minD, h[i+1]-h[i]);

    if(h[1]-h[0]==minD) {
        swap(h[1],h[n-1]);
    } else if(h[n-1]-h[n-2]==minD) {
        swap(h[0],h[n-2]);
    } else {
        for(int i=0; i+1<n; i++) {
            if(h[i+1]-h[i]==minD) {
                swap(h[0],h[i]);
                swap(h[i+1],h[n-1]);
                break;
            }
        }
    }

    for(int i=0; i<n; i++) 
        cout<<h[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
