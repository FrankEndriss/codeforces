/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, vector<ll> &factorization) {
    for(int i=2; i*i<=n; i++) {
        if(n%i==0) {
            factorization.push_back(i);
            if(i*i!=n)
                factorization.push_back(n/i);
        }
    }
}

/* Any prime is a loose position.
 * ...
 * Consider 10
 * 10 -> 5,8
 * So 10 is win
 * 12 -> 6,8,9
 *
 * So, do a dfs, at some point only one player can win.
 *
 * But this goes TLE.
 * We need to somehow do a bfs instead :/
 */
void solve() {
    cini(n);


    vb memo(1000);
    vb vis(1000);
    function<bool(int)> iswin=[&](int nn) {
        if(vis[nn])
            return memo[nn];

        vis[nn]=true;

        //cerr<<"iswin, nn="<<nn<<endl;
        vi d;
        trial_division4(nn,d);
        if(d.size()==0) {
            return memo[nn];
        }

        for(int dd : d) {
            const int nnn=nn-dd;
            if(!iswin(nnn)) {
                memo[nn]=true;
                break;
            }
        }

        return memo[nn];
    };

    for(n=2; n<100; n++) {
    cout<<"n="<<n<< " ";
    if(iswin(n))
        cout<<"Alice"<<endl;
    else
        cout<<"Bob"<<endl;

    }

}

signed main() {
    cini(t);
    while(t--) {
        solve();
        break;
    }
}
