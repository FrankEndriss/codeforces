/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to use the lex smallest repition of a prefix
 * of s with size <=k.
 * So, check from left to right, ans once there is a letter
 * bigger s[0], stop.
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);
    int ans;
    for(ans=1; ans<n && ans<k; ans++) {
        if(s[ans]>s[0]) {
            break;
        }
    }

    string sans;
    int j=0;
    for(int i=0; i<k; i++) {
        cout<<s[j];
        j++;
        if(j==ans)
            j=0;
    }
    cout<<endl;
}

signed main() {
    solve();
}
