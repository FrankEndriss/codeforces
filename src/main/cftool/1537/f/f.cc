/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider starting at a leave.
 * There is only one possible operation...
 *
 * What is the invariant?
 *
 * We can move a value over distance 2 by two operations.
 * We can change to adj vertex by same value.
 *
 * Start at leaf, move the diff, and remove the leaf.
 * Once there are only two vertex left, fix them or not.
 *
 * ***
 * Fuck...its not a tree :/
 */
void solve() {
    cini(n);
    cini(m);
    cinai(v,n);
    cinai(t,n);

    vector<set<int>> adj(n);
    for(int i=0; i<m; i++) {
        cini(a);
        a--;
        cini(b);
        b--;
        adj[a].insert(b);
        adj[b].insert(a);
    }

    queue<int> leafs;
    for(int i=0; i<n; i++)
        if(adj[i].size()==1)
            leafs.push(i);

    int j=0;
    for(int i=0; i<n-2; i++) {
        if(leafs.size()==0) {
            /* remove any edge until there is a leaf */
            for(; j<n; j++) {
                if(adj[j].size()==0)
                    continue;

                while(adj[j].size()>1) {
                    int vv=*adj[j].begin();
                    adj[vv].erase(j);
                    adj[j].erase(vv);
                    if(adj[vv].size()==1) {
                        leafs.push(*adj[vv].begin());
                        break;
                    }
                }
                if(adj[j].size()==1) 
                    leafs.push(j);
                break;
            }
        }

        int l=leafs.front();
        leafs.pop();
        int d=t[l]-v[l];
        for(int chl : adj[l]) {
            v[chl]+=d;
            d=0;
            adj[chl].erase(l);
            if(adj[chl].size()==1)
                leafs.push(chl);
        }
        adj[l].clear();
    }

    for(int i=0; i<n; i++) {
        if(adj[i].size()>0) {
            int v1=i;
            int v2=*adj[i].begin();
            if(v[v1]-t[v1]==v[v2]-t[v2])
                cout<<"YES"<<endl;
            else
                cout<<"NO"<<endl;
        }
    }
    assert(false);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
