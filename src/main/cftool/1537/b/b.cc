/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider a diagonal, dist n+m.
 * So use two diagonal symetric corners, check both possibilities.
 */
void solve() {
    cini(n);
    cini(m);
    cini(i);
    cini(j);

    function<int(int,int,int,int)> d=[](int x1, int y1, int x2, int y2) {
        return abs(x1-x2)+abs(y1-y2);
    };
    function<int(int,int,int,int)> go=[&](int x1, int y1, int x2, int y2) {
        return d(i,j,x1,y1)+d(x1,y1,x2,y2)+d(x2,y2,i,j);
    };

    vvi ans={
        { 0,0,n-1,m-1 },
        { 0,m-1,n-1,0 }
    };
    if(go(ans[0][0],ans[0][1],ans[0][2],ans[0][3])>go(ans[1][0],ans[1][1],ans[1][2],ans[1][3]))
        cout<<ans[0][0]+1<<" "<<ans[0][1]+1<<" "<<ans[0][2]+1<<" "<<ans[0][3]+1<<endl;
    else
        cout<<ans[1][0]+1<<" "<<ans[1][1]+1<<" "<<ans[1][2]+1<<" "<<ans[1][3]+1<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
