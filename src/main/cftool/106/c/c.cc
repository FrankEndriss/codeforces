/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Mechanics like knapsack.
 * From every stuff we can max
 * create a[i]/b[i] buns.
 * Each of them has cost c[i] and value d[i]
 * Max cost possible is n.
 *
 * Since a[i]/b[i] is fairly small we can iterate
 * over every single possible bun.
 * So dp[i][j]
 */
void solve() {
    cini(n);
    cini(m);
    cini(c0);
    cini(d0);

    vector<pii> bu;
    for(int i=0; i<m; i++) {
        cini(a);
        cini(b);
        cini(c);
        cini(d);
        int cnt=a/b;
        for(int j=0; j<cnt; j++)
            bu.emplace_back(c,d);
    }

/* dp[j]=max tugriks after use of j grams of dough */
    vi dp(n+1);
    for(size_t i=0; i<bu.size(); i++) {
        vi dp2(dp.size());
        for(int j=0; j<=n; j++) {
            if(j>=bu[i].first)
                dp2[j]=max(dp[j], dp[j-bu[i].first]+bu[i].second);
            else
                dp2[j]=dp[j];
        }

        dp=move(dp2);
    }

    for(int i=0; i+c0<=n; i++)
        dp[i+c0]=max(dp[i+c0], dp[i]+d0);

    int ans=*max_element(all(dp));
    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
