/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Foreach flow we need to find if with first or second circle.
 * Greedy.
 * We put all flowers in first circle.
 * The consecutively we put the one nearest to c2 into second
 * circle. Check each one.
 * We can implement brute force O(n^2)
 */
void solve() {
    cini(n);
    cini(x1);
    cini(y1);
    cini(x2);
    cini(y2);

    vector<pii> f(n);
    for(int i=0; i<n; i++)
        cin>>f[i].first>>f[i].second;

    /* sort by distance to c2 */
    sort(all(f), [&](pii p1, pii p2) {
        int x1d=x2-p1.first;
        int y1d=y2-p1.second;

        int x2d=x2-p2.first;
        int y2d=y2-p2.second;

        return (x1d*x1d)+(y1d*y1d)<(x2d*x2d)+(y2d*y2d);
    });

    int ans=1e18;
    for(int i=0; i<=n; i++) {
        int r1=0;
        int r2=0;
        for(int j=0; j<n; j++) {
            if(j>=i) {
                int xd=x1-f[j].first;
                int yd=y1-f[j].second;
                r1=max(r1, xd*xd+yd*yd);
            } else {
                int xd=x2-f[j].first;
                int yd=y2-f[j].second;
                r2=max(r2, xd*xd+yd*yd);
            }
        }
        ans=min(ans, r1+r2);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
