/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9;

void solve() {
    cini(n);
    cini(m);
    vvi dp(n, vi(m));
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            cin>>dp[i][j];
            if(dp[i][j]==0)
                dp[i][j]=INF+2000;
        }

    bool ok=true;
    for(int i=n-2; i>=0; i--) {
        for(int j=m-2; j>=0; j--) {
            if(dp[i][j]>INF) {
                dp[i][j]=min(dp[i][j], dp[i+1][j]-1);
                dp[i][j]=min(dp[i][j], dp[i][j+1]-1);
            } else {
                if(dp[i][j]>=dp[i][j+1] || dp[i][j]>=dp[i+1][j])
                    ok=false;
            }
        }
    }

    ll ans=0;
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            ans+=dp[i][j];
            if(i>0 && dp[i-1][j]>=dp[i][j])
                ok=false;
            if(j>0 && dp[i][j-1]>=dp[i][j])
                ok=false;
        }

    if(!ok) {
        cout<<-1<<endl;
        return;
    }

    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

