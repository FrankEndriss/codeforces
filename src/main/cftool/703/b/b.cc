/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* We would like to add up all roads, but that are to much.
 * So, we add up all capital roads.
 * Subtract capital to capital roads once, because we did add them twice.
 * Then add all normal roads between non capital cities.
 */
void solve() {
    cini(n);
    cini(k);
    vi c(n);
    int sumAll=0; // sum beauties all cities
    for(int i=0; i<n; i++) {
        cin>>c[i];
        sumAll+=c[i];
    }

    int sumCaps=0;  // sum beauties capital cities
    int ans=0;
    vb caps(n);
    for(int i=0; i<k; i++) {
        cini(aux);
        aux--;
        caps[aux]=true;
        sumCaps+=c[aux];
        ans+=c[aux]*(sumAll-c[aux]);
    }

    int ansInterCaps=0; // cost of roads between capitals
    for(int i=0; i<n; i++) 
        if(caps[i])
            ansInterCaps+=(sumCaps-c[i])*c[i];

    ansInterCaps/=2;
    ans-=ansInterCaps;

    for(int i=0; i<n-1; i++) {
        if(!caps[i] && !caps[i+1])
            ans+=c[i]*c[i+1];
    }
    if(!caps[0] && !caps[n-1])
        ans+=c[0]*c[n-1];

    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

