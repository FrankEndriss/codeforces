/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * The tree is rooted at vertex 1.
 * We need to adjust the tree from the leafs.
 * Whenever a leaf is zero we remove that vertex.
 *
 * So, in each round we choose a subset of the leafs, and
 * update at least that nodes wich are on the path to the choosen
 * leafs.
 *
 * Let v[mileaf] be the min negative leaf, and v[maleaf] the max positive leaf.
 * Then we have to increment the path to v[mileaf] at least v[mileaf] times.
 * Same for maxleaf.
 *
 * Consider vp[] the parents of all leafs.
 * Each parent has a vp[min] and vp[max], ant the updates done to that parent
 * are at least vp[min] + vp[max]
 * The same is true for all other parents of leafs.
 * So "on the way" to updating the v[mileaf] we can update all other vp[min], ie all
 * leafs with negative v. Same true for positive.
 *
 * Now consider a parent with only one negative child. That parent will become a
 * child on itself at some point of time, and then the same as before
 * applies to it. So, the set of childs change, and propably the smallest value
 * in this set.
 * Also it is unclear if we first should go positive or negative.
 * dp for both possibilities? no, there are much to much states.
 *
 * On the other hand, for each subtree, there is a min number of 
 * pos and neg operations to delete the whole subtree.
 * This is some increase (for removing the smallest child) and some decrease for removing the biggest
 * child, and some more operations for removing the node itself.
 * Simply calc that from leafs to root.
 */
const int INF=1e18;
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }
    cinai(a,n);

    function<pii(int,int)> dfs=[&](int v, int p) {
        int mi=0;
        int ma=0;
        for(int chl : adj[v]) {
            if(chl!=p) {
                auto val=dfs(chl,v);
                mi=min(mi, val.first);
                ma=max(ma, val.second);
            }
        }

        int a0=a[v]-mi-ma;
        if(a0<0)
            mi+=a0;
        else
            ma+=a0;

        return make_pair(mi,ma);
    };

    auto ans=dfs(0,-1);
    cout<<abs(ans.first)+abs(ans.second)<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
