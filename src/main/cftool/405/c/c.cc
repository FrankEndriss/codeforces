/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

/* Addition in GF(2) is XOR,
 * so we need to count parities.
 *
 * Multiplication is AND.
 * Every pair a[i][j]==a[j][i]==1 contributes.
 *
 * Tutorial: 
 * All cells contribute a multiple of two times,
 * except the diagonal.
 * So, flipping allways flips ans.
 */
void solve() {
    cini(n);
    vvi a(n, vi(n));
    int ans=0;
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++) {
            cin>>a[i][j];
            if(i==j)
                ans^=a[i][j];
        }

    cini(q);
    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1 || t==2) { 
            cini(aux);
            ans^=1;
        } else if(t==3)
            cout<<ans;
        else
            assert(false);
    
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
