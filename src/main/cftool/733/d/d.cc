/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int mi(int i1, int i2, int i3) {
    return min(i1, min(i2, i3));
}

struct abc {
    int a, b, c;
    abc(int _a, int _b, int _c):a(_a),b(_b),c(_c) {
    }
};

/* We need to find pairs of triples 
 * where pairs of values ab,ac,bc are equal.
 * Among all those pairs find the one with biggest
 * min value.
 * Note that each stone allone counts as min(a,b,c)
 *
 * We need to index the stones by pairs of indexes (
 */
void solve() {
    cini(n);
    vvi s(n, vi(3));
    map<pii,pii> bg;    /* <{a1,a2},{a3,idx}> index of stone with pairOfSides and biggest third side */

    for(abc i : {abc(0,1,2),abc(0,2,1),abc(1,2,0)}) {
        //cout<<"abc.a="<<i.a<<" abc.b="<<i.b<<" abc.c="<<i.c<<endl;
        cin>>s[i][0]>>s[i][1]>>s[i][2];
        sort(all(s[i]));

        pii p={s[i][0],s[i][1]};
        if(bg[p].first<s[i][2])
            bg[p]={s[i][2],i};

        p={s[i][0],s[i][2]};
        if(bg[p].first<s[i][1])
            bg[p]={s[i][1],i};

        p={s[i][1],s[i][2]};
        if(bg[p].first<s[i][0])
            bg[p]={s[i][0],i};
    }

    int ans=0;
    int ans1=-1;
    int ans2=-1;
    for(int i=0; i<n; i++) {
        pii p={s[i][0],s[i][1]};
        pii match=bg[p];
        int s2=s[i][2]+match.first;
        int lans=mi(s[i][0], s[i][1], s2);
        if(lans>ans) {
            ans=lans;
            ans1=i;
            if(match.first>0)
                ans2=match.second;
        }
        p={s[i][0],s[i][2]};
        match=bg[p];
        s2=s[i][1]+match.first;
        lans=mi(s[i][0], s[i][2], s2);
        if(lans>ans) {
            ans=lans;
            ans1=i;
            if(match.first>0)
                ans2=match.second;
            else
                ans2=-1;
        }
        p={s[i][1],s[i][2]};
        match=bg[p];
        s2=s[i][0]+match.first;
        lans=mi(s[i][1], s[i][2], s2);
        if(lans>ans) {
            ans=lans;
            ans1=i;
            if(match.first>0)
                ans2=match.second;
        }
    }

    if(ans2>=0)
        cout<<2<<endl<<ans1<<" "<<ans2<<endl;
    else
        cout<<1<<endl<<ans1<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
