/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* strange rules again...but however
 * !all points are dist 1 from adj points.
 * We need to pair the outmost two points (its allways possible)
 * i times, and foreach i=0..n
 * add the number of possible same size pairings.
 *
 * How to find the same size pairings?
 * Note that the dist of the indexes of the points are all same.
 * So, since n-i is even, we can pair every two points together.
 * We can build packs of 4 if 2*(n-i) is div by 4
 * We can build packs of 6 if 2*(n-i) is div by 6
 * etc
 * so, we need the number of divisors of n-i.
 * And that for all numbers up to n.
 *
 * ...
 * But we can have additionally a combination of outermost overlapping,
 * with innermost posibilities, see third example picture.
 *
 * So, dp[i]=sum(dp[i-1], dp[i-2]...)+divs
 */
const int MOD=998244353;
void solve() {
    cini(n);

    vi dp(n+1,1); /* dp[i]=number of divisors of i */
    vi dp1(n+1);    /* dp1[i]=ans for n=i */
    vi dp2(n+1);    /* dp2[i]=prefix sum of dp1[i] */

    for(int i=2; i<=n; i++) {
        for(int j=i; j<=n; j+=i)
            dp[j]++;
    }

    for(int i=1; i<=n; i++) {
        dp1[i]=dp2[i-1]+dp[i];
        dp1[i]%=MOD;
        dp2[i]=dp2[i-1]+dp1[i];
        dp2[i]%=MOD;
    }

    cout<<dp1[n]<<endl;
        
}

signed main() {
    solve();
}
