/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* oh my...
 * Whats the rules of this one?
 * What is a "strange" seq?
 *
 * Every diff of two nubmers in the seq must be bigger than the biggest element.
 * So, one of the element is the biggest element, and the bigger it is, the 
 * less other elements we can choose.
 * Find the freq of the diffs...then use the prefix sum..?
 *
 * How can we find for a max diff the biggest possible number of elements,
 * and, that in less than O(n^2) for all biggest elements?
 *
 * Or binary search the the optimum element?
 * Brute force would be to check every element to be the biggest one,
 * then foreach one count the numbers of smaller elements we can put into
 * the list. 
 * How to count correctly?
 * -> start with the smallest element, then add the next if possible.
 * But that is O(n^2)
 *
 * So start at smallest number, then find the numbers we must remove
 * if using the next number, and remove them.
 * But that is...very implementation heavy, there must be simpler solution.
 * How to remove correctly?
 *
 * if a diff is to small, we need to remove that element,
 * so the diff of the next element becomes the sum of both.
 * This should work with max n removals.
 *
 * ...ans still WA, sic :/
 * There is some observation needed, most likly a simple one.
 * idk
 * ********************
 * We can choose all neg elements.
 * It is allways optimal to use the smallest element in the list.
 * ...not really, since the freq of the elements comes into play.
 * It is allways optimal to use the next possible element in the list.
 * How in less than O(n^2)?
 */
const int INF=1e10;
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));

    int ans=1;

    map<int,set<int>> didx;   /* didx[j]=indexes where diff to previous is j */

    vi d(n);
    d[0]=INF;
    vi next(n, -1);
    vi prev(n, -1);
    for(int i=1; i<n; i++) {
        d[i]=a[i]-a[i-1];
        assert(d[i]>=0);
        didx[d[i]].insert(i);
        next[i-1]=i;
        prev[i]=i-1;
    }

    auto it=didx.begin();
    int sub=0;
    for(int i=1; i<n; i++) {
        if(a[i]>0) {
            /* remove all numbers where d[j]<a[i] */
            while(it!=didx.end() && it->first<a[i]) {
                sub++;
                assert(it->second.size()>0);
                auto it2=it->second.begin();
                int remIdx=*it2;
                it->second.erase(it2);

                if(next[remIdx]>0) {
                    didx[d[next[remIdx]]].erase(next[remIdx]);
                    d[next[remIdx]]+=d[remIdx];
                    didx[d[next[remIdx]]].insert(next[remIdx]);
                }

                if(prev[remIdx]>=0)
                    next[prev[remIdx]]=next[remIdx];
                if(next[remIdx]>=0)
                    prev[next[remIdx]]=prev[remIdx];

                while(it!=didx.end() && it->second.size()==0)
                    it++;
            }
            ans=max(ans, i+1-sub);
        } else {
            ans=i+1;
        }
            
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
