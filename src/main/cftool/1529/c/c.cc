/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* For leaf vertex there are only two possible values, l[i] or r[i].
 * Other vertex same, because the median of the adj vertex values
 * would minimize the beauty, so l[i] or r[i] maximizes it.
 *
 * How to find the max beauty for a subtree?
 * Each subtree can be created in two ways, so check both and use the max.
 */
void solve() {
    cini(n);
    vi l(n);
    vi r(n);
    for(int i=0; i<n; i++) 
        cin>>l[i]>>r[i];

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vvi val(2, vi(n));  /* val[k][i]=max possible beauty for subtree i if used 0=l[i] or 1=r[i] */

    function<void(int,int)> dfs=[&](int v, int p) {
        vi ans(2);

        for(auto c : adj[v]) {
            if(c!=p) {
                dfs(c,v);
                int ma=    abs(l[c]-l[v])+val[0][c];
                ma=max(ma, abs(r[c]-l[v])+val[1][c]);
                ans[0]+=ma;
                ma=        abs(l[c]-r[v])+val[0][c];
                ma=max(ma, abs(r[c]-r[v])+val[1][c]);
                ans[1]+=ma;
            }
        }
        val[0][v]=ans[0];
        val[1][v]=ans[1];
    };

    dfs(0,-1);
    cout<<max(val[0][0], val[1][0])<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
