/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* This looks fairly greedy.
 * Optimal order of projects is 
 * -all prj with positive change/b[i] in order of difficulty/a[i]
 * Then how to find optimal order of negative b[i] prj?
 * Let S be rating after completing all positives.
 * We need to order the negs so that each S-prefix[b[i]]>=a[i]
 *
 * We remove allways the biggest possible a[i] while still be
 * able to remove the next/first a[i].
 * How to profe?
 * see https://codeforces.com/blog/entry/69108?#comment-535157
 * Actually we need to sort by a[i]+b[i] non increasing.
 */
void solve() {
    cini(n);
    cini(r);

    vector<pii> pos;
    vector<pii> neg;
    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        if(b>=0)
            pos.emplace_back(a,b);
        else
            neg.emplace_back(a,b);
    }

    sort(all(pos));
    bool ok=true;
    for(size_t i=0; i<pos.size(); i++) {
        if(r<pos[i].first)
            ok=false;
        else
            r+=pos[i].second;
    }
    
    sort(all(neg), [](pii p1, pii p2) {
            return p1.first+p1.second > p2.first+p2.second;
    });
    for(size_t i=0; i<neg.size(); i++) {
        if(r<neg[i].first)
            ok=false;
        else
            r+=neg[i].second;
    }
    if(r<0)
        ok=false;

    if(ok)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    solve();
}
