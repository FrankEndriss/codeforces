/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to find the leftmost positions
 * of t in s, and the rightmost.
 * Then check the length of the gap
 * for all positions, max==ans.
 */
void solve() {
    cins(s);
    cins(t);    /* subseq of s */
    s="0"+s+"0";
    t="0"+t+"0";

    vi dpL(t.size());
    vi dpR(t.size());

    int jL=0;
    int jR=0;
    for(size_t i=0; i<s.size(); i++) {
        if(s[i]==t[jL]) {
            dpL[jL]=i;
            jL++;
        }

        if(s[s.size()-1-i]==t[t.size()-1-jR]) {
            dpR[t.size()-1-jR]=s.size()-1-i;
            jR++;
        }
    }
    assert(jL==t.size());
    assert(jR==t.size());

    int ans=0;
    for(int i=0; i+1<t.size(); i++)
        ans=max(ans, dpR[i+1]-dpL[i]-1);

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
