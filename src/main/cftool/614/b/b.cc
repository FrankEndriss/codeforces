/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

bool check(string &s) {
    bool one=false;
    for(char c : s) {
        if(c=='1') { 
            if(one)
                return false;
            one=true;
        } else if(c!='0')
            return false;
    }
    return one;
}

void solve() {
    cini(n);
    int ans=0;  // cnt of pow
    string mul;
    bool zero=false;
    for(int i=0; i<n; i++) {
        cins(s);
        if(s=="0") {
            zero=true;
            break;
        }

        if(check(s))
            ans+=s.size()-1;
        else
            mul=s;
    }

    if(zero) {
        cout<<"0"<<endl;
    } else {
        string res(ans, '0');
        if(mul.size()==0)
            mul="1";
        cout<<mul<<res<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

