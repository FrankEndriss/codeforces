/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * What a clumsy work...feels like geometrics problem :/
 *
 * Find the nice subrects, and check if two of them intersect.
 *
 * How to find nice subrects. ?
 * Brute force extend from every position.
 * ..no.
 * To complecated for friday afternoon :/
 */
void solve() {
    cini(n);
    cini(m);
    cinas(a,n);
    assert(a[0].size()==m);

    vector<pair<pii,pii>> r;    /* leftUpper, hight/width

    vvi pre(n, vi(m+1));    /* number of black rects in a row */
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) {
            if(a[i][j]=='0')
                continue;

            /* on each of the four sides there must be at least 1 white cell */
            for(int ii=1; i+ii<=n; ii++) 
                for(int jj=1; j+jj<=m; jj++) {
                    if(a[ii][jj]=='0')
                        continue;

                }
            }
        }



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
