/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* use first col ans sort the rows according to that col.
 */
void solve() {
    cini(n);
    cini(m);

    vi rowof(n*m+1);

    vvi ans(n, vi(m));

    vvi rows(n, vi(m));
    for(int i=0; i<n; i++) {    /* rows */
        for(int j=0; j<m; j++) {
            cin>>rows[i][j];
            rowof[rows[i][j]]=i;
        }
    }

    vvi cols(m, vi(n));
    for(int j=0; j<m; j++) {    /* cols */
        for(int i=0; i<n; i++) {
            cin>>cols[j][i];
        }
    }

    for(int i=0; i<n; i++) {
        int val=cols[0][i];
        int row=rowof[val];
        ans[i]=rows[row];
    }
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) 
            cout<<ans[i][j]<<" ";
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
