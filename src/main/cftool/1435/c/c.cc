/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* dp
 * dp[i][j] min/max if playing ith note on jth string
 * ...no, no usefull state changes possible.
 *
 * Binary search the smallest used fret?
 * put all other notes on the lowest possible fret above min.
 * But that might not be increasing/decreasing function :/
 *
 * Note that the notes are shiftet on the strings by a[i+1]-a[i]
 * Let n[0] be smallest note and n[x] be biggest.
 * So if all played on one string the spread is n[x]-n[0]
 *
 * So put all notes on lowest string, then move the one
 * on the biggest fret to the next string and so on.
 * Until all notes are on the highest string.
 *
 * output ans.
 */
const int INF=1e9;
void solve() {
    cinai(a,6);
    sort(all(a));
    cini(n);
    cinai(b,n);
    sort(all(b));

    priority_queue<pii> q;      /* <fret,string> */
    int mi=INF; /* lowest fret used */
    for(int i=0; i<n; i++) {
        q.emplace(b[i]-a[0], 0);
        mi=min(mi, b[i]-a[0]);
    }

    int ans=q.top().first-mi;

    int cnt=0;
    while(cnt<n) {
        auto [fret, idx]=q.top();
        q.pop();
        //cerr<<"ans0="<<ans<<" ";
        ans=min(ans, fret-mi);
        //cerr<<"ans="<<ans<<" fret="<<fret<<" mi="<<mi<<endl;

        //assert(idx+1<6);

        if(idx+1<6) {
            fret+=a[idx];
            fret-=a[idx+1];
            //cerr<<"push fret="<<fret<<" idx="<<idx+1<<endl;
            q.emplace(fret, idx+1);
            if(idx+1==5)
                cnt++;
            mi=min(mi, fret);
        }  else
            break;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
