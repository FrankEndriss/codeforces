/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 *
 * There are some cases
 * s is reversed and forms an palindrome with all strings t that
 * t==reverse(s), or
 * case s.size()==1
 * 	t.size()==2 && t[0]==s[0]
 * 	t.size()==3 && t[0]==s[0] && t[1]==t[2]
 * case s.size()==2
 *      t.size()==1 && t[0]==s[0]
 *      t.size()==2 && t[0,1]=s[1,0]	(reverse, see above)
 *      t.size()==3 && t[0,1]=s[1,0]
 * case s.size()==3
 * 	t.size()==1 && t[0]==s[2]
 * 	...
 */
void solve() {
	cini(n);
	map<string,bool> m;
	
	bool ans=false;
	for(int i=0; i<n; i++) {
		cins(s);
		if(s[0]==s.back()) 
			ans=true;

		string t=s;
		reverse(all(t));
		if(m[t])
			ans=true;
		m[s]=true;
	}
	if(ans)
		cout<<"YES"<<endl;
	else
		cout<<"NO"<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
