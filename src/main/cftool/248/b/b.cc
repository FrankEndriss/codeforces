/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void test() {
const int INF=1e18;
    int ten=10;
    for(int i=2; i<18; i++) {
        ten*=10;
        int x=ten;
        while(x%7!=0)
            x++;
        cout<<i<<": "<<x<<endl;
    }
}

int digs(int i) {
    int ans=0;
    while(i) {
        ans+=i%10;
        i/=10;
    }
    return ans+1;
}

void solve() {
    cini(n);
    if(n<3) {
        cout<<-1<<endl;
        return;
    } else if(n==3) {
        cout<<210<<endl;
        return;
    }

    vi fi={ 6, 4, 5, 1, 3, 2 };

    int dig=fi[(n-1)%6];
    while(dig%2!=0 || digs(dig)%3!=0 || dig%5!=0)
        dig+=7;

    string ans="1";
    while(ans.size()<n)
        ans+="0";

    int idx=n-1;
    while(dig) {
        ans[idx]='0'+dig%10;
        dig/=10;
        idx--;
    }

    cout<<ans<<endl;
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
//test();
    solve();
}

