/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* How to move min distances?
 * Has something to do with median.
 *
 * Consider some "center" position.
 * Move all people in direction to this position,
 * once start L then start R.
 * How to implement?
 * Twice from left, second time with reversed a[].
 *
 * ...ah, no.
 * ****
 * Consider left prefix of the seats.
 * If in a prefix the number of free is bigger than the number of occ
 * then we need to move people left, else right.
 * Check this foreach prefix, so it determines the direction of move.
 *
 * But, it could be beneficial to move in both directions...somehow :/
 *
 * ***
 * Greedy.
 * Move each person to the nearest seat, order by dist.
 * How to implement? Linked list and simulate?
 * But how to know if moving a person left or right?
 * ****
 * dp[i][j]=min moves to fix subseg (i,j), free(i,j)>=occupied(i,j) else INF
 * How the transition works?
 * ***
 * Problem is, the opt direction to move the people may change for segments, consider
 * 0001111000111110000
 * How can we know if it is optimal to move people into the middle gab, and when not?
 *
 * Consider a prefix. We can move each person in that prefix left or right, leaving us
 * with a different list of occ seats.
 * However, moving a person is allways optimal to the next free seat, but 
 * we do not know with direction.
 *
 * ***
 * Consider the leftmost person:
 * If the seat left of it is free it is allways optimal to move it there.
 * Else it is optimal to move it to the left most free seat right of it.
 * Consider the second person:
 * There are free seats at left, and at right of it in some distance.
 * How can we know which one to use?
 * If moving it to the right, it would never be optimal to move any other person
 * to a seat left of it. Same goes for the left position.
 * So we get dp[i][j]=
 *   min cost to fix i persons with rightmost used seat is j-1, so j is the first useable one.
 *
 * Then move each person to the left or to the right.
 * But we somehow have to handle the impossible positions...
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    vi fre;
    for(int i=0; i<n; i++) 
        if(a[i]==0)
            fre.push_back(i);

    vi dp(n+1);
    for(int i=0; i<n; i++) {
        if(!a[i])
            continue;

        vi dp0(n+1, INF);

        for(int j=0; j<n; j++) {
            /* move to left */
            auto it=lower_bound(all(fre), j);
            if(it!=fre.end()) {
                dp0[*it]=min(dp[*it], dp[j]+abs(i-*it));
            }
            it=lower_bound(all(fre), i);
            if(it!=fre.end()) {
                dp0[*it]=min(dp[*it], dp[j]+abs(i-*it));
            }
        }
        dp.swap(dp0);
    }

    int ans=*min_element(all(dp));
    cout<<ans<<endl;

}

signed main() {
    solve();
}
