/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Position of a robot:
 * x0 + t* + ...
 * Obviously robots going the same dir collide if the distance is even.
 * robots going oposite dirs also collide if distance is even.
 *
 * How to find the times when (and if) each one explodes?
 * Robots move like on a circle...
 * Give a robot r0, how to find the robot coliding with that one first?
 * Say r0 start in dir d0, then it collides with a robot right of it
 * -if the other robot is oposite dir and even dist (t=dist/2)
 * -if the other robot is same dir, and even dist (t=distBorderOfOther+dist/2)
 * and with a robot left of it
 * -if the other robot is same dir and even dist  (t=distBorder+dist/2)
 * -if the other robot is oposite dir, 
 *  -- and m+dist even (t=distBorder+distBorderOfOther+m/2)
 *  -- and m+dist odd  (t=...same but one round later...)
 *
 *  How to find the order of collides?
 *  We could find foreach robot the t it collides with another one,
 *  then sort these events by t...so kind of simulate it.
 *
 *  How to find quick the next event?
 *  Consider the robots with same direction and same positionParity,
 *  we will hit the rightmost one first.
 *  Consider the robots with opp direction and same position Parity,
 *  ...
 *
 *  That is to much cases to implement :/
 */
void solve() {
    cini(n);
    cini(m);
    cinai(x,n);
    cins(s);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
