/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* We can ignore D spiders.
 * Nom will meet even U spiders per column.
 * In every row he will meet the
 * Rs left of him in even dists.
 * Ls right of him in even dists.
 * So we do range queries separated by parity.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vi cols(m); /* cols[i]=num spiders in col i in even positions. */

    cinas(s,n);

    for(int j=0; j<m; j++) {
        int ans=0;

        for(int i=1; i<n; i++) {
            if(i%2==0 && s[i][j]=='U')
                ans++;

            /* Rs left of and at j-i */
            if(j-i>=0 && s[i][j-i]=='R')
                ans++;

            if(j+i<m && s[i][j+i]=='L')
                ans++;
        }
        cout<<ans<<" ";
    }
    cout<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

