/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to count and maintain the zeros.
 * ... and the sum.
 *
 * A pair i,j contributes if 
 * 
 * pre[j]-pre[i-1]==j-(i-1)
 * pre[j]-j == pre[i-1]-(i-1)
 *
 * Something wrong... idk :/
 */
void solve() {
    cini(n);
    cins(s);
    assert(s.size()==n);

    vi pre(n+1);
    for(int i=1; i<=n; i++) 
        pre[i]=pre[i-1]+s[i-1]-'0';

    for(int i=1; i<=n; i++)
        pre[i]-=i;

    map<int,int> f;
    for(int i=1; i<=n; i++)
        f[pre[i]]++;

    int ans=0;
    for(int i=1; i<=n; i++) {
        f[pre[i]]--;
        ans+=f[pre[i]];
    }

    cout<<ans/2<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
