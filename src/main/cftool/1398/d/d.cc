/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vs= vector<string>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;

#define endl "\n"

/* greedy, always choose the biggest ones
 *
 * No! It can be better to create more triangels,
 * if of one kind are more available than the others.
 *
 * Sic, we need to db all possible  combinations...somehow.
 *
 * We can build the first
 */
void solve() {
    cinai(cnt,3);

    vvi rgb(3);
    for(int i=0; i<3; i++)  {
        for(int j=0; j<cnt[i]; j++) {
            cini(aux);
            rgb[i].push_back(aux);
        }
        sort(all(rgb[i]), greater<int>());
    }

    // dp[r][g][b]=max score with r, g, b available elements
    vvvi dp(cnt[0]+1, vvi(cnt[1]+1, vi(cnt[2]+1, -1)));

    function<int(vi&)> calc=[&](vi& ii) {
        //cerr<<"calc, i0="<<ii[0]<<" i1="<<ii[1]<<" i2="<<ii[2]<<endl;
        if(dp[ii[0]][ii[1]][ii[2]]>=0)
            return dp[ii[0]][ii[1]][ii[2]];

        dp[ii[0]][ii[1]][ii[2]]=0;
        for(int i=0; i<3; i++) {
            for(int j=i+1; j<3; j++) {
                if(ii[i]==0 || ii[j]==0)
                    continue;

                vi ii2=ii;
                ii2[i]--;
                ii2[j]--;
                dp[ii[0]][ii[1]][ii[2]]=max(dp[ii[0]][ii[1]][ii[2]], rgb[i][cnt[i]-ii[i]]*rgb[j][cnt[j]-ii[j]] + calc(ii2));
            }
        }
        //cerr<<"return="<< dp[ii[0]][ii[1]][ii[2]]<<endl;
        return dp[ii[0]][ii[1]][ii[2]];
    };
    
    vi ii={ cnt[0], cnt[1], cnt[2] };
    int ans=calc(ii);

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
