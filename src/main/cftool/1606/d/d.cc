/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * big numbers have to be red/left or blue/right,
 * sml numbers have to be blue/left or red/right.
 *
 * Foreach column-cut we have two max values in each row, left and right.
 * We search for a cut where x smallest rows from left are exactly
 * the x biggest from right.
 *
 * Just brute force find them, using prefix sums?
 *
 * ...idk :/
 */
void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));
    vvi preL(n, vi(m+1));
    vvi preR(n, vi(m+1));
    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++) {
            cin>>a[i][j];
            preL[i][j+1]=max(preL[i][j], a[i][j]);
        }
    }
    for(int i=0; i<n; i++)  {
        for(int j=m-1; j>=0; j--) {
            preR[i][j]=max(preR[i][j+1], a[i][j]);
        }
    }

    for(int j=0; j+1<m; j++) {
        vector<pii> le(n);
        vector<pii> ri(n);
        for(int i=0; i<n; i++) {
            le[i]={ preL[i][j+1], i};
            ri[i]={ preR[i][j], i};
        }
        sort(all(le));
        sort(all(ri), greater<pii>());

        vector<bool> vle(n);
        vector<bool> vri(n);
        for(int i=0; i<n; i++) {
            if(le[i].first>ri[i].first)
                break;

            vle[le[i].second]=true;
            vri[ri[i].second]=true;

            if(vle==vri) {
                cout<<"YES"<<endl;
                for(int k=0; k<n; k++) {
                    if(vle[k])
                        cout<<'B';
                    else
                        cout<<'R';
                }
                cout<<" ";
                cout<<i<<endl;
                return;
            }
        }
    }

    cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
