/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to find the smallest number not representable
 * by k terms.
 *
 * Consider not having a note of p.
 * Then we need 10*pos(p) notes to represent that number.
 *
 * Use at most possible 9th.
 *
 * We need to use at most possible small notes.
 * Foreach note we need to check how much of them we can use at most, and just do that.
 */
void solve() {
    cini(n);
    cini(k);

    vb a(20);
    for(int i=0; i<n; i++) {
        cini(aa);
        a[aa]=true;
    }

    int kk=k;
    vi d;
    while(kk) {
        d.push_back(kk%10);
        kk/=10;
    }
    while(d.size()<20)
        d.push_back(0);

    vi ans(20);

    int num=1;
    int prev=-1;
    a[18]=true;

    k++;
    for(int i=0; k>=0 && i<=18; i++) {
        if(a[i]) {
            if(prev>=0) {
                ans[prev]=min(k, num*10-1);
                k-=ans[prev];
            }
            num=1;
            prev=i;
        } else 
            num*=10;
    }

    int aans=0;

    int ten=1;
    for(int i=0; i<18; i++) {
        aans+=ten*ans[i];
        ten*=10;
    }

    cout<<aans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
