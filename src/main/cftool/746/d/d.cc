/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cini(k);
    cini(a);
    cini(b);

    vector<char> c={ 'G', 'B' };
    if(a>b) {
        swap(a,b);
        swap(c[0], c[1]);
    }

    int cnt=a+1;
    int ccnt=cnt;
    for(int i=0; i<cnt; i++) {
        int ans;
        if(b%ccnt==0)
            ans=b/ccnt;
        else
            ans=b/ccnt+1;

        if(ans>k) {
            cout<<"NO"<<endl;
            return;
        }

        for(int j=0; j<ans; j++) {
            if(b) {
                cout<<c[1];
                b--;
            }
        }
        if(a) {
            cout<<c[0];
            a--;
        }
        ccnt--;
    }
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

