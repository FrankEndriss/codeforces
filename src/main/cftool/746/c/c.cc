/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

int sig(int i) {
    if(i<0)
        return -1;
    else
        return 1;
}

/* if tram is faster then person it is best
 * to jumb into the tram whenever it passes along
 * in the right direction.
 *
 * see tutorial: If the tram is faster than the
 * person is does not matter where the person
 * enters the tram. 
 * The tram will arrive at the end when it is there,
 * it just have to pick up the person at x1 first.
 *
 * ans=min(personTime, tramTime)
 */
void solve() {
    cini(s);    // point of reverse
    cini(x1);   // start from
    cini(x2);   // to

    cini(t1);   // pace of tram time/way
    cini(t2);   // pace of person

    cini(p);    // pos tram at t of start
    cini(d);    // dir of tram at t of start -1/1

    int ans=abs(x2-x1)*t2; // just walk

/* casework of tram directions. We first need to 
 * start at p and pick up person at x1.
 */
    int x1pshort=abs(p-x1)*t1;
    int x1plong=0;
    if(p<x1) 
        x1plong=(x1+p)*t1;
    else if(p>x1)
        x1plong=(s-x1+s-p)*t1;

/* basically the same for going from x1 to x2.
 */
    int x1x2short=abs(x1-x2)*t1;
    int x1x2long=0;
    if(x1<x2) 
        x1x2long=(x1+x2)*t1;
    else if(x1>x2)
        x1x2long=(s-x1+s-x2)*t1;

/* put together the cases */
    int ans2=0;
    if(x1==p || sig(x1-p)==d) {
        ans2=x1pshort;
        if(sig(x2-x1)==d)
            ans2+=x1x2short;
        else
            ans2+=x1x2long;
    } else {
        ans2=x1plong;
        if(sig(x2-x1)==-d) {
            ans2+=x1x2short;
        } else {
            ans2+=x1x2long;
        }
    }

    cout<<min(ans, ans2)<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
