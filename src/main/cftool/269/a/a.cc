/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We can start with smallest boxes and count min number of
 * next box sizes...until all boxes are contained in one last box
 *
 * Note that the boxes always must be put into another, outer box,
 * even if it is only one.
 */
void solve() {
    cini(n);    
    map<int,int> f;
    for(int i=0; i<n; i++) {
        cini(k);
        cini(a);
        f[k]=a;
    }

    int prev=0;
    int prevk=0;
    for(auto ent : f) {
        while(prevk<ent.first && prev>4) {
            prev=(prev+3)/4;
            prevk++;
        }
        prev=max(prev, ent.second);
        prevk=ent.first;
    }

    bool first=true;
    while(first || prev>1) {
        prev=(prev+3)/4;
        prevk++;
        first=false;
    }

    cout<<prevk<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
