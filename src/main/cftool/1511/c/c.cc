/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*  On query we need to find the leftmost t[q], 
 *  then move that element to position 0.
 *
 * Maintain array of position by color and inctime that position was set.
 * Then, on query, the positon of that color is lastset position
 * plus number of set other colors since then that where at lower
 * positions.
 *
 * So, if a color is queried, we need to update all other colors
 * at positions before that color.
 * ...Simply do that.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    vector<int> pos(51, INF);  
    for(int i=0; i<n; i++) 
        pos[a[i]]=min(pos[a[i]], i+1);

    for(int i=0; i<q; i++) {
        cini(t);
        int ans=pos[t];
        cout<<ans<<" ";
        for(int j=1; j<=50; j++) 
            if(pos[j]<pos[t])
                pos[j]++;
        pos[t]=1;
    }
    cout<<endl;
}

signed main() {
    solve();
}
