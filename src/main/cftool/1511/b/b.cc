/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* if a==c, then na=nc, and nb is a multiple of a.
 * if(c<a...
 *
 * nc=gcd(na,nb);
 *
 * We choos nc to be 100...0, c digits
 * na is 100...0, a digits
 * nb is 100...0, b digits
 * ...no
 * Example, c=2, b=3, a=4
 * c= 11
 * b=110
 * a=121
 * a=c*b ???
 *
 * Example, c=8, b=9, a=9
 * c=11111111
 * b=111111110
 * a=122222221
 *
 */
void solve() {
    cini(a);
    cini(b);
    cini(c);

    bool sw=false;
    if(a<b) {
        swap(a,b);
        sw=true;
    }

    int nc=0;
    int nb=0;
    for(int i=0; i<c; i++) {
        nc*=10;
        nc++;
        nb*=10;
        nb++;
    }
    for(int i=c; i<b; i++) {
        nb*=10;
    }
    int na=nb;
    for(int i=b; i<a; i++)
        na*=10;

    na+=nc;

    if(sw)
        swap(na,nb);
    //cerr<<"gcd="<<gcd(na,nb)<<endl;
    cout<<na<<" "<<nb<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
