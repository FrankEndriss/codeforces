/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

/* How to do that?
 * Count white cells to be w.
 * Then when we have two horz adjacent white cells, we can paint
 * them in 2^(w-2) configuration so that we can place a domino there.
 * Same for two vert adjacent white cells.
 *
 * But it is not that simple.
 * Consider
 * *oo
 * ooo
 * The horz values in below row are not 2, since there are confgurations
 * where they overlap.
 *
 * So, for both positions there are 2^3 configurations, but 2^2 of them
 * cause one domino less.
 * So, foreach triple we need to divide by www.
 *
 * What about
 * oooo
 * rrrr
 * rrrb
 * rrbb
 * rbbb
 * ....
 * A row of x (x>=2) white cells counts:
 * 2^(x-2) * x/2 + 2^(x-3)* (x-1)/2
 */
void solve() {
    cini(n);
    cini(m);
    cinas(s,n);

    int w=0;
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++)
            if(s[i][j]=='o')
                w++;

    vvi hcnt(n, vi(m+1));
    for(int i=0; i<n; i++) 
        for(int j=1; j<=m; j++) {
            if(s[i][j-1]=='o') {
                hcnt[i][j]=hcnt[i][j-1]+1;
                hcnt[i][j-1]=0;
            }
        }

    vvi vcnt(n+1, vi(m));
    for(int i=1; i<=n; i++) 
        for(int j=0; j<m; j++) {
            if(s[i-1][j]=='o') {
                vcnt[i][j]=vcnt[i-1][j]+1;
                vcnt[i-1][j]=0;
            }
        }

    if(w<2) {
        cout<<0<<endl;
        return;
    }

    int ans=0;
    int ww=toPower(2, w-2);
    int www=0;
    if(w>=3)
        www=toPower(2, w-3);

    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            if(hcnt[i][j+1]>1)
                ans=pl(ans, pl(mul(ww, hcnt[i][j+1]/2), mul(www, (hcnt[i][j+1]-1)/2)));
            if(vcnt[i+1][j]>1)
                ans=pl(ans, pl(mul(ww, vcnt[i+1][j]/2), mul(www, (vcnt[i+1][j]-1)/2)));
        }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
