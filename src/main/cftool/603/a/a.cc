/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* if we find two same adjacent we can inc 1.
 * if we find three same adjacent we can inc 2.
 *
 * if we find two two-blocks then inc 2, too.
 */
void solve() {
    cini(n);
    cins(s);

    if(n==1)  {
        cout<<1;
        return;
    } else if(n==2) {
        cout<<2<<endl;
        return;
    }

    int ans=1;
    int bl2=0;
    int bl3=0;
    int pprev=s[0];
    int prev=s[1];
    if(prev!=pprev)
        ans++;
    else
        bl2=1;

    for(int i=2; i<n; i++) {
        if(s[i]==prev) {
            bl2++;
            if(prev==pprev)
                bl3++;
        } else
            ans++;

        pprev=prev;
        prev=s[i];
    }

    int inc=0;
    if(bl3 || bl2>1)
        inc=2;
    else if(bl2)
        inc=1;

    cout<<ans+inc<<endl;
    
}

signed main() {
    solve();
}

