/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The difficulty of all towers is the number 
 * of continous segments on all towers.
 *
 * If two towers are merged, some of those segments
 * get merged into one.
 *
 * let towers be vector<pii>, then merging is
 * efficient with two pointer.
 */
void solve() {
    cini(n);
    cini(m);
    vvi init(n);
    for(int i=0; i<n; i++) {
        cini(t);
        t--;
        init[t].push_back(i);
    }

    vector<vector<pii>> to(n);
    int ans=0;
    for(int i=0; i<n; i++) {
        if(init[i].size()==0)
            continue;

        int prev=-2;
        pii p={ init[i][0], init[i][0] };
        to[i].push_back(p);
        for(size_t j=1; j<init[i].size(); j++) {
            if(init[i][j]==init[i][j-1]+1)
                to[i].back().second=init[i][j];
            else {
                to[i].pus_back({ init[i][j], init[i][j]});
            }
        }
        ans+=to[i].size();
    }

    for(int i=0; i<m-1; i++) {
        cini(a); a--;
        cini(b); b--;

        if(to[b].size()==0) {
            cout<<ans<<endl;
            continue;
        }

        if(to[a].size()==0) {
            cout<<ans<<endl;
            to[a].swap(to[b]);
            continue;
        }

        int ia=0;
        int ib=0;
        vector<pii> c;

        pii prev;
        if(to[a][0]<to[b][0]) {
            prev=to[a][0];
            ia++;
        } else {
            prev=to[b][0];
            ib++;
        }

        while(ia<a.size() || ib<b.size()) {
            if(ia==a.size()) {
                if(prev.second+1==to[b][ib].first) {
                    prev.second=to[b][ib].second;
                    ans--;
                } else {
                    c.push_back(prev);
                    prev=to[b][ib];
                }
                ib++;
                continue;
            }

            if(ib==b.size()) {
                if(prev.second+1==to[a][ia].first) {
                    prev.second=to[a][ia].second;
                    ans--;
                } else {
                    c.push_back(prev);
                    prev=to[a][ia];
                }
                ia++;
                continue;
            }


            pii p;
            if(to[a][ia].first<to[b][ib]) {
                p=to[a][ia];
                ia++;
            } else {
                p=to[b][ib];
                ib++;
            }
            if(prev.second+1==p.first) {
                prev.second=p.second;
                ans--;
            } else {
                c.push_back(prev);
                prev=p;
            }
        }
        c.push_back(prev);
        cout<<ans<<endl;
        
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
