/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    cinai(p,n);
    vi pos(n+1);
    for(int i=0; i<n; i++) 
        pos[p[i]]=i+1;

    vi ma(n);
    vi mi(n);

    ma[0]=p[0];
    for(int i=1; i<n; i++)
        ma[i]=min(ma[i-1], p[i]);

    mi[n-1]=p[n-1];
    for(int i=n-2; i>=0; i--)
        mi[i]=min(mi[i+1], p[i]);

    for(int j=1; j<n-1; j++)  {
        if(ma[j-1]<p[j] && mi[j+1]<p[j]) {
            cout<<"YES"<<endl;
            cout<<pos[ma[j-1]]<<" "<<j+1<<" "<<pos[mi[j+1]]<<endl;
            return;
        }
    }
    cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
