/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The difficulty of all towers is the number
 * of continous segments on all towers.
 *
 * If two towers are merged, some of those segments
 * get merged into one.
 *
 * Let towers be set<int>, then merging is
 * efficient with smaller to bigger.
 * Just count the segment borders.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    vector<set<int>> to(n);
    int ans=n-1;
    for(int i=0; i<n; i++) {
        cini(t);
        t--;
        ans-=to[t].count(i-1);
        ans-=to[t].count(i+1);
        to[t].insert(i);
    }
 
    for(int i=0; i<m-1; i++) {
        cout<<ans<<endl;
        cini(a); a--;
        cini(b); b--;

        if(to[a].size()<to[b].size())
            to[a].swap(to[b]);

        while(to[b].size()) {
            auto it=to[b].begin();
            ans+=to[b].count((*it)+1);
            ans-=to[a].count((*it)-1);
            ans-=to[a].count((*it)+1);
            to[a].insert(*it);
            to[b].erase(it);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
