/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to destroy n-m warriors.
 *
 * cost x Fireball kills k warriors.
 * cost y Berserk kills one.
 *
 * b[] must be a subsequence of a[]
 *
 * Its a dp.
 * cost(int idxa, int idxb)
 *
 * Note that powers are distinct, so there is 
 * only one possible subseq.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    cini(x);
    cini(k);
    cini(y);
    cinai(a,n);
    cinai(b,m);

    vi s0; /* subseq */
    s0.push_back(-1);
    int idx=0;
    for(int i=0; i<n; i++) {
        if(a[i]==b[idx]) {
            s0.push_back(i);
            idx++;
        }
        if(idx==m)
            break;
    }
    if(idx<m) {
        cout<<-1<<endl;
        return;
    }
    s0.push_back(n);

/* @return min cost detroying l..r, INF if impossible. 
 *
 * how to implement?
 * if biggest warrior is bigger left and right we have
 * to use fireball at least once.
 * So we can choose to destroy the other by fireball
 * and/or spell, than use one fireball.
 * Else we can simply choose the cheaper variant.
 * */
    function<int(int,int)> cost=[&](int l, int r) -> int {
        if(l>r)
            return 0LL;

        int cnt=r-l+1;
        int ans=0;
        int ma=*max_element(a.begin()+l, a.begin()+r+1);

        int left=-1;
        if(l>0)
            left=a[l-1];
        int right=-1;
        if(r+1<n)
            right=a[r+1];

        if(ma>left && ma>right) {   /* must use fireball at least once */
            if(cnt<k)
                return INF;

            ans+=x;
            cnt-=k;
        }

        if(k*y>x) {
            ans+=x*(cnt/k);
            cnt%=k;
        }

        ans+=cnt*y;
        return ans;
    };

    /* now destroy consecutive segments in a */
    int ans=0;
    for(size_t i=1; i<s0.size(); i++)  {
        ans+=cost(s0[i-1]+1, s0[i]-1);
        if(ans>INF) {
            ans=INF;
            break;
        }
    }

    if(ans==INF)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
