/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*  greedy/dp
 *  Let x(l) be k*y%(10^l)
 *  Ie. x is the smallest multiple of k with l digits
 *
 *  sol(x)=sol(x-1)*10
 *
 * ***
 *  Number of numbers with n digits divisable by k is
 *  (10^(n+1))/k - (10^n)/k
 *
 * But we need to add the numbers for n-1, but only 
 * those wich are not a postfix of one of the numbers
 * of n.
 *
 * The postfixes somehow repeat with periode gcd(100, k)
 */
void solve() {
    cini(n);
    cini(m);
    cini(MOD);

    vvi dp(n+1, vi(10));   /* dp[i][j]=number of sulutions for n=i */

    int kk=k;
    while(kk<10) {
        dp[0][kk]++;
        kk+=k;
    }

    for(int i=1; i<=n; i++) {
    }

    while(kk<100) {
        dp[1]++;
        kk+=k;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
