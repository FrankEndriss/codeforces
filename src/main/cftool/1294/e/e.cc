/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to solve each col separate.
 * foreach col we need to find the 
 * optimal shift. This is, the shift where
 * most of the values are in correct position.
 * How to to that in O(n) or O(nlogn)?
 * We collect the frequency of the offsets
 * of the position of elements to the target
 * position.
 */
void solve() {
    cini(n);
    cini(m);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            cin>>a[i][j];

    int ans=0;
    for(int j=0; j<m; j++) {
        map<int,int> f; /* f[i]= freq of offset i in col j*/
        for(int i=0; i<n; i++) {
            if((a[i][j]-1-j)%m==0) {
                int target=(a[i][j]-1-j)/m;
                if(target>=0 && target<n) 
                    f[(i-target+n)%n]++;
            }
        }
        int lans=n;
        for(auto ent : f)  {
            lans=min(lans, n+ent.first-ent.second);
            //cerr<<"col="<<j<<" f["<<ent.first<<"]="<<ent.second<<" lans="<<lans<<endl;
        }

        ans+=lans;
    }

    cout<<ans<<endl;


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
