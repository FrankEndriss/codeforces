/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* it is opt to give the smallest cups to the girls.
 * Let x= amount that girls get
 **/
void solve() {
    cini(n);
    cind(w);

    cinai(a,2*n);
    sort(a.begin(), a.end());

    double x=min(1.0*a[0], 1.0*a[n]/2.0);
    x=min(x, w/(3*n));

    cout<<setprecision(10)<<fixed<<x*3*n<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

