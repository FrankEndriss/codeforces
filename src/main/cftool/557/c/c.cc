/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to remain some same sized legs of the table.
 * We must remove all longer legs, and depending on 
 * the number of remaining longest legs, some smaller ones, too.
 *
 * So, we can iterate the leg lengths biggest first, remove that
 * biggest legs, and foreach length,
 * calculate how much remaining cheapest legs (of smaller length) we need to remove
 * and how much cost that is.
 *
 * How to implement?
 * Maintain a lists of legs, sorted by length desc
 * Maintain an array of lists, list of length of legs by costs
 *
 * Since we need to iterate the legs in order of leg length,
 * we need to maintain a vector<pii> of the <len,cost>
 *
 * But also we need to maintain the prefix sum on the
 * legs sorted by cost, that where not removed so far. 
 *
 * Do that with a segtree, <cost,len,cnt>, entries sorted by cost
 * and cnt=1 for not removed, else 0.
 *
 * Build sum of both values in prefix.
 * Then, to find the cost of removing x elements we can binsearch
 * for the position where cnt eq x, and sum of cost is ans.
 * But how comes into play that we must not remove the longest length?
 * -> Just remove them before querying the cheapest legs to remove.
 */
const int INF=1e9;
void solve() {
    cini(n);

    vector<pii> a(n);  /* <len,cost> */
    vvi b(201); /* b[i]=list of lengths of legs of cost i */

    for(int i=0; i<n; i++)
        cin>>a[i].first;

    for(int i=0; i<n; i++)  {
        cin>>a[i].second;
        b[a[i].second].push_back(a[i].first);
    }

    sort(all(a));
    for(int i=0; i<201; i++) 
        sort(all(b[i]));

    int ans=INF;
    int sum=0;      /* current cost of removing the longest legs */
    int idx=n-1;    /* idx of longest remaining leg */
    int remaining=n;    /* current number of remaining legs */
    while(idx>=0) {
        int sum0=sum;
        int maxlen=a[idx].first;
        int cnt=0;

        while(idx>=0 && a[idx].first==maxlen) {
            cnt++;
            sum0+=a[idx].second;
            assert(b[a[idx].second].back()==maxlen);
            b[a[idx].second].pop_back();
            idx--;
        }

        int mcnt=cnt*2-1;
        int rem=max(0LL, remaining-mcnt);
        int lans=0;
        for(int i=1; i<201 && rem>0; i++) {
            lans+=min(rem, (int)b[i].size())*i;
            rem-=min(rem,(int)b[i].size());
        }
        assert(rem==0);
        ans=min(ans, sum+lans);
        sum=sum0;
        remaining-=cnt;
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
