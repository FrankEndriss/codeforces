/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cins(s);
    string ss;
    vi len;
    char prev=s[0];
    len.push_back(0);
    for(size_t i=0; i<s.size(); i++) {
        if(prev!=s[i]) {
            ss+=prev;
            len.push_back(1);
        } else {
            len.back()++;
        }
        prev=s[i];
    }
    ss+=prev;

//cout<<"s="<<s<<" ss="<<ss<<endl;

    if(len.size()%2==0) {
        cout<<0<<endl;
        return;
    }

    int l=0;
    int r=len.size()-1;
    while(l<=r) {
        if(l==r) {
            if(len[l]<2) {
                cout<<0<<endl;
            } else {
                cout<<len[l]+1<<endl;
            }
            return;
        }
        if(ss[l]!=ss[r] || len[l]+len[r]<3) {
            cout<<0<<endl;
            return;
        }
        l++;
        r--;
    }
    assert(false);
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

