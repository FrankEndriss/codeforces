/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* s==size
 * m==number of screens
 *
 * let f[i] be freq of some c[j]
 * s <= min(f[i])+1
 * all other f[i] must be deployable on s. That is,
 * if f[i]%s==0 || f[i]%s==s-1
 * !!!NO we can make more than one screen with s-1 icons!!!
 * let mm=f/s;
 * s is ok if f[i]%s+mm >= s-1
 *
 * Finding minimal m is finding maximal s.
 * Brute force?  binary search does not work.
 * Note that s==2 always works.
 */
void solve() {
    cini(n);
    map<int,int> f;
    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux]++;
    }

/* @return true if s works */
    auto check=[&](int s) {
        for(auto ent : f) {
            int mm=ent.second/s;
            int mod=ent.second%s;
            if(mod==0 || mod+mm>=s-1) {
                ;
            } else {
                return false;
            }
        }
        return true;
    };

    int k=f.begin()->second+1;
    for(; k>2; k--) {
        if(check(k))
            break;
    }
    int ans=0;
    for(auto ent : f) 
        ans+=(ent.second+k-1)/k;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
