/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vvd= vector<vd>;
using vvvd= vector<vvd>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vvvb= vector<vvb>;

#define endl "\n"

/**
 * dp[r][s][p]=1
 * Then iterate all states down to 0,0,0
 */
void solve() {
    cini(rr);
    cini(ss);
    cini(pp);

    vvvd dp(rr+1, vvd(ss+1, vd(pp+1)));
    vvvb vis(rr+1, vvb(ss+1, vb(pp+1)));
    dp[rr][ss][pp]=1;
    vis[rr][ss][pp]=true;
    using t3=tuple<int,int,int>;
    queue<t3> q;
    q.emplace(rr,ss,pp);
    while(q.size()) {
        auto [r,s,p]=q.front();
        q.pop();
        if(r+s+p==r || r+s+p==s || r+s+p==p)
            continue;
        const int rs=r*s;
        const int sp=s*p;
        const int rp=r*p;
        const int rsp=rs+sp+rp;

        if(r>0 && p>0) {
            dp[r-1][s][p]+=dp[r][s][p]*rp/rsp;
            if(!vis[r-1][s][p]) {
                vis[r-1][s][p]=true;
                q.emplace(r-1,s,p);
            }
        }
        if(s>0 && r>0) {
            dp[r][s-1][p]+=dp[r][s][p]*rs/rsp;
            if(!vis[r][s-1][p]) {
                vis[r][s-1][p]=true;
                q.emplace(r,s-1,p);
            }
        }
        if(p>0 && s>0) {
            dp[r][s][p-1]+=dp[r][s][p]*sp/rsp;
            if(!vis[r][s][p-1]) {
                vis[r][s][p-1]=true;
                q.emplace(r,s,p-1);
            }
        }
    }
    ld ansR=0;
    for(int i=1; i<=rr; i++)
        ansR+=dp[i][0][0];
    ld ansS=0;
    for(int i=1; i<=ss; i++)
        ansS+=dp[0][i][0];
    ld ansP=0;
    for(int i=1; i<=pp; i++)
        ansP+=dp[0][0][i];

    cout<<ansR<<" "<<ansS<<" "<<ansP<<endl;
}

signed main() {
        solve();
}
