/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Two cases:
 * Target cracked or not cracked.
 * In case cracked we need to find a way to walk to it, fini.
 * Extra case if start==fini.
 *
 * In case target not cracked we need to find a path going throug
 * target twice. How?
 * Ok, we need to find a path to target, and there must be a second cell
 * next to target not cracked. Simple as that.
 *
 * So do dijkstra
 */
void solve() {
const vi dx={-1, 1, 0, 0};
const vi dy={ 0, 0,-1, 1};

    cini(n);
    cini(m);
    cinas(s,n);
    cini(r1);
    cini(c1);
    cini(r2);
    cini(c2);
    r1--; c1--; r2--; c2--;

    if(r1==r2 && c1==c2) {
        for(int i=0; i<4; i++) {
            int x=dx[i]+r1;
            int y=dy[i]+c1;
            if(x>=0 && x<n && y>=0 && y<m && s[x][y]=='.') {
                cout<<"YES"<<endl;
                return;
            }
        }
        cout<<"NO"<<endl;
        return;
    }

    const int INF=1e9;
    vvi dp(n, vi(m, INF));
    queue<pii> q;
    q.emplace(r1,c1);
    dp[r1][c1]=0;
    while(q.size()) {
        auto [r,c]=q.front();
        q.pop();
        for(int i=0; i<4; i++) {
            int rr=r+dx[i];
            int cc=c+dy[i];
            if(rr>=0 && rr<n && cc>=0 && cc<m && ((rr==r2 && cc==c2) || s[rr][cc]=='.') && dp[rr][cc]==INF) {
                dp[rr][cc]=dp[r][c]+1;
                q.emplace(rr,cc);
            }
        }
    }

    if(dp[r2][c2]==INF)
        cout<<"NO"<<endl;
    else {
        if(s[r2][c2]=='X')
            cout<<"YES"<<endl;
        else {
            int cnt=0;
            for(int i=0; i<4; i++) {
                int r=r2+dx[i];
                int c=c2+dy[i];
                if(r>=0 && r<n && c>=0 && c<m && dp[r][c]!=INF)
                    cnt++;
            }
            if(cnt>=2)
                cout<<"YES"<<endl;
            else
                cout<<"NO"<<endl;
        }
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
