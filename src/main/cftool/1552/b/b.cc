
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Is this randomized solution expected?
 * If we simply brute force check we should quickly find
 * all athletes not able to win.
 * ***
 *
 * Consider all 3/5 combinations.
 *
 * Sort the athletes by this.
 * If one is the best in all 3, he can win.
 * No, thats bullshit. 
 *
 * ****
 * We want to find one athlete that has no superior opponent.
 * ****
 * Just check all pairs, but sort out all not superior athletes.
 * So with each comparison at least one athlete is sorted out.
 *
 * Why this TLE?
 * Consider list sorted worst first.
 * Then each one will loop until own index+1
 *
 * If a>b && b>c, is then a>c?
 * If yes, then we can simply sort and check if the first works.
*/
void solve() {
    cini(n);

    vvi r(n, vi(6));
    for(int i=0; i<n; i++) {
        for(int j=0; j<5; j++)
            cin>>r[i][j];
        r[i][5]=i;
    }

    sort(all(r), [](vi r1, vi r2) {
            int cnt=0;
            for(int i=0; i<5; i++) 
                if(r1[i]<r2[i])
                    cnt++;

            return cnt>=3;
    });

    bool ok=true;
    for(int i=1; i<n; i++) {
            int cnt=0;
            for(int j=0; j<5; j++) 
                if(r[0][j]<r[i][j])
                    cnt++;

            if(cnt<3)
                ok=false;
    }
    if(ok)
        cout<<r[0][5]+1<<endl;
    else
        cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
