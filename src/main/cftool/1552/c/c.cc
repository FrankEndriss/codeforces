
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int cross(int x1, int y1, int x2, int y2) {
    int d1=x1-y1;   /* clockwise positions */
    if(d1<0)
        d1+=2*n;

    int d2=x1-x2;
    if(d2<0)
        d2+=2*n;

    int d3=x1-y2;
    if(d3<0)
        d3+=2*n;

    if(d1>min(d2,d3) && d1<max(d2,d3))
        return 1;
    else 
        return 0;
}

/**
 * Note that n is small, O(n^3) is ok.
 *
 * But how can we solve this without knowing the positions of the points?
 * -> We know the positions, they are sorted by number.
 *
 * Brute force:
 * Foreach free point, check all other free points. Choose the one that produces
 * most intersections. With existing chords.
 * ...no, this does not work. We propably need to construct crossings.
 *
 * Connect each point to "the most opposite" free point?
 * Cant be that wrong...but does not work either :/
 *
 * ****
 * Can we check all nCr(2,n-k) pairings?
 * No, it is much more possible pairings, it is like O(n!)
*/
void solve() {
    cini(n);
    cini(k);

    vi vis(2*n);
    vi x(k);
    vi y(k);
    for(int i=0; i<k; i++) {
        cin>>x[i]>>y[i];
        vis[x[i]-1]=true;
        vis[y[i]-1]=true;
    }

    for(int i=0; i<2*n; i++) 
        for(int j=0; j<2*n; j++) {
        }

    int ans=0;
    for(int i=0; i<n; i++)    /* n chords */
        for(int j=i+1; j<n; j++) 
            ans+=cross(x[i], y[i], x[j], y[j]);

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
