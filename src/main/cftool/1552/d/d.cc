
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * n is very small, so kind of bitmask?
 *
 * consider a[0]
 * We need to have 
 * a[0]=abs(b[0]-b[1])
 * a[1]=abs(b[1]-b[2])
 * and so on, but that is one b to much.
 *
 * So we need to find if there is one b[] we can "reuse".
 * We can reuse a b if sum of two a eq another a.
 * So simply check that.
 * No.
 * ***
 * fix b[0]=0
 * b[1]=a[0], so b[1]-b[0]=a[0]
 * b[2]=a[1], so b[2]-b[0]=a[1]
 * but, if there is any b[i]-b[j]==a[k], skip that a[k]
 * ...but also does not work  :/
*/
void solve() {
    cini(n);
    cinai(a,n);

    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            for(int k=0; k<n; k++) {
                for(int l=0; l<n; l++) {
                    if(l==k)
                        continue;

                    if(abs(a[i]-a[j])==abs(a[k]-a[l])) {
                        cout<<"YES"<<endl;
                        return;
                    }
                }
            }
        }
    }
    cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
