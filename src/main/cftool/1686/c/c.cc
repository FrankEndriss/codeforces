/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to find some bi-partition where...
 *
 * Odd seems not possible.
 * Else its possible if there are allways two biggers.
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    if(n%2==1) {
        cout<<"NO"<<endl;
        return;
    }

    sort(all(a));

    vi ans(n);
    for(int i=0; i<n; i+=2) {
        ans[i]=a[n-1-i/2];
        ans[i+1]=a[n/2-1-i/2];
    }

    /*
    cerr<<"ans[]=";
    for(int i : ans)
        cerr<<i<<" ";
    cerr<<endl;
    */

    for(int i=0; i<n; i++) {
        if((ans[(n+i-1)%n]<ans[i] && ans[(n+i+1)%n]<ans[i]) ||
           (ans[(n+i-1)%n]>ans[i] && ans[(n+i+1)%n]>ans[i]))
           ;
        else {
            cout<<"NO"<<endl;
            return;
        }
    }


    cout<<"YES"<<endl;
    for(int i=0; i<n; i++) 
        cout<<ans[i]<<" ";
    cout<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
