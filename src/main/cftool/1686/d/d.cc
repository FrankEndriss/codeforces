/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * First condition is that string len matches.
 * Also freq of A,B must match.
 * Then each AA...A and BB...B, the middle ones
 * are singles, remove them.
 *
 * ABAB
 * BAAB
 * ABBA
 * BABA
 * How?
 *
 * Maintain a quadrupel of possible numbers of the 4 types
 * up to the current position.
 * But that may be O(n^2) :/
 * And actually O(n^4) in space :/
 *
 * ....
 * It is just that after removeing AAA and BBB, if the remaining
 * number of symbols fits, it is allways possible?
 * No.
 *
 * ******
 * Consider the min number of singe A,B, which is 0 or 1.
 * Actually, we have to find only the AB and BA, since if found
 * all other symbols match automagically.
 * Split doubles AA,BB in the middle, creating a pair of each...
 * somehow.
 *
 * To much edgecases, no observation.
 */
void solve() {
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
