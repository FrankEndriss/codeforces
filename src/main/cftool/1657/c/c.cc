/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Utilize Manacher's algo.
 * see https://cp-algorithms.com/string/manacher.html
 * @return d1[i]= size of left half of palindrome with center at i, ie if size==5 then d1[i]==3
 **/
vector<int> manacher_d1(string s) {
    const int n=s.size();
    vi d1(n);
    for (int i = 0, l = 0, r = -1; i < n; i++) {
        int k = (i > r) ? 1 : min(d1[l + r - i], r - i + 1);
        while (0 <= i - k && i + k < n && s[i - k] == s[i + k]) {
            k++;
        }
        d1[i] = k--;
        if (i + k > r) {
            l = i - k;
            r = i + k;
        }
    }
    return d1;
}

/*
 * @return d2[i]= size of half of palindrome with right center at i, ie if size==4 then d2[i]==2
 * and palindrome starts at i-d2[i]
 */
vector<int> manacher_d2(string s) {
    const int n=s.size();
    vi d2(n);
    for (int i = 0, l = 0, r = -1; i < n; i++) {
        int k = (i > r) ? 0 : min(d2[l + r - i + 1], r - i + 1);
        while (0 <= i - k - 1 && i + k < n && s[i - k - 1] == s[i + k]) {
            k++;
        }
        d2[i] = k--;
        if (i + k > r) {
            l = i - k - 1;
            r = i + k ;
        }
    }
    return d2;
}
/**
 * Simulate, iterate the string finding if its a palindrome.
 *
 * Consider
 * )((((((((
 * Is there any other non palindrome?
 *
 * ((
 * ()
 * ))
 * )(
 */
void solve() {
    cini(n);
    cins(s);

    int offs=0;
    bool ok=true;
    int ans=0;
    while(offs<n && ok) {

        if(offs+1<n) {
            if(s[offs]!=')' || s[offs+1]==')') {
                ans++;
                offs+=2;
                continue;
            }

            assert(s[offs]==')');
            assert(s[offs+1]=='(');
            ok=false;
            for(int j=offs+2; j<n; j++) {
                if(s[j]==')') {
                    offs=j+1;
                    ok=true;
                    ans++;
                    break;
                }
            }
        } else 
            break;
    }

    cout<<ans<<" "<<n-offs<<endl;



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
