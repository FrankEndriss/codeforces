/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * More or less obviusly we need to binary search the best unit per monster.
 * How?
 *
 * So, which units can kill the monster?
 * -> all can, we only need enough of them
 *
 * So sort them by h[i]
 *
 * The units will give damage for the time h[i]/D
 * So, 
 * h[i]/D > H/(d[i]*x)
 * (h[i]*d[i]*x)/D > H
 * h[i]*d[i]*x > H*D
 * x> H*D / h[i]*d[i]
 * and costs=x*c[i]
 *
 * So sort units by c[i]/(h[i]*d[i]) and choose the first one.
 * Really?
 */
void solve() {
    cini(n);
    cini(C);
    vi c(n);
    vi d(n);
    vi h(n);
    using t3=tuple<int,int,int>;
    vector<t3> un;
    for(int i=0; i<n; i++)  {
        cin>>c[i]>>d[i]>>h[i];
        un.push_back(h[i], c[i], d[i]);
    }

    cini(m);
    for(int i=0; i<m; i++) {
        cini(D);
        cini(H);
    }
}

signed main() {
    solve();
}
