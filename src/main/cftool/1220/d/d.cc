/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * If we remove all even numbers, 
 * then all numbers are connected
 * only to other numbers with different 
 * parity.
 *
 * (what is the word for odd/even-state?)
 * ->parity
 *
 * But can there be better solutions?
 * If n==1 the graph is always bp.
 * ****
 * It turns out that we can multiply all those odd numbers
 * with 2^k to get even numbers, wich forms the same
 * bipartite graph, too.
 *
 * So we need to check for this case, too.
 * Check the set of even numbers for the 2^k which is 
 * the most times in the divisor, since all other numbers
 * must be removed.
 *
 * -> see tutorial...for some reason we need to 
 *  find 'maximum power of 2 that devides all b[i]'.
 *  Why???
 */
void solve() {
    cini(n);
    vvi b(2);

    if(n==1) {
        cout<<0<<endl;
        return;
    }

    for(int i=0; i<n; i++) {
        cini(aux);
        b[aux&1].push_back(aux);
    }

    if(b[0].size()>b[1].size()) {
        vi ans1=b[0];
        for(int i=1; i<30; i++) {
            int bit=1LL<<i;
            vi lans1;
            for(int j=0; j<b[0].size(); j++) {
                if(b[0][j]%bit!=0)
                    lans1.push_back(b[0][j]);
            }
            if(lans1.size()<ans1.size())
                ans1.swap(lans1);
        }

        if(b[0].size()-ans1.size()>b[1].size()) {
            cout<<b[1].size()+ans1.size()<<endl;
            for(int a : b[1])
                cout<<a<<" ";
            for(int a : ans1)
                cout<<a<<" ";
            cout<<endl;
            return;
        }
    }

    cout<<b[0].size()<<endl;
    for(int a : b[0])
        cout<<a<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
