/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to find foreach point used as an upper left corner, the number of
 * ops to make 
 * -the top row of some size black
 * -the left col of some size black
 * -the rigth col fitting to the two above black
 * -the bottom row fitting to the two above black
 * -the space in between white (use 2d fenwick)
 *
 * So, iterating all left upper corners is O(n^2), and iterating all bottom/right
 * foreach top/left is also O(n^2), so its O(n^4)
 *
 * How to get rid of one dimension?
 * Why there is that 5x4 ???
 * *****************************
 * Create the 2d-prefix sum of number of black cells in recht 0,0 to i,j.
 * Then iterate all pairs of cols (with with at least 4).
 * Then foreach such pair, iterate the rows.
 * At each row, calculate the cost as min of the above 5 rows,
 * or the previous row combined with the current.
 */
void solve() {
}

signed main() {
    solve();
}
