/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A casework.
 *
 * ...
 * sic problem, no fun, not interesting, bad for the comunity.
 * I dont get it why such problem is in the contest.
 * They could also ask for the 42th digit of PI.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    k-=2;   /* max dia of the graph */

    if(n==1) {  /* minimum tree/graph, dia=0 */
        if(m==0 && 0<=k)
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    } else if(m<n-1 || m>n*(n-1)/2) {   /* impossible graph */
        cout<<"NO"<<endl;
    } else if(m==n*(n-1)/2) {   /* completly connected, dia=1 */
        if(1<=k)
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    } else if(m==n-1) { /* tree, dia=2 */
        if(n==2) {  /* small tree, dia=1 */
            if(1<=k)
                cout<<"YES"<<endl;
            else
                cout<<"NO"<<endl;
        } else {    /* n>2, dia=2 */
            if(2<=k)
                cout<<"YES"<<endl;
            else
                cout<<"NO"<<endl;
        }
    } else { /* kind of star graph, dia=2 */
        if(2<=k)
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
