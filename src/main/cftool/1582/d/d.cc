/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider combining twp primes a[i] and a[j], so
 * b[i]=a[j]
 * b[j]=-a[i]
 * But then sum can be up to 2e9.
 *
 * **************************
 * ...NO! single n<=1e5 !!!
 *
 * So just consider odd n, so 3 primes, 
 * how?
 *
 * say a={3,5,7}
 * b={12*3,5*3,7*3}
 */
const int N=1e4+3;
void solve() {
    cini(n);
    cinai(a,n);

    vvi f(2*N);
    for(int i=0; i<n; i++) {
        f[N+a[i]].push_back(i);
    }
    sort(all(a), [](int a1, int a2)  {
            return abs(a1)<abs(a2);
            });

    vi b(n);

    if(n&1) {
        int a0=a[0];
        int a1=a[n-2];
        int a2=a[n-1];

        b[f[N+a0].back()]=a0*(a1*a2);
        f[N+a0].pop_back();
        b[f[N+a1].back()]=-a0*a1;
        f[N+a1].pop_back();
        b[f[N+a2].back()]=-a0*a2;
        f[N+a2].pop_back();
    }
    a.pop_back();
    a.pop_back();


    for(int i=1; i+1<a.size(); i+=2) {
        int a1=a[i];
        int i1=f[N+a1].back();
        f[N+a1].pop_back();
        int a2=a[i+1];
        int i2=f[N+a2].back();
        f[N+a2].pop_back();

        b[i1]=a1*a2;
        b[i2]=-a1*a2;
    }

    for(int i=0; i<b.size(); i++)
        cout<<b[i]<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
