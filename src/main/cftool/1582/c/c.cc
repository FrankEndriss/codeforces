/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Just brute force this.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cins(s);

    int ans=INF;
    for(char c='a'; c<='z'; c++) {
        int l=0;
        int r=n-1;
        bool ok=true;
        int lans=0;
        while(ok && l<r) {
            if(s[l]==s[r]) {
                l++;
                r--;
            } else if(s[l]==c) {
                l++;
                lans++;
            } else if(s[r]==c) {
                r--;
                lans++;
            } else
                ok=false;
        }
        if(ok)
            ans=min(ans, lans);
    }

    if(ans==INF)
        cout<<-1<<endl;
    else
        cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
