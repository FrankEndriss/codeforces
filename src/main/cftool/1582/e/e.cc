/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}

/* This is the neutral element */
const S INF=1e9;
S st_e() {
    return INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return min(f,g);
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return INF;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Binary search?
 * Note that k cannot be that big, because number of elements in 
 * all the segments is k*(k+1)/2, 
 * so k*(k+1)<=2e5, so max k aprox <500;
 *
 * So, foreach segment 1..k, we got a min and max index where it is in a[].
 * Foreach such index there is a list of <maxSum,minValLastSegment>, both non-decreasing.
 * Note that these lists can be as big as the number of different segments of last k exist.
 * So basically N. Which means we cannot iterate it.
 *
 * How to check if a given k works:
 * go left to right over a[], dp[i]=min possible sum of last segement 
 * within the prefix up to i;
 *
 * So, from all segments possibe for k-1, we want to choose from the 
 * k-segments the one with max sum, from all with r[k]<l[k-1]
 *
 * ***
 * Consider the other direction.
 * Start with the segments of len=1 from right, cnt=n-(k*(k-1)/2)+1;
 *
 * Then, again go from right to left for len=2, start at n-1-1;
 * And create the new list from that...
 * Should work in O(n*sqrt(n)*log(500))
 *
 * ...no, kind of does not work at all :/
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);
    vi pre(n+1);
    for(int i=0; i<n; i++)
        pre[i+1]=pre[i]+a[i];

    function<int(int,int)> psum=[&](int l, int r) {
        return pre[r+1]-pre[l];
    }


    /* dp[i]=<maxSum,maxVal) using postfix including position i */
    vector<pii> dp(n, {-INF,-INF});
    dp[n-1]={a[i],a[i]};
    for(int i=n-2; i>=0; i--)
        dp[i]={ max(a[i], dp[i+1].first), max(a[i], dp[i+1].first)};


    //... TODO 
    
    function<bool(int)> go=[&](int k) {

        vector<pii> dp(n+1, {INF,-INF}); /* minSumLastSegment,maxSumOverAll */

        int start=0;
        while(k>0) {
            int cnt=0;
            for(int i=start; i<n; i++) {
                cnt++;
                sum+=a[i];
                if(cnt>=k)
                    dp[i+1]=min(dp[i], sum);

                if(cnt>k)
                    sum-=a[i-k];
            }
            start+=k;
            k--;
        }
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
