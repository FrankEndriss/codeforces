/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Kind of knapsack.
 *
 * But, how comes that "increasing" into play?
 *
 * Note that the max len of a increasing subseq is 500.
 * Consider 2D-Array of indexes.
 * Then, maintain the smallest possible index per xor-value.
 * And iterate the inedexes of the values in increasing order of the values.
 */
const int N=513;
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    vvi idx(N);
    for(int i=0; i<n; i++)
        idx[a[i]].push_back(i);

    /* dp[i]=smallest possible last index to reach xorsum=i */
    vi dp(N,INF);
    dp[0]=-1;

    for(int i=0;i<N; i++) { /* the numbers */
        vi dp0=dp;
        for(int j=0; j<N; j++) { /* the xorsums */
            if(dp[j]==INF)
                continue;

            auto it=upper_bound(all(idx[i]), dp[j]);
            if(it!=idx[i].end()) {
                    dp0[j^i]=min(dp[j^i],*it);
            }
        }
        dp.swap(dp0);
    }

    int ans=0;
    for(int i=0; i<N; i++) 
        if(dp[i]<INF)
            ans++;
    cout<<ans<<endl;
    for(int i=0; i<N; i++) 
        if(dp[i]<INF)
            cout<<i<<" ";
    cout<<endl;


}

signed main() {
    solve();
}
