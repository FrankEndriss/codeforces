/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* if rows>cols then it is simply the n max column values.
 * -> It is not. Because from some columns we want to take two values, not only one.
 *  Then, how???
 *  Some brute force.
 *  Sort cols by max value.
 *  Consider first col.
 *  Say that all rows are 'fixed' where the val is bigger than the one from the biggest
 *  of the next col.
 *
 *  Then assume the second col. There are some rotations (less than n) where the biggest
 *  value of that col comes into count.
 *  Try all of them, and say that the rows of the biggest value is 'fixed', too.
 *  Again, all rows are fixed with max values bigger than the biggest of the next rows.
 *  Repeat until all rows are fixed.
 *
 *  Note cols are on left dimension for simpler implementation.
 *
 *  ...here it does not work. In every of the 12 rows all nearly 2000 
 *  rotations are tried... that is much to much.
 **/
void solve() {
    cini(n);
    cini(m);
    vvi a(m, vi(n));
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            cin>>a[j][i];

    vvi b(m);   // cols, but values sorted
    for(int i=0; i<m; i++)  {
        b[i]=a[i];
        sort(all(b[i]), greater<int>());
    }

    /* sort the cols by the max values in that col, biggest first. */
    vi id(m);
    iota(all(id), 0);
    sort(all(id), [&](int i1, int i2) {
        return b[i1]>b[i2];
    });

    vvi aa(m);
    vvi bb(m);
    for(int i=0; i<m; i++) {
        aa[i]=a[id[i]];
        bb[i]=b[id[i]];
    }
    a.swap(aa);
    b.swap(bb); /* a and b now sorted desc */

    /*
    for(int i=0; i<m; i++) {
        cerr<<"b["<<i<<"]= ";
        for(int j=0; j<n; j++) 
            cerr<<b[i][j]<<" ";
        cerr<<endl;
    }
    */

    vvi ama(m);  /* indexes of b[m][0] in a[m] */
    for(int i=0; i<m; i++)
        for(int j=0; j<n; j++) 
            if(a[i][j]==b[i][0])
                ama[i].push_back(j);


    /* rotate column idx, with maxvalues pre, and return max possible result.
     * Simply check all rotations where maxval would contribute.
     * if there is none then no further rotations are of use. */
    function<int(int,vi&)> rot=[&](int idx, vi pre) {
        int ans=accumulate(all(pre), 0LL);
        if(idx==m)
            return ans;

        int macol=b[idx][0];

        vb vis(n);
        for(int i=0; i<n; i++) {
            if(pre[i]>=macol)
                continue;

            for(int j : ama[idx]) {
                if(vis[(n+(j-i))%n])
                    continue;

                vis[(n+(j-i))%n]=true;

                vi pre2=pre;
                for(int k=0; k<n; k++)
                    pre2[k]=max(pre[k], a[idx][(n+k+(j-i))%n]);
                ans=max(ans, rot(idx+1, pre2));
            }
        }
        return ans;
    };

    vi ans(n);
    cout<<rot(0,ans)<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
