/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need leftmost and rightmost of each element.
 * Then all overlapping segments form a component,
 * and number of elements in component minus one
 * is the number of neccecary operations.
 *
 * maintain the elements in set<int> so first,last 
 * are available.
 * -> not sure how to handle updates, but easy version
 *  is without updates.
 *
 *  Then, how to do updates?
 *  Difficulty is that we somehow need to build the
 *  components.
 */
void solve() {
    cini(n);
    cini(q);
    vi a(n);
    map<int,vi> f;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        f[a[i]].push_back(i);
    }
    vvi ff;
    for(auto ent : f)
        ff.push_back(ent.second);

    sort(all(ff));   /* by first position */

    vb vis(ff.size());
    int ans=0;

    for(size_t i=0; i<ff.size(); i++) {
        if(vis[i])
            continue;

        vis[i]=true;
        size_t macnt=ff[i].size();
        size_t sucnt=ff[i].size();
        int idx=i;
        int r=ff[idx].back();

        while(idx+1<ff.size() && r>ff[idx+1][0]) {
            r=max(r,ff[idx+1].back());
            macnt=max(macnt, ff[idx+1].size());
            sucnt+=ff[idx+1].size();
            idx++;
            vis[idx]=true;
        }
        ans+=sucnt-macnt;
    }

    cout<<ans<<endl;


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
