/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

struct Dsu {
    vector<int> p;
    vector<int> s;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        iota(p.begin(), p.end(), 0);
        s=vi(size, 1);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            s[a]+=s[b];
        }
    }
};

/* Graph, Dsu
 * The snacks are vertices, the guests are the edges.
 * A component of size C satisfies C-1 guests.
 * So count them and subtract from n;
 */
void solve() {
    cini(n);
    cini(k);

    Dsu dsu(n);

    
    for(int i=0; i<k; i++) {
        cini(x);
        cini(y);
        x--;
        y--;
        dsu.union_sets(x,y);
    }

    int ans=k;
    vb vis(n);
    for(int i=0; i<n; i++) {
        int s=dsu.find_set(i);
        if(!vis[s]) {
            vis[s]=true;
            ans-=(dsu.s[s]-1);
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
