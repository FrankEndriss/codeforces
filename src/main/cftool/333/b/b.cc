/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* So we can move from edge to edge in a streigth line only.
 * And only if there is no obstacle, and only in one direction.
 * Only question is, do vertical/horizontal intersect or not.
 * -> this does not work
 * ******
 * Tutorial
 * Caused by funny observation we can solve in O(n)
 * This is we build pairs of (i,n-i+1)
 * By switching x/y and sides on which edge we
 * place a chip we can construct 4 starting points:
 * (1,n-i+1)    horizontal
 * (i,1)        vertical
 * (n,i)        horizontal
 * (n-i+1,n)    vertical
 * These 4 starting points never intersect each other (except odd grid with i==(n+1)/2)
 * so we can put chips there if no obstacles in the way.
 */
void solve() {
    cini(n);
    cini(m);
    vb sx(n+1);
    vb sy(n+1);
    for(int i=0; i<m; i++) {
        cini(x);
        cini(y);
        sx[x]=true;
        sy[y]=true;
    }

    int ans=0;
    for(int i=2; i<n/2+1; i++) {
        if(!sy[n-i+1])
            ans++;
        if(!sy[i])
            ans++;
        if(!sx[n-i+1])
            ans++;
        if(!sx[i])
            ans++;
    }

    if(n&1) {   // middle row/col
        if((!sx[n/2+1]) || (!sy[n/2+1]))
            ans++;
    }


    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
