/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int BLOCK_SIZE=sqrt(1e5+5)+1;


/* see https://cp-algorithms.com/data_structures/sqrt_decomposition.html#toc-tgt-4 */
bool cmp(pair<int, int> p, pair<int, int> q) {
    if (p.first / BLOCK_SIZE != q.first / BLOCK_SIZE)
        return p < q;
    return (p.first / BLOCK_SIZE & 1) ? (p.second < q.second) : (p.second > q.second);
}

struct Query {
    int l, r, idx;

    bool operator<(Query other) const
    {
        return cmp(make_pair(l, r), make_pair(other.l, other.r));
    }
};


/* implement updates and query */
void moremove(int);
void moadd(int);
int moget_answer();

vector<int> mo_s_algorithm(vector<Query> queries) {
    vector<int> answers(queries.size());
    sort(queries.begin(), queries.end());
    int cur_l = 0;
    int cur_r = -1;

    /* invariant: data structure will always reflect the range [cur_l, cur_r] */
        for (Query q : queries) {
        while (cur_l > q.l) {
            cur_l--;
            moadd(cur_l);
        }
        while (cur_r < q.r) {
            cur_r++;
            moadd(cur_r);
        }
        while (cur_l < q.l) {
            moremove(cur_l);
            cur_l++;
        }
        while (cur_r > q.r) {
            moremove(cur_r);
            cur_r--;
        }
        answers[q.idx] = moget_answer();
    }
    return answers;
}

int gans;
unordered_map<int,int> f;
vi a;

void moremove(int idx) {
    int v=--f[a[idx]];
    assert(v>=0);
    if(v==a[idx])
        gans++;
    else if(v==a[idx]-1)
        gans--;
}

void moadd(int idx) {
    int v=++f[a[idx]];
    if(v==a[idx])
        gans++;
    else if(v==a[idx]+1)
        gans--;
}

int moget_answer() {
    return gans;
}

/* can we index a somehow?
 * from left to right counting elements
 * then store the positions
 * dp[i][j]=position of iht same element starting at j
 *
 * We can remove/ignore all elements which are
 * less times than its number in whole array.
 * The remaining elements are less than sqrt(1e5), ie
 * ~350
 * We could iterate those per query.
 *
 * Count all elements, remove removables by setting to zero. O(nlogn)
 *
 * Reorder queries by l.
 * Count up to l, store those tables per position.
 *
 * Reorder queryies by r.
 * Count up to r, subtract stored l table.
 * O(n*sqrt(1e5))
 */
void solve() {
    cini(n);
    cini(m);
    a.resize(n);
    for(int i=0; i<n; i++)
        cin>>a[i];
    
    vector<Query> q(m);
    for(int i=0; i<m; i++) {
        cin>>q[i].l>>q[i].r;
        q[i].l--;
        q[i].r--;
        q[i].idx=i;
    }

    vector<int> ans=mo_s_algorithm(q);
    for(int i=0; i<m; i++) 
        cout<<ans[i]<<endl;
    
}

/* second solution O(logn)
 * Reorder queries.
 * maintain evt array of size 1e5 (note that we can ignore all 
 * numbers > n in a[])
 * Iterate a, maintain indexes of elements in freq mapping (array).
 * First time a[i] is found the ith time update evt[i]+=1
 * Next times a[i] is found update evt[...
 *
 * Assume numbers in array are INF and 3, ie
 * 3 I I I 3 3 I 3 I I I 3
 *
 * 1,4 ->1
 * 1,5 ->2
 * 2,5 ->1
 * 1,6 ->3*
 * 2,6 ->2
 * 1,7 ->3*
 * 1,8 ->4
 * 2,8 ->3*
void solve2() {
    cini(n);
    cini(m);
    cinai(a,n);

    vi d(n+1);
    vvi f(n+1);
    for(int i=0; i<n; i++) {
        if(a[i]>n)
            continue;
        f[a[i]].push_back(i);

        if(f[a[i]].size()==i) {
            d[i]+=1;
        } else if(f[a[i]].size()>i) {
            d[i]+=1;
            int sz=f[a[i]].size();
            f[a[i]][sz-i];
        }
    }
}
 */

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
