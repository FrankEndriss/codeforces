/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    vs f(n);
    vs s(n);
    for(int i=0; i<n; i++)
        cin>>f[i]>>s[i];

    cinai(p,n);
    string prev=min(f[p[0]-1], s[p[0]-1]);
    
    int ok=true;
    for(int i=1; ok && i<n; i++) {
        p[i]--;
        if(min(f[p[i]], s[p[i]]) >= prev)
            prev=min(f[p[i]], s[p[i]]);
        else if(max(f[p[i]], s[p[i]]) >= prev)
            prev=max(f[p[i]], s[p[i]]);
        else
            ok=false;
    }
    if(ok)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}


signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
