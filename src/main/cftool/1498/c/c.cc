/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=1e9+7;

/**
 * So, a p(i) produces a p(i-1) on every plane it goes throug.
 * This is, a p has decay, dir and position.
 * Its a dp.
 * We need to optimize with prefix sums, super errorprone,
 * i will need like two hours to get all off by one solved. :/
 */
void solve() {
    cini(n);
    cini(k);

    vvvi dp(2, vvi(n+1, vi(k+1)));
    for(int i=0; i<2; i++)
        for(int j=0; j<n; j++)
            dp[i][j][1]=1;

    for(int kk=2; kk<=k; kk++)  {
        dp[1][0][kk]=1;
        for(int j=1; j<n; j++) {
            dp[1][j][kk]=dp[1][j-1][kk]+dp[0][j][kk-1];
            dp[1][j][kk]%=MOD;
        }
        dp[0][n][kk]=1;
        for(int j=n-1; j>=0; j--) {
            dp[0][j][kk]=dp[0][j+1][kk]+dp[1][j][kk-1];
            dp[0][j][kk]%=MOD;
        }
    }

    cout<<dp[0][0][k]<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
