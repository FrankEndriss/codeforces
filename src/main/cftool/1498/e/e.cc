/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* if there is a house with exactly 1 road incoming,
 * then we can query all other houses with big k[j]
 * if the k[i]==1 house is reachable.
 * If yes, thats ans.
 *
 * So, what if there is no k[i]==1 house?
 * ...Is there allways one? -> No.
 */
void solve() {
    cini(n);
    vi ones;
    vi k(n);
    for(int i=0; i<n; i++) {
        cin>>k[i];
        if(k[i]==1)
            ones.push_back(i);
    }

}

signed main() {
    solve();
}
