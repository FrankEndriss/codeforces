/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* brute force the sequence 
 * 1
 * 4
 * 16
 * 64
 * 256
 * */
void help() {
    int N=100000;
    vb vis(N);
    vis[1]=true;
    vis[2]=true;
    vis[3]=true;

    while(true) {
        /* first unused */
        int i1;
        for(int i=1; i<N; i++) {
            if(!vis[i]) {
                i1=i;
                break;
            }
        }
        /* second */
        int i2;
        for(int i=i1+1; i<N; i++) {
            if(vis[i])
                continue;
            if((i1^i)>i && !vis[i1^i]) {
                i2=i;
                break;
            }
        }

        vis[i1]=true;
        vis[i2]=true;
        vis[i1^i2]=true;
        cout<<bitset<16>(i1)<<" "<<bitset<16>(i2)<<" "<<bitset<16>(i1^i2)<<endl;
    }
}

void solve() {
    cini(n);
    int start=1;
    while(n>start*3) {
        n-=start*3;
        start*=4;
    }

    int m=(n-1)%3;
    start+=start*m;
    int row=(n-1)/3;
    if(m==0) {
        start+=n/3;
    } else if(m==1) {   /* pattern 00 10 11 01 */
        vi p={ 0, 2, 3, 1 };
        int cnt=0;
        stack<int> st;
        do {
            st.push(p[row%4]);
            row/=4;
        }while(row);
        int add=0;
        while(st.size()) {
            int i=st.top();
            st.pop();
            add*=4;
            add+=i;
        }
        start+=add;
    } else if(m==2) {
        vi p={ 0, 3, 1, 2 };
        int cnt=0;
        stack<int> st;
        do {
            st.push(p[row%4]);
            row/=4;
        }while(row);
        int add=0;
        while(st.size()) {
            int i=st.top();
            st.pop();
            add*=4;
            add+=i;
        }
        start+=add;
    }
    cout<<start<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
//help();
    cini(t);
    while (t--)
        solve();
}

