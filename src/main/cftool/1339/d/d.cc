/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Choose one leaf as root.
 * All sister leafs must have same edge value.
 * All non-leaf sisters have that edge-value as "input".
 * That can be reduced, or extended.
 *
 * All paths to leafs in that subtree must xor to the input value.
 * reduce by removeing bits, extend by adding bits.
 */
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);    
        cini(v);
        u--; v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    int root=-1;
    for(int i=0; i<n; i++) {
        if(adj[i].size()==1) {
            root=i;
            break;
        }
    }
/* @return min,max of subtree node */
    function<pii(int,int)> dfs=[&](int node, int parent, int input) {
        // ???
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

