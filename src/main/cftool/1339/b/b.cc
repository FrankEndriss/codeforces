/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cinai(a,n);

    sort(a.begin(), a.end());
    int center=(n+1)/2-1;
    int dir=1;
    int dist=1;
    vi ans;
    ans.push_back(a[center]);
    int cnt=0;
    for(int i=1; i<n; i++) {
        int idx=center+dist*dir;
        ans.push_back(a[idx]);
        dir=-dir;
        cnt++;
        if(cnt==2) {
            dist++;
            cnt=0;
        }
    }
    
    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

