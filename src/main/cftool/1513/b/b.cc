/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

/* This somehow works bit by bit.
 * Obviously, each bit must be either all zero or all 1.
 * How to find the number of good perms?
 * Assume all numbers are 0 or 1.
 * Then, if there is any 0 in the numbers, one side is allways 0.
 * So, the number of good perms is the number of perms
 * where both sides are zero.
 *
 * This means, foreach bit that is not set in all numbers, we must
 * put a number with that bit set to zero on both ends.
 * So, all perms with all bits eq zero on both ends are ok, all others
 * not.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi cnt(31);
    for(int i=0; i<31; i++) {
        for(int j=0; j<n; j++) {
            if(a[j]&(1LL<<i))
                cnt[i]++;
        }
    }

    int mask=0;
    for(int i=0; i<31; i++) 
        if(cnt[i]==n)
            mask|=(1LL<<i);

    int zero=0;
    for(int i=0; i<n; i++) {
        if((a[i]&(~mask)) == 0)
            zero++;
    }

    int fac=1;
    for(int i=2; i<=n-2; i++) 
        fac=mul(fac,i);

    int ans=0;
    if(zero>=2) {
        ans=mul(zero, zero-1);
        ans=mul(ans, fac);
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
