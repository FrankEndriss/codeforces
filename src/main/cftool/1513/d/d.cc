/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* With Edmonds/Karp we need to find the edges sorted by weight.
 *
 * Since there is obviosly a MST of cost p*(n-1) we want to 
 * find the gcd-constructed edges cheaper than p.
 *
 * So, to have an edge we need to find an subarray where
 * all elements are multiples of the smallest element.
 * We can do this foreach element with two-pointer, go left and
 * right until an element does not match.
 * But note that if all numbers are same we got a lot of edges,
 * also with array like (2,4,2,6,2,4,...)
 *
 * We can iterate the elements by value, smallest first.
 * Foreach, first check if used in another component.
 * If not, then Foreach, find the biggest component of cheap edges, and construct a star. 
 * Note that components are not connected to each other.
 * Then, finally connect all stars by edges of cost p each.
 *
 * ...What wrong here???
 * There must be some edgecase :/
 * Fuck it!
 */
void solve() {
    cini(n);
    cini(p);
    cinai(a,n);

    vi id(n);
    iota(all(id), 0);
    sort(all(id), [&](int i1, int i2) {
            return a[i1]<a[i2];
    });

    int comp=0;
    int ans=0;
    vb vis(n);
    for(int i=0; i<n; i++) {
        if(vis[id[i]])
            continue;

        comp++;
        vis[id[i]]=true;

        if(a[id[i]]>=p)
            continue;

        int l=id[i]-1;
        while(l>=0 && !vis[l] && a[l]%a[id[i]]==0) {
            ans+=a[id[i]];
            vis[l]=true;
            l--;
        }

        int r=id[i]+1;
        while(r<n && !vis[r] && a[r]%a[id[i]]==0) {
            ans+=a[id[i]];
            vis[r]=true;
            r++;
        }
    }
    ans+=p*(comp-1);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
