/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Observe that each digit works on its own, so x(ab,m)==x(a,m)x(b,m)
 *
 * Consider single digit d
 * 10-d: -> 10
 * +9: -> 109
 * +1: -> 2110
 * +8: -> 10998
 * +1:    2110109
 * +1:    32212110
 * +7:    109989887
 * ...
 * Its a dp, simply consider the freq of each digit and do m loops.
 * But the number of operations could be huge, note that there is
 * no "the sum of m over all testcases does not exceed 1e5" :/
 */
const int MOD=1e9+7;

const int N=2e5+57;
vi ans(N);
void init() {

    vi dp(10);
    dp[0]=1;

    for(int i=0; i<N-20; i++) {
        for(int j=0; j<10; j++)
            ans[i]+=dp[j];
        ans[i]%=MOD;

        vi dp0(10);
        for(int j=0; j<9; j++)
            dp0[j+1]=dp[j];

        dp0[0]+=dp[9];
        dp0[1]+=dp[9];
        if(dp0[0]>=MOD)
            dp0[0]-=MOD;
        if(dp0[1]>=MOD)
            dp0[1]-=MOD;
        dp.swap(dp0);
    }
}

void solve() {
    cini(n);
    cini(m);

    vi f(10);
    while(n) {
        f[n%10]++;
        n/=10;
    }

    int aa=0;
    for(int i=0; i<10; i++) {
        int p=(ans[m+i]*f[i])%MOD;
        aa=(aa+p)%MOD;
    }
    cout<<aa<<endl;
}
signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
