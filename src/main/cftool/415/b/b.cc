/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* w is number of token.
 * So we get max d=(w*a)/b  dollars.
 * For that, we need at least
 * w0=(d*b+a-1)/a tokens.
 **/
void solve() {
    cini(n);
    cini(a);
    cini(b);
    for(int i=0; i<n; i++) {
        cini(w);
        int d=(w*a)/b;  /* dollars */

        /* min num of token to get d dollars. w0*a>=d*b */
        int w0=(d*b+a-1)/a;
        cout<<w-w0<<" ";
    }
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

