/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Obs:
 * Every boy must give at least one girl exactly b[i] sweets
 * Every girl must get at least once exactly g[i] sweets
 *
 * A girl get max g[j] sweets cannot get more from any boy. So there must
 * be at least on boy with b[i]<=g[j].
 *
 * How to construct sol?
 * Edge case n==1 and/or m==1
 */
void solve() {
    cini(n);
    cini(m);
    cinai(b,n); // min per boy
    cinai(g,m); // max per girl

    /* how to check if all conditions are met? */
    if(*max_element(b.begin(), b.end())>*min_element(g.begin(), g.end())) {
        cout<<-1<<endl;
        return;
    }

    int sumb=0;
    for(int i=0; i<n; i++)
        sumb+=b[i];

    int ans=sumb*m;

/* Let be firstBoy the boy with max b[i]
 * Let be secondBoy the boy with second max b[i]
 * Foreach girl we need to find the boy giving the max sweets, and increase that ones
 * sweets up to g[i].
 * If the number of first boy was increased for all girls, then we must use 
 * second boy on one girl.
 */
    vi id(n);
    iota(id.begin(), id.end(), 0);
    sort(id.begin(), id.end(), [&](int i1, int i2) {
        return b[i2]<b[i1];
    });

    /* All girls do it with first boy */
    bool incAll=true;
    for(int j=0; j<m; j++) {
        if(b[id[0]]<g[j])
            ans+=g[j]-b[id[0]];
        else
            incAll=false;
    }

    if(incAll) /* one girl does it with second boy */
        ans+=b[id[0]]-b[id[1]];


    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

