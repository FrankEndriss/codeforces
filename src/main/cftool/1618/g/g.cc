/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How are the mechanics?
 *
 * Consider all items are in b[], and a[0]
 * is one single item.
 * We can trade up to where b[i+1]-b[i]>k, there it stops.
 *
 * So start with the biggest items. Trade it up while possible.
 * continue with next.
 *
 * How to optimize?
 * We cannot trade each item, that would be n*n per query.
 * We evan cannot trade a single item
 *
 * Sort queries by k?
 *
 *
 */
void solve() {
    cini(n);
    cini(m);
    cini(q);

    priority_queue<int> a;
    for(int i=0;i<n; i++) {
        cini(aux);
        a.emplace(aux);
    }

    multiset<int> b;
    for(int i=0;i<n; i++) {
        cini(aux);
        b.insert(aux);
    }

    vector<pii> qq(q);

    for(int i=0; i<q; i++) {
        cin>>qq[i].first;
        qq[i].second=i;
    }
    sort(all(qq));

    /* can we now simply trade linear? */
    vi ans(q);
    for(int i=0;i<q;i++) {
        const int k=qq[i].first;
        int sum=0;
        for(auto itA=a.begin(); itA!=a.end();) {
            int val=-(*itA);
            auto itB=b.upper_bound(val);
            while(itB!=b.end()) {
                if(val+k<(*itB)) 
                    break;
                else {
                    val=*itB;
                    itB++;
                }
            }

            if(val!=-(*itA)) {
                auto itaux=b.find(val);
                assert(itaux!=b.end());
                b.erase(itaux);
                b.insert(-(*itA));
                a.insert(-val);
                itA=a.erase(itA);
            } else
                sum-=*itA;

            if(itA==a.end())
                break;
            else
                itA++;
        }
        ans[qq[i].second]=sum;
    }

    for(int i=0;i<q; i++) 
        cout<<ans[i]<<endl;




}

signed main() {
    solve();
}
