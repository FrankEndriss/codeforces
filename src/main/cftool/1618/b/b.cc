/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * AB BB _AB_ BA
 *
 * What wrong here?
 */
void solve() {
    cini(n);
    vs s(n);
    for(int i=0; i<n-2; i++) {
        cin>>s[i];
        assert(s[i].size()==2);
    }

    string ans;
    ans.push_back(s[0][0]);
    int cnt=0;
    for(int i=1; i<n-2; i++) {
        if(s[i][0]==s[i-1][1]) {
            ans.push_back(s[i][0]);
        } else {
            ans.push_back(s[i-1][1]);
            ans.push_back(s[i][0]);
            cnt++;
        }
    }
    while(cnt--)
        ans.push_back(s.back()[1]);

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
