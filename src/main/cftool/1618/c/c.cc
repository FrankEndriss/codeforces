/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


bool check(vi &a, int d) {
    int cnt=0;
    for(int i : a)
        if(i%d==0)
            cnt++;
    return cnt>=ssize(a)/2;
}

/**
 * cannot primefactorize.
 *
 * We need to find a common factor of half of the numbers.
 *
 * use gcd to find pairs with common factors.
 * From all pairs...somehow.
 * ???
 */
void solve() {
    cini(n);
    cinai(a,n);

    map<int,int> f;
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            f[gcd(i,j)]++;
        }
    }

    vector<pii> v;
    for(auto ent : f)  {
        if(ent.second>1) 
            v.emplace_back(-ent.second,ent.first);
    }

    sort(all(v));
    int ans=1;
    int cnt=0;
    for(int i=0; cnt<ssize(a)/2 && i<ssize(v); i++) {
        ans*=v[i].second;
        cnt-=v[i].first;
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
