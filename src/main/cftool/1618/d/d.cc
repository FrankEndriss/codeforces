/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Some greedy.
 *
 * Sort array, use pairs of numbers next to each other.
 * But if k is lt n/2 we can ignore some numbers pairs.
 * Which ones?
 *
 * Let b[]=a[i]-a[i+1]
 * And we must use k elements of b[], but must not use two
 * consecutive ones.
 *
 * ****
 * No, it is other problem.
 * We want to remove cheap pairs, but also want to 
 * remove biggest numbers.
 * So allways remove the two biggest, but what if they are same?
 * If two biggest are not same remve them for cost=0.
 *
 * We must/want remove the k*2 biggest numbers.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    sort(all(a), greater<int>());

    int ans=0;
    for(int i=0; i<k; i++)  {
        ans+=a[i+k]/a[i];
        a[i]=a[i+k]=0;
    }

    ans+=accumulate(all(a), 0LL);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
