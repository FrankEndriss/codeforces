/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that the conversion to decimal does not change the value.
 * So we got two binarie strings and want to know
 * if we can make the one from the other.
 *
 * If we append 0 then the number is only reversed.
 * Else a 1 is appended.
 * So we can remove arbitrary 0 from right, and add arbitrary 1
 * to either end.
 *
 * Just generate all possible binstrings up to size...maybe 40?
 */
void solve() {
    cini(x);
    cini(y);

    string b;
    while(x) {
        if(x%2)
            b.push_back('1');
        else
            b.push_back('0');
        x/=2;
    }
    reverse(all(b));


    set<string> s;
    s.insert(b);
    queue<string> q;
    q.push(b);

    while(q.size()) {
        string b0=q.front();
        q.pop();

        if(b0.size()<63) {
            string b1=b0;
            b1.push_back('1');
            reverse(all(b1));
            while(b1.back()=='0')
                b1.pop_back();

            if(s.count(b1)==0) {
                s.insert(b1);
                q.push(b1);
            }
        }

        string b1=b0;
        reverse(all(b1));
        while(b1.back()=='0')
            b1.pop_back();

        if(b1.size()>0 && s.count(b1)==0) {
            s.insert(b1);
            q.push(b1);
        }
    }

    b="";
    while(y) {
        if(y%2)
            b.push_back('1');
        else
            b.push_back('0');
        y/=2;
    }
    reverse(all(b));
    if(s.count(b))
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;



}

signed main() {
    solve();
}
