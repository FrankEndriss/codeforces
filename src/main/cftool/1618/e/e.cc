/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/**
 * In each town there was one singers firsts concerts, ones seconds... and so on
 * b[i]=a[i]+
 *   a[i-1]*2 +
 *   a[i-2]*3 +
 *   ...
 * So it is gauss elemenation.
 * But we cannot do gauss with N=2e5
 *
 * Since a[x]>0, the value of
 * a[i+1]<=(b[i]- (n-1)*(n-2)/2)/n
 *
 * But consider all a[x] same value.
 * Then sum(b[])=n*(n-1)/2 * n * a[i]
 *
 * Each a[i] is in sum(b[]) n*(n-1)/2 times
 *
 * Consider the diff
 * b[i+1]-b[i]=sum(a[])-(a[i+1]*n)
 * b[i+1]-b[i]-sum(a[])=-(a[i+1]*n)
 * b[i+1]-b[i]-sum(a[]) /n =-a[i+1]
 */
void solve() {
    cini(n);
    cinai(b,n);

    if(n==1) {
        cout<<"YES"<<endl;
        cout<<b[0]<<endl;
        return;
    }

    int sum=accumulate(all(b), 0LL);

    if(sum%(n*(n+1)/2)!=0) {
        cout<<"NO"<<endl;
        return;
    }

    int sumA=sum/(n*(n+1)/2);

    bool ok=true;
    vi ans(n);
    for(int i=0;i<n; i++) {
        int val= (b[(i+1)%n]-b[i]-sumA);
        if(val%n==0 && val/-n>0) {
            ans[(i+1)%n]=(b[(i+1)%n]-b[i]-sumA)/-n;
        } else 
            ok=false;
    }

    if(ok)  {
        cout<<"YES"<<endl;
        for(int i=0; i<n; i++) 
            cout<<ans[i]<<" ";
        cout<<endl;
    } else 
        cout<<"NO"<<endl;


    
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
