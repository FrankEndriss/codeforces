/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * All numbers in a are also in s.
 * So, if there is a number in s it can be there because it is in a,
 * or because it is the gcd of one bigger number and another one.
 * So lets create the number biggest to smallest.
 *
 * Biggest number of s must be in a because it cannot be the gcd of
 * a bigger number.
 * Then process numbers from biggest to smallest.
 * Foreach number, if two coprime multiples of it are in a, then
 * we can put it into a, but we do not need to.
 * But it also does not hurt, so put it in.
 *
 * How comes the order of numbers into play?
 *
 * What about a[0], 1, a[1], 1, a[2], 1,...
 * And then check/correct if 1 is not in s
 *
 * Find
 */
void solve() {
    cini(m);
    cinai(s,m);

    int g=s[0];
    for(int i : s)
        g=gcd(g, i);

    if(g!=s[0]) {
        cout<<-1<<endl;
        return;
    }

    vi ans;
    for(int i=0; i<m; i++) {
        ans.push_back(s[i]);
        ans.push_back(s[0]);
    }

    cout<<ans.size()<<endl;
    for(int i : ans)
        cout<<i <<" ";
    cout<<endl;
}

signed main() {
    solve();
}
