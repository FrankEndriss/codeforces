/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int llog(int a) {
    for(int i=0; i<32; i++) {
        if((1<<i)==a)
            return i;
    }
    assert(false);
}

void solve() {
    cinll(n);
    cini(m);
    ll sum=0;
    vi a(33);
    for(int i=0; i<m; i++) {
        cini(aux);
        sum+=aux;
        a[llog(aux)]++;
    }
    if(sum<n) {
        cout<<-1<<endl;
        return;
    }

    int ans=0;
    for(int i=0; i<32; i++) {
        if(n&(1LL<<i)) {
            if(a[i]==0) {
                bool ok=false;
                for(int j=i+1; j<32; j++) {
                    if(a[j]) {
                        a[j]--;
                        a[j-1]+=2;
                        ans++;
                        i--;
                        ok=true;
                        break;
                    }
                }
                if(!ok) {
                    cout<<-1<<endl;
                    return;
                }
                continue;
            } else
                a[i]--;
        }
        while(a[i]>=2) {
            a[i]-=2;
            a[i+1]++;
        }
    }
    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

