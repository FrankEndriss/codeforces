/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * We need to find how much
 * was spent on  which route,
 * without travel cards.
 * Then decrease that amount for
 * max k routes to f.
 * Then sum up all routes.
 */
void solve() {
    cini(n);
    cini(a);
    cini(b);
    cini(k);
    cini(f);

    string prev="::";
    map<string,int> cost;
    for(int i=0; i<n; i++) {
        cins(st);
        cins(fi);
        string r=min(st,fi)+":"+max(st,fi);
        if(prev==st)
            cost[r]+=b;
        else
            cost[r]+=a;

        prev=fi;
    }
    vi c;
    for(auto ent : cost)
        c.push_back(ent.second);
    sort(all(c), greater<int>());
    for(int i=0; i<k && i<c.size(); i++) 
        c[i]=min(c[i],f);

    int ans=accumulate(all(c), 0LL);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
