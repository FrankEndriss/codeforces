/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* compress blocks.
 * then use 1 element from each block, and decrement all blocks.
 * cnt++
 * repeat
 *
 * But this is O(n^2)
 * simulate on linked list.
 *
 * Every block counts as that number it is more that the previous
 * (other colored) block.
 */
void solve() {
    cini(n);
    cins(s);

    vector<stack<int>> st(2);
    vi ans(n);
    int cnt=0;

    for(int i=0; i<n; i++) {
        int sym=s[i]-'0';
        if(st[!sym].size()==0) {
            cnt++;
            st[sym].push(cnt);
            ans[i]=cnt;
        } else {
            int q=st[!sym].top();
            ans[i]=q;
            st[!sym].pop();
            st[sym].push(q);
        }
    }
    cout<<cnt<<endl;
    for(int i=0; i<n; i++) 
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
