/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * How to find answer for 1 path?
 * -> We would half the biggest vertex until sum is as wanted.
 *
 * It is simply that each weight counts as often as there are paths throug it.
 * Count the paths, then multiply, then build the sum.
 * then use prio_queue and subtract until number of moves found.
 */
void solve() {
    cini(n);
    cini(S);

    vector<vector<pii>> adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        cini(w);
        u--;
        v--;
        adj[u].emplace_back(v, w);
        adj[v].emplace_back(u, w);
    }

    //using pair<int,pii>=pval;  
    /* <removal,<edgeval,mult>> */

    priority_queue<pair<int,pii>> prio;
    int sum=0;

    /* @return cnt of pathes in this subtree */
    function<int(int,int)> dfs=[&](int v, int p) {
        int cnt=0;

        for(pii c : adj[v]) {
            if(c.first!=p) {
                int lcnt=dfs(c.first, v);
                int half=c.second/2;
                half=c.second-half;
                prio.push({lcnt*half,{c.second,lcnt}});
                sum+=lcnt*c.second;
                cnt+=lcnt;
            }
        }
        if(cnt==0)
            cnt=1;
        return cnt;
    };

    dfs(0,-1);

    int ans=0;
    while(sum>S) {
        pair<int,pii> e=prio.top();
        prio.pop();
        ans++;
        sum-=e.first;

        e.second.first/=2;
        int half=e.second.first/2;
        e.first=(e.second.first-half)*e.second.second;
        if(e.first)
            prio.push(e);
    }

    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
