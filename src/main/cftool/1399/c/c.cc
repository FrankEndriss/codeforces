/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    vi fa(51);
    for(int i=0; i<n; i++) {
        cini(aux);
        fa[aux]++;
    }

    vb fs(101, true);
    /*
    for(int i=1; i<=50; i++) {
        if(fa[i]==0)
            continue;

        if(fa[i]>1)
            fs[i*2]=true;

        for(int j=i+1; j<=50; j++) {
            if(fa[j]>0)
                fs[i+j]=true;
        }
    }
    */

    int ans=0;
    for(int i=2; i<=100; i++) {
        if(!fs[i])
            continue;

        int lans=0;
        for(int j=1; j<=i/2; j++) {
            if(j+j==i) {
                lans+=fa[j]/2;
            } else {
                //cerr<<"adding j="<<j<<" i-j="<<i-j<<" sum="<<i<<endl;
                if(i-j<=50)
                    lans+=min(fa[j], fa[i-j]);
            }
        }
        //cerr<<"lans="<<lans<<endl;
        ans=max(ans, lans);
    }
    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
