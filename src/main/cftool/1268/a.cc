/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to repeat the first k digits.
 * If the resulting number is to small, 
 * then we need to add 1 to the number
 * represented by the first k digits, and
 * then use that number as the repeater.
 * Be careful if addition of 1 creates
 * an string one digit longer.
 */
void solve() {
    cini(n);
    cins(k);
    cinai(a,n);

    string b=a.substr(0,k);
    string ans;
    bool ok=true;
    for(int i=0; i<n; i++) {
        ans+=b[i%k];
        if(ans.back()<s[i]) {
            ok=false;
            break;
        }
    }
    if(ok) {    
        cout<<n<<endl;
        cout<<ans<<endl;
        return;
    }
    int carry=1;
    for(int i=k-1; i>=0; i--) {
        b[i]+=carry;
        if(b[i]=='9'+1) {
            b[i]='0';
            carry=1;
        } else
            break;
    }
    if(carry)
        b="1"+b;

    ans.clear();
    for(int i=0; i<n+carry; i++) 
        ans+=b[i%b.size()];

    cout<<n+carry<<endl;
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
