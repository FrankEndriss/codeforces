/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

vs digits= { "1110111", "0010010", "1011101", "1011011", "0111010",
             "1101011", "1101111", "1010010", "1111111", "1111011"
           };


/* @return number of segment we need to switch
 * on to get digit digit, or -1 if not possible.
 */
int posi(string &s7, int digit) {
    int ans=0;
    for(int j=0; j<7; j++) {
        if(s7[j]=='1' && digits[digit][j]=='0')
            return -1;
        else if(s7[j]=='0' && digits[digit][j]=='1')
            ans++;
    }
    return ans;
}

/* 
 * Exaktly k.
 * So we need to dp/brute force this.
 * dp[i][j]=best digit possible if starting at i with k==j
 */
void solve() {
    cini(n);
    cini(k);
    cinas(s,n);

    vvi dp(n+1, vi(k+1, -1));
    dp[n][0]=0; 

    for(int i=n-1; i>=0; i--) {
        for(int d=0; d<10; d++) {   
            int cnt=posi(s[i], d);
            if(cnt>=0) {    /* d is creatable with cnt switches */
                for(int j=0; j+cnt<=k; j++)
                    if(dp[i+1][j]>=0)
                        dp[i][j+cnt]=max(dp[i][j+cnt], d);
            }
        }
    }

    if(dp[0][k]<0) {
        cout<<-1<<endl;
        return;
    }

    string ans;
    for(int i=0; i<n; i++) {
        int d=dp[i][k];
        ans+=(char)('0'+d);
        k-=posi(s[i], d);
    }
    assert(k==0);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
