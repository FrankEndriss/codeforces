/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Let tripartite r[], and use middle
 * part for middle balls.
 * What if n not divisable by 3?
 * -> put them in middle group.
 * Greedy
 *
 * Assume
 * 1 1 1 1  1 1 1 1  2 3 3 4
 * How do we find 1 2 3, 1 3 4 ?
 * Remove all 1 which are more freq than n/3
 */
void solve() {
    cini(n);
    vi r(n);
    map<int,int> f;
    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux]++;
        r[i]=aux;
    }

    vector<pii> ff;
    for(auto ent : f) 
        ff.push_back({ ent.second, ent.first});

    sort(all(ff), greater<pii>());
    while(ff[0].first>n/3) {
        for(int i=0; i<ff.size(); i++) {
            if(ff[i].first>n/3) {
                int d=ff[i].first-n/3;
                n-=d;
                ff[i].first-=d;
            }
        }
    }

    r.clear();
    for(int i=0; i<ff.size(); i++) {
        for(int j=0; j<ff[i].first; j++)
            r.push_back(ff[i].second);
    }
    sort(all(r));

    vector<multiset<int>> rr(3);
    for(int i=0; i<n/3; i++) {
        rr[0].insert(r[i]);
        rr[2].insert(r[n-1-i]);
    }
    for(int i=0; i<n-n/3-n/3; i++) 
        rr[1].insert(r[i+n/3]);

    vvi ans(3);
    for(auto it0=rr[0].begin(); it0!=rr[0].end(); it0++) {
        auto it1=rr[1].upper_bound(*it0);
        if(it1!=rr[1].end()) {
            auto it2=rr[2].upper_bound(*it1);
            if(it2!=rr[2].end()) {
                ans[0].push_back(*it0);
                ans[1].push_back(*it1);
                ans[2].push_back(*it2);
                rr[1].erase(it1);
                rr[2].erase(it2);
            }
        }
    }

    cout<<ans[0].size()<<endl;
    for(int i=0; i<ans[0].size(); i++) 
        cout<<ans[2][i]<<" "<<ans[1][i]<<" "<<ans[0][i]<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
