/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=10000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* We need to find the number of squares
 * - lteq 2*n
 * - usable as a in a^2=c^2-b^2
 *
 * How to quick find the squares which are sums 
 * of two other sqares?
 * ***
 * Given to some google formular we want to write the three numbers as:
 * a=m*m - n*n 
 * b=2*m*n 
 * c=m*m+n*n
 * Since c must be <=2*N, we might count all pairs of m>n with
 * m*m+n*n<=2*N
 *
 * How?
 *
 */
const int N=1e5+7;
const int NN=2e9+7;
vi sq(N);
vi ans={0};

void init() {
    //for(int i=1; i<N; i++) 
    //    sq[i]=i*i;

    for(int i=1; pr[i]*pr[i-1]<=NN; i++) {
        ans.push_back(ans.back()+i);
    }
}

void solve() {
    cini(n);
    int l=0;
    int r=ans.size();
    while(l+1<r) {
        int mid=(l+r)/2;
        int val=pr[mid]*pr[mid];
        if(val<=n)
            l=mid;
        else
            r=mid;
    }
    cout<<ans[l]<<endl;
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
