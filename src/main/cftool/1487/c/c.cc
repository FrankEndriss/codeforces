/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* if number of teams is odd
 * each team should win the same number of times as loose.
 * if number of teams is even
 * echo team should have one tie, and else same number of loose and win.
 */
void solve() {
    cini(n);

    if(n%2==1) {
        for(int i=0; i<n; i++) {
            for(int j=i+1; j<n; j++) {
                if(j==i)
                    continue;
                if((i&1)^(j&1))
                    cout<<1<<" ";
                else
                    cout<<-1<<" ";
            }
        }
    } else {
        for(int i=0; i<n; i++) {
            for(int j=i+1; j<n; j++) {
                if(j==i)
                    continue;
                if(i%2==0 && j==i+1)
                    cout<<0<<endl;
                else if(i%2==1 && j==i-1)
                    cout<<0<<endl;
                else if((i&1)^(j&1))
                        cout<<1<<" ";
                    else
                        cout<<-1<<" ";
            }
        }
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
