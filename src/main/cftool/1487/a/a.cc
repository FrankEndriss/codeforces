/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* A hero can win if its level
 * plus the number of heros with smaller level
 * is bigger than the level of the best
 * hero.
 * ...
 * No, a hero can win if he is not the worst hero.
 */
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));

    if(n==1) {
        cout<<1<<endl;
        return;
    }

    for(int i=1; i<n; i++) {
        if(a[i]>a[0]) {
            cout<<n-i<<endl;
            return;
        }
    }
    cout<<0<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
