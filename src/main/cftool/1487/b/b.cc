/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* If n is even then the never occ the same spot.
 * Else it is one jump per round.
 * No...
 * Poor index fiddling again. :/
 *
 * On odd n A and B occ the same spot after n/2 th
 * step. Then again after n/2 th step and so on.
 * Something wrong... :/
 */
void solve() {
    cini(n);
    cini(k);
    k--;

    if(n%2==0) {
        cout<<(k%n)+1<<endl;
        return;
    } else {
        int cyc=n/2;
        int cnt=k/cyc;

        int pos=(cyc+1)*cnt+k%cyc;
        pos%=n;
        cout<<pos+1<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
