/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* Find foreach c the best d.
 * find foreach b the best a.
 * find foreach b the best c, output best of those.
 */
const int INF=1e9;
void solve() {
    cinai(n,4);
    vvi cost(4);
    vvi id(4);
    for(int i=0; i<4; i++)  {
        cost[i].resize(n[i]);
        for(int j=0; j<n[i]; j++)
            cin>>cost[i][j];

        id[i].resize(n[i]);
        iota(all(id[i]), 0LL);
        sort(all(id[i]), [&](int i1, int i2) {
            return cost[i][i1]<cost[i][i2];
        });
    }

    vvvi adj(3);
    for(int i=0; i<3; i++) {
        adj[i].resize(n[i]);
        cini(m);
        for(int j=0; j<m; j++) {
            cini(x);
            x--;
            cini(y);
            y--;
            adj[i][x].push_back(y);
        }
        for(int j=0; j<n[i]; j++)
            sort(all(adj[i][j]));
    }

    vi bestDforC(n[2]);
    vi costC(n[2], INF); /* cost of C plus best D */
    for(int i=0; i<n[2]; i++) {
        /* find the first d not in blocklist */
        for(int idx : id[3]) {
            if(!binary_search(all(adj[2][i]), idx)) {
                costC[i]=cost[2][i]+cost[3][idx];
                break;
            }
        }
    }


    vi bestAforB(n[1]);
    vi costB(n[1], INF); /* cost of B plus A */
    /* we need reverse blocklist */
    vvi adjR(n[1]);
    for(int j=0; j<n[0]; j++)
        for(int k : adj[0][j])
            adjR[k].push_back(j);

    for(int j=0; j<n[1]; j++)
        sort(all(adjR[j]));

    for(int i=0; i<n[1]; i++) {
        /* find the first a not in blocklist */
        for(int idx : id[0]) {
            if(!binary_search(all(adjR[i]), idx)) {
                costB[i]=cost[1][i]+cost[0][idx];
                break;
            }
        }
    }

    /* now find cheapest possible pair costB,costC */

    vi id2(costC.size());
    iota(all(id2), 0LL);
    sort(all(id2), [&](int i1, int i2) {
        return costC[i1]<costC[i2];
    });

    int ans=INF;
    for(size_t i=0; i<costB.size(); i++) {
        for(int idx : id2) {
            if(!binary_search(all(adj[1][i]), idx)) {
                ans=min(ans, costB[i]+costC[idx]);
                break;
            }
        }
    }
    if(ans>=INF)
        ans=-1;

    cout<<ans<<endl;
}

signed main() {
    solve();
}
