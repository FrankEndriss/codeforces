/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int dist(char c1, char c2) {
    if(c1>c2)
        swap(c1,c2);
    return min(c2-c1, c1-c2+26);
}

/* move p to first half of n */
int mirror(int p, int n) {
    if(p<=(n-1)/2)
        return p;
    else
        return n-1-p;
}

/* Looks like dp.
 * Ok, every position has a counterpart, and the number of 
 * up/down is fixed. So it is only the question how is shortest path 
 * to reach all positions.
 * And that is not dp, but shortest path bfs. Or greedy.
 *
 * How to get this simple shit right?
 * -> We need to find all positions where diffs are, and 
 *  need to find a path over all those positions.
 *  We can use the positions of the pairs which are in that
 *  half of the circle where the starting position is.
 *  Or simpler, move the starting position to the first
 *  half, and use that one.
 *
 *  Then there are two strategies:
 *  -walk to the leftmost, then right to the rightmost.
 *  -walk to the rightmost, then left to the leftmost.
 */
void solve() {
    cini(n);
    cini(p);
    cins(s);

    p=mirror(p-1,n);

//cerr<<"pathlen="<<pathlen<<endl;
    int mipos=p;
    int mapos=p;
    int pathlen=0;
    for(int i=0; i<=n/2-1; i++) {
        if(s[i]!=s[n-1-i]) {
            mipos=min(mipos, i);
            mapos=max(mapos, i);
        }
        pathlen+=dist(s[i], s[n-1-i]);
    }

    pathlen+=min(abs(p-mipos)+abs(mapos-mipos), abs(p-mapos)+abs(mapos-mipos));
    cout<<pathlen<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
