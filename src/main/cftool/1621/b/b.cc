/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Wiered confusing statement :/
 * However, we want to buy the cheapest segment including the leftmost int,
 * and the cheapest segemnt including the rightmost segment.
 *
 * Also we need to consider the extra case if one segemnt
 * covers minL..maxR
 * How to implement?
 *
 * On first seg, one seg covers all, consider this segment the special case.
 */
const int INF=1e18;
void solve() {
	cini(n);
	int minL=INF;
	int minCostL=INF;
	int maxR=0;
	int minCostR=0;

	int coveralcost=-1;
	int coverallL=INF;
	int coverallR=-INF;

	for(int i=1; i<=n; i++) {
		cini(l);
		cini(r);
		cini(c);

		if(l==coverallL && r==coverallR) {
			coveralcost=min(coveralcost, c);
		} else if(l<=coverallL && r>=coverallR) {
			coverallL=l;
			coverallR=r;
			coveralcost=c;
		}

		if(l<minL) {
			minL=l;
			minCostL=c;
		} else if(minL==l) {
			minCostL=min(c, minCostL);
		}

		if(r>maxR) {
			maxR=r;
			minCostR=c;
		} else if(maxR==r) {
			minCostR=min(c, minCostR);
		}

		int ans;
		if(minL==coverallL && maxR==coverallR) {
			ans=min(coveralcost, minCostL+minCostR);
		} else  {
			ans=minCostL+minCostR;
		}
		cout<<ans<<endl;
	}
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
