/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Obs:
 * If it is more teachers than groups, we can forget about the n-m youngest teachers.
 *
 * Then sort the groups and teachers by age, avgage
 * Obs: By removing one student the avg of one group changes,
 * so the position of one group changes by j positions,
 * and all from i to j by 1 position (in oposite direction).
 * So we need to check all groups from i to j.
 * Also, j can be found by binary search.
 *
 * ...For some reason that check of positions i..j is fast enough,
 * in spite of looking like O(n^2).
 */
void solve() {
	cini(n); 	/* teachers */
	cini(m);	/* groups */

	cinai(a,n);	/* teacher ages */
	sort(all(a), greater<int>());
	a.resize(m);
	reverse(all(a));

	vvi b(m);
	vi sum(m);
	for(int i=0; i<m; i++) {
		cini(k);
		cinai(bb,k);
		b[i].swap(bb);
		sum[i]=accumulate(all(b[i]), 0LL);
	}
	vi id(m);
	iota(all(id), 0);
	sort(all(id), [&](int i, int j) { return sum[i]<sum[j] });

	// ... TODO

}

signed main() {
	cini(t);
	while(t--) 
    		solve();
}
