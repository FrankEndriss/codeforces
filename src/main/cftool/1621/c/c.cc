/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/* ask 0 based */
int acnt=0;
int ask(int pos) {
	acnt++;
	cout<<"? "<<pos+1<<endl;
	int ans;
	cin>>ans;
	return ans-1;
}

/*
vi p={ 1, 3, 5, 2, 4 };
vi q= {0,1,2,3,4};
int ask(int pos) {
	acnt++;
	int ans=q[pos];
	vi q0=q;
	for(size_t i=0; i<p.size(); i++) {
		q0[i]=q[p[i]-1];
	}
	q=q0;
	//cerr<<"ask pos="<<pos<<" ans="<<ans<<endl;
	return ans;
}
*/

/**
 * Consider first position, query it until its first value is repeated.
 * All nubmers that where there build a cycle.
 * Continue to do so with all other unknown numbers.
 *
 * So, when we query at some position a y then a x, it means that
 * ans[x]=y
 */
void solve() {
	cini(n);
	vi ans(n, -1);

	for(int i=0; i<n; i++) {
		if(ans[i]<0) {
			// ask circle including i

			int start=ask(i);
			int v=start;
			do {
				int vv=ask(i);
				ans[v]=vv;
				v=vv;
			}while(v!=start);
		}
	}

	cout<<"! ";
	for(size_t i=0; i<ans.size(); i++) {
		cout<<ans[i]+1<<" ";
	}
	cout<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
