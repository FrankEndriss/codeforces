/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Obviously we need to take the upper left initial cells and the
 * lower right final cells.
 *
 * Then see editorial.
 * From the initial position of the friends, one the 4 corners must be moved
 * anyway whatever we do.
 * Then think about, each of these 8 cells connect the source and the target
 * square, so we can use all friends through it.
 * Because of this, ans=min value of these 8 cells.
 */
const int INF=1e18;
void solve() {
	cini(n);
	//cerr<<"n="<<n<<endl;

	vvi a(2*n, vi(2*n));
	int ans=0;
	for(int i=0; i<2*n; i++) {
		for(int j=0; j<2*n; j++) {
			cin>>a[i][j];
			if((i<n && j<n)  || (i>=n && j>=n)) {
				ans+=a[i][j];
			}
		}
	}

	const int N=n*2;
	ans=ans+min({
		a[0][n],
		a[0][N-1],
		a[n-1][n],
		a[n-1][N-1],
		a[n][0],
		a[n][n-1],
		a[N-1][0],
		a[N-1][n-1],
	});


	cout<<ans<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
