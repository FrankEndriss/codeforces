/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Create a linked list 1,4,5,6...,2
 * Then find the position in that list where we want
 * to start the next list, to fullfill
 * d23 and d31.
 *
 * So much off by one traps... :/
 */
void solve() {
    cini(n);
    cini(d12);  /* number of edges */
    cini(d23); 
    cini(d31); 

    if(d23>d12+d31) {
        cout<<"NO"<<endl;
        return;
    }

    if(d31>d12+d23) {
        cout<<"NO"<<endl;
        return;
    }

    if(d12>d23+d31) {
        cout<<"NO"<<endl;
        return;
    }

    vi l;
    l.push_back(1);

    vector<pii> ans;
    int idx=4;
    int prev=1;
    for(int i=0; i+1<d12; i++)  {
        ans.emplace_back(prev, idx);
        l.push_back(idx);
        prev=idx;
        idx++;
    }
    ans.emplace_back(prev, 2);
    l.push_back(2);

    bool done=false;
    for(int pos=0; pos<(int)l.size(); pos++) {
        if(d31+pos==d12-pos+d23) {
            done=true;
            prev=l[pos];
            for(int i=0; pos+i<d31; i++) {
                ans.emplace_back(prev,idx);
                prev=idx;
                idx++;
            }
        }
    }
    if(!done || idx>n+1) {
        cout<<"NO"<<endl;
        return;
    }
    ans.emplace_back(prev,3);

    prev=1;
    while(idx<=n) {
        ans.emplace_back(prev, idx);
        prev=idx;
        idx++;
    }

    assert(ans.size()==n-1);
    cout<<"YES"<<endl;
    for(int i=0; i<n-1; i++) 
        cout<<ans[i].first<<" "<<ans[i].second<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
