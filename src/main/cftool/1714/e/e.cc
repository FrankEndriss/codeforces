/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider the last digit:
 * 0.
 * 1, 2, 4, 8, 6, 2,...
 *            16,22,24,28,36,42,...
 *
 * 3, 6, 2, 4, 8, 6,...
 * 5, 0.
 * 7, 4, 8, 6, 2, 4,...
 * 9, 8, 6, 2, 4, 8,...
 *
 * also the distances (to the 10th where the 8 is last digit) must fit.
 */
void solve() {
    cini(n);
    cinai(a,n);
    vb check(2);
    set<int> dist;

    for(int i=0; i<n; i++) {
        int aa=a[i]%10;
        if(aa==0 || aa==5) {
            check[0]=true;
        } else {
            check[1]=true;
            if(aa==1)
                dist.insert((a[i]/10+0)%2);
            else if(aa==2)
                dist.insert((a[i]/10+0)%2);
            else if(aa==3)
                dist.insert((a[i]/10+1)%2);
            else if(aa==4)
                dist.insert((a[i]/10+0)%2);
            else if(aa==6)
                dist.insert((a[i]/10+1)%2);
            else if(aa==7)
                dist.insert((a[i]/10+1)%2);
            else if(aa==8)
                dist.insert((a[i]/10+0)%2);
            else if(aa==9)
                dist.insert((a[i]/10+1)%2);
        }
    }
    if(check[0]&&check[1])
        cout<<"No"<<endl;
    else {
        if(check[0]) {
            set<int> sa;
            for(int i=0; i<n; i++) 
                sa.insert(a[i]);

            if(sa.size()==1 || (sa.size()==2 && (*sa.begin())%10==5 && (*sa.begin())+5==*sa.rbegin()))
                cout<<"Yes"<<endl;
            else
                cout<<"No"<<endl;
        } else {
            if(dist.size()==1) 
                cout<<"Yes"<<endl;
            else 
                cout<<"No"<<endl;
        }
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
