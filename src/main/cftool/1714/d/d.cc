/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Input is short, so brute force:
 * dp[i]=min number of ops to color prefix of len i
 * ans[i]=idx of string that was used to color last position of 
 *              prefix of len i
 *
 * ...That no fun to implement.
 */
const int INF=1e9;
void solve() {
    cins(t);
    cini(n);
    cinas(s,n);

    vi dp(t.size()+1, INF);
    dp[0]=0;
    vi ans(t.size()+1, -1);
    cerr<<"t="<<t<<endl;
    for(size_t j=0; j<t.size(); j++) {
        cerr<<"check at pos="<<j<<endl;

        for(int i=0; i<n; i++) { /* check if s[i] fits at current position */
            cerr<<"check: "<<s[i]<<endl;
            bool ok=false;
            if(j+s[i].size()<=t.size()) {
                ok=true;
                for(int k=0; k<s[i].size(); k++) {
                    if(s[i][k]!=t[j+k]) {
                        ok=false;
                        break;
                    }
                }
            }

            cerr<<"ok="<<ok<<endl;
            if(ok) { /* yes, it fits */
                if(dp[j+s[i].size()]>dp[j]+1) {
                    for(int k=0; k<s[i].size(); k++) {
                        dp[j+1+k]=min(dp[j+1+k], dp[j]+1);
                        ans[j+1+k]=i;
                    }
                }
            }
        }
    }
    if(dp[t.size()]==INF)
        cout<<-1<<endl;
    else {
        int prev=-1;
        vector<pii> p;
        for(int i=1; i<=t.size(); i++) {
            if(ans[i]!=prev)  {
                p.emplace_back(ans[i]+1, i);
            }
            prev=ans[i];
        }
        cout<<p.size()<<endl;
        for(size_t i=0; i<p.size(); i++) 
            cout<<p[i].first<<" "<<p[i].second<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
