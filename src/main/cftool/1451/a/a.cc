/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int bdiv(int n) {
    for(int i=2; i*i<=n; i++) 
        if(n%i==0)
            return i;
    return 0;
}

/* do we need to implement a dijkrstra?
 * idk :/
 * ***
 * There is a simple trick.
 * Since we can reduce each even number in one step to 2,
 * we use one step to make n even.
 */
void solve() {
    cini(n);

    if(n==1)
        cout<<0<<endl;
    else if(n==2)
        cout<<1<<endl;
    else if(n==3)
        cout<<2<<endl;
    else if(n%2==0)
        cout<<2<<endl;
    else
        cout<<3<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
