/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* we need to be able to use some char left of s[l]
 * instead of s[l], or some char right of s[r]
 * instead of s[r].
 */
void solve() {
    cini(n);
    cini(q);
    cins(s);
    int first0=-1;
    int first1=-1;
    int last0=-1;
    int last1=-1;

    for(int i=0; i<n; i++) {
        if(s[i]=='0') {
            last0=i;
            if(first0<0)
                first0=i;
        } else {
            last1=i;
            if(first1<0)
                first1=i;
        }
    }

    for(int i=0; i<q; i++) {
        cini(l);
        cini(r);
        if(l==r) {
            cout<<"NO"<<endl;
            continue;
        }
        l--;
        r--;

        if(     ((s[l]=='1' && first1==l) || (s[l]=='0' && first0==l)) && 
                ((s[r]=='1' && last1==r)  || (s[r]=='0' && last0==r)))
            cout<<"NO"<<endl;
        else
            cout<<"YES"<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
