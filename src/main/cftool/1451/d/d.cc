/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Consider the current x value.
 * There are yy available moves on y axis.
 * The current player wants the number of available
 * moves to be odd, the other one even.
 *
 * ...
 * Note that on first of all moves the position is
 * symetric, so it does not matter where to move.
 * Then consider the second move. Second player wants the
 * number of moves to be even, so if staying on the diagonal results
 * in even number of moves, he will move back to the diagonal.
 * If diagonal is of odd size (d/k is odd), then
 * he must move in same dir as first move. Which results in position
 * of the diagonal two fields offset...
 *
 * So, if the d0-diagonal is win for P2, then P2 wins.
 * Else if the d1 or d3-diagonal is win for P1, then P1 wins.
 * Else if the d2 diagonal is win for P2...
 * They alternate.
 *
 * How to find number of moves if we go twice (or y-times)
 * on one axis?
 * Position after that moves is y*k,0
 * We need to find max r, such that 
 * ((y*k)+(r*k))*(r*k)<=(d/k)*(d/k)
 *
 * at first make d=d/k, and k==1
 * (y+r)*(y+r)+(r*r)<=d*d
 * y^2 + 2*y*r + r^2 + r^2 <= d^2
 *
 * ...see tutorial:
 * Let y be the possible number of moves on the diagonal.
 * Consider the field y*k,(y+1)*k:
 * If this is outside of the circle, then p2 forces the last
 * field to be on the diagonal.
 * Else p1 forces the last field to be that one.
 */
void solve() {
    cini(d);
    cini(k);

    /* find biggest y so that hypot(y,y)<=d */
    int l=0;
    int r=d/k+1;
    while(l+1<r) {
        int mid=(l+r)/2;
        int val=mid*k*mid*k*2;
        if(val>d*d)
            r=mid;
        else
            l=mid;
    }
    //cerr<<"k="<<k<<" d="<<d<<" l="<<l<<endl;

    int val=(l*k)*(l*k)+((l+1)*k)*((l+1)*k);
    if(val>d*d) {
        cout<<"Utkarsh"<<endl;
    } else {
        cout<<"Ashish"<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
