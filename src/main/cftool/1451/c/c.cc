/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Note that there is no way to decrement 
 * any char.
 * Ok, we can increment only k chars at once.
 *
 * So, we sort both strings, since we can swap arbitray anyway.
 * Then check if we can make a prefix equal, simulate.
 * If all prefixes can be made equal then ans="Yes".
 *
 * ...
 * Consider some substring 
 * a="...aaaaaaa..."
 * a="...aabbaaa..."
 * a="...aabbcca..."
 *
 *
 * a="...aaabbbb..."
 *
 * b="...aabbccc..."
 * and k=2
 */
void solve() {
    cini(n);
    cini(k);
    cins(a);
    cins(b);

    sort(all(a));
    sort(all(b));

    //cerr<<"next a="<<a<<" b="<<b<<endl;
    if(a>b) {
        cout<<"No"<<endl;
        return;
    }

    for(int i=0; i+k-1<n; i++) {
        if(a[i]<b[i]) {
            bool alleq=true;
            char c=a[i];
            a[i]=b[i];
            for(int j=1; j<k; j++) {
                if(c!=a[i+j])
                    alleq=false;
                else
                    a[i+j]=a[i];
            }
            if(!alleq) {
                //cerr<<" no alleq, a="<<a<<endl;
                cout<<"No"<<endl;
                return;
            }
        } else if(a[i]>b[i]) {
            //cerr<<" a[i]>b[i], a="<<a<<endl;
            cout<<"No"<<endl;
            return;
        }
    }
    //cerr<<"b="<<b<<" a="<<a<<endl;
    if(a==b)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
