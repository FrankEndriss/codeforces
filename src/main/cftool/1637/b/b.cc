/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Is it simple dp?
 *
 * So solve for a prefix, and add the next element
 * to the last segment, or create a new segment.
 * Then state is index,numSegmentsUsed,sizeRightSegment.
 *
 * MEX is much irritating concept.
 * Also, statement is hard to get. What does it mean, 
 * "Find the sum of values of all its subsegments."
 * Which segments?
 * ->Consider all.
 * And then?
 *
 *
 * Maybe solve it better by a segment tree.
 * How to combine two segments?
 * What is the state of one segment?
 *
 * Or just solve starting at all positions?
 *
 * -> It is kind of a joke.
 * There cannot be more cost than size of a subarray.
 * So just use that.
 * But also consider the 0, they contribute 1 extra, how often?
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi pre(n+1);
    for(int i=0; i<n; i++) 
        pre[i+1]=pre[i]+(a[i]==0);

    int ans=0;
    for(int i=0; i<n; i++) 
        for(int j=i; j<n; j++)  {
            ans+=(j-i+1);
            ans+=pre[j+1]-pre[i];
        }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
