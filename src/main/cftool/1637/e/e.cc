/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * What to do here?
 *
 * Just check each pair in O(n^3)?
 *
 * We can somehow optimize by frequency.
 * Note that for a given freqency cntx=f, we only have to check the biggest
 * of value whith freq=f.
 * So build all pairs of frequencies, which are at most N.
 * Then simply check that pair.
 *
 * ...
 * Unfortunatly we do _not_ only have to check the biggest element of a freq,
 * but we need to consider the two biggest ones.
 */
void solve() {
    cini(n);
    cini(m);

    cinai(a,n);

    set<pii> bad;
    for(int i=0; i<m; i++)  {
        cini(x);
        cini(y);
        bad.emplace(x,y);
        bad.emplace(y,x);
    }

    map<int,int> f;
    for(int i=0; i<n; i++) 
        f[a[i]]++;

    map<int,int> ff;
    for(auto ent : f) 
        ff[ent.second]=ent.first;

    vector<pii> vff;
    for(auto ent : ff)
        vff.emplace_back(ent.first, ent.second);

    int ans=0;
    for(size_t i=0; i<vff.size(); i++) 
        for(size_t j=i+1; j<vff.size(); j++) {
            if(bad.count({vff[i].second, vff[j].second})==0)
                ans=max(ans, (vff[i].first+vff[j].first)*(vff[i].second+vff[j].second));
        }

    map<int,vi> two;    /* two[i]=list of values with freq==i */
    for(auto ent : f) 
        two[ent.second].push_back(ent.first);

    for(auto ent : two) {
        sort(all(ent.second), greater<int>());
        if(ent.second.size()>1) {
            /* find the biggest pair/sum in ent.second[] that is not bad :/ */
            for(size_t k=0; k<ent.second.size(); k++) 
                for(size_t j=k+1; j<ent.second.size(); j++) {
                    if(bad.count({ent.second[k],ent.second[j]})==0) {
                        ans=max(ans, (ent.second[0]+ent.second[1])*(ent.first*2));
                        break;
                    }
                }
        }
    }

    
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
