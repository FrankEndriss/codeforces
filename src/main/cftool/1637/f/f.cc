/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * consider h[x] of some vertex.
 * Obviously we need to build at least 2 towers with hight at least h[x]
 * So if we build the two towers for the max(h[x]), we would like to maximize
 * the number of other vertex also on the path between those max towers.
 * 
 * ...This ends in building towers on all leafs.
 * How to find the minimum sum of heights?
 *
 * Consider the max(h[x]), and all pairs of leafs including x on their path.
 * From all those pairs choose the one with the max(h[y]) also on the path.
 * ...and so on, until there is only one pair left. Build the towers there,
 * repeat.
 * How to implement that?
 */
void solve() {
    cini(n);
    cinai(h,n);

    vvi adj(n);
    for(int i=0; i<n; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }
}

signed main() {
    solve();
}
