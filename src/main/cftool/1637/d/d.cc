/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How does the brute force look like?
 * ->idk
 *
 * let p(i) be the number of pairs index i is part of.
 *
 * let f(a[]) be the double sum formular.
 * Consider changeing a[i],b[i]. Let x be a[i]-b[i],
 * so 
 * a[i]-=x
 * b[i]+=x
 *
 * To decide for one index is simple, also brute forceable.
 * But we cannot simply iterate the indexes, because swaping
 * at one position changes the values for all other positions.
 *
 * So it is somehow a dp.
 *
 * Or can we simply greedyly swap at arbitrary indexes
 * until no more swap gives better result?
 * -> try it
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    for(int i=0; i<n; i++)
        if(a[i]<b[i])
            swap(a[i],b[i]);

    function<int(vi&)> go=[&](vi &v) {
        int ans=0;
        for(int i=0; i<n; i++) 
            for(int j=i+1; j<n; j++) 
                ans+=(v[i]+v[j])*(v[i]+v[j]);
        return ans;
    };

    while(true) {
        bool swapped=false;
        int va=go(a);
        int vb=go(b);
        for(int i=0; i<n; i++) {
            swap(a[i], b[i]);
            int va0=go(a);
            int vb0=go(b);
            if(va0+vb0<va+vb) {
                swapped=true;
                break;
            }
        }

        if(!swapped)
            break;
    }

    cout<<go(a)+go(b)<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
