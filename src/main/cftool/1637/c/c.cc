/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to have even number of stones on each pile.
 * Else one has to be put there.
 * So some cases.
 *
 * If there is at least one pile with more than 1 
 * stone, we can fill all other piles to some 
 * even number of stones, if needed.
 *
 * Note that piles 0 and n-1 can also be used to to
 * this.
 * Note: They cannot.
 *
 * Another edgecase is n==3, there is only one chooseable i.
 */
void solve() {
	cini(n);
	cinai(a,n);

        if(n==3 && (a[1]&1)) {
            cout<<-1<<endl;
            return;
        }

	int ans=0;
	int odd=0;
	int two=0;
	for(int i=1; i+1<n; i++) {
		ans+=(a[i]+1)/2;
		if(a[i]&1)
			odd++;
		if(a[i]>1)
			two++;
	}

	if(odd==0 || two>0) {
		cout<<ans<<endl;
	} else {
		cout<<-1<<endl;
        }

}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
