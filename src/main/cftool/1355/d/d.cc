/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* 1st: a[n], sum=s
 * choose k<=s
 * then
 * 2nd: find subaray with sum=k or sum=s-k to win
 *
 * Play for 1st, print a[] and k
 * *******
 * if sum >= 2*n
 * use 1 1 1 ... s-n+1 
 * k=n
 */
void solve() {
    cini(n);
    cini(s);

    if(s>=2*n) {
        cout<<"YES"<<endl;
        for(int i=0; i<n-1; i++) {
            cout<<"1 ";
        }
        cout<<s-n+1<<endl;
        cout<<n<<endl;
    } else {
        cout<<"NO"<<endl;
    }
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
