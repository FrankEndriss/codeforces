/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* Fix end hight of wall, it is between min and max h[]
 *
 * A move is a remove+add
 * So, if m>=a+b ignore m
 * else use max possible m operations.
 *
 * We need two strategies:
 * if m<a+b we should use m
 * else we should not.
 *
 * solve move:
 * find average hight. moves until all pillows
 * have hight avg+-1
 * Then use min num/cost of add/rem.
 *
 * solve addrem:
 * binsearch the hight where the ratio of add/rem
 * is optimal.
 *
 * On every hight we have to make a certein number of adds,
 * and removes, which sums up to the costs.
 *
 * ...nah, it is all wrong :/
 */
void solve() {
    cini(n);
    cini(a);
    cini(r);
    cini(m);

    cinai(h,n);
    int sum=0;
    for(int i : h)
        sum+=i;

    function<int()> solve_move=[&]() {
        int avg=sum/n;
        int avgMod=sum%n;   /* avgMod pillows of size avg+1 */
        sort(h.begin(), h.end(), greater<int>());
        int ans=0;
        for(int i=0; i<n; i++) {
            if(i<avgMod) {
                if(h[i]>avg+1)
                    ans+=m*(h[i]-avg-1);
            } else {
                if(h[i]>avg)
                    ans+=m*(h[i]-avg);
            }
        }
        ans+=min(avgMod*r, (n-avgMod)*a);
        return ans;
    };

    function<int()> solve_addrem=[&]() {
        vi pre(n+1);
        pre[0]=0;
        for(int i=0; i<n; i++) 
            pre[i+1]=pre[i]+h[i];

        int ans=1e18;
        for(int i=0; i<n; i++) {
            int rcnt=(i+1)*h[i];    // desired number of bricks in first i+1 pillows
            int lans=(pre[i+1]-rcnt)*r; // remove costs=actual number - desired number times r

            int acnt=(n-i)*h[i];  // desired number of bricks in second half of wall 
            lans+=(acnt-(pre[n]-pre[i]))*a; // add costs
            ans=min(ans, lans);
        }
        return ans;
    };

    int ans=0;
    if(m<a+r) {
cerr<<"solve_move"<<endl;
        ans=solve_move();
    } else {
cerr<<"solve_addrem"<<endl;
        ans=solve_addrem();
    }

    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
