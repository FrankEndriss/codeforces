/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * We need to fix z and foreach add
 * number of possible x+y combinations.
 *
 * Therefor we need to build a map of all possible x+y
 * values, created somehow using prefix sums.
 *
 * How much ways to build x+y for a given range of x?
 * -> Intersection of ranges.
 */
void solve() {
    cini(a);
    cini(b);
    cini(c);
    cini(d);

    vi xy(b+c+1);

    for(int i=0; i<b+c+1; i++) {
/* how much pairs x+y=i exist? */
        int mi=max(i-c, a);
        int ma=min(i-b, b);

        if(ma>=mi)
            xy[i]=ma-mi+1;
    }

    vi pxy(xy.size());
    pxy[0]=xy[0];
    for(int i=1; i<xy.size(); i++)
        pxy[i]=pxy[i-1]+xy[i];

    int ans=0;
    for(int z=c; z<=d; z++) {
        if(z<pxy.size())
            ans+=pxy.back()-pxy[z];
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
