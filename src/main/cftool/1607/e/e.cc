/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Maintain the dist from min to max in both dimensions.
 * if dist exceeds board, robot falls off.
 * Opt start pos is then determined by min/max.
 */
void solve() {
    cini(n);
    cini(m);
    cins(s);

    //cerr<<s<<endl;
    int minR=0;
    int maxR=0;
    int minC=0;
    int maxC=0;
    int r=0;
    int c=0; 

    for(int i=0; i<s.size(); i++) {
        if(s[i]=='D')
            r++;
        else if(s[i]=='U')
            r--;
        else if(s[i]=='R')
            c++;
        else if(s[i]=='L')
            c--;
        else 
            assert(false);

        if(r-minR>=n || maxR-r>=n || c-minC>=m || maxC-c>=m) {
            cout<<1-minR<<" "<<1-minC<<endl;
            return;
        }
        minR=min(minR,r);
        maxR=max(maxR,r);
        minC=min(minC,c);
        maxC=max(maxC,c);
        //cerr<<"minR="<<minR<<" maxR="<<maxR<<" minC="<<minC<<" maxC="<<maxC<<endl;
    }

     cout<<1-minR<<" "<<1-minC<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
