/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * If there are fields we cannot enter we can start a dp there.
 * But what about circles?
 *
 * Part1:
 * Consider starting on some falloff cell, and find longest possible path
 * in reverse order of path, dfs.
 *
 * Part2:
 * All remaining fields: Note that we cannot get from the remainings to the
 * ones from part1.
 * So these are separate paths or circles.
 * So check dfs both directions.
 *
 * ....
 * But, in first place only do the second part.
 * Foreach cell that is not visited so far, do a dfs in both directions, maintain max out and in lenght
 * of each cell.
 */
void solve() {
    cini(n);
    cini(m);

    cinas(s,n);

    
    vvi dpI(n, vi(m, 0));
    vvi dpO(n, vi(m, 0));

    vi dx={ 0, 0, -1, 1 };
    vi dy={ 1, -1, 0, 0 };
    function<void(int,int)> dfsI(int i, int j) {
        if(
    };

}

signed main() {
    cini(t);
    while(t--) 
        solve();
}
