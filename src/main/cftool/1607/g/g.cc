/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The more near both are together, the more we can choose.
 * ...
 * Each dish contributes some balance in a segment min to max.
 * So we can sum left and right ends.
 * How to handle that only every second value can be used?
 *
 */
void solve() {
    cini(n);
    cini(m);

    vector<pii> p(n);
    int mi=0;   // more a[i]
    int ma=0;   // more b[i]

    for(int i=0; i<n; i++)  {
        cin>>p[i].first>>p[i].second;
        mi+=p[i].first-(p[i].second-m);
        ma+=p[i].second-(p[i].first-m);
    }

    if(mi<=0 && ma<=0) {
        cout<<abs(max(mi,ma))<<endl;
    } else if(mi>=0 && ma>=0) {
        cout<<abs(min(mi,ma))<<endl;
    } else {
        // consider odd/even etc...
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
