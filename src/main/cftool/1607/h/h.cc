/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to group dishes by m.
 * Then, foreach group, sort dishes by a[].
 * From remaining dishes, choose the one with min a,
 * and add all to same group with enough b to do so.
 * Repeat. Implement using priority queues.
 * Collect ans while adding dishes to groups.
 *
 * ...
 * turns out it is another problem, see example 1.
 * How can ans=1?
 * Even after trying an hour to understand the problem
 * and example it is unclear.
 * The basic question is "what is meant ot be a dish here"?
 * ... -> see tutorial, a dish is what is left after the
 *        taster has eaten m[i] parts of it.
 *        OMG, how can somebody assume that this problem
 *        is somehow understandable, why not just write down
 *        what is meant?
 * ****
 * So, we need to group by a[i]+b[i]-m[i]
 * And same, order by a[i], and make max
 * possible same pairs.
 * ...ans lost fun in implementing this :/
 */
const int N=1e6+7;
void solve() {
    cini(n);

    using t3=tuple<int,int,int>;
    vector<vector<t3>> g(2*N);
    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        cini(m);
        g[a+b-m].emplace_back(a,b,i);
    }

    vector<pii> ans(n);
    int cnt=0;
    for(int abm=0; abm<2*N; abm++) {
        if(g[abm].size()>0) {
            set<t3> aa;
            set<t3> bb;
            for(size_t j=0; j<g[abm].size(); j++) {
                auto [ga, gb, gi]=g[abm][j];
                aa.emplace(ga,gb,gi);
                bb.emplace(-gb,ga,gi);
            }

            while(aa.size()) {
                cnt++;
                auto it=aa.begin();
                auto [ga, gb, gi]=*it;
                int mm=ga+gb-abm;
                assert(mm>=0);
                int useA=min(mm, ga);
                ans[gi]={useA, mm-useA};
                bb.erase({-gb,ga,gi});
                aa.erase(it);

                while(bb.size()) {
                    auto it1=bb.begin();
                    auto [bgb, bga, bgi]=*it1;
                    mm=bga-bgb-abm;
                    if(-bgb>=mm-ga) {
                        ans[bgi]={useA,mm-useA};
                        bb.erase(it1);
                        aa.erase({bga,-bgb,bgi});
                    } else 
                        break;
                }
            }
        }
    }
    cout<<cnt<<endl;
    for(int i=0; i<n; i++) 
        cout<<ans[i].first<<" "<<ans[i].second<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
