/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e18;
bool checkP(vi &r, vi &b) {
    set<int> s;
    for(int i : r)
        s.insert(i);
    for(int i : b)
        s.insert(i);

    if(s.size()!=r.size()+b.size())
        return false;

    int val=INF;
    for(auto it=s.begin(); it!=s.end(); it++) {
        if(val==INF)
            val=*it-1;
        if(val+1!=*it)
            return false;
        val=*it;
    }
    return true;
}

/**
 * red  +1
 * blue -1
 *
 * After shifting the smallest r must fit in first gap in b,
 * or smallest b must fit in first gap in r.
 *
 * So, find MEX in both arrays, try both shifts.
 *
 * We must shift r[] so that r[0]>=1
 *
 * ...oh no, its another problem.
 * We can increase/decrease _one_ element after the other!
 *
 * So, the positive numbers in r[] cannot be used in position left of the value.
 * The numbers in b[] cannot be used in position right of the value.
 *
 * So lets fill the permutation greediely from left:
 * if blue is avail, use it, else use smallest red.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cins(s);

    vi r;
    vi b;
    for(int i=0;i<n; i++)  {
        if(s[i]=='R')
            r.push_back(a[i]);
        else
            b.push_back(a[i]);
    }
    sort(all(r));
    sort(all(b));

    int ridx=0;
    int bidx=0;

    for(int i=1; i<=n; i++) {
        bool ok=false;
        if(bidx<b.size() && b[bidx]>=i) {
            ok=true;
            bidx++;
        } else if(ridx<r.size() && r[ridx]<=i) {
            ok=true;
            ridx++;
        }
        if(!ok) {
            cout<<"NO"<<endl;
            return;
        }
    }
    cout<<"YES"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
