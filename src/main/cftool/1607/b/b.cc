/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * i=0 pos=0
 * i=1 pos=-1
 * i=2 pos=1
 * i=3 pos=4
 * i=4 pos=0
 * i=5 pos=-5
 * i=6 pos=1
 * i=7 pos=8
 * i=8 pos=0
 * i=9 pos=-9
 * i=10 pos=1
 * i=11 pos=12
 * i=12 pos=0
 * i=13 pos=-13
 * i=14 pos=1
 * i=15 pos=16
 * i=16 pos=0
 * ...
 */
void solve() {
    cini(x0);
    cini(n);

    int nn=n-(n%4);

    for(int i=nn+1; i<=n; i++) {
        if(x0%2==0)
            x0-=i;
        else
            x0+=i;

        //cerr<<"i="<<i<<" pos="<<pos<<endl;
    }

    cout<<x0<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
