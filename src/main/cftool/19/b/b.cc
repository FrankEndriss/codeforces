/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * We have to pay only the first some
 * items with sum(t)<=n, then all other
 * are free.
 * So we basically search the cheapest (min(sum(c[])))
 * set of items with sum(tt[])>=n-sizeof(tt[])
 *
 * Obs: 
 * Items with t=0 should not be in tt[]
 * At most n/2 item must be in tt[] since min t==1.
 *
 * Knapsack?
 * dp[i]=min sum(c) we can get while occupy i seconds (note that each items ocupies t[i]+1 seconds)
 */
const int N=2001;
const int INF=1e18;
void solve() {
    cini(n);
    int sumc=0;
    vi c(n);
    vi t(n);
    for(int i=0; i<n; i++) {    
        cin>>t[i]>>c[i];
        sumc+=c[i];
    }

    vi dp(2*N+1,INF);
    dp[0]=0;
    const int nn=dp.size();

    for(int i=0; i<n; i++)
        for(int j=nn-1-(t[i]+1); j>=0; j--) 
            dp[j+t[i]+1]=min(dp[j+t[i]+1], dp[j]+c[i]);

    int ans=1e18;
    for(int i=n; i<nn; i++)
        ans=min(ans, dp[i]);

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
