/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Smallest possible BA is
 * when inserting 0*'b'
 * then 1*'b'
 * ...k*b
 * and so on.
 *
 * So its k based number with num('*') digits
 *
 * But what about consecutive '*' ?
 * a**a
 * aa
 * aba
 * abba
 * abbba
 * -> Consecutive starts count as one star.
 *  But we can insert cnt*k times 'b'
 */
void solve() {
    cini(n);
    cini(k);
    cini(x);
    x--;
    cins(s);

    reverse(all(s));

    string sans;
    int stcnt=0;
    for(char c : s) {
        if(c=='*') {
            stcnt++;
            continue;
        }

        int cnt=x%(stcnt*k+1);
        x/=(stcnt*k+1);
        for(int i=0; i<cnt; i++)
            sans+='b';

        sans+='a';
        stcnt=0;
    }
    int cnt=x%(stcnt*k+1);
    x/=(stcnt*k+1);
    for(int i=0; i<cnt; i++)
        sans+='b';

    reverse(all(sans));
    cout<<sans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
