/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each position of appended nums x have a postfix of operations.
 * So go from right to left, and maintain the table of operations.
 */
const int N=5e5+7;
void solve() {
    cini(q);

    using t3=tuple<int,int,int>;
    vector<t3> op(q);

    for(int i=0; i<q; i++) {
        cini(t);
        cini(x);
        int y=0;
        if(t==2)
            cin>>y;

        //cerr<<" in="<<t<<" "<<x<<" "<<y<<endl;
        op[i]={t,x,y};
    }

    vi ans;
    vi tr(N);
    iota(all(tr), 0LL);
    for(int i=q-1; i>=0; i--) {
        auto [t,x,y]=op[i];
        if(t==1) {
            ans.push_back(tr[x]);
        } else {
            tr[x]=tr[y];
        }
    } 

    while(ans.size()) {
        cout<<ans.back()<<" ";
        ans.pop_back();
    }
    cout<<endl;

}

signed main() {
    solve();
}
