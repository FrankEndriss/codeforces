/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(w);
    cini(h);

    vi x0;
    vi xH;

    cini(k);
    for(int i=0;i<k; i++) {
        cini(x);    // x<=w
        x0.push_back(x);
    }
    cin>>k;
    for(int i=0;i<k; i++) {
        cini(x);
        xH.push_back(x);
    }

    sort(all(x0));
    sort(all(xH));
    int ans=0;
    ans=max(ans, h*abs(x0[0]-x0.back()));
    ans=max(ans, h*abs(xH[0]-xH.back()));

    cin>>k;

    vi y0;
    vi yW;
    for(int i=0;i<k; i++) {
        cini(y);    // 0 or h
        y0.push_back(y);
    }
    cin>>k;
    for(int i=0;i<k; i++) {
        cini(y);
        yW.push_back(y);
    }
    sort(all(y0));
    sort(all(yW));
    if(y0.size())
        ans=max(ans, w*abs(y0[0]-y0.back()));
    if(yW.size()) 
        ans=max(ans, w*abs(yW[0]-yW.back()));

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
