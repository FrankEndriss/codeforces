/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * For one flavor is simple, 
 * choose the biggest coins possible, so the number of coins is minimized.
 * But we need to fit all a[i].
 * So at what a[i] that simple strategy fails?
 * Consider a={ 2,3}
 * We need two coins, of value 1 and one of value 2.
 *
 * ...
 * For all a[i]%3==0 we use 3-coins
 * if( there is no a[i]%1 ans no a[i]%2 we are done.
 * Else we need one 1-coin, or one 2-coin, or two 1-coin.
 * ...???
 * check freq %6
 *
 * 1 0 0 0 0 0 -> ma/3
 * 1 0 0 1 0 0 -> ma/3
 * 0 0 0 1 0 0 -> ma/3
 *
 * 1 1 0 0 0 0 -> ma/3 + c1 || ma/3-1 + 2*c2
 * 1 1 0 1 0 0 -> ma/3 + c1 || ma/3-1 + 2*c2
 * 0 1 0 1 0 0 -> ma/3 + c1 || ma/3-1 + 2*c2
 *
 * 1 0 1 0 0 0 -> ma/3 + c2 || ma/3   + 2*c1
 * 1 0 1 1 0 0 -> ma/3 + c2 || ma/3   + 2*c1
 * 0 0 1 1 0 0 -> ma/3 + c2 || ma/3   + 2*c1
 *
 * ***************
 * ??? idk :/
 *
 * *** 
 * Can we somehow brute force?
 * Say the last 5 coins?
 * ***
 * What about
 * 2 4 6 8
 * We need to use
 * 2 2 3 3
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);
    set<int> aa;
    for(int i : a)
        if(i>0)
            aa.insert(i);

    vi b(3);
    int ma=0;
    int mi=INF;
    for(int i : aa) {
        b[i%3]++;
        ma=max(ma, i);
        mi=min(mi, i);
    }

    int ans=ma/3;   /* 3-coins */
    int c3=ans;
    if(b[1])
        ans++;  /* need a 1-coin */
    if(b[2])
        ans++;  /* need a 2-coin or another 1 coin */

    /* But, if we got a 1 and a 2 and ma is div by 3, then we need one less 3-coin */
    if(ma%3==0 && b[1] && b[2] && ma>=3)
        ans--;
    /* Ex 2, we can also save the 1-coin and a 3-coin if we can subst the 1 with a 2 */
    else if(mi>1 && b[1] && b[2] && aa.count(c3*3)==0)
        ans--;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
