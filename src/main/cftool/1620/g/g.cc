/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider the subseqs starting with some a.
 * Sort the strings by f['a']...
 *
 * Think of the i substrings as freq[i][26] arrays.
 * Iterate the 26 letters
 * dp0[i]['a']=freq[i]['a']
 * dp['a']=max(dp0[i]['a'])
 * dp0[i]['b']=freq[i]['a']*freq[i]['b']
 *
 * if a string j has less 'a' but more 'b' than string i,
 * then j contributes freq[j]['a']*(freq[j]['b']-freq[i]['b'])
 * ...
 * How to handle the exclusion correctly?
 */
void solve() {
}

signed main() {
    solve();
}
