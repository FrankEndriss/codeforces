/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we can create the numbers
 * by the sums of 
 * 1,x,x*x,...,x^n
 * 1,y,y*y,...,y^m
 * x^n<r
 * y^m<r
 * Simply brute force all of them, sort and
 * find biggest gab in the list.
 */
void solve() {
    cini(x);
    cini(y);
    cini(l);
    cini(r);

    vi x0={ 1 };
    int x1=x;
    while(x1<=r) {  /* overflow ? */
        x0.push_back(x1);
        double dx=x1;
        dx*=x;
        if(dx>r)
            break;
        x1*=x;
    }
    vi y0={ 1 };
    int y1=y;
    while(y1<=r) {
        y0.push_back(y1);
        double dy=y1;
        dy*=y;
        if(dy>r)
            break;
        y1*=y;
    }

    set<int> a;
    for(int i=0; i<x0.size(); i++) {
        for(int j=0; j<y0.size(); j++) {    
            int val=x0[i]+y0[j];
            if(val>=l && val<=r)
                a.insert(val);
        }
    }
    a.insert(l-1);
    a.insert(r+1);

    int prev=-2;
    int ma=0;
    for(int v : a) {
        if(prev>-2) {
            ma=max(ma, v-prev-1);
        }
        prev=v;
    }
    cout<<ma<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS, SOLVE ONE AFTER THE OTHER
