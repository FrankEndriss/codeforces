/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that the product of two sharks is then a multiple of
 * p if at least one of the sharks values is a multiple
 * of p.
 * So, each shark has a number of multiples of p in its
 * segment l,r; lets call them lucky-numers, and the prop that
 * such one is chooses by shark i p[i]
 * Then, an edge between two sharks is rewarded if at least one
 * of the sharks chooses one of its lucky numbers.
 * So, an edge is rewarded with prob
 * pe[i]=p[i]+(1-p[i])*p[i+1]
 */
void solve() {
    cini(n);
    cini(pr);

    vd p(n);
    for(int i=0; i<n; i++) {
        cini(l);
        cini(r);
        int cnt=r/pr-(l-1)/pr;
        p[i]=cnt;
        p[i]/=(r-l+1);
        //cerr<<"i="<<i<<" p[i]="<<p[i]<<endl;
    }

    ld ans=0;
    for(int i=0; i<n; i++)
        ans+=(p[i]+(1-p[i])*p[(i+1)%n])*2000;

    cout<<ans<<endl;
}

signed main() {
        solve();
}
