/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* we cannot iterate all pairs.
 * the lcm contains all primefactors...
 *
 * Editorial:
 * let gcdi(i)=gcd(a[0], ...,a[i-1], a[i+1], ...,a[n-1]);
 * ans=lcm(gcdi(0), gcdi(1),...,gcdi(n-1))
 */
void solve() {
    cini(n);    
    cinai(a,n);

    vi preg(n);
    preg[0]=a[0];
    vi sufg(n);
    sufg[n-1]=a[n-1];
    for(int i=1; i+1<n; i++) {
        preg[i]=gcd(preg[i-1], a[i]);
        sufg[n-1-i]=gcd(sufg[n-i], a[n-1-i]);
    }

    function<int(int)> gcdi=[&](int i) {
        if(i==0)
            return sufg[1];
        else if(i==n-1)
            return preg[i-1];
        else
            return gcd(preg[i-1], sufg[i+1]);
    };
    
    int ans=1;
    for(int i=0; i<n; i++)
        ans=lcm(ans, gcdi(i));
    
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
