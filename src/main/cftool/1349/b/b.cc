/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Once we have to same adj numbers we can make the whole
 * array same that number.
 * So question is can we make two ajd elemets eq k.
 * This is true if k is median in any subarray.
 *
 * To have k as median in a subarray we need to create a 
 * subarray includeing k and some other numbers bigger or eq
 * k.
 * Check all such subarrays, until one is found with (n+1)/2
 * memebers bgeq k.
 * How to implement?
 *
 * Find/Store positions of the a[i]=K elements and the a[j]>=K
 * elements.
 * The j-positions are possible start/end positions, there must
 * be at least one i-position, and the size of interval must
 * not be bigger that twice the numbers of j-elements.
 *
 * ..To have any subarray with at least half of the positions
 * being j positions, we must have somewhere 2 or 3 positions
 * with at least 2 j positions.
 * Just search that ones.
 *
 * We need to have a double/triple with a[i]>=k and at least
 * on a[i]==k in the array.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    if(n==1 && a[0]==k) {
        cout<<"yes"<<endl;
        return;
    }

    bool hasK=false;
    for(int i=0; i<n; i++) {
        if(a[i]==k)
            hasK=true;
    }

    if(!hasK) {
        cout<<"no"<<endl;
        return;
    }

    for(int i=1; i<n; i++) {
        if(i>=1) {
            vi aux(2);
            aux[0]=a[i-1];
            aux[1]=a[i];
            sort(all(aux));
            if(aux[0]>=k) {
                cout<<"yes"<<endl;
                return;
            }
                
        } 
        
        if(i>=2) {
            vi aux(3);
            aux[0]=a[i-2];
            aux[1]=a[i-1];
            aux[2]=a[i];
            sort(all(aux));
            if(aux[1]>=k) {
                cout<<"yes"<<endl;
                return;
            }
        }
    }

    cout<<"no"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
