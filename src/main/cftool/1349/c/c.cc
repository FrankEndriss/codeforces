/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * After short time all are white or all are black,
 * just simulate.
 * And sort queries.
 *
 * Note: simulaton is to slow :/
 **/
void solve() {
    cini(n);
    cini(m);
    cini(t);

    vvb a(n, vb(m));  /* map as given in input */
    for(int i=0; i<n; i++) {
        cins(s);
        for(size_t j=0; j<m; j++) 
            if(s[j]=='1')
                a[i][j]=true;
    }

    vvb b(n, vb(m));  /* single, blinking cells */
    const vi dx={ -1, 1, 0, 0};
    const vi dy={  0, 0,-1, 1};

    vvi q(t, vi(4));
    for(int i=0; i<t; i++) {
        cin>>q[i][1]>>q[i][2]>>q[i][0];
        q[i][3]=i;
    }

    sort(all(q));
    bool fini=false;
    int j=0;
    for(int i=0; i<t; i++) {
        while(!fini && j<q[i][0]) {
            vvb aa(n, vb(m));
            bool nochg=true;
            for(int i=0; i<n; i++) {
                for(int j=0; j<m; j++) {
                    int cnt=0;
                    for(int k=0; cnt==0 && k<4; k++) {
                        const int ii=i+dx[k];
                        const int jj=j+dy[k];
                        if(ii>=0 && ii<n && jj>=0 && jj<m && a[i][j]==a[ii][jj])
                            cnt++;
                    }
                    if(cnt==0)
                        aa[i][j]=a[i][j];
                    else {
                        nochg=false;
                        aa[i][j]=!a[i][j];
                    }
                }
            }
            a.swap(aa);
            if(nochg)
                fini=true;
            j++;
        }
        q[i][0]=a[q[i][1]-1][q[i][2]-1];
        swap(q[i][0], q[i][3]);
    }
    sort(all(q));
    for(int i=0; i<t; i++)
        cout<<q[i][3]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
