/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* Starting from any person i the call goes into a loop after af[i] steps 
 * and loop of size al[i].
 * For every person we can construct a list of persons which are reachable
 * in the graph. Let y be af[i] and x be al[i] and p position of a person
 * in another persons list, then persons in this 
 * list can be reached after p steps, and if p>y
 * after p+k*x steps.
 * But, if y>0 that means that that i is never reached from any other i,
 * so there is no solution.
 * Else solution is LCM of the cycle lengths.
 * Note that for even length cycles it is length/2.
 */
void solve() {
    cini(n);
    vi a(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        a[i]=aux-1;
    }

    int ans=1;
    for(int i=0; i<n; i++) {
        vb vis(n);
        vis[i]=true;
        int ii=a[i];
        int cnt=1;
        while(!vis[ii]) {
            vis[ii]=true;
            cnt++;
            ii=a[ii];
        }
        if(ii!=i) {
            cout<<-1<<endl;
            return;
        }
        if(cnt%2==0)
            cnt/=2;
        ans=lcm(ans, cnt);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
