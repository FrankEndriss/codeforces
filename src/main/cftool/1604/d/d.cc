/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider x==y, then ans=x
 * if x>y then choose n=x+y
 * if x<y then value<x, obviously.
 * n%x == y%n
 *
 *
 * Why x and y are even???
 */
void solve() {
    cini(x);
    cini(y);

    if(x==y)
        cout<<x<<endl;
    else if(x>y) 
        cout<<x+y<<endl;
    else {
        int d=y-x;
        int d2=d/2;
        for(int i=x+d2-10;i<x+d2+13;i++) {
            if(i%x == y%i) {
                cout<<i<<endl;
                return;
            }
        }
    }
    assert(false);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
