/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** Erase from right to left.
 */
void solve() {
    cini(n);
    cinai(a,n);

    while(a.size()) {
        bool done=false;
        for(int i=a.size()-1; !done && i>=0; i--) {
            if(a[i]%(i+2)!=0) {
                for(int j=i; j+1<a.size(); j++) 
                    a[j]=a[j+1];
                a.pop_back();
                done=true;
            }
        }
        if(!done)
            break;
    }

    if(a.size()==0)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
