/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find foreach hight
 * how much boulders it takes to reach the next hight.
 *
 * We fill the sinks from local max to local max.
 * So, foreach local max, find how much boulders it takes
 * to fill until the next local max.
 * Then, when a local max is filled on both sides, it disapears.
 * How to implement?
 * How to utilize n<=100 ???
 * 
 * Simulate.
 * Foreach boulder find how wide the plateau is where the
 * boulder stops.
 * If it is more than available k, fill by one level.
 * Find how much levels the current plateau must be filled,
 * and if that much k is available.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);
    vi h(n+1);
    for(int i=0; i<n; i++) 
        cin>>h[i+1];

    //cerr<<"n="<<n<<" k="<<k<<endl;
    h[0]=INF;

    while(k>0) {
        int with=1;
        int idx=-1;
        for(int i=1; i+1<=n; i++) {
            if(h[i]<h[i+1]) {   /* found end of current plateau */
                idx=i+1;
                break;
            } else if(h[i]==h[i+1]) {
                with++;
            } else if(h[i]>h[i+1]) {
                with=1;
            }
        }
        if(idx<=0)
            break;

        int ma=min(h[idx], h[idx-with-1]);
        int mi=h[idx-1];
        assert(ma>mi);

        //cerr<<"with="<<with<<" idx="<<idx<<" ma="<<ma<<" mi="<<mi<<" cnt="<<(ma-mi)*with<<" k="<<k<<endl;

        if(k>(ma-mi)*with) {
            k-=(ma-mi)*with;
            for(int j=idx-with; j<idx; j++)
                h[j]=ma;

        } else {
            k%=with;
            if(k==0)
                k=with;
            //cerr<<"k%with="<<k<<endl;
            cout<<idx-k<<endl;
            return;
        }
    }
    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
