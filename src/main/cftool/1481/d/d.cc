/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to start somewhere, then find the same
 * path two times.
 * So, to have an palindrome of even length, we need to 
 * have a vertex with an incoming path of length n/2 same
 * as an outgoing path of length n/2
 * For odd length the outgoing path must have the middle char as first edge.
 *
 * How to find such path?
 * Can we do dfs from all nodes on graph and reverse graph simultanious...somehow?
 * n<=1000 so O(n^2 logn) is ok.
 * But that does not work with m<=1e5
 *
 * ***
 * But we can find a shortest path from all vertex to all vertex.
 * And then?
 * We need to utilize loops somehow
 */
void solve() {
    cini(n);
    cini(m);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
