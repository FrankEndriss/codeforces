/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* foreach color we can use two strategies:
 * -move all of them to the right end, or
 * -move all other books between the first and last to the right end
 *
 * So find foreach color the first and last idx, and the colors in between.
 * Then how to find best group of moved colors?
 *
 * Consider graph with an edge meaning "if notmoved c1 then we must move c2" from c1 to c2
 *
 * Also consider the rightmost color, there is no need to move that elements at all.
 * ... TODO
 */
void solve() {
    cini(n);

    vi l(n+1, -1);
    vi r(n+1, -1);
    vi cnt(n+1);
    for(int i=0; i<n; i++) {
        cini(aux);
        cnt[aux]++;
        if(l[aux]<0)
            l[aux]=i;
        else if(r[aux]<0)
            r[aux]=i;
    }
}

signed main() {
    solve();
}
