/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* foreach color planks in b not in a we need a painter for that plank.
 * if we got enough painter, we need to find if the last painter can
 * paint a plank.
 *
 * Then we go from last to first painter. If we need all of that colors
 * painter, than he paints the last plank of that color.
 * Else he paints the first plank of the color of the first painter.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    cinai(b,n);
    cinai(c,m);

    vvi need(n+1);  /* indexes of planks we need to paint, ordered by color */
    for(int i=0; i<n; i++)  {
        if(a[i]!=b[i]) {
            need[b[i]].push_back(i);
        }
    }

    int last=-1;    /* index of plank the last painter paints */
    if(need[c.back()].size()>0)
        last=need[c.back()].back();
    else {
        for(int i=n-1; i>=0; i--)
            if(last<0 && b[i]==c.back())
                last=i;
    }

    if(last<0) {
        cout<<"NO"<<endl;
        return;
    }

    vi ans;
    for(int i=m-1; i>=0; i--) {
        if(need[c[i]].size()>0)  {
            ans.push_back(need[c[i]].back());
            need[c[i]].pop_back();
        } else {
            ans.push_back(last);
        }
    }
    for(int i=1; i<=n; i++) {
        if(need[i].size()>0) {
            cout<<"NO"<<endl;
            return;
        }
    }

    cout<<"YES"<<endl;
    for(int i=m-1; i>=0; i--)
        cout<<ans[i]+1<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
