/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* count wher we are and if we can remove so much chars.
 */
void solve() {
    cini(px);
    cini(py);
    cins(s);
    vi cnt(256);
    for(char c : s)
        cnt[c]++;

    bool xOk=false;
    if(px<=0 && cnt['L']>=abs(px))
        xOk=true;
    if(px>=0 && cnt['R']>=px)
        xOk=true;

    bool yOk=false;
    if(py<=0 && cnt['D']>=abs(py))
        yOk=true;
    if(py>=0 && cnt['U']>=py)
        yOk=true;

    if(xOk && yOk)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
