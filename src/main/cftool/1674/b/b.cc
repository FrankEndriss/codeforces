/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

vs dp;
void init() {
    for(char c='a'; c<='z'; c++)
        for(char d='a'; d<='z'; d++) {
            if(d==c)
                continue;
            string w;
            w+=c;
            w+=d;
            dp.push_back(w);
        }
}
/**
 */
void solve() {
    cins(s);
    auto it=lower_bound(all(dp),s);
    cout<<distance(dp.begin(), it)+1<<endl;
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
