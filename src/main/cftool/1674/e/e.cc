/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can break one segment by x[i]/2 shots,
 * and two adj segments by
 * -let d= abs(a[i]-a[i+1])
 *  let x= max(a[i],a[i+1]-d
 *  d/2 + x/3 + maybe 1
 *  ...no, all wrong: Left-1 Middle-2 Right-1
 *
 *  But it is allways optimal to hit two independent, 
 *  or three adj.
 *
 *  ...need to recall all cases.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi b=a;
    sort(all(b));
    int ans=(b[0]+1)/2+(b[1]+1)/2;

    for(int i=0; i+1<n; i++) {
        if(i+2<n) {
            ans=min(ans, max(a[i],a[i+2]));
            ans=min(ans, max(a[i],(a[i+1]+1)/2));
            ans=min(ans, max(a[i+2],(a[i+1]+1)/2));

            int mi=min(a[i], a[i+2]);
            int ma=max(a[i], a[i+2]);
            ans=min(ans, mi+(ma-mi+1)/2);
        }
            int mm=max(a[i],a[i+1]);
            int mi=min(a[i],a[i+1]);
            int a1=min(mi, (mm+1)/2);  /* make one of both zero */
            int a2=(max(mi-a1, mm-2*a1)+1)/2; /* ...and the other one */
            ans=min(ans, a1+a2);

        int a0=a[i];
        a1=a[i+1];
        if(a0>a1)
            swap(a0,a1);

        ans=min(ans, a0+max(0LL, (a1-(2*a0)+1)/2));

        if(a0*2<=a1) {
            ans=min(ans, a1/2);
        } else {
            int d=a1-a0;
            int lans=d;
            int d2=(a0-d)*2/3;
            if(((a0-d)*2)%3!=0)
                d2++;
            lans+=d2;
            ans=min(ans, lans);
        }
    }
    cout<<ans<<endl;


}

signed main() {
    solve();
}
