/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Fiddling :/
 *
 * Cases:
 * t=="a"   -> 1
 * t=="aa"  -> inf
 * t=="abc" -> inf
 * t=="bcd" -> s.size()+1
 *
 * WA :/ 
 * Why?
 */
void solve() {
    cins(s);
    cins(t);

    int acnt=0;
    set<char> f;
    for(char c : t) {
        f.insert(c);
        if(c=='a')
            acnt++;
    }

    if(t=="a") {
        cout<<1<<endl;  /* no subst changes anything */
    } else if(acnt>0) {
        cout<<-1<<endl; /* subst all recursive */
    } else {
        cout<<(1LL<<s.size())<<endl;
        //cout<<s.size()+1<<endl; /* subst none to all */
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
