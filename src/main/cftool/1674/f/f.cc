/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Maintain the number of icons, and the positions of icon in 1D array,
 * segment tree.
 *
 * How is the ordering of icons on the desktop?
 * It seems not to be full row.
 */
void solve() {
    cini(n);
    cini(m);
    cini(q);

    int cnt=0;
    vi a(n*m);
    for(int i=0; i<n; i++) {
        cins(s);
        for(int j=0; j<m; j++) {
            if(s[j]=='*') {
                a[j*n+i]=1;
                cnt++;
            }
        }
    }

    stree seg(a);

    for(int i=0; i<q; i++) {
        cini(x); x--;
        cini(y); y--;
        int idx=y*n+x;

        int val=seg.get(idx);
        if(val)
            cnt--;
        else
            cnt++;
        
        seg.set(idx,1-val);
        int ans=cnt-seg.prod(0,cnt);
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}
