/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vvd= vector<vd>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp[k][w][b]=prob that w,b happens while its ks turn.
 * But k is redundant, since it depends on w+b.
 *
 * Use bfs.
 *
 * ans is sum of probs while its princess turn to get a white mouse.
 */
void solve() {
    cini(w);
    cini(b);

    vvd dp(w+1, vd(b+1));
    vvb vis(w+1, vb(b+1));

    dp[w][b]=1;
    vis[w][b]=true;
    using t3=tuple<int,int,int>;
    queue<t3> q;
    q.emplace(0,w,b);  /* first turn princess */
    ld ans=0;
    while(q.size()) {
        auto [p,ww,bb]=q.front();
        q.pop();
        if(ww==0)
            continue;

        ld pww=1;
        pww=(pww*ww)/(ww+bb); /* prop to get a white mouse */
        ld pbb=1-pww;  /* prop to get a black mouse */


        if(p==0) {  /* princess turn */
            ans+=dp[ww][bb]*pww;

            if(bb>0) {
                dp[ww][bb-1]+=dp[ww][bb]*pbb;
                if(!vis[ww][bb-1]) {
                    vis[ww][bb-1]=true;
                    q.emplace(1,ww,bb-1);
                }
            }
        } else {    /* dragons turn */
            /*
            if(ww>0) {
                if(ww-1>0) {
                    dp[ww-2][bb]+=dp[ww][bb]*pww*pww;
                    if(!vis[ww-2][bb]) {
                        vis[ww-2][bb]=true;
                        q.emplace(0, ww-2, bb);
                    }
                }
                if(bb>0)  {
                    dp[ww-1][bb-1]+=dp[ww][bb]*pww*pbb;
                    if(!vis[ww-1][bb-1]) {
                        vis[ww-1][bb-1]=true;
                        q.emplace(0,ww-1,bb-1);
                    }
                }
            }
            */
            if(bb>0 && ww>0) {
                ld pww2=1;
                pww2=pww2*(ww)/(ww+bb-1);
                if(ww>0)  {
                    dp[ww-1][bb-1]+=dp[ww][bb]*pbb*pww2;
                    if(!vis[ww-1][bb-1]) {
                        vis[ww-1][bb-1]=true;
                        q.emplace(0,ww-1,bb-1);
                    }
                }
                ld pbb2=1-pww2;
                if(bb-1>0) {
                    dp[ww][bb-2]+=dp[ww][bb]*pbb*pbb2;
                    if(!vis[ww][bb-2]) {
                        vis[ww][bb-2]=true;
                        q.emplace(0, ww, bb-2);
                    }
                }
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
        solve();
}
