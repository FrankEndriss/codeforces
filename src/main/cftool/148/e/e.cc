/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * we need to find max prefix/suffix arrays, or
 * minimum subarrays of certain length.
 *
 * We should propably use the smaller number.
 *
 * *************
 * finding min subarrays:
 * from each line we can choose one subarray, sum of lengths is fixed.
 * for each length in each line we have the contribution value.
 * now dp the max value.
 *
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    vvi a(n);
    int cnt=0;
    int sumall=0;

    for(int i=0; i<n; i++) {
        cini(aux);
        cnt+=aux;
        for(int j=0; j<aux; j++) {
            cini(v);
            sumall+=v;
            a[i].push_back(v);
        }
    }

/* dp[i][j]=min possible sum if i+1 lines used and j elements choosen. */
    vvi dp(n, vi(cnt-m+1, INF));

/* sub[i][j] min sum subarray in set i of len j */
    vvi sub(n);
    for(int k=0; k<n; k++) {
        sub[k].resize(a[k].size()+1);
        for(int i=1; i<=a[k].size(); i++) {
            int sum=0;
            int mi=1e9;
            for(int j=0; j<a[k].size(); j++) {
                sum+=a[k][j];

                if(j+1>=i) {
                    mi=min(mi, sum);
assert(j-(i-1)>=0);
                    sum-=a[k][j-(i-1)];
                }
            }
            sub[k][i]=mi;
        }
    }

    for(int i=0; i<=min(cnt-m, (int)a[0].size()); i++)
        dp[0][i]=sub[0][i];

    for(int i=1; i<n; i++) {
        for(size_t j=0; j<=a[i].size(); j++) {
            for(int k=0; k+j<=cnt-m; k++) {
                dp[i][k+j]=min(dp[i][k+j], dp[i-1][k]+sub[i][j]);
            }
        }
    }

    int ans=sumall-dp[n-1][cnt-m];
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
