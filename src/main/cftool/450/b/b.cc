/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* f1=x; f2=y=x+f3; f3=y-x=y+f4; f4=y-x-x=x-y+y-2x=-x; f5=-x + f6; 
 * f1=x; f2=y=f1+f3; f3=y-x=y+f4; f4=-x=y-x+f5; f5=-y=-x+f6; f6=x-y=-y+f7; f7=x=x-y+f8;
 */
void solve() {
    cini(x);
    cini(y);
    cini(n);

    n=(n-1)%6+1;
    int ans;
    if(n==1)
        ans=x;
    else if(n==2) 
        ans=y;
    else if(n==3)
        ans=y-x;
    else if(n==4)
        ans=-x;
    else if(n==5)
        ans=-y;
    else if(n==6)
        ans=x-y;

    const int mod=1e9+7;
    while(ans<0)
        ans+=mod;

    cout<<ans%mod<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

