/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we can dp the number of geom of size 2
 * for every position. (right to left O(nlogn)
 * Then once more do the same with size 3
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vi dp2(n+1);
    map<int,int> f;
    for(int i=n-1; i>=0; i--) {
        dp2[i]=f[a[i]*k];
        f[a[i]]++;
    }

    int ans=0;
    f.clear();
    for(int i=n-1; i>=0; i--) {
        ans+=f[a[i]*k];
        f[a[i]]+=dp2[i];
    }

    cout<<ans<<endl;
}

/* tutorial, fix middle element
 */
void solve2() {
    cini(n);
    cini(k);
    map<int,int> fR;
    vi a(n);
    for(int i=0; i<n; i++) {
        cin>>a[i];
        fR[a[i]]++;
    }


    int ans=0;
    map<int,int> fL;
    for(int i=0; i<n; i++) {
        fR[a[i]]--;
        if(a[i]%k==0)
            ans+=fL[a[i]/k]*fR[a[i]*k];

        fL[a[i]]++;
    }

    cout<<ans<<endl;
}

signed main() {
    solve2();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
