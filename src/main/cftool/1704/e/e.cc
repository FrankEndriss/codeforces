/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Since there are no cycles, and only one vertex without outgoing edges,
 * everything goes to that vertex. So that one is the last with a value.
 * Each vertex j sends a[j] units to it, so after sum(a[]) all are finished.
 * Really, so simple?
 *
 * ...No, not exactly so simple. Since some vertex have k>1 outputs,
 * their values is multiplied by k.
 * So we need to do dfs on reverse graph, with memoization.
 * ...
 * And here comes the difficulty:
 * The root vertex can be a[x]=0 for some steps, and we need to
 * add that number of steps in the end.
 * Note that longest path is n==1000, so there are at most
 * n-1 such steps.
 * Do we need to simulate m steps initially to be sure?
 * -> I guess so.
 *
 * ...but unfortunatly soccer EM final starts right now :)
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    vvi adj(n);
    vi cnt(n);
    for(int i=0; i<m; i++) {
        cini(x); x--;
        cini(y); y--;
        adj[y].push_back(x);    /* reverse */
        cnt[x]++;
    }

    int root=-1;
    for(int i=0; i<n; i++) 
        if(cnt[i]==0) {
            assert(root<0);
            root=i;
        }

    vector<mint> memo(n);
    vi depth(n);
    vb vis(n);
    function<void(int,int)> dfs=[&](int v, int dep) {
        vis[v]=true;
        memo[v]=a[v];
        for(int chl : adj[v]) {
            if(!vis[chl])
                dfs(chl,dep+1);
            memo[v]+=memo[chl];
        }
    };

    dfs(root);

    /*
    mint ans=0;
    for(int i=0; i<n; i++) 
        ans+=memo[i];
        */

    cout<<memo[root].val()<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
