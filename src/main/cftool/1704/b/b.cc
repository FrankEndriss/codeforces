/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each position has a min and a max, so whenever
 * the range degenerates, we have to reset the afinity.
 *
 * But what rude formular...
 * max(v)=x+a[i]
 * min(v)=-x+a[i];
 * ...no, what if a[i] much bigger than x?
 *
 * x>= v-a[i]   |positive v-a[i]
 * -v>=-a[i]-x
 *  v<=a[i]+x
 *
 * v-a[i]>=-x   |negative v-a[i]
 * v>=a[i]-x
 *
 * Why not work?
 */
void solve() {
    cini(n);
    cini(x);
    cinai(a,n);

    //cerr<<"n="<<n<<" x="<<x<<endl;
    int ma=a[0]+x;
    int mi=a[0]-x;
    //cerr<<"ma="<<ma<<" mi="<<mi<<endl;

    int ans=0;
    for(int i=1; i<n; i++) {
        int lma=a[i]+x;
        int lmi=a[i]-x;
        //cerr<<"lma="<<lma<<" lmi="<<lmi<<endl;
        if(lma<mi || lmi>ma) {
            ans++;
            ma=lma;
            mi=lmi;
            //cerr<<"new     , ma="<<ma<<" mi="<<mi<<endl;
        } else {
            ma=min(ma, lma);
            mi=max(mi, lmi);
            //cerr<<"combined, ma="<<ma<<" mi="<<mi<<endl;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
