/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * I am fairly sure I had this one also before.
 * ***
 * somehow it is possible to find the special array, and from that determine
 * all op2, and from that all op1...or the like.
 *
 * Also sum over all array does never change.
 * The invariant is about parity, op1 does not change the sum within the 
 * parity grouped elements, but op2 does.
 * Unfortunatly op2 reverse changes the sums if used on odd/even index :/
 *
 * Note that c[0], c[m-1] and ck[m-2] never decrements.
 *
 * Note that op1 does note change sum(c[i]*(i+1)), but op2 does add 1!
 */
void solve() {
    cini(n);
    cini(m);

    vvi c(n, vi(m));
    vi sum(n);
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            cin>>c[i][j];
            sum[i]+=c[i][j]*(j+1);
        }
    }

    if(sum[0]!=sum[1]) {
        if(sum[0]!=sum[2]) {
            cout<<1<<" "<<sum[0]-sum[1]<<endl;
        } else {
            cout<<2<<" "<<sum[1]-sum[0]<<endl;
        }
    } else {
        for(int ans=2; ans<n; ans++) {
            if(sum[ans]!=sum[0]) {
                cout<<ans+1<<" "<<sum[ans]-sum[0]<<endl;
                return;
            }
        }
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
