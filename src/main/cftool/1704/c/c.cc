/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * I had this prob before, at least similar, also same story.
 * ****
 * However, each gab has a size, and "shrinks" each day by one unit
 * from each end.
 * let g[i] be initial size of ith gap.
 * So on day1 and day2 we can save some g[0]-1, 
 * then g[1]-3 and so on.
 * Since gaps that are fully filled do not shrink any more, we just
 * greedy choose the biggest gap to save.
 *
 * ...seems some edgecases :/
 * third example:
 * 20 3
 * 3 7 12
 * ans=11, how can we save 11?
 * Gaps are
 * 3, 4, 10, so save
 * 9, 1, 0
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,m);

    if(m==1) {
        cout<<2<<endl;
        return;
    }

    for(int i=0; i<m; i++) 
        a[i]--;

    sort(all(a));
    priority_queue<int> q;
    for(int i=0; i+1<m; i++)
        q.push(a[i+1]-a[i]-1);

    q.push(n-1-a.back()+a[0]);

    //cerr<<"n="<<n<<" m="<<m<<endl;
    int ans=0;  /* saved houses */
    int d=0;
    for(int i=0; i<m; i++) {
        int v=q.top();
        //cerr<<"v="<<v<<" v-d="<<v-d<<endl;
        q.pop();
        v-=d*2;
        if(v>0) {
            if(v<2) {
                ans++;
                d++;
            } else {
                ans+=(v-1);
                d+=2;
            }
        }
    }
    cout<<n-ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
