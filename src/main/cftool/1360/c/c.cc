/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* if number of odd/even numbers is even, we can simply make pairs.
 * else we need to make one pair of odd/even by pairing a +-1 pair.
 */
void solve() {
    cini(n);    // even
    cinai(a,n);

    vi cnt(2);
    for(int i=0; i<n; i++)
        cnt[a[i]%2]++;

    if(cnt[0]%2==0) {
        cout<<"YES"<<endl;
        return;
    } else { 
        sort(a.begin(), a.end());
        for(int i=1; i<n; i++) {
            if(a[i]==a[i-1]+1) {
                cout<<"YES"<<endl;
                return;
            }
        }
    }

    cout<<"NO"<<endl;
    return;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
