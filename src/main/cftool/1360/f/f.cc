/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int dsize(string& s1, string& s2) {
    int cnt=0;
    assert(s1.size()==s2.size());
    for(size_t j=0; j<s1.size(); j++) {
        if(s1[j]!=s2[j])
            cnt++;
    }
    return cnt;
}

/* check if solution works */
bool ok(vs& a, string ans) {
    int cnt=0;
    for(size_t i=0; cnt<2 && i<a.size(); i++) {
        cnt=max(cnt, dsize(a[i], ans));
    }
    return cnt<2;
}

/* we check all pairs, if diff>2 no sol avail.
 * else we check the four possibilities.
 */
void solve() {
    cini(n);
    cini(m);
    cinas(a,n);

    for(int i=0; i<n; i++) {
        if(ok(a,a[i])) {
            cout<<a[i]<<endl;
            return;
        }

        for(int j=i+1; j<n; j++) {
            int d=dsize(a[i], a[j]);
            if(d>2) {
                cout<<-1<<endl;
                return;
            } else if(d==2) {
                for(int k=0; k<m; k++) {
                    if(a[i][k]!=a[j][k]) {
                        string sans=a[i];
                        sans[k]=a[j][k];
                        if(ok(a,sans)) {
                            cout<<sans<<endl;
                            return;
                        }

                        sans=a[j];
                        sans[k]=a[i][k];
                        if(ok(a,sans)) {
                            cout<<sans<<endl;
                            return;
                        }
                    }
                }
            }
        }
    }
    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
