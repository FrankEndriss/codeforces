/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* if any col/row does contain a gap at the end, 
 * then the 1 before the gap must have been 
 * shot from the other direction.
 */
void solve() {
    cini(n);
    cinas(s,n);
    vvb a(n, vb(n));
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++)
            a[i][j]=s[i][j]-'0';

    /* find gaps in rows */
    set<pii> hgap;
    for(int i=0; i<n; i++) {
        for(int j=0; j+1<n; j++) {
            if(a[i][j] && !a[i][j+1]) {
                hgap.insert({i,j});
            }
        }
    }

    /* check with gaps in cols */
    for(int j=0; j<n; j++) {
        for(int i=0; i+1<n; i++) {
            if(a[i][j] && !a[i+1][j]) {
                if(hgap.count({i,j})) {
                    cout<<"NO"<<endl;
                    return;
                }
            }
        }
    }
    cout<<"YES"<<endl;
    return;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
