/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The media has n/2 elements bigger that itself, and at least n/2-1 smaller.
 */
void solve() {
    cini(n);    
    cini(m);    // bits

    vi a(n);
    for(int i=0; i<n; i++) {
        cins(s);
        reverse(s.begin(), s.end());
        int k=1;
        for(int j=0; j<m; j++)  {
            if(s[j]=='1')
                a[i]+=k;
            k*=2;
        }
    }

    int N=1LL<<m;
    sort(a.begin(), a.end());
    int med=(1<<m)/2-1;
    int up=N/2-distance(a.end(), upper_bound(a.begin(), a.end(), med));    // numbers bigger than med
    while(up<N/2) {
        med--;
        while(true) {
            auto it=lower_bound(a.begin(), a.end(), med);
            if(it==a.end())
                break;
            else if(*it==med)
                med--;
            else
                break;
        }
        up++;
    }

    while(up>N/2) {
        med++;
        while(true) {
            auto it=lower_bound(a.begin(), a.end(), med);
            if(it==a.end())
                break;
            else if(*it==med)
                med++;
            else 
                break;
        }
        up--;
    }

    stack<int> st;
    for(int i=0; i<m; i++)  {
        st.push(med&1);
        //cout<<(med&1);
        med>>=1;
    }
    while(st.size()) {
        cout<<st.top();
        st.pop();
    }
    cout<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
