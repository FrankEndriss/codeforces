/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* construct a matrix...
 * rows and cols are interchangeable, we can swap any two rows and any two cols.
 * first row, a 1s on first a cols
 * then for every row, we need to shift the pattern by a cols.
 * for b cols, repeat this
 */
void solve() {
    cini(n);
    cini(m);
    cini(a);    
    cini(b);

    if(n*a!=m*b) {
        cout<<"NO"<<endl;
        return;
    }

    vvi ans(n, vi(m));
    for(int i=0; i<n*m; i++) {
        int x=0;
        int r=i/m;
        if((i+(r*a))%m<a)
            x=1;
        ans[i/m][i%m]=x;
    }
    cout<<"YES"<<endl;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            cout<<ans[i][j];
        }
        cout<<endl;
    }
    

    
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
