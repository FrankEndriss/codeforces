/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* first odd raze.
 * second even breach.
 *
 * if n is even last digit is on even position.
 * if n is odd last digit is on odd position.
 *
 * last digit odd: 1
 */
void solve() {
    cini(n);
    cins(s);

    bool oddodd=false;
    for(int i=0; i<n; i+=2)  {
        if((s[i]-'0')%2==1)
            oddodd=true;
    }

    bool eveneven=false;
    for(int i=1; i<n; i+=2)  {
        if((s[i]-'0')%2==0)
            eveneven=true;
    }

    bool outcomeOdd;
    if(n%2==1) {
        if(oddodd)
            outcomeOdd=true;
        else
            outcomeOdd=false;
    } else {
        if(eveneven) {
            outcomeOdd=false;
        } else
            outcomeOdd=true;
    }

    if(outcomeOdd)
        cout<<1<<endl;
    else
        cout<<2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
