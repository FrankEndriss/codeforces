/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* xxx9
 * xxy0
 * xxy1
 * ....
 * if k==1    two numbers
 * xxx7 + xxx8 == odd
 * if n==even?
 * Last digit of first num must be 9, so 
 * last digit of second num is 0, and second last didit increases 
 * by 1. So sum changes by 8.
 * But second last digit must not be 9, so it must be 8
 * So min n what this works for is 
 * 09
 * 10 -> 10
 * 19
 * 20 -> 12
 *
 * for k>=2 we can use prefix sums over all possible numbers
 * 150/3=50
 * 599999
 * Then we need to implement only logic for k==1
 *
 */
void solve() {
    cini(n);
    cini(k);

    if(k==0) {
        int ans=n%9;
        n-=ans;
        
        while(n) {
            ans*=10;
            ans+=9;
            n-=9;
        }
        cout<<ans<<endl;
    }
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
