/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Reversing even length array changes all even positions in the array 
 * with the odd ones in that array.
 * So we want to find the subarray where the sum(odd)-sum(even) is
 * maximized.
 * If max sum<0 the empty array is ans.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int start=0;
    int sum=0;
    int ma=0;
    pii map;
    for(int i=0; i+1<n; i+=2) {
        if(sum==0)
            start=i;
        
        sum+=a[i+1]-a[i];
        if(sum<0)
            sum=0;

        if(sum>ma) {    
            ma=sum;
            map.first=start;
            map.second=i+1;
        }
    }

    start=0;
    sum=0;
    for(int i=1; i+1<n; i+=2) {
        if(sum==0)
            start=i;

        sum+=a[i]-a[i+1];
        if(sum<0)
            sum=0;

        if(sum>ma) {    
            ma=sum;
            map.first=start;
            map.second=i+1;
        }
    }

    int ans=0;
    for(int i=0; i<n; i+=2)
        ans+=a[i];
    ans+=ma;

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
