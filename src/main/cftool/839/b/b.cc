/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * We need to find the min number of rows and 
 * compare to given n.
 *
 * Note that a row is split into 3 parts
 * 1,2; 3,4,5,6; 7,8;
 *
 * We need to place groups greedy.
 * -groups with 8 or more get a complete row.
 * -groups with 4 or more get a 3,6 or 1,2;7,8;
 * -groups with ...
 *  Why, How??? 
 */
void solve() {
    cini(n);
    cini(k);

    vi a(k);
    for(int i=0; i<k; i++) {
        cini(aux);
        n-=aux/8;
        a[i]=aux%8;
    }


    if(cnt<=n*8-1)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
