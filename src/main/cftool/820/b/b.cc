/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* alpha=180-(360/n)
 * 180-2*(360/n)
 * ...
 *
 * We can create triangles starting at 1.
 * Then +n+m
 * 1+1+1 is 1, 2, 3
 * 1+2+1 is 1, 3, 4
 * How to calc the angle at 1?
 * Note that this gives (n-1)*(n-2)/2 triangles, which is to much.
 * ... idk.
 * --
 *  Key observation is, that angle at 2 is independent of where 2 is,
 *  it depends only on 1 and 3. Since "alle winkel auf dem Halbkreis sind gleich".
 *  So we calc the angles "1 2 i" for all i=3..n and choose the best.
 **/
void solve() {
    cini(n);
    cind(a);

    double diff=1e9;
    int ans=-1;
    for(int i=3; i<=n; i++) {
        double ang=(180.0*(n-(i-1)))/n;
        if(abs(a-ang)<diff) {
            diff=abs(a-ang);
            ans=i;
        }
    }
    cout<<"1 2 "<<ans<<endl;
    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

