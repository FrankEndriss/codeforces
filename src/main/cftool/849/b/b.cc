/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

using vpii=vector<pii>;
#define endl "\n"


/* simple fraction implementation based on long long */
struct fract {
    ll f, s;

    fract(ll counter, ll denominator) :
        f(counter), s(denominator) {
        norm();
    }

    bool operator<(const fract &rhs) {
        return f * rhs.s < s * rhs.f;
    }

    fract& operator+=(const fract &rhs) {
        ll g = __gcd(this->s, rhs.s);
        ll lcm = this->s * (rhs.s / g);
        this->f = this->f * (lcm / this->s) + rhs.f * (lcm / rhs.s);
        this->s = lcm;
        norm();
        return *this;
    }

    friend fract operator+(fract lhs, const fract &rhs) {
        lhs += rhs;
        return lhs;
    }

    friend fract operator-(fract lhs, const fract &rhs) {
        lhs += fract(-rhs.f, rhs.s);
        return lhs;
    }

    fract& operator*=(const fract &rhs) {
        this->f *= rhs.f;
        this->s *= rhs.s;
        norm();
        return *this;
    }

    friend fract operator*(fract lhs, const fract &rhs) {
        lhs *= rhs;
        return lhs;
    }

    friend fract operator/(fract lhs, const fract &rhs) {
        return lhs * fract(rhs.s, rhs.f);
    }

    void norm() {
        int sign = ((this->f < 0) + (this->s < 0)) % 2;
        sign = -sign;
        if (sign == 0)
            sign = 1;
        ll g = __gcd(abs(this->f), abs(this->s));
        if (g != 0) {
            this->f = (abs(this->f) * sign) / g;
            this->s = abs(this->s) / g;
        }
    }
};

/*
 * We need to split the points into two sets.
 * Each one has to include all colinear points.
 *
 * Consider first three values, there are three
 * possible lines, check those possibilities.
 */
void solve() {
    cini(n);
    cinai(a,n);

    function<vi(int,int)> coli=[&](int i0, int i1) {
        vi ans(n);
        const fract d(a[i0]-a[i1], i0-i1);

        for(int i=0; i<n; i++)  {
            const fract ld(a[i0]-a[i], i0-i);
            if(i==i0 || (ld.f == d.f && ld.s==d.s))
                ans[i]=1;
        }

        return ans;
    };

    function<int(int,vi&)> ft=[](int begin, vi& v) {
        for(int i=begin; i<(int)v.size(); i++)
            if(v[i])
                return i;
        return -1LL;
    };

    function<int(int,vi&)> ff=[](int begin, vi& v) {
        for(int i=begin; i<(int)v.size(); i++)
            if(!v[i])
                return i;
        return -1LL;
    };

    for(pii p : {
                make_pair(0,1)
                , make_pair(0,2), make_pair(1,2)
            }) {
        //cerr<<" line "<<p.first<<" "<<p.second<<" elements: ";
        vi l1=coli(p.first, p.second);
        /*
        for(int i=0; i<n; i++)
            cerr<<l1[i]<<" ";
        cerr<<endl;
        */
        const int l2_0=ff(0,l1);
        if(l2_0>=0) {
            const int l2_1=ff(l2_0+1, l1);
            bool ok=true;
            if(l2_1>=0) {
                vi l2=coli(l2_0, l2_1);
                /*
                cerr<<"xline "<<l2_0<<" "<<l2_1<<" elements: ";
                for(int i=0; i<n; i++)
                    cerr<<l2[i]<<" ";
                cerr<<endl;
                */

                int l1_0=ft(0, l1);
                int l1_1=ft(l1_0+1, l1);
                fract d1(a[l1_0]-a[l1_1], l1_0-l1_1);
                fract d2(a[l2_0]-a[l2_1], l2_0-l2_1);
                //cerr<<"check slope, d1.s="<<d1.s<<" d1.f="<<d1.f<<" d2.s="<<d2.s<<" d2.f="<<d2.f<<endl;
                if(d1.f!=d2.f || d1.s!=d2.s)
                    ok=false;

                for(int i=0; i<n; i++)
                    if((l1[i] + l2[i])!=1)
                        ok=false;
            }
            if(ok) {
                cout<<"Yes"<<endl;
                return;
            }
        }
    }

    cout<<"No"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
