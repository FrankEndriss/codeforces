/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

struct fract {
    ll f, s;

    fract(ll counter, ll denominator) :
        f(counter), s(denominator) {
        norm();
    }

    bool operator<(const fract &rhs) {
        return f * rhs.s < s * rhs.f;
    }

    fract& operator+=(const fract &rhs) {
        ll g = __gcd(this->s, rhs.s);
        ll lcm = this->s * (rhs.s / g);
        this->f = this->f * (lcm / this->s) + rhs.f * (lcm / rhs.s);
        this->s = lcm;
        norm();
        return *this;
    }

    friend fract operator+(fract lhs, const fract &rhs) {
        lhs += rhs;
        return lhs;
    }

    friend fract operator-(fract lhs, const fract &rhs) {
        lhs += fract(-rhs.f, rhs.s);
        return lhs;
    }

    fract& operator*=(const fract &rhs) {
        this->f *= rhs.f;
        this->s *= rhs.s;
        norm();
        return *this;
    }

    friend fract operator*(fract lhs, const fract &rhs) {
        lhs *= rhs;
        return lhs;
    }

    friend fract operator/(fract lhs, const fract &rhs) {
        return lhs * fract(rhs.s, rhs.f);
    }

    void norm() {
        int sign = ((this->f < 0) + (this->s < 0)) % 2;
        sign = -sign;
        if (sign == 0)
            sign = 1;
        ll g = __gcd(abs(this->f), abs(this->s));
        if (g != 0) {
            this->f = (abs(this->f) * sign) / g;
            this->s = abs(this->s) / g;
        }
    }
};

void solve() {
    cini(a);
    cini(b);
    cini(c);
    cini(d);
    if(fract(c,d)<fract(a,b)) {
        int a2=a*c;
        int b2=b*c;
        int c2=c*a; // a2==c2
        int d2=d*a; 

        fract ans(d2-b2, d2);
        cout<<ans.f<<"/"<<ans.s<<endl;
    } else {
        int b2=b*d;
        int a2=a*d;

        int d2=d*b;
        int c2=c*b;

        fract ans(c2-a2, c2);
        cout<<ans.f<<"/"<<ans.s<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

