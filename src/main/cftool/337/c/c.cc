/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+9;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* Manao should first get all doubles
 * he must get, then only the one points.
 * n questsions, m times correct.
 * if every kth question should be wrong, there
 * must not be more than (n/k)*(k-1)
 * correct answers.
 *
 * Max answers without doubles:
 * (m/k)*(k-1)+m%k
 *
 * Points for doubles:
 * 1. 2*k
 * 2. (2^2+2)*k == 6k
 * 3. (2^3+2^2+2)*k == 14k
 * ...
 * NOTE: MOD=1e9+9, not 1e9+7
 */
int e(int n) {
    return pl(toPower(2, n+1), -2);
}

void solve() {
    cini(n);
    cini(m);
    cini(k);

    int d=m-((n/k)*(k-1)+n%k);     /* doubles */
    d=max(0LL, d);

    int ans=e(d);
//cerr<<"e("<<d<<")="<<ans<<endl;
    ans=mul(ans,k);
    ans+=m-(d*k);
    ans%=MOD;
    
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
