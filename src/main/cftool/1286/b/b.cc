/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
#include <ext/rope>

using namespace std;
using namespace __gnu_cxx;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Number of nodes in subtree of vertex i
 * with a[sub]<a[i] == c[i]
 *
 * Obs
 * -leafs need to have c[i]=0
 * -all vertex c[i]<= num nodes in subtree
 * -num nodes in subtree - c[i]== num nodes in subtree with a[sub]>=a[i]
 * -a[leaf] can be virtually any number
 *
 *  For any vertex i the subtree has size sz[i].
 *  We need to place a number so that...
 *
 *  Assume all numbers are different, permutation.
 *  Then parents of leafs have number bigger or smaller than the leafs,
 *  depending on c[p]==0 or c[p]==1
 *  And again c[pp]
 *
 * Algo:
 * dfs, every subtree creates a list of the vertex in that subtree,
 * the order of vertex in list is the order of numbers of the a[i] in that
 * subtree.
 * So, for every vertex i we create the child lists, concat them
 * and then insert the vertex at position determined by c[i].
 * if not possible NO.
 *
 * We can use a rope to do so.
 * -> works.
 *
 * Tutorial: 
 * We can also use a BIT.
 * How to use BIT to do Kth insertion?
 * However, since N=2000 we can use simple vector and insert
 * in O(n^2).
 *
 */
void solve() {
    cini(n);
    vi c(n);
    vvi adj(n);
    vi pp(n);
    fori(n) {
        cini(p);
        p--;
        pp[i]=p;
        if(p>=0) {
            adj[p].push_back(i);
            adj[i].push_back(p);
        }
        cin>>c[i];
    }

    function<void(int,int,rope<int>&)> dfs=[&](int v, int p, rope<int>& ans) {
        int offs=ans.size();

        for(int i=0; i<adj[v].size(); i++)
            if(adj[v][i]!=p)
                dfs(adj[v][i], v, ans);

        if(ans.size()<c[v]+offs) {
            cout<<"NO"<<endl;
            exit(0);
        }
        ans.insert(ans.mutable_begin()+offs+c[v], v);
    };

    int root=0;
    while(pp[root]>=0)
        root=pp[root];

    rope<int> pos;
    dfs(root, -1, pos);
    vi ans(n);
    int i=1;
    for(auto p : pos) {
        ans[p]=i;
        i++;
    }

    cout<<"YES"<<endl;
    for(int a : ans)
        cout<<a<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
