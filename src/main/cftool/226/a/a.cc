/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* matrix 2x2 */
vvi mulM(vvi &m1, vvi &m2, int mod) {
    vvi ans(2, vi(2));
    for(int i=0; i<2; i++) {
        for(int j=0; j<2; j++) {
            for(int k=0; k<2; k++) {
                ans[i][j]+=m1[i][k]*m2[k][j];
                ans[i][j]%=mod;
            }
        }
    }
    return ans;
}

vvi toPower(vvi &a, int p, int mod) {
    vvi res = vvi(2, vi(2));
    res[0][0]=res[1][1]=1;

    while (p != 0) {
        if (p & 1)
            res = mulM(a, res, mod);
        p >>= 1;
        a = mulM(a, a, mod);
    }
    return res;
}

/* to move one we need to move all higher
 * ones to and back.
 *
 * first one to and back needs  2
 * second one to and back needs 2+ 3*2
 * third one to and back needs  2+ 3*8
 * ...
 * f(i)= 2 + 3*f(i-1)
 * ...
 * Use matrix exponentiation:
 * f[i-1] X M= f[i]
 * [2 1] X M = [8 1]
 * [8 1] X M = [26 1]
 * ...
 * M=|3 0|
 *   |2 1|
 *
 */
void solve() {
    cini(n);
    cini(mod);

    vvi M={
        { 3, 0 },
        { 2, 1 }
    };

    M=toPower(M, n-1, mod);
    int ans=(2*M[0][0]+M[1][0])%mod;
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

