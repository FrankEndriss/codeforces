/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint1000000007;

const int N=1e6;
vector<mint> fac(N);

const bool initFac=[]() {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=fac[i-1]*i;
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
mint nCr(int n, int k) {
    if(n<k)
        return mint(0);
    if(k==0 || k==n)
        return mint(1);
    if(k==1 || k==n-1)
        return mint(n);
    if(k>n/2)
        return nCr(n, n-k);

    return fac[n]*fac[k].inv()*fac[n - k].inv();
}

/**
 * Consider odd and even n separate.
 * Because, if bit i is set on left, then
 * -for even n it is not set on right
 * -for odd n it is set on right
 *
 * Go from MSB to LSB
 * Foreach bit we need to find 4 possible numbers:
 * left==1, right==0, all remaining contribute
 * left==1, right==1, samePrefix*=
 * left==0, right==0, samePrefix*=
 * left==0, right==1, do not contribute
 *
 * Number of ways to choose even number out of n
 * evenCnt = nCr(n, 0)*nCr(n,2)*...*nCr(n,i<=n)
 * How to calc this?
 * -> There are only 5 testcases, so iterate foreach one
 *
 * Case n is even:
 *...to complecated, need to try again some other time. :/
 */
vector<mint> evenCnt(N);
void solve() {
    cini(n);
    cini(k);    /* max bits */

    mint evenCnt=1;
    mint oddCnt=1;
    for(int i=2; i<=n; i+=2)
        evenCnt*=nCr(n,i);
    for(int i=1; i<=n; i+=2)
        oddCnt*=nCr(n,i);

    mint ans=1;
    mint samePre=1;
    if(n&1) {
        for(int i=0; i<k; i++) {
            samePre*=evenCnt;
        }
        ans*=samePre;
    } else {
        for(int i=0; i<k; i++) {
            ans*=evenCnt;
            samePre*=oddCnt;    /* both 1 */
            samePre*=

        }
    }
    cout<<ans.val()<<endl;
}

signed main() {
    evenCnt[0]=1;
    for(int i=2; i<N; i++) 
        evenCnt[i]=evenCnt[i-2]+nCr
    cini(t);
    while(t--)
        solve();
}
