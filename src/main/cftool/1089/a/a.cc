/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vvvb= vector<vvb>;
using vvvvb= vector<vvvb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* There are 6 results possible which are sorted by 'bestness' as
 * 3:0
 * 3:1
 * 3:2
 * 2:3
 * 1:3
 * 0:3
 * But casework does not work out, we need a dp.
 *
 *
 * let dp[i][j][x][y]=true if at standing i:j there can be x and y points used
 * dp=vb[3][3][aa][bb];
 * But how to backtrack results of matches?
 * Store the prev state.
 *
 *
 * Note that we then can calc the dp once, and reuse for all queries.
 */
const int N=201;
pair<bool,tuple<int,int,int,int>> dp[4][4][N][N];

void precalc() {

    for(int i=0; i<4; i++) {
        for(int j=0; j<4; j++) {
            for(int x=0; x<N; x++) {
                for(int y=0; y<N; y++) {
                    dp[i][j][x][y]= {false,{-1,-1,-1,-1}};
                }
            }
        }
    }

    dp[0][0][0][0].first=true;

    vector<pii> pp= {{0,0}, {1,0}, {0,1}, {1,1}, {2,0}, {0,2}, {2,1}, {1,2}};
    for(auto [i,j] : pp) {
        const int ma=25;
        //cerr<<"i="<<i<<" j="<<j<<endl;
        for(int x=0; x<N; x++) {
            for(int y=0; y<N; y++) {
                if(dp[i][j][x][y].first) {
                    if(y+ma<N) { /* possible outcomes if y wins */
                        for(int xx=0; x+xx<N && xx<=ma-2; xx++) /* not extended */
                            dp[i][j+1][x+xx][y+ma]= {true,{i,j,x,y}};
                        for(int xx=ma-1, yy=ma+1; x+xx<N && y+yy<N; xx++, yy++)
                            dp[i][j+1][x+xx][y+yy]= {true,{i,j,x,y}};
                    }
                    if(x+ma<N) { /* possible outcomes if x wins */
                        for(int yy=0; y+yy<N && yy<=ma-2; yy++)
                            dp[i+1][j][x+ma][y+yy]= {true,{i,j,x,y}};
                        for(int yy=ma-1,xx=ma+1; y+yy<N && xx+x<N; yy++,xx++)
                            dp[i+1][j][x+xx][y+yy]= {true,{i,j,x,y}};
                    }
                }
            }
        }
    }

    /* check 5th game */
    const int ma=15;
    for(int x=0; x<N; x++) {
        for(int y=0; y<N; y++) {
            if(dp[2][2][x][y].first) {
                if(y+ma<N) { /* possible outcomes if y wins */
                    for(int xx=0; x+xx<N && xx<=ma-2; xx++) /* not extended */
                        dp[2][3][x+xx][y+ma]= {true,{2,2,x,y}};
                    for(int xx=ma-1, yy=ma+1; xx+x<N && yy+y<N; xx++, yy++)
                        dp[2][3][x+xx][y+yy]= {true,{2,2,x,y}};
                }
                if(x+ma<N) { /* possible outcomes if x wins */
                    for(int yy=0; y+yy<N && yy<=ma-2; yy++)
                        dp[3][2][x+ma][y+yy]= {true,{2,2,x,y}};
                    for(int yy=ma-1,xx=ma+1; yy+y<N && xx+x<N; yy++,xx++)
                        dp[3][2][x+xx][y+yy]= {true,{2,2,x,y}};
                }
            }
        }
    }
}

void solve() {
    cini(aa);
    cini(bb);

    /* trackback result */
    const vector<pii> res={ 
        {3,0}, {3,1}, {3,2}, {2,3}, {1,3}, {0,3}
    };

    for(size_t idx=0; idx<res.size(); idx++) {
        int i=res[idx].first;
        int j=res[idx].second;
        int x=aa;
        int y=bb;

        if(dp[i][j][x][y].first) {

            cout<<i<<":"<<j<<endl;

            vector<pii> ans;
            do {
                auto [ii,jj,xx,yy]=dp[i][j][x][y].second;
                ans.push_back({x-xx, y-yy});
                i=ii;
                j=jj;
                x=xx;
                y=yy;
            }while(get<0>(dp[i][j][x][y].second)>=0);
            reverse(all(ans));
            for(size_t k=0; k<ans.size(); k++) {
                cout<<ans[k].first<<":"<<ans[k].second<<" ";
            }
            cout<<endl;
            return;
        }
    }
    cout<<"Impossible"<<endl;

}

signed main() {
    precalc();
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
