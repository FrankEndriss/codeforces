/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider chess board like coloring with 3 colors, c=(i+j)%3
 * We want to make all cells of one color being non-X.
 * So we we count the freq of x in all 3 colors, and remove the
 * ones on color fields of smallest freq.
 * Through pigeon hole that must be not more than k/3
 */
void solve() {
    cini(n);
    cinas(s,n);
    vi f(3);
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++)
            if(s[i][j]=='X')
                f[(i+j)%3]++;

    int ans=0;
    if(f[1]<f[ans])
        ans=1;
    if(f[2]<f[ans])
        ans=2;

    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++)
            if(s[i][j]=='X' && (i+j)%3==ans)
                s[i][j]='O';

    for(int i=0; i<n; i++) 
        cout<<s[i]<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
