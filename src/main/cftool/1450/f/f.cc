/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to solve the longest subarrays without repetition.
 * So, if we got a repetition, we must jump 
 * there the number of times the length of the rep - 1.
 * And we can with each jump remove one element from the
 * ends of two consecutive repetitions.
 * Example
 *
 *   3 4 2 9 3
 * - 2 3 2 9 3
 * - 1 2 2 9 3
 * - 0 1 2 9 3
 * - 0 0 1 8 3
 *   0 0 0 7 3
 *   0 0 0 6 2
 *   ...
 *   Just simulate that.
 *
 *   ***
 *   We need to consider that we can jump only to a position with
 *   same tag. Then is is a extra cost. How to to that? :/
 */
void solve() {
    cini(n);
    cinai(a,n);
    vi r;
    int prev=-1;
    int cnt=1;
    for(int i=0; i<n; i++) {
        if(a[i]!=prev) {
            r.push_back(cnt);
            cnt=1;
        } else {
            cnt++;
        }
        prev=a[i];
    }
    r.push_back(cnt);

    int ans=0;
    int l=0;
    n=r.size();
    while(l<n) {
        while(l<n && (r[l]==1 || r[l]==0)) {
            r[l]=0;
            l++;
        }
        if(l<n && r[l]>1) {
            r[l]--;
            ans++;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
