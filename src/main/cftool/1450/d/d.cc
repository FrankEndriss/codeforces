/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



template<typename E>
struct SegmentTree {
    vector<E> data;

    E neutral;
    int n;

    function<E(E,E)> plus;

    SegmentTree(int _n, E _neutral) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2, _neutral);
        neutral=_neutral;
    }

    /* bulk update in O(n) */
    template<typename Iterator>
    void init(Iterator beg, Iterator end) {
        for(int idx=n; beg!=end; beg++, idx++)
            data[idx]=*beg;

        for(int idx=n-1; idx>=1; idx--)
            data[idx]=plus(data[idx*2], data[idx*2+1]);
    }

    /* @return accumulative (l,r), both inclusive, top down */
    E query(int node, int sL, int sR, int l, int r) {
        if (r < sL || l > sR)
            return neutral;
        //cerr<<"query, node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" data[node]="<<data[node]<<endl;

        if (l<=sL && r>=sR)
            return data[node];

        int mid = (sL+sR)/2;
        return plus(query(node*2, sL, mid, l, r), query(node*2+1, mid+1, sR, l, r));
    }
    E query(int l, int r) {
        return query(1, 0, n-1, l, r);
    }

    E get(int idx) {
        return data[idx+n];
    }

    /* set val at position */
    void set(int idx, E val) {
        idx+=n;
        data[idx]=val;
        while(idx>1) {
            idx/=2;
            data[idx]=plus(data[idx*2], data[idx*2+1]);
        }
    }
};

/* N=3e5
 * For a k compression to be a permutation each
 * number <n-k must be min exactly once.
 *
 * Consider a[0]==1. If the 1 is at any other position than first or last, there will
 * be two such arrays.
 * a[x]=2 if next to the 1 it is ok, or it must be on the other end of the array.
 * And so on, up to n-k.
 *
 * What for big k, k>n/2?
 * if(k==n) there must exist a 1.
 * if(k==n-1) there must exist a 1, and 2 must be on one end.
 * if(k==n-2) there must exist a 1, and distance from 1 to 2 must be at least k...
 * We need to know the distances!
 *
 * We need to know foreach position the dist to the next smaller value to the
 * left or right. Border counts as -1.
 */
void solve() {
    cini(n);
    vi a(n);
    for(int i=0; i<n; i++) {
        cin>>a[i];
        a[i]--;
    }

    SegmentTree<int> segL(n, -1);
    segL.plus=[&](int i1, int i2) {
        return max(i1, i2);
    };
    vi dl(n);   /* dist to the left of element a[i] */
    for(int i=0; i<n; i++)  {
        int mapos=segL.query(0,a[i]-1);
        dl[i]=i-mapos-1;
        segL.set(a[i], i);
    }

    SegmentTree<int> segR(n, n);
    segR.plus=[&](int i1, int i2) {
        return min(i1, i2);
    };

    vi dr(n);   /* dr[i]=dist to the right of element a[i] */
    for(int i=n-1; i>=0; i--)  {
        int mipos=segR.query(0, a[i]-1);
        dr[i]=mipos-i-1;
        segR.set(a[i], i);
    }

    vi pos(n, -1);
    vi x(n,-1);  /* max subseq size to make element with value i min element */
    for(int i=0; i<n; i++) {
        if(a[i]<n) {
            pos[a[i]]=i;    /* any position */
            x[a[i]]=max(x[a[i]], dl[i]+dr[i]+1);
        }
    }

    /* let be x[i] the free space of a[j]=i, ie the 
     * number of positions to left and right with 
     * not smaller elements.
     * To make k=n-i work the free space x[i] of all 
     * j<i must be bgeq i+1.  (all zero based)
     */
    string ans(n,'0');
    int mind=n;
    for(int i=0; i<n; i++) {    /* check size n-i */
        int sz=x[i];
        mind=min(mind, sz);
        if(mind>=n-i)
            ans[n-i-1]='1';
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
