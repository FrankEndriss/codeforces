/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

template<typename T>
void floyd_warshal(vector<vector<T>> &d, int n) {
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}

/* We need to find the min number of rombus
 * covering all points, and each rombus must cover
 * at least one point from another rombus.
 *
 * idk :/
 * I could find if there exists any such covering
 * using dsu.
 * How to find min number?
 *
 * ***
 * Observe that we can do only one operation.
 * because after one operation all vertex are
 * in one point or the first center is unreachable.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);

    vvi adj(n, vi(n, INF));

    vi x(n);
    vi y(n);
    for(int i=0; i<n; i++)
        cin>>x[i]>>y[i];

    for(int i=0; i<n; i++) {
        int ok=true;
        for(int j=0; j<n; j++) {
            int dist=abs(x[i]-x[j])+abs(y[i]-y[j]);
            if(dist>k) {
                ok=false;
            }
        }
        if(ok) {
            cout<<1<<endl;
            return;
        }
    }
    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
