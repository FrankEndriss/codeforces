/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Find element b[i] which removed makes
 * the seq arithmetic.
 *
 * So, all diffs must be same.
 * if one b[i] is removed, two diffs are
 * replaced by the sum of that both.
 *
 * Impl?
 * Cases: Remove first/last, remove some middle
 *
 * Find blocksizes of diffs. Let a diff block be like
 * {diff,cnt}
 * They must have form
 * {d1,x}           // all same, remove first
 * {d1,1} {d2,x}    // remove first
 * {d1,x} {d2,1}    // remove last
 * {d1,x} {d2,2}
 * {d1,2} {d2,x}
 * {d1,x} {d2,2}        {d1,y} // with d2+d2==d1; x>=0; y>=0;
 * {d1,x} {d2,1} {d3,1} {d1,y} // with d2+d3==d1; x>=0; y>=0;
 * -> remove element x+1
 *
 *  sic casework :/
 *  -> tutorial
 *  check if first element is ans, check if second element is ans,
 *  else know the d and check all other positions.
 **/
void solve() {
    cini(n);

    vector<pii> b(n);
    for(int i=0; i<n; i++) {
        cin>>b[i].first;
        b[i].second=i+1;
    }

    if(n<=3) {
        cout<<1<<endl;
        return;
    }
    sort(all(b));

    bool ok=true;
    int d=b[2].first-b[1].first;
    for(int i=2; i+1<n; i++) {
        if(d != b[i+1].first-b[i].first)
            ok=false;
    }

    if(ok) {
        cout<<b[0].second<<endl;
        return;
    }

    ok=true;
    d=b[2].first-b[0].first;
    for(int i=2; i+1<n; i++) {
        if(d != b[i+1].first-b[i].first)
            ok=false;
    }
    if(ok) {
        cout<<b[1].second<<endl;
        return;
    }

    d=b[1].first-b[0].first;

    int idx=0;
    ok=true;
    int prev=b[1].first;
    for(int i=2; i<n; i++) {
        if(b[i].first-prev!=d) {
            if(idx==0) {
                idx=i;
            } else
                ok=false;
        } else
            prev=b[i].first;
    }

    if(!ok)
        cout<<-1<<endl;
    else {
        cout<<b[idx].second<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
