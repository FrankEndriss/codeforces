/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to act on sums.
 * for student i, we need to find smallest k so that
 * M - sum of k biggest t[j] >= t[i]
 *
 * So brute force would be to run all seen students
 * and add up the sum of the t[] smallest until M-sum is to small.
 *
 * ...
 * Note that t[i] is limited to 100. So we can store the studends int 
 * freq arr.
 */
void solve() {
    cini(n);
    cini(M);    /* available time */
    cinai(t,n);
//cerr<<"solve, n="<<n<<" M="<<M<<endl;

    const int N=101;
    vi freq(101);

    for(int i=0; i<n; i++) {
        int MM=M;
//cerr<<"M="<<M<<endl;
        int cnt=0;
        for(int j=N-1; MM<t[i] && j>=0; j--) {
            if(freq[j]==0)
                continue;

//cerr<<"M="<<M<<" MM="<<MM<<endl;

            if(MM+freq[j]*j<t[i]) {
                MM+=freq[j]*j;
                cnt+=freq[j];
            } else {
                int diff=t[i]-MM;
                int cnt2=(diff+j-1)/j;
                assert(cnt2<=freq[j]);
                MM+=cnt2*j;
                assert(MM>=t[i]);
                cnt+=cnt2;
            }
        }
        cout<<cnt<<" ";
        M-=t[i];
        freq[t[i]]++;
    }
    cout<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
