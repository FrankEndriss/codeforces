/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Create r[n][m] with numbers of 1s to the left of the position, 
 * including the current position.
 * Then iterate the cols, and foreach col c
 * find freq of r[i][c]
 * Then iterate the freqs, and find max product of number of ones
 * times rows with that freq.
 * O(n^2)
 */
void solve() {
    cini(n);
    cini(m);

    cinas(s,n);
    vvi r(n, vi(m+1));
    for(int i=0;  i<n; i++)
        for(int j=0; j<m; j++) 
            if(s[i][j]=='1')
                r[i][j+1]=r[i][j]+1;
            else
                r[i][j+1]=0;

    int ans=0;
    for(int j=0; j<m; j++) {
        vi f(m+1);
        for(int i=0; i<n; i++) 
            f[r[i][j+1]]++;

        int pre=0;
        for(int i=m; i>=1; i--)  {
            pre+=f[i];
            ans=max(ans, pre*i);
        }
    }
    cout<<ans<<endl;
}

signed main() {
        solve();
}
