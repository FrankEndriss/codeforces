/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Tutorial:
 * Permutation of 1,6,8,9 can form all 7 possible remainders,
 * so we can construct ans as
 * nonzero digits + p(1,6,8,9) + zeros
 * How to find which remainder the digits-part has?
 */
void solve() {
    cins(s);
    vi cnt(10);
    for(char c : s)
        cnt[c-'0']++;

    cnt[1]--;
    cnt[6]--;
    cnt[8]--;
    cnt[9]--;

    int n=1;
    int m=0;
    for(int i=0; i+4+cnt[0]<s.size(); i++) {
        while(cnt[n]==0)
            n++;

        m*=10;
        m+=n;
        m%=7;
        cout<<n;
        cnt[n]--;
    }

    vi p={1, 6, 8, 9 };
    vi mm(7);
    do {
        int i=p[0]*1000+p[1]*100+p[2]*10+p[3];
        mm[i%7]=i;
    }while(next_permutation(p.begin(), p.end()));

    for(int i=0; i<4; i++) {
        m*=10;
        m%=7;
    }

    for(int i=0; i<cnt[0]; i++)  {
        vi mm2(7);
        for(int j=0; j<7; j++)
            mm2[(j*10)%7]=mm[j];
        mm.swap(mm2);
        m*=10;
        m%=7;
    }

    cout<<mm[(7-m)%7];
    for(int i=0; i<cnt[0]; i++)
        cout<<"0";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
