/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It is about prefix sums.
 * So, removing a position means
 * remove the one item at pos and
 * switch the sign of all pos right of it.
 *
 * Ok, we get the initial sum in l,r by prefix sums.
 *
 * If we remove an even number of items, we actually
 * remove pairs (from left to right), with meaning
 * The two positions removed, and everything in between
 * switched sign.
 * Removing an odd number of item can be seen as removing
 * an even one, with an added position at r+1.
 *
 * ...some other observation?
 * Is it like the answer is allways 0,1 or 2?
 * Why?
 * We can quickly say if ans=0 is possible.
 * How to check if ans=1 is possible?
 * *********************
 * -> See tutorial, if r-l+1 is odd, 1 is possible.
 */
void solve() {
}

signed main() {
    solve();
}
