/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Note that no 2 can be after a 2, because then we would
 * need 3 crosses in a row.
 * So, a 1 occupies a row on its own.
 * A 3 occupies only a 2x2 square in the current col, and 
 * the next row.
 * A 2 occupies a 1x2 square starting in the current col.
 *
 * We can (and propably must) pair cols as follows:
 * 3,3
 * 3,2
 * 3,1
 * 2,1
 *
 * So, lets find the graph of rows, then construct the solution
 * from this graph.
 * As the tutorial suggests, the graph can be more or less simple
 * constucted by going from right to left, maintaining unused
 * cols of 3 kinds.
 */
void solve() {
    cini(n);
    cinai(a,n);

    bool ok=true;
    vi p(n,-1); /* p[i]=col closed by i */
    vector<stack<int>> ss(4); /* unmatched cols */
    for(int i=n-1; i>=0; i--)  {
        if(a[i]==1) {
            ss[1].push(i);
        } else if(a[i]==2) {
            if(ss[1].size()) {
                p[ss[1].top()]=i;
                ss[1].pop();
            } else 
                ok=false;
            ss[2].push(i);

        } else if(a[i]==3) {
            if(ss[3].size())  {
                p[ss[3].top()]=i;
                ss[3].pop();
            } else if(ss[2].size()) {
                p[ss[2].top()]=i;
                ss[2].pop();
            } else if(ss[1].size()) {
                p[ss[1].top()]=i;
                ss[1].pop();
            } else
                ok=false;

            ss[3].push(i);
        }
    }

    if(!ok) {
        cout<<-1<<endl;
        return;
    }

    
    vector<pii> ans;
    int idx=0;      /* next free row */
    vb vis(n);

    /** @return row/col of first x of element v */
    function<pii(int)> dfs=[&](int v) {
        assert(a[v]>0);
        vis[v]=true;
        if(p[v]<0) {
            pii pp= { idx, v };
            idx++;
            ans.push_back(pp);
            return pp;
        } else {
            pii prev=dfs(p[v]);
            if(a[p[v]]==2) {
                prev.second=v;
                ans.push_back(prev);
                return prev;
            } else if(a[p[v]]==3) {
                prev.second=v;
                ans.push_back(prev);
                prev.first=idx;
                idx++;
                ans.push_back(prev);
                return prev;
            } else
                assert(false);
        }
        assert(false);
    };

    for(int i=n-1; i>=0; i--)
        if(!vis[i] && a[i]>0)
            dfs(i);

    if(idx>n) {
        cout<<-1<<endl;
        return;
    }

    cout<<ans.size()<<endl;
    for(pii pp : ans)
        cout<<pp.first+1<<" "<<pp.second+1<<endl;

}



signed main() {
    solve();
}
