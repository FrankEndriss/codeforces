/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* simple fraction implementation based on long long */
struct fract {
    ll f, s;

    fract(ll counter, ll denominator) :
        f(counter), s(denominator) {
        norm();
    }

    bool operator<(const fract &rhs) const {
        return f * rhs.s < s * rhs.f;
    }

    fract& operator+=(const fract &rhs) {
        ll g = __gcd(this->s, rhs.s);
        ll lcm = this->s * (rhs.s / g);
        this->f = this->f * (lcm / this->s) + rhs.f * (lcm / rhs.s);
        this->s = lcm;
        norm();
        return *this;
    }

    friend fract operator+(fract lhs, const fract &rhs) {
        lhs += rhs;
        return lhs;
    }

    friend fract operator-(fract lhs, const fract &rhs) {
        lhs += fract(-rhs.f, rhs.s);
        return lhs;
    }

    fract& operator*=(const fract &rhs) {
        this->f *= rhs.f;
        this->s *= rhs.s;
        norm();
        return *this;
    }

    friend fract operator*(fract lhs, const fract &rhs) {
        lhs *= rhs;
        return lhs;
    }

    friend fract operator/(fract lhs, const fract &rhs) {
        return lhs * fract(rhs.s, rhs.f);
    }

    void norm() {
        int sign = ((this->f < 0) + (this->s < 0)) % 2;
        sign = -sign;
        if (sign == 0)
            sign = 1;
        ll g = __gcd(abs(this->f), abs(this->s));
        if (g != 0) {
            this->f = (abs(this->f) * sign) / g;
            this->s = abs(this->s) / g;
        }
    }
};

int sum(int i, int p) {
    int d=i/p;
    int dd=i%p;
    int n0=(d+1)*(d+1)*dd;
    int n1=d*d*(p-dd);
    return n0+n1;
}
/* We need to minimize the max remaining size of all carrots.
 * So we cut the biggest carrot in two.
 * If that is still the biggest we cut it in three...
 * Repeat.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    priority_queue<pair<int, pii>> q;    /* <saveNextCut,<origSize, pieces>> */
    for(int i=0; i<n; i++) {
        q.emplace(sum(a[i],1)-sum(a[i],2), make_pair(a[i], 1));
    }

    int cnt=n;
    while(cnt<k) {
        auto cur=q.top();
        //cerr<<"ff="<<cur.first<<" f="<<cur.second.first<<" s="<<cur.second.second<<endl;
        q.pop();
        q.push({sum(cur.second.first, cur.second.second+1)-sum(cur.second.first, cur.second.second+2),
                make_pair(cur.second.first, cur.second.second+1)
               });
        cnt++;
    }

    int ans=0;
    while(q.size()) {
        int f=q.top().second.first;
        int s=q.top().second.second;
        q.pop();
        ans+=sum(f,s);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
