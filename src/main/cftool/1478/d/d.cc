/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* It seems we cannot make smaller steps than min(gcd(x[i],x[j])) of all pairs.
 *
 * How to construct a given number?
 * We need to find some sum of numbers==k
 * We can add each number 2i times, or remove r times.
 * sum(i)==sum(r)
 *
 * Note that k is huge.
 * So it not seems to be a dp. The x[] are also big, so we need some greedy
 * part...constructive.
 *
 * Can we construct k bit by bit somehow?
 *
 * Consider two elements of x, a and b.
 * c=2*a - b
 * d=2*c - b 
 *  =2*(2*a -b) - b
 *  =4*a - 3*b
 * e=b - 2*c
 *  =b - 2*(2*a - b)
 *  =b - 4*a - 2*b
 *  =-b - 4*a
 */
void solve() {
    cini(n);
    cini(k);
    cinai(x,n);

    int g=abs(x[0]);
    for(int i=1; i<n; i++) 
        g=gcd(g, abs(x[i]));

    if(abs(k)%g!=0)
        cout<<"NO"<<endl;
    else
        cout<<"YES"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
