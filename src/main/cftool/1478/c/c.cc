/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Since a[] is symetric it is that:
 * sum(a[0..n-1])==0
 *
 * Cosider the biggest element in d[bg]
 * It is the biggest because the diff of all
 * numbers |a[bg]-a[j]| is maximum.
 * And that is for the biggest or the smallest a[k].
 * And since the array a is symetric, it is that we must
 * have that number in d[] twice, too.
 * We must have all numbers there twice.
 *
 * Consider the smallest element in d[], it conforms to
 * the or one of the median elements in a[].
 * How does this help?
 *
 * However, what is wrong with third example?
 * Why can we have sums 8 12, but not 7 11 ?
 * Consider 3 numbers with sum of diffs
 *
 *
 * First example
 * d[0]=abs(1+3) + abs(1+1) + abs(1-3) = 8
 * d[1]=abs(-3-1) + abs(-3+1) + abs(-3-3) = 12...
 *
 */
void solve() {
    cini(n);
    cinai(d,2*n);
    map<int,int> f;
    for(int i=0; i<2*n; i++)
        f[d[i]]++;

    for(auto ent : f) {
        if(ent.second%2!=0 || ent.first%2!=0) {
            cout<<"NO"<<endl;
            return;
        }
    }
    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
