/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


#include <atcoder/lazysegtree>
using namespace atcoder;


using S=pii; /* freq, first=0, second=1 */
using F=signed; /* 0 or 1, the one comming later overrides the first */

S st_op(S a, S b) {
    return { a.first+b.first, a.second+b.second};
}

S st_e() {
    return { 0LL, 0LL };
}

/* This applies an update to some value.
 * ie if st_op is mul, then we would 
 * return f*x.
 */
S st_mapping(F f, S x) {
    if(f==-1) {
        return x;
    } else if(f==0)
        return { x.first+x.second, 0LL };
    else if(f==1)
        return { 0LL, x.first+x.second };
    else 
        assert(false);
}

/* This creates a mapping from two given mappings.
 * ie if st_op is mul, then we would
 * return f*g, since f*(g*x)==(f*g)*x
 */
F st_composition(F f, F g) {
    if(f<0)
        return g;
    return f;   /* f overwrites g */
}

/* this is the neutral update. ie
 * if st_op is + then st_id would be 0,
 * if st_op is mul then st_id would be 1,
 * if st_op is min then st_id is INF
 * It is similar to st_e() in some sense.
 */
F st_id() {
    return -1;
}

using mysegtree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/* We start with s and must make it eq f in the end.
 * Additionally we must make all q segments all same
 * in order of the segments.
 *
 * Operations:
 * _First_ a segment is inspected to be all same, then
 * afterwards the elements in the segment can be 
 * changed.
 * They must be changed so, that all following segment
 * checks will succeed.
 *
 * We need to go from back to front. Consider the last 
 * segment llast,rlast. Before the check it must be all same, after
 * the check it must be like f[llast,rlast]
 * Since we can change strictly less than the halve the letters,
 * there is only one possible "all same" configuration.
 * Then do the same for the day before, but not start with f,
 * but with the determined "all same" configuration.
 * Go on until first day.
 *
 * ... Fuck, the segtree somehow does not work!
 * I do not understand.
 */
void solve() {
    cini(n);
    cini(q);
    cins(s);
    cins(f);

    //cerr<<"s="<<s<<" f="<<f<<endl;

    vi l(q);
    vi r(q);
    for(int i=0; i<q; i++) {
        cin>>l[i]>>r[i];
        l[i]--;
        r[i]--;
    }


    assert(f.size()==n);
    assert(s.size()==n);
    vector<pii> data(n);
    for(int i=0; i<n; i++)  {
        if(f[i]=='0')
            data[i].first=1;
        else
            data[i].second=1;
    }

    mysegtree seg(data);

    for(int i=q-1; i>=0; i--) {
        pii freq=seg.prod(l[i], r[i]+1);

        if(freq.first>freq.second) {
            //cerr<<"change all to 0, l[i]="<<l[i]<<" r[i]="<<r[i]<<endl;
            seg.apply(l[i], r[i]+1, 0);
        } else if(freq.second>freq.first) {
            //cerr<<"change all to 1, l[i]="<<l[i]<<" r[i]="<<r[i]<<endl;
            seg.apply(l[i], r[i]+1, 1);
        } else {
            //cerr<<" failed after day "<<i<<endl;
            cout<<"NO"<<endl;
            return;
        }
    }

    for(int i=0; i<n; i++) {
        pii val=seg.prod(i,i+1);
        //cerr<<"val.first="<<val.first<<" val.second="<<val.second<<endl;
        if((s[i]=='0' && val.second) || (s[i]=='1' && val.first)) {
            //cerr<<" failed on first day at pos="<<i<<endl;
            cout<<"NO"<<endl;
            return;
        }
    }

    cout<<"YES"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
