/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to find foreach set of friends the expected value,
 * and from all those values the one nearest to 1.0
 *
 * Each friend is identified by its propability.
 * Consider a set S, and f to be one element in S, then 
 * E(S) = f*(1+E(S\f)) + (1-f)*E(S\f)
 *
 * A single element set produces f problems, consider adding a f0:
 * E(f+f0)=2*f0*f + f0*(1-f) + f*(1-f0) + 0*(1-f)*(1-f0)
 *
 * But, there are 2^100 sets :/
 *
 * ...
 * But we do need only the best propability for exactly _one_ problem, 
 * so this is for a set S of f[] friedns: let
 * p(f[]) be the product of all f friends, and
 * p1(f[]) be the product of all 1-f[i] friends
 * sum(i=0..n-1)
 *  f[i]*p1(f)/(1-f[i])
 *
 * ...
 * editorial says, for some reason it is allways optimal
 * to use a suffix of the sorted list of propabilities, ie
 * the k biggest ones.
 * So just add them until ans becomes worse.
 *  
 */
void solve() {
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
