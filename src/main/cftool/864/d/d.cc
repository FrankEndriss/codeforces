/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* greedy
 * we need to find the numbers missing,
 * and the freq of the existing numbers.
 * Then, change existing numbers whit
 * freq>1 from left to right by the 
 * missing numbers in increasing order.
 *
 * Note: if the missing number is less than the
 * existing one we need to replace from left,
 * else from right.
 */
void solve() {
    cini(n);
    map<int,int> f;
    set<int> mis;
    for(int i=1; i<=n; i++)
        mis.insert(i);

    vi a(n);
    for(int i=0; i<n; i++) {
        cin>>a[i];
        mis.erase(a[i]);
        f[a[i]]++;
    }

    vb second(n);
    cout<<mis.size()<<endl;
    for(int i=0; i<n; i++) {
        if(f[a[i]]>1) {
            auto it=mis.begin();
            if(*it<a[i] || second[a[i]]) {
                f[a[i]]--;
                a[i]=*it;
                mis.erase(it);
            } else if(*it>a[i])
                second[a[i]]=true;
        }
    }

    for(int i=0; i<n; i++)
        cout<<a[i]<<" ";
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

