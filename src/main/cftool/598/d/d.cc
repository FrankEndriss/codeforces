/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* bfs all positions which result will be components.
 * for every component find number of borders to
 * adjacent wall cells.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    cinas(s,n);

    const vi dx={1,-1,0,0};
    const vi dy={0,0,1,-1};

    vvi comp(n, vi(m,-1));  /* comp[i][j]=component of cell i,j */
    vi csz(n*m+1); /* csz[i]= num pics in component i */

    int co=0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(s[i][j]=='*' || comp[i][j]>=0)
                continue;

            co++;
            csz[co]=0;

            queue<pii> q;
            q.emplace(i,j);
            comp[i][j]=co;
            while(q.size()) {
                auto [i0,j0]=q.front();
                q.pop();

                for(int k=0; k<4; k++) {
                    int i1=i0+dx[k];
                    int j1=j0+dy[k];
                    if(i1>=0 && i1<n && j1>=0 && j1<m) {
                        if(s[i1][j1]=='*')
                            csz[co]++;
                        else if(comp[i1][j1]<0) {
                            comp[i1][j1]=co;
                            q.emplace(i1,j1);
                        }
                    }
                }
            }
        }
    }

    for(int i=0; i<k; i++) {
        cini(r);r--;
        cini(c);c--;
        cout<<csz[comp[r][c]]<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
