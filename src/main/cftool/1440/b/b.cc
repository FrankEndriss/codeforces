/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * What do the arrays look like to have max sum?
 * Assume we simply use as is, sorted.
 * Can we make it better? What if we change numbers
 * between blocks?
 * Consider lowest block.
 * if we change the whole left half (all left of the media)
 * between both blocks the median in the lower block will
 * arise.
 * So use the smallest numbers for the left halfs!
 * Then increasing.
 */
void solve() {
    cini(n);
    cini(k);

    cinai(a,n*k);
    sort(all(a));

    const int le=(n+1)/2-1; /* number of element left of the median */
    int idx=k*le;
    int ans=0;
    for(int i=0; i<k; i++) {
        //cerr<<a[idx]<<" ";
        ans+=a[idx];
        idx+=(n-le);
    }
    //cerr<<endl;
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
