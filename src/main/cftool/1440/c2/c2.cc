/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 3*n*m vs n*m
 * N*M=100x100
 *
 * Make all 2x2 grids zeros by at most 4 operations.
 * Check before each 2x2 if the next 2x3 can be fixed
 * in two operations.
 * How to implement the operations?
 * ***
 * Consider the next 2*1 field, 4 cases:
 * 00 -> ok
 * 11 -> fix with one operation
 * 10 -> fix with one operation
 * 01 -> fix with one operation
 * Then do operations on last row.
 * And on last col.
 */
void solve() {
    cini(n);
    cini(m);
    cinas(s,n);

    vector<pii> ans;

    function<void(int,int)> flip=[&](int i, int j) {
        if(s[i][j]=='0')
            s[i][j]='1';
        else
            s[i][j]='0';

        ans.emplace_back(i+1,j+1);
    };

    for(int i=0; i+1<n; i++) {
        for(int j=0; j+1<m; j++) {
            if(s[i][j]=='1' && s[i][j+1]=='1') {
                flip(i,j);
                flip(i,j+1);
                flip(i+1,j);
            } else if(s[i][j]=='1' && s[i][j+1]=='0') {
                flip(i,j);
                flip(i+1, j);
                flip(i+1, j+1);
            } else if(s[i][j]=='0' && s[i][j+1]=='1' && j+2==m) {
                flip(i, j+1);
                flip(i+1, j);
                flip(i+1, j+1);
            }
        }
    }

    /* now last col and last rows contains 1s */
    for(int i=0; i+1<n; i++) {
        if(s[i][m-2]=='1' && s[i][m-1]=='1') {
            flip(i,m-2);
            flip(i,m-1);
            flip(i+1,m-1);
        } else if(s[i][m-1]=='1') {
            flip(i,m-1);
            flip(i+1, m-2);
            flip(i+1, m-1);
        }
    }

    /* last row */
    for(int j=0; j+2<m; j++) {
        if(s[n-2][j]=='1' && s[n-1][j]=='1') {
            flip(n-2, j);
            flip(n-1, j);
            flip(n-1, j+1);
        } else if(s[n-2][j]=='1' && s[n-1][j]=='0') {
            flip(n-2, j);
            flip(n-2, j+1);
            flip(n-1, j+1);
        } else if(s[n-1][j]=='1') {
            flip(n-2, j+1);
            flip(n-1, j+1);
            flip(n-1, j);
        }
    }

    function<int(int,int)> cnt=[&](int i, int j) {
        return s[i][j]-'0'+s[i+1][j]-'0'+s[i][j+1]-'0'+s[i+1][j+1]-'0';
    };

    int lcnt=0;
    const int i=n-2;
    const int j=m-2;
    while((lcnt=cnt(i, j))>0) {
        if(lcnt==4) {
            flip(i,j);
            flip(i,j+1);
            flip(i+1,j);
        } else if(lcnt==3) {
            if(s[i][j]=='1')
                flip(i,j);
            if(s[i+1][j]=='1')
                flip(i+1,j);
            if(s[i][j+1]=='1')
                flip(i,j+1);
            if(s[i+1][j+1]=='1')
                flip(i+1, j+1);
        } else if(lcnt==2) {
            int cnt1=0;
            for(int ii=0; ii<2; ii++)
                for(int jj=0; jj<2; jj++) {
                    if(s[i+ii][j+jj]=='0')
                        flip(i+ii, j+jj);
                    else if(cnt1==0)  {
                        flip(i+ii, j+jj);
                        cnt1++;
                    }
                }
        } else if(lcnt==1) {
            int cnt0=0;
            for(int ii=0; ii<2; ii++)
                for(int jj=0; jj<2; jj++) {
                    if(s[i+ii][j+jj]=='1')
                        flip(i+ii, j+jj);
                    else if(cnt0<2) {
                        flip(i+ii, j+jj);
                        cnt0++;
                    }
                }
        }
    }

    cout<<ans.size()/3<<endl;
    for(int i=0; i<ans.size(); i+=3) {
        cout<<ans[i].first<<" "<<ans[i].second<<" "<<ans[i+1].first<<" "
            <<ans[i+1].second<<" "<<ans[i+2].first<<" "<<ans[i+2].second<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
            int cnt0=0;
