/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using piii= pair<pii,int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

template <typename E>
class SegmentTree {
  private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

  public:
    /** SegmentTree. Note that you can hold your data in your own storage and give
     * an Array of indices to a SegmentTree.
     * @param beg, end The initial data.
     * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
     * @param pCumul The cumulative function to create the "sum" of two nodes.
    **/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=(int)distance(beg, end);
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Updates all elements in interval(idxL, idxR].
    * iE add 42 to all elements from 4 to inclusive 7:
    * update(4, 8, [](int i1) { return i1+42; });
    */
    void updateRange(int pIdxL, int pIdxR, function<E (E)> apply) {
        pIdxL+=treeDataOffset;
        pIdxR+=treeDataOffset;
        for(int i=pIdxL; i<pIdxR; i++)
            tree[i]=apply(tree[i]);

        while (pIdxL != ROOT) {
            pIdxL=parent(pIdxL);
            pIdxR=max(pIdxL, parent(pIdxR-1));
            for(int i=pIdxL; i<=pIdxR; i++)
                tree[i]=cumul(tree[leftChild(i)], tree[rightChild(i)]);
        }
    }

    /** Updates the data at dataIdx to value returned by apply. */
    void updateApply(int dataIdx, function<E(E)> apply) {
        updateSet(dataIdx, apply(get(dataIdx)));
    }

    /** Updates the data at dataIdx by setting it to value. */
    void updateSet(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return value at single position, O(1) */
    E get(int idx) {
        return tree[idx+treeDataOffset];
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

const int INF=1e9;

/* First check how long it takes if all students
 * work on the bugs.
 * Then find cheapest set of students taking not
 * longer.
 *
 * Obviously there must be at least one student with
 * skill more than the biggest bug.
 * Greedy:
 * 1. Use the cheapest one.
 * 2. We need at least #bugs/days students.
 *   Remove the days hardest bugs (fixed by the first student)
 *   and add the cheapest student able to fix the biggest
 *   remaining bug.
 * 3. Repeat
 * *****************************
 * Ok, completly wrong.
 * There is a maximus price sum.
 * We want to find the set of students we can buy for that price
 * fixing the bugs in min possible days.
 *
 * If we book a student it is always best if he fixes the biggest
 * bugs possible at his skill.
 * For the biggest bug of all we need a student with a bigger skill,
 * so we use the cheapest stu bigger than the biggest bug.
 * If that student is to expensive no solution.
 * Else binsearch number of days.
 * Buy cheapest students possible for that number of days.
 * The less days the more expensive.
 *
 */
void solve() {
    cini(n);
    cini(m);
    cini(s);

    vector<pii> a;
    int mabug=0;
    for(int i=0; i<m; i++) {
        cini(aux);
        a.emplace_back(aux, i);
        mabug=max(mabug, aux);
    }
    sort(all(a), greater<pii>());

    cinai(b,n); /* student skills */
    cinai(c,n); /* student costs */
    vector<piii> bc;
    int cheastu=INF;    /* cheapest student kill biggest bug */
    for(int i=0; i<n; i++) {
        bc.push_back({{b[i], c[i]}, i+1});
        if(b[i]>=mabug)
            cheastu=min(cheastu, c[i]);
    }
    sort(all(bc));

    if(cheastu==INF) {
        cout<<"NO"<<endl;
        return;
    }

    int l=1;
    int r=m;

    vi ans(m);
    while(l+1<r) {
        vi lans(m);

        int mid=(l+r)/2;

        SegmentTree<piii> seg(all(bc), {{INF,INF},INF}, [](piii p1, piii p2) {
            if(p1.first.second<=p2.first.second)
                return p1;
            else
                return p2;
        });

        int price=0;    /* price paid for booked students */
        for(int i=0; price<=s && i<(m+mid-1)/mid; i++) {
            int bug=a[i*mid].first;
//cerr<<"bug="<<bug<<" at i*mid="<<i*mid<<endl;

            int l2=-1;
            int rr=n;
            while(l2+1<rr) {
                int mm=(l2+rr)/2;
//cerr<<"mm="<<mm<<" seg.get(mm).first.first="<<seg.get(mm).first.first<<endl;
                if(seg.get(mm).first.first<bug)
                    l2=mm;
                else
                    rr=mm;
            }
//cerr<<"rr="<<rr<<" n="<<n<<endl;
            if(rr==n) {
                price=INF;
                break;
            }
            
            piii stu=seg.get(rr, n);
            price+=stu.first.first;

            for(int j=i*mid; j<m && j<(i+1)*mid; j++)
                lans[a[j].second]=stu.second;
        }

        if(price<=s) {
            l=mid;
            ans=lans;
        } else
            r=mid;
    }

    cout<<"YES"<<endl;
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
