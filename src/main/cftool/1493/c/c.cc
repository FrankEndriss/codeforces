/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* we need to append letters to s
 * so that it becomes beautiful, in alphabetical order.
 * ...no.
 *
 * Consider the number of different letters in ans, at least
 * 1, but allways a divisior of n/k.
 *
 * First letter of s stays same.
 * then check if k times that letter is ok, it yes, fini.
 * else try the second letter of s.
 * and so on.
 *
 * We need to consider the longest prefix of s that
 * we can be build in ans. We must build this prefix.
 * then use as next letter a letter which is bigger as
 * the one in s.
 * The prefix is the longest prefix using the max given 
 * number of letters.
 * Also the number of usages of one symbol must not be to big.
 * Since the total number of usages must be divisable by k,
 * we need to add a certain number of symbols after the prefix.
 * That sum of length must not be bigger n.
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);

    if(n%k!=0) {
        cout<<-1<<endl;
        return;
    }
    if(s.size()>n)
        s=s.substring(0,n);

    vi pre(s.size());   /* pre[i]=min len of ans if prefix up to i is used */

    pre[0]=k;
    vi freq(26);
    freq[s[0]]++;
    vvi ffreq(s.size());
    ffreq[0]=freq;
    int val=k;
    for(size_t i=1; i<s.size(); i++) {
        freq[s[i]]++;
        if(freq[s[i]]%k==1)
            val+=k;

        ffreq[i]=freq;
        pre[i]=val;
    }

    /*
    const int nk=n/k;
    vi d;
    for(int i=2; i*i<=nk; i++) {
        if(nk%i==0) {
            d.push_back(i);
            if(nk/i!=i)
                d.push_back(nk/i);
        }
    }
    */

        auto it=upper_bound(all(pre), n);
        it--;
        int prelen=distance(pre.begin(), it);
        /* note that in prelen there are not more than n/k different symbols */
        int cnt=0;
        vi touse(256);

        string ans;
        int kk=n;
        for(int i=0; i<prelen; i++) {
            ans+=s[i];
            touse[s[i]]++;
            if(touse[s[i]]==k) {
                kk-=k;
                touse[s[i]]=0;
            }
        }

        touse['a']+=kk;
        int c='a';
        while(ans.size()<n) {
            if(ans.size()+1>=s.size()) {
                /* use smallest from touse[] bigger than s[ans.size()] */
                /* another case: there could be no one bigger.
                 * in that case we need to increment the last used symbol by one,
                 * like subst all 'b' by 'c' 
                 * ...that makes no fun, sorry, I am out.
                 * */
            } else {
                /* use smallest from touse[] */
            } else {
                /* add new touse
            }

        }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
