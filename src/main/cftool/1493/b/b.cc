/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* mirrored numbers */
vi mir={ 0, 1, 5, -1, -1, 2, -1, -1, 8, -1 };
/* what a useless casework...
 */
void solve() {
    int h,m;
    cin>>h>>m;
    string s;
    cin>>s;

    int h01=s[0]-'0';
    int h02=s[1]-'0';
    int m01=s[3]-'0';
    int m02=s[4]-'0';

    /* increment the hour until we find a working one.
     * Note that minutes 00 allways work as hours. 
     * So two cases:
     * if orig hour is mirrorable, then find the next minute 
     * int that hour.
     * else use the next mirrorable hour and minute 00.
     * */
    if(mir[h01]>=0 && mir[h02]>=0 && mir[h02]*10+mir[h01]<m) {
        /* case 1, hour is mirrorable */
        for(int i=m01*10+m02; i<m; i++) {
            /* find first minute mirrored usable as hour */
            if(mir[i%10]>=0 && mir[i/10]>=0 && mir[i%10]*10+mir[i/10]<h) {
                cout<<h01<<h02<<":"<<i/10<<i%10<<endl;
                return;
            }
        }
    }

    /* now increment hours and use minute=00 */
    int hh=(h01*10+h02+1)%h;
    while(true) {
        int h1=hh/10;
        int h2=hh%10;
        //cerr<<"hh="<<hh<<" h1="<<h1<<" h2="<<h2<<endl;
        if(mir[h1]>=0 && mir[h2]>=0 && mir[h2]*10+mir[h1]<m) {
            cout<<h1<<h2<<":00"<<endl;
            return;
        }

        hh=(hh+1)%h;
    }
    assert(false);

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
