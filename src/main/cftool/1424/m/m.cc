/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/*
 * see https://cp-algorithms.com/graph/topological-sort.html
 */

vector<bool> visited;
vector<int> ans;

void dfs(vector<set<int>>& adj, int v) {
    visited[v] = true;
    for (int u : adj[v]) {
        if (!visited[u])
            dfs(adj,u);
    }
    ans.push_back(v);
}

vi topological_sort(vector<set<int>>& adj, int n) {
    visited.assign(n, false);
    ans.clear();
    for (int i = 0; i < n; ++i) {
        if (!visited[i])
            dfs(adj, i);
    }
    reverse(ans.begin(), ans.end());
    return ans;
}

void solve() {
    cini(n);
    cini(k);
    vector<vector<string>> p(n);
    for(int i=0; i<n; i++) {
        cini(idx);
        for(int j=0; j<k; j++) {
            cins(s);
            p[idx].push_back(s);
        }
    }
    vs dict;
    for(int i=0; i<n; i++) {
        for(size_t j=0; j<p[i].size(); j++) 
            dict.push_back(p[i][j]);
    }
    p.clear();

    vector<set<int>> adj(n);

    for(int j=0; j<100; j++) {
        for(size_t i=1; i<dict.size(); i++) {
            if(dict[i-1].size()<=j || dict[i].size()<=j)
                continue;

            if(j==0 || dict[i-1].substr(0,j)==dict[i].substr(0,j)) {
                if(dict[i-1][j]!=dict[i][j]) {
                    adj[dict[i-1][j]-'a'].insert(dict[i][j]-'a');
                }
            }
        }
    }

    topological_sort(adj, n);
    for(int i=0; i<n; i++) {
        cout<<(char)(ans[i]+'a');
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
