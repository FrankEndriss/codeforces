/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, vector<ll> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization.push_back(d);
            n /= d;
        }
    }
    if (n > 1)
        factorization.push_back(n);
}

/* Euler totiem function, number of coprimes of n in (1,n)
 * see https://cp-algorithms.com/algebra/phi-function.html
 **/
int phi(int n) {
    int result = n;
    for (int i = 2; i * i <= n; i++) {
        if(n % i == 0) {
            while(n % i == 0)
                n /= i;
            result -= result / i;
        }
    }
    if(n > 1)
        result -= result / n;
    return result;
}

/* We need to find number b to pair with a.
 * b>a+1 && b<=a*a
 * b=a*a allways friends
 *
 * a=1 -> none
 * a=2 -> b=3 -> 1,2,3 -> no
 * a=2 -> b=4 -> 2,1,2 -> yes
 * a=2 -> b=5 -> 1,2,5 -> no
 * a=2 -> b=6 -> 2,1,3 -> no
 *
 * a=3 -> b=4 -> 1,3,4 -> no
 * a=3 -> b=5 -> 1,3,5 -> no
 * a=3 -> b=6 -> 3,1,2 -> no
 * a=3 -> b=7 -> 1,3,7 -> no
 * a=3 -> b=9 -> 3,1,3 -> yes
 *
 * a=4, b=5 -> 1,4,5 no
 * a=4, b=6 -> 2,2,3 yes
 * a=4, b=7 -> 1,4,7 no
 * a=4, b=8 -> 2,2,4 no
 * a=4, b=10 -> 2,2,5 no
 * a=4, b=12 -> 4,1,3 no
 * ...
 * I dont see the pattern :/
 * Is 6 friend of 8?
 * 6,8 -> 2,3,4 -> yes
 * 8,10 -> 2,4,5 -> yes
 * ...so all even numbers are friends.
 * and all numbers are friends with their quads, starting at 3
 * We need to consider odd quads.
 *
 * 3*3+3,3*3+3+3
 * 12,15 3,4,5
 *
 * 4*4+4,4*4+4+4
 * 20,24 4,5,6
 *
 * 30,35 5 6 7
 * 35,40 5 7 8
 * 40,45 5 8 9
 *
 * Let x be some odd number, then we 
 * can create triple x,x+k,x+k+1, which was a pair
 * x*(x+k),x*(x+k+1)
 * So we can pair all numbers wich are odd but dividable
 * by some number.
 * ans=number of primes up to n plus 1.
 *
 * *****
 * We need to consider only the primes with pr[i]*pr[i]>n
 */
const int N=1e6+3;
void solve() {

    notpr[1]=false;
    vi ans(N);
    for(int i=1; i<N; i++) {
        ans[i]=ans[i-1];
        if(!notpr[i])
            ans[i]++;
    }

    cini(t);
    while(t--) {
        cini(n);
        int a=ans[n];
        int sq=sqrt(n);
        a-=ans[sq];
        cout<<a+1<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
