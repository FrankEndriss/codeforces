/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

using mvvi= map<int,map<int,int>>;

#define endl "\n"

/* Max-Flow with Edmonds-Karp
 * see https://cp-algorithms.com/graph/edmonds_karp.html */
const int INF = 1e10;

int bfs(int s, int t, vi &parent, vvi &adj, mvvi &capacity) {
	fill(parent.begin(), parent.end(), -1);
	parent[s] = -2;
	queue<pii> q;
	q.push( { s, INF });

	while (!q.empty()) {
		int cur = q.front().first;
		int flow = q.front().second;
		q.pop();

		for (int next : adj[cur]) {
			if (parent[next] == -1 && capacity[cur][next]) {
				parent[next] = cur;
				int new_flow = min(flow, capacity[cur][next]);
				if (next == t)
					return new_flow;
				q.push( { next, new_flow });
			}
		}
	}

	return 0;
}

/* s==source
 * t==sink
 * n==adj.size()
 * adj is standart adjency matrix
 * NOTE: capacity is n*n matrix, only positive capacity is inserted
 * NOTE: adj is unidirected, ie both directions must be inserted!
 */
int maxflow(int s, int t, vvi &adj, mvvi &capacity, int n) {
	int flow = 0;
	vi parent(n);
	int new_flow;

	while ((new_flow = bfs(s, t, parent, adj, capacity))) {
		flow += new_flow;
		int cur = t;
		while (cur != s) {
			int prev = parent[cur];
			capacity[prev][cur] -= new_flow;
			capacity[cur][prev] += new_flow;
			cur = prev;
		}
	}

	return flow;
}

/* Max flow...somehow?
 * We need to find the pairing with the minimum max(cost).
 * Sum of costs does not matter.
 *
 * Binary search.
 * For given maxcost=x, is there a pairing?
 * How to check? -> Remove all edges with costs>x, then find
 * any pairing with max flow.
 *
 * ... something wrong here :/
 */
void solve() {
    cini(n);
    cini(m);
    vector<pair<int,pii>> edg(m);
    for(int i=0; i<m; i++)
        cin>>edg[i].second.first>>edg[i].second.second>>edg[i].first;

    sort(all(edg));


    /* return true if there is a pairing with maxcost<=x */
    function<bool(int)> go=[&](int x) {
        /* n=S; n+1=T */
        const int S=n*2;
        const int T=n*2+1;
        mvvi cost;
        vvi adj(n*2+2);

        for(int i=0; i<m && x>=edg[i].first; i++)  {
            adj[S].push_back(edg[i].second.first);
            adj[edg[i].second.first].push_back(S);
            cost[S][edg[i].second.first]=1;
            cost[edg[i].second.first][S]=0;

            adj[edg[i].second.first].push_back(edg[i].second.second+n);
            adj[edg[i].second.second+n].push_back(edg[i].second.first);
            cost[edg[i].second.first][edg[i].second.second+n]=1;
            cost[edg[i].second.second+n][edg[i].second.first]=0;

            adj[T].push_back(edg[i].second.second+n);
            adj[edg[i].second.second+n].push_back(T);
            cost[edg[i].second.second+n][T]=1;
            cost[T][edg[i].second.second+n]=0;
        }

        int flow=maxflow(S, T, adj, cost, n*2+2);
        cerr<<"x="<<x<<" flow="<<flow<<endl;
        return flow==n;
    };

    int l=0;
    int r=1e9+7;
    int ans=-1;
    while(l+1<r) {
        int mid=(l+r)/2;
        if(go(mid)) {
            r=mid;
            ans=r;
        } else 
            l=mid;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
