/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve2(const int n) {
    cini(q);
    cinai(a,n);
    cinai(b,n);

    vll preA(n);
    vll preSumA(n);

    for(int i=0; i<n; i++) {
        preA[i]=a[i];
        if(i>0)
            preA[i]+=preA[i-1];

        preSumA[i]=preA[i];
        if(i>0)
            preSumA[i]+=preSumA[i-1];
    }

    for(int i=0; i<q; i++) {
        cini(l);
        cini(r);
        l--;
        r--;

        ll pre=0;
        if(l>0)
            pre=preSumA[l-1];

        ll ans=preSumA[r]-pre*(r-l+1);
        cout<<ans<<endl;
    }
}

void solve() {
    cini(n);
    if(n>5000) {
        solve2(n);
        return;
    }

    cini(q);
    cinai(a,n);
    cinai(b,n);
 
    for(int i=0; i<q; i++) {
        cini(l);
        cini(r);
        l--;
        r--;
 
        /* ok, do the brute force again... */
        ll ans=0;
        ll la=0;
        ll lb=0;
        for(int i=l; i<=r; i++)  {
            la+=a[i];
            lb+=b[i];
            ans+=max(la, lb);
        }
 
        cout<<ans<<endl;
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

