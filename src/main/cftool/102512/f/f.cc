/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* last resolved ? */

const string LOVE="LOVE";

vs pat= {
    "?OVE",
    "L?VE",
    "LO?E",
    "LOV?"
};

int cnt=0;
int idx=0;
vi ones;

void index(string &s) {
    for(char c : s) 
        if(c=='?')
            cnt++;

    int offs=0;
    for(string p : pat) {
        size_t pos=0;
        while(true) {
            pos=s.find(p, pos);
            if(pos!=string::npos) {
                ones.push_back(pos);
                pos++;
            } else
                break;
        }
    }
    sort(ones.begin(), ones.end());
//log(ones.size());
}

void setone(string &s, int i, char c) {
    s[i]=c;

    string str="  ";
    str[1]=c;
    cout<<i+1<<str<<endl;
    cnt--;

    if(cnt>0) {
        cnt--;
        cini(lidx);
        lidx--;
        cins(s0);
        s[lidx]=s0[0];
        for(string p : pat) {
            bool done=false;
            for(int i=max(0, lidx-3); i<=lidx; i++) {
                if(i+3<=s.size() && p==s.substr(i, 4)) {
//cout<<"substr=="<<s.substr(i,4)<<endl;
                    ones.push_back(i);
                    done=true;
                    break;
                }
            }
            if(done)
                break;
        }
    }
}

int idx2=0;

/* minimize LOVE, try to
 * destroy as much as possible
 * single letter resolvables,
 * dont care for others. */
void dekomori(string &s) {
    while(cnt) {
        bool done=false;
        while(idx<ones.size() && !done) {
            for(int i=0; i<4; i++) {
                if(s[ones[idx]+i]=='?') {
                    char c;
                    if(i==0)
                        c='O';
                    else
                        c='L';

                    setone(s, ones[idx]+i, LOVE[(i+2)%4]);
                    done=true;
                }
            }
            idx++;
        }

        if(!done) {
            while(idx2<s.size()) {
                if(s[idx2]=='?') {
                    setone(s, idx2, 'L');
                    break;
                }
                idx2++;
            }
        }
    }
}

/* maximize LOVE,
 * resolve all patterns resolvable with one move,
 * do not care about others. */
void nibutani(string &s) {
    while(cnt) {
        bool done=false;
        while(idx<ones.size() && !done) {
            for(int i=0; i<4; i++) {
                if(s[ones[idx]+i]=='?') {
                    setone(s, ones[idx]+i, LOVE[i]);
                    done=true;
                }
            }
            idx++;
        }
    
        if(!done) {
            while(idx2<s.size()) {
                if(s[idx2]=='?') {
                    setone(s, idx2, 'L');
                    break;
                }
                idx2++;
            }
        }
    }
}

void solve() {
    cins(s);
    cini(t);
    index(s);
    if(t==1)
        nibutani(s);
    else
        dekomori(s);
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

