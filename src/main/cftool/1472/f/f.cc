/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* we cannot simply simulate the strip,
 * but we can simply cut out parts of it,
 * since nothing changes after even number
 * of tiles if there are no obstacles.
 */
void solve() {
    cini(n);
    cini(m);
    vector<pii> a(m);  /* obstacles */
    for(int i=0; i<m; i++)
        cin>>a[i].second>>a[i].first;
    sort(all(a));

    /* Starting at the first col not completly blocked there are 3 possible states:
     * 0:none blocked
     * 1:row 0 blocked
     * 2:row 1 blocked
     * Create the states until after last obstacle. If there the state is 0, 
     * then ans=YES, else NO
     */
    int state=0;
    int col=0;
    for(int i=0; i<m; i++) {
        int dist=a[i].first-col;

        /* edgecase, two obstacles in one col */
        if(col==a[i].first) {
            if(state==0) {
                cout<<"NO"<<endl;
                return;
            } else
                state=0;
            continue;
        }

        col=a[i].first;

        if(state==0) {
            state=a[i].second;
        } else if(state==1) {
            if((a[i].second==1 && dist%2==1) || (a[i].second==2 && dist%2==0)) {
                state=0;
            } else {
                cout<<"NO"<<endl;
                return;
            }
        } else if(state==2) {
            if((a[i].second==2 && dist%2==1) || (a[i].second==1 && dist%2==0)) {
                state=0;
            } else {
                cout<<"NO"<<endl;
                return;
            }
        } else 
            assert(false);
    }

    if(state==0) 
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
