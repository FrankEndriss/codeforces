/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We swap all peoples sizes so that h>=w.
 * Then sort all people desc.
 * So given the height we can place all later
 * in list before all earlier in list.
 * Then we go throug the list and find 
 * among the later ones, one with a with less than current.
 * To do this, we maintain a multiset of all persons in the
 * list after the current (and with bigger h).
 * To find the one we binsearch this multiset for the first
 * with fitting with.
 */
void solve() {
    cini(n);
    vector<tuple<int,int,int>> a(n);

    multiset<pii> f;

    for(int i=0; i<n; i++) {
        cini(h);
        cini(w);
        if(h<w)
            swap(h,w);

        a[i]={-h,-w,i};
        f.insert({-w,i});
    }

    sort(all(a));

    int idx=0;
    vi ans(n);
    for(int i=0; i<n; i++) {

        while(idx<n && get<0>(a[idx])==get<0>(a[i])) {
            auto it=f.lower_bound(make_pair(get<1>(a[idx]), get<2>(a[idx])));
            assert(it!=f.end());
            f.erase(it);
            idx++;
        }

        auto it=f.upper_bound({get<1>(a[i]), n});
        if(it==f.end()) {
            ans[get<2>(a[i])]=-1;
        } else {
            ans[get<2>(a[i])]=it->second+1;
        }
    }
    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
