/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider bfs with dist[i]=dist from root to i
 * Then we need from all i the dist to root.
 * Do the same again, but with reverse adj.
 *
 * Then dfs with the rules given in statement.
 * Foreach vertex we are visiting find ans as the min
 * of current dist and the dist of all reachable 
 * vertex.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
    }
    //cerr<<"after parse"<<endl;

    vi dist1(n, INF);   /* dist root to i */
    queue<int> q;
    q.push(0);
    dist1[0]=0;
    while(q.size()) {
        int v=q.front();
        q.pop();
        for(int chl : adj[v]) {
            if(dist1[chl]==INF) {
                dist1[chl]=dist1[v]+1;
                q.push(chl);
            }
        }
    }
    //cerr<<"after bfs 1"<<endl;

    vvi ans(n, vi(2,INF));
    ans[0][0]=0;
    ans[0][1]=0;
    vvb vis(n, vb(2));

    function<int(int,bool)> dfs=[&](int v, bool rec) {
        if(vis[v][rec])
            return ans[v][rec];

        vis[v][rec]=true;
        ans[v][rec]=min(ans[v][rec], dist1[v]);
        for(int chl : adj[v]) {
            if(dist1[chl]>dist1[v])
                ans[v][rec]=min(ans[v][rec], dfs(chl,rec));
            else if(!rec) 
                ans[v][rec]=min(ans[v][rec], dfs(chl,true));
        }
        return ans[v][rec];
    };

    for(int i=0; i<n;i++) 
        dfs(i,false);

    //cerr<<"after dfs"<<endl;

    for(int i=0; i<n; i++) {
        cout<<min(ans[i][0], ans[i][1])<<" ";
    }
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
