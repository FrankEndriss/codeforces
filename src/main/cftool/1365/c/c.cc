/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* How to find which shift is opt?
 * Do it a[i] for a[i]?
 * Since it is permutation every element has two
 * positions, diff is shift.
 * Find highest freq of shifts.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    vi posa(n+1);
    vi posb(n+1);
    for(int i=0; i<n; i++)  {
        posa[a[i]]=i;
        posb[b[i]]=i;
    }

    map<int,int> f;
    for(int i=1; i<=n; i++) {
        int sh=posa[i]-posb[i];
        if(sh<0)
            sh+=n;
        f[sh]++;
    }

    int ans=1;
    for(auto ent : f)
        ans=max(ans, ent.second);

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
