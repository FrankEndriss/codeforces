/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* escape at n,m
 *
 * Only 2500 cells.
 * We need to find a path from all G to Escape
 * while dist to B is allways >1. Because then we
 * can put blocks there.
 * Simply to BFS.
 *
 * Corner case:
 * We need to check for B next to escape.
 */
void solve() {
    cini(n);
    cini(m);
    vs g(n);
    vector<pii> goods;
    for(int i=0; i<n; i++) {
        cin>>g[i];
    
        for(int j=0; j<m; j++) {
            if(g[i][j]=='G') {
                goods.emplace_back(i,j);
                g[i][j]='.';
            }
        }
    }

    vi dr={ -1, 1, 0, 0};
    vi dc={ 0, 0, -1, 1};
    function<bool(int,int)> canUse=[&](int r, int c) {
        if(g[r][c]!='.')
            return false;

        for(int i=0; i<4; i++) {
            if(r+dr[i]>=0 && r+dr[i]<n && c+dc[i]>=0 && c+dc[i]<m) {
                if(g[r+dr[i]][c+dc[i]]=='B')
                    return false;
            }
        }

        return true;
    };

    vvb vis(n, vb(m));
    queue<pii> q;

    if(canUse(n-1,m-1)) {
        q.emplace(n-1, m-1);
        vis[n-1][m-1]=true;
    }

    while(q.size()) {
        pii p=q.front();
        int r=p.first;
        int c=p.second;
//cerr<<q.size()<<" r="<<r<<" c="<<c<<endl;
        q.pop();

        for(int i=0; i<4; i++) {
            if(r+dr[i]>=0 && r+dr[i]<n && c+dc[i]>=0 && c+dc[i]<m) {
                if(canUse(r+dr[i], c+dc[i]) && !vis[r+dr[i]][c+dc[i]]) {
                    vis[r+dr[i]][c+dc[i]]=true;
                    q.emplace(r+dr[i], c+dc[i]);
                }
            }
        }
    }

    bool ans=true;
    for(int i=0; i<goods.size(); i++) {
        if(!vis[goods[i].first][goods[i].second])
            ans=false;
    }

    if(ans)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
