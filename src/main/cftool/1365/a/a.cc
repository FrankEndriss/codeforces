/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* last move wins.
 * There are components in the grid of claimable cells.
 * If we claim on one end of such component we remove
 * make the component two shorter.
 * If we claim in the middle we claim tree cells
 * What if claming 3x3 cross ? ...somehow?
 *
 * Count number of free components.
 * With every move we make one component smaller by 2, 3, 4 or 5 cells.
 *
 * blah.
 * We can claim min(free(row), free(col)) cells
 */
void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));
    vb rows(n);
    vb cols(m);
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            cini(aux);
            if(aux) {
                rows[i]=true;
                cols[j]=true;
            }
        }

    int frow=0;
    for(int i=0; i<n; i++)
        if(!rows[i])
            frow++;
    int fcol=0;
    for(int i=0; i<m; i++)
        if(!cols[i])
            fcol++;

    if(min(frow,fcol)%2==1)
        cout<<"Ashish"<<endl;
    else
        cout<<"Vivek"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
