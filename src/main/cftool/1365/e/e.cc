/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to find a subset of a
 * Such that the highest bit is set in at least
 * k-2 elements of the set.
 * So, using one element with the highest bit of all set
 * we can add two other arbitrary numbers without that bit set.
 *
 * First group numbers by highest bit set.
 *
 * Then choose one element from highest set.
 * from all numbers of that group find the ones haveing next bit set.
 * ...blah...
 *
 * Since the highest bit is the highest number, we need to add
 * the biggest numbers until max possible k is reached.
 *
 * We need to eliminate double elements.
 *
 * What can be wrong???
 * -> Best solution is always of size 3. Just brute force it.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans=0;
    for(int i=0; i<n; i++) {
        ans=max(ans, a[i]);
        for(int j=i+1; j<n; j++) {
            ans=max(ans, a[i] | a[j]);
            for(int k=j+1; k<n; k++)  {
                ans=max(ans, a[i] | a[j] | a[k]);
            }
        }
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
