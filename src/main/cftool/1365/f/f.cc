/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* implement bfs to find which permutations are (not) reacheable */
void test(int n) {
    vi a(n);
    iota(all(a), 1);

    set<vi> s;
    queue<vi> q;
    q.push(a);
    s.insert(a);
    while(q.size()) {
        a=q.front();
        q.pop();
        for(int k=1; k<=n/2; k++) {
            vi b=a;
            for(int i=0; i<k; i++) 
                swap(b[i], b[n-k+i]);
            if(s.count(b)==0) {
                s.insert(b);
                q.push(b);
            }
        }
    }
    for(vi v : s) {
        for(int i=0; i<n; i++) 
            cout<<v[i]<<" ";
        cout<<endl;
    }
}

/**
 * What is the invariant?
 * Since k<=floor(n/2) we cannot move the center
 * element of an odd length array.
 * Else we can create any permutation.
 *
 * WA ???
 *
 * Consider
 * 1 2 3 4 5 6
 * 6 2 3 4 5 1
 * 5 6 3 4 1 2
 *
 * 1 2 3 4 5 6 7
 * 4 5 6 1 2 3
 * 2 3 6 1 4 5
 *
 * -> Positions of element change ever only by +-(n/2)
 * ...no
 *
 * -> Number pairs stay on symetric positions, 
 *  if a[i] is moved to b[j], then a[n-1-i] is moved to b[n-1-j],
 *  so these numbers must match over all pairs.
 *  Also consider single center element of odd len arrays.
 *
 *  Implement by collecting all pairs (multiset), these pairs
 *  must be same in both arrays.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    if(n&1) {
        if(a[n/2]!=b[n/2]) {
            cout<<"No"<<endl;
            return;
        }
    }
    multiset<pii> sa;
    multiset<pii> sb;

    for(int i=0; i<n/2; i++) {
        sa.emplace(min(a[i], a[n-1-i]), max(a[i], a[n-1-i]));
        sb.emplace(min(b[i], b[n-1-i]), max(b[i], b[n-1-i]));
    }
    if(sa==sb)
        cout<<"yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
//    test(6);
    cini(t);
    while(t--)
        solve();
}
