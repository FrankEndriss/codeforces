/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

struct Dsu {
    vector<int> p;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        iota(p.begin(), p.end(), 0);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};

/* the alternating b[] form components.
 * a swap is possible only in one component.
 * DSU?
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    Dsu dsu(n);

    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            if(b[i]!=b[j])
                dsu.union_sets(i,j);
        }
    }

    map<int,vi> c;  /* c[i]=list of elements in group i */
    for(int i=0; i<n; i++)
        c[dsu.find_set(i)].push_back(i);

    /* now sort every component, then check if whole array is sorted */

    for(auto ent : c) {
        sort(all(ent.second));
        vi vals;
        for(int idx : ent.second)
            vals.push_back(a[idx]);
        sort(all(vals));
        for(int i=0; i<ent.second.size(); i++) {
            a[ent.second[i]]=vals[i];
        }
    }

    int ok=true;
    for(int i=1; i<n; i++) {
        if(a[i]<a[i-1])
            ok=false;
    }

    if(ok)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
    
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
