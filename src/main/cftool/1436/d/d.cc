/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Bandit must choose one leaf.
 * People in that leaf cannot escape.
 * People in parents of leafs must distribute
 * thair number on all reachable leafs evenly, since one leaf
 * wont escape.
 * So each childs number of people is max(sum(vertex)/numberOfChilds, sum(child))
 *
 */
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=1; i<n; i++) {
        cini(aux);
        adj[aux-1].push_back(i);
    }
    cinai(a,n);

    vi cnt(n);

    /* @return number of citizens in subtree v */
    function<int(int)> dfs=[&](int v) {
        int ans=a[v];
        for(int chl : adj[v])
           ans+=dfs(chl); 
        return cnt[v]=ans;
    };

    /* @return number of leafs in subtree v */
    vi cnt2(n);
    function<int(int)> dfs2=[&](int v) {
        int ans=0;
        for(int chl : adj[v])
           ans+=dfs2(chl); 
        if(ans==0)
            ans=1;
        return cnt2[v]=ans;
    };

    vi cnt3(n);
    /* @return min number of citizen in any leaf of subtree v */
    function<int(int)> dfs3=[&](int v) {
        int ans=(cnt[v]+cnt2[v]-1)/cnt2[v]; 
        //cerr<<"dfs3, v="<<v<<" first ans="<<ans<<endl;
        //cerr<<"dfs3, cnt[v]="<<cnt[v]<<" cnt2[v]="<<cnt2[v]<<endl;
        for(int chl : adj[v])
            ans=max(ans, dfs3(chl));
        return ans;
    };

    dfs(0);
    dfs2(0);
    int ans=dfs3(0);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
