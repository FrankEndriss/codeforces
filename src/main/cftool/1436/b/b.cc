/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * smallest prime as a sum of two non primes:
 * non neg: 0,1,2...
 * prime: 2,3,5...
 * put 0s and two 1s in every row/col
 *
 * x 0 x
 * x x 0
 * 0 x x
 *
 * x 0 0 x
 * x x 0 0 
 * 0 x x 0
 * 0 0 x x
 *
 */
void solve() {
    cini(n);

    vvi ans(n, vi(n));
    int idx=0;
    for(int i=0; i<n; i++) {
        ans[i][i]=1;
        ans[i][(i+1)%n]=1;
    }
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) 
            cout<<ans[i][j]<<" ";
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
