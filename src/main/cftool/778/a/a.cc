/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* simulate?
 * would not be that hard.
 * but O(n^2)
 *
 * -> binary search for number of removed letters.
 */
void solve() {
    cins(t);
    cins(p);
    cinai(a, t.size());

    auto pos=[&](int cnt) {

        vb rem(t.size());
        for(int i=0; i<cnt; i++)
            rem[a[i]-1]=true;

        size_t i1=0;
        size_t i2=0;
        while(i2<p.size()) {
            while(i1<t.size() && ( rem[i1] || t[i1]!=p[i2]))
                i1++;

            if(i1==t.size())
                break;      

            i1++;
            i2++;
        }

        bool ans=(i2==p.size());
        return ans;
    };

    int l=0;
    int r=t.size();
    while(l+1<r) {
        int mid=(l+r)/2;
        if(pos(mid))
            l=mid;
        else
            r=mid;
    }
    cout<<l<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
