/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* We need to find in a tree some paths and distances between "special" nodes.
 * We have to create new edge between two special nodes.
 * That new edge shortens the path from some node 1 and some node n.
 * We want to create in so that the shortening is minimized.
 * That is, we want to create it between the nearest two nodes.
 *
 * And, we need to check if there is a special node not on the
 * path between 1 and n, because then we can create the edge
 * between that node and some arbitrary other one, without
 * shortening the path.
 *
 * So, we BFS find the path from 1 to n. (it is not a tree)
 * Traversing that path, we can count shortest dist between two 
 * contained special nodes, and check if (at least) one special is not 
 * on the path.
 * Then ans= length of the path - shortening
 *
 * What about multiple paths?
 * Say there is one of len x, and we can opt shorten it to x-1.
 * And there is one of len x+1, and we can opt shorten that one to x.
 * How to find that?
 * We would need to first consider the shortest path, and check if all
 * special nodes are on that path.
 * if not, we can consider a shortest path over the first such special node, 
 * ie 1-sp-n.
 * Then again find the opt shortening of that path.
 * repeat, until all sp are found on some path.
 * ...
 * Soooo
 * We need to collect the distances from all special nodes back home.
 * Then alls distances from n to any special node.
 * So we have two distances per sp node.
 * We need to maximize min(first.first+second.second)
 * Note that we cannot make shortest path longer, so if we find any sol
 * beeing path 1,n the shortest, that will be ok.
 * TODO: implementation
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    cinai(a,k); // special fields

    vvi adj(n);
    for(int i=0; i<m; i++) {    // m
        cini(u);
        cini(v);
        u--;
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

// bfs shortest paths to any node starting from 1 and starting from n
    vb vis(n);
    queue<int> q;
    q.push(0);
    while(q.size()) {
    }

// then find the opt pair a,b
// TODO
    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

