/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find matching chains, like in domino
 * Note that someone can be matches with himself.
 * Matching two guests costs the bigger one, ie
 * saves the smaller one.
 * Maximize the saves.
 *
 * Since we can choose for each guest virtually any neigbour guest,
 * we sort the left sides, and sort the right sides, then match
 * them by that order.
 * magically that works, but should be proved somehow :/
 */
void solve() {
    cini(n);
    vi l(n);
    vi r(n);
    for(int i=0; i<n; i++)
        cin>>l[i]>>r[i];

    sort(all(l));
    sort(all(r));
    int ans=n;
    for(int i=0; i<n; i++) 
        ans+=max(l[i], r[i]);

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
