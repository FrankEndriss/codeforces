/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to find in a[] for every length 1<=i<=n
 * the minimum sum subarray.
 * Then for that subarray the maximums size subarray
 * in b[] with product<=x.
 * From all those local max find the max for ans.
 *
 * Find subarrays in a[] with slide windows in n*n
 * Find max segment in b[] with two pointer.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    cinai(b,m);
    cini(x);

    int ans=0;
    for(int i=1; i<=n; i++) {
        int sum=0;
        for(int j=0; j<i; j++) 
            sum+=a[j];

        int mi=sum;
        for(int j=i; j<n; j++) {
            sum-=a[j-i];
            sum+=a[j];
            if(sum<mi) {
                mi=sum;
            }
        }

        if(mi>x)
            continue;

//cerr<<"mi sum of len="<<i<<" sum="<<mi<<" x="<<x<<endl;

        int l=0;
        sum=0;
        for(int r=0; r<m; r++) {
            sum+=b[r]*mi;
            while(sum>x)
                sum-=b[l++]*mi;
            ans=max(ans, i*(r-l+1));
        }
    }

    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
