/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/* greedy
 * we need to consecutively add biggest h
 * to a local list and cut of min(limitK,hdiffToNext).
 *
 * In every step we cut the list to the hight of the next
 * tower, if possible.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(h,n);

    sort(all(h), greater<int>());
    if(h[0]==h.back()) {
        cout<<0<<endl;
        return;
    }

    int ans=1;
    int kk=k;
    for(int i=0; i+1<n; i++) {  /* we cut all but the last tower */

        while(h[i]>h[i+1]) {
            if(kk<i+1) {
                ans++;
                kk=k;
            }
            int numcut=min(kk/(i+1), (h[i]-h[i+1])); /* we actually cut only up to next hight */
            kk-=numcut*(i+1);
            h[i]-=numcut;
        }

    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
