/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Obviously its a dp.
 * One result is giving all friends the dollars.
 * We can do better if we can buy for a friend a 
 * cheaper present, we want to buy the cheapest.
 *
 * Note that it matters for wich friend we buy a present,
 * since we pay the present price, but get the friends price.
 *
 * So we search for a matching from presents to friends
 * which maximizes the flow.
 * But we cannot use max-flow algo since TLE.
 *
 * Some kind of greedy?
 * Consider the friends in order of friends price, most expensive first.
 * From all remaining presents choose the best.
 * if a friend cannot get any present, try to change with one of the friends
 * allready having a present. The one with the biggest k[i]
 *
 * ****
 * We want to buy the most possible presents, since giving the money
 * to a friend is the worst solution.
 * We just need to find how many presents can be bought under the rules.
 * -> We buy the cheapest present, and give it to the friend with 
 *  the highest price among all friends which can get it.
 * -> Segment tree, sort the friends by price
 *
 *  ****
 *  Sort the friends by k[i]
 *  Iterate the friends.
 *  From all presents available to ith friend, choose the cheapest one.
 *  Note that later friends also can get the presents from earlier friends.
 *  Note also that given some presents and some friends, it does not 
 *  matter which friend gets which present.
 *
 *  So, if a friend does not get a present at all, change him against
 *  a cheaper friend.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cinai(k,n);
    cinai(c,m);

    for(int i=0; i<n; i++) 
        k[i]--;

    sort(all(k));

    multiset<int> pres;  /* prices of available presents */
    multiset<int> frie;  /* prices of friends which have a present */
    int idx=0;
    int ans=0;
    for(int i=0; i<n; i++) {
        while(idx<=k[i]) {
            pres.insert(c[idx]);
            idx++;
        }

        /* we need to consider if it is cheaper to buy the cheapest
         * available present, or the cheapest friend .
         */

        int prprice=INF;
        int frprice=c[k[i]];

        if(pres.size())
            prprice=*pres.begin();

        if(frie.size())
            frprice=min(frprice, *frie.begin());

        if(prprice<frprice) {
            ans+=prprice;
            pres.erase(pres.begin());
            frie.insert(c[k[i]]);
        } else {
            ans+=frprice;
            frie.insert(c[k[i]]);
            auto it=frie.lower_bound(frprice);
            frie.erase(it);
        }
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
