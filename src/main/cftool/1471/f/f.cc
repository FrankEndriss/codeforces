/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* see https://cp-algorithms.com/data_structures/disjoint_set_union.html
 * Also known as union find structure. 
 * Note that the above link has some optimizations for faster combining of sets if needed. 
 */

struct Dsu {
    vector<int> p;
    vector<int> s;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        s.resize(size, 1);
        iota(p.begin(), p.end(), 0);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members. 
 * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            s[a]+=s[b];
        }
    }
};

/* We need to color the graph with two alternating colors while still
 * beeing able to reach all vertex.
 * We would like to do a dfs, but that will most likely TLE.
 * *****
 * What else can we do?
 * Note that if there is a cycle of odd length then it is not possible.
 * Else it allways is, simply to the coloring.
 * How to find all circle sizes?
 * We dont need it. Simply do the coloring, if possible then it is ok,
 * else not.
 */
void solve() {
    cini(n);
    cini(m);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vi col(n,-1);

    int ans=0;
    function<bool(int,int)> dfs=[&](int v, int c) {
        if(col[v]>=0)
            return col[v]==c;

        col[v]=c;

        if(c==1)
            ans++;

        bool ans=true;
        for(int chl : adj[v])
            if(!dfs(chl,!c))
                ans=false;

        return ans;
    };

    bool possible=dfs(0,0);

    if(!possible) {
        cout<<"NO"<<endl;
        return;
    } else 
        cout<<"YES"<<endl;

    cout<<ans<<endl;
    for(int i=0; i<n; i++) {
        if(col[i]==1)
            cout<<i+1<<" ";
    }
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
