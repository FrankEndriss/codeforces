/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * if n is odd we can remove the center element.
 * If all 3 center are same, we can remove any of them.
 *
 * if n is even, we can remove any from the center group if
 * center group > 1.
 */
void solve() {
    cini(n);
    cins(s);

    if(n%2==1) {
        int c=n/2;
        int ans=1;
        for(int j=1; j<=c; j++) {
            if(s[c-j]==s[c])
                ans+=2;
            else 
                break;
        }
        cout<<ans<<endl;
    } else {
        int c2=n/2;
        int c1=c2-1;
        int ans=0;
        if(s[c1]==s[c2]) {
            ans=2;
            for(int j=c1-1; j>=0; j--)
                if(s[j]==s[c1])
                    ans+=2;
                else
                    break;
        }
        cout<<ans<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
