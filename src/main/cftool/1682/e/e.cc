/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The swaps form cycles.
 * We need to sort each cylce in a way, so that
 * each swap eliminates one element from the cycle.
 *
 * Use dsu to find cycles.
 * then foreach cycle, find a swap that moves one element
 * to correct position, and execute the swap.
 * Repeat for circleSize-1 times.
 *
 * Can there be more than one next swap in a cycle, can
 * we choose a wrong one?
 *
 * And how to implement choosing efficient?
 *
 * Since there are still only 14 submissions, I guess it is more complected :/
 */
void solve() {
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
