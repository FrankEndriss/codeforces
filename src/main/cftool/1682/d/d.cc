/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The rules of "not intersecting inside the circle" say
 * we can connect each vertex to a segment of adj vertex,
 * on both sides...somehow.
 * However, if we connect some a and b, there are
 * some vertex on half-circle from X=a->b, and some on the other
 * half, Y=b->a, and
 * we cannot connect any X to any Y.
 * Note that each s[i]=1 must have (at least) one edge.
 *
 * So we can simply connect all s[i]==1 in a circle to each other.
 * Really?
 * No, "it is a tree" means most likely that every vertex is connected,
 * so every s[i]==0 has to have at least 2 edge, the other at least 1 edge.
 *
 * Ok, so we connect all s[i]==0 in a circle, and each s[i]==1 on the
 * segments between them with one of the adj even vertex.
 * But, what if the number of vertex on the segment is odd?
 * -> no solution, I guess.
 *
 * Also, eliminate a bunch of edgecases.
 */
void solve() {
    cini(n);
    cins(s);

    vi a;   /* even indexes */
    for(int i=0; i<n; i++)
        if(s[i]=='0')
            a.push_back(i);

    if(a.size()==0)  {
        cout<<"NO"<<endl;
        return;
    } else if(a.size()==1) {
        if(n%2==1) {
            cout<<"YES"<<endl;
        } else
            cout<<"NO"<<endl;
        return;
    } else if(a.size()==2) {
        /* conside first testcase.
         * the number of odd vertex is (n-2)
         * Consider connecting the both even vertex,
         * then we need an odd number to connect on both of
         * them, so
         * if (n-2) is even one to one , and the others
         * to the other, else
         * if (n-2) is odd no sol.
         *
         * Also consider if n==2...
         * thats to sic edgecases, I dont want ot implement such shitty problem.
         * ...
         */

        // TODO
    }

    /* check each segment */
    vector<pii> ans;
    for(size_t i=0; i+1<a.size(); i++) {
        int cnt=a[i+1]-a[i]-1;
        if(cnt%2==1) {
            cout<<"NO"<<endl;
            return;
        }
        ans.emplace_back(a[i], a[i+1]);
        for(int k=a[i]+1; k<a[i+1]; k++)
            ans.emplace_back(a[i], k);
    }

    int cnt=n-1-a.back()+a[0];
    if(cnt%2==1) {
        cout<<"NO"<<endl;
        return;
    }
    ans.emplace_back(a.back(), a[0]);
    for(int k=(a.back()+1)%n; k!=a[0]; k=(k+1)%n)
        ans.emplace_back(a.back(), k);

    cout<<"YES"<<endl;
    for(pii p : ans)
        cout<<p.first+1<<" "<<p.second+1<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
