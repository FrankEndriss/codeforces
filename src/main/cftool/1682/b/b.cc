/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How?
 * We need to move all indexes where there is a wrong number.
 * So, we have to find the smallest mask including them all.
 * ...
 * No.
 * For X=0 we can allways sort the array.
 *
 * Consider index 1. 
 * If (x&1)==0 we cannot move pos=1 ever.
 * If (x&2)==0 we cannot positions (p&2)==0
 *
 * So, if all positions
 *
 * ...
 * consider positions 1..7 sorted, 8,9 swapped.
 *
 * So, from examples, it seems that
 * if first is sorted, X=1
 * else if first 2 are sorted, x=2
 * else if first 3 are sorted, x=4
 * etc
 * Not fully clear why.
 */
void solve() {
    cini(n);
    cinai(p,n);

    vi a=p;
    sort(all(a));

    int cnt=0;
    for(int i=0; i<n; i++) {
        if(a[i]==p[i])
            cnt++;
        else
            break;
    }

    if(cnt==0) {
        cout<<0<<endl;
        return;
    }

    int ans=1;
    while((ans<<1)<=cnt)
        ans*=2;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
