/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * First, red wants to minimize blues nodes.
 * So first color K leafs since that maximizes the number
 * of subtrees blue cannot color.
 *
 * DFS for the size of the subtree of each vertex.
 * Then DFS the tree sorted by uncovered subtree size, down to K leafs.
 * This creates the max possible not-blue coverage.
 * Blue gets all uncovered tree nodes.
 * Then add more red chips "up" in the tree. This creates more
 * red nodes. That should allways be possible, so if all leafs are covered,
 * then R has k nodes and B has none.
 *
 * Note that red gets allways exactly K vertex (if it wants it).
 *
 * How to optimze w*(r-b)?
 * If r-b is positive, R wants to maximize w, else R wants
 * to minimize w.
 * By minimizing b R has used some r vertex, so w=n-b-r 
 * Now if R adds another one vertex, w gets one smaller, but r-b one bigger.
 * So this optimizes until r-b==w.
 *
 * Else if r-b is negative R cannot do anything about it, since it cannot 
 * color more vertex than K, and has minimzed b anyway.
 *
 * Can it be better to give blue some leaf to maximize w?
 * -> No. If R gives away a vertex, it gets colored blue, so w does not change.
 *
 * So there must be some implementation bug since pretest 13 bullies me :/
 * Some edgecase???
 */
void solve() {
	cini(n);
	cini(k);
	vvi adj(n);
	for(int i=0; i<n-1; i++) {
		cini(u); u--;
		cini(v); v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	vi dp(n);
	vi par(n, -1);
	function<void(int,int)> dfs=[&](int v, int p) {
		//cerr<<"dfs, v="<<v<<endl;
		par[v]=p;
		dp[v]=1;
		for(int chl : adj[v]) {
			if(chl!=p) {
				dfs(chl,v);
				dp[v]+=dp[chl];
			}
		}
	};
	dfs(0,-1);

	vvi adj2(n);	/* adj[][] without parents and sorted by vertex count in subtree */
	for(int i=0; i<n; i++) {
		for(int v : adj[i]) {
			if(v!=par[i])
				adj2[i].push_back(v);
		}
		sort(all(adj2[i]), [&](int i1, int i2) {
			return dp[i1]<dp[i2];
		});
	}

	set<pii> q;	/* priority_queue <-cnt,v> */
	for(int i=0; i<n; i++) 
		q.emplace(-dp[i],i);

	/* dfs adj2, and remove each. Also update the prio q */
	function<void(int)> dfs2=[&](int v) {
		//cerr<<"dfs2, v="<<v<<endl;

		if(adj2[v].size()) {
			dfs2(adj2[v].back());
			adj2[v].pop_back();
		}

		auto it=q.find({-dp[v],v});
		assert(it!=q.end());
		q.erase(it);
	};

	int cntR=0;
	while(cntR<k && q.size()) {
		auto it=q.begin();
		auto [sz,v]=*it;
		cerr<<"sz="<<sz<<endl;
		dfs2(v);
		cntR++;
	}

	const int cntB=(int)q.size();
	int w=n-cntB-cntR;
	assert(w>=0);
	int ans=w*(cntR-cntB);
	while(w>0 && cntR<k) {
		w--;
		cntR++;
		ans=max(ans, w*(cntR-cntB));
	}

	cout<<ans<<endl;

}

signed main() {
    solve();
}
