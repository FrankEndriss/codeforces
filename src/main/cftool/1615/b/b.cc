/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to remove all numbers where one bit is not set.
 * So count the freq of 0s per bit.
 * Prefix sums.
 */
const int N=2e5+7;
vvi cnt(20, vi(N));

void solve() {
	cini(l);
	cini(r);

	int ans=r-l+1;
	for(int i=0; i<20; i++) {
		int c=cnt[i][r]-cnt[i][l-1];
		ans=min(ans, r-l+1-c);
	}
	cout<<ans<<endl;
}

signed main() {
	for(int i=1; i<N; i++) {
		for(int j=0; j<20; j++) {
			cnt[j][i]=cnt[j][i-1]+((i>>j)&1);
		}
	}

	cini(t);
	while(t--)
    		solve();
}
