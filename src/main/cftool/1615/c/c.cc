/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * In one operation we flip one candle in
 * inverse the whole thing.
 * So we switch only odd number of candles.
 *
 * But how to see if the inverse could be better than
 * the original string?
 *
 * example:
 * if s0 has one more 1 than t, we need one operation
 *
 *
 * So, in even parity moves we have cnt0+(i*2) lit candles,
 * in odd parity moves we have (cnt1=n-cnt0)+1+(i*2) lit candles
 *
 * Note also that we cannot switch more than one candle.
 * So we add one candle and flip all others, or move the lit candles.
 */
const int INF=1e9;
void solve() {
	cini(n);
	cins(s);
	cins(t);

	string s0=s;	/* inverse of s */
	for(int i=0; i<n; i++) {
		if(s[i]=='1')
			s0[i]='0';
		else
			s0[i]='1';
	}

	int cntS=0;
	int cntT=0;
	for(int i=0; i<n; i++) {
		cntS+=s[i]-'0';
		cntT+=t[i]-'0';
	}

	//cerr<<"s="<<s<<" cntS="<<cntS<<" n-cntS="<<n-cntS<<endl;
	//cerr<<"t="<<t<<" cntT="<<cntT<<"   cntT="<<cntT<<endl;
	int ans=INF;
	if(cntS==cntT) {
		int lans=0;
		for(int i=0; i<n; i++) 
			if(s[i]!=t[i])
				lans++;
		ans=min(ans, lans);
	} 
	if(n-cntS+1==cntT) {
		int lans=0;
		for(int i=0; i<n; i++) 
			if(s[i]==t[i])
				lans++;
		ans=min(ans, lans);
	}

	if(ans==INF) 
		ans=-1;
	cout<<ans<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
