/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We go a tree with ~some~ weighted edges, a paths weight
 * is defined to be the XOR of the edges weights.
 *
 * Then we got a set of paths with path weights, and need 
 * to tell if there is a tree constructable so that all the
 * given paths exist.
 * Also, we need to output the tree.
 *
 * How to construct?
 * If there are paths where only 1 w is missing it is trivial.
 * So, first resolve all of them.
 *
 * Then choose an arbitray edge and assign arbitrary w=0.
 * Repeat until there is a path with only one vertex left
 * and continue with first case.
 *
 * How to implement?
 * We would like to maintain foreach path the number of unassigned edges.
 * ...to complecated :/
 */
void solve() {
}

signed main() {
    solve();
}
