/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * First we need to formular for the  area of
 * the triangle defined by 3 points.
 * ->
 *  ( a.x*(b.y-c.y) + b.x(c.y-a.y) + c.x(a.y-b.y) ) / 2 = (n*m)/k
 *
 *  ( a.x*(b.y-c.y) + b.x(c.y-a.y) + c.x(a.y-b.y) )*k = 2*n*m
 */
struct poi {
    int x;
    int y;
};

/* this TLE for big n*m and small k
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    if((2*n*m)%k!=0)
        cout<<"NO"<<endl;
    else {
        poi a;
        poi b;
        poi c;

        a.x=0;
        b.x=0;
        c.x=1;
        b.y=c.y=0;
        a.y=(2*n*m)/k;

        for(int i=2; i*i<=a.y; i++) {
            if(a.y%i==0) {
                if(a.y/i <=m && i<=n) {
                    a.y/=i;
                    c.x=i;
                    break;
                } else if(i<=m && a.y/i<=n) {
                    c.x=a.y/i;
                    a.y=i;
                    break;
                }
            }
        }

        cout<<"YES"<<endl;
        cout<<a.x<<" "<<a.y<<endl;
        cout<<b.x<<" "<<b.y<<endl;
        cout<<c.x<<" "<<c.y<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
