/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* consider leftmost subseq of all m chars, 
 * and rightmost subseq of all m chars.
 * ans=max diff in seq created from left half and right half.
 */
void solve() {
    cini(n);
    cini(m);
    cins(s);
    cins(t);
    assert(n==s.size());
    assert(m==t.size());

    /* leftmost subseq */
    int ti=0;
    vi le(m);   /* leftmost possible index of t[i] in s[] */
    for(int i=0; ti<m && i<n; i++) {
        if(t[ti]==s[i]) {
            le[ti]=i;
            ti++;
        }
    }
    assert(ti==m);

    /* rightmost subseq */
    ti=m-1;
    vi ri(m);   /* rightmost possible index of t[i] in s[] */
    for(int i=n-1; ti>=0 && i>=0; i--) {
        if(t[ti]==s[i]) {
            ri[ti]=i;
            ti--;
        }
    }
    assert(ti==-1);

    int ans=0;
    for(int i=0; i+1<m; i++) {
        ans=max(ans, abs(le[i]-ri[i+1]));
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
