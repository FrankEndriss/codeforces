/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find a row of size m
 * with at most diff in two positions to all given rows.
 * Consider first row.
 * ans array can be one of
 * 1 + m + m*(m-1) arrays differing in 0,1 or 2 positions agains a[0].
 *
 * Consider first position:
 * It is allways optimal to choose one of the given values of that col,
 * let that be k[0] values.
 * Result is k[0] subsets of rows haveing 0 or 1 diff.
 *
 * So, foreach row and col way may choose the value given as final value or not.
 * Depending on that the count of differing positions increases or not.
 *
 * How to model this as a graph?
 * Each row must not differ to each other row in more than four positions.
 * Consider
 * 221111
 * 112211
 * 111122
 * Ans=Yes because 111111 fits.
 * But:
 * 222211
 * Ans=No because 111111 fits.
 * ****
 * Consider 3 rows by 70k cols.
 * How?
 * Consider the rows differ in at most 1 position...
 * **
 * Consider the first row to be ans, then check to 
 * find contradictions. If found, check possible 
 * alternatives.
 *
 * Check if the rows differing in 0,1,2,3 or 4 positions between row 0 
 * and row j.
 * if more than 0 then ans=no.
 * if 0 then ignore row j.
 * if 1 then 2 cases, 1, 0
 * if 2 then 4 cases, 11, 10, 01, 00
 * if 3 then 6 cases, 110, 101, 011, 100, 010, 001, 
 * if 4 then 6 cases, 1100, 1010, 1001, 0110, 0101, 0011
 * Now consider a[j+1] and the up to 6 cases above.
 * Foreach index there are 3 cases:
 * a[j+1][k]==a[0][k]; a[j+1][k]==a[i][k]; otherwise
 * ...
 */
void solve() {
    cini(n);
    cini(m);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) 
            cin>>a[i][j];

    vi ans=a[0];
    for(int i=1; i<n; i++) {

    }

}

signed main() {
    solve();
}
