/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to put the smallest deck first,
 * so whenever p[] is increasing it is
 * better to split, else it is better to take.
 *
 * Foreach number we use all number up to the next higher number.
 */
void solve() {
    cini(n);
    cinai(p,n);

    vi ans;
    stack<int> aux;
    int prev=-1;
    for(int i=0; i<n; i++) {
        if(p[i]>prev) {
            while(aux.size()) {
                ans.push_back(aux.top());
                aux.pop();
            }
            prev=p[i];
        }
        aux.push(p[i]);
    }

    while(aux.size()) {
        ans.push_back(aux.top());
        aux.pop();
    }

    reverse(all(ans));
    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
