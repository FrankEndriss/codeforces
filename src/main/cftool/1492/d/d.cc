/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Ok, finally I have found the most idiotic problems of all.
 * There is really so much _nothing_ interesting in it...sic.
 *
 * Solution copied from https://codeforces.com/contest/1492/submission/108444695
 */
void solve() {
    int a,b,k;
    cin>>a>>b>>k;
    int t=a+b-1;
    string s;
    for(int i=0; i<b; ++i)
        s+='1';
    for(int i=0; i<a; ++i)
        s+='0';
    while((s[t-k]=='0'||s[t]=='1')&&t-k>=0)
        t--;
    if(t-k<=0&&k)
        cout<<"NO"<<endl;
    else {
        cout<<"YES"<<endl<<s<<endl;
        swap(s[t],s[t-k]);
        cout<<s<<endl;
    }
}

signed main() {
    solve();
}
