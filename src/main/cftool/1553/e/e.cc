
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* So, m<=n/3, which means at least n-m*2 of the p[i] are in 
 * the unswapped positions.
 *
 * So find foreach p[i] the k if it was unswapped
 * Consider the freq of these k values.
 * We can use each set of n-2*m positions as the unswapped ones.
 *
 * Why this does not work, WA???
 *
 * -> We need to check if it can be done with m swaps,
 *    so need to find the min possible number of swaps.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(p,n);

    map<int,int> f;
    for(int i=0; i<n; i++) {
        int k=i+1-p[i];
        if(k<0)
            k+=n;
        f[k]++;
    }

    vi ans;
    for(auto ent : f) {
        if(ent.second>=n-2*m) {
                ans.push_back(ent.first);
        }
    }
    sort(all(ans));

    cout<<ans.size()<<" ";
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
