
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to consider two cases:
 * 1: Team 1 allways hits and Team 2 never
 * 2: Team 1 never hits and Team 2 allways
 *
 * But we have to check at each position if we finished.
 * So, after each kick there is 
 * -a max number of goals each team can reach
 * -a min number of goals each team can reach
 */
const int INF=1e9;
void solve() {
    cins(s);

    int t1max=0;
    int t1min=0;
    int t2max=0;
    int t2min=0;
    for(int i=0; i<s.size(); i++) {
        if(i%2==0) {
            if(s[i]=='1') {
                t1max++;
                t1min++;
            } else if(s[i]=='?') 
                t1max++;
        } else if(i%2==1) {
            if(s[i]=='1') {
                t2max++;
                t2min++;
            } else if(s[i]=='?')
                t2max++;

        }

        int t1all=t1min;
        int idx=i+1;
        if(i%2==0)
            idx++;
        for(; idx<s.size(); idx+=2) {
        //    if(s[idx]=='1' || s[idx]=='?')
                t1all++;
        }

        int t2all=t2min;
        idx=i+1;
        if(i%2==1)
            idx++;
        for(; idx<s.size(); idx+=2) {
         //   if(s[idx]=='1' || s[idx]=='?')
                t2all++;
        }

        if(t1max>t2all || t2max>t1all) {
            cout<<i+1<<endl;
            return;
        }
    }
    cout<<s.size()<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
