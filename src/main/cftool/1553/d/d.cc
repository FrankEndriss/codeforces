
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Typing backspace removes two consecutive chars.
 * So, we can remove a prefix from s, and several
 * adj pairs of chars.
 *
 * Just check all corresponding pairs of possible indexes.
 * But that O(n^2)
 *
 * t must be a subseq of s, with all distances of positions
 * beeing even, except the first one.
 * So check all subseqs?
 *
 * Each char in t[] has a min and a max position in s[]
 * Consider the min/max even positions,
 * and the min/max odd positions
 * And from those only the min.
 */
void solve() {
    cins(s);
    cins(t);
    //cerr<<"s="<<s<<" t="<<t<<endl;

    vi miEve;   /* miEve[i]=min even position of t[i] in s[] */

    int idxS=0;
    for(int i=0; i<t.size(); i++) {
        while(idxS<s.size() && s[idxS]!=t[i]) {
            idxS+=2;
        }
        if(idxS<s.size() && s[idxS]==t[i]) {
            miEve.push_back(idxS);
            idxS++;
        }
    }

    //cerr<<"miEve.size()=="<<miEve.size()<<endl;
    if(miEve.size()==t.size()) {
        bool ok=true;
        for(int i=1; i<miEve.size(); i++) {
            if((miEve[i]-miEve[i-1])%2!=1)
                ok=false;
        }
        if(ok) {
            cout<<"YES"<<endl;
            return;
        }
    }

    vi miOdd;   /* miOdd[i]=min odd  position of t[i] in s[] */
    idxS=1;
    for(int i=0; idxS<s.size() && i<t.size(); i++) {
        while(idxS<s.size() && s[idxS]!=t[i]) {
            idxS+=2;
        }
        if(idxS<s.size() && s[idxS]==t[i]) {
            miOdd.push_back(idxS);
            idxS++;
        }
    }

    //cerr<<"miOdd.size()=="<<miOdd.size()<<endl;
    if(miOdd.size()==t.size()) {
        bool ok=true;
        for(int i=1; i<miOdd.size(); i++) {
            if((miOdd[i]-miOdd[i-1])%2!=1)
                ok=false;
        }
        if(ok) {
            cout<<"YES"<<endl;
            return;
        }
    }

    cout<<"NO"<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
