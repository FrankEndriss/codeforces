
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * move it right some times, then
 * move it left some times.
 *
 * oh my...what wiered implementation :/
 * We got three operations, and foreach one several
 * possible starting positions.
 * So just implement.
 *
 * ...Why it does not work?
 * Ok, use state machine.
 */
void solve() {
    cins(s);
    cins(t);

    using t3=tuple<int,int,int>;

    set<t3> state;

    for(int i=0; i<s.size(); i++) { /* start in s */
        if(s[i]==t[0]) {
            state.emplace(i,0,1);
            state.emplace(i,0,2);
            if(t.size()==1) {
                cout<<"YES"<<endl;
                return;
            }
        }
    }

    while(state.size()) {
        auto it=state.begin();
        auto [si,ti,op]=*it;
        state.erase(it);

        if(ti+1==t.size()) {
            cout<<"YES"<<endl;
            return;
        }

        if(op==1) {
            if(si+1<s.size() && s[si+1]==t[ti+1]) {
                state.emplace(si+1, ti+1, 1);
                state.emplace(si+1, ti+1, 2);
            }
        } else if(op==2) {
            if(si-1>=0 && s[si-1]==t[ti+1]) {
                state.emplace(si-1, ti+1, 2);
            }
        }
    }

    cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
