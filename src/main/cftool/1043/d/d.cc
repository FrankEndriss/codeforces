/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Mayby all arrays are same. Then we can remove any prefix with any suffix, basically
 * make every substring possible.
 * n+2*(n-1)+3*(n-2)... n*1
 * To much to enumerate.
 *
 * Note that for every subarray in a[0] there is at most one possible way to make 
 * all others same as this one, since all are permutations.
 * So we need to find the number of subarrays in a[0] which are present in all other
 * a[j], too.
 * Note that for a subarray of len k we can calc the number of included subarrays
 * of len less than k.
 *
 * So we need to find the longest subarray starting at each position in a[0], but can
 * increase that position by the found length.
 */
void solve() {
    cini(n);
    cini(m);
    vvi a(m, vi(n));
    vvi pos(m, vi(n));
    for(int i=0; i<m; i++) {
        for(int j=0; j<n; j++) {
            cin>>a[i][j];
            a[i][j]--;
            pos[i][a[i][j]]=j;
        }
    }

    int ans=0;
    for(int i=0; i<n; ) {
        vi match(m);
        for(int j=1; j<m; j++) 
            match[j]=pos[j][a[0][i]];

        int sz=1;
        while(i+sz<n) {
            bool ok=true;
            for(int j=1; ok && j<m; j++) {
                if(pos[j][a[0][i+sz]]!=match[j]+1)
                    ok=false;
                else
                    match[j]++;
            }
            if(!ok)
                break;

            sz++;
        }

        ans+=sz*(sz+1)/2;
        i+=sz;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
