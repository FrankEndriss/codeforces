/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


template<typename E>
E rsq_add(E e1, E e2) {
    return e1+e2;
}

template<typename E>
E rsq_neg(E e) {
    return -e;
}

template<typename E>
struct RSQ {
    vector<E> p;
    function<E(E,E)> cummul;
    function<E(E)> neg;

    template<typename Iterator>
    RSQ(Iterator beg, Iterator end, function<E(E,E)> pCummul=rsq_add<E>, function<E(E)> pNeg=rsq_neg<E>)
        : cummul(pCummul), neg(pNeg) {
        p.push_back(*beg);
        beg++;
        while(beg!=end) {
            p.push_back(pCummul(p.back(), *beg));
            beg++;
        }
    }

    /* inclusive l, exclusive r */
    E get(int l, int r) {
        r=min((int)p.size(), r);
        l=max(0LL, l);
        if(r<=l)
            return cummul(p[0], neg(p[0])); // neutral

        E ans=p[r-1];
        if(l>0LL)
            ans=cummul(ans, neg(p[l-1]));
        return ans;
    }
};

/* Every person is in a team once
 * with each person not in his negative list,
 * and those persons not in their negative list.
 * Note: We sum the scores of the teams, not the
 * scores of the individuals.
 *
 * Let xyd=x[i]-y[i]
 * All other pairs <i,j> fall in one group of
 * xyd[i]]  < xyd[j] -> cntJ
 * xyd[i]] >= xyd[j] -> cntI
 *
 * ...
 * Maintain RSQ of pair<x[i],y[i]> in order of xyd[]
 * Then each person at position i gets sum(0,i).x + sum(i+1, n).y
 * Minus all people in dislike list, each one categorized for being index
 * j>i or j<i.
 */
void solve() {
    cini(n);
    cini(m);
    vi x(n);
    vi y(n);
    vector<pii> xyd(n);
    for(int i=0; i<n; i++) {
        cin>>x[i]>>y[i];
        xyd[i]={x[i]-y[i], i};
    }
    sort(all(xyd));

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;

        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vi sx(n);
    vi sy(n);
    vi pos(n);
    for(int i=0; i<n; i++) {
        sx[i]=x[xyd[i].second];
        sy[i]=y[xyd[i].second];
        pos[xyd[i].second]=i;
    }

    RSQ<int> rsqx(all(sx));
    RSQ<int> rsqy(all(sy));

    vi vans(n);
    for(int i=0; i<n; i++) {
        int ans=rsqx.get(0,i) + rsqy.get(i+1,n) + i*y[xyd[i].second] + (n-1-i)*x[xyd[i].second];

        for(int oth : adj[xyd[i].second]) {
            if(pos[oth]<i) {
                ans-=x[oth];
                ans-=y[xyd[i].second];
            } else {
                ans-=y[oth];
                ans-=x[xyd[i].second];
            }
        }
        vans[xyd[i].second]=ans;
    }
    for(int i : vans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
