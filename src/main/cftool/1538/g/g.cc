/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* ternary search, see https://en.wikipedia.org/wiki/Ternary_search */
/* return the point in [l,r] where f(x) is max */
int terns(int l, int r, function<int(int)> f) {
    while(l+2<r) {
        const int l3=l+(r-l)/3;
        const int r3=r-(r-l)/3;
        if(f(l3)<f(r3))
            l=l3;
        else
            r=r3;
    }
    vector<pii> v={ 
        { f(l), l },
        { f(l+1), l+1 },
        { f(r), r }
    };
    sort(all(v));
    return v.back().second;
};

/* We got 
 * s1 sets of a blue and b red, and
 * s2 sets of a red and b blue
 *
 * So
 * s1*a + s2*b <=x
 * s1*b + s2*a <=y
 * We want to find max s1+s2
 *
 * So, foreach s1 we can calc maximum s2.
 * Looks like ternary search.
 */
const int INF=1e12;
void solve() {
    cini(x);
    cini(y);
    cini(a);
    cini(b);

    function<int(int)> f=[&](int s1) {
        int x1=x-s1*a;
        int y1=y-s1*b;
        if(x1<0 || y1<0)
            return 0LL;

        int s2=min(x1/b, y1/a);
        return s1+s2;
    };

    int s1=terns(0,INF,f);
    int ans=f(s1);
    ans=max(ans, f(s1-1));
    ans=max(ans, f(s1+1));
    ans=max(ans, f(s1+2));
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
