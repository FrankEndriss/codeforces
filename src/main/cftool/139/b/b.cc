/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* for every room separate, find cheapest
 * wallpaper.
 * Since O(n*m) is ok we can calc all possible prices
 * and choose smallest ones.
 */
void solve() {
    cini(n);
    vi ra(n), rb(n), rc(n);
    for(int i=0; i<n; i++)
        cin>>ra[i]>>rb[i]>>rc[i];

    cini(m);
    vi pl(m), pw(m), pp(m);
    for(int i=0; i<m; i++)
        cin>>pl[i]>>pw[i]>>pp[i];


/* According to the rules there is only one way
 * to put the paper to the walls. This is putting
 * a strip from top to down, then the second next to it
 * and so on until all the walls are done.
 */
    int ans=0;
    for(int i=0; i<n; i++) {
        int lans=1e18;
        int w=2*ra[i]+2*rb[i];
        for(int j=0; j<m; j++) {
            if(pl[j]<rc[i])
                continue;

            int piecePerRol=pl[j]/rc[i];
            int needRol=(w+pw[j]*piecePerRol-1)/(pw[j]*piecePerRol);
//cerr<<"j="<<j<<" piecePerRol="<<piecePerRol<<" needRol="<<needRol<<" pp="<<pp[j]<<endl;
            lans=min(lans, needRol*pp[j]);
        }
        ans+=lans;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
