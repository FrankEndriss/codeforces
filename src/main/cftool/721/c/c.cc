/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int N=5003;
int enc(int i,int j) {
    return i*N+j;
}

pair<short,short> dec(int e) {
    return { (short)(e/N), (short)(e%N) };
}

/* Looks like dp/dijkstra.
 * dp[i][j]=<min time at vertex i having seen j places,backpointer>
 * The backpointer makes the path restorable,
 * and is coded as i*N+j
 * With this, there are N*N*8 bytes storage, ~200M
 * Additionally the prio q.
 */
const int INF=1e9+7;
void solve() {
    cini(n);
    cini(m);
    cini(T);

    vector<vector<pair<short,int>>> adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        cini(t);
        adj[u].emplace_back(v,t);
    }

    vector<vector<pair<int,short>>> dp(n, vector<pair<int,short>>(n+1, {INF,-1}));

    priority_queue<pii> q;
    q.emplace(0,enc(0,1));
    dp[0][1]={0,-1};
    while(q.size()) {
        auto [t,e]=q.top();
        q.pop();
        auto [v,cnt]=dec(e);
        //cerr<<"v="<<v<<" cnt="<<cnt<<" e="<<e<<endl;

        if(dp[v][cnt].first!=-t)
            continue;

        for(pii p : adj[v]) {
            const int nval=dp[v][cnt].first+p.second;
            if(nval<=T && nval < dp[p.first][cnt+1].first) {
                dp[p.first][cnt+1].first=nval;
                dp[p.first][cnt+1].second=v;
                q.emplace(-nval, enc(p.first, cnt+1));
            }
        };
    }

    vi ans;
    for(short cnt=n; cnt>=1; cnt--) {
        if(dp[n-1][cnt].first<=T)  {
            short v=n-1;
            do {
                ans.push_back(v);
                v=dp[v][cnt].second;
                cnt--;
            }while(v>=0);

            break;
        }
    }
    reverse(all(ans));
    cout<<ans.size()<<endl;
    for(int i : ans) 
        cout<<i+1<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
