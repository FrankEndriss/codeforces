/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * board n*n
 *
 * We do not need to move rooks that are already on the diagonal.
 * With each move we can move any rook to the empty col/row.
 *
 * if(there is a diagonal field with no rook on it and 
 * a free row or col, but not both exists, then 
 * we can move that rook to the diagonal field.
 *
 * What a horrible fiddling... we need a clear strategy.
 * Is there a formular?
 * We should be able to move a rook to a digonal field
 * if there is a free col one in a row with a rook.
 *
 * So foreach move we want to find a 
 * -unfinished dia field
 * -with a free x and 
 * -unfree y 
 * -or with a free y and unfree x.
 *
 * Rook can be organinzed by row, since row is distinct.
 * We allways want to move a rook to a diagonal field if 
 * possible.
 * Else we want to move any root from a not diagonal field
 * to the free row.
 * We can allways move each rock in any direction until we finish on a 
 * free row/col.
 *
 * So keep track of 
 * -unfinished diagonal fields 
 * -and rooks
 * -and free rows/cols
 */
void solve() {
    cini(n);
    cini(m);
    set<pii> xy(m); /* position of the rooks */
    set<int> freex;
    set<int> freey;
    set<int> diaunfini;  /* not finished diagonal fields */

    for(int i=1; i<=n; i++) {
        freex.insert(i);
        freey.insert(i);
        dianfini.insert(i);
    }

    for(int i=0; i<m; i++)  {
        cini(x);
        cini(y);
        xy.insert({x,y});

        freex.erase(x);
        freey.erase(y);
        if(xy[i].first==xy[i].second)
            dianfini.erase(xy[i].first);
    }
    assert(freex.size()>0);
    assert(freey.size()>0);

    bool fini=false;
    int ans=0;
    while(!fini) {
        /* find a freex not in freey but in dianfini */
        vi res;
        set_intersection(all(freex), all(diaunfini), back_inserter(res));
        for(int i : res) {
            if(freey.count(i)==0) {
                /* move the rook x/somecol to x/x occupying that diago field */
                auto it=xy.lower_bound({i, 0});
                assert(it!=xy.end());
                xy.erase(it);   /* we do not want to move that rook again, so forget it */
                diaunfini.erase(i);
                freex(...)
            }
        }



        set_difference(all(freex), all(freey), back_inserter(res));
        if(res.size()==0)
            set_difference(all(freey), all(freex), back_inserter(res));
        if(res.size()==0) { 
            /* move the first rook not on a diagonal field */
            int dia=*dianfini.begin();
            auto it=xy.lower_bound({dia, 0});
            assert(it!=freex.end());

            // TODO...

        }
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
