/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * there are cnt(x) * cnt(y) contributions.
 * it seems allways optimal to put all 0/1
 * to left/right, and the other to the other 
 * side.
 * then check all possible combinations.
 * How to implement?
 * 
 * At each position there is a number of 0 left and 1 left
 * and 0 right, and 1 right.
 * At each ? we assume once
 * * all ? left are replaced by 0 and left by 1
 * * all ? left are replaced by 1 and right by 0
 *
 * We go from left to right, mainting two sums.
 * s0=sum of all subseqs left if all ?==0
 * s1=sum of all subseqs left if all ?==1
 * We store both such sums.
 * Then do the same from right to left.
 * Then go from left to right again, and
 * sum the numbers, finding the min overall sum.
 */
const int INF=1e18;
void solve() {
    cins(s);
    cini(x);
    cini(y);
    const int n=s.size();

    vvi le(3, vi(s.size()));   /* le[i][j]=number of i up to position j. ?==2 */
    vvi ri(3, vi(s.size()));   /* ri[i][j]=number of i right of position j. ?==2 */

    if(s[0]=='0')
        le[0][0]=1;
    else if(s[0]=='1')
        le[1][0]=1;
    else
        le[2][0]=1;

    for(int i=1; i<n; i++) {
        le[0][i]=le[0][i-1];
        le[1][i]=le[1][i-1];
        le[2][i]=le[2][i-1];

        if(s[i]=='0')
            le[0][i]++;
        else if(s[i]=='1')
            le[1][i]++;
        else
            le[2][i]++;
    }

    /* no init for ri */
    for(int i=n-2; i>=0; i--) {
        ri[0][i]=ri[0][i+1];
        ri[1][i]=ri[1][i+1];
        ri[2][i]=ri[2][i+1];

        if(s[i+1]=='0')
            ri[0][i]++;
        else if(s[i+1]=='1')
            ri[1][i]++;
        else
            ri[2][i]++;
    }

    vvi sumleft(2, vi(n));  /*  sumleft[0][i]=sum of all subseqs up to pos i if all ? are 0
                            sumleft[1][i]=sum of all subseqs up to pos i if all ? are 1
    */
    vvi sumright(2, vi(n)); /* same but from right to left */
    for(int i=1; i<n; i++) {
        sumleft[0][i]=sumleft[0][i-1];
        if(s[i]=='0' || s[i]=='?')
            sumleft[0][i]+=le[1][i-1]*y;
        else if(s[i]=='1')
            sumleft[0][i]+=(le[0][i-1]+le[2][i-1])*x;

        sumleft[1][i]=sumleft[1][i-1];
        if(s[i]=='0')
            sumleft[1][i]+=(le[1][i-1]+le[2][i-1])*y;
        else if(s[i]=='1' || s[i]=='?')
            sumleft[1][i]+=(le[0][i-1])*x;

        /* right */
        sumright[0][n-1-i]=sumright[0][n-i];
        if(s[n-i]=='0' || s[n-i]=='?')
            sumright[0][n-1-i]+=ri[1][n-i]*x;
        else if(s[n-i]=='1')
            sumright[0][n-1-i]+=(ri[0][n-i]+ri[2][n-i])*y;

        sumright[1][n-1-i]=sumright[1][n-i];
        if(s[n-i]=='1' || s[n-i]=='?')
            sumright[1][n-1-i]+=ri[0][n-i]*y;
        else if(s[n-i]=='0')
            sumright[1][n-1-i]+=(ri[1][n-i]+ri[2][n-i])*x;
    }

    /* Now sum both up
     * */
    int ans=INF;
    for(int i=0; i<n; i++) {
        /* assume all ? left == 0 and all ? right == 1 */
        int a1=(le[0][i]+le[2][i])*(ri[1][i]+ri[2][i])*x + le[1][i]*ri[0][i]*y + sumleft[0][i] + sumright[1][i];
        ans=min(ans, a1);

        /* assume all ? left == 1 and all ? right == 0 */
        int a2=(le[1][i]+le[2][i])*(ri[0][i]+ri[2][i])*y + le[0][i]*ri[1][i]*x + sumleft[1][i] + sumright[0][i];
        ans=min(ans, a2);
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
