/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Understand the statement:
 * We want as much members as possible invited, 
 * but also must have an even number of pairs.
 *
 * Ex 2:
 * pair 1 3,
 * Invited: 2,3, so 1 not invited
 * ...
 * Actually no idea how to do this???
 * Brute force would be to check all 2^n combinations.
 *
 * Ok, consider two independend pairs, and invite both. Then there are 4 less unhappy
 * persons.
 * Also invite all persons not in any pair. That does not change the number of pairs
 * at all.
 * Then invite from all pairs where only 1 person is invited, the other person...so
 * that the number of pairs stays even. That is, one person that is member of an
 * even number of uninvited pairs, or two persons that are (together) members of
 * odd number of teams.
 * To find them, choose all even, and prio queue the odd.
 * What about the not independent pairs?
 *
 * And then? Is this all? Why?
 * ****
 * Ok, wrong idea. Instead, lets invite all, then try to uninvite the
 * cheapest person.
 * That is:
 * -if number of pairs is even, none. Else number of pairs is odd:
 * -The cheapest person member of odd number of pairs, or
 * -The cheapest pair of persons in any pair, since they both have
 *  even number of pairs, but one together, so sum of pairs of both is odd.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);

    vi cnt(n);
    vector<pii> p;
    for(int i=0; i<m; i++) {
        cini(x); x--;
        cini(y); y--;
        cnt[x]++;
        cnt[y]++;
        p.emplace_back(x,y);
    }

    if(m%2==0) {
        cout<<"0\n";
        return;
    }

    vi odd;
    for(int i=0; i<n; i++) 
        if(cnt[i]&1)
            odd.push_back(i);

    sort(all(odd), [&](int i1, int i2) {
            return a[i1]<a[i2];
    });

    int ans=INF;
    if(odd.size())
        ans=a[odd[0]];

    for(int i=0; i<m; i++) {
        if(cnt[p[i].first]%2==0 && cnt[p[i].second]%2==0)
            ans=min(ans, a[p[i].first]+a[p[i].second]);
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
