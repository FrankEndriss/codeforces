/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * ToroidalForces
 * What shapes can be created?
 *
 * xxx
 * xxx
 *
 * Is there a way to color a cell without coloring the whole row (or col)?
 * -> No, since the outermost cell (on the corner of the filled rect)
 *  would have only two same col neigbours.
 *  I guess.
 * So all a[i] must be divideable by n or all by m.
 *
 * ...Note that sum(a[]) can be bg n*m, so we use from each color as max as 
 * possible.
 *
 * Also note that there must be at least 2 consecutive rows with same color.
 *
 * Also note that total number of rows maybe not fit, see example 2.
 * How to check this?
 * -> From each color we have a min and a max number of rows.
 *      Since the min number is allways 2, we might end up with an even sum.
 *      If n (or m) is odd, then we need to check if there is at least one
 *      color good for 3 rows.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    cinai(a,k);

    int ans1=0;
    int a1_3=false;
    int ans2=0;
    int a2_3=false;
    for(int i=0; i<k; i++) {
        int cnt=a[i]/n;
        if(cnt>1)
            ans1+=cnt*n;
        if(cnt>2)
            a1_3=true;
        cnt=a[i]/m;
        if(cnt>1)
            ans2+=cnt*m;
        if(cnt>2)
            a2_3=true;
    }

    //cerr<<"n="<<n<<" m="<<m<<endl;
    //cerr<<"ans1="<<ans1<<" ans2="<<ans2<<endl;
    //cerr<<"a1_3="<<a1_3<<" a2_3="<<a2_3<<endl;
    bool ans=false;
    if(ans1>=n*m && (m%2==0 || a1_3)) 
        ans=true;

    if(ans2>=n*m && (n%2==0 || a2_3)) 
        ans=true;

    if(ans)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
