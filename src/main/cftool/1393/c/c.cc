/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* binary search 
 * how to check one number?
 */
void solve() {
    cini(n);

    vi f(n+1);
    int ma=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux]++;
        ma=max(ma, f[aux]);
    }
    sort(all(f), greater<int>());

    /* return true if dist==x is possible */
    function<bool(int)> posi=[&](int x) {
        int mi=0;
        for(int i=0; i<n; i++)  {
            mi=max(mi, i+(f[i]-1)*(x+1)+1);
            if(mi>n)
                break;
        }
        return mi<=n;
    };

    int l=0;
    int r=n;

    while(l+1<r) {
        int mid=(l+r)/2;
        if(posi(mid))
            l=mid;
        else
            r=mid;
    }
    cout<<l<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
