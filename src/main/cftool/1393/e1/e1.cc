/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We can remove any one letter from first string
 * and check how many possibilities exist to 
 * create the other strings.
 * Repeat.
 */
void solve() {
    cini(n);
    cinas(s,n);


    vvi dp(n);
    for(int i=0; i<n; i++)  {
        dp[i].resize(s[i].size()+1);
    }

    /* number of possibilities to choose string starting at i+1
     * if Remth char was removed from  string i.
     * iRem==s[i].size();
     */
    function<int(int,int)> posi=[&](int i, int iRem) {
        if(i+1==n)
            return 1;

        /* if a prefix without removal is bigger that s[i]
         * then we can remove any position later in the string */
        for(int jRem=0; jRem<=s[i+1].size(); jRem++) {
            bool bigger=true;
            for(int j=0; j<s[i+1].size(); j++) {
                int idxPrev=j;
                if(j>=iRem)
                    idxPref++;

                if(idxPref>=s[i].size())
                    break;

                int idxNext=j;
                if(j>=jRem)
                    idxNext++;

                if(idxNext>s[i+1].size())
                    break;

                char c1=s[i][idxPref];
                char c2=s[i+1][idxNext];
                if(c1...
            }
        }
    };


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
