/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Maintain three biggest frequencies.
 *
 * Implement using a map and multiset, which does
 * not look really nice :/
 * **************
 * More correct sol is to use set<pii> as <freq,num>
 * and maintain it for all numbers.
 * So we get rid of the frequency as map, which is then
 * an array.
 * **************
 * Still more correct is to use a multiset instead
 * of the set<pii>, but still use freq as array.
 * And second, do not sort ans(3), but write one
 * single if statement.
 * **************
 * We can get rid to the multiset, too.
 * Maintain the freqs not as a set, but as kind of a 
 * prefix array.
 * f[i]=frequency of i
 * ff[j]=number of numbers with f[i]>=j
 * Then we can directly access ff[8], ff[6] etc
 */
const int N=1e5+5;
void solve() {
    cini(n);
    cinai(a,n);

    vi f(N);
    vi ff(N);
    for(int i=0; i<n; i++) {
        f[a[i]]++;
        ff[f[a[i]]]++;
    }

    cini(q);
    for(int i=0; i<q; i++) {
        char c;
        int ai;
        cin>>c>>ai;
        if(c=='-') {
            ff[f[ai]]--;
            f[ai]--;
        } else if(c=='+') {
            f[ai]++;
            ff[f[ai]]++;
        } else assert(false);

        if(ff[8]>=1 || (ff[6]>=1 && ff[2]>=2) || (ff[4]>=1 && ff[2]>=3) || (ff[4]>=2))
            cout<<"YES"<<"\n";
        else
            cout<<"NO"<<"\n";
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
