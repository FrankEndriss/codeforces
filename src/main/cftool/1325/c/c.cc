/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* we need to place 0 and 1 and 2 at a leafs edge, then max(MEX)==1 */
void solve() {
    cini(n);
    vector<pii> edges;
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);    
        cini(v);
        u--; v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
        edges.push_back({u,v});
    }

    vi leafs;
    for(int i=0; i<n; i++) {
        if(adj[i].size()==1)
            leafs.push_back(i);
    }

    /* find two leafs with not same parent */
    int l1=-1;
    int l2=-1;
    bool done=false;
    for(size_t i=0; !done && i<leafs.size(); i++) {
        if(i>0 && adj[leafs[i-1]][0]==adj[leafs[i]][0])
            continue;

        for(size_t j=i+1; !done && j<leafs.size(); j++) {
            if(adj[leafs[i]][0]!=adj[leafs[j]][0]) {
                l1=leafs[i];
                l2=leafs[j];
                done=true;
            }
        }
    }

    if(l1<0) {
        for(int i=0; i<edges.size(); i++) 
            cout<<i<<endl;
        return;
    }

    int l3=-1;
    for(int i=0; i<leafs.size(); i++) {
        if(leafs[i]!=l1 && leafs[i]!=l2) {
            l3=leafs[i];
            break;
        }
    }


    int label=3;
    if(l3<0)
        label=2;

    for(pii e : edges) {
        if(e.first==l1 || e.second==l1) 
            cout<<0<<endl;
        else if(e.first==l2 || e.second==l2)
            cout<<1<<endl;
        else if(e.first==l3 || e.second==l3)
            cout<<2<<endl;
        else {
            cout<<label<<endl;
            label++;
        }
    }
    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

