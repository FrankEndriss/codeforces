/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* In x,y going D adds x+y
 * In x,y going R adds x+y-1
 * It does not matter where we start.
 * xd times R
 * yd times D
 *
 * We allways add k or k+1
 * Let steps=xd+yd, ie number of steps
 * Adding k+1 in ith step, contributes the 1 steps-i times
 * Doing it early as possible contributes
 * steps + steps-1 + ... +steps-xd
 * late as possible
 * 1+2+...+xd
 *
 */
void solve() {
    cini(x1);
    cini(y1);
    cini(x2);
    cini(y2);

    int xd=x2-x1;
    int yd=y2-y1;

    int mi=xd*(xd+1)/2;
    int ma=(xd+yd)*(xd+yd+1)/2 - yd*(yd+1)/2;

    cout<<ma-mi+1<<endl;
}

signed main() {
    cini(t);
    while(t--)
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
