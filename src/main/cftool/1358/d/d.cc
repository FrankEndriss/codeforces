/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to find the biggest possible 
 * sum of x consecutive numbers in an compressed
 * array.
 * x limited to 1e5
 * Two pointer?
 *
 * The leftmost possible sement starts at 1 of first month,
 * and ends at d[m][k1]
 * Moving it one position right removes the 1 of the begin, and adds k1+1, so k1 is added
 * so moving it to the last of d[m] increases the number.
 * Then, instantly d[m]-1 hugs are removed.
 *
 * Start with leftmost x and move it from position to position where months start and end.
 */
void solve() {
    cini(n);
    cini(x);
    cinai(d,n);
    d.resize(n*2)
    for(int i=0; i<n; i++)
        d[i+n]=d[i];

    vi pre(d.size()+1);
    for(int i=0; i<n; i++)
        pre[i+1]=pre[i]+d[i];

    int d0=0;   /* month where x begins */
    int d1=0;   /* month after where x ends */
    int sum=0;  /* current sum of days in full months */
    int sumH=0; /* sum hugs in current interval */

    for(int i=0; sum<x && i<n; i++) {
        sum+=d[i];
        sumH+=d[i]*(d[i]+1)/2;
        d1++;
    }
    while(sum-d[d0]>=x) {
        sum-=d[d0];
        sumH-=d[d0]*(d[d0]+1)/2;
    }

    int diff=sum-x;
    sumH-=diff*(diff+1)/2;

    int ans=sumH;

/* now shift from month to month */
    while(d1<n) {
        int sum2=sum+d[d1];
        int sumH2=sumH+d[d1]*(d[d1]+1)/2;

        /* days in X in first month */
        int dd=sum-x;       /* removed days in first month */
        int ddin=d[d0]-dd;  /* remaining days in first month */

        /* now we shift by d[d1] days...*/
        /* shift only part of first month */
        if(ddin>=d[d1]) {
            int nddin=ddin-d[d1];
            int firstin=d[d0]-ddin+1;
            int nfirstin=firstin+d[d1];
            sumH2-=nfirstin*(nfirstin+1)/2 - firstin*(firstin+1)/2;
            if(nddin==0) {
                sum2-=d[d0];
                d0++;
            }
        } else {
            int nddin=ddin-d[d1];
            int firstin=d[d0]-ddin+1;

... fuck that shit :/
        }

/*
        while(sum-d[d0]>=x) {
            sum-=d[d0];
            sumH-=d[d0]*(d[d0]+1)/2;
        }
*/
        

        sum=sum2;
        sumH=sumH2;
        d1++;
    }

    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
