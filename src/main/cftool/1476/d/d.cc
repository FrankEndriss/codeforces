/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Each city has two states.
 * And each move moves to the next city in one of both states.
 * So create a graph of that, then find path with most distinct
 * vertex.
 * How to find that path?
 *
 * What about patterns?
 * RRL
 * Start at 1
 * 1->2, LLR
 * 2->1, RRL... Its a loop
 *
 * RLR, Start at 1
 * 1->2, LRL
 * 2->3, RLR
 * 3->4, LRL
 * 4->3. Loop
 * So, two same dirs make the traveler stop, else he can go
 * the longest alternating path.
 *
 * But, he can go only in the direction where the road goes the
 * first day.
 * So, we need to find foreach position the longest alternating
 * seq in the directions of the initial roads.
 *
 * ...somehow :/
 */
void solve() {
    cini(n);
    cins(s);

    vector<pii> alt;    /* first and last index of alternating segments */
    alt.push_back({0,0});
    for(int i=1; i<n; i++) {
        if(s[i]!=s[i-1]) 
            alt.back().second=i;
        else  {
            alt.back().second=i-1;
            alt.push_back({i,-1});
        }
    }
    alt.back().second=n-1;

    vi ans(n+1, 1);
    int altIdx=0;
    for(int i=0; i<=n; i++) {
        while(alt[altIdx].second<i-1)
            altIdx++;

        bool L=i>0 && s[i-1]=='L';
        bool R=i<n && s[i]=='R';


        if(L) 
            ans[i]=max(ans[i], i-alt[altIdx].first+1);
        if(R)
            ans[i]=max(ans[i], alt[altIdx].second-i+1);
    }

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
