/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider each of the n values fixed, and find
 * how that changes all other values.
 * Since we can only increase, not decrease, we must increase
 * to next fitting value whenever two values do not fit.
 *
 * But... to find the k% factor, we need to know the absolute
 * hight of the prefix sum :/
 * How can we go from a given p[i] to the lower values?
 *
 * Consider value p[i]. Since sum*k/100 >=p[i], sum>=p[i]*100/k
 *
 * Actually p[i] must not be to big. But since we cannot make it
 * smaller, we need to increase the sum on the day before i.
 *
 * So, starting at last p[n-1], we can calculate the min sum just before each day,
 * and from that the min increases to get that sum.
 *
 * But, that increase makes that the minSum of the previous day increases...
 *
 * So we need to start on last day, then increase one p[i] after
 * the other.
 *
 * If we change a p[i], then we need to recalculate all following sums,
 * from that all p[i].
 * Do that for all i from right to left.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(p,n);

    vi pp=p;

    vi sums(n); /* sums[i]=sum after day i */
    sums[0]=p[0];
    for(int i=1; i<n; i++)
        sums[i]=sums[i-1]+p[i];

    int ans=0;
    for(int i=n-1; i>=1; i--) {
        int minSum=(pp[i]*100+k-1)/k;
        ans=max(ans, minSum-sums[i-1]);
                }

    cout<<ans<<endl;



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
