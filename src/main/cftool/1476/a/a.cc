/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to find the number x*n = y*k
 * So, x*n>=k
 * x>=k/n
 * We need to workarround the special case
 * that n>k, cinse each element must not be smaller than 1.
 */
void solve() {
    cini(n);
    cini(k);

    if(n>k) {
        int ans=1;
        if(n%k!=0)
            ans++;
        cout<<ans<<endl;
        return;
    }
    int d=k/n;
    if(d==0)
        d++;
    int ans=d;
    if(ans*n<k)
        ans++;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
