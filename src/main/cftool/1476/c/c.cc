/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * We need to travel a segement of chains, once from left to rigth,
 * then back from right to left.
 * We cannot travel from chain[i] to chain[i+1] (and/or back) if
 * -in chain[i+1] both ends are connected to one element (like in example)
 * -then we allways travel from the two connection-points to each
 *  the nearer end of the next chain.
 * -we start at the two connection points of the first chain, and
 *  add the dist between them.
 *
 * A cylce consists of abs(a[i]-b[i]) of first chain, 
 * + (number of chains-1)*2 
 * + len of last chain
 *
 * We can start a new chain everywhere, but we must stop each chain
 * at any chain with a[i]==b[i]
 */
void solve() {
    cini(n);
    cinai(c,n);
    cinai(a,n);
    cinai(b,n);

    int ans=0;
    int lans=c[n-1];
    for(int i=n-2; i>=0; i--) {
        ans=max(ans, lans+abs(a[i+1]-b[i+1])+1);

        lans+=min(a[i+1],b[i+1]);
        lans+=c[i]-max(a[i+1], b[i+1])+1;

        if(a[i+1]==b[i+1]) {
            lans=c[i];
        }
        lans=max(lans, c[i]);
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
