/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Allways save the bigger of the two subtrees, then continue
 * with the other.
 * Find  the size of the subtrees beforehand dfs.
 *
 * ...no.
 * We can make the infection stop at first vertex with less
 * than 2 childs.
 * So number of infected vertex is the shortest path to such one.
 */
const int INF=1e9;
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=0; i+1<n; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    const int root=0;

    if(adj[root].size()==0) {
        cout<<0<<endl;
        return;
    }

    if(adj[root].size()==1) {
        cout<<n-2<<endl;
        return;
    }

    vi cnt(n);

    function<int(int,int,int)> dfs=[&](int v, int p, int d) {
        if(v!=0 && adj[v].size()==1)    /* no childs, so no childs lost */
            return d;

        if((v==0 && adj[v].size()<2) || (v!=0 && adj[v].size()<3)) /* one child lost */
            return d+1;

        int ans=INF;
        for(int chl : adj[v]) {
            if(chl!=p)
                ans=min(ans, dfs(chl,v,d+2));       /* one child lost and other infected */
        }
        return ans;
    };

    int ans=dfs(0,-1,1);
    cout<<n-ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
