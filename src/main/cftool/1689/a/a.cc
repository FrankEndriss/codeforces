/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * ???
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    cinas(ab,2);

    sort(all(ab[0]));
    reverse(all(ab[0]));
    sort(all(ab[1]));
    reverse(all(ab[1]));

    string ans;
    vi cnt(2);
    while(ab[0].size()>0 && ab[1].size()>0) {
        if(cnt[0]==k) {
            ans.push_back(ab[1].back());
            ab[1].pop_back();
            cnt[0]=0;
            cnt[1]=1;
        } else if(cnt[1]==k) {
            ans.push_back(ab[0].back());
            ab[0].pop_back();
            cnt[1]=0;
            cnt[0]=1;
        } else {
            if(ab[0].back()<ab[1].back()) {
                ans.push_back(ab[0].back());
                ab[0].pop_back();
                cnt[0]++;
                cnt[1]=0;
            } else {
                ans.push_back(ab[1].back());
                ab[1].pop_back();
                cnt[1]++;
                cnt[0]=0;
            }
        }
    }

    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
