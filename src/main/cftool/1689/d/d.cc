/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * simple multi source dfs ???
 *
 * But it is the _farthest_ black cell, not the nearest, so its otherwise.
 *
 * Consider the max distance between any two black cells, then
 * the optimal cell is in the middle between them...no.
 * ...
 * But we can start anywhere, and move one cell at a time until no
 * better result possible. But that O(n^4).
 * Can we remove all put kind of convex hull?
 * Convex hull on manhattan distance should be max 8 cells,
 * on the min/max x and y, isn't it?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    cinas(s,n);

    int minI=INF;
    int maxI=-INF;
    int minJ=INF;
    int maxJ=-INF;

    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            if(s[i][j]=='B') {
                minI=min(minI,i);
                maxI=max(maxI,i);
                minJ=min(minJ,j);
                maxJ=max(maxJ,j);
            }

    vector<pii> pminI;
    vector<pii> pmaxI;
    vector<pii> pminJ;
    vector<pii> pmaxJ;

    for(int j=0; j<m; j++)  {
        if(s[minI][j]=='B')
            pminI.emplace_back(minI,j);
        if(s[maxI][j]=='B')
            pmaxI.emplace_back(maxI,j);
    }

    for(int i=0; i<n; i++) {
        if(s[i][minJ]=='B') 
            pminJ.emplace_back(i,minJ);
        if(s[i][maxJ]=='B')
            pmaxJ.emplace_back(i,maxJ);
    }

    sort(all(pminI));
    sort(all(pmaxI));
    sort(all(pminJ));
    sort(all(pmaxJ));

    set<pii> p;
    if(pminI.size()>0) {
        p.insert(pminI[0]);
        p.insert(pminI.back());
    }
    if(pmaxI.size()>0) {
        p.insert(pmaxI[0]);
        p.insert(pmaxI.back());
    }
    if(pminJ.size()>0) {
        p.insert(pminJ[0]);
        p.insert(pminJ.back());
    }
    if(pmaxJ.size()>0) {
        p.insert(pmaxJ[0]);
        p.insert(pmaxJ.back());
    }

    /*
    cerr<<"p[]=";
    for(pii pp1 : p) 
        cerr<<pp1.first<<"/"<<pp1.second<<"  ";
    cerr<<endl;
    */

    int dist=0;
    pii ansp1;
    pii ansp2;
    for(pii pp1 : p) {
        for(pii pp2 : p) {
            int d=abs(pp1.first-pp2.first) +abs(pp1.second-pp2.second);
            if(d>dist) {
                dist=d;
                ansp1=pp1;
                ansp2=pp2;
            }
        }
    }
    //cerr<<"dist="<<dist<<" ansp1="<<ansp1.first<<"/"<<ansp1.second<<"  ansp2="<<ansp2.first<<"/"<<ansp2.second<<endl;

    int x=min(ansp1.first,ansp2.first)+abs(ansp1.first-ansp2.first)/2;
    int y=min(ansp1.second,ansp2.second)+abs(ansp1.second-ansp2.second)/2;
    cout<<x+1<<" "<<y+1<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
