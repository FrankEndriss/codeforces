/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

const S INF=1e9;
S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 * It is about number of inversions.
 * With each move of Alice the number of inversions decreases by one.
 * With each move of Bob, it 0.5 prop decrease or 0.5 prop increses.
 * Also there are max n*(n-1) inversions, but that does not matter since
 * the overall number of inversions never increases above the level at begin.
 *
 * let p[t][i]=prop that after move t there are i inversions, n initial inversions.
 * Consider the position after two moves:
 * p[2][n-2]=0.5
 * p[2][n]=0.5
 * And after 4:
 * p[4][n-4]=0.25
 * p[4][n-2]=0.5
 * p[4][n]=0.25
 * and so on....
 *
 * let E(i) be the expected number of moves if p[i]==1
 * E(i)=2 + 0.5*E(i-2) + 0.5*E(i)
 * E(i)/2 = 2 + E(i-2)/2
 * E(i)   = 4 + E(i-2)
 */
void solve() {
    cini(n);

    stree seg(n);

    int inv=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        inv+=seg.prod(aux-1,n);
        seg.set(aux-1, 1);
    }
    if(inv%2==0)
        cout<<inv*2<<endl;
    else
        cout<<(inv-1)*2+1<<endl;
}

signed main() {
        solve();
}
