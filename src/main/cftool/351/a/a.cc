/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We are asked to round up half of the numbers,
 * and round down the other half.
 * The sum of rounding diff should be min.
 *
 * Per number there are two possibilities, we want to find the 
 * sum nearest to zero.
 *
 * Inore digits before the decimal point, just use the digits
 * after the point as integers.
 * Then we can adjust by -a or 1000-a.
 * Knapsack all possible sums?
 *
 * ***
 * Obs1 Integer numbers behave different than reals.
 * Obs2 Difference in rounding up or down a real is exactly 1.0, independent of the number.
 */
void solve() {
    cini(n);
    vi a(2*n);
    int sum=0;
    int icnt=0;
    for(int i=0; i<2*n; i++) {
        cins(s);
        int idx=0;
        while(s[idx]!='.')
            idx++;
        idx++;
        a[i]=(s[idx]-'0')*100+(s[idx+1]-'0')*10+(s[idx+2]-'0');
        sum+=a[i];
        if(a[i]==0)
            icnt++;
    }

    // now we must change the up/down for n numbers
    // this will change sum by -nn*1000  where nn is the number of none
    // integers.
    
    int mi=max(0LL, n-icnt);
    int ma=min(n, 2*n-icnt);

    int ans=1e9;
    for(int i=mi; i<=ma; i++)
        ans=min(ans, abs(sum-i*1000));

    if(ans%1000==0)
        cout<<ans/1000<<".000"<<endl;
    else if(ans%1000<10) 
        cout<<ans/1000<<".00"<<ans%1000<<endl;
    else if(ans%1000<100) 
        cout<<ans/1000<<".0"<<ans%1000<<endl;
    else
        cout<<ans/1000<<"."<<ans%1000<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
