/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

string i2c(int i) {
    int k=0;
    int n26=26;
    int cnt=1;
    while(k+n26<i) {
        k+=n26;
        n26*=26;
        cnt++;
    }

    int num=i-k-1;
    string ans;
    while(cnt) {
        ans.push_back('A'+num%26);
        num/=26;
        cnt--;
    }

    reverse(all(ans));
    return ans;
}

/* A=1 ... Z=26
 * AA=27 ... AZ=52
 * AB=53 ... AZ=78
 */
int c2i(string s) {
//cerr<<"c2i, s="<<s<<endl;
    int k=1;
    int num=0;
    for(int i=1; i<s.size();  i++) { 
        k*=26;
        num+=k;
    }

    int ans=0;
    for(char c : s)
        ans=ans*26+c-'A';
    
//cerr<<"c2i, ans="<<ans+num+1<<endl;
    return ans+num+1;
}

bool isDigit(char c) {
    return c>='0' && c<='9';
}

/* We need to implement a nice parser,
 * and a calculation to and from an
 * immmediate format.
 *
 * If after the first digit there are more alphas,
 * it is format 2, else format 1.
 */
void solve() {
    cins(s);

    int format=1;
    bool seenDig=false;
    for(char c : s) {
        if(seenDig && !isDigit(c))
            format=2;

        seenDig=seenDig||isDigit(c);
    }

    if(format==1) { /* BC23 */
        string scol;
        int i=0;
        while(!isDigit(s[i])) {
            scol+=s[i];
            i++;
        }
        int row=0;
        while(i<s.size()) {
            row*=10;
            row+=s[i]-'0';
            i++;
        }
        int col=c2i(scol);
        cout<<"R"<<row<<"C"<<col<<endl;

    } else if(format==2) { /* R1C2 */
        int row=0;
        int i=1;
        while(isDigit(s[i])) {   
            row*=10;
            row+=s[i]-'0';
            i++;
        }
        i++;
        int col=0;
        while(i<s.size() && isDigit(s[i])) {   
            col*=10;
            col+=s[i]-'0';
            i++;
        }

        cout<<i2c(col)<<row<<endl;
        
    } else
        assert(false);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
