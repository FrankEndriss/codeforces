/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* ans must obviously be sorted seq.
 * So, for every block of same ab[i]
 * we can create blocksz! permutations.
 *
 * No, it is not always blocksz!
 * Because if two elements are at same
 * position in the list, the number has to
 * be devided by two.
 *
 * How to do the divisions by 2?
 */
void solve() {
    cini(n);
    cinai(ab, 2*n);

    map<int,int> f;
    for(int i : ab)
        f[i]++;

    int dup=0;
    for(int i=0; i<n; i++)
        if(ab[i]==ab[i+n])
            dup++;

    cini(MOD);

    function<int(int)> fak=[&](int nn) {
        int ans=1;
        for(int i=2; i<=nn; i++) {
            int ii=i;
            while(dup && ii%2==0) {
                dup--;
                ii/=2;
            }
            ans=(ans*ii)%MOD;
        }
        return ans;
    };

    int ans=1;
    for(auto ent : f) {
        if(ent.second==1)
            continue;

        ans=(ans*fak(ent.second))%MOD;
    }
    assert(dup==0);

    cout<<ans<<endl;
}


signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
