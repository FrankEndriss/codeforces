/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* f(0)=0
 * f(1)=1
 * f(2)=1
 * f(3)=2
 * f(4)=1
 * f(5)=2
 * f(6)=2
 * f(7)=3
 * f(8)=1
 * f(9)=2
 * f(10)=2
 * f(11)=3
 * f(12)=2
 * f(13)=3
 * f(14)=3
 * f(15)=4
 * f(16)=1
 * ...number of bits for some reason
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi f(32);
    for(int i=0; i<n; i++) {
        const int b=__builtin_popcount(a[i]);
        f[b]++;
    }

    int ans=0;
    for(int i=0; i<32; i++)
        ans+=(f[i]*(f[i]-1))/2;

    cout<<ans<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

