/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* k periodic
 * n is len of s, max 2000
 *
 * ans<=n
 * let x be a number <=n and multiple of k.
 * we can build a neck of len x if we can build
 * x/k same ones.
 * ie we need to have k times at least x/k same symbols.
 * Just brute force this.
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);

/*
sort(all(s));
cerr<<s<<endl;
*/

    vi f(26);
    for(char c : s)
        f[c-'a']++;

    for(int x=n; x>=1; x--) {
        vi ff=f;
        int kk=k;

        if(kk>=x)
            kk%=x;

        if(kk==0) {
            cout<<x<<endl;
            return;
        }

/*
        if(x%kk!=0)
            kk=1;
*/

        kk=gcd(kk, x);
        
        int kkk=kk;
        for(int i=0; i<ff.size(); i++) {
            while(ff[i]>=x/kk) {
                ff[i]-=x/kk;
                kkk--;
            }
        }
        if(kkk<=0) {
            cout<<x<<endl;
            return;
        }
    }
    cout<<1<<endl;
}

signed main() {
    cini(t);
    while(t--)
      solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
