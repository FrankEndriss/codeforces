/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* some kind of dp.
 * Of all elements we need to move to end
 * we need to start with the smallest one.
 *
 * So, we can move the biggest right, the smallest left
 * ... until all are moved to the correct end.
 * Then sort those moves by number desc and dont move the
 * ones in correct position anyway... somehow.
 *
 * What about assuming we move all smaller x to left
 * and all bigger x to right. Then simply count.
 * Should work for f1.
 * How to implement?
 *
 * There is one element we do not move at all, let it be X.
 * For all smaller than x, we need to move that ones
 * where a bigger one (but less than x) is left of it.
 * Else no move.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi aa=a;
    sort(all(aa));

    int ans=1e9;
    for(int i=0; i<n; i++) {
        int lans=0;
        int x=aa[i];

        vi maL(n, -1); /* biggest element smaller than x left of pos i */
        if(a[0]<=x)
            maL[0]=a[0];
        for(int i=1; i<n; i++) {
            maL[i]=maL[i-1];
            if(a[i]<=x)
                maL[i]=max(maL[i], a[i]);
        }

        bool moved=false;
        for(int i=0; i<n; i++) {
            if(moved || a[i]<maL[i]) {
                lans++;
                moved=true;
            }
        }

        const int INF=1e9;
        vi miR(n, INF); /* smallest element bigger than x right of pos i */
        if(a[n-1]>=x)
            miR[n-1]=a[n-1];
        for(int i=n-2; i>=0; i--) {
            miR[i]=miR[i+1];
            if(a[i]>=x)
                miR[i]=min(miR[i], a[i]);
        }

        moved=false;
        for(int i=0; i<n; i++) {
            if(moved || a[i]>miR[i]) {
                lans++;
                moved=true;
            }
        }

        ans=min(ans, lans);
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
