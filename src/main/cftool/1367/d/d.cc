/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* first we need to construct an
 * array t from b.
 * then check if we can put the symbols of
 * s into t.
 */
void solve() {
    cins(s);
    cini(m);
    cinai(b,m);

    vi a(m, -1);

    for(int sym=26; sym>=0; sym--) {
        for(int i=0; i<m; i++) {
            if(a[i]>=0)
                continue;

            int sum=0;
            for(int k=0; k<m; k++) {
                if(a[k]>sym)
                    sum+=abs(i-k);
            }

            if(b[i]==sum)
                a[i]=sym;
        }
    }

for(int i=0; i<m; i++) {
    if(a[i]<0) {
        cerr<<"err, i="<<i<<endl;
        assert(a[i]>=0);
    }
 }

    vi fs(27);
    for(char c : s) 
        fs[c-'a']++;

    vi fa(27);
    for(int i : a)
        fa[i]++;

    map<int,int> mm;
    int idxa=0;
    for(int i=0; idxa<27 && i<27; i++) {
        while(idxa<27 && fa[idxa]==0)
            idxa++;
        if(idxa==27)
            break;

        if(fa[idxa]<=fs[i]) {
            mm[idxa]=i;
            idxa++;
        }
    }

    //assert(idxa==27);
    string ans;
    for(int i=0; i<m; i++) {
        ans+=(char)(mm[a[i]]+'a');
    }
    cout<<ans<<endl;

}

signed main() {
    cini(q);
    while(q--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
