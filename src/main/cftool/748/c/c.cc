/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll

#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Greedy:
 * We cannot use D after using U in one segment.
 * We cannot use L after using R in one segment.
 * And vice versa.
 */
void solve() {
    cini(n);
    cins(s);

    vector<char> op(256);
    op['D']='U';
    op['U']='D';
    op['L']='R';
    op['R']='L';

    int ans=0;
    vb used(256);
    for(int i=0; i<n; i++) {
        if(used[op[s[i]]]) {
            ans++;
            used.assign(256, false);
            used[s[i]]=true;
        } else {
            used[s[i]]=true;
        }
    }
    cout<<ans+1<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

