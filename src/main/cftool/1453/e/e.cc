/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*  Note that the path is more or less determined because
 *  badugi allways chooses the closest snack.
 *
 *  Since it is a tree, once eaten a snack he will go 
 *  deeper into that subtree until a leaf is reached.
 *  Then he goes back to the parent of the leaf, and chooses
 *  a sibling of the leaf and again goes until he finds a leaf.
 *
 *  ...He must go from leaf to leaf somehow...
 *
 *  Assume to go into the deepest leaf first, then travel back to root
 *  visiting all leafs in that subtree. Then, at root, repeat.
 *
 *  So we need to find from one leaf the nearest next leaf, 
 *  and remove leafs (recursive) somehow.
 *  But note that N=1e5 :/
 */
void solve() {
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
