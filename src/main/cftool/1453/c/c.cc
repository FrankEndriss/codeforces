/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to find foreach 0..9 the two vertex
 * whith wich we can create the biggest area triangle.
 *
 * We check each i,j.
 * Create a parallel side by placing one vertex at max/min
 * in same row/col, and pair with max/min from all other vertex.
 *
 * O(n^2)
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinas(s,n);

    vi cnt(10);
    vi maxI(10, -INF); 
    vi minI(10,  INF); 
    vi maxJ(10, -INF);
    vi minJ(10,  INF);

    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            int d=s[i][j]-'0';
            cnt[d]++;

            maxI[d]=max(maxI[d], i);
            minI[d]=min(minI[d], i);
            maxJ[d]=max(maxJ[d], j);
            minJ[d]=min(minJ[d], j);
        }
    }

    vi ans(10);
    for(int i=0; i<n; i++)  {
        for(int j=0; j<n; j++) {
            const int d=s[i][j]-'0';
            if(cnt[d]<2)
                continue;

            int lans=0;
            lans=max(lans, max(i, n-i-1)*max(abs(j-maxJ[d]), abs((j-minJ[d]))));
            lans=max(lans, max(j, n-j-1)*max(abs(i-maxI[d]), abs((i-minI[d]))));
            ans[d]=max(ans[d], lans);
        }
    }

    for(int i=0; i<10; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--) {
        solve();
    }
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
