/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* How to calc the expected number of tries
 * for given checkpoints and stages?
 *
 * 1 stage is 1/2 * 1 + 1/4 * 2 + 1/8 * 3 + ...
 * ???
 * How can we get an integer result at all?
 * -> Note that the expected number of throws
 *  until getting head is 2.
 *  This is because
 *  x[1]=1/2 + 1/2 * (1+x[1])
 *  x[1]=2
 *  ...
 *  x[i]=x[i-1]+1/2* (1+x[i-1])
 *  x[i]=(x[i-1]+1)*2
 */
const int INF=1e18;
void solve() {
    vi kk(2);
    kk[1]=2;
    for(int i=2; i<=2000; i++) {
        int lkk=(kk.back()+1)*2;
        kk.push_back(lkk);

        if(lkk>=INF)
            break;
    }

    cini(t);
    while(t--) {
        cini(k);
        k-=2;
        vi aa(1,1);

        vi ans;
        for(int j=kk.size()-1; j>=1; j--) {
            while(k>=kk[j]) {
                k-=kk[j];
                ans.push_back(j);
            }

            if(k==0)
                break;
        }

        if(k!=0) {
            cout<<-1<<endl;
        } else {
            for(int i : ans) {
                for(int j=0; j+1<i; j++) 
                    aa.push_back(0);
                aa.push_back(1);
            }

            cout<<aa.size()<<endl;
            for(int i : aa)
                cout<<i<<" ";
            cout<<endl;
        }
    }
}

signed main() {
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
