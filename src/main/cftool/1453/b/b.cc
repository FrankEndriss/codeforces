/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider a[i] and a[i+1]
 * We need to change them by abs(a[i]-a[i+1])
 *
 * One such pair can be ignored, so we can jumber over
 * one of the i.
 */
void solve() {
    cini(n);
    cinai(a,n);

    if(n==2) {
        cout<<0<<endl;
        return;
    }

    int ans=abs(a[0]-a[1]);
    int ma=abs(a[0]-a[1]);
    ma=max(ma, abs(a[n-1]-a[n-2]));

    for(int i=1; i+1<n; i++)  {
        ma=max(ma, abs(a[i-1]-a[i])+abs(a[i]-a[i+1])-abs(a[i-1]-a[i+1]));
        ans+=abs(a[i]-a[i+1]);
    }
    ans-=ma;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
