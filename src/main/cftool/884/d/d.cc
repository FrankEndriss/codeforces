/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/*
 * Seems to be some dp.
 * Obs:
 * We can sort the colors by number of balls of that color
 * One strategy to do would be:
 *  -take all balls, select the 2 colors with most balls (c0,c1,c2)
 *  -put c0 and c1 into their boxes
 *  -put all other in some other box, repeat
 * Better strategy, consider it is "a lot of" colors
 * Devide balls in 3 groups with one move,
 * then solve for those 3 groups.
 * But solving for all subgroups does TLE, since N==2e5.
 * So there must be some kind of greedy.
 *
 * We want to minimize the number of times colors get moved.
 * The bigger the color, the more we want to minimize the number of moves.
 * Consider 
 * 4 colors: 3 are moved once, one is moved twice. 
 * 5 colors: 3 once, two twice.
 * 6 colors: 
 * streight: 6 -> 411 -> 21111 -> 111111: * 2 once, 2 twice, 2 three times 
 * split   : 6 -> 33 -> 3111 -> 111111: 6 moved twice
 * Up to 6 streight is allways better.
 * With more split could be better.
 * At some point, split is allways better, but it depends on the numbers.
 * But still, we cannot check all subgroups.
 *
 * There must be some obs allowing "direct" greedy.
 * -> WE can go from the other end:
 * Combine the last 3 colors to one set, until there is only one
 * bucket.
 * Note that the last move consists of k==2 if n is even.
 */
void solve() {
    cini(n);

    multiset<int> s;
    for(int i=0; i<n; i++) {
        cini(aux);
        s.insert(aux);
    }

    int ans=0;
    while(s.size()>1) {
        if(s.size()%2==0) {
            auto it=s.begin();
            int i=*it;
            it=s.erase(it);
            i+=*it;
            s.erase(it);
            s.insert(i);
            ans+=i;
        } else {
            auto it=s.begin();
            int i=*it;
            it=s.erase(it);
            i+=*it;
            it=s.erase(it);
            i+=*it;
            s.erase(it);
            s.insert(i);
            ans+=i;
        }
    }
    cout<<ans<<endl;
}

signed main() {
        solve();
}

