/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* find biggest two circles and connect them. 
 * The size of resulting circles determines ans.
 * ans is the sum of ordered pairs of elements 
 * of the circles.
 * foreach circle
 *  ans+=sum(numElem * numElem)
 **/
void solve() {
    cini(n);
    cinai(p,n);

    vb vis(n+1);
    int cnt;
    function<void(int)> dfs=[&](int node) {
        vis[node]=true;
        cnt++;
        if(!vis[p[node-1]])
            dfs(p[node-1]);
    };

    vi c;
    for(int i=1; i<=n; i++) {
        if(!vis[i]) {
            cnt=0;
            dfs(i);
            c.push_back(cnt);
        }
    }
    sort(c.begin(), c.end());
    if(c.size()>1) {
        int c0=c.back();
        c.pop_back();
        c.back()+=c0;
    }
    int ans=0;
    for(int i : c)
        ans+=i*i;
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

