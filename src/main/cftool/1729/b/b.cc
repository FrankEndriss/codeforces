/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Find the '0' which postceed a 2-digit letter, all before are one digit letters.
 *
 * Note that '00' indicates a 10 or 20 followed by a 0.
 */
void solve() {
    cini(n);
    cins(s);
    assert(s.size()==n);

    //cerr<<"s="<<s<<endl;
    string ans;
    int i=0;
    for(int j=0; j<n; j++) {
        if(s[j]=='0' && (j+1==n || s[j+1]!='0')) {
            if(i+2>=j) {
                //cerr<<"i="<<i<<" j="<<j<<endl;
                assert(i+2<=j);
            }
            stack<char> st;
            st.push('a'-1+s[j-1]-'0'+(s[j-2]-'0')*10);
            for(int k=j-3; k>=i; k--)
                st.push('a'+s[k]-'1');
            while(st.size()) {
                ans.push_back(st.top());
                st.pop();
            }
            i=j+1;
        }
    }
    for(int k=i; k<n; k++)
        ans.push_back('a'+s[k]-'1');

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
