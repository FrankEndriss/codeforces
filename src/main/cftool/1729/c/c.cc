/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp[c]=max number of jumps ending on a tile with letter c left of current pos.
 *
 * Traceback is annoying implementation...
 *
 * Also we can jump in both directions :/ What a shitty prob statement.
 * So, we can (and will) jump on each tile once that is between
 * s[0] and s.back(), in order of the chars.
 *
 * More than 20 minutes to understand the problem text. That is sic.
 */
const int INF=1e9;
void solve() {
    cins(s);

    vvi idx(26);
    for(size_t i=0; i<s.size(); i++) 
        idx[s[i]-'a'].push_back(i);

    int dir=1;
    if(s[0]>s.back())
        dir=-1;

    vi ans;
    for(char c=s[0]; ; c+=dir) {
        for(int i : idx[c-'a'])
            ans.push_back(i);

        if(c==s.back())
            break;
    }

    cout<<abs((int)(s[0]-s.back()))<<" "<<ans.size()<<endl;
    for(int i : ans)
        cout<<i+1<<" ";
    cout<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
