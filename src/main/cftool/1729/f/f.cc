/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How to quickly find remainder v(i,j)%9 ?
 * Can we use 9 as MOD? Its not a prime :/
 *
 * To find L1 and L2 we can iterate/find indexes categorized by 9 outcomes/remainders.
 * But we need to know the v(i,j)%9 to combine them.
 *
 * Note that remainders %9 go like
 * 1,1,1,...
 * 2,2,2,...
 * 3,3,3,...
 * So v(i,j)=rem[j]-rem[i];
 *
 * To complecated to implement :/
 */
void solve() {
    cins(s);
    cini(w);
    cini(m);

    /* r[i+1]=v(0,i)%9 */
    vi rem(s.size()+1);
    int sum=0;
    for(size_t i=0; i<s.size(); i++) {
        sum*=10;
        sum+=s[i]-'0';
        sum%=9;
        rem[i+1]=sum;
    }

    /* ww[i]=v(i,i+w-1) */
    vvi ww(9);
    for(size_t i=w; i<=s.size(); i++)
        ww[(rem[i]-rem[i-w]+9)%9].push_back(i-w);

    for(int i=0; i<m; i++) {
        cini(l); l--;
        cini(r); r--;
        cini(k);

        const int rr=(rem[r+1]-rem[l+1]+9+9)%9;

        /* now second part, we have to find for substring s[0..w-1] a position
         * L2, so that 
         * v(L2, L2+w-1) == (rem[w-1]*rr)
         * How to quickly find a position L2 for given value?
         * -> Since w is fixed, we can once calc all values of all positions.
         */

        bool done=false;
        for(int L1=0; !done && L1+w<=s.size(); L1++) {
            const int val=(rr*rem[L1+w])%9;
            for(int L2idx=0; !done && L2idx<9; L2idx++) {
                if((val*L2idx)%9==k) {
                    auto it=upper_bound(all(ww[L2idx]), L1);
                    if(it!=ww[L2idx].end()) {
                        cout<<L1<<" "<<*it<<endl;
                        done=true;
                    }
                }
            }
        }
        if(!done) {
            cout<<"-1 -1"<<endl;
        }
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
