/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that each friend has a balance.
 * We can combine a positive, and a not more negative to each other.
 * That is also optimal.
 * Then use the two most positive...and so on.
 * 
 * Also consider bal=0 friends.
 * ...
 * No.
 * Foreach negative friend find _one_ positive, starting with worst negative.
 * If there is none, ignore that 'friend'.
 * Once there are no more negative, pair positives.
 *
 * Also this is much simpler than hard to understand C.
 */
void solve() {
    cini(n);
    cinai(x,n);
    cinai(y,n);

    multiset<int> pos;
    vi neg;
    for(int i=0; i<n; i++) {
        if(x[i]>y[i]) {
            neg.push_back(x[i]-y[i]);
        } else {
            pos.insert(y[i]-x[i]);
        }
    }

    sort(all(neg));

    int ans=0;
    while(neg.size()) {
        auto it=pos.lower_bound(neg.back());
        if(it!=pos.end()) {
            ans++;
            pos.erase(it);
        }
        neg.pop_back();
    }
    ans+=pos.size()/2;
    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
