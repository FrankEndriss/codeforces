/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* She needs to put the numbers in order
 * under the stack.
 * She cannot put the first card under the stack
 * before all other cards are "available".
 * Available is, when the card in on hand or on pile
 * max i-2 positions away. (get the 2 while putting the 1)
 *
 * Edgecase:
 * If from the bottom of the pile the numbers are sorted
 * up to 1, then probably she can directly put all cards
 * under it. The same rules apply, but with other
 * distance of positions.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    set<int> avail;
    for(int i=0; i<n; i++) 
        if(a[i])
            avail.insert(a[i]);
    
    /* check if edgecase */
    int last=b[n-1];
    if(last>0) {
        bool sorted=true;
        for(int j=0; j<last; j++)
            if(b[n-1-j]!=last-j)
                sorted=false;

        if(sorted) {
            int idx=0;
            int ok=true;
            for(int next=last+1; next<=n; next++) {
                if(!avail.count(next))
                    ok=false;
                if(b[idx])
                    avail.insert(b[idx]);
                idx++;
            }

            if(ok) {
                cout<<n-last<<endl;
                return;
            }
        }
    }

    /* now standard case, check all the cards in b */
    int ans=0;
    for(int i=0; i<n; i++) {
        if(b[i]>0)
            ans=max(ans, i+2-b[i]);
    }
    ans+=n;

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
