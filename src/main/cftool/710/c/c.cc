/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* So we need an odd number of odd numbers in
 * every row, col and diago.
 * Note that even numbers do not contribute at all.
 * Note also that given d is allways odd.
 *
 * ...Its like aprils fool.
 * The pattern is, if we fill a rombus from center
 * it magically works. You see that at some point
 * of time or you do not.
 */
void solve() {
    cini(n);

    vector<pii> dir={ { -1, 0},{1,0},{0,-1},{0,1}};
    vvi ans(n, vi(n, -1));

    vector<pii> q;
    q.push_back({ n/2, n/2 });
    ans[n/2][n/2]=1;
    int odd=3;
    for(int i=0; i<n/2; i++) {
        vector<pii> q1;
        for(pii cur : q) {
            for(pii d : dir) {
                pii next={ cur.first+d.first, cur.second+d.second };
                if(next.first>=0 && next.first<n && next.second>=0 && next.second<n) {
                    if(ans[next.first][next.second]<0) {
                        ans[next.first][next.second]=odd;
                        odd+=2;
                        q1.push_back(next);
                    }
                }
            }
        }
        q.swap(q1);
    }

    int even=2;
    for(int row=0; row<n; row++) {
        for(int col=0; col<n; col++) {
            if(ans[row][col]>0)
                cout<<ans[row][col]<<" ";
            else {
                cout<<even<<" ";
                even+=2;
            }
        }
        cout<<endl;
    }

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

