/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* find some "induced" graph of
 * given graph where sum(vertex) / sum(edge)
 * is maximum.
 *
 * Brute force would be to create all induced
 * graphs and check the quotient.
 *
 * Singel vertex graph counts as 0, min size of G
 * is 2.
 *
 * Note n limited to 500
 *
 * How to create all subgraphs?
 * Size==2 is O(m)...
 *
 * Obs: If we combine two separate induced graphs,
 * the result is allways less optimal the the better of
 * the two graphs.
 * So, the optimal answer is a graph of size==2;
 */
void solve() {
    cini(n);
    cini(m);
    cinai(x,n);

    vector<vector<pii>> adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        cini(w);
        adj[u-1].emplace_back(v-1, w);
        adj[v-1].emplace_back(u-1, w);
    }

    double ans=0;
    for(int i=0; i<n; i++) {
        for(auto p : adj[i]) {
            if(p.first>i) {
                ans=max(ans, (1.0*x[i]+x[p.first])/p.second);
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
