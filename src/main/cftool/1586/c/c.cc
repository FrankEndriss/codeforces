/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * So, brute force is to do a multi-bfs from left/top margin,
 * that separates all cells into vis and notvis.
 * Consider the notvis ones.
 * If there is one "inside, ie dist>=2 from any vis one, then
 * it is not determined.
 *
 * So, how to find them quick for all queries?
 * There can be a lot of cols, so a lot lot pairs of x1,x2.
 * But obviously we cannot do a bfs on each query.
 * Can we create some dsu sets of cells, the connected components?
 *
 * Then each such component touches some cols and toprow or nottoprow.
 * Consider...
 *
 * ***************
 * No.
 * Actually we need to find if there are not-exitable cells independent of 
 * their color, ie surrounded by a border of X.
 * So if there is some connection from bottom-cell to right cell 
 * (or top-cell to right cell), then this connection creates an area with
 * unexitable cells, independent of their color.
 * So, foreach bottom/top cell find the path with the less leftmost cell connected
 * to the right border, and memoize it.
 *
 * Then, on each query, find for the cells in top/bottom row in segment x1,x2, 
 * if there is a cell with connection to x2 border with max(leftmost)>=x1
 * ...but still like O((nm/2)^2) :/
 */
void solve() {
    cini(n);
    cini(m);

    cinas(s,n);

    cini(q);
    for(int i=0; i<q; i++) {
        cini(x1);
        cini(x2);
    }
}

signed main() {
    solve();
}
