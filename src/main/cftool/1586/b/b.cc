/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider first rule, use b[0] as a leaf, so start with
 * b[0] - a[0] - c[0]
 *
 * Then consider each b[i]...
 * We somehow need to sort the rules, and connect them.
 * How?
 *
 * Find two vertex with no rule, so any vertex between them...no.
 *
 * connect 
 * a[0] - c[0]
 * a[1] - c[1] and so on
 * What if a[i] - c[i] is not possible, because b[i]==a[i-1] ?
 * ...so that does not work.
 *
 * Sort all rules by min(a,b,c)...no.
 *
 * Can we somehow create groups, or subtrees...
 * Say, if two vertex are not used together in a rule, they are unconnected.
 * Is there any rule saying two given vertex must be connected?
 * Say we got 3 vertex and 2 rules
 * 1 2 3
 * 2 1 3
 * Then sol is to put 3 in the middle:
 * 1 - 3 - 2
 */
void solve() {
    cini(n);
    cini(m);

    for(int i=0; i<m; i++) {
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
