/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << "=" << a << " ";
	logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* LCA based on euler path with segment tree. */
struct LCA {
    vector<int> height, euler, first, segtree;
    vector<bool> visited;
    int n;

    LCA(vector<vector<int>> &adj, int root = 0) {
        n = (int)adj.size();
        height.resize(n);
        first.resize(n);
        euler.reserve(n * 2);
        visited.assign(n, false);
        dfs(adj, root);
        int m = (int)euler.size();
        segtree.resize(m * 4);
        build(1, 0, m - 1);
    }

    void dfs(vector<vector<int>> &adj, int node, int h = 0) {
        visited[node] = true;
        height[node] = h;
        first[node] = (int)euler.size();
        euler.push_back(node);
        for (auto to : adj[node]) {
            if (!visited[to]) {
                dfs(adj, to, h + 1);
                euler.push_back(node);
            }
        }
    }

    void build(int node, int b, int e) {
        if (b == e) {
            segtree[node] = euler[b];
        } else {
            int mid = (b + e) / 2;
            build(node << 1, b, mid);
            build(node << 1 | 1, mid + 1, e);
            int l = segtree[node << 1], r = segtree[node << 1 | 1];
            segtree[node] = (height[l] < height[r]) ? l : r;
        }
    }

    int query(int node, int b, int e, int L, int R) {
        if (b > R || e < L)
            return -1;
        if (b >= L && e <= R)
            return segtree[node];
        int mid = (b + e) >> 1;

        int left = query(node << 1, b, mid, L, R);
        int right = query(node << 1 | 1, mid + 1, e, L, R);
        if (left == -1) return right;
        if (right == -1) return left;
        return height[left] < height[right] ? left : right;
    }

    /* @return the lca of u and v */
    int lca(int u, int v) {
        int left = first[u], right = first[v];
        if (left > right)
            swap(left, right);
        return query(1, 0, (int)euler.size() - 1, left, right);
    }
};

#define key(u,v) make_pair(min(u,v),max(u,v))
void solve() {
    cini(n);
    vvi adj(n);
    vector<pii> edgeids(n-1);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
        edgeids[i]=key(u,v);
    }

    LCA lca(adj);

    vi p(n);    // parent
    function<void(int,int)> dfs=[&](int node, int parent) {
        p[node]=parent;
        for(int chl : adj[node]) {
            if(chl!=parent)
                dfs(chl, node);
        }
    };
    dfs(0,-1);

    function<void(int,int,vector<pii>&)> path=[&](int from, int to, vector<pii> &res) {
        int root=lca.lca(from,to);
//cout<<"path, lca="<<root<<endl;
        while(from!=root) {
            int nex=p[from];
            res.push_back(key(from,nex));
            from=nex;
        }
        while(to!=root) {
            int nex=p[to];
            res.push_back(key(to,nex));
            to=nex;
        }
    };
    
    struct groupdata {
        int mi; // minval of group
        int micnt;   // edges with minval
        vector<pii> path;
    };

    struct edgedata {
        int val;
        vector<pii> groups;
    };

    map<pii,edgedata>  edges;  
    map<pii,groupdata> groups;

    cini(m);
    for(int i=0; i<m; i++) {
//cout<<"m="<<i<<endl;
        cini(a);    
        cini(b);
        cini(f);
        a--;
        b--;

        groupdata gd;
        gd.mi=f;
        path(a,b,gd.path);
        gd.micnt=gd.path.size();
//cout<<"path.size()="<<gd.micnt<<endl;

        for(pii p : gd.path)  {
            auto it=edges.find(p);
            if(it==edges.end()) {
                edgedata edge;
                edge.val=gd.mi;
                edge.groups.push_back(key(a,b));
                edges[p]=edge;
            } else {
                if(it->second.val<gd.mi) {
                    for(pii gr : it->second.groups) {
                        if(groups[gr].mi==it->second.val)
                            groups[gr].micnt--;
                    }
                    it->second.val=gd.mi;
                } else if(it->second.val>gd.mi)
                    gd.micnt--;
                it->second.groups.push_back(key(a,b));
            }
        }
        groups[key(a,b)]=gd;
    }

    bool ok=true;
    for(auto ent : groups) {
        if(ent.second.micnt<=0)
            ok=false;
    }
    if(!ok)
        cout<<-1<<endl;
    else {
        int MA=1e6;
        for(pii e : edgeids) {
            int f=edges[e].val;
            if(f==0)
                f=MA;
            cout<<f<<" ";
        }
        cout<<endl;
    }
    
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
		solve();
}

