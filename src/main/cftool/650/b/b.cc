/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* How much does one swipe cost?
 * Each swipe costs a.
 * 
 * So we can swipe to next pic, or change direction.
 * Note that changing dir does makes sence only once,
 * since if we change twice it would have been better 
 * to watch all pics in the first place.
 *
 * We need to solve twice, one start going left, once right.
 * Then calc cost in positions in both directions.
 * Ans is max of both dirs and every position checked
 * how far we would get in the other direction with 
 * the rest of time.
 *
 * "w" needs to be rotated.
 */
void solve() {
    cini(n);
    cini(a);
    cini(b);
    cini(T);

    cins(s);
    int tt=T;
    if(s[0]=='w')
        tt-=b;  
    tt--;
    if(tt<0) {
        cout<<0<<endl;
        return;
    }
    vi dpL; /* dpL[i]=time needed to open and watch i photos in left dir */

    int idx=0;
    for(int t=0; t<=tt; ) {
        idx--;
        if(idx<0)
            idx=n-1;
        if(idx==0) {
            cout<<n<<endl;
            return;
        }

        t+=a;   /* move */
        if(s[idx]=='w')
            t+=b;
        t++;
        if(t<=tt)
            dpL.push_back(t);
    }

    vi dpR; /* same right dir */
    idx=0;
    for(int t=0; t<=tt; ) {
        idx++;
        assert(idx<n);

        t+=a;   /* move */
        if(s[idx]=='w')
            t+=b;
        t++;
        if(t<=tt)
            dpR.push_back(t);
    }

    int ans=max((int)dpL.size(), (int)dpR.size())+1;

    for(size_t i=0; i<dpL.size(); i++)  {
        int et=tt-dpL[i]-(i+1)*a; /* extra time for other side */
        int cnt=distance(dpR.begin(), upper_bound(all(dpR), et));
        ans=max(ans, (int)i+2+cnt);
    }

    for(size_t i=0; i<dpR.size(); i++)  {
        int et=tt-dpR[i]-(i+1)*a; /* extra time for other side */
        int cnt=distance(dpL.begin(), upper_bound(all(dpL), et));
        ans=max(ans, (int)i+2+cnt);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
