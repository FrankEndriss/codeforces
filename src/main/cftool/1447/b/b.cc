/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We want to minimize the sum of negative values, ie make them all positive.
 * Only the parity is invariant, so we can make all positive, or
 * at least all except the abs(smallest)
 */
void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));
    int cntneg=0;
    int mi=1e9;
    int sum=0;
    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++)  {
            cin>>a[i][j];
            if(a[i][j]<0)
                cntneg++;
            mi=min(mi, abs(a[i][j]));
            sum+=abs(a[i][j]);
        }
    }

    if(cntneg%2==0) 
        cout<<sum<<endl;
    else
        cout<<sum-mi-mi<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
