/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Since we search the max(S(C,D)), we want the
 * longest subseq build of shortest substrings.
 *
 * What would bruteforce look like?
 * Note that the s() cannot be bigger than min(|C|,|D|)*2, 
 *
 * dp[i]=max S() for C starting at a[i]
 * a[i] exists at some positions in b[], posb[i][j] (if not, ignore dp[i] and i++)
 * So D can start at any of those positions.
 * Consider a[i+1].
 * We can extend a[i] by add foreach posb[i][j] the next posb[i+1][k] with posb[i+1][k]>posb[i][j]
 * ...somehow.
 * ...???
 *
 * Whenever we add a[i] (and some b[j]) to a subseq its S() might become
 * negative. In this case we can throw it away, since it would be better
 * to start a new one.
 * So maintain only positive subseqs!
 *
 * per subseq we need to know the length/number of matches and first position and last position in b[] 
 *
 * ****
 * Consider dp[i][j]=max(s()) for subseq ending in a[i] and b[j]
 */
void solve() {
    cini(n);
    cini(m);
    cins(a);
    cins(b);

    vvi dp(n, vi(m));

    int ans=0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(a[i]==b[j]) {
                dp[i][j]=max(dp[i][j], 2LL);
                if(i>0 && j>0)
                    dp[i][j]=max(dp[i][j], dp[i-1][j-1]+2);
            } else {
                if(i>0)
                    dp[i][j]=max(dp[i][j], dp[i-1][j]-1);
                if(j>0)
                    dp[i][j]=max(dp[i][j], dp[i][j-1]-1);
                if(i>0 && j>0)
                    dp[i][j]=max(dp[i][j], dp[i-1][j-1]-2);
            }
            ans=max(ans, dp[i][j]);
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
