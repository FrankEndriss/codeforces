/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to count the number of edges to find if it is n-1.
 * Since we search for every i for the opt j, there must
 * be exactly one i where we find an edge previusly found.
 * Which means that none of the previous ones should be 
 * found more than once.
 *
 * So, after haveing removed all nodes there must not be
 * more than one pair where the two numbers choose each other.
 * What is not so simple, because if a^b is small then b^a, too.
 *
 * Lets try some examples.
 * Obviusly every set so size 2 is good.
 *
 * {1,2,3}
 * 1-3
 * 2-3
 * 3-2 -> good
 * {1,2,3,4}
 * 1-3
 * 2-3
 * 3-2
 * 4-1 -> good
 *
 * let hb be the highest bit, and sHB be the set of numbers having that bit set.
 * These numbers link to each other.
 * So, we need to build sets by highest bit set.
 * if such set has only one member that member might link to some other sets.
 * We need to find all linked sets/components.
 *
 * And remove numbers from the other sets.
 *
 * Where do single member sets link?
 * To a member of the set with the higest of the lower bits set
 * which is also set in the single member.
 * Exception is if the single member is in the lowest set, then it links to
 * some member of the next higher set.
 *
 * But consider one set with 2 elements, and two sets which each 1 element.
 * The both 1s could link to each other (example { 1024,1025, 1,3 }
 * Here we need to consider that single set link to lower direction.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi sets(32);
    for(int i=0; i<n; i++) {
        for(int j=30; j>=0; j--)
            if(a[i]&(1LL<<j)) {
                sets[j+1]++;
                break;
            }
        if(a[i]==0)
            sets[0]++;
    }

    /* now resolve one single set after the other */
    /* resolve the lowest set to higher direction. */
    bool done=false;
    for(int i=0; !done && i<32; i++) {
        if(sets[i]==1) {
            for(int j=i+1; j<32; j++) {
                if(sets[j]>0) {
                    sets[j]++;
                    sets[i]--;
                    done=true;
                    break;
                }
            }
        }
    }

    /* resolve all other sets to lower direction */
    for(int i=31; i>=0; i--) {
        if(sets[i]==1) {
// ... to slow :/
        }
    }

    int carry=-1;
    bool linked=false;
    for(int i=31;i>=0; i--) {
        if(sets[i].size()==1) {
            if(carry)
                linked=true;
            carry=i;
        } else if(sets[i].size()>1) {
            carry=-1;
            linked=false;
        }
    }

    int ans=0;
    for(int i=0; i<32; i++) 
        if(sets[i].size()>1)
            ans+=sets[i].size()-1;

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
