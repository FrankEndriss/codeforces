/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Unclear statement...
 * We have 2e5 items each of some weight.
 * We can put int knapsack up to sum_weight==maxW
 * And sum_weight must be at least bigW/2
 *
 * We can ignore all items bigger maxW.
 * Obviously if there is one item in range we can use that one.
 * Else all items are smaller than minW.
 * So just add them up one by one in order of size.
 *
 */
void solve() {
    cini(n);
    cini(maxW);
    vector<pii> w(n);
    for(int i=0; i<n; i++) {
        cin>>w[i].first;
        w[i].second=i;
    }

    sort(all(w));

    const int minW=(maxW+1)/2;
    auto it=lower_bound(all(w), make_pair(minW, 0LL));
    if(it!=w.end()) {
        if(it->first<=maxW) {
            cout<<"1 "<<endl;
            cout<<it->second+1<<endl;
            return;
        }
    }

    vi ans;
    int sum=0;
    for(int i=0; i<n; i++) {
        sum+=w[i].first;
        ans.push_back(w[i].second+1);
        if(sum>=minW && sum<=maxW) {
            cout<<ans.size()<<endl;
            for(int i : ans)
                cout<<i<<" ";
            cout<<endl;
            return;
        }
    }
    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
