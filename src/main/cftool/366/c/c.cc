/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * if we multiply b[] by k we simply search 
 * sum(a[i])==sum(b[i]) and need to find
 * the biggest possible sum.
 *
 * so substitute all a[i]/b[i] by
 * d[i]=<a[i]-b[i], a[i]> we want to find the combination
 * of sum(d[i]) where sum(d[i]).first==0 and sum(d[i]).second is maximized
 */
const int N=10001;
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
    cinai(b,n);
    
    const int NN=N*k;
    vi dp(2*NN+7, -1);
    dp[NN]=0;
    for(int i=0; i<n; i++) {
        int d=a[i]-b[i]*k;
        vi dp2=dp;
        for(size_t j=0; j<dp.size(); j++) {
            if(dp[j]>=0)
                dp2[j+d]=max(dp2[j+d], dp[j]+a[i]);
        }
        dp.swap(dp2);
    }
    
    if(dp[NN]==0)
        dp[NN]=-1;

    cout<<dp[NN]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
