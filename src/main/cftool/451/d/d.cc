/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Consider ending positions
 * from left to right
 * for every position i with symbol a
 * ansOdd/Even+=number of a seen in positions with same/other parity
 */
void solve() {
    cins(s);

    vi ans(2); /* ans[0]==even ans[1]==odd */
    vvi cnt(2, vi(2));  /* 0=even, 1==odd; a=0, b=1 */

    for(size_t i=0; i<s.size(); i++) {
        int ab=(s[i]=='b');
        cnt[i%2][ab]++;

        ans[1]+=cnt[i%2][ab];
        ans[0]+=cnt[!(i%2)][ab];
    }

    cout<<ans[0]<<" "<<ans[1]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
