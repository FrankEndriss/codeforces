/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* first doubled from left, first doubled from right.
 * But not all indexes are permutations. */
void solve() {
    cini(n);
    cinai(a,n);

    vb visL(n);
    vi ansL;    // sizes of left permutations
    int maL=0;
    for(int i=0; i<n; i++) {
        if(visL[a[i]])
            break;
        maL=max(maL, a[i]);
        if(maL==i+1)
            ansL.push_back(i+1);
        visL[a[i]]=true;
    }

    vb visR(n);
    vi ansR;    // sizes of right permutations
    int maR=0;
    for(int i=0; i<n; i++) {
        if(visR[a[n-1-i]])
            break;
        maR=max(maR, a[n-1-i]);
        if(maR==i+1)
            ansR.push_back(i+1);
        visR[a[n-1-i]]=true;
    }

    vector<pii> ans;
    for(int l : ansL) {
        int r=n-l;
        if(binary_search(ansR.begin(), ansR.end(), r))
            ans.emplace_back(l, r);
    }
    cout<<ans.size()<<endl;
    for(pii p : ans) 
        cout<<p.first<<" "<<p.second<<endl;
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

