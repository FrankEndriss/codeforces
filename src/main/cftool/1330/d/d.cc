/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/*
 * add numbers as possible.
 *
 * Consider b[0] and b[1]
 * to make b[0]<b[1] 
 * a[1] must add some bit. All lower bits
 * can (and should) be removed.
 * We cannot add any bit bigger than the biggest set
 * in d. (upper bound)
 * So, if we start at a[0]=0, then b[0]==0
 * a[1]=1 -> b[1]=1
 * a[2]=3 -> b[2]=2
 * a[3]=1 -> b[3]=3
 * a[4]=7 -> b[4]=4
 *
 * What if limit is reached, ie if d==6?
 *
 * ---
 * Other perspective:
 * for fixed a[0], how much a[1] exist?
 * We must add at least one bit, and can remove
 * all set bits lower than the highest added one.
 * So, foreach notset bit in b[i] we can use that
 * bit as highest bit.
 * Thats 2^b-1 possibilities.
 *
 * ---
 * Why is m given as parameter and not constant?
 */
void solve() {
    cini(d);
    cini(m);
    
    while(true) {
    }
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

