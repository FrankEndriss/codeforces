/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* if sum of a[i]<n no ans.
 * We make operations from left to right.
 * If one operations overpaints "to much" 
 * cells it will be to less colors in the end.
 *
 * Start paint from cell 1 a[i] cells
 * Then from cell 2 a[i+1] cells
 * etc...
 * We need to strech this to make it fit over all cells,
 * ie by n-m.
 **/
void solve() {
    cini(n);    // cells
    cini(m);    // operations

    vi a(m);
    int sum=0;
    for(int i=0; i<m; i++) {
        cin>>a[i];
        sum+=a[i];
    }
    if(sum<n) { /* not enough paint */
        cout<<-1<<endl;
        return;
    }

    vi ans(m);  // ans[i]= index of first painted cell in operation i
    ans[m-1]=n-a[m-1];  // last paint operation paints end of strip

    for(int i=m-2; i>=0; i--)
        ans[i]=min(n-a[i], ans[i+1]-1);

    if(ans[0]<0) {
        cout<<-1<<endl;
        return;
    }

/* move operations to the left as much as needed */
    int toPaint=0;  // next index we must paint since unpainted
    for(int i=0; i<m; i++) {
        if(ans[i]>toPaint)
            ans[i]=toPaint;

        toPaint=max(toPaint, ans[i]+a[i]);
    }

    for(int i=0; i<m; i++)
        cout<<ans[i]+1<<" ";
    cout<<endl;
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

