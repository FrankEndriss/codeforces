/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We do not want to change existing pairs, so ignore them.
 * Count the LR of the remaining socks per color.
 *
 * WLG let L>=R
 * We must change (L-R)/2 socks from L to R anyway.
 * So, from each color where we have more than one L-sock change one,
 * to build a pair at the cost==1.
 * After that, if still L>R change any (L-R)/2 socks direction.
 *
 * Then we end up with same number of L/R socks, and can make pairs
 * by changing the color of one sock by cost of 1.
 */
void solve() {
    cini(n);
    cini(l);
    cini(r);

    vi a(n);
    for(int i=0; i<l; i++) {
        cini(aux);
        a[aux-1]++;
    }
    for(int i=0; i<r; i++) {
        cini(aux);
        a[aux-1]--;
    }


    int L=0, R=0;
    for(int i=0; i<n; i++) {
        if(a[i]>0)
            L+=a[i];
        else
            R-=a[i];
    }

    if(L<R) {
        swap(L,R);
        for(int i=0; i<n; i++)
            a[i]=-a[i];
    }

    int ans=0;
    for(int i=0; L>R && i<n; i++) {
        if(a[i]>1) {
            int cnt=min((L-R)/2, a[i]/2);
            a[i]-=cnt*2;
            L-=cnt;
            R+=cnt;
            ans+=cnt;
        }
    }
    ans+=(L-R)/2;

    int ans2=0;
    for(int i=0; i<n; i++) 
        ans2+=abs(a[i]);


    cout<<ans+ans2/2<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
