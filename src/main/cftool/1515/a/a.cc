/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We need to add smallest to biggest.
 *
 * ???
 */
void solve() {
    cini(n);
    cini(x);
    cinai(w,n);

    sort(all(w));

    set<int> s;
    for(int i : w)
        s.insert(i);

    vi ans;
    bool ok=true;
    int sum=0;
    for(int i=0; i<n; i++) {
        int l=*s.begin();
        int r=*s.rbegin();
        if(sum+l!=x) {
            sum+=l;
            s.erase(l);
            ans.push_back(l);
        } else if(sum+r!=x) {
            sum+=r;
            s.erase(r);
            ans.push_back(r);
        } else
            ok=false;
    }

    if(!ok)
        cout<<"NO"<<endl;
    else {
        cout<<"YES"<<endl;
        for(int i : ans)
            cout<<i<<" ";
        cout<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
