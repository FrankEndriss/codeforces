/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * This is somehow dp.
 * Consider ranges.
 * There are 4 kinds of ranges, classified by 
 * the types of the two borders, end of all or running comp:
 *
 * Consider case both ends running comp:
 * dp[i][j]=number of ways if first and last position is i,j
 * dp[i][j]=sum(dp[i][k]+dp[k+1][j]) for all k in i..j
 *
 * ...
 * It is not that simple, because we can turn on computers in different
 * subsegments in any order.
 * So, each go has a min and max number of computers to turn on etc...
 * idk :/
 *
 */
int MOD;
void solve() {
    cini(n);
    cin>>MOD;

    cerr<<"n="<<n<<endl;

    vvi dp(n+1, vi(n+1, -1));
    /* case i-1 and j+1 both running computers */
    function<int(int,int)> go0=[&](int i, int j) {
        cerr<<"go0, i="<<i<<" j="<<j<<endl;
        assert(i!=j);   /* not possible since such segment would have been auto filled */
        assert(i<j);
        assert(i!=0);
        assert(j!=n-1);

        if(dp[i][j]>=0)
            return dp[i][j];

        if(j-i==1)
            return dp[i][j]=2;

        dp[i][j]=0;
        for(int k=i; k<=j; k++)  {
            int lans=1;
            if(k>i+1)
                lans*=go0(i,k-1);

            if(k+1<j)
                lans*=go0(k+1,j);

            dp[i][j]+=lans;
            dp[i][j]%=MOD;
        }
        return dp[i][j];
    };

    /* case left end and right computer */
    function<int(int,int)> go1=[&](int i, int j) {
        cerr<<"go1, i="<<i<<" j="<<j<<endl;
        assert(i<=j);
        assert(i==0);
        assert(j!=n-1);

        if(dp[i][j]>=0)
            return dp[i][j];

        if(i==j)
            return dp[i][j]=1;

        if(j-i==1)
            return dp[i][j]=2;

        dp[i][j]=0;
        for(int k=i; k<=j; k++)  {
            int lans=1;
            if(k>i)
                lans*=go1(i,k-1);

            if(k+1<j)
                lans*=go0(k+1,j);

            dp[i][j]+=lans;
            dp[i][j]%=MOD;
        }
        return dp[i][j];
    };

    /* case left computer and right end */
    function<int(int,int)> go2=[&](int i, int j) {
        cerr<<"go2, i="<<i<<" j="<<j<<endl;
        assert(i<=j);
        assert(i!=0);
        assert(j==n-1);

        if(dp[i][j]>=0)
            return dp[i][j];

        if(i==j)
            return dp[i][j]=1;

        if(j-i==1)
            return dp[i][j]=2;

        dp[i][j]=0;
        for(int k=i; k<=j; k++)  {
            int lans=1;
            if(k>i+1)
                lans*=go0(i,k-1);

            if(k<j)
                lans*=go2(k+1,j);

            dp[i][j]+=lans;
            dp[i][j]%=MOD;
        }
        return dp[i][j];
    };

    /* case both end */
    function<int(int,int)> go3=[&](int i, int j) {
        cerr<<"go3, i="<<i<<" j="<<j<<endl;
        assert(i<=j);
        assert(i==0);
        assert(j==n-1);

        if(dp[i][j]>=0)
            return dp[i][j];

        dp[i][j]=0;
        for(int k=i; k<=j; k++)  {
            int lans=1;
            if(k>i)
                lans*=go1(i,k-1);

            if(k<j)
                lans*=go2(k+1,j);

            dp[i][j]+=lans;
            dp[i][j]%=MOD;
        }
        return dp[i][j];
    };

    int ans=go3(0,n-1);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
