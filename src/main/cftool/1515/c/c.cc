/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* distribute biggest block to smallest block,
 * allways the smallest tower gets the next block.
 * Then check in the end.
 */
void solve() {
    cini(n);
    cini(m);
    cini(x);
    cinai(h,n);


    vvi tdata(m);    /* m towers */

    priority_queue<pii> t;  /* tower sizes */
    for(int i=0; i<m; i++) 
        t.emplace(0,i);

    vi id(n);
    iota(all(id), 0);
    sort(all(id), [&](int i1, int i2) {
            return h[i1]>h[i2];
            });

    for(int i=0; i<n; i++) {
        auto cur=t.top();
        t.pop();
        cur.first-=h[id[i]];
        tdata[cur.second].push_back(id[i]);
        t.push(cur);
    }

    int a=-t.top().first;
    int b=0;
    while(t.size()) {
        b=-t.top().first;
        t.pop();
    }

    if(abs(a-b)>x) {
        cout<<"NO"<<endl;
    } else {
        cout<<"YES"<<endl;
        vi ans(n);
        for(int i=0; i<m; i++) {
            for(int k : tdata[i])
                ans[k]=i+1;
        }
        for(int i : ans)
            cout<<i<<" ";
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
