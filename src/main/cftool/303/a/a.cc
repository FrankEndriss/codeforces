/**
* Dont raise your voice, improve your argument.
* --Desmond Tutu
*/

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    if(n%2==0)
        cout<<-1<<endl;
    else {
        vvi ans(3, vi(n));
        for(int i=0; i<n; i++) {
            ans[0][i]=i;
            ans[1][i]=i;
            ans[2][i]=(i+i)%n;
        }
        for(int i=0; i<3; i++)  {
            for(int j=0; j<n; j++)
                cout<<ans[i][j]<<" ";
            cout<<endl;
        }
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

