/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* The distance of the both knights
 * in both directions must be a multiple of 4.
 *
 * And there must be at least one # field
 * with even dist in both dirs, and 
 * parity of half dist same on both axes.
 */
void solve() {
    vector<pii> cr;
    vector<pii> kn;

    for(int i=0; i<8; i++) {
        cins(s);
        for(int j=0; j<8; j++) {
            if(s[j]=='K') {
                kn.push_back({i,j});
                cr.push_back({i,j});
            } else if(s[j]=='.')
                cr.push_back({i,j});
        }
    }
    assert(kn.size()==2);

    if(abs(kn[0].first-kn[1].first)%4 || abs(kn[0].second-kn[1].second)%4) {
        cout<<"NO"<<endl;
        return;
    }
    bool ok=false;
    for(pii p : cr) {
        int xd=abs(kn[0].first-p.first);
        int yd=abs(kn[0].second-p.second);
        if(xd%2==0 && yd%2==0) {
            xd/=2;
            yd/=2;
            if(xd%2==yd%2)
                ok=true;
        }
    }

    if(ok)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

