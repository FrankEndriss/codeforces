/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* simple fraction implementation based on long long */
struct fract {
    ll f, s;

    fract(ll counter, ll denominator) :
        f(counter), s(denominator) {
        norm();
    }

    bool operator<(const fract &rhs) {
        return f * rhs.s < s * rhs.f;
    }

    fract& operator+=(const fract &rhs) {
        ll g = __gcd(this->s, rhs.s);
        ll lcm = this->s * (rhs.s / g);
        this->f = this->f * (lcm / this->s) + rhs.f * (lcm / rhs.s);
        this->s = lcm;
        norm();
        return *this;
    }

    friend fract operator+(fract lhs, const fract &rhs) {
        lhs += rhs;
        return lhs;
    }

    friend fract operator-(fract lhs, const fract &rhs) {
        lhs += fract(-rhs.f, rhs.s);
        return lhs;
    }

    fract& operator*=(const fract &rhs) {
        this->f *= rhs.f;
        this->s *= rhs.s;
        norm();
        return *this;
    }

    friend fract operator*(fract lhs, const fract &rhs) {
        lhs *= rhs;
        return lhs;
    }

    friend fract operator/(fract lhs, const fract &rhs) {
        return lhs * fract(rhs.s, rhs.f);
    }

    void norm() {
        int sign = ((this->f < 0) + (this->s < 0)) % 2;
        sign = -sign;
        if (sign == 0)
            sign = 1;
        ll g = __gcd(abs(this->f), abs(this->s));
        if (g != 0) {
            this->f = (abs(this->f) * sign) / g;
            this->s = abs(this->s) / g;
        }
    }
};

/* smallest (premeter^2)/area is the one
 * where all 4 sides have equals size
 * Given non desc sortet a[]
 * we search the pair of 2-sticks where
 * a[i]/a[j] biggest possible.
 */
void solve() {
    cini(n);

    map<int,int> f;
    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux]++;
    }

    vector<pii> p;

    int prev=-1;
    for(auto ent : f) {
        if(ent.second>3) {
            cout<<ent.first<<" "<<ent.first<<" "<<ent.first<<" "<<ent.first<<endl;
            return;
        }
        if(ent.second>1) {
            if(prev>0) {
                p.emplace_back(prev, ent.first);
            }
            prev=ent.first;
        }
    }

    sort(all(p), [](pii p2, pii p1) {
            return fract(p1.first, p1.second)<fract(p2.first, p2.second);
    });

    cout<<p[0].first<<" "<<p[0].first<<" "<<p[0].second<<" "<<p[0].second<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
