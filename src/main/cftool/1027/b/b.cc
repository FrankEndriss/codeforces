/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* left upper cell is white */
void solve() {
    cinll(n);
    cini(q);

    ll wcnt=(n*n+1)/2;
    
    for(int i=0; i<q; i++) {
        cinll(r);
        cinll(c);
        if((r+c)%2==0) {    // count white cells
/* two rows have n black and n white cells. */
            ll ans=((r-1)/2) * n;

            if((r-1)%2) { // one row with first cell white
                ans+=(n+1)/2;
                ans+=c/2;   // last row first cell black
            } else {
                ans+=(c+1)/2;   // last row first cell white
            } 
            cout<<ans<<endl;
        } else {    // count black cells
            ll ans=wcnt;
            ans+=((r-1)/2) * n;

            if((r-1)%2) { // one row with first cell white
                ans+=n/2;
                ans+=(c+1)/2;   // last row first cell black
            } else {
                ans+=c/2;   // last row first cell white
            }
            cout<<ans<<endl;
        }
    }

    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

