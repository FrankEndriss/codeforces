/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Note that both seqs are same if we switch the signs.
 * So solve only first case, and do that twice.
 *
 * Obviously the pairs have a[i]<b[i], 
 * and we need to sort them so that b[i]>a[i+1]
 *
 * How to sort?
 * It is a graph and we want to find the longest path.
 * There must be no circle in the path, each vertex only once.
 * But, longest path is O(to much)
 * ...
 * Can we sort by b[i] desc, biggest b first?
 * Since all 
 * a[i+1]<b[i+1], and
 * b[i]>b[i+1], it is that
 * b[i]>a[+1]
 * So we can use all pairs.
 */
void solve() {
    cini(n);
    vector<pii> ab(n);
    vi a(n);
    vi b(n);
    int cnt=0;
    for(int i=0; i<n; i++) {
        cin>>a[i]>>b[i];
        if(a[i]>b[i])
            cnt++;
        else 
            cnt--;
    }

    if(cnt>0) {
        for(int i=0; i<n; i++) {
            a[i]=-a[i];
            b[i]=-b[i];
        }
    }

    vi id;
    for(int i=0; i<n; i++)
        if(a[i]<b[i])
            id.push_back(i);

    sort(all(id), [&](int i1, int i2) {
            return b[i1]>b[i2];
    });

    cout<<id.size()<<endl;
    for(size_t i=0; i<id.size(); i++) 
        cout<<id[i]+1<<" ";
    cout<<endl;

}

signed main() {
    solve();
}
