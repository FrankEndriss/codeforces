/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We place the shob somewhere between two existing shops.
 * Most likely where the outcome is optimized.
 * Sort x[]
 * How to find that point for two adj x[i], x[i+1]?
 * We need to consider all p[k] strictly between them.
 * Then place the new hut so that first p[k] is covered,
 * and calc how many p[...] are earned, prefix sum the sum(p[...]).
 * Repeat with next hut...until all p[] in range are covered.
 * Then go to next gap.
 *
 * Note that there are at most 1e7 huts, so the will work out.
 * Edgecases: Check sums left of x[0], and right of x.back()
 */
void solve() {
    cini(n);
    cini(m);
    cinai(p,n);
    cinai(x,m);
    sort(all(x));

    vi pre(n+1);    /* prefix sums for p[] */
    for(int i=0; i<n; i++)
        pre[i+1]=pre[i]+p[i];

    int ans=0;
    for(int i=0; i+1<m; i++) {
        int L=(x[i]+99)/100;    /* leftmost hut to consider */
        if(L>=x[i+1])
            continue;

        int R=(x[i+1]-1)/100;   /* rightmost hut to consider */

        ...TODO
    }
}

signed main() {
    solve();
}
