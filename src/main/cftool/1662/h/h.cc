/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Some casework...
 * so many cases :/
 * No, the longer tiles allways lay parallel to the sides.
 * So there are not that much cases.
 */
void solve() {
    cini(w);
    cini(l);

    set<int> ans;
    for(int i=0; i<2; i++) {
        for(int a=1; a*a<=w; a++) {
            for(int aa : { a, w%a==0?w/a:a, (w-1)%a==0?(w-1)/a:a, (w-2)%a==0?(w-2)/a:a }) {
            /* case 1, left side complete by a */
            if(w%aa==0) {
                if((l-2)%aa==0) 
                    ans.insert(aa);
                if((l-1)%aa==0 && (w-2)%aa==0)
                    ans.insert(aa);
                if((l-1)%aa==0 && (l-2)%aa==0 && (w-1)%aa==0)
                    ans.insert(aa);
            }

            /* case 2, left side all but one cell of top or bottom left out */
            if((w-1)%aa==0) {
                if((l-1)%aa==0) 
                    ans.insert(aa);
                if((l-1)%aa==0 && l%aa==0 && (w-2)%aa==0)
                    ans.insert(aa);
                if((l-1)%aa==0 && (w-1)%aa==0)
                    ans.insert(aa);
            }

            /* case 3, left side all but two cells */
            if((w-2)%aa==0) {
                if(l%aa==0) 
                    ans.insert(aa);
                if((l-1)%aa==0 && w%aa==0)
                    ans.insert(aa);
                if(l%aa==0 && (l-1)%aa==0 && (w-1)%aa==0)
                    ans.insert(aa);
            }
            }
        }

        swap(w,l);
    }
    cout<<ans.size()<<" ";
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
