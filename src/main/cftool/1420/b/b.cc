/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * i<j && a[i]&a[j]>=a[i]^a[j]
 *
 * a[i]&a[j]==a[i]^a[j] if a[i]==0 && a[j]==0 
 * a[i]&a[j] > a[i]^a[j] if highest bit set in both, 
 * a[i]&a[j] < a[i]^a[j] if highest bit is set in only one of i,j
 *
 * foreach i we need to find the number of a[j] where
 * highest bit same as a[i]
 */
void solve() {
    cini(n);

    vi hbit(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        for(int j=31; j>=0; j--) {
            if(aux&(1LL<<j)) {
                hbit[i]=j+1;
                break;
            }
        }
    }

    vvi pre(32, vi(n+1));
    for(int i=0; i<n; i++)  {
        for(int j=0; j<32; j++) 
            pre[j][i+1]=pre[j][i];
        pre[hbit[i]][i+1]++;
    }

    int ans=0;
    for(int i=0; i<n; i++) {
        ans+=pre[hbit[i]][n]-pre[hbit[i]][i+1];
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
