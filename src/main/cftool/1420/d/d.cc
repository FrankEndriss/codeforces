/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int nom, const int denom) {
    return mul(nom, inv(denom));
}

const int N=3e5+7;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

/* turn on k ligths.
 * How much such sets of k indxes possible?
 *
 * n intervals, how much subset of size k are 
 * there that these k intervals overlap in at
 * least one point.
 *
 * If we count the overlappig intervals...then
 * if count==k first time that is one such subset, ans++.
 * if count==k+1 we can remove any one of the k previous intervals, so ans+=k
 * if count==k+2 -> can remove any two of previos intervals, nCr(k+2-1, 2)
 * count==k+i -> nCr(k+i, i)
 *
 * We need to somehow multiply something foreach new overlapping interval.
 * But what?
 * The kth overlapping interval always contributes 1
 * The (k+1)th contributes k-all that previously contributed.
 **/
void solve() {
    cini(n);
    cini(k);

    vi l(n);
    vi r(n);
    vector<pii> evt;
    for(int i=0; i<n; i++) {
        cin>>l[i]>>r[i];
        evt.emplace_back(r[i]+1, -1);
        evt.emplace_back(l[i],    1);
    }
    sort(all(evt));

    int ans=0;
    int cnt=0;
    for(size_t i=0; i<evt.size(); i++) {
        cnt+=evt[i].second;
        if(evt[i].second==1) {
            if(cnt>=k) {
                ans=(ans+nCr(cnt-1, cnt-k))%MOD;
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
