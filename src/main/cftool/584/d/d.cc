/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=10000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/**
 * Brute force should work.
 * Find the biggest primes smaller than n,
 * foreach one brute force all pairs of 
 * (small) numbers that coult add up to the diff.
 */
const int N=1e9;
void solve() {
    assert(pr.back()*pr.back()>N);
    cini(n);

    if(n<=pr.back() && !notpr[n]) {
        cout<<1<<endl<<n<<endl;
        return;
    }

    for(int p=n; ; p--) {
        bool pispr=true;
        for(int j=0; pispr && pr[j]*pr[j]<=p; j++)
            if(p%pr[j]==0)
                pispr=false;

        if(pispr && p==n) {
            cout<<1<<endl<<n<<endl;
            return;
        }

        if(pispr) {
            if(!notpr[n-p]) {
                cout<<2<<endl<<p<<" "<<n-p<<endl;
                return;
            }

            int pp=n-p;
            for(int j=0; pr[j]<=pp; j++) {
                if(!notpr[pp-pr[j]]) {
                    cout<<3<<endl;
                    cout<<p<<" "<<pr[j]<<" "<<pp-pr[j]<<endl;
                    return;
                }
            }
        }
    }
    assert(false);
}

signed main() {
        solve();
}
