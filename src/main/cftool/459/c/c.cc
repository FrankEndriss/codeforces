/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to build subsets of numbers 1..n
 * building subsets d times.
 * Subset size is max k.
 * No need to optimize k.
 *
 * Output:
 * foreach day
 * set number of each student
 *
 * Single day:
 * Every student own bus.
 * Two days:
 * SQRT(n) buses
 * Three days:
 * sqrt3(n); etc.
 *
 * How to construct the lists?
 * -> Recursive, start with whole list
 *  ans split to k groups.
 *  Recursive repeat for all days.
 *
 * if  k^d<=n then possible.
 */
void solve() {
    cini(n);
    cini(k);
    cini(d);

    if(k>n)
        k=n;

    int kk=k;
    for(int i=2; kk<n && i<=d; i++)
        kk*=k;
    if(kk<n)  {
        cout<<-1<<endl;
        return;
    }

    vvi busses(k);
    for(int i=0; i<n; i++) 
        busses[0].push_back(i);

    for(int i=0; i<d; i++) {
        vi stud;
        for(int j=0; j<k; j++) {
            copy(all(busses[j]), back_inserter(stud));
            busses[j].clear();
        }
        assert(stud.size()==n);

        
        int bidx=0;
        for(int j=0; j<n; j++) {
            busses[bidx%k].push_back(stud[j]);
            bidx++;
        }

        vi pos(n);
        for(int j=0; j<k; j++) {
            for(int l : busses[j])
                pos[l]=j+1;
        }
        
        for(int j : pos)
            cout<<j<<" ";
        cout<<endl;
    }


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
