
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * See editorial, its a more or less funny observation:
 * "aaa...aaa" (even/odd k times a) contains for odd k
 * the substring "a' odd times, "aa" even times... and so on.
 * So we combine two of them to get odd sums
 * string(k, 'a')+"b"+string(k-1, 'a')
 * and find suitable k.
 * If one to short add an 'c' somewhere.
 */
void solve() {
    cini(n);

    if(n<26) {
        string ans;
        for(int i=0; i<n; i++) 
            ans+=(char)('a'+i);
        cout<<ans<<endl;
        return;
    }

    int evenN=n-(n&1);
    string ans=string(evenN/2-1, 'a')+"b"+string(evenN/2, 'a');
    if(n>evenN)
        ans+='c';

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
