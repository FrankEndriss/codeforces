/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return max(a,b);
}

const S INF=1e9;
S st_e() {
    return -INF;
}

using stree=segtree<S, st_op, st_e>;

/* Foreach position consider a[i] to be the smallest value,
 * so find the biggest possible value in range to the left
 * and right.
 * Ignore left, just consider right.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans=0;
    for(int k=0; k<2; k++) {

        stack<pii> st;
        vi next(n, -1); /* next[i]=index of next smaller value right of i */

        for(int i=0; i<n; i++) {
            while(st.size() && st.top().first>a[i]) {
                auto [val,idx]=st.top();
                st.pop();
                next[idx]=i;
            }
            st.emplace(a[i], i);
        }

        stree seg(a);

        for(int i=0; i<n; i++) {
            if(next[i]<0)
                next[i]=n;

            if(next[i]>i+1) {
                int lans=a[i]*seg.prod(i, next[i]);
                ans=max(ans, lans);
            }
        }
        reverse(all(a));
    }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
