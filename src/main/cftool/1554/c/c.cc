/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * n^0==n
 * n^1== n+1 || n-1
 * n^2== n+2 || n-2
 *
 * if m<n then ans(n,m)==0
 *
 * So m>=n
 * Just find all x foreach number.
 * The first x>m is ans.
 *
 * Actually that number cannot be very big.
 * -> Is seems otherwise, TLE :/
 *
 * So, for what number i is bigger value?
 * -> For n=0
 * ok, there are also other ones.
 * which?
 * ***************
 * Editorial says we can construct
 * the smallest i so that x>m somehow
 * bit by bit.
 */
void solve() {
    cini(n);
    cini(m);

    if(m<n) {
        cout<<0<<endl;
        return; 
    }

    if(n==0) {
        cout<<m+1<<endl;
        return;
    }

    // so n<m

    for(int i=0; ; i++) {
        int x=i^n;
        if(x>m) {
            cout<<i<<endl;
            return;
        }
    }
    assert(false);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
