/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Just count.
 * After s.size()-1 turns all have a perfec ribbon.
 *
 * Edge case: if n==1 && initial perfect ribbon ans-=1
 */
void solve() {
    cini(n);
    cinas(s,3);

    vvi cnt(3, vi(256));
    for(size_t i=0; i<s[0].size(); i++) {
        cnt[0][s[0][i]]++;
        cnt[1][s[1][i]]++;
        cnt[2][s[2][i]]++;
    }

    vi ma(3);
    vi ans(3);
    for(int i=0; i<3; i++) {
        ma[i]=*max_element(all(cnt[i]));
        ans[i]=min((int)s[i].size(), ma[i]+n);
    }

    for(int i=0; i<3; i++) {
        if(ma[i]==s[0].size() && n==1)
            ans[i]--;
    }


//cerr<<"kuro="<<kuro<<" shiro="<<shiro<<" katie="<<katie<<endl;

    if(ans[0]>ans[1] && ans[0]>ans[2]) 
        cout<<"Kuro"<<endl;
    else if(ans[1]>ans[0] && ans[1]>ans[2])
        cout<<"Shiro"<<endl;
    else if(ans[2]>ans[0] && ans[2]>ans[1])
        cout<<"Katie"<<endl;
    else
        cout<<"Draw"<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
