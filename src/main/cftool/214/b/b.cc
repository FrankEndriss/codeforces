/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Must end with 0
 * and sum digits %3==0
 * To get max we need to remove smallest
 * number of digits, and from that the
 * smallest possible digits.
 *
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi f(10);
    int dsum=0;
    for(int i : a) {
        f[i]++;
        dsum+=i;
    }

    if(f[0]==0) {
        cout<<-1<<endl;
        return;
    }

    dsum%=3;
    int notdone=0;
    if(dsum>0) {
        notdone=1;
        for(int k=dsum; notdone && k<10; k+=3) {
            if(f[k]) {
                f[k]--;
                notdone--;
            }
        }

        if(notdone) {
            int ddsum=1;
            if(dsum==1) {
                ddsum=2;
            }
            notdone=2;
            for(int k=ddsum; notdone && k<10; k+=3) {
                while(f[k] && notdone) {
                    f[k]--;
                    notdone--;
                }
            }
        }
    }

    // check if any digit else than 0 is left
    int cnt=0;
    for(int i=1; i<10; i++) {
        cnt+=f[i];
    }
    if(cnt==0)
        f[0]=1;

    if(notdone==0) {
        for(int i=9; i>=0; i--) {
            for(int j=0; j<f[i]; j++)
                cout<<i;
        } 
    } else
        cout<<-1;
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
