/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;



const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

const S INF=1e9;
S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/* We need to find foreach char,
 * how many positions the right char
 * has to be moved to the left.
 *
 * Consider t to be s reversed.
 *
 * So we need to maintain the list of the positions of the 26 chars,
 * an index of "next" per char, and a segtree of 1s at removed 
 * indexes, so that we can find the number on each trun.
 */
void solve() {
    cini(n);
    cins(s);

    vvi pos(26);
    for(int i=0; i<n; i++) 
        pos[s[i]-'a'].push_back(i);

    stree seg(n);
    vi idx(26);
    int ans=0;


    for(int i=0; i<n; i++) {
        int l=s[n-1-i]-'a';
        int p=pos[l][idx[l]];
        idx[l]++;
        ans+=p-seg.prod(0,p);
        seg.set(p,1);
    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}
