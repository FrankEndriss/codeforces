
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find a way to remove this single element
 * in a way that it has "no effect".
 * This is the case if we remove it from a subseq fo
 * consecutive elements.
 *
 * Let cntCon be the number of consecutive subseqs, then
 * in first place ans=cntCon.
 * But, we need to be able to remove single elements from 
 * these subseqs, cntCon times.
 * If not, then at some point of time the seq is alternating,
 * and we have to remove an element.
 * Note that this decreases cntCon the first time we do it,
 * then the third time...and so on.
 *
 * We can do the first operation "for free" cnt times...
 * No, we need a better def of cnt.
 * In first segment we can do op1 max 1 time.
 * In first+second segment we can do op1 max 2 times.
 * etc
 *
 * Note the sic defintion of the optional second step "if the string s is not empty..."
 */
void solve() {
    cini(n);
    cins(s);

    /* count consecutive subseqs, and the number of elements available
     * to op1. */
    int ssCnt=0;
    int cnt=0;
    char prev='x';
    for(char c : s) {
        if(c!=prev) {
            ssCnt++;
        } else {
            cnt++;
            cnt=min(cnt, ssCnt);
        }

        prev=c;
    }
    //cerr<<"s="<<s<<" cnt="<<cnt<<" ssCnt="<<ssCnt<<endl;

    int ans=cnt;
    if(ssCnt>cnt) {
        int d=ssCnt-cnt;
        /* now we got an alternating seq of len d. We allways remove the first and last elements. */
        ans+=(d+1)/2;
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
