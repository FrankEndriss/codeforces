/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * (a+b+1)/2
 * find min
 * So we want to not round up each operation.
 * So a+b should be odd. So one odd, one even number.
 *
 * Try to make odd/even allways available by createing more odd/even numbers...
 *
 * Or greedy remove allways the biggest possible pair.
 * What is the pattern?  
 *
 * examples
 * n=5
 * 5+4 -> 5 3 2 1
 * 5+2 -> 4 3 1
 * 4+1 -> 3 2
 * 3+2 -> 3
 *
 * n=6 -> 6 5 4 3 2 1
 * 6+5 -> 6 4 3 2 1
 * 6+3 -> 5 4 2 1
 * 5+4 -> 5 2 1
 * 5+2 -> 4 1
 * 4+1 -> 3
 *
 * n=7 -> 7 6 5 4 3 2 1
 * 7+6 -> 7 5 4 3 2 1
 * 7+4 -> 6 5 3 2 1
 * 6+5 -> 6 3 2 1
 * 6+3 -> 5 2 1
 * 5+2 -> 4 1
 * 4+1 -> 3
 *
 * ********************
 * FUCK!!! everthing wrong. We do not want to 
 * sum odd and even.
 * We want to sum odd+odd or even+even!
 * n=4 -> 4 3 2 1
 * 4+2 -> 3 3 1
 * 3+3 -> 3 1
 * 3+1 -> 2
 * n=5 -> 5 4 3 2 1
 * 5+3 -> 4 4 2 1
 * 4+4 -> 4 2 1
 * 4+2 -> 3 1
 * 3+1 -> 2
 * n=6 -> 6 5 4 3 2 1
 * 6+4 -> 5 5 3 2 1
 * 5+5 -> 5 3 2 1
 * 5+3 -> 4 2 1
 * 4+2 -> 3 1
 */
void solve() {
    cini(n);
    multiset<int> odd;
    multiset<int> even;
    for(int i=1; i<=n; i++) {
        if(i%2) {
            odd.insert(-i);
        } else
            even.insert(-i);
    }
    vector<pii> ans;
    int sum=1;
    int ma=n;
    while(odd.size()+even.size()>1) {
        if(ma%2 && odd.size()>1) {
            auto it=odd.begin();
            int i1=*it;
            odd.erase(it);
            it=odd.begin();
            int i2=*it;
            odd.erase(it);
            sum=-(i1+i2);
            ans.emplace_back(-i1, -i2);
            sum/=2;
            if(sum%2)
                odd.insert(-sum);
            else
                even.insert(-sum);
        } else if(ma%2==0 && even.size()>1) {
            auto it=even.begin();
            int i1=*it;
            even.erase(it);
            it=even.begin();
            int i2=*it;
            even.erase(it);
            sum=-(i1+i2);
            ans.emplace_back(-i1, -i2);
            sum/=2;
            if(sum%2)
                odd.insert(-sum);
            else
                even.insert(-sum);
        } else {
            int i1=-*odd.begin();
            int i2=-*even.begin();
            ans.emplace_back(i1,i2);
            sum=(i1+i2+1)/2;
            break;
        }

        ma=0;
        if(odd.size())
            ma=max(ma, -*odd.begin());
        if(even.size())
            ma=max(ma, -*even.begin());

    }
    cout<<sum<<endl;
    for(auto p : ans)
        cout<<p.first<<" "<<p.second<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
