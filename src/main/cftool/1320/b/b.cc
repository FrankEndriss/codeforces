/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* First bfs from t to get distance
 * from all other vertex.
 * While this bfs we need to collect
 * number of parent vertex for the 
 * given min distance, so
 * vector<pair<int,map<int,int>> dp(n); dp[i]=<distT,map<vertex,cntPaths>>
 *
 * Then traverse the given path.
 * On every move to next vertex cases:
 * next=nextInNavPath
 * ->nothing happens
 * else
 * ->path has changed to one of paths in dp[next]
 *
 * For min we assume that nothing happens whenever possible,ie
 * if dist-1!=nDist then ans++.
 *
 * For max we assume that change happens whenever possible, ie
 * if dist-1!=nDist or number of paths in dp[cur]>1
 * then ans++
 *
 * ...again, to unclear to implement :/
 * How is the rule that max get incremented?
 */
void solve() {
    cini(n);
    cini(m);
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[v].push_back(u);    /* NOTE reverse */
    }

    cini(k);
    vi p(k);
    for(int i=0; i<k; i++)  {
        cin>>p[i];
        p[i]--;
    }

    const int INF=1e9;

    vector<set<int>> dp2(n); /* dp2[i]=set of possible predecessors */
    vi dp1(n,INF);

    queue<int> q;
    q.push(p.back());
    dp1[p.back()]=0;
    while(q.size()) {   
        int v=q.front();
        q.pop();

        for(int chl : adj[v]) {
            if(dp1[chl]>dp1[v]+1) {
                q.push(chl);
                dp1[chl]=dp1[v]+1;
                dp2[chl].clear();
                dp2[chl].insert(v);
            }   else if(dp1[chl]==dp1[v]+1) {
                dp2[chl].insert(v);
            }
        }
    }

    int ansmin=0;
    int prev=dp1[p[0]];
    for(size_t i=1; i<p.size(); i++) {
        int dist=dp1[p[i]];
        if(dist!=prev-1)
            ansmin++;
        prev=dist;
    }

    int ansmax=0;
    prev=dp1[p[0]];
    for(size_t i=1; i<p.size(); i++) {
        int dist=dp1[p[i]];
        if(dist!=prev-1 || dp2[i-1].size()>1)
            ansmax++;
        prev=dist;
    }
    
    cout<<ansmin<<" "<<ansmax<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
