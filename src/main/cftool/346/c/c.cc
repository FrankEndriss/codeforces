/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * It seems to be always optimal to go the largest possible
 * jump downwards from a to b.
 * So we create a dp with the max length of step
 * foreach position.
 *
 * To do so we go from b to a, maintaining a prioQ of 
 * the multiples of x.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(x,n);
    cini(a);
    cini(b);

    sort(all(x));
    auto it=unique(all(x));
    x.resize(distance(x.begin(), it));
    n=(int)x.size();

    /* next multiple of xx>=bb */
    function<int(int,int)> nextval=[](int bb, int xx) {
        int val=bb-(bb%xx);
        if(val<bb)
            val+=xx;
        return val;
    };

    const int N=a-b+1;
    vi dp(N, INF);

    priority_queue<pii> q;
    for(int i=0; i<x.size(); i++) {
        int val=nextval(b, x[i]);
        q.push({-val, x[i]});
    }

    for(int i=b; i<=a; ) {
        auto [next,xx]=q.top();
        next=-next;

        if(next+xx<=i) {
            q.pop();
            next=nextval(i+1,xx)-xx;
            q.push({-next, xx});
            continue;
        }

        dp[i-b]=min(i-b-1, next-b);
        i++;
    }

    int ans=0;
    int idx=a-b;
    while(idx>0) {
        /*
        if(dp[idx]>=idx) {
            assert(dp[idx]<idx);
        }
        */
        idx=dp[idx];
        ans++;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
