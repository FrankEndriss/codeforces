/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/** see https://cp-algorithms.com/algebra/linear-diophantine-equation.html */
int extGcd(int a, int b, int& x, int& y) {
    if (b == 0) {
        x = 1;
        y = 0;
        return a;
    }
    int x1, y1;
    int d = extGcd(b, a % b, x1, y1);
    x = y1;
    y = x1 - y1 * (a / b);
    return d;
}

/* @return true if a sol exists, one sol int x0,y0 */
bool find_any_solution(int a, int b, int c, int &x0, int &y0, int &g) {
    g = extGcd(abs(a), abs(b), x0, y0);
    if (c % g) {
        return false;
    }

    x0 *= c / g;
    y0 *= c / g;
    if (a < 0)
        x0 = -x0;
    if (b < 0)
        y0 = -y0;
    return true;
}

/* Construct next solution shifted by cnt positions. */
void shift_solution(int & x, int & y, int a, int b, int cnt) {
    x += cnt * b;
    y -= cnt * a;
}

/* @return number of solutions, xMin, y
 * note
 * lx1=xval with min x value
 * rx1=xval with max x value
 * lx2=xval with min y value
 * rx2=xval with max y value
 * */
tuple<int,int,int> find_all_solutions(int a, int b, int c, int minx, int maxx, int miny, int maxy) {
    int x, y, g;
    if (!find_any_solution(a, b, c, x, y, g))
        return {0,-1,-1};
    a /= g;
    b /= g;

    int sign_a = a > 0 ? +1 : -1;
    int sign_b = b > 0 ? +1 : -1;

    shift_solution(x, y, a, b, (minx - x) / b);
    if (x < minx)
        shift_solution(x, y, a, b, sign_b);
    if (x > maxx)
        return {0,-1,-1};
    int lx1 = x;

    shift_solution(x, y, a, b, (maxx - x) / b);
    if (x > maxx)
        shift_solution(x, y, a, b, -sign_b);
    int rx1 = x;

    shift_solution(x, y, a, b, -(miny - y) / a);
    if (y < miny)
        shift_solution(x, y, a, b, -sign_a);
    if (y > maxy)
        return {0,-1,-1};
    int lx2 = x;

    shift_solution(x, y, a, b, -(maxy - y) / a);
    if (y > maxy)
        shift_solution(x, y, a, b, sign_a);
    int rx2 = x;

    if (lx2 > rx2)
        swap(lx2, rx2);
    int lx = max(lx1, lx2);
    int rx = min(rx1, rx2);

    if (lx > rx)
        return {0,-1,-1};

    return { (rx - lx) / abs(b) + 1, lx, (c-a*lx)/b };
}

/*
 * This is somehow diophantine equations...
 * at least it looks like so.
 *
 * 1 
 * 1+b
 * 1+2*b
 * 1+3*b
 * a
 * a*a
 *
 * a*a
 * (1+b)*a
 * 1+b+b
 * (1+(2*b))*x
 * x+b
 * b+x
 * ...
 * n is in the set if n is of form
 * q*b + 1 + p*a
 *
 * so that is if n-1 is of the form
 * q*b + p*a
 *
 * But, also b+1 is in the set
 * How does this matter?
 */
void test() {
    int N=100;
    vb s(N);

    int a=3;
    int b=5;

    s[1]=true;
    for(int i=1; i<N; i++) {
        if(!s[i])
            continue;
        if(i*a<N)
            s[i*a]=true;
        if(i+b<N)
            s[i+b]=true;
    }

    for(int i=1; i<N; i++)  {
        if(s[i])
            cout<<"x";
        else
            cout<<"o";
        if(i%10==0)
            cout<<endl;
    }
    cout<<endl;
}

void solve() {
    cini(n);
    cini(a);
    cini(b);

    if(n==1) 
        cout<<"Yes"<<endl;
    else if(n<a && n<b+1)
        cout<<"No"<<endl;
    else if(n%b==1 || n%b==a)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
    cini(t);
    test();
    /*
    while(t--)
        solve();
        */
}
