
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * why this unusual def of mod?
 * However
 *
 * The perm starting with 1 is lex smaller than all perms not starting with 1.
 * There are (n-1)! starting with 1, and
 * n!-(n-1)! ones starting with a bigger number.
 *
 * Same with 2, its lex smaller than all perms starting with x>2
 * There are (n-1)! starting with 2, and
 * n!- (n-1)!*2 ones starting with a number bt 2
 *
 * Same with 3, its lex smaller than all perms starting with x>3
 * There are (n-1)! starting with 3, and
 * n!- (n-1)!*3 ones starting with a number bt 3
 *
 * Then, consider all starting with the same number.
 * There are n-1 other numbers.
 * And for them the same as above works.
 *
 * How to include the number of inversions?
 *
 */
const int N=507;

void solve() {
    cini(n);
    cini(M);

    vi fac(N);
    fac[1]=1;
    for(int i=2; i<N; i++) 
        fac[i]=(fac[i-1]*i)%M;

    int ans=0;

    for(int i=n; i>=1; i--) {
        for(int j=1; j<i; j++) {
            ans+=fac[i]-(((fac[i-1])*j)%M);
            ans%=M;
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
