
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* More math... :/
 *
 * Consider each + element and how often it contributes.
 * It contributes if it is not removed.
 * It is removed if the position of the element is lteq the
 * number of following -.
 *
 * foreach position
 * let dp[i]=number of prefix subseqs resulting in being the current
 * element the ith smallest on the current list.
 * Then find the number of postfix subseqs removing less/more than
 * i elements from that list.
 *
 * How to calc that numbers?
 */
using mint=modint998244353;
void solve() {
    cini(n);
    for(int i=0; i<n; i++) {
        char c;
        cin>>c;
        int k;
        if(c=='+')
            cin>>k;
    }
}

signed main() {
    solve();
}
