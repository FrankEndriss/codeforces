/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int dsum(int i) {
    int ans=0;
    while(i) {
        ans+=i%10;
        i/=10;
    }
    return ans;
}

/* Since 1e9 has max digitsum 81
 * there are max 80 possible
 * values for the right side of the equation.
 * Count the ones where the dsum of the right side
 * equals the equation value.
 */
void solve() {
    cini(a);
    cini(b);
    cini(c);

    const int N=1e9;
    vi ans;
    for(int i=1; i<=81; i++) {
        int ii=i;
        for(int j=1; j<a; j++)
            ii*=i;

        ii*=b;
        ii+=c;
        if(dsum(ii)==i && ii>0 && ii<N) {
            ans.push_back(ii);
        }
    }
    sort(ans.begin(), ans.end());
    cout<<ans.size()<<endl;
    if(ans.size()>0) {
        for(int i : ans)
            cout<<i<<" ";
        cout<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

