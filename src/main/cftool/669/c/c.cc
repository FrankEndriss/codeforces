/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9+7;

void solve() {
    cini(n);
    cini(m);
    cini(q);

    vvi dp(n, vi(m, INF));
    stack<vi> s;

    for(int i=0; i<q; i++) {
        vi a(4);
        cin>>a[0]>>a[1];
        if(a[0]==3)
            cin>>a[2]>>a[3];
        s.push(a);
//cout<<"x a="<<a[0]<<" "<<a[1]<<" "<<a[2]<<endl;
    }

//cout<<"did read input"<<endl;

    while(s.size()) {
        vi a=s.top();
//cout<<"a="<<a[0]<<" "<<a[1]<<" "<<a[2]<<endl;
        s.pop();

        if(a[0]==1) {   // shift row
            int r=a[1]-1;
            int aux=dp[r][m-1];
            for(int i=m-1; i>0; i--) 
                dp[r][i]=dp[r][i-1];
            dp[r][0]=aux;
        } else if(a[0]==2) { // shift col
            int c=a[1]-1;
            int aux=dp[n-1][c];
            for(int i=n-1; i>0; i--) 
                dp[i][c]=dp[i-1][c];
            dp[0][c]=aux;
        } else { 
            assert(a[0]==3);
            dp[a[1]-1][a[2]-1]=a[3];
        }
    }

    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++) {
            if(dp[i][j]==INF)
                dp[i][j]=0;
            cout<<dp[i][j]<<" ";
        }
        cout<<endl;
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

