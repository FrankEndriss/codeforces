/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}

/* This is the neutral element */
const S INF=1e18;
S st_e() {
    return INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Note that the order of operations does not matter.
 * So we want to decrease until all numbers are same,
 * then increase to all eq 0.
 *
 * Consider a[i] and a[i+1]
 * Whichever is bigger, that end must be decrease abs(a[i]-a[i+1]) times.
 * Note that order does not matter again.
 *
 * But we need to keep track of the smallest number we reach, so we need to 
 * consider min values on both sides.
 *
 * ...need a lazy segment tree for some reason :/
 */
void solve() {
    cini(n);
    cinai(a,n);

    stree seg(a);

    int decrsum=0;
    for(int i=0; i+1<n; i++) {
        int decr=abs(a[i+1]-a[i]);
        if(a[i]>a[i+1]) {
            seg.apply(0,i+1, -decr);
        } else {
            seg.apply(i+1,n, -decr);
        }
        decrsum+=decr;
    }

    int ans=decrsum+abs(seg.prod(0,n));
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
