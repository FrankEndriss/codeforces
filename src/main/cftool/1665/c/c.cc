/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    vi p(n+1);
    p[0]++;
    for(int i=0; i<n-1; i++)  {
        cini(aux);
        p[aux]++;
    }
    sort(all(p), greater<int>());
    priority_queue<int> q;

    int cnt=0;
    for(int i=0; p[i]>0; i++) {
        cnt++;
        q.emplace(p[i]+i);
    }

    while(cnt<q.top()) {
        int v=q.top();
        q.pop();
        cnt++;
        q.push(v-1);
    }

    int ans=cnt;
    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
