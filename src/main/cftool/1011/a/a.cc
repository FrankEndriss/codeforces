/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A bit wiered dp.
 * dp[i][j]=min weight of rocket with last stage i and j stages at all
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);
    cins(s);
    sort(all(s));

    vvi dp(n+1, vi(n+1, INF));
    dp[0][1]=s[0]-'a'+1;
    for(int j=1; j<=n; j++) {
        for(int i=1; i<n; i++) {
            for(int k=0; k<j; k++)  {
                if(...
                dp[i][j]=max(dp[i][j], 
            }
        }
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
