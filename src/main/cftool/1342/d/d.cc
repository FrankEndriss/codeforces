/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* foreach testcase we include as most arrays as possible.
 * That is trivial.
 * So we just have to do it quick. How?
 *
 * -> calculate how much the minimum number of testcases is,
 *  then distribute them round robin.
 */
void solve() {
    cini(n);
    cini(k);

    cinai(a,n);
    sort(a.begin(), a.end(), greater<int>());

    vi c(k+1);
    for(int i=1; i<=k; i++) 
        cin>>c[i];

    int t=0;
    for(int i=0; i<n; i++)
        t=max(t, (i+c[a[i]])/c[a[i]]);


    vvi ans(t);
    for(int i=0; i<n; i++)
        ans[i%t].push_back(a[i]);

    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++) {
        cout<<ans[i].size()<<" ";
        for(int j : ans[i])
            cout<<j<<" ";
        cout<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
