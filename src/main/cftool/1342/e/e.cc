/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}


const int N=200007;
vector<int> fac(N);

void initFac(int mod=MOD) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
}

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}


/* Rooks must be places one per row to make all fields attacked.
 * But we can and must configure cols.
 * How to build positions with k attacks?
 * How to calc the permutations of these positions?
 * Note that k cannot be larger than n-2
 *
 * let fk(k)== number of ways for given k
 * So, fk(0) there are n! permutations of all different columns
 * for fk(1) exactly one pair of cols must be same, which can be constructed
 *
 * fk(2) moving 1 rook to any col, and another  to any of the remaining used cols, which are 1 less.
 *
 * k==2 can be made with 2+2 or 3
 * k==3 can be made with 2+2+2 or 2+3 or 4
 */
void solve() {
    cini(n);
    cini(k);

    if(k>n-1) {
        cout<<0<<endl;
        return;
    }

    int fk0=fac[n-k];
    int ans=fk0;
    for(int i=1; i<=k; i++) {
        int extra=0;
        if(i>1)
            extra=1;
        ans=mul(ans, n+1-i-extra);    // we can move n different rooks
        ans=mul(ans, n-i);  // to n-1 positions each
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    initFac();
    solve();
}

