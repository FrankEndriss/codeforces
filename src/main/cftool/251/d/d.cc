/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Obviously we need to work bit by bit.
 * Consider from MSB to LSB the number of (remaining) numbers
 * having that bit set:
 * 0:ignore
 * odd: one group odd, one group even
 *   minimize x1 means use x1 as even group
 * even: both groups odd
 *
 * How to determine the groups then? somehow dp?
 * We want to split the groups so that next bit is even/even
 *
 * Foreach bit there are 3 possibilities:
 * Both groups have the bit set,
 * one groups has the bit set, else
 * none.
 *
 * If MSB freq is even, MSB can be set in both.
 * Try to find if first two bits can be set, ... somehow?
 * ***
 * Editorial proposes to calculate the bits in Z2 space using gauss
 * elimination. Well, might read about Z2.
 */
void solve() {
    cini(n);
    cinai(a,n);
}

signed main() {
        solve();
}
