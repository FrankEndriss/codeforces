/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to count the freq of i%k platforms,
 * then combine these infos and try all possible shifts.
 *
 * Consider edgecase y*k<x !
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(p);
    cini(k);
    cins(s);
    cini(x);
    cini(y);

    assert(s.size()==n);

    vi f(k);    /* freq of platforms it index i. Note that first jump goes to position p-1, not p.
                    Ignore platforms left of p. */
    for(int i=p-1; i<n; i++) 
        if(s[i]=='1')
            f[i%k]++;

    int ans=INF;
    for(int i=0; n-i>=p; i++) {  /* i== number of shifted cells */

        int cost=y*i;   /* shift costs */

        int cnt=1+(n-p-i)/k;    /* number of platforms needed */

        cost+=x*(cnt-f[(p+i-1)%k]);
        ans=min(ans, cost);

        if(s[p-1+i]=='1')
            f[(p-1+i)%k]--;
    }

    cout <<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
