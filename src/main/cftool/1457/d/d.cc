/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* "make it not non-deceasing" means to change the order.
 * ie we need to make a[i]^a[i+1]>a[i+2] || a[i]^a[i+1]<a[i-1] 
 * So we want to find the shortest subarray of len j starting at i so that
 * a[i-1] > xor(a[i]...a[i+j]) 
 * a[i+j+1] < xor(a[i]...a[i+j]) 
 *
 * Simply create all those subarray xorsums is O(n^2)  :/
 * ***
 * Consider the most sign bit of 3 consecutive numbers.
 * If it is the same then a[i]>a[i+1]^a[i+2].
 *
 * If there is no such triple then we can probably make
 * a[i]^a[i+1]>a[i+2] only if hbit(a[i+1])==hbit(a[i+2)
 *
 * So we need to maintain the xorsum of the numbers since
 * the last triple, and since there is none, from begin!
 */
int hbit(int num) {
    for(int i=31; i>=0; i--)
        if(num&(1LL<<i))
            return i;

    return -1;
}

const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n); /* non decreasing */

    vi h(n);
    for(int i=0; i<n; i++) 
        h[i]=hbit(a[i]);

    for(int i=0; i+2<n; i++) {
        if(h[i]>=0 && h[i]==h[i+1] && h[i]==h[i+2]) {
            cout<<1<<endl;
            return;
        }
    }

    int ans=INF;
    int sum=0;
    for(int i=0; i+1<n; i++) {
        //cerr<<" i="<<i<<" sum="<<sum<<endl;
        if(sum>a[i]) {
            int lans=i-1;
            for(int idx=0; idx<i; idx++) {
                //cerr<<"idx="<<idx<<" sum="<<sum<<" a[i]="<<a[i]<<" sum^a[idx]="<<(sum^a[idx])<<endl;
                if((sum^a[idx])>a[i]) {
                    sum^=a[idx];
                    lans--;
                } else 
                    break;
            }

            cout<<lans<<endl;
            return;
        }

        sum^=a[i];
    }

    cout<<-1<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
