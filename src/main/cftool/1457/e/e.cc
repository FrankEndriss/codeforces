/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* This must by greedy like...
 * So, without resets it would be mostly usefull to defeat the 
 * positive bosses first, ie ordered by c[i] non increasing.
 * Because each boss b defeated in round i contributes c[b]*(n-i-1)
 *
 * How can we optimize this by resets?
 * At the point where sum(c[b]) becomes negative it increases the
 * score to use the reset. How to find the optimal positions of resets?
 * Let bb[i] be the boss bonus after having defeated i bosses.
 * A reset contributes by -bb[i]*(n-i-1), and updates all consecutive 
 * bb to bb[i+1..n]-=bb[i];
 * Can we dp this somehow?
 * dp[i][k]=min (or max) points if at position i there are still k resets available.
 * But its O(n^2) or worse :/
 * ...
 * How to find the best reset if k==1?
 * -> We would find maximum -bb[i]*(n-i-1).
 * How to find the best resets if k==2?
 * Well, we would need to try all possible i for the first reset, and then
 * find the optimal second one the same way.
 * ...Same for more than 2. It is O(n!) :/
 * ***
 * By a reset we split the array b[] in two parts, and each one contributes separate.
 * So we need to find the optimum k-split.
 * Consider two adjacent splits. We can move the border between both a positin left
 * or right. 
 * Moving the border left makes the rightmost element of left beeing made
 * the first element of right.
 * Moving left makes the left split better by sum(c[l..l+k-1]), and the right one
 * worse by c[l+k]*sizeof(r)
 */
void solve() {
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
