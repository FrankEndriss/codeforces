/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Its a dp.
 * dp[i][j]=number of moves needed to make all positions up to i color j.
 * But that TLE :/
 *
 * We need to do this color by color.
 */
const int N=101;
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);
    cinai(c,n);

    int ans=INF;
    for(int j=1; j<N; j++) {
        vi dp(n);
        dp[0]=1;
        int next=k;
        int lans=1;

        if(c[0]==j) {
            dp[0]=0;
            next=1;
            lans=0;
        }

        for(int i=1; i<n; i++) {
            if(next>i)
                continue;

            if(c[i]==j) {
                next++;
            } else {
                next+=k;
                lans++;
            }
        }
        ans=min(ans, lans);
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
