/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

vi border;

int calculate_border(string& s) {
    int i = 1;
    int j = -1;
    int n = (int)s.size();

    border[0] = -1;
    while(i < n) {
        while(j >= 0 && s[i] != s[j+1])
            j = border[j];
        if (s[i] == s[j+1])
            j++;
        border[i++] = j;
    }
    return border.back()+1;
}

/* We need to somehow utilize the prefix function.
 * There is problem with "xyxyxy".
 * prefix function find len 4, but there is no
 * len 4 infix. So ans is of len 2.
 *
 * In case intersecting pre/postfixes we check border[border.back].
 * See tutorial!
 */
void solve() {
    cins(s);

    border.resize(s.size());
    int post=calculate_border(s);
    if(post==0) {
        cout<<"Just a legend"<<endl;
        return;
    }

//cerr<<"post="<<post<<" border.back()="<<border.back()<<" border[border.back()]="<<border[border.back()]<<endl;

    for(size_t i=0; i+1<s.size(); i++) {
        if(border[i]==border.back()) {
            cout<<s.substr(0, border.back()+1)<<endl;
            return;
        }
    }

    int ans=border[border.back()]+1;
    if(ans==0)
        cout<<"Just a legend"<<endl;
    else
        cout<<s.substr(0, ans)<<endl;;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
