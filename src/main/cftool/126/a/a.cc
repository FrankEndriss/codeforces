/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * Not less than t0, but as close as possible to t0. 
 *
 * t=( t1*y1 + t2*y2) / (y1+y2)
 *
 * t*(y1+y2)= t1*y1 + t2*y2
 *
 * (t-t1)*y1+t*y2 = t2*y2
 * (t-t1)*y1= = t2*y2-t*y2
 * (t-t1)*y1= = (t2-t)*y2
 * ((t-t1)*y1)/(t2-t)= y2
 * ((t-t2)*y2)/(t1-t)= y1
 *
 * if t0==t1 || t0==t2 one tab
 * must be closed.
 *
 * ((t-t1)*y1)= (t2-t) * y2
 */
void solve() {
    cini(t1);
    cini(t2);
    cini(x1);
    cini(x2);
    cini(t0);

    if(t0==t1) {
        cout<<x1<<" 0"<<endl;
    } else if(t0==t2) {
        cout<<"0 "<<x2<<endl;
    }

    
    int anom=1e9;
    int aden=1;

/* foreach y1 binsearch y2, choose best value in the end 
 * TODO implement
 * */
    for(int i=0; i<=x1; i++) {

        int l=0;
        int r=x2+1;
        
        while(l+1<r) {
            int mid=(l+r)/2;
            int lhs=(t-t1)*i;
            int rhs=(t2-t)*mid;
            if(lhs<=rhs)
                r=mid;
            else
                l=mid;
        }

        cout<<l<<" "<<x2<<endl;

    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
