/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* So, if we 
 * use one bit range==2
 * use two bits r==4
 * 3 r==8
 * ...
 * First compress and count distinct vals.
 * From I calc the number of available bits per sample,
 * from bits the range.
 *
 * From range and prefix sums number of unchanged samples.
 */
void solve() {
    cini(n);
    cini(I);
    map<int,int> f;
    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux]++;
    }

    vi ff(f.size()+1);
    int i=0;
    for(auto ent : f) {
        ff[i+1]=ff[i]+ent.second;
        i++;
//cerr<<f[i]<<" ";
    }
//cerr<<endl;
    
    int bits=min(31LL, (I*8)/n);
    int range=1;
    for(i=0; i<bits; i++)
        range*=2;

//cerr<<"bits="<<bits<<" range="<<range<<endl;
    int ans=-1;
    for(i=0; i+range<ff.size(); i++) {
        ans=max(ans, ff[i+range]-ff[i]);
    }
    if(ans==-1)
        ans=ff.back();

    cout<<n-ans<<endl;

    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
