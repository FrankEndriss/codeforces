/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Fro every citicen after all events there are three 
 * possible outcomes:
 * a[i], last t==1 update or some t==2 update.
 * So, for each one we have to find the biggest
 * t==2 after the last t==1
 */
void solve() {
    cini(n);
    cinai(a,n);
    vi lastT1(n);   // lastT1=evt of last set a[i]

    cini(q);
    vi t2(q+1);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(p);
            cini(x);
            a[p-1]=x;
            lastT1[p-1]=i;
        } else if(t==2) {
            cini(x);
            t2[i]=x;
        } else 
            assert(false);
    }

    for(int i=q-1; i>=0; i--) 
        t2[i]=max(t2[i], t2[i+1]);

    for(int i=0; i<n; i++)
        cout<<max(a[i], t2[lastT1[i]])<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
