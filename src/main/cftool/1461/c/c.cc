/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to take into account that the end of the 
 * array can be sorted prior to all experiments.
 *
 * Once it is sorted that does not change.
 *
 * In round i:
 * It is sorted with prop p[i]
 * if r[i]>= n-k.
 * So it is unsorted with prop 1.0 if r[i]+k<n
 * Else if is unsorted 
 * with prop 1-p[i]
 * So maintain the prop that it is unsorted after each step.
 *
 * WA for some reason :/ idk.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    vi r(m);
    vd p(m);
    ld ans=1.0;

    int cnt=0;
    int idx=n-1;
    while(idx>=0 && a[idx]==idx+1) {
        idx--;
        cnt++;
    }

    for(int i=0; i<m; i++)  {
        cin>>r[i]>>p[i];
    }

    if(cnt==n) {
        cout<<ans<<endl;
        return;
    }

    for(int i=0; i<m; i++)  {
        //cerr<<"cnt="<<cnt<<"n-cnt="<<n-cnt<<" r[i]="<<r[i]<<" p[i]="<<p[i]<<endl;
        if(r[i]>=(n-cnt))
            ans*=(1-p[i]);
    }
    cout<<(1-ans)<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
