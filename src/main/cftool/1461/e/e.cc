/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * x<=1e6
 *
 * k liters init
 * level between l and r incl
 * t days
 *
 * every day k-=x
 * and if possible k+=y
 * does k at any time becomes <l ?
 *
 *
 * while some days we cannot add y because
 * k+y>r, so k decreases by x.
 * On the other days k decreases by (x-y)
 * How much days max?
 * Binary search?
 */
const int N=1e8;
void solve() {
    cini(k);
    cini(l);
    cini(r);
    cini(t);
    cini(x);
    cini(y);

    int diff=x-y;
    
    /* find level of k at first time we can add y 
     * see third example... after first time filled up we need to check again
     * ...and again... ???
     * */

    set<int> ttset;
    int tt=0;
    int sumDays=0;
    while(true) {
        int ma=r-y;
        days=(k-ma)/x;
        if((k-ma)%x!=0)
            days++;
        firstTime=k-(x*days);
        ttset.insert(k);
        k=firstTime;
        sumDays+=days;
    }
    
    int firstTime=k;
    int days=0;
    if(k+y>r) {
        int ma=r-y;
        days=(k-ma)/x;
        if((k-ma)%x!=0)
            days++;

        firstTime=min(firstTime, k-(x*days));
    }

    if(firstTime<l && k-(x*t)<l) {
        cout<<"No"<<endl;
        return;
    }

    if(k-(x*t)>=l) {
        cout<<"Yes"<<endl;
        return;
    }

    t-=days;
    k=firstTime;

    if(k-t*diff>=l) 
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
