/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* slicing:
 * split by value of (max+min)/2
 * relative order does not matter.
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);
    cinai(s,q);

    sort(all(a));
    vi pre(n+1);
    for(int i=1; i<=n; i++) 
        pre[i]=pre[i-1]+a[i-1];

    set<int> ans;

    /* inc l excl r */
    function<void(int,int)> split=[&](int l, int r) {
        ans.insert(pre[r]-pre[l]);

        int mi=a[l];
        int ma=a[r-1];
        int mid=(mi+ma)/2;

        auto it=upper_bound(a.begin()+l, a.begin()+r, mid);
        int idx=distance(a.begin(), it);
        if(idx>l && idx<r) {
            split(l,idx);
            split(idx,r);
        }
    };

    split(0,n);

    for(int i=0; i<q; i++) {
        if(ans.count(s[i]))
                cout<<"Yes"<<endl;
        else
            cout<<"No"<<endl;
    }



}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
