/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Brute force?
 * no.
 *
 * ***
 * Better dp is to go from bottom line upwards.
 * whenever there are 3 spruces are just "below" 
 * the current position we got another spruce there. So
 * if(s[i][j]=='*')
 *   dp[i][j]= 1 + min(dp[i-1][j-1], dp[i-1][j], dp[i-1][j+1]);
 * */
void solve() {
    cini(n);
    cini(m);
    cinas(s,n);

    vvi dp(n, vi(m));   /* dp[i][j]=size of spruce with right down corner at i,j */

    int ans=0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(s[i][j]=='*') {
                dp[i][j]=1;
                ans++;
            }
        }
    }
    for(int sz=1; sz<=n; sz++) {
        vvi dp0(n, vi(m));
        for(int i=sz; i<n; i++) {
            int cnt=0;
            for(int j=0; j<m; j++) {
                if(s[i][j]=='*')
                    cnt++;
                else
                    cnt=0;

                if(cnt>=sz*2+1 && dp[i-1][j-1]==sz) {
                    dp0[i][j]=sz+1;
                    ans++;
                }
            }
        }
        dp=dp0;
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
