/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Come from left or go to n+2
 *
 * Why WA?
 */
const int INF=1e9;
void solve() {
    cini(n);

    vi dp(20, INF);
    dp[0]=0;
    queue<int> q;
    q.push(0);

    while(q.size()) {
        int v=q.front();
        q.pop();
        if(v-2>=1) {
            if(dp[v-2]>dp[v]+1) {
                dp[v-2]=dp[v]+1;
                q.push(v-2);
            }
        }
        if(v+2<20) {
            if(dp[v+2]>dp[v]+1) {
                dp[v+2]=dp[v]+1;
                q.push(v+2);
            }
        }
        if(v-3>=1) {
            if(dp[v-3]>dp[v]+1) {
                dp[v-3]=dp[v]+1;
                q.push(v-3);
            }
        }
        if(v+3<20) {
            if(dp[v+3]>dp[v]+1) {
                dp[v+3]=dp[v]+1;
                q.push(v+3);
            }
        }
    }

    
    if(n<20) {
        cout<<dp[n]<<endl;
        return;
    }

    int ans=(n/3);

    if(n%3==1) {    /* make last step 2 instead of 3 and another one of 2 */
        ans+=1;
    } else if(n%3==2)   /* make an additional step by 2 */
        ans+=1;

    cout<<ans<<endl;



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
