/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * There is only one path-pattern:
 * Filling both rows until a certain cell, then go right to the 
 * end and back in the other row.
 *
 * Create some prefix arrays.
 *
 * ...not able to find the off by one bug :/
 */
const int INF=1e10+7;
void solve() {
    cini(m);

    vvi a(2, vi(m));
    for(int i=0; i<2; i++) 
        for(int j=0; j<m; j++)  {
            cin>>a[i][j];
            a[i][j]++;
        }
    a[0][0]=0;

    vvi dp1(2, vi(m,INF));  /* dp1[i][j]=earliest time we can reach cell[i][j] if going in both rows */
    dp1[0][0]=0;
    dp1[1][0]=max(1LL, a[1][0]);
    int i=1;
    for(int j=1; j<m; j++) {
        dp1[i][j]=max(dp1[i][j-1]+1, a[i][j]);  /* go right into col j*/
        dp1[1-i][j]=max(dp1[i][j]+1, a[1-i][j]);    /* go up/down */
        i=1-i;
    }

    vvi b(2, vi(m));    /* earliest time to right border */
    for(int j=0; j<m; j++) {
        b[0][j]=a[0][j]+m-1-j;
        b[1][j]=a[1][j]+m-1-j;
    }
    for(int j=m-2; j>=0; j--)
        for(int i=0; i<2; i++)
            b[i][j]=max(b[i][j], b[i][j+1]);

    vvi c(2, vi(m));    /* earliest time we can reach a[i][j] if starting from right */
    c[0][m-1]=a[0][m-1];
    c[1][m-1]=a[1][m-1];
    for(int j=m-2; j>=0; j--) 
        for(int i=0; i<2; i++) 
            c[i][j]=max(a[i][j], c[i][j+1]+1);

    int ans1=dp1[m%2][m-1];
    /* go right in both rows until col j, then go right in row 0 and left in row 1 */
    for(int j=0; j<m; j+=2) {
        /* go until right end */
        int lans=max(dp1[0][j]+m-1-j, b[0][j]);    /* now we are in a[0][m-1] */
        int k=j;    /* col in row[1] to where we go back */

        lans=max(lans+1,a[1][m-1]); /* now we are in a[1][m-1] */
        lans=max(lans+(m-1)-k, c[1][k]);    /* go some positions left */

        ans1=min(ans1, lans);
    }

    int ans2=INF;
    /* go right in both rows until col j, then go right in row 1 and left in row 0 */
    for(int j=1; j<m; j+=2) {
        /* go until right end */
        int lans=max(dp1[1][j]+m-1-j, b[1][j]);    /* now we are in a[1][m-1] */
        int k=j;
        lans=max(lans+1,a[0][m-1]); /* now we are in a[0][m-1] */
        lans=max(lans+(m-1)-k, c[0][k]);
        ans2=min(ans2, lans);
    }

    cout<<min(ans1, ans2)<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
