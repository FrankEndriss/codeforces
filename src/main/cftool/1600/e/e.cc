/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Once a player chooses the bigger side, the outcome is determined
 * because both players can only choose the side.
 * So, if that leads to win for the current player, he will choose the 
 * bigger side, else the smaller.
 *
 * So, for first move we need to know how many elements are there on the bigger end
 * until no more move is possible. This is, the first element that is smaller than any
 * one before it.
 * Same we need in next step...and so on.
 * So, foreach position, we need to know the position of the nearest smaller element to the left
 * and to the right, then simulate and see who wins.
 *
 * Find next smaller elements with a stack.
 *
 * What if both elements are same?
 * ->If one side leads to win he will choose it
 *
 *
 *  ...to complecated to implement :/
 */
const int INF=1e9+7;
void solve() {
    cini(n);
    cinai(a,n);

    stack<pii> st;

    st.push({-1,-1});
    vi l(n, -1);
    for(int i=0; i<n; i++) {
        while(st.top().first>a[i])
            st.pop();
        l[i]=st.top().second;
        st.push({a[i],i});
    }

    while(st.size())
        st.pop();
    st.push({-1,n});
    vi r(n, n);
    for(int i=n-1; i>=0; i--) {
        while(st.top().first>a[i])
            st.pop();
        r[i]=st.top().second;
        st.push({a[i],i});
    }

    vi idx={ 0, n-1 };
    int p=0;    /* Alice begins */

    vs ans={ 
        "Alice",
        "Bob"
    };
    while(idx[0]<idx[1]) {
        int ldist=r[idx[0]]-idx[0];
        int rdist=l[idx[1]]-idx[1];

        if(a[idx[0]]<a[idx[1]]) {
            /* will choose R if it leads to win, else L */
            if(rdist%2==1) {
                cout<<ans[p]<<endl;
                return;
            } else {
                idx[0]++;
            }
        }  else if(a[idx[1]]<a[idx[0]]) {
            if(ldist%2==1) {
                cout<<ans[p]<<endl;
                return;
            } else {
                idx[1]++;
            }
        } else { /* same */
            if(ldist%2==1 || rdist%2==1)
                cout<<ans[p]<<endl;
            else
                cout<<ans[!p]<<endl;
            return;
        }
        p=!p;
    }
    cout<<ans[!p]<<endl;
}

signed main() {
    solve();
}
