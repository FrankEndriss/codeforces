/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int q(int i, int j) {
    cout<<"? "<<i<<" "<<j<<endl;
    cini(ans);
    return ans;
}

/**
 * Place the divice in a corner, and a next corner.
 * This gives two diagonals where the corners of the base are
 * located.
 * Somewhere these diagonals meet.
 * Select on the side at that hight, ans will be distance from 
 * border of grid to border of base.
 * Do on all 4 sides, it is 8 queries.
 *
 */
const int N=1e9;
void solve() {
    vvi c(2, vi(2));    /* distances from the 4 corners */

    c[0][0]=q(1,1);
    c[0][1]=q(1,N);
    c[1][0]=q(N,1);
    c[1][1]=q(N,N);

    // TODO more visually explainable index plus/minus formular...
}

signed main() {
    solve();
}
