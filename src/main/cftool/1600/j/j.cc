/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** We need to dfs and collect the number of cells connected.
 * An edge is everywhere where there is no wall.
 *
 * For some reason
 * -the outside walls are not in the example data
 * -and it does not work correctly.
 */
void solve() {
    cini(n);
    cini(m);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++)
            cin>>a[i][j];

    vvb vis(n, vb(m));

    vi dx={ -1, 0, 1, 0 };
    vi dy={ 0, 1, 0, -1 };
    function<int(int,int)> dfs=[&](int i, int j) {
        //assert(i>=0 && i<n);
        //assert(j>=0 && j<m);
        if(i<0 || i>=n || j<0 || j>=m || vis[i][j])
            return 0LL;

        vis[i][j]=true;

        int ans=1;
        for(int ii=0; ii<4; ii++) {
            if((a[i][j]&(1<<ii))==0) {
                if(i+dy[ii]>=0 && i+dy[ii]<n && j+dx[ii]>=0 && j+dx[ii]<m) {
                    if((a[i][j]&(1<<ii)) != (a[i+dy[ii]][j+dx[ii]]&(1<<((ii+2)%4)))) {
                        cerr<<"asser fail, i="<<i<<" j="<<j<<" ii="<<ii<<endl;
                    }
                }
                ans+=dfs(i+dy[ii], j+dx[ii]);
            }
        }
        return ans;
    };

    vi ans;
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) 
            if(!vis[i][j])
                ans.push_back(dfs(i,j));

    sort(all(ans), greater<int>());
    for(int i=0; i<ans.size(); i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
