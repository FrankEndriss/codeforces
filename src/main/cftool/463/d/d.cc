/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to maintain a list of lengths of prefixes.
 * Whenever a number is after a previous number
 * in all perms we increase the length of that prefix.
 * For every number we add a new prefix of the max len
 * so far.
 *
 * Note: It is longest _common_ subsequence, not longet _increasing_ subsequence.
 */
void solve() {
    cini(n);
    cini(k);

    vvi a(k, vi(n));
    vvi pos(k, vi(n+1));
    for(int i=0; i<k; i++)
        for(int j=0; j<n; j++) {
            cin>>a[i][j];
            pos[i][a[i][j]]=j;
        }

    vi pre(n+1,1);    // pre[i]= max len of LCS ending in element i 

    int ans=1;
    for(int i : a[0]) {
        int len=1;

        for(int j: a[0]) {
            int cnt=0;
            for(int i1=0; i1<k; i1++)
                cnt+=pos[i1][j]<pos[i1][i];

            if(cnt==k) {
                len=max(len, pre[j]+1);
                ans=max(ans, len);
            }
        
        }
        pre[i]=len;
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
