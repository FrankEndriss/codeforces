/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Just sort ???
 * Ok, its aprils fools contest, and rejected on testcase 1.
 * So, what could be acceptable?
 */
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));

    int x =  clock() * 1000LL / CLOCKS_PER_SEC;
        while ( clock() * 1000LL / CLOCKS_PER_SEC < x + 1100)
                {
                            x = x;
                }
    //std::this_thread::sleep_for (std::chrono::milliseconds(1100));
    for(int i=0; i<n; i++) 
        cout<<a[i]<<" ";
    cout<<endl;
}

signed main() {
        solve();
}
