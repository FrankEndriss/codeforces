/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;
using vvs= vector<vs>;

#define endl "\n"

/**
 */
void solve() {
    vector<set<string>> a(3);

    cini(n);
    for(int i=0; i<3; i++) {
        for(int j=0; j<n; j++) {
            cins(s);
            a[i].insert(s);
        }
    }

    vi ans(3);
    for(int i=0; i<3; i++) {
        for(string s : a[i]) {
            int cnt=0;
            for(int j=0; j<3; j++) {
                if(i==j)
                    continue;
                cnt+=a[j].count(s);
            }

            if(cnt==0)
                ans[i]+=3;
            else if(cnt==1)
                ans[i]+=1;
        }
    }

    for(int i=0; i<3; i++) 
        cout<<ans[i]<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
