/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * build some sum formular?
 * brute force should also work.
 * ...
 * No, its complete other question.
 * We need to count the number of given rects in initial list
 * that fit the two rects given in each query.
 * ...
 * Also we need to calc the area.
 *
 * That is 2D-Fenwick?
 */
const int N=1e3+3;
void solve() {
    vvi hw(N);

    cini(n);
    cini(q);
    for(int i=0; i<n; i++)  {
        cini(h);
        cini(w);
        hw[h].push_back(w);
    }

    vvi hw2(N);
    for(int i=1; i<N; i++)  {
        sort(all(hw[i]));
        hw2[i].push_back(0);
        for(size_t j=0; j<hw[i].size(); j++)
            hw2[i].push_back(i*hw[i][j]+hw2[i].back());
    }

    for(int i=0; i<q; i++) {
        cini(hs);
        cini(ws);
        cini(hb);
        cini(wb);
        int ans=0;
        for(int h=hs+1; h<hb; h++) {
            auto it1=upper_bound(all(hw[h]), ws);
            auto it2=lower_bound(all(hw[h]), wb);
            ans+=hw2[h][distance(hw[h].begin(), it2)]-hw2[h][distance(hw[h].begin(), it1)];
        }
        cout<<ans<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
