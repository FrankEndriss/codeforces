/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Odd indexes can be one more than even.
 * Note that one 0 is ok.
 *
 * So, one group can have bit 31 set in all numbers,
 * other group in 0 or 1 number.
 * Then simply use 1..n/2
 *
 * consider case n=6, so both groups are odd number of elements:
 */
void solve() {
    cini(n);

    vi ans(n);
    int x=0;
    for(int i=0; i<n; i+=2) {
        ans[i]=(i+1)+(1LL<<30);
        x^=ans[i];
    }
    int x2=0;
    for(int i=1; i+2<n; i+=2) {
        ans[i]=(i+1);
        x2^=ans[i];
    }
    if(n&1) {
        ans[n-2]=x^x2;
    } else {
        ans[n-1]=x^x2;
    }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
