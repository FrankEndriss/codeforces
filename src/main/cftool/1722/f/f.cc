/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;
using vvs= vector<vs>;

#define endl "\n"

/**
 * Brute force check all cells.
 *
 * ...;) Its an implementation prob.
 * How to do this without becomming crazy?
 *
 * Lets draw the surrounding area, there are only
 * 4 such patterns.
 *
 * ...still WA :/
 * Edgcase n==1 || m==1?
 * ...well, I am happy that this is not rated for me :)
 */
void solve() {
    cini(n);
    cini(m);

    vvs p={
        { 
        "....",
        ".**.",
        ".*..",
        "...." },
        { 
        "....",
        ".**.",
        "..*.",
        "...." },
        { 
        "....",
        "..*.",
        ".**.",
        "...." },
        { 
        "....",
        ".*..",
        ".**.",
        "...." },
    };

    vs s(n+3);
    for(int i=0; i<n; i++)  {
        cin>>s[i+1];
        s[i+1]="."+s[i+1];
    }

    for(size_t i=0; i<s.size(); i++) {
        while(s[i].size()<m+3)
            s[i]+='.';
    }

    function<bool(int,int)> go=[&](int i, int j) {
        for(int k=0; k<4; k++) {
            bool ok=true;
            for(int ii=0; ii<4; ii++) 
                for(int jj=0; jj<4; jj++) {
                    if(p[k][ii][jj]!=s[i+ii][j+jj])
                        ok=false;
                }
            if(ok)
                return true;
        }
        return false;
    };

    vvb vis(n+2, vb(m+2));

    function<void(int,int)> govis=[&](int i, int j) {
        for(int ii=0; ii<4; ii++) 
            for(int jj=0; jj<4; jj++)
                vis[i+ii][j+jj]=true;
    };

    for(int i=1; i<=n; i++) {
        for(int j=1; j<=m; j++) {
            if(vis[i][j])
                continue;

            if(s[i][j]=='.')
                continue;

            if(i==n) {
                cout<<"No"<<endl;
                return;
            }

            if(go(i-1,j-1)) {
                govis(i-1,j-1);
                continue;
            }

            if(j-2>=0 && go(i-1, j-2)) {
                govis(i-1,j-2);
                continue;
            }

            cout<<"No"<<endl;
            return;
        }
    }

    cout<<"Yes"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
