/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

bool cmp(string& s1, int s1begin, int s1len, string &s2, int s2begin, int s2len) {
    int same=true;
    for(int i=0; same && i<s1len; i++)
        same=s1[s1begin+i]==s2[s2begin+i];
    return same;
}

bool iseq(string& s1, int s1begin, int s1len, string &s2, int s2begin, int s2len) {

    if(cmp(s1, s1begin, s1len, s2, s2begin, s2len))
        return true;

    if(s1len%2!=0)
        return false;

    return  (iseq(s1, s1begin, s1len/2, s2, s2begin+s2len/2, s2len/2) &&
            iseq(s1, s1begin+s1len/2, s1len/2, s2, s2begin, s2len/2))
            ||
            (iseq(s1, s1begin, s1len/2, s2, s2begin, s2len/2) &&
            iseq(s1, s1begin+s1len/2, s1len/2, s2, s2begin+s2len/2, s2len/2));
}

void solve() {
    cins(s1);
    cins(s2);

    if(iseq(s1, 0, s1.size(), s2, 0, s2.size()))
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
