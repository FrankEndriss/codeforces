/**
 *  * Dont raise your voice, improve your argument.
 *   * --Desmond Tutu
 *    */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* use a[0] as top edge, a[1] is the 
 * one right of it, a[5] the one left.
 **/
void solve() {
    cinai(a,6);
    assert(a[1]+a[2]==a[5]+a[4]);
    int ans=0;
    int cnt=a[0]*2-1;
    for(int i=0; i<a[5]+a[4]; i++) {
        if(i<a[5])
            cnt++;
        if(i<a[1])
            cnt++;

        ans+=cnt;

        if(i>=a[5])
            cnt--;
        if(i>=a[1])
            cnt--;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
//
