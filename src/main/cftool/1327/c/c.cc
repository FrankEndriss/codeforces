/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* For every chip
 * Consider row
 * if the chip is above (ie dest>sourc) we need to move the board
 * up by dest-1 rows.
 * Same true for cols.
 * So we could check that for every chip independend.
 * But, how to combine chips?
 * How to find minimim num of shifts to move all chips to dest?
 *
 * We could move all chips into one place, then move the board 
 * until that cell was on all dests.
 * But how to calculate the min number of moves?
 * ...
 * Binary search... somehow?
 * ---
 * We can move any chip everywhere, with at most n+m moves.
 *
 *
 * */
void solve() {
    cini(n);
    cini(m);

    string ans;
    for(int i=0; i<n-1; i++) 
        ans+='D';
    for(int j=0; j<m-1; j++)
        ans+='L';

    char dir='R';

    bool first=true;
    for(int i=0;i<n; i++) {
        if(!first) {
            ans+='U';
        }
        first=false;

        for(int j=0; j<m-1; j++) {
            ans+=dir;
        }
        if(dir=='R')
            dir='L';
        else
            dir='R';
    }
    cout<<ans.size()<<endl;
    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

