/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

/* for every next digit 1 digit will increase the current blocksize, 
 * and 9 digits will start a new block.
 *
 * There are 10 blocks of len n
 * 2*9*10 blocks of len n-1;
 * 3*9*9*10 blocks of len n-2;
 * ...
 * n*9^(n-1)*10 blocks of len n-(n-1)==1
 *
 *
 */
void solve() {
    cini(n);

/*
    vvi dp(n, vi(10)); // dp[i][j]== number of blocks of len j+1 ending at idx i
    dp[0][0]=10;
    for(int i=1; i<n; i++) {
        for(int j=9; j>=1; j--) {
            dp[i][j]=dp[i-1][j-1]*9;
        }
        dp[i][0]=10;
    }
*/

    vi ans;
    ll sum1=0;
    ll sum=0;
    for(int i=0; i<n; i++) {
        int all=mul(i+1, toPower(10, i+1));
        int a=pl(all, -sum);
        ans.push_back(a);
        sum1=pl(sum1, a);
        sum=pl(sum, a);
        sum=pl(sum, sum1);
    }

    for(int i=n-1; i>=0; i--)
        cout<<ans[i]<<" ";
    cout<<endl;


}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

