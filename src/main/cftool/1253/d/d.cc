/** Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h> using namespace std;

#define DEBUG #ifndef DEBUG #define endl "\n" #define log(args...) #else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', '
');
stringstream _ss(_s);
istream_iterator<string> _it(_ss);
logf(_it, args);
}
void logf(istream_iterator<string> it) { } template<typename T, typename ...
        Args> void logf(istream_iterator<string> it, T a, Args ... args)
{
    cout << *it
         << " = " << a << endl;
    logf(++it, args...);
} #endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++) #define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0;
i<(n);
i++)

#define cins(s) string s; cin>>s; #define cini(i) int i; cin>>i; #define
cinll(l) ll l;
cin>>l;
#define cind(d) double d; cin>>d; #define cinai(a, n) vi
a(n);
fori(n)
{
    cin>>a[i];
} #define cinall(a, n) vll a(n); fori(n) {
    cin>>a[i];
} #define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; } #define
cinad(a, n) vd a(n);
fori(n)
{
    cin>>a[i];
}

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef
vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef
vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef
vector<ll> vll;

struct Dsu {
    vector<int> p;
    /* initializes each (0..size-1) to single set */
    Dsu(int size)
    {
        p.resize(size);
        for(int i=0; i<size; i++) p[i]=i;
    }

    /* finds set representative for member v */
    int find_set(int v)
    {
        if (v == p[v]) return v;
        return p[v] =
                   find_set(p[v]);
    }

    /* makes v a single set, removes it from other set if contained there. */
    void make_set(int v)
    {
        p[v] = v;
    }

    /* combines the sets of two members.  Use the bigger set as param a for
    * optimized performance.  */
    void union_sets(int a, int b)
    {
        a = find_set(a);
        b = find_set(b);
        if (a !=
                b) p[b] = a;
    }
};

/* We need to find for all components min and max node.  Then for all nodes
 * min..max check if in same or other component.  if other, ans++ and union
 * components.
 */
void solve()
{
    cini(n);
    cini(m);
    Dsu dsu(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        dsu.union_sets(u-1, v-1);
    }
    map<int, int> maxN; // max node per set
    for(int i=0; i<n; i++) maxN[dsu.find_set(i)]=i;

    vi vis(n);
    int ans=0;
    for(int i=0; i<n; i++) {
        int s=dsu.find_set(i);
        if(vis[s]) continue;
        vis[s]=true;
        int ma=maxN[s];
        for(int j=i; j<ma; j++) {
            int
            s2=dsu.find_set(j);
            if(s2!=s) {
                ans++;
                ma=max(ma, maxN[s2]);
                dsu.union_sets(s, s2);
            }
        }
    }

    cout<<ans<<endl;
}

int main()
{
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

