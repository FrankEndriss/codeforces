/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


using cd = complex<double>;

void fft(vector<cd> &a, bool invert) {
	int n = a.size();
	if (n == 1)
		return;

	vector<cd> a0(n / 2), a1(n / 2);
	for (int i = 0; 2 * i < n; i++) {
		a0[i] = a[2 * i];
		a1[i] = a[2 * i + 1];
	}
	fft(a0, invert);
	fft(a1, invert);

	double ang = 2 * PI / n * (invert ? -1 : 1);
	cd w(1, 0), wn(cos(ang), sin(ang));
	for (int i = 0; 2 * i < n; i++) {
		a[i] = a0[i] + w * a1[i];
		a[i + n / 2] = a0[i] - w * a1[i];
		if (invert) {
			a[i] /= 2;
			a[i + n / 2] /= 2;
		}
		w *= wn;
	}
}

/* see https://cp-algorithms.com/algebra/fft.html */
vector<int> multiply(vector<int> const &a, vector<int> const &b, vector<int> &result) {
	vector<cd> fa(a.begin(), a.end()), fb(b.begin(), b.end());
	size_t n = 1;
	while (n < a.size() + b.size())
		n <<= 1;
	fa.resize(n);
	fb.resize(n);

	fft(fa, false);
	fft(fb, false);
	for (size_t i = 0; i < n; i++)
		fa[i] *= fb[i];
	fft(fa, true);

	result.assign(n, 0);
	for (size_t i = 0; i < n; i++)
		result[i] = round(fa[i].real());
	return result;
}

/* see also
 * https://ikatanic.github.io/2018/09/22/polynomial-multiplication/
 * https://de.wikipedia.org/wiki/Sch%C3%B6nhage-Strassen-Algorithmus
 * https://everything2.com/title/Strassen+algorithm+for+polynomial+multiplication
 * TODO implement Karatsuba algorithm
 * TODO implement strassen
 */

/* if c[0]%p==0 then a[0]%p==0 || b[0]%p==0
 * c[1]=a[0]*b[1] + a[1]*b[0]
 * ...
 * There must be at least one a[i1] and one b[i2]
 * not divi by p.
 * We need to find a c[i3] where a[i1] contributes, but not b[i2]
 * or vice versa.
 */
void solve() {
    cini(n);
    cini(m);
    cini(p);

    cinai(a,n);
    cinai(b,m);

    int aidx=-1;
    for(size_t i=0; i<a.size(); i++) {
        if(a[i]%p!=0) {
            aidx=i;
            break;
        }
    }
    int bidx=-1;
    for(size_t i=0; i<b.size(); i++) {
        if(b[i]%p!=0) {
            bidx=i;
            break;
        }
    }

    cout<<aidx+bidx<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
