/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* we can choose nCr(n,p) people on p! position permutations,
 * and additionally nCr((n-p), k) people for the audience.
 * n==1e5
 * p<=7
 * k==1e5
 *
 * Obs
 * Having choosen the team, we simply add the "best" audience.
 * We can repeat that p times, removing the best audiencer
 * each time for having him in audience, then use max.
 *
 * So, we need to find the best teams.
 * For p==1 trivial, use best.
 * for p==2
 */
void solve() {
    cini(n);
    cini(p);
    cini(k);
    vector<pii> a;
    for(int i=0; i<n; i++) {
        cin>>a[i].first;
        a[i].second=i;
    }

    vvi s(n, vi(p));
    vector<pair<pii,pii>> worth;
    for(int i=0; i<n; i++) 
        for(int j=0; j<p; j++) {
            cin>>s[i][j];
            worth.push_back({ { s[i][j], -a[i].first }, { i, j}});
        }

    sort(wort.begin(), worth.begin()+p*p, worth.end(), greater<int>());

/* now find the best team.
 */
    set<pii> tmembers;
    map<int,vi> tpos;  // tpos[i]=possible possitions for member i
    for(int i=0; i<p*p; i++) {
        int memb=worth[i].second.first;
        tmembers.insert(memb);
        tpos[worth[i].second.second].push_back(memb);

        if(tmembers.size()==2*p)
            break;
    }

/* now check all possible compinations.
 * we do this by pos.
 * on every pos we use the possible players.
 * */
    vi team(p, -1);     // team by positions
    set<int> teamset;
    function<void(int,int)> perm=[&](int pos) {
        bool taken=false;
        for(int m : tpos[pos]) {
            if(teamset.count(m)==0) {
                team[pos]=m;
                teamset.insert(m);

                if(pos+1<p)
                    perm(pos+1);
                else {  // 
                }
                teamset.erase(m);
            }
        }
    }
    
    

    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

