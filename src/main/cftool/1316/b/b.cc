/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void test() {
    string s1="123456789";
    int n=s1.size();

        for(int k=1; k<=n; k++) {
            string s2=s1;
            for(int j=0; j+k<=n; j++) {
                reverse(s2.begin()+j, s2.begin()+j+k);
            }
            cout<<"k="<<k<<" s2="<<s2<<endl;
        }
}

void solve() {
    cini(n);
    cins(s);
    string t=s;
    int ans=1;

    for(int i=2; i<=n+1; i++) {
        string s2=s;
        if(((s.size()+i)%2)==0)
            reverse(s2.begin(), s2.begin()+i-1);
//cout<<"i="<<i<<" after rev, s2="<<s2<<endl;
        rotate(s2.begin(), s2.begin()+i-1, s2.end());
//cout<<"i="<<i<<" after rot, s2="<<s2<<endl;

//cout<<"s2="<<s2<<endl;
        if(s2<t) {
            ans=i;
            t=s2;
        }
    }
    cout<<t<<endl;
    cout<<ans<<endl;
    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
//test();
//return 0;
    cini(t);
    while (t--)
        solve();
}

