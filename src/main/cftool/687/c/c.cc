
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vvvb= vector<vvb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Knapsack sets of coins.
 * But add not only single coins, but for each coin value
 * all possible numbers of coins separatly.
 * As result we get a list of possible coin combinations
 * for k.
 * Then knapsack each such coin combination for find
 * all possible x.
 * ***
 * no :/
 * dp[i][j][k]=true if there is a subset build by first i coins
 * with sum==j and a subset sum==k
 */
const int N=501;
void solve() {
    cini(n);
    cini(k);
    cinai(c,n);

    vvb dp(k+1, vb(k+1));
    dp[0][0]=true;

    for(int i=0; i<n; i++) {
        vvb dp0=dp;
        for(int j=c[i]; j<=k; j++) {
            for(int l=0; l<=k; l++) {
                if(dp[j-c[i]][l]) {
                    dp0[j][l]=true;
                    dp0[j][c[i]]=true;
                    if(l+c[i]<=k)
                        dp0[j][l+c[i]]=true;
                }
            }
        }
        dp.swap(dp0);
    }

    vi vans;
    for(int l=0; l<=k; l++) 
        if(dp[k][l])
            vans.push_back(l);

    cout<<vans.size()<<endl;
    for(int i : vans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
