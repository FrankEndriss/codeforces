/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that number of testcases is small.
 * Also, the number of races is limited, since in each race
 * the splip prop increases by at least 0.05,
 * which means after max 20 races its over.
 * Since each race has only 2 outcomes that generated at most
 * 2^20 = ~1e6 states.
 * We should be able to iterate them.
 *
 *
 * ...
 * However, its hard to get rid of precission errors :/
 */
const ld EPS=0.0000000001;
void solve() {
    cind(c);
    cind(m);
    cind(p);
    cind(v);

    using t3=tuple<ld,ld,ld>;
    vector<t3> dp;  /* <prop,c,m> */
    dp.emplace_back((ld)1,c,m);

    ld ans=0;
    for(int i=1; i<22; i++) {
        cerr<<"i="<<i<<" dp.size()="<<dp.size()<<endl;
        vector<t3> dp0;
        for(size_t j=0; j<dp.size(); j++) {
            auto [p0,c0,m0]=dp[j];
            if(p0==0)
                continue;

            const ld pp=1.0-c0-m0;
            ans+=pp*i*p0;

            if(c0+m0<EPS)
                continue;

            if(c0==0 && m0>0) {
                ld nval=max((ld)0, m0-v);
                if(nval<EPS)
                    nval=0;
                dp0.emplace_back(
                    p0*m0,
                    0,
                    nval);
            } else if(c0>0 && m0==0) {
                ld nval=max((ld)0, m0-v);
                if(nval<EPS)
                    nval=0;
                dp0.emplace_back(
                    p0*c0,
                    nval,
                    0);
            } else {
                //if(p0*m0>EPS)
                dp0.emplace_back(   /* m wins */
                    p0*m0,
                    c0+min(m0,v)/2,
                    max((ld)0, m0-v));

                //if(p0*c0>EPS)
                dp0.emplace_back(    /* c wins */
                    p0*c0,
                    max((ld)0, c0-v),
                    m0+min(c0,v)/2);
            }
        }
        dp.swap(dp0);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
