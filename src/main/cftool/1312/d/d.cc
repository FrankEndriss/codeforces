/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

const int N=2e5+7;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    assert(n>=1 && n<N && k<=n);
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}


/* There is a max element somewhere in middle.
 * array be like left,middle,right
 * Exactly one element of left must be eq one of right
 *
 * Min middle element: n-1 since there must be n-2 distinct elements in left/right.
 * Position of middle can be anywhere so that L/R are not empty (since double element).
 *
 * For the doubled element, just choose any one from left. All other on right side
 * are determined.
 *
 * Somehow this is wrong, TC 3.
 * And slow, we need to make inner loop some formular :/
 * -> Tutorial
 *  Array has n-1 distinct element out of m: nCr(m, n-1)
 *  There are 2^(n-3) possible permutations of these elements.
 *      n-1 minus the middle element minus the doubled element.
 *  The doubled element can be chooses in n-2 ways.
 *  ans=nCr(m,n-1) * (n-2) * 2^(n-3)
 */
void solve() {
    cini(n);
    cini(m);

    if(n==2) {  /* not ascending/descending */
        cout<<0<<endl;
        return;
    }

    
    int ans=nCr(m,n-1);
    ans=mul(ans, n-2);
    ans=mul(ans, toPower(2, n-3));
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
