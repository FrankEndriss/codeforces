/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* see tutorial, its a dp.
 * First we find for all (i,j) if it can be 
 * reduces to one element, and which value that would have.
 * dp[i][i]=a[i] since it is only one element
 * dp[i][j]=if(dp[i][k]==dp[k+1][j]) dp[i][k]+1
 *
 * Then we find the smallest number of segments covering
 * the whole array.
 * foreach dp2[i] ans=min(1+dp[x][n]);
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    /* dp[i][j]= if==-1 not reducable to one number, else number segment i,j reduces to. */
    vvi dp(n, vi(n, -2));

    function<int(int,int)> dpfirst=[&](int i, int j) {
        assert(i<=j);
        if(dp[i][j]>-2)
            return dp[i][j];

        if(i==j)
            return dp[i][j]=a[i];

        dp[i][j]=-1;
        for(int k=i; k<j; k++) {
            int vleft=dpfirst(i,k);
            if(vleft>-1) {
                int vright=dpfirst(k+1,j);
                if(vright==vleft) {
                    dp[i][j]=vleft+1;
                    break;
                }
            }
        }
        return dp[i][j];
    };

    /* dp2[i]== min number of segments covering array (i,n], -1 unknown */
    vi dp2(n, -1);
    dp2[n-1]=1;
    function<int(int)> cover=[&](int i) {
        if(dp2[i]>0)
            return dp2[i];

        dp2[i]=INF;
        for(int j=i; j<n; j++) {
            if(dp[i][j]>=0) {
                if(j<n-1) 
                    dp2[i]=min(dp2[i], 1+cover(j+1));
                else
                    dp2[i]=1;
            }
        }
        return dp2[i];
    };

    for(int i=0; i<n; i++)
        dpfirst(i,n-1);

    int ans=cover(0);
    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

