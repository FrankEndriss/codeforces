/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * First sol is to simply buy all needed products.
 * Then find the price for that.
 *
 * Then check the 2 rubel items by b[i], how much additional
 * items we need to buy to get a better price.
 *
 * Continue to buy products until it does not pay of any more.
 * But in that loop we maby go O(n^2)...
 * Try to implement, then see.
 *
 * ...
 * It is "for all _future_ purchases".
 * So, order does not matter, we pay b[i]*2 rubels anyway, and then get all
 * other items for 1 rubel.
 * ...No.
 * We greedy buy the biggest b[i] first, and all available one rubel products.
 */
void solve() {
    cini(n);
    vi a(n);
    vi b(n);

    multiset<pii> ba;
    for(int i=0;i<n; i++)  {
        cin>>a[i]>>b[i];
        ba.emplace(b[i],a[i]);
    }


    int ans=0;  /* paid price */
    int cnt=0;  /* bought items */
    while(ba.size()) {  /* buy each product */
        int need=ba.begin()->first-cnt;

        if(need<=0) {    /* can buy cheap */
            ans+=ba.begin()->second;
            cnt+=ba.begin()->second;
            ba.erase(ba.begin());
            //cerr<<"bought cheap, ans="<<ans<<" cnt="<<cnt<<endl;
        } else {
            auto it=ba.end();
            it--;
            int purch=min(need, min(it->first-cnt, it->second));
            //cerr<<"purch two, purch="<<purch<<"it->first="<<it->first<<" it=>second="<<it->second<<endl;
            ans+=purch*2;
            cnt+=purch;

            int bb=it->first;
            int aa=it->second;
            ba.erase(it);
            if(aa-purch>0)
                ba.emplace(bb,aa-purch);
        }
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
