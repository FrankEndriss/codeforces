/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 1. fini sees min((t/x,n-1) parts
 * 2. fini sees min((t/x,n-2) parts
 * 3. fini sees min((t/x,n-3) parts
 * ...
 *
 *
 * Example 1
 * 4 2 5
 * Max dissat= 5/2=2
 * Number of participant with max dissat= (4-2)*2
 * remaining num: k, so ans+=k*(k-1)/2
 *
 * Example 3
 * 3 3 10
 * Max dissat= 10/3=3
 *
 *
 *
 * 
 */
void solve() {
    cini(n);
    cini(x);
    cini(t);

    int k=t/x;  /* max dissat of a participant. */

    int ans=0;
    if(n>k)
        ans+=(n-k)*k;

    //cerr<<"n="<<n<<" x="<<x<<" t="<<t<<endl;
    //cerr<<"k="<<k<<" ans1="<<ans<<endl;

    k=min(k,n);
    ans+=k*(k-1)/2;

    cout<<ans<<endl;



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
