/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * We got an array, and want to find forach position
 * the max possible distance that element is moved away from
 * its current position if it is choosen as the median of 
 * one of the possible arrays that have its median at that
 * position.
 * The movement is that the array is sorted.
 *
 * To simulate that, we would have to iterate all positions, foreach
 * iterate all possible arrays, foreach
 * check the moving distance
 * That is O(n^2), at least.
 * ...
 * Consider a prefix of the whole array.
 * For len=1 it is trivial.
 * Then, what changes for len=i?
 * There are i*(i-1)/2 more subarrays (the ones ending in i).
 * How to find the contribution of that ones?
 */
void solve() {
}

signed main() {
    solve();
}
