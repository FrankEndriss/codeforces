/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* build groups, each has min and max student.
 * find size of gap between groups.
 * close as much gaps as possible.
 */
const int INF=1e18+7;
void solve() {
    cini(n);
    cini(k);
    cini(x);
    cinai(a,n);
    sort(all(a));

    vector<pii> g;  /* groups minidx,maxidx */
    g.emplace_back(0, -1);
    for(int i=1; i<n; i++) {
        if(a[i]-a[i-1]>x) {
            g.back().second=i-1;
            g.emplace_back(i, -1);
        }
    }
    g.back().second=n-1;

    vi gaps(g.size()-1);
    for(size_t i=1; i<g.size(); i++)
        gaps[i-1]=(a[g[i].first]-a[g[i-1].second]-1)/x;

    sort(all(gaps));
    int ans=g.size();
    //cerr<<"g.size()="<<g.size()<<endl;
    for(size_t i=0; i<gaps.size(); i++) {
        if(k>=gaps[i]) {
            ans--;
            k-=gaps[i];
            //cerr<<"closing gap of size="<<gaps[i]<<endl;
        } else 
            break;
    }

    cout<<ans<<endl;


}

signed main() {
    solve();
}
