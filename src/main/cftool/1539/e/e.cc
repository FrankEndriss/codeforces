/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* In each move we can replace left or right,
 * and that gives two possible outcomes.
 *
 * Consider only one hand.
 * In each move we can replace the card, and in some
 * moves we must replace the card.
 * dpL[i]=max ok query if we replace card in left hand in ith round
 * dpR[i]=max ok query if we replace card in right hand in ith round
 *
 * Then we need to find a path from 0,0... to "the end"?
 * What is the end here? Well, the last round.
 *
 * So, how do we "find a path"?
 *
 * Also note that we allways _must_ replace one of the cards.
 * And there is a max value m, not sure if relevant, I dont think so.
 ***
 * In each round L and R both are 
 * must replace, or
 * must not preplace, or
 * can replace
 * Obviously the only choice is if both are "can replace", with two outcomes.
 *
 * ...
 * Can we check from back to first?
 * Foreach round we can tell for L and R the latest round where
 * a change must have been done to fullfill the current rule.
 * If that latest is only for one of L,R the current round, then
 * that one is a must.
 * What if 
 */
void solve() {
    cini(n);
    cini(m);

    for(int i=0; i<n; i++) {
        cini(miL);
        cini(maL);
        cini(miR);
        cini(maR);

        // ???
    }

}

signed main() {
    solve();
}
