/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/**
 * Bob can choose of two colors.
 *
 * 1 2 3 1 2 3
 * 2 3 1 2 3 1
 * 3 1 2 3 1 2
 *
 * 1 2 1 2
 * 2 1 2 1 
 * 1 2 1 2
 *
 * Consider the last cell. To make B win there must be two possible
 * colors for that cell, else A would choose the single possible color.
 * That means, all 4 adj cells must be same color.
 * How to get there?
 *
 * Consider checker board.
 * If we can fill all white with one number, or all black with one
 * number then we are good, since the other color can be filled
 * with remaining numbers.
 * So allways try to put 1 onto white, else 2 onto black until one
 * of them is complete, then fill the remaining cells.
 */
void solve() {
    cini(n);
    vector<pii> w;
    vector<pii> b;
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++) {
            if((i+j)%2==0)
                w.emplace_back(i,j);
            else
                b.emplace_back(i,j);

        }

    for(int i=0; i<n*n; i++) {
        cini(a);
        if(a!=1 && w.size()>0) { /* 1 on white */
            cout<<1<<" "<<w.back().first+1<<" "<<w.back().second+1<<endl;
            w.pop_back();
        } else if(a!=2 && b.size()>0) { /* 2 on black */
            cout<<2<<" "<<b.back().first+1<<" "<<b.back().second+1<<endl;
            b.pop_back();
        } else {
            if(w.size()==0) {   /* only black avail, use 2 or 3 */
                int c=2;
                if(a==c)
                    c++;
                cout<<c<<" "<<b.back().first+1<<" "<<b.back().second+1<<endl;
                b.pop_back();
            } else {            /* only white avail, use 1 or 3 */
                int c=1;
                if(a==c)
                    c=3;
                cout<<c<<" "<<w.back().first+1<<" "<<w.back().second+1<<endl;
                w.pop_back();
            }
        }
    }
}

signed main() {
        solve();
}
