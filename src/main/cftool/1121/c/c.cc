/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Simulate that process.
 *
 * put k in queue with end times.
 *
 * The tests for ith sol run in [t, t+a[i]], where a[i] consecutive numbers are 'current'.
 * We can store these intervals.
 *
 * Lets simulate every second.
 * At start of second there is m value. Then all the current solutions test some test.
 * if it is test m then that sol is set to ans[sol]=true;
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vb ans(n);

    int sidx=0;
    vector<pair<int, pii>> proc;   /* proc[i]=<testfini,#test> */
    
    int m=0;
    while(m<n) {
        while(sidx<n && proc.size()<k) {
            proc.push_back({sidx, {0,a[sidx]}});
            sidx++;
        }

        int d=100.0*m/n+.5001;

        int mm=0;   /* finished submission in this round */
        vector<pair<int,pii>> proc1;
        for(size_t i=0; i<proc.size(); i++) {
            if(proc[i].second.first+1==d)  {
                ans[proc[i].first]=true;
            }
            if(proc[i].second.first+1<proc[i].second.second) {
                proc1.push_back({ proc[i].first, { proc[i].second.first+1, proc[i].second.second}});
            } else 
                mm++;
        }
        proc.swap(proc1);
        m+=mm;
    }
    int aa=0;
    for(int i=0; i<n; i++)
        if(ans[i])
            aa++;

    cout<<aa<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
