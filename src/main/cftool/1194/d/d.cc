/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Lets first ignore k, then
 * since 0 is loose position, win positions are
 * w: 1,2
 * l: 3,
 * w: 4,5
 * l: 6
 *
 * if k%3!=0 then nothing changes, since from no l position
 * another l position can be reached.
 *
 * if k%3==0 then position k is a win instead of loose.
 * And k+1 is the next loose instead of k. Then k+2 and k+3 are wins...
 * so the pattern shifts one to right.
 * Then again, after k/3 triples the same happens again...etc.
 */
void solve() {
    cini(n);
    cini(k);

    if(k%3!=0) {
        if(n%3==0)
            cout<<"Bob"<<endl;
        else
            cout<<"Alice"<<endl;
    } else {
        int shifts=(n+1)/(k+1);
        n-=shifts;
        if(n%3==0)
            cout<<"Bob"<<endl;
        else
            cout<<"Alice"<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
