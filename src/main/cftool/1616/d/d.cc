/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We select all elements on all subarrays that have second condition fullfilled.
 * Then on the remaining segments every second.
 *
 * So, how to find the max size subarrays for the second condition (avg)?
 *
 * Consider a[i]. We need to find max j so that 
 * sum(a[i]..a[j]) >= x*(j-i+1)
 *
 * pre[j]-pre[i-1] >= x*(j-i+1)
 * pre[j] >= x*(j-i+1)+pre[i-1]
 * pre[i-1] >= pre[j]- x*(j-i+1)
 *
 * Consider sorting the prefix sums by value, then find
 * in the segment above the max j.
 * Maintain the indexes in segment tree.
 * ***
 * Other problem :/
 * If there is an subarray that does _not_ fullfill rule2, then
 * we must not select a whole segment containing it.
 *
 * So, again, how to find the _not selectable_ segments?
 * Well, it contains at least one a[i]<x.
 * So start at first a[i]<x, then extends to left, and extend to right.
 * Repeat.
 * ****
 * No, its still another problem :/
 */
void solve() {
	cini(n);
	cinai(a,n);
	cini(x);

	vb ans(n);	/* ans[i]=true -> cannot select pos i for rule2 */
	int i=0;
	while(i<n) {
		if(a[i]<x) {
			int sum=a[i];
			for(int j=i-1; j>=0; j--) {
				sum+=a[j];
				if(!ans[j] && sum<x*(i-j+1)) {
					ans[i]=true;
					ans[j]=true;
				} else
					break;
			}
			sum=a[i];
			for(int j=i+1; j<n; j++) {
				sum+=a[j];
				if(sum<x*(j-i+1)) {
					ans[i]=true;
					ans[j]=true;
					i=j+1;
				} else
					break;
			}
		}
		i++;
	}

	int cnt=0;
	for(int i=0; i<n; i++) {
		if(i>0 && ans[i] && ans[i-1])
			ans[i]=false;

		cnt+=(!ans[i]);
	}
	cout<<cnt<<endl;

}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
