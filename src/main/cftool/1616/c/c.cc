/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * What wiered problem construction :/
 * We want to make all subarrays sums equal to (l+r)/2 * len
 * So all deltas must be same.
 *
 * In the end the array looks like
 * a[0], a[0]+d, a[0]+2*d, ... a[0]+(n-1)*d
 *
 * Note that it is allways possible to subst n-2 elements.
 *
 * So check all pairs of elements as two unchanged ones.
 * Then find check all possible n-2 values for d.
 * ...But that O(n^5) at least :/
 * Maybe it work nonetheless, just give it a try.
 *
 * How to check if a a[k] is one of the values for current i,j,pi,pj?
 * -> Calc if if fits a[i]+d*y, then calc the position.
 *  It it is different than prev value, and withing range 0..n-1, 
 *  then no need to change that element.
 *  ***
 *  Note that we must not sort the array :/
 *  Consider a[0] and a[1]
 */
void solve() {
	cini(n);
	cinai(a,n);

	int ans=0;
	for(int i=0; i<n; i++) {
		for(int j=i+1; j<n; j++) {	/* i and j do not change, check all other values */
			for(int k=0; k<n; k++) {
				if(k==i || k==j) {
					ans++;
					continue;
				}
				pii d={a[j]-a[i], j-i};
				int x=a[i]-(a[i]*(j-i))
					...some simple fraction calculation :/
			}
		}
	}
	cout<<n-ans<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
