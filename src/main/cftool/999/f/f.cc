/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

/* 
 * Obs: It is always better to distribute all cards
 * than distribute less.
 * We need to group the players by f[] value.
 * Then we have per f[] value 
 * -a number of players
 * -a number of cards
 * We need to find for that combination the optimal distribution
 * under given h[].
 * We try all possible distributions, that is we give the first 
 * player the max amount, and all next players get same or less.
 * dp[i][j]=max possible ans if starting at ith player with j cards.
 *
 * We need to do this for every cards/player combination separate.
 * Note that the number of combinations is much less than n!
 *
 */
void solve() {
    cini(n);
    cini(k);
    cinai(c,n*k);
    cinai(f,n);
    cinai(h,k);

    map<int,int> ff;
    for(int i=0; i<n; i++)
        ff[f[i]]++;

    map<int,int> fc;
    for(int i=0; i<n*k; i++)
        fc[c[i]]++;

/* loop over all combinations of players/cards foreach find
 * optimal distribution of fc[ent.first] cards among 
 * ent.second players. */
    int ans=0;
    for(auto ent : ff) {
        int players=ent.second;
        int cards=fc[ent.first];
        if(cards==0)
            continue;

/* dp[i][j][k]=maxval if begin at ith player with ca cards remaining and each player
 * get max macnt cards. 
 * Note that we need not recurse if remaining player times macnt<ca.
 * */
        vvvi dp(players, vvi(cards+1, vi(k+1, -1)));

        function<int(int,int,int)> calc=[&](int pl, int ca, int macnt) {
            if(dp[pl][ca][macnt]>=0)
                return dp[pl][ca][macnt];

            if(ca==0 || macnt==0) {
                return dp[pl][ca][macnt]=0;
            } else if(ca==1) {
                return dp[pl][ca][macnt]=h[0];
            }

            if(pl==players-1)
                return dp[pl][ca][macnt]=h[min(ca, macnt)-1];

/* loop over number of cards current player gets */
            dp[pl][ca][macnt]=0;
            for(int cnt=min(ca, macnt); cnt>=1; cnt--) {
                dp[pl][ca][macnt]=max(dp[pl][ca][macnt], h[cnt-1]+calc(pl+1, ca-cnt, cnt));
            }
            return dp[pl][ca][macnt];
        };

        ans+=calc(0, cards, k);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
