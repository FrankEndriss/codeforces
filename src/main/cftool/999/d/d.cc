/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We have n elements, and want to make 
 * n/m haveing a[i]%m=0, n/m haveing a[i]%m=1 etc
 * until n/m having a[i]%m=m-1
 *
 * We can reduce all number a[i]%=m, since the absolute value
 * does not matter at all.
 *
 * Find freq of a[i]%m, then find the position in the circular list
 * of freqs where all prefix sums are positive. O(2*n)
 *
 * Then start at that position, and "move" the overcounting elements
 * to the next position, for one full circle. Then there are no more overcountings.
 *
 * How to construct the ans array of numbers?
 * While moving the elements arround, do maintain them as a list of original 
 * indexes. 
 */
void solve() {
    cini(n);
    cini(m);
    const int sz=n/m;

    cinai(a,n);

    vvi f(m);
    for(int i=0; i<n; i++)
        f[a[i]%m].push_back(i);

    vi ff(2*m);
    for(int i=0; i<2*m; i++) 
        ff[i]=f[i%m].size();

    /*
    cerr<<"ff[]=";
    for(int i : ff)
        cerr<<i<<" ";
    cerr<<endl;
    */


    int idx=0;
    int sum=0;
    for(int i=0; i<2*m; i++) {
        if(sum<0){
            sum=0;
            idx=i;
        }
        sum+=ff[i]-sz;

        if(i-idx==m)
            break;
    }
    //cerr<<"idx="<<idx<<endl;



    int ans=0;
    vi inc(n);
    vi over;
    for(int i=0; i<m; i++) {
        const int cur=(idx+i)%m;
        if(f[cur].size()>sz) {
            for(size_t k=sz; k<f[cur].size(); k++)
                over.push_back(f[cur][k]);
        } else if(f[cur].size()<sz) {
            for(size_t k=f[cur].size(); k<sz; k++) {
                int lidx=over.back();
                over.pop_back();

                inc[lidx]=(i+idx-(a[lidx]%m)+m)%m;
            }
        }
        ans+=over.size();
    }

    cout<<ans<<endl;
    for(int i=0; i<n; i++) 
        cout<<a[i]+inc[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
