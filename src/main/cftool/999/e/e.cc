/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * First mark cities reacheable from capitol.
 * Then dfs/connect all  vertex without an incoming edge.
 * Then connect the remaining circles.
 *
 * Does not work...
 * Since there can be circles with "outgoing tails". In that 
 * case we must first start at a vertex part of a circle, not
 * one part of the tail. 
 * So we need to a "find cirlce" phase. TODO
 */
void solve() {
    cini(n);
    cini(m);
    cini(s); s--;

    vvi adj(n);
    vi cntR(n);

    for(int i=0; i<m;i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        cntR[v]++;
    }

    vb vis(n);

    function<void(int,int)> dfs=[&](int v, int p) {
        if(vis[v])
            return;

        vis[v]=true;
        for(int chl : adj[v]) 
            if(chl!=p)
                dfs(chl, v);
    };

    dfs(s, -1);

    int ans=0;
    for(int i=0; i<n; i++) {
        if(!vis[i] && cntR[i]==0) {
            ans++;
            dfs(i, -1);
        }
    }

    for(int i=0; i<n; i++) {
        if(!vis[i]) {
            ans++;
            dfs(i, -1);
        }
    }

    cout<<ans<<endl;
            

}

signed main() {
    solve();
}
