/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * We need to find pairs of p/k so that
 * max(dist(p,k)+dist(k,o)) is minimum
 *
 * Binary search?
 * Sort p by dist to o.
 * For each p use outmost k possible in time t.
 * Persons right of o use rightmost keys,
 * persons left of o use leftmost keys.
 */
void solve() {
    cini(n);
    cini(k);
    cini(p);

    set<int> pers;
    for(int i=0; i<n; i++) {
        cini(aux);
        pers.insert(aux);
    }
    set<int> keys;
    for(int i=0; i<k; i++) {
        cini(aux);
        keys.insert(aux);
    }

    int l=-1;
    int r=1e10;
    while(l+1<r) {
        int t=(l+r)/2;

        set<int> lpers=pers;
        set<int> lkeys=keys;

//cerr<<"new t="<<t<<endl;
        bool imp=false;
        while(lpers.size()) {
            auto it1=lpers.begin();
            auto it2=lpers.rbegin();
            int lp;
            if(abs(*it1-p)>=abs(*it2-p))
                lp=*it1;
            else
                lp=*it2;

            lpers.erase(lp);
            if(abs(lp-p)>t) {
                imp=true;
                break;
            }
//cerr<<"pers="<<lp<<endl;

            if(lp>=p) {
                /* use rightmost key possible to use.
                distance lp -> key -> p must not bt t
                */
                int d=lp-p;
                int mak=lp+(t-d)/2;
//cerr<<"mak="<<mak<<endl;
                auto it=lkeys.upper_bound(mak);
                if(it!=lkeys.begin()) {
                    it--;
                }
                if(abs(lp-*it)+abs(*it-p)>t) {
                    imp=true;
                    break;
                }
//cerr<<"key="<<*it<<endl;
                lkeys.erase(*it);
            } else {
                /* use leftmost key possible to use.
                distance lp -> key -> p must not bt t
                */
                int d=p-lp;
                int mik=lp-(t-d)/2;
                auto it=lkeys.lower_bound(mik);
                if(it==lkeys.end()) {
                    imp=true;
                    break;
                }
                if(abs(lp-*it)+abs(*it-p)>t) {
                    imp=true;
                    break;
                }
//cerr<<"key="<<*it<<endl;
                lkeys.erase(*it);
            }
        }
//cerr<<"imp="<<imp<<endl;
        if(imp)
            l=t;
        else
            r=t;
    }
    cout<<r<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
