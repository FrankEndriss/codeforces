/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Note that one team is build, so 
 * most likely a lot of students will
 * be on no team at all.
 *
 * Let pother=number of players in other
 * departments, and pwafa=players in wafas dep.
 *
 * So n-1 times a player is choosen,
 * and the the prop it is none of wafas
 * is pother/(pother+pwafa).
 */
void solve() {
    cini(n);
    cini(m);
    cini(h);

    cinai(s,m);
    int pwafa=s[h-1]-1;
    int pother=accumulate(s.begin(), s.end(), 0LL)-s[h-1];

    if(pother+pwafa+1<n) {
        cout<<-1<<endl;
        return;
    }

    double ans=1.0;
    for(int i=1; i<n; i++) {
        ans*=(1.0*pother)/(pother+pwafa);
        pother--;
        if(ans==0)
            break;
    }
    cout<<1.0-ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
