/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * sort by {a,b}
 * Then reconstruct swaps.
 * How...??? sic
 * I dont get it :/ Why it does not work?
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    cerr<<"n="<<n<<endl;
    using t3=tuple<int,int,int>; /* a,b,idx */
    vector<t3> p(n);
    for(int i=0; i<n; i++)  {
        p[i]={a[i],b[i],i};
    }


    /* implement fucking bubblesort... sic */

    vector<pii> ans;
    for(int j=0; j<n; j++) {
    for(int i=0; i+1<n; i++) {
        if(p[i]>p[i+1]) {
            swap(p[i],p[i+1]);
            ans.emplace_back(i+1, i+2);
        }
    }
    }

    for(int i=0; i+1<n; i++) {
        auto [a0,b0,ii0]=p[i];
        auto [a1,b1,ii1]=p[i+1];

        if(b0>b1) {
            cout<<"-1"<<endl;
            return;
        }
    }
    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++) 
        cout<<ans[i].first<<" "<<ans[i].second<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
