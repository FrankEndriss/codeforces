/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Maintain shortest path to the doors of next level.
 * Consider x to be rows.
 *
 * How to know the shortest path from door to door
 * on certain levels?
 */
void solve() {
    cini(n);

    vi d1x(n-1);
    vi d1y(n-1);
    vi d2x(n-1);
    vi d2y(n-1);
    for(int i=0; i<n-1; i++) 
        cin>>d1x[i]>>d1y[i]>>d2x[i]>>d2y[i];

    vi dpU(n-1,INF);   /* dpU[i]=shortest dist from 0,0 to door 1/x of layer i to i+1 */
    vi dpR(n-1,INF);   /* dpR[i]=shortest dist from 0,0 to door 2/y of layer i to i+1 */

    dpU[0]=dpR[0]=0;

    for(int i=1; i<n; i++) {
        dpU[i]=min(dpU[i], dpU[i-1]+abs(d1x[i]-d1x[i-1]);
        dpU[i]=min(dpU[i], dpR[i-1]+i+1-d2y[i-1]+i+1-d1x[i]);

        dpR[i]=min(dpR[i], dpR[i-1]+abs(d2y[i]=d2y[i-1]);
        dpR[i]=min(dpR[i], dpU[i-1]+i+1-d1x[i-1]+i+1-d1y[i]);
    }

    cini(m);
    for(int i=0; i<m; i++) {
        cini(x1);
        cini(y1);
        cini(x2);
        cini(y2);

        /* There are 4 possible paths:
         * start to one of the doors on start level,
         * then to one of the doors of dest level,
         * then to dest cell.
         * How to know the shortest path from door to door?
         */
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
