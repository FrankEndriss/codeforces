/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Observe that
 * -it is allways optimal to land directly behind an obstacle.
 * -if there is less than s way between two obstacles, it is like deadend.
 * -if landing directly behind an obstacle is possible, there is not
 *  better or less god start of the jump.
 *
 * So simulate, by finding for every ostacle the point where the jump
 * starts that makes us land there.
 *
 * also, define a forbidden obstacle as one where the position directly
 * right of it is blocked by the next obstacle.
 *
 * WA, some edgecase?
 * -> We need to be exact at iterating positions... sic implemantation :/
 */
void solve() {
    cini(n);
    cini(m);
    cini(s);
    cini(d);

    vi a(n+1);
    a[0]=-1;    /* artificial obstacle at -1 */
    for(int i=1; i<=n; i++)
        cin>>a[i];

    sort(all(a));

    vi ans(n+1, -1);
    ans[0]=-2;
    vi pre(n+1, -1);

    queue<int> q;
    q.push(0);  /* landed just behind a[0]=-1, ie at 0 */

    int done=0;
    while(q.size()) {
        int idx=q.front();
        q.pop();
        assert(idx<n);

        /* cannot jump any more from here */
        if(a[idx+1]==a[idx]+1 || a[idx+1]-(a[idx]+2)<s) 
            continue;

        int l=a[idx+1]-1;   /* jump from */
        int r=a[idx+1]-1+d; /* rightmost pos where we can jump */
        for(int j=done+1; j<=n && a[j]<r; j++) {
            done=j;
            ans[j]=l;
            pre[j]=idx;
            if(j<n)
                q.push(j);
        }
    }

    /* backtrack path */
    if(ans[n]<0)
        cout<<"IMPOSSIBLE"<<endl;
    else {
        stack<string> sts;
        stack<int> sti;

        if(m>a.back()+1) {
            sts.push("RUN");
            sti.push(m-(a.back()+1));
        }

        int idx=n;
        while(idx>0) {
            sts.push("JUMP");
            sti.push(a[idx]+1-ans[idx]);

            sts.push("RUN");
            sti.push(ans[idx]-(a[pre[idx]]+1));
            idx=pre[idx];
        }

        while(sts.size()) {
            cout<<sts.top()<<" "<<sti.top()<<endl;
            sts.pop();
            sti.pop();
        }
    }
}

signed main() {
    solve();
}
