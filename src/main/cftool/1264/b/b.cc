/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cinai(aa,4);
    const int sum=aa[0]+aa[1]+aa[2]+aa[3];
    /* start with 0 or 1, all others are deterministic */
    for(int i : {
                0, 1, 2, 3
            }) {
        vi ans;
        vi a=aa;
        while(a[0]||a[1]||a[2]||a[3]) {
            if(i>3)
                break;

            if(a[i]) {
                ans.push_back(i);
                a[i]--;
                if(i>0 && a[i-1])
                    i--;
                else
                    i++;
            } else
                break;
        }
        if(ans.size()==sum) {
            cout<<"YES"<<endl;
            for(int i : ans)
                cout<<i<<" ";
            cout<<endl;
            return;
        }
    }
    cout<<"NO"<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

