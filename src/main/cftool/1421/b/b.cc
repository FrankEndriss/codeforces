/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider first and last steps.
 */
void solve() {
    cini(n);
    cinas(s,n);

    int first=' ';
    if(s[0][1]==s[1][0]) {
        first=s[0][1];
    }

    int second=' ';
    if(s[n-1][n-2]==s[n-2][n-1]) {
        second=s[n-1][n-2];
    }

    int cnt=0;
    vector<pii> ans;
    if(first==' ') {
        if(second==' ') {   /* make first 1, second 0 */
            cnt=2;
            if(s[0][1]=='0') {
                ans.emplace_back(0,1);
                s[0][1]='1';
            } else {
                ans.emplace_back(1,0);
                s[1][0]='1';
            }

            if(s[n-1][n-2]=='1') {
                ans.emplace_back(n-1,n-2);
                s[n-1][n-2]='0';
            } else {
                ans.emplace_back(n-2,n-1);
                s[n-2][n-1]='0';
            }
        } else if(second=='0') {    /* make first 1 */
            if(s[0][1]=='0') {
                cnt++;
                s[0][1]='1';
                ans.emplace_back(0,1);
            }
            if(s[1][0]=='0') {
                cnt++;
                s[1][0]='1';
                ans.emplace_back(1,0);
            }
        } else if(second=='1') {    /* make first 0 */
            if(s[0][1]=='1') {
                cnt++;
                s[0][1]='0';
                ans.emplace_back(0,1);
            }
            if(s[1][0]=='1') {
                cnt++;
                s[1][0]='0';
                ans.emplace_back(1,0);
            }
        }
    } else if(first=='0') { /* make second both 1 */
        if(s[n-1][n-2]=='0') {
            cnt++;
            s[n-1][n-2]='1';
                ans.emplace_back(n-1,n-2);
        }
        if(s[n-2][n-1]=='0') {
            cnt++;
            s[n-2][n-1]='1';
                ans.emplace_back(n-2,n-1);
        }
    } else if(first=='1') { /* make second both 0 */
        if(s[n-1][n-2]=='1') {
            cnt++;
            s[n-1][n-2]='0';
                ans.emplace_back(n-1,n-2);
        }
        if(s[n-2][n-1]=='1') {
            cnt++;
            s[n-2][n-1]='0';
                ans.emplace_back(n-2,n-1);
        }
    }
    cout<<cnt<<endl;
    for(pii p : ans)
        cout<<p.first+1<<" "<<p.second+1<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
