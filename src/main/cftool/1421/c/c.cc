/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * op1 choose i, but i less than n-1
 * prepend substring(1, i)
 * op2 choose i, but i less than n
 * apend substring(i, n-i)
 *
 * abac
 * R2 -> abacab
 * R5 -> abacaba
 *
 * acccc
 * L4 -> cccacccc
 * L2 -> ccccacccc
 *
 * [1],[2..n-2],[n-1],[n]
 * [1],[2..n],[n-1]
 * [n..2],[1],[2..n],[n-1]
 * [n-1][n..2],[1],[2..n],[n-1]
 *
 * abac
 * R 3 -> abaca
 * L 4 -> cababaca
 * L 2 -> acababaca
 *
 */
void solve() {
    cins(s);
    int n=s.size();

    vs ans;

    if(n==3) {
        s=s+s[1];
        ans.push_back("R 2");
        n++;
    }

    cout<<3+ans.size()<<endl;
    for(int i=0; i<ans.size(); i++) 
        cout<<ans[i]<<endl;

    cout<<"R "<<n-1<<endl;
    cout<<"L "<<n<<" "<<endl;
    cout<<"L 2"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
