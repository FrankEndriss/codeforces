/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* First find minimum cost for single steps.
 *
 * Then we need to go abs(x) steps vertical,
 * and abs(y) steps horizontal.
 *
 * Smaller of both can be choosen. We can choose to use
 * two times the zero step operation, or once the inc and once the dec.
 */
void solve() {
    cini(x);
    cini(y);
    vi c(7);
    for(int i=1; i<=6; i++)
        cin>>c[i];

    for(int i=0; i<36; i++) {
        c[1]=min(c[1], c[2]+c[6]);
        c[2]=min(c[2], c[1]+c[3]);
        c[3]=min(c[3], c[2]+c[4]);
        c[4]=min(c[4], c[3]+c[5]);
        c[5]=min(c[5], c[4]+c[6]);
        c[6]=min(c[6], c[1]+c[5]);
    }

    /* now the shortest way is always cheapest */

    int ans=-1;
    if(x>=0) { 
        if(y>=0) {
            ans=c[1]*min(x,y);
            if(x>y) 
                ans+=c[6]*abs(x-y);
            else
                ans+=c[2]*abs(y-x);
        } else if(y<0) {
            ans=-c[5]*y + c[6]*x;
        } else 
            assert(false);
    } else if(x<0) {
        if(y<0) {
            ans=-c[4]*max(x,y);
            if(x<=y) {
                ans+=c[3]*abs(x-y);
            } else if(y<x) {
                ans+=c[5]*abs(x-y);
            } else
                assert(false);
        } else if(y>=0) {
            ans=-c[3]*x + c[2]*y;
        }
    } else 
        assert(false);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
