/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));

    priority_queue<int> q;
    int sum=accumulate(all(a), 0LL);
    q.push(sum);

    while(q.size()) {
        if(q.size()>a.size()) {
            cout<<"NO"<<endl;
            return;
        }

        int val=q.top();
        q.pop();
        if(val==a.back()) {
            a.pop_back();
        } else if(val<a.back()) {
            cout<<"NO"<<endl;
            return;
        } else {
            if(val<2) {
                cout<<"NO"<<endl;
                return;
            }
            int x=val/2;
            int y=val-x;
            q.push(x);
            q.push(y);
        }
    }
    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
