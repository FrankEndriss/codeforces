/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    set<int> a;

    for(int i=0; i<n; i++) {
        cini(k);
        a.insert(k);
    }

    n=(int)a.size();
    set<int> aaa=a;
    while(a.size()<=300) {
        set<int> aa;
        for(auto it=a.begin(); it!=a.end(); it++) {
            for(auto it2=aaa.begin(); it2!=aaa.end(); it2++) {
                int d=abs(*it-*it2);
                if(d!=0 && a.count(d)==0)
                    aa.insert(d);
            }
        }

        if(aa.size()==0)
            break;

        aaa.clear();
        for(int i : aa) {
            a.insert(i);
            aaa.insert(i);
        }
    }

    if(a.size()<=300) {
        cout<<"YES"<<endl;
        cout<<a.size()<<endl;
        for(int i : a)
            cout<<i<<" ";
        cout<<endl;
    } else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
