/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, vector<ll> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization.push_back(d);
            n /= d;
        }
    }
    if (n > 1)
        factorization.push_back(n);
}

/* Note that the ratio never changes, so first find it, in all prefixes.
 * To split a string and get same ratio in both halfs we need to split
 * the string by some integer divisable part, and each part then
 * must still have same ratio.
 * We simply to a dfs, that runs max N*logN*sqrt(N)
 * ...which is a lot :/
 * And it is not what is asked for.
 *
 * Note that each prefix has its ratio.
 * Then if we cut the shortest prefix of that ratio, then the 
 * postfix has same ratio.
 *
 * So, foreach prefix, we search the number of positions with same
 * ratio left of that position.
 */
void solve() {
    cini(n);
    cins(s);

    vvi cnt(2, vi(n));  /* ratio of prefix[i]= cnt[0][i]/cnt[1][i] */
    if(s[0]=='D')
        cnt[0][0]=1;
    else
        cnt[1][0]=1;

    for(int i=1; i<n; i++)  {
        cnt[0][i]=cnt[0][i-1];
        cnt[1][i]=cnt[1][i-1];
        if(s[i]=='D')
            cnt[0][i]++;
        else
            cnt[1][i]++;
    }

    for(int i=0; i<n; i++) {
        if(cnt[0][i]==0 && cnt[1][i]!=0)
            cnt[1][i]=1;
        else if(cnt[0][i]!=0 && cnt[1][i]==0)
            cnt[0][i]=1;
        else {
            int g=gcd(cnt[0][i], cnt[1][i]);
            cnt[0][i]/=g;
            cnt[1][i]/=g;
        }
    }

    map<pii,int> memo;
    for(int i=0; i<n; i++) {
        cout<<memo[{cnt[0][i],cnt[1][i]}]+1<<" ";
        memo[{cnt[0][i],cnt[1][i]}]++;
    }
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
