/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* add/remove elements, get median() element in O(1) */
struct Median {
    multiset<int> lo;    // smaller elements
    multiset<int> hi;    // bigger elements
    int sumlo=0;
    int sumhi=0;

    void norm() {
        while(lo.size()+1<hi.size()) {
            auto it=hi.begin();
            sumlo+=*it;
            sumhi-=*it;
            lo.insert(*it);
            hi.erase(it);
        }

        while(lo.size()>hi.size()) {
            auto it=lo.end();
            it--;
            sumlo-=*it;
            sumhi+=*it;
            hi.insert(*it);
            lo.erase(it);
        }

        while(lo.size()>0 && *lo.rbegin()>*hi.begin()) {
            auto itlo=lo.end();
            itlo--;
            hi.insert(*itlo);
            sumhi+=*itlo;
            sumlo-=*itlo;
            lo.erase(itlo);
            auto it=hi.begin();
            lo.insert(*it);
            sumlo+=*it;
            sumhi-=*it;
            hi.erase(it);
        }
    }

    void add(int a) {
        if(lo.size()<hi.size()) {
            lo.insert(a);
            sumlo+=a;
        } else {
            hi.insert(a);
            sumhi+=a;
        }
        norm();
    }

    void remove(int a) {
        if(a<*hi.begin()) {
            auto it=lo.find(a);
            assert(it!=lo.end());
            sumlo-=*it;
            lo.erase(it);
        } else {
            auto it=hi.find(a);
            assert(it!=hi.end());
            sumhi-=*it;
            hi.erase(it);
        }
        norm();
    }

    int median() {
        return *(hi.begin());
    }
};

/* Brute force the median, or binary search it?
 *
 * First, get the definitions right.
 * b[i] is the median of a[0]...a[2*i-1]
 *
 * So, a[0]==b[0]
 * then, a[1 or 2]==b[1]
 * and so on...
 *
 * Try to construct a[], if not possible then NO
 *
 * Obviously we must add foreach b[i] that b[i] itself to a[]
 * And a smaller, or bigger number, depending on
 * the current median...somehow.
 * But we cannot know which smaller or bigger number in advance, because that
 * depends also on the following b[i].
 *
 * Consider a prefix b[i]. We know the number of smaller and bigger numbers
 * than b[i] in b[0]..b[i-1], so we can tell how much smaller and bigger 
 * other numbers we need.
 *
 * What about same elements in b[]?
 * can we allways use same as b[i] for bigger numbers, and b[i]-1 for smaller?
 * Or allways b[i]???
 * **
 * What if we construct an sorted array?
 * Is there some kind of invariant...somewhere?
 * ** 
 * Consider last b[i] first.
 * That splits the other elements into three sets lo,hi,mid
 * where 
 * lo[j]<=b[i] and mid=b[i] and hi[j]>=b[i]
 * Now consider second last b[i]
 * ***
 * Consider adding b[i] in order of i.
 * With second i, we know two of the three
 * a[0] and one of a[1] or a[2]
 * Note that it does not matter if we place b[i] in
 * a[1] or a[2], so lets say WLG a[0]=b[0] and a[1]==b[1], (...a[3]=b[2]...)
 * if(b[0]<b[1]) then a[2]>=b[1], else
 * if(b[0]>b[1]) then a[2]<=b[1], else
 * if(b[0]==b[1]) then a[2] can be anything
 * So, we got a constrait on a[2]
 * Now consider b[2]
 * We need to have 2 numbers smaller or eq b[2] in a[] (and 2 bigger or eq),
 * check if this is possible.
 * We count the ones we have fixed, and adjust the constraints on some others as needed.
 * The trick is to adjust the constraints on that numbers that does the least 
 * demage, ie leaves the most oportunities to the next move.
 * How to do that?
 *
 */
void solve() {
    cini(n);
    cinai(b,n);

    vi a(2*n-1);
    a[0]=b[0];

    Median med;
    med.add(b[0]);
    for(int i=1; i<n; i++) {
        med.add(b[i]);
        med.add(b[i]);
        if(med.median()!=b[i]) {
            cout<<"NO"<<endl;
            return;
        }
    }

    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
