/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We got initial points x, so after a[0], let result after a[i] be c[i]
 * so 
 * c[0]=x+a[0]
 * c[1]=x+a[0]+a[1]
 * We want to find the possible number of choices for x.
 *
 * Consider min possible x, how to find?
 *
 * Consider min/max values in b[]. They relate to min/max values
 * in c[], ie one of the k-n+1 biggest values in c[] marks the position
 * of the biggest value in b[].
 * How?
 * ...-> editorial
 *  Consider b[0], that was score after one of 1..k juries, 
 *  so x is one of x+a[0..k-1]=b[0]
 *  x=b[0]-a[0..k-1]
 *  Then just check each one of those k possible values of x. 
 *  if possible then ans++.
 */
void solve() {
    cini(k);
    cini(n);
    cinai(a,k);
    cinai(b,n);

    //cerr<<"n="<<n<<" k="<<k<<endl;
    set<int> x;

    x.insert(b[0]-a[0]);
    //cerr<<"x=";
    for(int i=1; i<k; i++)  {
        a[i]=a[i-1]+a[i];   /* prefix sums */
        x.insert(b[0]-a[i]);
        //cerr<<b[0]-a[i]<<" ";
    }
    //cerr<<endl;

    int ans=0;
    for(int xx : x) {
        set<int> bb;
        for(int i=0; i<n; i++) 
            bb.insert(b[i]);

        for(int i=0; i<k; i++)
            bb.erase(xx+a[i]);

        if(bb.size()==0)
            ans++;
    }
    cout<<ans<<endl;
}

signed main() {
        solve();
}
