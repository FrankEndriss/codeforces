/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider ma0 be the max value in a, and idxMa0 the idx of it.
 * And ma1 be the second max value in a, and idxMa1 the idx of it.
 *
 * Then a nice permutation is one
 * where abs(ma0-ma1)==0 && idxMa0>idxMa1, 
 * or    abs(ma0-ma1)==1 && idxMa0<idxMa1, 
 *
 * Number of perms of first type is n * (n-2)!
 * ....
 * No, completly wrong.
 * a[i] is given, so we need to find the possible perms among _that given a[]_.
 */
using mint=modint998244353;

void solve() {
    cini(n);
    cinai(a,n);

    sort(all(a), greater<int>());
    map<int,int> f;
    for(int i=0; i<n; i++) 
        f[a[i]]++;

    /* number of ans of first type */
    mint ans=f[a[0]]*(f[a[0]]-1)/2;

    /* add all perms where the leftmost of the second biggest elements 
     * is left of all biggest elements.
     */
    if(f[a[0]]<n) {
        int cnt2=f[a[f[a[0]]]];
        mint ans2=cnt2*
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
