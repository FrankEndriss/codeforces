/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * If there are at least 3 players want to win one game,
 * they can win in circle order.
 * Else not possible, because the winners must not win against
 * the "not loose" players.
 */
void solve() {
    cini(n);
    cins(s);
    cerr<<"n="<<n<<" s="<<s<<endl;

    vi w;
    for(size_t i=0; i<s.size(); i++)  {
        if(s[i]!='1')
            w.push_back(i);
    }

    cerr<<"w.size()="<<w.size()<<"w= ";
    for(size_t i=0; i<w.size(); i++)
        cerr<<w[i]<<" ";
    cerr<<endl;

    

    if(w.size()>0 && w.size()<3) {
        cout<<"NO"<<endl;
        return;
    }
    cout<<"YES"<<endl;

    vs ans(n, string(n, '='));

    for(int i=0; i<n; i++) 
        ans[i][i]='X';

    if(w.size()>0) {
    const int sz=w.size();
    w.push_back(w[0]);
    for(size_t i=0; i<sz; i++) {
        ans[w[i]][w[i+1]]='+';
        ans[w[i+1]][w[i]]='-';
    }
    }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
