/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Simulation, try to get all details right. 
 * Note that only the day of first reach the apple 
 * is ans, independent of time.
 *
 * One whole day:
 * h+=12*(a-b)
 *
 * cornercase: if b>=a maybe no result.
 *
 **/
void solve() {
    cini(h1);
    cini(h2);
    cini(a);
    cini(b);

    h1+=8*a;    // after first day
    if(h1>=h2) {
        cout<<0<<endl;
        return;
    }
    if(a<=b) {
        cout<<-1<<endl;
        return;
    }

    int d=(a-b)*12;
    int ans=(h2-h1+d-1)/(d);
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

