/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* One option is that we multiply smallest numbers
 * with biggest ones.
 * Other option is to sort smallest number into middle
 * because center elements are included in more pairs.
 *
 * see tutorial.
 * We need to use information of how much * a given a[i]
 * contributes.
 * It is included in some pair (i+1)*(n-i-1) times (i zero based).
 *
 * For some reason it seems that the order of a[] does not make
 * any difference (what I do not really get, because it seems
 * obvious that higher contributing positions should have lower
 * numbers...)
 * Answer is simple: Problem statement says to find order of b, a
 * must not be reordered. Sic :/
 *
 */
const int MOD=998244353;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

/* order b[] by least contribution */
    vi id(n);
    iota(id.begin(), id.end(), 0);
    sort(id.begin(), id.end(), [&](int i1, int i2) {
        return (i1+1)*(n-i1)*a[i1]<(i2+1)*(n-i2)*a[i2];
    });
    sort(b.begin(), b.end(), greater<int>());

    int ans=0;
    for(int i=0; i<a.size(); i++) {
        int val=(a[id[i]]*b[i])%MOD;
        val*=(id[i]+1);
        val%=MOD;
        val*=(n-id[i]);
        val%=MOD;
        ans+=val;
        ans%=MOD;
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS, SOLVE ONE AFTER THE OTHER
