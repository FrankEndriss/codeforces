/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Brute force try all numbers for c, starting with 1.
 * ...no.
 * NOTE: it is not any power of c, it is power the index
 *
 * Since the cost grow fairly quick we need to make
 * all a[i]==1, or it is very small n.
 *
 * How to solve for small n?
 */
const int INF=1e18;
const int N=2e5;
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));

    int ans=accumulate(all(a), 0LL)-n;  // sol for c==1
    /* note ans<=1e14 */

    for(int c=2; c<N; c++) {
        int cc=1;
        int lans=a[0]-1;
        bool ok=true;
        for(int i=1; i<n; i++) {
            if(INF/c<cc) {
                ok=false;
                break;
            }
            cc*=c;
            lans+=abs(a[i]-cc);
        }
        if(ok)
            ans=min(ans,lans);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
