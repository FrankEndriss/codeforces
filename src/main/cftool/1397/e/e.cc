/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Annoying rules:
 * Pistol used on boss: change level
 * Laser: If boss not dead change level
 * AWP: Kill normal monsters xor boss
 *
 * Greedy:
 * So on each level we have basically two choices:
 * 1. Kill all and boss, then move to next level
 * 2a. Kill monsters and do bitch move to change to next level
 * 2b. Kill monsters and bitch move on next level
 * 3. Go back finish boss.
 * 4. Go forward finish other boss, then continue on next level.
 */
void solve() {
    cini(n);
    cinai(r,3); /* load times, ie shoot costs */
    cini(d);    /* level move time, same for all level */
    cinai(a,n); /* Monster count per level */

    vi ans(n);  /* ans[i]=min time to kill all bosses up to i */
    vi extra(n);    /* extra[i]= time after having killed all bosses up to i to get to level i+1 */
    for(int i=0; i<n; i++) {
        if(i==0) {
            // ... TODO
        }
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
