/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * One n
 * two times n-1
 *
 * So at position i we remove a[i] n times, and add a[i] n-1 times.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vvi ans(3, vi(n));

    if(n==1) {
        cout<<"1 1"<<endl;
        cout<<-a[0]<<endl;
        cout<<"1 1"<<endl;
        cout<<a[0]<<endl;
        cout<<"1 1"<<endl;
        cout<<-a[0]<<endl;

    } else {

        for(int i=0; i+1<n; i++) {
            ans[0][i]=-1*a[i]*n;
            ans[1][i]=a[i]*(n-1);
        }
        ans[0][n-1]=-1*a[n-1]*n;
        ans[2][n-1]=a[n-1]*(n-1);

        cout<<"1 "<<n<<endl;
        for(int i : ans[0])
            cout<<i<<" ";
        cout<<endl;
        cout<<"1 "<<n-1<<endl;
        for(int i=0; i+1<n; i++)
            cout<<ans[1][i]<<" ";
        cout<<endl;
        cout<<"2 "<<n<<endl;
        for(int i=1; i<n; i++)
            cout<<ans[2][i]<<" ";
        cout<<endl;
    }


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
