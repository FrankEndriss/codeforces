/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* How to win?
 * The other player must take the last stone from the pre-last pile.
 * So taking the last stone from a pile when there are 3 piles is
 * loosing position. So we do this only if it is 1 1 1.
 * x y is win if x!=y, loose if x==y
 * 1 1 1 is winning position.
 * 2 1 1 is loosing
 * -> odd is winning
 * -> even is loosing
 *
 *  What about 1 1 1 1? -> loose
 *  What about 2 1 1 1? -> win
 *  What about 3 1 1 ? -> win
 *
 */
void solve() {
    cini(n);
    cinai(a,n);
    int ma=*max_element(all(a));
    int sum=accumulate(all(a), 0LL);

    if(n==1 || ma*2>sum) {
        cout<<"T"<<endl;
    } else if(n==2) {
        if(a[0]!=a[1])
            cout<<"T"<<endl;
        else
            cout<<"HL"<<endl;
    } else {
        if(sum&1)
            cout<<"T"<<endl;
        else
            cout<<"HL"<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
