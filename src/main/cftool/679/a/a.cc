/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

void init() {

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
}

bool ask(int num) {
    cout<<num<<endl;
    cins(s);
    return s=="yes";
}

/* if composite one of the factors must be less than
 * 10, ie 2, 3, 5, or 7, so first find that one.
 * Then, the number can be that factor, or a multiple
 * of it.
 * If it is a multiple of it there is a second prime
 * less than 50 dividing the number, too.
 * What for quadratic numbers? We need to check them, too.
 *
 */
void solve() {
    int cnt=0;
    int lpr=0;

    for(int p : { 2, 3, 5, 7}) {
        if(ask(p)) {
            lpr=p;
            cnt++;
        }
    }
    if(cnt==0) {
        cout<<"prime"<<endl;
        return;
    } else if(cnt>1) {
        cout<<"composite"<<endl;
        return;
    }

    for(int q : { 4, 9, 25, 49, 81 } ) {
        if(ask(q)) {
            cout<<"composite"<<endl;
            return;
        }
    }
    
    for(int i=4; cnt<2 && pr[i]<50; i++) {
        if(ask(pr[i])) {
            cnt++;
        }
    }
    if(cnt>=2)
        cout<<"composite"<<endl;
    else
        cout<<"prime"<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    init();
        solve();
}

