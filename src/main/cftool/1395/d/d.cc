/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* There are n days.
 * Some days are on, some are off.
 *
 * First day counts, and if >m the next
 * d days will be off.
 *
 * Greedy?
 * Put all a[i]<=m to begin, so they all contribute.
 * then put biggest a[i] on remaining positions every d days.
 *
 * Other Greedy
 * Put biggest a[i] as first, then the next bigger ones
 * on next every d positions.
 * Then consecutivly substitute the last biggest one.
 * ******
 * Still other:
 * We use biggest a[i], plus all possible b[j].
 * then biggest two a[i], plus all possible b[j]
 * etc, max is ans.
 *
 * shit implementation... sic :/
 */
void solve() {
    cini(n);
    cini(d);
    cini(m);

    vi a;   /* bigger */
    vi b;   /* smaller */
    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux>m)
            a.push_back(aux);
        else
            b.push_back(aux);
    }

    sort(all(a), greater<int>());
    sort(all(b), greater<int>());

    vi preB(b.size()+1);
    for(int i=1; i<=b.size(); i++)
        preB[i]=preB[i-1]+b[i-1];

    /* all b[] and one a[] in the end */
    int ans=preB[b.size()];
    if(b.size()<n)
        ans+=a[0];

    int sumA=0;
    for(int i=0; i<a.size(); i++) {
        sumA+=a[i];
        int lsum=sumA;
        ans=max(ans, lsum);

        /* first index avail for some b[] */
        int idx=(i+1)*(d+1);
        /* number of usable b[] */
        int bcnt=min(n-idx, (int)b.size()+1);

        if(bcnt<=0)
            break;

        if(bcnt>0 && i+1<a.size()) {
            lsum+=a[i+1];
            bcnt--;
        }

        lsum+=preB[min(bcnt, (int)b.size())];
        ans=max(ans, lsum);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
