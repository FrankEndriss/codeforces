/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Bit by bit.
 *
 * For each bit and a[i] we need to check if it 
 * is possible to switch the bit off.
 * If it is possible then do it,
 * which means we put all possible b[i] into
 * set for next bit.
 * */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    cinai(b,m);

    vector<vector<int>> bsets(n, b);

    int ans=0;

    for(int bit=1LL<<10; bit>0; bit>>=1) {
        vector<vector<int>> bsets2(n);

        bool bitInAns=false;
        for(int i=0; i<n; i++) {
            if(a[i]&bit) {
                bool canRemove=false;
                for(int bval : bsets[i]) {
                    if((bval&bit)==0) {
                        bsets2[i].push_back(bval);
                        canRemove=true;
                    }
                } 
                if(!canRemove) {
                    ans|=bit;
                    bitInAns=true;
                    break;
                }
            } else {
                bsets2[i]=bsets[i];
            }
        }
        if(!bitInAns)
            bsets.swap(bsets2);
    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
