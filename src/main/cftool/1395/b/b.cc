/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* move in current row 
 *  to left, then all fields execpt the start field to right,
 *  the choose an unused row,
 *  move there
 *  all fields there,
 *  and next row.
 */
void solve() {
    cini(n);
    cini(m);
    cini(sx);
    cini(sy);

    for(int i=sx; i>=1; i--) 
        cout<<i<<" "<<sy<<endl;
    for(int i=sx+1; i<=n; i++)
        cout<<i<<" "<<sy<<endl;

    int dir=-1;
    for(int j=sy-1; j>=1; j--) {
        int st=0;
        int en=n+1;
        if(dir==-1) {
            st=n+1;
            en=0;
        }
        for(int i=st+dir; i!=en; i+=dir) 
            cout<<i<<" "<<j<<endl;

        dir=-dir;
    }

    for(int j=sy+1; j<=m; j++) {
        int st=0;
        int en=n+1;
        if(dir==-1) {
            st=n+1;
            en=0;
        }
        for(int i=st+dir; i!=en; i+=dir) 
            cout<<i<<" "<<j<<endl;

        dir=-dir;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
