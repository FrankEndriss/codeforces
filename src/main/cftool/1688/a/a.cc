
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * it is x&y AND x^y
 * The min bit and the min !bit
 */
void solve() {
    cini(x);
    bool m1=true;
    bool m2=true;
    int ans=0;
    for(int i=0; m1 ; i++)  {
        if(m1 && ((1LL<<i)&x)) {
            m1=false;
            ans|=(1LL<<i);
        }
    }
    if((x&ans)>0  && (x^ans)>0) {
        cout<<ans<<endl;
        return;
    }
    for(int i=0; m2; i++) {
        if(m2 && ((1LL<<i)&x)==0) {
            m2=false;
            ans|=(1LL<<i);
        }
    }
    assert(!(m1 || m2));
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
