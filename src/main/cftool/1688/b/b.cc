
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider token 16 and 2.
 * Obviously its best to add, then split.
 *
 * Why?
 * Note that 
 *
 * We cannot remove two even token in one move.
 * But we can remove one even token with one move
 * if adding to an odd token.
 * So find fastest way to create an odd token,
 * then use it to remove all even token.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    int cnt=0;
    for(int i=0; i<n; i++) {
        if(a[i]&1)
            cnt++;
    }

    if(cnt>0) {
        cout<<n-cnt<<endl;
        return;
    }

    int ans=INF;
    for(int i=0; i<n; i++) {
        int lcnt=0;
        while((a[i]&1)==0) {
            lcnt++;
            a[i]/=2;
        }
        ans=min(ans, n-1+lcnt);
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
