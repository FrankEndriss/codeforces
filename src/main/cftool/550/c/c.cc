/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we need to find up to 3 digit as a subsequece of s.
 * Since s is small we can simply brute force all
 * such 3 digit seqs.
 **/
void solve() {
    cins(s);

    reverse(s.begin(), s.end());

    for(int i=0; i<1000; i+=8) {
        string s3;
        int ii=i;
        do {
            s3+='0'+(ii%10);
            ii/=10;
        }while(ii>0);

        int idx=0;
        for(char c : s) {
            if(idx<s3.size() && c==s3[idx])
                idx++;
        }
        if(idx==s3.size()) {
            reverse(s3.begin(), s3.end());
            cout<<"YES"<<endl;
            cout<<s3<<endl;
            return;
        }
    }
    cout<<"NO"<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

