/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Ask for the first char, then the first two, then first three
 * ...and so on.
 * Whenever there is a new char, ask which one.
 * So we get the 26 positions of the leftmost char of any kind,
 * and the info of same chars next to each other.
 *
 * So, we want to find 26 classes with 6e3 queries.
 *
 * Consider kind to two pointer.
 * Increase size of segment until there are two same letters in segment, at most 27 steps.
 * Then find the couterpart of the last letter, at most 26 steps.
 * Can we binary search that, instead of 26 steps?
 *
 * ...idk :/
 */
void solve() {
    cini(n);

    int p=0;
    for(int i=1; i<=n; i++) {
        int pp=ask(1,i);
        if(pp!=
    }
}

signed main() {
        solve();
}
