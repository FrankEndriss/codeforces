/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider distance matrix of all points.
 *
 * Find all triples of same color. O(n^3)
 * if a point is member of more than one triple, 
 * these triples belong to the same color class.
 * ...no.
 *
 * Consider two points p0 and p1, there are n ways
 * to give them same color, and n*(n-1) ways for two different colors.
 * The n same color ways imply, that
 * -if p2 also has this color, the dists are all same
 * -if p2 has other color, dists confirm to second rule.
 *
 *
 * Note there are fac(n) ways to give all points different colors.
 * Note triples conforming to first rule conform in all 6 permutations.
 * Note triples conforming to second rule do not, there conform exaclty one of the perms.
 *
 * So iterate all pairs a,b and count all 
 * possible colors for c that conform.
 * ****
 */
using mint=modint998244353;
void solve() {
    cini(n);
    vi x(n);
    vi y(n);
    for(int i=0; i<n; i++) 
        cin>>x[i]>>y[i];

    vvi d(n, vi(n));
    for(int i=0; i<n; i++) 
        for(int j=i+1; j<n; j++) 
            d[i][j]=d[j][i]=abs(x[i]-x[j])+abs(y[i]-y[j]);

    /* all different colors */
    /*
    mint ans=1;
    for(int i=2; i<=n; i++) 
        ans*=i; 
        */

    mint ans=0;
    for(int i=0; i<n; i++) 
        for(int j=i+1; j<n; j++) {
            for(int k=j+1; k<n; k++) {
                /* consider i and j same color, n colors possible */
                if(d[i][j]==d[i][k] && d[i][j]==d[j][k]) {
                    ans+=n;
                } else if(d[i][j]<d[i][k] && d[i][j]<d[j][k]) {
                    ans+=n*(n-1);
                } 

                /* consider i and j diff color, n*(n-1) colorpairs possible */
                ans+=n*(n-1)*n;
            }
        }

    cout<<ans.val()<<endl;



}

signed main() {
        solve();
}
