/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can move 'a' to right of 'b'.
 * We can move 'c' to left of 'b'.
 * And since we cannot jump any 'a' over 'c' or vice versa,
 * movements of 'a'
 *
 * We cannot move b or c over each other.
 *
 * So try to move all 'a' as left as necessary,
 * and all 'c' as right as necessary.
 * ...somehow.
 *
 *
 * Go from left to right, if there is an 'a' in t[i] then 
 * try to move the next right 'a' in s[] to that position.
 * ...O(n^2)
 *
 * We can move an 'a' throug a subarray of 'b's, from right to left.
 *
 *
 * omg...there are so much cases... sic :/
 * But nevertheless, we need to simulate this. Dont like the problem.
 *
 *
 * ..............
 * BUT, order of 'a' and 'c' cannot change, so must be same in both strings
 * beforehand. If they are, we just have to check that all 'a' can be put in 
 * place by moveing them left, and all 'c' by moving right.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cins(s);
    cins(t);

    string s1, t1;
    for(int i=0; i<n; i++) {
        if(s[i]!='b')
            s1.push_back(s[i]);
        if(t[i]!='b')
            t1.push_back(t[i]);
    }

    if(s1!=t1) {
        cout<<"NO"<<endl;
        return;
    }

    int cnts=0;
    int cntt=0;
    for(int i=0; i<n; i++) {
        cnts+=(s[i]=='a');
        cntt+=(t[i]=='a');
        if(cntt>cnts) {
            cout<<"NO"<<endl;
            return;
        }
    }

    cnts=0;
    cntt=0;
    for(int i=0; i<n; i++) {
        cnts+=(s[i]=='c');
        cntt+=(t[i]=='c');
        if(cnts>cntt) {
            cout<<"NO"<<endl;
            return;
        }
    }

    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
