
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Find number of subarrays.
 * Subtract bad subarrays.
 * Foreach position l find max position r that makes a good subarray.
 *
 * Check a third point, if it make a bad triple with two prev points by:
 * dx=d(p1.x,p2.x)
 * dy=d(p1.y,p2.y) // note that y is the index
 * Four cases:
 * dx and dy are odd
 * -> the point 'in the middle' makes a bad triple
 * dx==0 and dy is odd
 * -> all points on the horz row 'in the middle' make a bad triple
 * dx is odd and dy==0
 * -> all points on the vert col 'in the middle' make a bad triple,
 *    but since dy!=0, this case never happens 
 * else
 * -> no bad triples
 *
 * So we would have to check foreach l:
 * The a[i] at even dist positions...
 * and somehow pairs, but thats O(n^2)
 * ???
 */
void solve() {
    cini(n);
    cinai(a,n);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
