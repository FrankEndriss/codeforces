/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Its some tricky transformation.
 * We start with finding for all positions i the distance to
 * the next smaller value j to the left and the right.
 * (Stack, O(n));
 * With these numbers we know the maximus interval size where a[i]
 * is smallest element.
 *
 * let size(i) be r[i]-l[i]-1, ie the max size of the interval where
 * a[i] is smallest element.
 * Then we iterate i over 0..n, foreach
 * ans[size(i)]=max(ans[size(i)], a[i])
 * Note that there is at least one i where size(i)==1
 *
 * Finally there are some ans[j] where there is no j==size(i) for some i.
 * These ans[j]=ans[j+1]
 *
 * See tutorial.
 */
void solve() {
    cini(n);
    cinai(a,n);
    vi l(n,-1);
    vi r(n, n);

    stack<int> st;
    for(int i=0; i<n; i++) {
        while(st.size() && a[st.top()]>=a[i])
            st.pop();

        if(st.size())
            l[i]=st.top();

        st.push(i);
    }

    while(st.size())
        st.pop();

    for(int i=n-1; i>=0; i--) {
        while(st.size() && a[st.top()]>=a[i])
            st.pop();

        if(st.size())
            r[i]=st.top();

        st.push(i);
    }

    vi ans(n+1);
    for(int i=0; i<n; i++) {
        int sz=r[i]-l[i]-1;
        ans[sz]=max(ans[sz], a[i]);
    }

    for(int i=n-1; i>=1; i--) {
        ans[i]=max(ans[i], ans[i+1]);
    }

    for(int i=1; i<=n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
