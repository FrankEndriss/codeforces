/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vvvvi= vector<vvvi>;
using vs= vector<string>;

const int MOD=1e8;

/* try dp
 * dp[i][j][k]=num posibilities if i n1 left and j n2 left and last block balance==k
 *
 * The walk from left to right along the line.
 * k: n1 count -1, n2 +1, k=sum+10
 */
void solve() {
    cini(n1);
    cini(n2);
    cini(k1);
    cini(k2);

    vvvvi dp(n1+1, vvvi(n2+1, vvi(2, vi(max(k1,k2)+1))));

    dp[n1][n2][0][0]=1;
    dp[n1][n2][1][0]=1;

    for(int i=0; i<n1+n2; i++) {
        for(int i1=n1; i1>=0; i1--) {
            for(int i2=n2; i2>=0; i2--) {
                if(i1>0) {
                    /* place a n1 after some n1 */
                    for(int kk=2; kk<=k1; kk++)
                        dp[i1-1][i2][0][kk]=dp[i1][i2][0][kk-1];

                    /* place a n1 after some n2 */
                    int sumk2=accumulate(dp[i1][i2][1].begin(), dp[i1][i2][1].end(), 0LL);
                    dp[i1-1][i2][0][1]=sumk2%MOD;
                    dp[i1-1][i2][0][0]=0;
                }
                if(i2>0) {
                    /* place a n2 after some n2 */
                    for(int kk=2; kk<=k2; kk++)
                        dp[i1][i2-1][1][kk]=dp[i1][i2][1][kk-1];
                    /* place a n2 after some n1 */
                    int sumk1=accumulate(dp[i1][i2][0].begin(), dp[i1][i2][0].end(), 0LL);
                    dp[i1][i2-1][1][1]=sumk1%MOD;
                    dp[i1][i2-1][1][0]=0;
                }
            }
        }
    }

    int ans=accumulate(all(dp[0][0][0]), 0LL)+accumulate(all(dp[0][0][1]), 0LL);
    cout<<ans%MOD<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
