/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

#define N 1
#define E 2
#define S 3
#define W 4

/* count the turns to left.
 * Every turn is a turn left or a turn right. */
void solve() {
    cini(n);
    int ans=0;
    int x0, y0, x1, y1;
    for(int i=0; i<n; i++) {
        cini(x);
        cini(y);

        if(i>=2) {
            int dir;    // N=1 E=2 S=3 W=4
            if(y1>y0)
                dir=N;
            else if(y1<y0)
                dir=S;
            else if(x1<x0)
                dir=W;
            else
                dir=E;
            
            if(dir==N) {
                if(x<x1)
                    ans++;
            } else if(dir==S) {
                if(x>x1)
                    ans++;
            } else if(dir==E) {
                if(y>y1)
                    ans++;
            } else
                if(y<y1)
                    ans++;
        }
        x0=x1;
        y0=y1;
        x1=x;
        y1=y;
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

