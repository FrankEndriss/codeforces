/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to know foreach removable prefix
 * how much suffixes we can remove to get a correct BS.
 * A correct BS is formed whenever sum of brackets eq 0,
 * and is bgeq 0 in all previous positions.
 *
 * Let dp[i]=number of open brackets before postion i;
 */
const int INF=1e18;
void solve() {
    cini(n);
    vi c(n);
    for(int i=0; i<n; i++) {
        cin>>c[i];
        if(i&1)
            c[i]=-c[i];
    }

    vi dp(n+1); /* prefix sum */
    int mi=0;
    int sum=0;
    for(int i=0; i<n; i++) {
        dp[i+1]=dp[i]+c[i];
        mi=min(mi, d[i+1]);
        sum+=c[i];
    }


    int ans=0;
    for(int i=0; i<n; i++) {
        /* Consider removing the prefixes of len a[i]...a[i+1] ...somehow :/
         */
    }
}

signed main() {
    solve();
}
