/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * There must be n/2 item of one kind, and n-n/2 of the other.
 * if n is odd, then a[] must start with the bigger freq of both.
 *
 * Then find the number of moves needed in one direction, ie left or right.
 *
 * ...
 * does not work, idk :/
 * How to find the min number of moves?
 * Just the diff: abs(is[i]-should[i])
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);

    vi f(2);
    for(int i=0; i<n; i++) {
        a[i]%=2;
        f[a[i]]++;
    }

    if((n&1) && abs(f[0]-f[1])!=1) {
        cout<<-1<<endl;
        return;
    }
    if(!(n&1) && (f[0]!=f[1])) {
        cout<<-1<<endl;
        return;
    }

    int ans=INF;
    for(int start=0; start<=1; start++) {
        if(f[start]<f[!start])
            continue;

        vi is;
        for(int i=0; i<n; i++)
            if(a[i]==start)
                is.push_back(i);

        vi should;
        for(int i=0; i<n; i+=2)
            should.push_back(i);

        assert(is.size()==should.size());

        int lans=0;
        for(size_t i=0; i<is.size(); i++) 
            lans+=abs(is[i]-should[i]);

        ans=min(ans, lans);
    }
    if(ans==INF)
        ans=-1;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
