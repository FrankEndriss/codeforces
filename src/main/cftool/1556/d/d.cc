
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int qor(int i, int j) {
    cout<<"or "<<i+1<<" "<<j+1<<endl;
    int ans;
    cin>>ans;
    return ans;
}
int qand(int i, int j) {
    cout<<"and "<<i+1<<" "<<j+1<<endl;
    int ans;
    cin>>ans;
    return ans;
}

/**
 * First of all, bits do not interact, we can solve foreach bit separate.
 *
 * If we know one element for sure, we can find the next with
 * two queries, AND and OR.
 * Also there are two determined query results:
 * if AND==1 we know both elements eq 1
 * if OR ==0 we know both elements eq 0
 * AND=1 and OR=0 is not possible
 * if AND=0 and OR=1 then we know that one element is 1, one is 0, 
 *  but we do not know witch.
 * So consider first three elements, for all three pairs of them
 * do both queries.
 * If 1,2==01 and 1,3==01, then 2,3 is for sure 00 or 11.
 */
void solve() {
    cini(n);
    cini(k);

    vi qa(n-1);
    vi qo(n-1);
    for(int i=0; i+1<n; i++) {
        qa[i]=qand(i,i+1);
        qo[i]=qor(i,i+1);
    }

    int q13a=qand(0,2);
    int q13o=qor(0,2);

    vi a(n);

    for(int i=0; i<30; i++) {
        const int b=(1LL<<i);
        if(qa[0]&b) {
            a[0]|=b;
            a[1]|=b;
            if(q13a&b)
                a[2]|=b;
        } else if((qo[0]&b)==0) {
            // bit not set in a[0],a[1]
            if(q13o&b)
                a[2]|=b;
        } else if(qa[1]&b) {
            a[1]|=b;
            a[2]|=b;
            if(q13a&b)
                a[0]|=b;
        } else  if((qo[1]&b)==0) {
            // bit not set in a[1],a[2]
            if(q13o&b)
                a[0]|=b;
        } else if(q13a&b) {
            a[0]|=b;
            a[2]|=b;
            if(qa[0]&b)
                a[1]|=b;
        } else if((q13o&b)==0) {
            // bit not set in a[0],a[2]
            if(qo[0]&b)
                a[1]|=b;
        } else 
            assert(false);

        for(int j=2; j+1<n; j++) {
            if(qa[j]&b)
                a[j+1]|=b;
            else if((a[j]&b)==0 && (qo[j]&b))
                a[j+1]|=b;
        }
    }
    sort(all(a));
    cout<<"finish "<<a[k-1]<<endl;

}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
