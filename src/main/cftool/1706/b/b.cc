

/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * To place two blocks of same color next to each other,
 * the distance must be odd.
 * So clasifiy the numbers in c[] by value and triple-parity of position.
 *
 * Distance between same color positions must be
 * 1,3,5,....
 * 2*y+1
 *
 * ***
 * Actually there is a much more simple greedy solution, based on
 * the observation that each two folowing blocks of a seq are
 * simple of "different parity".
 * Which means we can collect color blocks by parity, then start
 * at the leftmost one of each, and binary search (or pointer increment) throu 
 * all those lists.
 */
void solve() {
    cini(n);
    cinai(c,n);

    for(int i=0; i<n; i++) 
        c[i]--;

    vvi dp(2, vi(n+2));
    vi ans(n);
    for(int i=n-1; i>=0; i--) {
        dp[i%2][c[i]]=dp[1-i%2][c[i]]+1;
        ans[c[i]]=max(ans[c[i]], dp[i%2][c[i]]);
    }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<" ";

    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
