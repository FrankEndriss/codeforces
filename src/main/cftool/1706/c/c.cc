

/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * If number of buildings is odd the contributing buildings
 * are fixed, just check eachone.
 *
 * If number of buildings is even, we can use a prefix and
 * a postfix, just check all combis.
 */
void solve() {
    cini(n);
    cinai(h,n);

    vi dp(n);       /* fixed hight adjustment */
    for(int i=1; i+1<n; i++) 
        dp[i]=max(0LL, max(h[i-1]-h[i]+1, h[i+1]-h[i]+1));

    if(n%2) {
        int ans=0;
        for(int i=1; i<n; i+=2)
            ans+=dp[i];
        cout<<ans<<endl;
    } else {
        vi le(n);
        le[1]=dp[1];
        for(int i=3; i<n; i+=2) 
            le[i]=le[i-2]+dp[i];
        vi ri(n);
        if(n>=2)
            ri[n-2]=dp[n-2];
        for(int i=n-4; i>=1; i-=2) 
            ri[i]=ri[i+2]+dp[i];

        int ans=1e18;
        if(n>2)
            ans=ri[2];
        for(int i=1; i<n; i+=2) {
            int r=0;
            if(i+3<n)
                r=ri[i+3];
            ans=min(ans, le[i]+r);
        }
        cout<<ans<<endl;
    }



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
