

/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider using k for biggest a[i], 
 * and same or smaller k-1 for smaller a[i],
 * so that a[i]/p[i]<a[max]/k
 * Then sort by value, and p[i]++ from left to right
 * until a[max]/k is smallest value.
 * Best of these solution is best of all.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    sort(all(a), greater<int>());

    const int ma=*max_element(all(a));
    if(k>ma) {
        cout<<0<<endl;
        return;
    }

    using t3=tuple<int,int,int>;
    set<t3> t;

    int kk=k;
    const int v=a[0]/k;
    t.emplace(a[0]/k,k,0);
    for(int i=1; i<n; i++) {
        while(kk>1 && a[i]/(kk-1)<=v)
            kk--;
        t.emplace(a[i]/kk, kk,i);
    }

    int ans=1e18;
    for(int i=0; i<n; i++) {
        auto [akk0,kk0,ii0]=*t.begin();
        auto [akk1,kk1,ii1]=*t.rbegin();
        ans=min(ans, akk1-akk0);
        t.erase(t.begin());
        t.emplace(a[ii0]/max(1LL,kk0-1), max(1LL,kk0-1),ii0);
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
