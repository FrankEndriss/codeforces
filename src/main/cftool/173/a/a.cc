/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

#define int ll

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cins(s);
    cins(t);
    vi ans(2);
    vi ansX(2);
    const int ss=s.size();
    const int st=t.size();
    int idx=0;
    for(size_t i=0; i<s.size(); i++)
        for(size_t j=0; j<t.size(); j++) {
            vi a(2);
            if(s[idx%ss]=='R' && t[idx%st]=='P')
                a[0]++;
            else if(s[idx%ss]=='P' && t[idx%st]=='S')
                a[0]++;
            else if(s[idx%ss]=='S' && t[idx%st]=='R')
                a[0]++;
            else if(s[idx%ss]=='P' && t[idx%st]=='R')
                a[1]++;
            else if(s[idx%ss]=='R' && t[idx%st]=='S')
                a[1]++;
            else if(s[idx%ss]=='S' && t[idx%st]=='P')
                a[1]++;

            ans[0]+=a[0];
            ans[1]+=a[1];
            if(idx<n%(ss*st)) {
                ansX[0]+=a[0];
                ansX[1]+=a[1];
            }

            idx++;
        }

    int cyc=n/(ss*st);
    cout<<ans[0]*cyc+ansX[0]<<" "<<ans[1]*cyc+ansX[1]<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

