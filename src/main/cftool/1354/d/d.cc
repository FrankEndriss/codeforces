/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;



template<typename E>
struct SegmentTree {
    vector<E> data;

    E neutral;
    int n;

    function<E(E,E)> plus;

    SegmentTree(int _n, E _neutral) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2, _neutral);
        neutral=_neutral;
    }

    /* bulk update in O(n) */
    template<typename Iterator>
    void init(Iterator beg, Iterator end) {
        for(int idx=n; beg!=end; beg++, idx++)
            data[idx]=*beg;

        for(int idx=n-1; idx>=1; idx--)
            data[idx]=plus(data[idx*2], data[idx*2+1]);
    }

    void rem(int node, int sL, int sR, int k) {
        //cerr<<"rem, node="<<node<<" sL="<<sL<<" sR="<<sR<<" k="<<k<<endl;
        if(sL==sR) {
            //assert(k==1);
            inc(sL, -1);
            return;
        }

        int left=data[node*2];
        int mid = (sL+sR)/2;
        if(left>=k) 
            rem(node*2, sL, mid, k);
        else
            rem(node*2+1, mid+1, sR, k-left);
    }

    /* find bucket with kth element and remove it, decrement bucket. */
    void rem(int k) {
        rem(1, 0, n-1, k);
    }

    E get(int idx) {
        return data[n+idx];
    }

    /* set val at position */
    void inc(int idx, E val) {
        idx+=n;
        data[idx]+=val;
        while(idx>1) {
            idx/=2;
            data[idx]=plus(data[idx*2], data[idx*2+1]);
        }
    }
};


const int N=1e6+3;
void solve() {
    cini(n);
    cini(q);

    vi a(N);
    for(int i=0; i<n; i++)  {
        cini(aux);
        a[aux]++;
    }

    SegmentTree<int> seg(N, 0);
    seg.plus=[](int i1, int i2) {
        return i1+i2;
    };
    seg.init(a.begin(), a.end());

    for(int i=0; i<q; i++) {
        cini(k);
        if(k>=1)
            seg.inc(k, 1);
        else {
            seg.rem(abs(k));
        }
    }

    int ans=0;
    for(int i=1; i<=n; i++)
        if(seg.get(i)>0) {
            ans=i;
            break;
        }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
