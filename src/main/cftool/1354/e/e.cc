/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* Must not put 1 and 3 adj at all.
 * Must not put x and x adj
 * 1-2-1-2-3-2...
 * Must use all according to nX.
 *
 * max(n1,n2)-min(n1-n2) must be on leafs.
 * max(n3,n2)-min(n3-n2) must be on leafs.
 *
 * if(n1>n2) fill leafs with 1, adj with 2
 * else fill leafs with 2
 * if(n3>n2) fill leafs with 3, adj with 2
 * else fill leafs with 2
 *
 * idk...
 *
 */
void solve() {
    cini(n);
    cini(m);
    cini(n1);
    cini(n2);
    cini(n3);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

/*
    vi leafs;
    for(int i=0; i<n; i++)
        if(adj[i].size()<=1) {
            leafs.push_back(i);
        }

    if(leafs.size()<max(n1,n2)-min(n1,n2)+max(n2,n3)-min(n2,n3)) {
        cout<<"NO"<<endl;
        return;
    }
*/

    vi ans(n,-1);
    function<bool(int, int)> draw=[&](int node, int cnt) {
        if(cnt==n)
            return true;

        vb adjc(3);
        for(int chl : adj[node]) {
            if(ans[chl]>=0) {
                adjc[ans[chl]]=true;
            }
        }

        for(int i=0; i<3; i++) {
            if(adjc[i])
                continue;

            ans[node]=i;
            for(int chl : adj[node]) {
                if(ans[chl]<0) {
                    if(draw(chl, cnt+1))
                        return true;
                }
            }
            ans[node]=-1;
        }
    };

    if(draw(0, 0)) {
        cout<<"YES"<<endl;
        for(int i : ans)
            cout<<i;
        cout<<endl;
    } else
        cout<<"NO"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
