/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* dp
 * String with all three in it starts at position
 * where next is other than previos. Then followed
 * by x*second, followed by third.
 *
 * compress the string? then
 *
 */
void solve() {
    cins(s);

    const int INF=1e9;
    vi p={ '1', '2', '3' };
    int ans=INF;
    do {
        int idx=-1;
        for(int i=0; i+1<s.size(); i++) {
            if(s[i]==p[0] && s[i+1]==p[1])
                idx=i;
            else if(idx>=0 && s[i]==p[1] && s[i+1]==p[2])
                ans=min(ans, i-idx+2);
        }
    }while(next_permutation(p.begin(), p.end()));

    if(ans==INF)
        ans=0;
    cout<<ans<<endl;


    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
