/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * longest increasing postfix?
 *
 * But we need to consider that after 
 */
void solve() {
    cini(n);
    cinai(a,n);

    map<int,vi> f;
    for(int i=0; i<n; i++) 
        f[a[i]].push_back(i);

    map<int,bool> vis;

    int ans=0;
    int idx=-1;
    stack<int> st;
    for(int i=0; i+1<n; i++) {
        if((idx>=i || a[i]>a[i+1]) && !vis[a[i]]) {
            int j=i;
            do {
                if(!vis[a[j]]) {
                    ans++;
                    vis[a[j]]=true;
                    idx=max(idx, f[a[j]].back());
                }
                if(st.size()) {
                    j=st.top();
                    st.pop();
                } else
                    j=-1;
            }while(j>=0);
        } else {
            st.push(i);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
