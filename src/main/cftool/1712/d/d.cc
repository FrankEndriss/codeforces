/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Obviously we allways assign 1e9 as weight.
 * The question is, on which of the k elements?
 *
 * Unterstand the graph:
 * Consider l=1, r=n
 * The path has weight min of direct path, or min(1->j + j->n)
 *
 * Now we want to optimise one pair u,v, so that the
 * dist between them gets maximum.
 * Note that it cannot become more than 1e9, since that the
 * max possible weight of the direct edge.
 *
 * Consider two vertex u,v, which edges would we update?
 * We would iterate all n-2 other vertex,
 * foreach build the sum, then update for the k smallest
 * so that they haves sum >1e9.
 * Resulting is ans=min(u->x + x->v) of the remaining vertex.
 *
 * So, how to find the pair with the max result of that?
 * Consider a pair of vertex a[i], a[i+1] with max(min(a[i],a[i+1]))
 * ...
 * Consider subarrays of len (n-k+2) with max(min(a[i..i+n-k+1]))
 * From that subarray taken two elements, then update all edges
 * not in the subarray.
 * But...which two do we choose, and why?
 * We use the two with the biggest min-value.
 * What about the direct edge u->v? Do we have two cases,
 * update it a and not update it?
 * Also, the shortest path in a[i...j] is min(a[i..j]*2)
 *
 * So there are two cases:
 * let m=n-k
 *
 * case1, updating k out of segment vertex:
 * We choose contigous subarray a[i..i+m-1] with max(min(a[i])),
 * update all vertex out of this subarray, then diameter is
 * min(min(a[i])*2, max(min(a[i],a[i+1])));
 *
 * case2, updating k-1 out of segment vertex, and one pair in segment:
 * We choose contigous subarray a[i..i+m] with max(min(a[i])),
 * update all vertex out of this subarray, then diameter is
 * min(min(a[i])*2, 1e9));
 *
 * What if a[i] allready > 1e9/2???
 * -> Most likely case2 will match.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    if(n==k) {
        cout<<(int)1e9<<endl;
        return;
    }

    if(k==1) {
        sort(all(a));
        int ans=0;
        if(n==3)
            ans=a[0]+a[1];
        else
            ans=2*a[0];

        cout<<ans<<endl;
        return;
    }

    cerr<<"n="<<n<<endl;
    int ans=0;
    int m=n-k;
    /* first find best subarray of size m */
    int l=0;
    int r=m-1;
    int ma=-1;
    multiset<int> s;
    for(int i=0; i<m; i++)
        s.insert(a[i]);
    int lans=0;

        ma=*s.begin();
        for(int i=m; i<n; i++) {
            auto it=s.find(a[i-m]);
            assert(it!=s.end());
            s.erase(it);
            s.insert(a[i]);
            int nmin=*s.begin();
            if(nmin>ma) {
                l=i+1-m;
                r=i;
                ma=nmin;
            }
        }
        cerr<<"l1="<<l<<" r1="<<r<<endl;
        lans=*min_element(a.begin()+l, a.begin()+r+1)*2;
        int map=0;
        for(int i=l; i+1<r; i++)
            map=max(map, min(a[i],a[i+1]));
        ans=min(lans, map);


    /* now thw same for m+1 subarray */
    m++;
    s.clear();
    l=0;
    r=m-1;
    ma=-1;
    for(int i=0; i<m; i++)
        s.insert(a[i]);
    ma=*s.begin();
    for(int i=m; i<n; i++) {
        auto it=s.find(a[i-m]);
        assert(it!=s.end());
        s.erase(it);
        s.insert(a[i]);
        int nmin=*s.begin();
        if(nmin>ma) {
            l=i+1-m;
            r=i;
            ma=nmin;
        }
    }
    cerr<<"l2="<<l<<" r2="<<r<<endl;
    lans=*min_element(a.begin()+l, a.begin()+r+1)*2;
    ans=max(ans, min(lans, (int)1e9));

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
