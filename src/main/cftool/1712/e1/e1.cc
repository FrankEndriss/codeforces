/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Lets try to count the opposite triples.
 *
 * lcm(i,j,k)<i+j+k
 * -for some small triples 
 * -if lcm(i,j) is divisor of k
 *
 * Seems somewhat more comlecated :/
 */
void solve() {
    cini(l);
    cini(r);

    vvi d(r+1);
    for(int i=l; i*2<=r; i++) 
        for(int j=i*2; j<=r; j+=i)
            d[j].push_back(i);

    int ans=0;

    for(int j=l+1; j+1<=r; j++) {
        int cnt=d[j].size();
        ans+=((r-j)/j)*cnt;
    }

    int c=r-l+1;
    cout<<c*(c-1)*(c-2)/6-ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
