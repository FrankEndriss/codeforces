/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* So, the perimeter is nothing more than the count of boards.
 * Forech Q we need to find the number of possible fences
 * with Q-szRed*2-1==cnt white boards.
 * We consider only that white boards smaller than the red one (cntW).
 * We can use each size at most twice, so initially remove
 * boards existing more times.
 * Then we need to find the number of distinct white board 
 * smaller the szRed, since this limits the number of
 * boards on one side of the red. (cntWd)
 *
 * But then it gets complecated:
 * For a given Q we can place on the left sum(i=0..cnt, nCr(i,cntWd)*nCr(cnt-i,cntXX))
 * where cntXX is the number of distinct white boards availble on the right side.
 * Wich is not good, since that number depends on the boards choosen on the left side.
 * What else can we do?
 *
 * That point with the distinct sizes is complecated.
 * -> Turns out we need FFT :/
 *
 *
 *
 */
void solve() {
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
