/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9;
#define l first
#define r second
void solve() {
    cini(n);    // odd
    cinll(s);
    vector<pii> lr(n);
    int maxR=0;
    for(int i=0; i<n; i++) {
        cin>>lr[i].l>>lr[i].r;
        maxR=max(maxR, lr[i].r);
    }
    //sort(lr.begin(), lr.end());

    int l=0;
    int r=maxR+1;
    while(l+1<r) {
        int mid=(l+r)/2;
        ll sum=0;
        int cntR=0; // cnt of empl with sal >= mid
        vi opt;
        for(int i=0; i<n; i++) {
            sum+=lr[i].l;
            if(lr[i].l>=mid) {
                cntR++;
            } else if(lr[i].r>=mid) {
                opt.emplace_back(i);
            }
        }
        // add empls from opt until more than half have sal>=mid
        sort(opt.begin(), opt.end(), [&](int i1, int i2) {
            return lr[i2].l<lr[i1].l;
        });
        for(int i : opt) {
            if(cntR>n/2)
                break;
            sum+=mid-lr[i].l;
            cntR++;
        }
        if(sum<=s && cntR>n/2)
            l=mid;
        else
            r=mid;
    }

    int ans=l;
    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

