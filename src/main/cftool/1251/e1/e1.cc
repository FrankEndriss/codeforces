/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to calc the min costs to make the 
 * cascade not stop.
 *
 * Consider the voters with max m[i], let that be m[mx]
 * We need to buy m[mx]-number of voters with less m[i].
 * Since the ones with less m[i] we get for free once
 * we have m[mx] ones.
 *
 * So consider from all m[mx] voters the m[mx] cheapest ones.
 * Then simply repeat, and conider the bought ones from previous 
 * step.
 */
void solve() {
    cini(n);

    vvi m(n);   /* group by m[i] */

    for(int i=0; i<n; i++) {
        cini(mi);
        cini(pi);
        m[mi].push_back(pi);
    }

    int ans=0;
    int cnt=0;
    int lowercnt=n;
    priority_queue<int> q;
    for(int i=n-1; i>=1; i--) {
        if(m[i].size()==0)
            continue;

        lowercnt-=m[i].size();
        for(int j : m[i])
            q.push(-j);      /* push all prices onto the queue, smallest on top */

        while(q.size() && lowercnt+cnt<i)  {
            int pri=q.top();
            q.pop();
            ans-=pri;   /* negative */
            cnt++;
        }
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
