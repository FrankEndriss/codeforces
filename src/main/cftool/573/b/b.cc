/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* dp
 * We need to calc from left, top and right.
 */
void solve() {
    cini(n);
    cinai(h,n);

    const int INF=1e9;
    vi dp(n, INF);
    for(int i=0; i<n; i++) {
        dp[i]=min(i+1, h[i]);
        if(i>0) 
            dp[i]=min(dp[i], dp[i-1]+1);
    }

    for(int i=0; i<n; i++) {
        dp[n-1-i]=min(i+1, dp[n-1-i]);
        if(i>0)
            dp[n-1-i]=min(dp[n-1-i], dp[n-i]+1);
    }
    cout<<*max_element(dp.begin(), dp.end())<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
