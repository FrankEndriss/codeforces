/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int ihypot(const int i, const int j) {
    int l=1;
    int r=i+j+1;
    while(l+1<r) {
        int m=(l+r)/2;
        if(m*m<=i*i+j*j)
            l=m;
        else
            r=m;
    }
    if(l*l==i*i+j*j)
        return l;
    else
        return -1;
}

/* A side lenght exists if it is sum
 * of two quads or a multiple of such
 * a sum.
 *
 * The two sides of the triangle must
 * be both a multiple of one and the
 * same such pair of numbers.
 *
 * Since constraints are small brute
 * force such pairs.
 *
 * Note that a and b are not "symtric", ie
 * the brute force creates different output for
 * ie "15 20" and "20 15".
 * So we sort a and b, then it is allways correct.
 */
void solve() {
    cini(a);
    cini(b);
    if(a>b)
        swap(a,b);

    vi ansX(2,-1);
    vi ansY(2,-1);

    for(int i=1; ansX[0]<0 && i<max(a,b); i++) {
        for(int j=i+1; j<max(a,b); j++) {
            int q=ihypot(i, j);
            if(q>0 && a%q==0 && b%q==0) {
                ansX[0]=i*(b/q);
                ansX[1]=i*(a/q);
                ansY[0]=j*(b/q);
                ansY[1]=j*(a/q);
                break;
            }
        }
    }

    if(ansX[0]>0 && ansX[1]>0) {
        cout<<"YES"<<endl;
        cout<<"0 0"<<endl;
        cout<<ansX[0]<<" "<<ansY[0]<<endl;
        cout<<-ansY[1]<<" "<<ansX[1]<<endl;
    } else {
        cout<<"NO"<<endl;
    }

}

signed main() {
    solve();
}

