/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We want to make the lexi smallest possible p[]
 *
 * The swappable pairs build components. Within one
 * component every two elements can be swapped.
 * BFS for components.
 * And sort components.
 */
void solve() {
    cini(n);
    cinai(p,n);
    vvb a(n, vb(n));
    for(int i=0; i<n; i++) {
        cins(s);
        for(int j=0; j<n; j++)
            a[i][j]=s[j]=='1';
    }

    vb vis(n);
    for(int i=n-1; i>=0; i--) {
        vi c;
        if(!vis[i]) {
            queue<int> q;
            q.push(i);
            vis[i]=true;
            c.push_back(i);
            while(q.size()) {
                int k=q.front();
                q.pop();
                for(int j=0; j<n; j++) {
                    if(!vis[j] && a[k][j]) {
                        q.push(j);
                        vis[j]=true;
                        c.push_back(j);
                    }
                }
            }

            vi cp(c.size());
            for(int j=0; j<c.size(); j++)
                cp[j]=p[c[j]];
            sort(cp.begin(), cp.end());
            sort(c.begin(), c.end());
            for(int j=0; j<c.size(); j++) 
                p[c[j]]=cp[j];
        }
    }

    for(int i : p)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
