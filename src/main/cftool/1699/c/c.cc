<<<<<<< HEAD


=======
>>>>>>> b60669e6a93697fd4c6443e0f1a82de2bb5214bd
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint1000000007;
const int N=1e5+7;
vector<mint> fac(N);

/**
 * Is there any a[]!=b[] where...
 * Yes, see example.
 * So, what are the properties here?
 * In the example, values 2 and 3 are aequivalent.
 * We need to find subarrays of aequivalent values,
 * each one contributes as a factor fac[size]
 *
 * What is the defintion of such subarray?
 * -> It must not contain 0
 *
 * Consider all subarrays not containing 0
 * -> they can be permuted in all ways
 * Consider all subarrays containing 0, but not 1
 * ...
 * So the array is split by the 0 and the 1, and 
 * we can permute the 3 other parts.
 * ...
 * But there are extra rules, consider example
 * 0 1 2 3
 * We cannot permute the 2 while it is next to the 1.
 *
 * So, we can never permute the 0, and never the 1,
 * and we cannot permute the 2 for subarrays where
 * the 0 and 1 are in it...
 * So we can permute the 2 only if it is between 0 and 1.
 * What else?
 * We cannot swap any numbers in an interval
 * containing 0.
 * We cannnot move the 0.
 * We also cannot move the 1.
 *
 * 0 and 1 split array into 3 segments, WLOG consider pos[1]<pos[0]
 * segment ...1: mex=0, so we can interchange all numbers, fac[s]
 * segment 1..0: mex=fixed, so we can interchange all bigger numbers, fac[thatFreq]
 * segment 0...: mex=fixed, so we can interchange all bigger numbers, fac[thatFreq2]
 */
void solve() {
    cini(n);
    cinai(a,n);

    if(n<3) {
        cout<<1<<endl;
        return;
    }

    vi p;
    for(int i=0; i<n; i++) 
        if(a[i]==0 || a[i]==1)
            p.push_back(i);
    assert(p.size()==2);
    sort(all(p));

    mint ans=1;
    if(p[0]>0) 
        ans*=fac[p[0]];
    if(p[1]-p[0]>1)
        ans*=fac[p[1]-p[0]-1];
    if(p[1]<n-1)
        ans*=fac[n-p[1]];

    cout<<ans.val()<<endl;
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
