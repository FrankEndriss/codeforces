<<<<<<< HEAD


=======
>>>>>>> b60669e6a93697fd4c6443e0f1a82de2bb5214bd
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/**
 * two different:
 * corners: both
 * ...how?
 *
 * with=3
 * xox
 * o-o...impossible
 *
 * hight=2
 * xoxox
 * oxoxo
 *
 * with=4
 * xoxo xoox
 * ooxx oxxo
 * xoxo oxxo
 * xoxo xoox
 * ---- xoox
 *  ...
 *
 * with=5
 * xooox
 * oxxxo
 * ooooo
 * xxxxx
 * ooooo
 * xxxxx
 * ooooo
 * oxxxo
 * xooox
 *
 * with=6
 * xoooox
 * oxxxxo
 * xoooox
 * ...no
 *
 * idk :/
 *
 * For all even sizes, we can "grow" from inner 2x2 to outer, since
 * in each step all cells are determined.
 * Then, having an iXi we can add cols, where again all cells are determined.
 * Guess that odd sizes cannot be created.
 *
 * how 3x3???
 * ...-> "You are given two _even_ integers..." :facepalm:
 *
 * How works the construction rules?
 * ...frankly, I dont like that problem. Thinking about it gives
 *    me bad mood. Feels like the wrong movie :/
 */
void solve() {
    cini(n);
    cini(m);

    const vvvi two={{ {1,0}, {0,1}}, { {0,1},{1,0}}};

    vvi a(n, vi(m,-1));
    int cc=0;
    for(int i=0; i<n; i+=2) {
        int c=cc;
        for(int j=0; j<m; j+=2) {
           a[i][j]=two[c][0][0];  
           a[i+1][j]=two[c][1][0];  
           a[i][j+1]=two[c][0][1];  
           a[i+1][j+1]=two[c][1][1];  
           c=1-c;
        }
        cc=1-cc;
    }

    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++)
            cout<<a[i][j]<<" ";
        cout<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
