<<<<<<< HEAD


=======
>>>>>>> b60669e6a93697fd4c6443e0f1a82de2bb5214bd
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * 5
 * 001^002=003
 *
 * More than one bit set:
 * 011
 * Use two same and one submasks
 * 011 011 001
 * 011^011=0
 * 011^001=010
 * ...how?
 *
 * Consisder n=1
 * ans=-1
 * Consider first bit:
 * In the sum, first bit is never set,
 * since if in no number, never set
 * if in 1 number then twice set
 * if in 2 number also twice set
 *
 * 101
 */
void solve() {
    cini(n);
    if(n%2==1)
        cout<<-1<<endl;
    else {
        int a=n/2;
        int b=n/2;
        int c=0;
        assert(n==(a^b)+(a^c)+(b^c));
        cout<<n/2<<" "<<n/2<<" "<<0<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
