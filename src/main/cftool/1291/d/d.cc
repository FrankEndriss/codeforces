/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cins(s);
    cini(q);

    /* this makes the length check O(log n)
     * using a vvi(n, vi(26)) can make it O(26)
     * That map should contain the counts of the
     * chars in all prefixes.
     */
    vvi f(256);
    for(size_t i=0; i<s.size(); i++)
        f[s[i]].push_back(i);

    for(int i=0; i<q; i++) {
        cini(l);
        cini(r);
        l--;
        r--;

        int cnt=0;
        for(char c='a'; c<='z'; c++) {
            auto it=lower_bound(f[c].begin(), f[c].end(), l);
            if(it!=f[c].end() && *it<=r)
                cnt++;
            if(cnt>2)
                break;
        }

        if(r-l+1==1 || s[l]!=s[r] || cnt>2)
            cout<<"Yes"<<endl;
        else
            cout<<"No"<<endl;
    }

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

