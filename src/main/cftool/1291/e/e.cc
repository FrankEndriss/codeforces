/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << "=" << a << " ";
	logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* Any lamp is in max two sets.
 * Switching with a set is like xor, twice no sense.
 * So, we need to find if use a set or not.
 * For all positions only in one set we must or must not
 * use that set.
 * The must/mustnot use-sets for prefix i are same as i+1
 * so dp solution.
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);
    vb lamps(n);
    for(int i=0; i<n; i++)
        lamps[i]=(s[i]=='1');

    vector<set<int>> sets(k);
    vvi l2sets(n);

    for(int i=0; i<k; i++) {
        cini(c);
        for(int i=0; i<c; i++) {
            cini(a);
            a--;
            sets[i].insert(a);
            l2sets[a].push_back(i);
        }
    }

    set<int> must;
    set<int> mustnot;
    vector<pii> can;

    for(int i=0; i<n; i++) {
        if(l2sets[i].size()==0) {
            assert(lamps[i]);
        } else if(l2sets[i].size()==1) {   // must set
            if(lamps[i]) {
                mustnot.insert(l2sets[i][0]);
            } else {
                must.insert(l2sets[i][0]);
                for(int l: sets[l2sets[i][0]])
                    lamps[l]=!lamps[l];
            }
        } else if(l2sets[i].size()==2) {
            can.emplace_back(l2sets[i][0], l2sets[i][1]);
        } else
            assert(false);

/* now we need to find the minimum set of sets of the
 * pairs in can to switch all lamps on wich are off.
 */
    }
    
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
		solve();
}

