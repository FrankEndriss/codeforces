/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Problem text is very confusing.
 * It turns out we got blocks of digits of
 * max size 9.
 * These blocks must not begin with a certain digit,
 * and must be divisible by a given number.
 * We need to know how much such numbers exist per block,
 * and multiply those results.
 * We can "cut" out the first digit while calculating
 * the number of multiples of given divisor.
 */
void solve() {
    cini(n);
    cini(k);    // k<=9
    cinai(a, n/k);
    cinai(b, n/k);

    const int MOD=1e9+7;
    int ten=1;  // number of numbers in each block
    for(int i=0; i<k; i++)
        ten*=10;
        
    int ans=1;
    for(int block=0; block<n/k; block++) {
//cerr<<"a[block]="<<a[block]<<" b[block]="<<b[block]<<endl;
        int da=(ten-1)/a[block]+1;  // number of multiples of a[block] plus one for "000000"

/* example b[block]==7 -> 7999/a[i] - 6999/a[i] 
 * b[block]==0 -> 0999/a[i] - 0
 **/
        int db;
        if(b[block]>0)
            db=((ten/10)*(b[block]+1)-1)/a[block] - max(0ll, (((ten/10)*b[block])-1)/a[block]);
        else
            db=(ten/10-1)/a[block]+1;
//cerr<<"da="<<da<<" db="<<db<<endl;
        ans=(ans*(da-db))%MOD;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

