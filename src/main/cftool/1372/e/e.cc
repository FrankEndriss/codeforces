/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * we need to create biggest groups of intervals.
 *
 * find biggest group, add.
 * remove all intervals of that group.
 * Repeat.
 *
 * How to remove and find the intervals?
 * Number of intervals is not so big, and intersections
 * are not that big, too.
 * We can work with events.
 *
 * Put events into set, iterate set, count intervals.
 * at maxval store maxset.
 * remove maxset.
 * repeat.
 *
 * ...This does not work :/
 * Among possible max groups, how to find the right one?
 * some dp somehow?
 * dfs, just try all possible biggest groups? No, that could be fac(100).
 * ->no, its faster, that would work.
 *  Just start with all columns, choose one of the as the 'first' col.
 *  Add up all intervals over that column and continue to solve both
 *  halfs left and right.
 *  Simply try all colunms as first colunm.
 */
void solve() {
    cini(n);
    cini(m);

    vector<vector<pii>> iv(n, vector<pii>(m+1));
    for(int i=0; i<n; i++) {
        cini(k);
        for(int j=0; j<k; j++) {
            cini(l);
            cini(r);
            for(int o=l; o<=r; o++) {
                iv[i][o].first=r;
                iv[i][o].second=l;
            }
        }
    }

    vvi dp(m+1, vi(m+1, -1));
    function<int(int,int)> calc=[&](int l, int r) -> int {
        if(dp[l][r]>=0)
            return dp[l][r];

        int ma=0;
        for(int i=l; i<=r; i++) {
            int a=0;
            for(int j=0; j<n; j++) {
                auto p=iv[j][i];
                if(p.second>=l && p.second<=i && p.first>=i && p.first<=r)
                    a++;
            }

            a*=a;
            if(i>l)
                a+=calc(l,i-1);
            if(i<r)
                a+=calc(i+1,r);

            ma=max(ma, a);
        }
        return dp[l][r]=ma;
    };


    int ans=calc(1,m);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
