/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* One operation is removing one number from the circle.
 *
 * Consider last 3: We remove the smallest.
 * Last 5: Create the smallest possible sum, so we can remove
 * the smallest possible in next step.
 *
 * Last 7: Create the minimum possible trible while
 * maintaing the max possible others...sic :/
 *
 * This must be some greedy, else 1e5 would not work.
 *
 * Assume dp, for every index we have possible choices:
 * -combine with idx-2
 * -combine with idx+2
 * -remove, ie combine idx-1 and idx+1
 * -ignore, ie flip to next
 *
 * Obs:
 * We want to create a biggest possible sum because that
 * one will be the last.
 *
 * From two numbers next ot each other only one will contribute.
 * Except last move :/
 */
void solve() {
    cini(n);
    cinai(a,n);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
