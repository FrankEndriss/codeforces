/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* arithmetic seq
 * x<y
 *
 * assume y is biggest element,
 * and k is minimized.
 * Find if first element is gt 0, if not increase all elements
 * by amount.
 *
 **/
void solve() {
    cini(n);
    cini(x);
    cini(y);

    int diff=y-x;
    /* find smallest k so that number of elements is lteq n */

    for(int i=1; i<=diff; i++) {
        //cerr<<" diff="<<diff<<" diff%i="<<diff%i<<" diff/i+1="<<diff/i+1<<endl;
        if(i<=diff && diff%i==0 && diff/i+1<=n) {
            int first=y-(n-1)*i;
            int offs=0;
            while(first+offs<=0)
                offs+=i;
            for(int j=0; j<n; j++)
                cout<<offs+first+i*j<<" ";
            cout<<endl;
            return;
        }
    }
    //cerr<<"no sol for n="<<n<<" x="<<x<<" y="<<y<<endl;
    assert(false);

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
