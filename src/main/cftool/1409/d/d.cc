/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int dsum(int n) {
    int ans=0;
    while(n) {
        ans+=n%10;
        n/=10;
    }
    return ans;
}

/* 
 * _less_ than or equal
 * decrease by increasing to the next 0.
 **/
void solve() {
    cini(n);
    cini(s);

    int ds=dsum(n);
    int ten=1;
    int ans=0;
    //cerr<<"dsum="<<ds<<endl;
    while(ds>s) {
        //cerr<<"n="<<n<<" ds="<<ds<<" s="<<s<<endl;
        assert(n>1);

        int d=n%10;
        n/=10;

        if(d>0) {
            int lans=(10-d)*ten;
            ans+=lans;
            n++;
            ds=dsum(n);
        }

        ten*=10;
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
