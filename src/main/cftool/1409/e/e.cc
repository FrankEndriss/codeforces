/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;



const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */

S st_op(S a, S b) {
    return max(a,b);
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

// example: segtree my_segtree(N);
/* Ok, we can ignore y coordinate, since we can place the plattforms below 
 * the southmost point.
 * So it is asked for two segments of size k covering the most points possible,
 * and ans=number of these points.
 *
 * So, first platform start at some point.
 * Second platform starts at some point after the last point of 
 * the first platform.
 *
 * If we iterate the points, then find foreach point the number of points covered if
 * a platform starts there.
 * Then iterate that array left to right, and foreach entry find the max of the
 * second segment.
 * Use segtree to maintain the max values in range.
 */
void solve() {
    cini(n);
    cini(k);

    cinai(x,n);
    cinai(y,n); /* ignored */

    sort(all(x));

    /* dp[i]=number of covered points if left border is at point i */
    vi dp(n);
    for(int i=0; i<n; i++) {
        auto it=upper_bound(all(x), x[i]+k);
        dp[i]=distance(x.begin()+i, it);
    }

    stree seg(dp);

    int ans=0;
    for(int i=0; i<n; i++) {
        int lans=dp[i];
        int j=i+dp[i];
        if(j<n)
            lans+=seg.prod(j,n);
        ans=max(ans, lans);
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
