/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cins(goods);
    set<char> good;
    for(char c : goods)
        good.insert(c);

    cins(pat);
    bool hasStar=false;
    int posStar=-1;
    for(int i=0; i<pat.size(); i++) {
        if(pat[i]=='*') {
            hasStar=true;
            posStar=i;
            break;
        }
    }

    cini(n);
    for(int i=0; i<n; i++) {
        cins(s);

        if(!hasStar) {
            bool ok=false;
            if(pat.size()==s.size()) {
                ok=true;
                for(size_t k=0; k<s.size(); k++) {
                    if((pat[k]=='?' && good.count(s[k])) || pat[k]==s[k])
                        ;
                    else
                        ok=false;
                }
            }
            if(ok)
                cout<<"YES"<<endl;
            else
                cout<<"NO"<<endl;

        } else {
            if(s.size()+1<pat.size()) {
                cout<<"NO"<<endl;
                continue;
            }

            bool ok=true;

            /* check positions left of star */
            for(int j=0; ok && j<posStar; j++) {
                if((pat[j]=='?' && good.count(s[j])) || pat[j]==s[j])
                    ;
                else
                    ok=false;
            }

            /* check positions right of star */
            int pos=1;
            for(int j=pat.size()-1; ok && j>posStar; j--,pos++) {
                if((pat[j]=='?' && good.count(s[s.size()-pos])) || pat[j]==s[s.size()-pos])
                    ;
                else
                    ok=false;
            }

            /* check positions covered by star */
            int lenStar=s.size()+1-pat.size();
            for(int j=0; ok && j<lenStar; j++) {
                if(good.count(s[posStar+j])==1)
                    ok=false;
            }
            if(ok)
                cout<<"YES"<<endl;
            else
                cout<<"NO"<<endl;
        }

    }
}

int main() {
        cin.tie(nullptr);
        std::ios::sync_with_stdio(false);
        solve();
    }

