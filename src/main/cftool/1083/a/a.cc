/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * dfs, each subtree returns the max gasoline it can 
 * contribute.
 * Then add two biggest subtrees plus current node.
 * Note that a subtree maybe contributes 0.
 */
void solve() {
    cini(n);
    cinai(w,n);

    vector<vector<pii>> adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        cini(km);

        adj[u].push_back({v,km});
        adj[v].push_back({u,km});
    }

    int ans=0;

    function<int(int,int)> dfs=[&](int v, int p) {
        int mac=0;
        int mas=0;
        for(auto c : adj[v]) {
            if(c.first==p)
                continue;
            int val=max(0LL, dfs(c.first, v)-c.second);
            mas=max(mas, mac+val);
            mac=max(mac, val);
        }
        ans=max(ans, mas+w[v]);
        return w[v]+mac;
    };

    dfs(0,-1);

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
