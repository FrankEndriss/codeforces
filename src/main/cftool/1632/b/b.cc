/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How to minimize xor sum of all pairs?
 * Obs:
 * -two adj numbers differ in at least one bit.
 * -each bit changes at least once.
 *
 * Consider the MST, we want to change it at most once.
 * So group by MST, then within that groups, group by second most sig bit
 * and so on.
 * So print simply increasing p
 * *****
 * Its other problem:
 * We want to minimize the XOR of the _max_ pair.
 * So, again consider the MST, it contributes at least once.
 * So try to minimize that by placing it next to 1.
 *
 * for n=2^y+x
 * ...
 * 1
 * 1001
 * 1000
 * ...
 *
 * for n=2^y
 * x...
 * 1
 * 1000
 * *******
 * Again slightly other problem:
 * Here, permutation starts at 0, not at 1.
 */
void solve() {
	cini(n);
	n--;

	int b=1;
	while(b*2<=n)
		b*=2;

	for(int i=1; i<b; i++) 
		cout<<i<<" ";
	cout<<0<<" "<<b<<" ";
	for(int i=b+1; i<=n; i++)
		cout<<i<<" ";
	cout<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
