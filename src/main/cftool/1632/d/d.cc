/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e9+7;
using S=struct {
	int l, r, g;
};

S st_op(S a, S b) {
    return { min(a.l,b.l), max(a.r,b.r), a.g==INF?b.g:b.g==INF?a.g:gcd(a.g,b.g)};
}

S st_e() {
    return { INF, 0, INF };
}

using stree=segtree<S, st_op, st_e>;

/**
 * if there is some subarray with gcd=size of array then we 
 * need to change at least one element of it.
 * Note that in overlapping such subarrays we can change 
 * several at once.
 *
 * So we need to find  the longest such subarr starting at
 * first position, then the longest starting at the first
 * position after the first... and so on.
 *
 * If linear search starting from some pos, we need to go right
 * until gcd becomes smaller than size.
 * But that helps only little if all the subarrays have size 1.
 * Consider x to be some big numbers, and array a like:
 * x x x ... x 1
 * How to see that ans=0 for all prefixes?
 *
 * ****
 * Consider some segment tree giving gcd.
 * Note that gcd becomes smaller for increasing size of segment.
 * So, we can binary search that position foreach left end...somehow.
 */

bool f(S s) {
	if(s.g==INF)
		return true;

	if(s.g>s.r-s.l+1)
		return true;
	else
		return false;
}

void solve() {
	cini(n);

	vector<S> a(n);
	for(int i=0; i<n; i++)  {
		cin>>a[i].g;
		a[i].l=i;
		a[i].r=i;
	}

	stree seg(a);

	int ans=0;
	int done=-1;
	for(int i=0; i<n; i++) {
		if(i<done) {
			cout<<ans<<" ";
			continue;
		}

		int r=seg.max_right<f>(i);
		cerr<<"i="<<i<<" r="<<r<<endl;
		if(r<n) {
			S s=seg.prod(i,r+1);
			cerr<<"s.l="<<s.l<<" s.r="<<s.r<<" s.g="<<s.g<<endl;
			if(s.r-s.l+1==s.g) {
				ans++;
				done=s.r;
				cerr<<"done="<<done<<endl;
			}
		}
		cout<<ans<<" ";


	}
}

signed main() {
    solve();
}
