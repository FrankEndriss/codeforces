/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * a<b
 * We want to make a some submask of b, then apply a|=b
 *
 * Consider some bit in a that is not set in b.
 * We can increase a that number of times up to the
 * next submask of b.
 * And we can do the same with b.
 * It seems to never makes sense to do both.
 */
void solve() {
	cini(a);
	cini(b);

	int aa=a;
	int ans=0;
	for(; (aa|b)!=b; aa++) {
		ans++;
	}
	assert((aa|b)==b);
	if(aa!=b)
		ans++;

	int ans2=1;
	for(int bb=b; (a|bb)!=bb; bb++) {
		ans2++;
	}

	cout<<min(ans,ans2)<<endl;

}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
