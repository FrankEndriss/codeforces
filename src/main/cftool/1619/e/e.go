package main

import (
    "bufio"
    . "fmt"
    "os"
    "sort"
)

/* 
* lets try some fancy go :)

sort existing numbers, iterate starting from 0
if current number eq it then ok, else if current number < it
put number on stack else take number from stack, ans+=diff.

Also we need to consider that we need to increase all
numbers eq it.
*/
func main() {
    re, w, e := bufio.NewReader(os.Stdin), bufio.NewWriter(os.Stdout), bufio.NewWriter(os.Stderr)
    defer w.Flush()
    defer e.Flush()

    var t int=0
    Fscan(re, &t)

    for i:=0; i<t; i++ {
        var n int
        Fscan(re, &n)

        var a=make([]int,n)
        var f=make([]int,n+1)

        for i:=0; i<n; i++ {
            Fscan(re, &a[i])
            f[a[i]]++
        }

        sort.Ints(a);
        st:=[]int{}
        var ans int64=0
        ok:=true;

        for i:=0; i<=n; i++ {
            if !ok {
                Fprintf(w, "-1 ")
                continue;
            }

            if f[i]>0 {
                Fprintf(w, "%d ", ans+int64(f[i]))
                for j:=1; j<f[i]; j++ {
                    st=append(st, i)
                }
            } else {
                Fprintf(w, "%d ", ans)
                if(len(st)==0) {
                    ok=false
                } else {
                    b:=st[len(st)-1]
                    ans+=int64(i-b)
                    st=st[:len(st)-1]
                }
            }
        }
        Fprintf(w,"\n")
    }
}
