package main

import (
    "bufio"
    . "fmt"
    "os"
    "sort"
)

/* why not work as expected ? */
func main() {
    re, w, e := bufio.NewReader(os.Stdin), bufio.NewWriter(os.Stdout), bufio.NewWriter(os.Stderr)
    defer w.Flush()
    defer e.Flush()

    var t int=0
    Fscan(re, &t)

    set :=make(map[int]bool)
    const N int=1e9+7;
    Fprintf(e, "N=%d\n", N)

    for i:=1; i*i<N; i++ {
        set[i*i]=true;
    }
    for i:=1; i*i*i<N; i++ {
        set[i*i*i]=true;
    }

    v := []int{}
    for i := range set {
        v=append(v, i)
    }
    Fprintf(e, "len(v)=%d\n", len(v))

    sort.Ints(v);
    //sort.Slice(v, func(p,q int) bool {
    //    return p<q
    //})
    Fprintf(e, "v[]=%d %d %d %d...\n", v[0], v[1], v[2], v[3])

    for i:=0; i<t; i++ {
        var n int
        Fscan(re, &n)

        /* v.lower_bound */
        idx := sort.SearchInts(v,n)
        if idx<len(v) && v[idx]==n {
            idx++;
        }
        Fprintf(w, "%d\n", idx)

    }
}
