package main

import (
    "bufio"
    . "fmt"
    "os"
//    "sort"
)

/* 
* lets try some fancy go :)
we need to binary search the maximum possible min of joy.

So, for given joy x check if we can buy a gift foreach friend.
How to implement?
Since we can buy multiple presents in each shop,
we need to find a set of n-1 shops so that
all friends are satisfied.

Most likely there are more shops than n-1, since else we would
simply buy in all shops.

We could solve with max flow algo, but that maybe TLE?

Can we choose one shop per friend?
Ok, we need to find one shop that works for two (or more) friends.
If such shop exists, then we are done since we can 
choose any shop for any other friend.
*/

func main() {
    re, w, e := bufio.NewReader(os.Stdin), bufio.NewWriter(os.Stdout), bufio.NewWriter(os.Stderr)
    defer w.Flush()
    defer e.Flush()

    var t int=0
    Fscan(re, &t)

    for i:=0; i<t; i++ {
        var m,n int64
        Fscan(re, &m, &n)

        var ma=make([]int64,n)    // max joy friend i can get in any shop 
        var p=make([][]int64, m)

        var i int64=0
        var j int64=0
        for ; i<m; i++ {
            p[i]=make([]int64, n)
        }

        // max two joys of any friends in shop i
        var ma2=make([][]int64, m)

        for i=0; i<m; i++ {
            ma2[i]=make([]int64, 2)
        }

        for i=0; i<m; i++ {
            for j=0; j<n; j++  {
                Fscan(re, &p[i][j])
                if p[i][j]>ma[j] {
                    ma[j]=p[i][j]
                }
                if ma2[i][0]<p[i][j] {
                    ma2[i][1]=ma2[i][0]
                    ma2[i][0]=p[i][j]
                } else if ma2[i][1]<p[i][j] {
                    ma2[i][1]=p[i][j]
                }
            }
        }

        var mi int64=1000000000  // min max joy of any friend, upper bound for ans
        for i=0; i<n; i++ {
            if mi>ma[i] {
                mi=ma[i]
            }
        }

        var l int64=0
        var r int64=mi+1

        for ; l+1<r ; {
            mid:=(l+r)/2
            ok:=false
            for i=0; !ok && i<m; i++ {
                if ma2[i][1]>=mid  {
                    ok=true
                }
            }
            if ok {
                l=mid
            } else {
                r=mid
            }
        }

        Fprintf(w, "%d\n", l)
    }
}
