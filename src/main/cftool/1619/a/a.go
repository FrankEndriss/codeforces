package main

import (
    "bufio"
    . "fmt"
    "os"
)

func main() {
    re, w := bufio.NewReader(os.Stdin), bufio.NewWriter(os.Stdout)
    defer w.Flush()

    var t int=0
    Fscan(re, &t)

    for i:=0; i<t; i++ {
        var s string
        Fscan(re, &s)
        //Fprintf(w, "%s\n", s)

        var ok=true
        if(len(s)%2==1) {
            ok=false;
        }

        for j:=0; j<len(s)/2; j++ {
            if(s[j]!=s[j+len(s)/2]) {
                ok=false
            }
        }

        if ok {
            Fprintf(w, "YES\n")
        } else {
            Fprintf(w, "NO\n")
        }
    }
}
