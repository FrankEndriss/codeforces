package main

import (
    "bufio"
    . "fmt"
    "os"
//    "sort"
)

/* 
* lets try some fancy go :)

foreach digt in a find the digit of b so
that the sum matches the current digit in s or the next two
digits in s.
*/
func main() {
    re, w, e := bufio.NewReader(os.Stdin), bufio.NewWriter(os.Stdout), bufio.NewWriter(os.Stderr)
    defer w.Flush()
    defer e.Flush()

    var t int=0
    Fscan(re, &t)

    for i:=0; i<t; i++ {
        var a,s int64
        Fscan(re, &a, &s)

        if a==s {
            Fprintf(w, "0\n")
            continue
        }

        ok:=true
        ans:= []int64{}
        for ; a>0 || s>0; {
            d:=a%10
            a/=10
            ds:=s%10;
            s/=10
            if ds>=d {
                ans=append(ans, ds-d)
            } else {
                ds+=(s%10)*10
                s/=10
                if ds<d || ds>d+9 {
                    ok=false
                } else {
                    ans=append(ans,ds-d);
                }
            }
        }

        if !ok {
            Fprintf(w, "%d\n", -1)
        } else {
            /* remove leading zeros, sic. How is supposed syntax???  */
            for ; len(ans)>0 && ans[len(ans)-1]==0 ; {
                ans=ans[:len(ans)-1]
            }

            for i:=len(ans)-1; i>=0 ; i-- {
                Fprintf(w, "%d", ans[i])
            }
            Fprintf(w, "\n")
        }
    }
}
