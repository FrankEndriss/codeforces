/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* First check if number of elments is possible.
 * Since we erase k-1 elements in each operation, 
 * it must hold that (n-m)%(k-1)==0
 *
 * Then, with each operation we need to choose one of the remaining 
 * elements, and k/2 left of it, and k/2 right of it.
 *
 * Lets call elements that need to be removed removals.
 * Once we have an element with same number of removals on both sides,
 * we can choose that element as the center for all remaining 
 * operations.
 * So, ans=YES if we can create such element.
 *
 * Note that we cannot choose an element as center if there are
 * to less elements either on the left or the right.
 *
 * So, if enumerated all removals, we can choose any k elements from
 * them and remove them.
 * If we split the removals in two halfs. Then if there is an element
 * between the two median elements, we can choose that as the center
 * for all remaining operations.
 * So, if there is no such element, then  the two medians are
 * adjacent. We try to move the two median elements to a position where
 * they are not adjacent. "Moving" the medians means we remove some
 * elements, so that others become the new medians.
 * Consider the first element to the left or right from the medians
 * which is not an adjacent. Let x be the distance from the median element
 * to that element.
 * If no such element exists, ans=NO.
 * Else we want to make it median by removing appropriate number of 
 * elements on both sides of it. To move it by one position we need
 * to remove two elements on the other side.
 * Just check if enough elements are available on both sides to make
 * it happen. Also this check must consider k.
 */
void solve() {

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
