/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* We need to find how many traps can be disarmed
 * within the time limit, starting with trap with
 * higest demage.
 * From that we can binsearch the number of soldiers
 * having more power than the hardest remaining trap.
 *
 * Binsearch
 * Howto check if x traps can be disarmed?
 * -find traps, sort by l[i]
 * -go with squad up to first armed trap
 * -go allone to r[i] of that trap
 * -while there where more traps on the way, go up to their r[j]
 * -go back to the swat and
 * -repeat going with swat to the next armed trap
 *
 * So we minimize the distance we walk without the squat.
 * Sum(time) is way from 0 to boss (n+1) + 2*trap_disarm_distance
 *
 * ...TODO there is still some of by one error...sic :/
 *
struct trap {
    int l, r, d;
};
void solve() {
    cini(m);
    cini(n);
    cini(k);
    cini(t);
    cinai(a,m);
    sort(a.begin(), a.end(), greater<int>());

    vector<trap> tr(k);
    for(int i=0; i<k; i++) {
        cin>>tr[i].l>>tr[i].r>>tr[i].d;
    }

    int l=0;
    int r=m+1;
    while(l+1<r) {  // binsearch max num of soldiers
        int mid=(l+r)/2;

        int maxD=1e9;
        if(mid>0)
            maxD=a[mid-1];   // weakest soldier in squat

        vector<pii> seg;
        for(int i=0; i<k; i++)
            if(tr[i].d>maxD && tr[i].r>=tr[i].l)
                seg.emplace_back(tr[i].l, tr[i].r);
        sort(seg.begin(), seg.end());

        int textra=0;
        int pos1=0; // pos of squad
        int x=0;    // pos of max x we must reach before we can go back
        for(int i=0; i<seg.size(); i++) {
            if(seg[i].first>x) {
                textra+=x-pos1;
                pos1=seg[i].first;
                x=max(x, seg[i].second);
            } else {
                x=max(x, seg[i].second);
            }
        }
        textra+=x-pos1;
        if(textra*2+n+1>t)
            r=mid;
        else
            l=mid;
    }

    int ans=l;
    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

fuck this shit...submit copy of tutorial, just to get rid of it.
 */

typedef pair<int, int> pt;

#define x first
#define y second


int m, n, k, t;
vector<int> l, r, d, a;

bool can(int x)
{
    int mn = int(1e9);
    for (int i = 0; i < x; i++)
        mn = min(mn, a[i]);
    vector<pt> segm;
    for (int i = 0; i < k; i++)
        if (d[i] > mn)
            segm.push_back(make_pair(l[i], r[i]));
    int req_time = 0;
    sort(segm.begin(), segm.end());
    int lastr = 0;
    for (auto s : segm)
    {
        if (s.x <= lastr)
        {
            req_time += max(0, s.y - lastr);
            lastr = max(s.y, lastr);
        }
        else
        {
            req_time += s.y - s.x + 1;
            lastr = s.y;
        }
    }
    req_time = 2 * req_time + n + 1;
    return req_time <= t;
}

int main()
{
#ifdef _DEBUG
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    scanf("%d %d %d %d", &m, &n, &k, &t);
    a.resize(m);
    for (int i = 0; i < m; i++)
        scanf("%d", &a[i]);
    sort(a.begin(), a.end());
    reverse(a.begin(), a.end());
    l.resize(k);
    r.resize(k);
    d.resize(k);
    for (int i = 0; i < k; i++)
        scanf("%d %d %d", &l[i], &r[i], &d[i]);

    int lf = 0;
    int rg = m + 1;
    while (rg - lf > 1)
    {
        int mid = (lf + rg) / 2;
        if (can(mid))
            lf = mid;
        else
            rg = mid;
    }
    printf("%d\n", lf);
    return 0;
}
