/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * We need to decide for each position if a block starts there or not.
 *
 * So we build an index with blocksums starting at each position, and 
 * collect all possible blocksums, and the positions where they start.
 *
 * Then check each blocksum. How, fast?
 * We need per blocksum a map from blocksum to list of positions.
 * Then we sort that list by R position and find most possible
 * non overlapping intervals.
 */
void solve() {
    cini(n);    // max 50/1500
    cinai(a,n);

    map<int,vector<pii>> blstart;   /* blstart[s]=vector<<idxL,idxR>> of blocks with sum s. */
    for(int j=0; j<n; j++) {
        int sum=0;
        for(int i=j; i>=0; i--) {
            sum+=a[i];
            blstart[sum].push_back({i,j});  // sorted by j 
        }
    }

    vector<pii> ans;
    for(auto ent : blstart) {
        /*
        sort(all(ent.second), [](pii p1, pii p2) {
                if(p1.second!=p2.second)
                    return p1.second<p2.second;
                else
                    return p1.first<p2.first;
                });
                */

        vector<pii> lans;

        int next=-1;
        for(pii p : ent.second) {
            if(p.first>next) {
                lans.push_back(p);
                next=p.second;
            }
        }
        if(lans.size()>ans.size()) {
            ans=lans;
        }
    }
    cout<<ans.size()<<endl;
    for(pii p : ans) 
        cout<<p.first+1<<" "<<p.second+1<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
