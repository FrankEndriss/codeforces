/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(H);
    cini(n);
    cinai(d,n);

    int mi=0;
    int sum=0;
    for(int i=0; i<n;  i++) {
        sum+=d[i];
        mi=min(mi, sum);

        if(H+sum<=0) {
            cout<<i+1<<endl;
            return;
        }
    }

    if(sum>=0) {
        cout<<-1<<endl;
        return;
    }

/* note mi and sum negative */
    int cyc=(H+mi-sum-1)/-sum;
    int ans=cyc*n;
    H+=cyc*sum;
    for(int i=0; i<n; i++) {
        H+=d[i];
        ans++;
        if(H<=0) {
            cout<<ans<<endl;
            return;
        }
    }
    assert(false);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
