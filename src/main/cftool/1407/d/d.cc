/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* we need to memoize jump from backwards
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(h,n);

    vi dp(n, INF); /* dp[i]=min number of jumps to position n-1 */
    dp[n-1]=0;

    function<void(int)> go=[&](int idx) {
        int mi=INF;
        int ma=-1;
        for(int i=idx+1; i<n; i++) {
            int mit=min(h[idx], h[i]);
            int mat=max(h[idx], h[i]);

            if(mat<mi || mit>ma)
                dp[idx]=min(dp[idx], dp[i]+1);

            mi=min(mi, h[i]);
            ma=max(ma, h[i]);

            if((mi==h[idx] || ma==h[idx]) || (mi<h[idx] && ma>h[idx])) {
                break;
            }
        }
    };
    for(int i=n-2; i>=0; i--)
        go(i);

    cout<<dp[0]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
