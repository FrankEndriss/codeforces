/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* How?
 * Is there allways a subarray of length
 */
void solve() {
    cini(n);    // even
    cinai(a,n); // + - + -

    if(n==2) {
        if(a[0]==0 || a[1]==0) {
            cout<<"1"<<endl<<"0"<<endl;
        } else {
            cout<<"2"<<endl<<"1 1"<<endl;
        }
        return;
    }

    int idx=0;
    vi ans;
    for(int i=0; ans.size()<n/2 && i<n; i+=2) {
        if(a[i]==a[i+1]) {
            ans.push_back(a[i]);
            ans.push_back(a[i+1]);
        } else {
            ans.push_back(a[i+2]);
            ans.push_back(a[i+2]);
            i++;
        }
    }

    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
