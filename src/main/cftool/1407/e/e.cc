/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* night and morning... ???
 *
 * If cities have no colors then there are
 * some shortest paths.
 *
 * We can block roads by giving the starting city
 * the opposite color.
 * So we can block per city a (possibly empty) set of roads, 
 * or all other roads outgoing from that city.
 *
 * We want to block the roads in a way that the path from 
 * 1 to n is longest possible afterwards.
 * Best would be to make two components for infinite path.
 */
void solve() {
    cini(n);
    cini(m);

    vector<vector<pii>> adj(n);
    for(int i=0;i <m; i++) {
        cini(u);
        cini(v);
        cini(t);

    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
