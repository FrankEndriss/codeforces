/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int N=1e3+7;
/* find lex maximal gcd()...
 * b[0],b[1] must be among the max possible gcd.
 * Then, how to find b[2]?
 */
void solve() {
    cini(n);
    cinai(a,n);
    if(n==1) {
        cout<<a[0]<<endl;
        return;
    }
       

    int g=*max_element(all(a));

    vi ans;

    while(a.size()) {
        int mag=-1;
        vi gc(a.size());
        for(size_t i=0; i<a.size(); i++) {
            gc[i]=gcd(g,a[i]);
            if(gc[i]>mag) {
                mag=gc[i];
            }
        }
        assert(mag>0);
        for(int i=a.size()-1; i>=0; i--) {
            if(gc[i]==mag) {
                ans.push_back(a[i]);
                a.erase(a.begin()+i);
            }
        }
        g=mag;
    }

    for(int b : ans)
        cout<<b<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
