/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Find pos where a div and b div.
 *
 * find left div by a, save positions.
 * find right div by b, if pos is a div then it is ans.
 */
void solve() {
    cins(s);
    cini(a);
    cini(b);
    const int n=s.size();

    vb dpa(n);
    int sum=0;
    for(size_t i=0; i+1<n; i++) {
        sum*=10;
        sum+=(s[i]-'0');
        sum%=a;
        if(sum==0)
            dpa[i]=true;
    }

    int ten=1;
    bool unzero=false;
    sum=0;
    for(int i=n-1; i>0; i--) {
        if(s[i]!='0')
            unzero=true;
        sum=(sum+(s[i]-'0')*ten)%b;
        if(unzero && sum==0 && dpa[i-1]) {
            cout<<"YES"<<endl;
            cout<<s.substr(0,i)<<endl;
            cout<<s.substr(i)<<endl;
            return;
        }
        ten=(ten*10)%b;
    }
    cout<<"NO"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
