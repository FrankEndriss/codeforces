/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We should reverse the queries, starting with 0.
 * Then adding each vertex we do one loop of
 * floyd warshal.
 * This is, we try to find for all vertex pairs if
 * it is possible to us cheaper path if current
 * vertex (and edges) is included.
 * Result is current min dist matrix, ans == sum of
 * all cells.
 */
const int INF=1e18;
void solve() {
    cini(n);
    vvi w(n, vi(n));
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            cin>>w[i][j];

    vi x(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        x[i]=aux-1;
    }
    reverse(all(x));

    vi vis;
    stack<int> ans;
    for(int k : x) {

        for(int i=0; i<n; i++)
            for(int j=0; j<n; j++)
                if(i!=j)
                    w[i][j]=min(w[i][j], w[i][k]+w[k][j]);

        vis.push_back(k);
    
        int sum=0;
        for(int k1 : vis){
            for(int k2 : vis) {
                if(k1!=k2)
                    sum+=w[k1][k2];
            }
        }
        ans.push(sum);
    }
    while(ans.size()) {
        cout<<ans.top()<<" ";
        ans.pop();
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
