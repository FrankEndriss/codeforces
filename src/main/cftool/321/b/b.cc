/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* strategies:
 * 1.
 * Use all possible attacks (biggest ones) on ATK cards.
 * demage is sum of all those cards.
 * So we use the biggest cards on the smallest ATKs.
 *
 * Kill all cards, ATKs with biggest possible.
 * Demage is sum of cards not used on DEF. So we need to use the
 * minimum value on DEFs still beeing able to kill all DEF.
 * ->
 *  1. Use smallest possible cards to destroy all DEF.
 *  2. Play other cards biggest to smallest.
 *     This might destroy all, or not.
 */

const int INF=1e9;

void solve() {
    cini(n);
    cini(m);
    multiset<pii> a1;
    multiset<pii> a2;
    for(int i=0; i<n; i++) {
        cins(s);
        cini(v);
        if(s=="ATK")
            a1.insert({v,i});
        else
            a2.insert({v,i});
    }

    multiset<pii> b;
    for(int i=0; i<m; i++) {
        cini(aux);
        b.insert({aux,i});
    }

    /* Strat 2: try to kill all cards */
    multiset<pii> aa1=a1;
    multiset<pii> aa2=a2;
    multiset<pii> bb=b;

    while(aa2.size() && b.size()) {
        auto itdef=aa2.begin();
        auto it=bb.upper_bound({itdef->first, INF});
        if(it==bb.end())
            break;
        aa2.erase(itdef);
        bb.erase(it);
    }

    int ans2=0;
    if(aa2.size()==0) {
        while(bb.size()) {
            auto itb=bb.rbegin();
            if(aa1.size())  {
                auto ita=aa1.rbegin();
                if(itb->first>=ita->first) {
                    ans2+=itb->first;
                    ans2-=ita->first;
                    aa1.erase(*ita);
                } else
                    break;
            } else {
                ans2+=itb->first;
            }

            bb.erase(*itb);
        }
    }

    /* Strat 1: try to kill all ATKs getting most possible points */
    int ans1=0;
    while(b.size() && a1.size()) {
        auto itb=b.rbegin();
        auto ita=a1.begin();
        if(itb->first>ita->first) {
            ans1+=itb->first;
            ans1-=ita->first;
            b.erase(*itb);
            a1.erase(*ita);
        } else
            break;
    }

    cout<<max(ans1, ans2)<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
