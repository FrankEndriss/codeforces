/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* adjust as needed */
using S=pii;

S st_op(S a, S b) {
    return {min(a.first,b.first), max(a.second,b.second)};
}

const int INF=1e9;
S st_e() {
    return {INF,-INF};
}

using stree=segtree<S, st_op, st_e>;

/**
 * Its the number of local minima and local maxima,
 * since these are connected.
 * ...they are not.
 *
 * Each a[i] is connected to the max right of it,
 * and the min right of it, also to some more
 * vertext, but they dont help I guess.
 * So just find min/max right.
 * ...no.
 *
 * At each position we can jump to the next local extreme,
 * or less, but that would not make sense.
 * ...still no.
 *
 * Consider values right of a[i], and group them as
 * a[j]>a[i] and a[j]<a[i]
 * Then we can jump exactly to the biggest element
 * in the first bigger group, or the smallest element in
 * the first smaller group.
 * How to find these indexes?
 * -> Go from right to left, find indexes of next bigger and
 *  next smaller (O(logn), maintain a set).
 *  The max of both is the begin of the second group, find
 *  again in O(logn) the end of the group, and finally
 *  the min/max index in that group.
 *
 *  How to find end (or min/max) of second group?
 *  Binary search on segment tree?
 *
 *  .....
 *  We need one observation:
 *  Consider the biggest (or smallest) element of all. This element
 *  is the endpoint of all segments including that element. It is not
 *  possible to "jump over" it.
 *  So simply solve recursive by considering 3 cases for a segement:
 *  1. first/last are min/max, so it is one step
 *  2. else max element is somewhere, split there
 *  3. else min element is somewhere, split there
 *  We can maintain a min/max segment tree to quickly find the elements.
 */
void solve() {
    cini(n);
    cinai(a,n);
    if(n<3) {
        cout<<n-1<<endl;
        return;
    }

    vi pos(n);
    vector<pii> data(n);
    for(int i=0 ;i<n; i++) {
        pos[a[i]-1]=i;
        data[i]={a[i]-1,a[i]-1};
    }

    stree seg(data);


    function<int(int,int)> go=[&](int l, int r) {
        //cerr<<"go, l="<<l<<" r="<<r<<endl;
        auto [mi,ma]=seg.prod(l,r+1);
        if((pos[mi]==l||pos[mi]==r) && (pos[ma]==l||pos[ma]==r)) 
            return 1LL;

        assert(pos[mi]>=l && pos[mi]<=r);
        assert(pos[ma]>=l && pos[ma]<=r);

        if(pos[ma]!=l && pos[ma]!=r)
            return go(l,pos[ma])+go(pos[ma],r);
        else
            return go(l,pos[mi])+go(pos[mi],r);
    };

    cout<<go(0,n-1)<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
