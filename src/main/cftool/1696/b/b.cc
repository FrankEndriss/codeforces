/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We go an array a[], positive numbers.
 * We are finished if all a[i]==0.
 * We can set a[l..r] to mex(a[l..r])
 * So we need to operate on each segment of a[i]!=0 once.
 *
 * ...
 * No.
 * Actually we can allways make all zero with two operations,
 * if there is no 0 then with one, and if all zero then with 0.
 */
void solve() {
    cini(n);
    cinai(a,n);

    while(a.size() && a.back()==0)
        a.pop_back();

    reverse(all(a));

    while(a.size() && a.back()==0)
        a.pop_back();

    n=a.size();
    int cnt=0;
    for(int i=0; i<n; i++) 
            cnt+=a[i]==0;

    if(a.size()==0)
        cout<<0<<endl;
    else if(cnt==0)
        cout<<1<<endl;
    else
        cout<<2<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
