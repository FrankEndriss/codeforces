/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Observe that b[0] is determined by a[0], so we must
 * transform a[0] into b[0].
 * Do this for all positions and check if a.size()==b.size()
 *
 * How to implement without getting insane?
 * There are cases:
 * b[i]==a[i]
 * -> remove both, check next
 * b[i]<a[i]
 * -> split a[i]
 * b[i]>a[i]
 * -> put m times a[i] into one element
 *
 *
 * ...
 * Note that there is a much simpler solution.
 * Since both operations are symetrical, we
 * can define kind of a normal form, ie to
 * operation expand on both arrays as often as
 * possible.
 * Then both arrays must be simply same.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(aa,n);

    cini(k);
    cinai(b,k);

    vector<pii> a;
    for(int i=0; i<n; i++) {
        a.emplace_back(aa[i],1);
        while(a.back().first%m==0) {
            a.back().first/=m;
            a.back().second*=m;
        }
        while(a.size()>1 && a[a.size()-1].first==a[a.size()-2].first) {
            a[a.size()-2].second+=a.back().second;
            a.pop_back();
        }
    }

    reverse(all(a));
    reverse(all(b));

    function<void()> norm=[&]() {
        while(a.size()>1 && a[a.size()-1].first==a[a.size()-2].first) {
            a[a.size()-2].second+=a.back().second;
            a.pop_back();
        }
        while(a.back().first%m==0) {
            a.back().first/=m;
            a.back().second*=m;
            norm();
        }
    };

    /* make 1 element from m */
    function<bool(int)> op2=[&](int mm) {
        if(a.back().second>=mm) {
            a.back().second-=mm;
            int val=a.back().first*mm;
            if(a.back().second==0)
                a.pop_back();
            a.emplace_back(val,1);
            //cerr<<"op2 done"<<endl;
            return true;
        } else  {
            //cerr<<"op2 notdone"<<endl;
            return false;
        }
    };

    while(a.size() && b.size()) {
        //cerr<<"a.size()="<<a.size()<<" "<<a.back().first<<" b.size()="<<b.size()<<" "<<b.back()<<endl;
        if(b.back()==a.back().first) {
            //cerr<<"case1"<<endl;
            b.pop_back();
            a.back().second--;
            if(a.back().second==0) {
                a.pop_back();
            }
            if(a.size()) 
                norm();
            continue;
        }

        if(b.back()<a.back().first) {
                cout<<"No"<<endl;
                return;
        } else if(b.back()>a.back().first) {
            int mm=m;
            while(a.back().first*mm<b.back())
                mm*=m;

            //cerr<<"case3"<<endl;
            op2(mm);
            if(b.back()!=a.back().first) {
                cout<<"No"<<endl;
                return;
            }
        }
    }
    if(a.size()==0 && b.size()==0)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
