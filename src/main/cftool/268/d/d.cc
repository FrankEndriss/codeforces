/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vvvvi= vector<vvvi>;
using vs= vector<string>;

#define endl "\n"

/* dp, most likely with memoization
 * Build a ladder in one dir, and
 * Use all other bars on one of the 3 other dirs.
 *
 * dp[i][j]=number of climbable ladders starting at i using j bars.
 *
 * Then calc sum(0,j, dp[i][j]*3^(n-j))
 *
 * ...Inclusion/Exclusion.
 * We need to consider overcount.
 * There are configurations where the inversion also counts.
 * Example input 4 2
 * Then bars at 1 3 is ok and bars at 2 4 is ok.
 * But if we turn this to 4 dirs, they are counted twice.
 * What about 5 3
 * 2 4 would be a solution, but also 1 3 5
 * *****
 * It is all to simple, we cannot calc for one dir, and mult by 4.
 * We need to consider all 4 directions in dp.
 * dp[i][j][k][l]=number of sol with dist=i,j,k,l to last bar in that 4 dirs
 * better:
 * dp[i][j][k][l]=number of sol with dists-1=i,j,k to last bar in 3 dirs that are not the last bar
 *  i=h+1 if i is not ok ladder, and l==ok/notok ladder in dir of last bar.
 */
const int MOD=1e9+9;
inline void ad(int &a, int b) {
    a+=b;
    if(a>=MOD)
        a-=MOD;
}
void solve() {
    cini(n);
    cini(h);

    vvvvi dp(2, vvvi(h+1, vvi(h+1, vi(h+1))));
    dp[1][1][1][1]=4;


    for(int nn=1; nn<n; nn++) {
        vvvvi dp0(2, vvvi(h+1, vvi(h+1, vi(h+1))));
        for(int i=0; i<=h; i++)  {
            for(int j=0; j<=h; j++)  {
                for(int k=0; k<=h; k++) {
                    for(int l=0; l<2; l++) {
                        /* same dir again */
                        ad(dp0[l][min(h,i+1)][min(h,j+1)][min(h,k+1)], dp[l][i][j][k]);

                        /* dir i */
                        ad(dp0[i<h][l?1:h][min(h,j+1)][min(h,k+1)], dp[l][i][j][k]);

                        /* dir j */
                        ad(dp0[j<h][min(h,i+1)][l?1:h][min(h,k+1)], dp[l][i][j][k]);

                        /* dir k */
                        ad(dp0[k<h][min(h,i+1)][min(h,j+1)][l?1:h], dp[l][i][j][k]);
                    }
                }
            }
        }
        dp.swap(dp0);
    }

    int ans=0;
    for(int i=0; i<=h; i++)  {
        for(int j=0; j<=h; j++)  {
            for(int k=0; k<=h; k++) {
                for(int l=0; l<2; l++) {
                    if(i<h || j<h || k<h || l==1)
                        ad(ans, dp[l][i][j][k]);
                }
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
