/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Understand the rule of the described game...
 *
 * Starter must choose a size so that it is garanteed that
 * the cell with the biggest number is in that area.
 * So it must be one bigger than the max number of rows/cell between 
 * that cell and any border.
 */
const int INF=1e9+7;
void solve() {
    cini(n);
    cini(m);

    int ma=-INF;
    int mI=-1;
    int mJ=-1;
    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) {
            cini(aux);
            if(aux>ma) {
                ma=aux;
                mI=i;
                mJ=j;
            }
        }

    int ansI=max(mI+1, n-mI);
    int ansJ=max(mJ+1, m-mJ);
    cout<<ansI*ansJ<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
