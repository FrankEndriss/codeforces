/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Use bitsets to denote possible values.
 */
const int N=2003;
const int N2=N/2;
void solve() {
    cini(n);
    cini(m);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) 
            cin>>a[i][j];

    //cerr<<"after parse n="<<n<<" m="<<m<<endl;

    vector<vector<bitset<N>>> dp(n, vector<bitset<N>>(m));
    dp[0][0][N2]=1;
    if(a[0][0]==-1)
        dp[0][0]=dp[0][0]>>1;
    else
        dp[0][0]=dp[0][0]<<1;

    for(int i=1; i<m; i++) {
        if(a[0][i]==-1)
            dp[0][i]=dp[0][i-1]>>1;
        else
            dp[0][i]=dp[0][i-1]<<1;
    }

    //cerr<<"after 2"<<endl;

    for(int i=1; i<n; i++)  {

        if(a[i][0]==-1)
            dp[i][0]=dp[i-1][0]>>1;
        else
            dp[i][0]=dp[i-1][0]<<1;

        for(int j=1; j<m; j++) {
            if(a[i][j]==-1)
                dp[i][j]=(dp[i-1][j]>>1) | (dp[i][j-1]>>1);
            else
                dp[i][j]=(dp[i-1][j]<<1) | (dp[i][j-1]<<1);
        }
    }
    //cerr<<"after 3"<<endl;

    if(dp[n-1][m-1][N2]==1)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
