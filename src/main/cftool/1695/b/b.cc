/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Two cases, the player gets its own pile at next round (n==even), 
 * or the other player gets it (n==odd)
 *
 * For odd n player takes all stones, so first player wins.
 *
 * For even n player takes one stone, so player with smallest pile
 * looses, if both same, first loses.
 */
const int INF=1e9+7;
void solve() {
    cini(n);
    cinai(a,n);

    if(n%2==1) {
        cout<<"Mike"<<endl;
        return;
    }

    int miMike=INF;
    for(int i=0; i<n; i+=2)
        miMike=min(miMike,a[i]);
    int miJoe=INF;
    for(int i=1; i<n; i+=2)
        miJoe=min(miJoe,a[i]);

    vs names={ "Mike", "Joe"};
    int ans=(n-1)%2;
    int mi=a[n-1];
    for(int i=n-1; i>=0; i--) {
        if(a[i]<=mi) {
            mi=a[i];
            ans=i%2;
        }
    }
    cout<<names[1-ans]<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
