/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* To make all palindromes it must hold that:
 * a[i][j]=a[i][m-1-j]
 * a[i][j]=a[n-1-i][j]
 * These are three cells, or one.
 * In case 3 use the middle one.
 */
void solve() {
    cini(n);
    cini(m);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) 
            cin>>a[i][j];

    int ans=0;
    for(int i=0; i<n/2; i++) 
        for(int j=0; j<m/2; j++) {
            vi aa={ a[i][j], a[i][m-1-j], a[n-1-i][j], a[n-1-i][m-1-j] };
            sort(all(aa));
            int mi=abs(aa[1]-aa[0]) + abs(aa[1]-aa[2]) + abs(aa[1]-aa[3]);
            ans+=mi;
        }

    if(n%2) {
        int i=n/2;
        for(int j=0; j*2<m; j++)
            ans+=abs(a[i][j] - a[i][m-1-j]);
    }
    if(m%2) {
        int j=m/2;
        for(int i=0; i*2<n; i++)
            ans+=abs(a[i][j] - a[n-1-i][j]);
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
