/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

/* We need to find how often each digit contributes, 
 * and with which power of 10.
 *
 * There are
 * n substring of len 1
 * all right of the position contribute with the initial power,
 * all left of the position contribute with initial power - 1
 *
 * So, the value s[0] contributes
 * n-1-0 times with ten[1+0]
 * n-2-0 times with ten[2+0]
 * ...
 * the value s[1] contributes
 * n-1-1 times with ten[1+1]
 * n-2-1 times with ten[1+2]
 * ...
 *
 * How to build the sum not in O(n^2)?
 *
 * When remove sub of len==1
 * ten[1] contributes 1*s[1] + n-1*s[0]
 * ten[2] contributes 2*s[2] + n-2*s[1]
 * ten[3] contributes 3*s[3] + n-3*s[2]
 * ...
 *
 * When remove sub of len==2
 * ten[2] contributes 1*s[2] + n-2*s[0]
 * ten[3] contributes 2*s[3] + n-3*s[1]
 * ten[4] contributes 3*s[4] + n-4*s[2]
 * ...
 *
 * When remove sub of len==3
 * ten[3] contributes 1*s[3] + n-3*s[0]
 * ten[4] contributes 2*s[4] + n-4*s[1]
 * ten[5] contributes 3*s[5] + n-5*s[2]
 * ...
 *
 * sum of 
 * ten[3] contributes (3+2+1)*s[3] + (n-3)*s[0] + (n-3)*s[1] + (n-3)*s[2]
 * ten[i] contributes gaus(i)*s[i] + (n-i)*spre[i]
 *
 */

int gaus(int i) {
    return (i*(i+1)/2)%MOD;
}

void solve() {
    cins(s);
    const int n=s.size();


    vi ten(n);
    ten.back()=1;
    for(int i=n-1; i>=1; i--) {
        ten[i-1]=mul(ten[i], 10);
    }

    vi spre(n);
    spre[0]=s[0]-'0';
    for(int i=1; i<n; i++)
        spre[i]=spre[i-1]+s[i]-'0';

    int ans=0;
    for(int i=1; i<n; i++) {
        // ten[i] contributes gaus(i)*s[i] + (n-i)*spre[i]
        ans=pl(ans, mul(ten[i], pl(mul(gaus(i), s[i]-'0'), mul(n-i, spre[i-1]))));
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
