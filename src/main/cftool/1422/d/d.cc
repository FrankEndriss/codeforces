/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* sounds like dijkstra...
 * To much edges?
 *
 * So we need to "combine" somehow two bfs.
 *
 * Dist between locations is manhattan distance.
 * But we might shorten by using the locs.
 * Dist between two locs is min(x0-x1, y0-y1)
 *
 * It is allways optimal to go in a streigt line to the next
 * position. Positions are the x and y of the locs, and 
 * finish position.
 * But still much to much edges.
 *
 * Ok, first nearing is simply walk, manh dist.
 *
 * Then we can try all loc to loc connections with less sum of dist.
 *
 * How to get rid of TLE?
 * We might try only the nearest locs.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);

    vi x(m+2);
    vi y(m+2);

    cin>>x[m]>>y[m];
    cin>>x[m+1]>>y[m+1];

    for(int i=0; i<m; i++)
        cin>>x[i]>>y[i];


    vi dp(m+2, INF);  /* dp[i]=min dist to loc[i] */

    /* dp[m+1]==finish, dp[m]=start */
    dp[m+1]=abs(x[m]-x[m+1])+abs(y[m]-y[m+1]);  /* all simple walk */
    dp[m]=0;

    priority_queue<pii> q;
    q.push({0,m});
    while(q.size()) {
        auto [op,v]=q.top();
        q.pop();
        if(-op!=dp[v])
            continue;

        if(dp[v]>=dp[m+1])
            break;

        for(int i=0; i<m; i++) {
            int dist=dp[v]+min(abs(x[v]-x[i]), abs(y[v]-y[i]));
            if(dist<=dp[m+1] && dist<dp[i]) {
                dp[i]=dist;
                dp[m+1]=min(dp[m+1], dp[i]+abs(x[i]-x[m+1])+abs(y[i]-y[m+1]));
                q.push({-dp[i], i});
            }
        }
    }
    cout<<dp[m+1]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
