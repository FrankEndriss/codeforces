/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int p(int i, int n) {
    int ans=1;
    for(int j=0; ans<=1000000 && j<n; j++) 
        ans*=i;

    return ans;
}

void solve() {
    cini(a);

    for(int i=2; i<20; i++) {
        for(int j=1; j<=a; j++) {
            int pp=p(j,i);
            if(pp==a) {
                cout<<j<<endl;
                return;
            } else if(pp>a) 
                break;
        }
    }

    int ans=sqrt(a);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
