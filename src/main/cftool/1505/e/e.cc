/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* take the nearest berry in sight.
 * if both dirs go horizontal.
 */
void solve() {
    cini(h);
    cini(w);
    cinas(s,h);

    int ans=0;
    pii pos={0,0};
    while(true) {
        //cerr<<"h="<<h<<" w="<<w<<" pos.first="<<pos.first<<" pos.second="<<pos.second<<endl;
        if(pos.first==h || pos.second==w)
            break;
        else if(s[pos.first][pos.second]=='*')
            ans++;

        int hh=1000;
        for(int i=pos.first+1; i<h; i++) {
            if(s[i][pos.second]=='*') {
                hh=i-pos.first;
                break;
            }
        }

        int vv=1000;
        for(int i=pos.second+1; i<w; i++) {
            if(s[pos.first][i]=='*') {
                vv=i-pos.second;
                break;
            }
        }

        if(hh<vv) {
            pos.first++;
        } else if(vv<hh) {
            pos.second++;
        } else if(vv<1000) {    /* same dist, go horz */
            pos.second++;
        } else {    /* none in sight */
            if(pos.first+1<h)
                pos.first++;
            else
                pos.second++;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
