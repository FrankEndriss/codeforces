/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* --just simulate bubble sort and count---
 * No, no need to sort the pairs.
 * just move the right partner to the left.
 **/
void solve() {
    cini(n);
    cinai(a,2*n);

    int ans=0;
    for(int i=1; i<2*n; i+=2) {

        /* find next a[i-1] starting search at i */
        for(int j=i; j<2*n; j++) {
            if(a[j]==a[i-1]) {
                if(j!=i) {
                    rotate(a.begin()+i, a.begin()+j, a.begin()+j+1);
                    ans+=j-i;
                }
                break;
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

