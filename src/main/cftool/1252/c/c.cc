/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it)
{
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args)
{
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve()
{
    cini(n);
    cini(q);
    cinai(r,n);
    cinai(c,n);

    vi rlef(n); // rlef[i]=next elem j<i with other parity
    int odd=-1;
    int even=-1;
    for(int i=0; i<n; i++) {
        if(r[i]&1) {
            rlef[i]=even;
            odd=i;
        } else {
            rlef[i]=odd;
            even=i;
        }
    }

    vi clef(n); // rlef[i]=next elem j<i with other parity
    odd=-1;
    even=-1;
    for(int i=0; i<n; i++) {
        if(c[i]&1) {
            clef[i]=even;
            odd=i;
        } else {
            clef[i]=odd;
            even=i;
        }
    }

    for(int i=0; i<q; i++) {
        cini(ar);
        cini(ac);
        cini(br);
        cini(bc);
        ar--;
        ac--;
        br--;
        bc--;
        int ok=true;
        if(rlef[max(ar,br)]>=min(ar,br))
            ok=false;
        if(clef[max(ac,bc)]>=min(ac,bc))
            ok=false;

        if(ok)
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    }
}

int main()
{
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

