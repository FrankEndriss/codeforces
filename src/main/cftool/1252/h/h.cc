/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(1);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Sol can be one land or two lands.
 * We can savely solve both independent and 
 * use max.
 *
 * Solve one land is sort once with simple comparator.
 *
 * Solve on two land...
 * We need to find the biggest rect fitting into more than one 
 * land.
 *
 * Normalize all land to A<=B
 * Sort by A, biggest first
 * Combine next rect with biggest B seen so far.
 *
 * Note: dont use double, precision loss and overflow!
 */
void solve() {
    cini(n);
    vector<pii> ab(n);
    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        ab[i].first=min(a,b);
        ab[i].second=max(a,b);
    }

    pii pans1=*max_element(all(ab), [&](pii l1, pii l2) {
        return l1.first*l1.second<l2.first*l2.second;
    });
    int ans1=pans1.first*pans1.second;

    int pans2=0;
    sort(all(ab), greater<pii>());
    int maxB=ab[0].second;
    for(int i=1; i<n; i++) {
        pans2=max(pans2, ab[i].first*min(maxB, ab[i].second));
        maxB=max(maxB, ab[i].second);
    }

    string post=".0";
    if(ans1%2==1)
        post=".5";
    ans1/=2;
    if(ans1>=pans2)
        cout<<ans1<<post<<endl;
    else
        cout<<pans2<<".0"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
