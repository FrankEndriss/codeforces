/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * We can make each element==1 by dividing with a bigger element.
 * How to make the biggest==2?
 *
 * Problem is the last one.
 * Consider two last numbers.
 * Go from sqrt to sqrt.
 *
 * Make the last number be sqrt(n-1)
 * Then n-2 -> sqrt(n-1)
 *
 * Consider SQ1= sqrt(n);
 * We can make n==1 in two steps with SQ1.
 * Consider SQ2= sqrt(SQ1);
 * We can make SQ1==1 in two steps with SQ2.
 * And so on.. until SQx==2
 */
void solve() {
    cini(n);

    const vi val={ 1000, 256, 16, 4, 2 };

    vector<pii> ans;
    for(int i : val) {
        if(n<=i)
            continue;
        for(int j=n-1; j>i; j--)
            ans.emplace_back(j, n);
        ans.emplace_back(n, i);
        ans.emplace_back(n, i);
        n=i;
    }

    cout<<ans.size()<<endl;
    for(int i=0; i<ans.size(); i++)  {
        cout<<ans[i].first<<" "<<ans[i].second<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
