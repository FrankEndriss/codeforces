/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* the max triple of all is either
 * r[0], b[0],
 * r[0]+max(b[0],r[1]);
 * b[0]+max(r[0],b[1]);
 * r[i]+r[i+1]+r[i+2];
 * b[i]+b[i+1]+b[i+2];
 * r[i]+b[i+1]+b[j]
 * b[i]+b[i+1]+r[j]
 *
 * ... no, fuck it. it is not triples, it is prefix sums of arbitrary lenght :/
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(r,n);
    cini(m);
    cinai(b,m);

    /*
    int mar1=*max_element(all(r));
    int mab1=*max_element(all(b));

    int mar2=-INF;
    for(int i=1; i<n; i++) 
        mar2=max(mar2, r[i-1]+r[i]);
    int mar3=-INF;
    for(int i=2; i<n; i++) 
        mar2=max(mar2, r[i-2]+r[i-1]+r[i]);

    int mab2=-INF;
    for(int i=1; i<m; i++) 
        mab2=max(mab2, b[i-1]+b[i]);
    int mab3=-INF;
    for(int i=2; i<m; i++) 
        mab2=max(mab2, b[i-2]+b[i-1]+b[i]);

    int ans=max(0LL, max(mar1, mab1));
    ans=max(ans, mar2);
    ans=max(ans, mab2);
    ans=max(ans, mar3);
    ans=max(ans, mab3);
    ans=max(ans, mar1+mab1);
    ans=max(ans, mar1+mab2);
    ans=max(ans, mab1+mar2);
    */

    vi preR(n+1);
    vi preB(m+1);
    for(int i=0; i<n; i++)
        preR[i+1]=preR[i]+r[i];

    for(int i=0; i<m; i++)
        preB[i+1]=preB[i]+b[i];

    int ans=*max_element(all(preR)) + *max_element(all(preB));

    cout<<ans<<endl;



}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
