/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 *    l[i]>=h[i] 
 * && l[i]>=l[i-1]-(k-1)
 * && l[i]<=l[i-1]+(k-1);
 * && l[i]<=h[i]+(k-1)
 *
 * go from left to right and
 * maintain min and max at each position.
 * if(min>max) then no
 * else yes
 *
 * somehow WA :/ I hate these april fools problems.
 * HAHA...you did not observe xy...very funny.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(h,n);

    vi mi(n);
    vi ma(n);
    mi[0]=h[0];
    ma[0]=h[0];

    bool ok=true;
    for(int i=1; i<n; i++) {
        mi[i]=max(h[i], mi[i-1]-(k-1));
        ma[i]=min(h[i]+k-1, ma[i-1]+k-1);

        if(mi[i]>ma[i])
            ok=false;
    }

    if(h.back()<mi[n-1] || h.back()>ma[n-1])
        ok=false;

    if(ok) 
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
