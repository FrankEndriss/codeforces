/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << "=" << a << " ";
	logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cins(s);
    cins(t);

    vector<pii> ans;
    for(int i=0; i<n; i++) {
        if(s[i]==t[i])
            continue;

/* find next available s[i] in s ans swap with that.
 * If none avail then find it in t, and swap that t
 * with last s. Then swap with that.
 */
        bool done=false;
        for(int j=i+1; j<n; j++) {
            if(s[i]==s[j]) {
                done=true;
                ans.emplace_back(j,i);
                swap(s[j], t[i]);
                break;
            } else if(s[i]==t[j]) {
                done=true;
                ans.emplace_back(n-1, j);
                swap(s[n-1], t[j]);
                ans.emplace_back(n-1, i);
                swap(s[n-1], t[i]);
                break;
            }
        }
        if(!done) {
            cout<<"NO"<<endl;
            return;
        }
    }

    if(s[n-1]!=t[n-1]) {
        cout<<"NO"<<endl;
        return;
    }

    cout<<"YES"<<endl;
    cout<<ans.size()<<endl;
    for(pii p : ans)
        cout<<p.first+1<<" "<<p.second+1<<endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

