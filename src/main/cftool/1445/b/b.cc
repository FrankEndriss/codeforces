/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* fuck, what is the fun with such problem statement?
 * do not do that, it is no competitive programming.
 *
 * What about that passport numbers, what does it mean?
 * It is random, or what?
 *
 * However,
 * 1st contest all bgeq a
 * all sum 1st+2nd >=a+b
 *
 * 2nd contest all bgeq c
 * all sum 1st+2nd >=c+d
 *
 * Assume all participants have same score, some are better.
 * ans=max(a+b, c+d);
 *
 */
void solve() {
    cini(a);
    cini(b);
    cini(c);
    cini(d);

    cout<<max(a+b, c+d)<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
