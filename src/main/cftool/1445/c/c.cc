/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, map<int,int> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization[d]++;
            n /= d;
        }
    }
    if (n > 1)
        factorization[n]++;
}

/* Find x, where
 * x is divider of p, 
 * but x is no multiple of q.
 *
 * Simply check all dividers of p.
 * But p up to 1e18.
 *
 * Biggest possible value for x is p.
 * But if q divides p, 
 * next is p/2 etc
 *
 * if q divides p they have some primefactor in common.
 * remove all those from p, then q does not divide p',
 * but p' divides p.
 *
 * q has some factors, and we need to remove some factors from 
 * p such that the number of that factors in p is less than the ones
 * in q.
 * Since we do not know which ones are in p we need to try all that 
 * are in q.
 *
 * If q divides p then all factors of q are in p, too.
 * So x is the biggest possible number created by multiplication
 * of factors in p, which are not in q.
 *
 * So foreach factor find the one where we need to take out 
 * the least number of times.
 *
 *
 * All factors of p are there at least the number of times they
 * are in q, too.
 * So we need to remove one factor that much times that it is less
 * times in p as in q.
 */
void solve() {
    cini(p);
    cini(q);

    if(p%q!=0) {
        cout<<p<<endl;
        return;
    }

    int ans=1;

    map<int,int> f;
    trial_division4(q, f);

    for(auto ent : f) {
        int cnt=0;
        int pp=p;
        while(pp%ent.first==0) {
            pp/=ent.first;
            cnt++;
        }
        int div=1;
        while(cnt>=ent.second) {
            cnt--;
            div*=ent.first;
        }
        ans=max(ans, p/div);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
