/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=998244353;


int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int nom, const int denom) {
    return mul(nom, inv(denom));
}
int gaus(int i) {
    return (i*(i+1)/2)%MOD;
}

int gausgaus(int i) {
    int ans=gaus(i);
    if(i>1)
        ans=(ans+gausgaus(i-1))%MOD;

    return ans;
}


const int N=3e5+7;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

/* How much partitions are there?
 * It seems n!/((n/2)!)*((n/2)!)
 *
 * Or 2^n ???
 * Or 2^(n/2) ???
 *
 * It is nCr(2*n, n)
 * Let number of partitions np(n) = nCr(2*n,n)
 *
 * Say we choose a[0] to be in first partition.
 * and a[1] in second.
 * Then 
 * abs(a[0]-a[1]) contributes np(n-1)*2 times.
 * abs(a[0]-a[2]) contributes np(n-1)*2 times.
 * ...
 * abs(a[1]-a[2]) np(n-1)...
 *
 * etc, all ordered pairs contribute np(n-1)*2 times.
 *
 * -> There is something wrong.
 *  we need to use inclusion/exclusion somehow :/
 *
 * How to find sum of all ordered pairs?
 * Sort and use prefix sum.
 */
void solve() {
    cini(n);
    cinai(a,2*n);

    sort(all(a));
    int asum=accumulate(all(a), 0LL);
    int ans=0;

    for(int i=0; i<2*n; i++) {
        asum-=a[i];
        int m=mul(a[i], 2*n-1-i);
        int s=pl(asum, -m);
        ans=pl(ans, s);
    }
    assert(asum==0);

    ans=mul(ans, nCr(2*n-2,n-1));
    ans=mul(ans, 2);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
