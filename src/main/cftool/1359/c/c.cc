/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* assume 
 * h-=c
 * t-=c
 * c==0
 *
 * 1 h
 * 3 2h/3
 * 5 3h/5
 * 7 4h/7
 * 9 5h/9
 *
 * binary search
 */
void solve() {
    cini(h);
    cini(c);
    cini(t);
    h-=c;
    t-=c;

    if(t>=h) {
        cout<<1<<endl;
        return;
    } else if(t*2<=h) {
        cout<<2<<endl;
        return;
    }

/* let val(l)=
 * find highest l still more than t.
 **/
    int l=1;
    int r=1e9;

    while(l+1<r) {
        int mid=(l+r)/2;    /* use midth odd number */
        int denom=mid*2-1;
        int nom=denom/2+1;
//cerr<<nom<<"/"<<denom<< " h*nom="<<h*nom<<" t*denom="<<t*denom<<" diff="<<nom*h-t*denom<<endl;
        if(nom*h>=t*denom)
            l=mid;
        else
            r=mid;
    }

/* ans = l or r */
    int denomL=(l*2-1);
    int denomR=(r*2-1);
    int nomL=denomL/2+1;
    int nomR=denomR/2+1;

    int diffL=abs(nomL*h-t*denomL);
    int diffR=abs(nomR*h-t*denomR);

//cerr<<"L="<<l<<" r="<<r<<endl;
//cerr<<"diffL="<<diffL<<" diffR="<<diffR<<endl;

    if(diffL*denomR<=diffR*denomL)
        cout<<denomL<<endl;
    else
        cout<<denomR<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
