
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* SCC strongly connected components
 * see https://www.geeksforgeeks.org/strongly-connected-components/
 */
void DFSUtil(int v, vb& vis, vvi& adj, vi& ans) {
    vis[v] = true;
    ans.push_back(v);

    for(int i : adj[v])
        if(!vis[i])
            DFSUtil(i, vis, adj, ans);
}

void fillOrder(int v, vb& vis, stack<int> &Stack, vvi& adj) {
    vis[v] = true;

    for(int i : adj[v])
        if(!vis[i])
            fillOrder(i, vis, Stack, adj);

    Stack.push(v);
}

/* The main function that finds returns all strongly connected components */
vvi printSCCs(vvi& adj) {
    stack<int> Stack;
    vb vis(adj.size());

    /* Fill vertices in stack according to their finishing times */
    for(size_t i = 0; i<adj.size(); i++)
        if(!vis[i])
            fillOrder(i, vis, Stack, adj);

    /* Create a reversed graph */
    vvi adjT(adj.size());
    for (size_t v=0; v<adj.size(); v++) {
        for(int i : adj[v])
            adjT[i].push_back(v);
    }

    vis.assign(vis.size(), false);

    vvi ans;
    while(Stack.size()) {
        int v = Stack.top();
        Stack.pop();

        if (!vis[v]) {
            ans.resize(ans.size()+1);
            DFSUtil(v, vis, adjT, ans.back());
        }
    }
    return ans;
}

/* Consider doing a bfs starting at root=0
 * Foreach vertex we need to find a state, that is
 * INF if any circle was in the path leading to that vertex.
 * 1 if visited the first time
 * 2 if visited the second time
 *
 * So, the state allways increases, and we need to push vertex onto
 * the queue only if it increases.
 *
 * How to detect circle?
 * Separatly find all SCC.
 */
void solve() {
    cini(n);
    cini(m);

    vb circ(n); /* vertex of circles members */
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        if(a!=b)
            adj[a].push_back(b);
        else
            circ[a]=true;
    }


    vvi cc=printSCCs(adj);
    for(size_t i=0; i<cc.size(); i++) {
        if(cc[i].size()>1) {
            for(size_t j=0; j<cc[i].size(); j++)
                circ[cc[i][j]]=true;
        }
    }


    queue<int> q;
    vb vis(n);
    for(int i=0; i<n; i++) 
        if(circ[i]) {
            q.push(i);
            vis[i]=true;
        }

    /* mark all circle members and vertex reacheable from them */
    while(q.size()) {
        int v=q.front();
        q.pop();
        for(int chl : adj[v]) {
            if(!vis[chl]) {
                circ[chl]=true;
                vis[chl]=true;
                q.push(chl);
            }
        }
    }

    vi dp(n);
    q.push(0);
    if(circ[0])
        dp[0]=-1;

    if(dp[0]==0)
        dp[0]=1;

    while(q.size()) {
        int v=q.front();
        q.pop();
        for(int chl : adj[v]) {
            if(dp[chl]==0) {
                if(circ[chl] || dp[v]==-1)
                    dp[chl]=-1;
                else
                    dp[chl]=1;

                q.push(chl);
            } else if(dp[chl]==1) {
                if(dp[v]==-1) {
                    dp[chl]=-1;
                } else {
                    dp[chl]=2;
                }
                q.push(chl);
            } else if(dp[chl]==2) {
                if(dp[v]==-1) {
                    dp[chl]=-1;
                    q.push(chl);
                }
            } else if(dp[chl]==-1) {
                ; //ignore
            }
        }
    }

    for(int i=0; i<n; i++) {
        cout<<dp[i]<<" ";
    }
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
