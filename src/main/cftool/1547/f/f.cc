/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, vector<ll> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        if(n % d == 0) {
            factorization.push_back(d);
            while (n % d == 0)
                n /= d;
        }
    }
    if (n > 1)
        factorization.push_back(n);
}

/*  The final value of all elements is gcd(a[])
 *  If we got  a subarray with same primefactor,
 *  then we need the len of that subarray steps to 
 *  remove them all.
 *  so ans=len of longest such subarray
 */
void solve() {
    cini(n);
    cinai(a,n);

    int g=a[0];
    for(int i=1; i<n; i++) 
        g=gcd(g, a[i]);

    for(int i=0; i<n; i++) 
        a[i]/=g;

    int ans=0;
    vvi p(n);
    map<int,int> s;
    for(int i=0; i<=2*n; i++) {
        if(i<n)
            trial_division4(a[i], p[i]);

        if(i==0) {
            for(int pp : p[i])
                s[pp]=1;
        } else {
            map<int,int> s0;
            for(int pp : p[i%n]) {
                auto it=s.find(pp);
                if(it!=s.end()) {
                    s0[pp]=it->second+1;
                    ans=max(ans, it->second+1);
                } else {
                    s0[pp]=1;
                    ans=max(ans, 1LL);
                }
            }
            s.swap(s0);
        }
    }

    ans=min(ans, n);
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
