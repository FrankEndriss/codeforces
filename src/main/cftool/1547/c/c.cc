/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(k);
    cini(n);
    cini(m);
    cinai(a,n);
    cinai(b,m);

    int i1=0;
    int i2=0;
    int cnt=k;
    vi ans;
    for(int i=0; i<n+m; i++) {
        if(i1<n && a[i1]==0) {
            cnt++;
            i1++;
            ans.push_back(0);
            continue;
        }
        if(i2<m && b[i2]==0) {
            cnt++;
            i2++;
            ans.push_back(0);
            continue;
        }

        if(i1<n && a[i1]<=cnt) {
            ans.push_back(a[i1]);
            i1++;
        } else if(i2<m && b[i2]<=cnt) {
            ans.push_back(b[i2]);
            i2++;
        } else {
            cout<<-1<<endl;
            return;
        }
    }

    for(int i : ans) 
        cout<<i<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
