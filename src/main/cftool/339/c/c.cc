/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * DFS
 * current state is min weight, last used weight
 * so only 11x11 vertices.
 * Find a path of length m using dfs.
 */
void solve() {
    cins(s);
    cini(m);

    struct vertex {
        vertex(int b, int p):bal(b),prev(p) {
        };

        int bal;
        int prev;
    };

    vector<vector<vector<vertex>>> adj(11, vector<vector<vertex>>(11));

    for(int b=0; b<10; b++) {
        for(int p=0; p<=10;  p++) {
            for(int j=0; j<s.size(); j++) {
                if(s[j]=='1' && b<j+1 && p!=j+1) {
                    adj[b][p].push_back(vertex(j+1-b,j+1));
                }
            }
        }
    }

    vi ans;
    function<bool(vertex,int)> dfs=[&](vertex v, int d) {
        if(d==0)
            return true;

        for(vertex chl : adj[v.bal][v.prev]) {
            ans.push_back(chl.prev);

            if(dfs(chl, d-1))
                return true;

            ans.pop_back();
        }
        return false;
    };

    if(dfs(vertex(0,0), m)) {
        cout<<"YES"<<endl;
        for(int i : ans)
            cout<<i<<" ";
        cout<<endl;
    } else
        cout<<"NO"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
