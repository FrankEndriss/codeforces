/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int nom, const int denom) {
    return mul(nom, inv(denom));
}

/* We need to find sum(r)-sum(l-1)
 * To find sum(x) we need to know how much elements from each 
 * set where written.
 * So we find number of full blocks, of both sets.
 * Sum the numbers from each set up to the nth block.
 * 1+4+16+64+...
 * 2+8+32+...
 * Then add the number of numbers from last block.
 *
 * Then build sum of both number of numbers.
 * odd: n*n
 * even: n*(n+1)
 */

void solve() {
    cini(l);
    cini(r);

    function<int(int)> sum=[](int k) {
        vi cnt(2);
        int cntset=1;   // odd==1, even==0
        int c=1;
        while(k) {
            cnt[cntset]=pl(cnt[cntset], min(k,c));
            k-=min(k,c);
            cntset=!cntset;
            c*=2;
        }

        return pl(mul(cnt[1], cnt[1]), mul(cnt[0], cnt[0]+1));
    };

    int ans=pl(sum(r), -sum(l-1));
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
