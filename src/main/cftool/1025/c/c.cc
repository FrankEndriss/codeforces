/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * The operation reverse and shifts the sequence.
 * Since reversing does not change anything, it
 * just shifts.
 * So we use the longest existing counting segment,
 * or try to build one by shifting. Ie put the sequence
 * twice into one, then count.
 */

void solve() {
    cins(s);
    const int n=(int)s.size();

    int ans=1;
    int cnt=1;
    for(int i=1; i<n*2; i++) {
        if(s[i%n]!=s[(i-1)%n]) {
            cnt++;
            ans=max(ans, cnt);
        } else 
            cnt=1;
    }
    ans=min(ans, n);
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
