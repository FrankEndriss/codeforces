/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* facs
 * 1 = 1
 * 2 = 2
 * 3 = 6
 * 4 = 24 = 3! * 2! *2!
 * 5 = X
 * 6 = 720 = 5! * 6 = 5! * 3!
 * 7 = X
 * 8 = 7! * 8 = 7! * 2! * 2! * 2!
 * 9 = 7! * 8 * 9 = 7! * 2 * 36 = 7! * 2! * 3! *3!
 */
void solve() {
    cini(n);
    cins(s);
    string ans;
    for(char c : s) {
        if(c=='4')
            ans+="322";
        else if(c=='6')
            ans+="53";
        else if(c=='8')
            ans+="7222";
        else if(c=='9')
            ans+="7332";
        else if(c!='0' && c!='1')
            ans+=c;
    }
    sort(ans.begin(), ans.end(), greater<char>());
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

