/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(d);

    if(n==1) {
        if(d==0) {
            cout<<"YES"<<endl;
            cout<<endl;
        } else {
            cout<<"NO"<<endl;
        }
        return;
    }

    ll ma=((n-1)*n)/2;
    int nn=n;
    ll mi=0;
    int lvl=0;
    while(nn>0) {
        mi+=min(nn, (1<<lvl))*lvl;
        nn-=(1<<lvl);
        lvl++;
    }

//cout<<"n="<<n<<" mi="<<mi<<" ma="<<ma;
    if(d<mi || d>ma) {
        cout<<"NO"<<endl;
        return;
    }

    /* how to construct the tree?
     * we can start at streight list, then remove from back.
     * or start with min hight tree then move nodes to deeper levels.
     *
     * Lets try start with streigt list.
     */

    vvi ans(n); // ans[i]==list of nodes on level i
    for(int i=0; i<n; i++)
        ans[i].push_back(i);

    int cnt=ma;
    int currLvl=1;    // current fill level

    for(int node=n-1; cnt>d && node>0; node--) {
        int maLvl=1<<currLvl;   // max num of nodes on currLvl
        int madiff=node-currLvl;
#ifdef DEBUG
cout<<"node="<<node<<" maLvl="<<maLvl<<" cnt="<<cnt<<" d="<<d<<" madiff="<<madiff<<endl;
#endif

        if(cnt-madiff>=d) {
            ans[currLvl].push_back(node);
            ans[node].clear();
            cnt-=madiff;
            if(ans[currLvl].size()==maLvl)
                currLvl++;
        } else {
#ifdef DEBUG
cout<<"last node="<<node<<endl;
#endif
            ans[node-(cnt-d)].push_back(node);
            ans[node].clear();
            cnt=d;
        }
        if(cnt==d)
            break;
    }

    vi p(n);
    for(int i=1; i<n; i++) {
#ifdef DEBUG
cout<<"level="<<i<<endl;
#endif
        if(ans[i].size()==0)
            break;

        for(int j=0; j<ans[i].size(); j++) {
#ifdef DEBUG
cout<<"i="<<i<<" j="<<j<<" ans[i][j]="<<ans[i][j]<<endl;
#endif
            p[ans[i][j]]=ans[i-1][j/2];
        }
    }

    cout<<"YES"<<endl;
    for(int i=1; i<n; i++)
        cout<<p[i]+1<<" ";
    cout<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

