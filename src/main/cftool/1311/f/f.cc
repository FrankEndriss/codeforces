/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

template <typename E>
class SegmentTree {
  private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

  public:
    /** SegmentTree. Note that you can hold your data in your own storage and give
     * an Array of indices to a SegmentTree.
     * @param beg, end The initial data.
     * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
     * @param pCumul The cumulative function to create the "sum" of two nodes.
    **/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=(int)distance(beg, end);
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Updates all elements in interval(idxL, idxR].
    * iE add 42 to all elements from 4 to inclusive 7:
    * update(4, 8, [](int i1) { return i1+42; });
    */
    void updateRange(int pIdxL, int pIdxR, function<E (E)> apply) {
        pIdxL+=treeDataOffset;
        pIdxR+=treeDataOffset;
        for(int i=pIdxL; i<pIdxR; i++)
            tree[i]=apply(tree[i]);

        while (pIdxL != ROOT) {
            pIdxL=parent(pIdxL);
            pIdxR=max(pIdxL, parent(pIdxR-1));
            for(int i=pIdxL; i<=pIdxR; i++)
                tree[i]=cumul(tree[leftChild(i)], tree[rightChild(i)]);
        }
    }

    /** Updates the data at dataIdx to value returned by apply. */
    void updateApply(int dataIdx, function<E(E)> apply) {
        updateSet(dataIdx, apply(get(dataIdx)));
    }

    /** Updates the data at dataIdx by setting it to value. */
    void updateSet(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return value at single position, O(1) */
    E get(int idx) {
        return tree[idx+treeDataOffset];
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

/* Per pair there are two cases:
 * 1. The initial distance is the smallest distance ever.
 * 2. The initial distanc shrinks to zero and then increases forever.
 * Note that if the distance does not change at all, it is a special case of 1,
 * and ignorable at all since it does not contribute at all.
 *
 * Let sum1(t) be the rate with wich the distance increasing points increase
 * the distance at time t.
 * Let sum2(t) be the rate with wich the distance decreasing points decrease
 * the distance at time t.
 * Since sum1 in decreasing, and sum2 in increasing, at some t these are same.
 * At this t the min distance sum of all is met.
 *
 * Binary search that point of time.
 * How to sum up pairs?
 * -> simply calc position of point for every point at time t, O(n)
 *
 * But the function is not montonic, we need to use ternary search.
 * **************
 * Got the problem completly wrong.
 * We do _not_ search the min sum at some specific t, but we search 
 * the minimum dist of every pair at any t.
 *
 * So per pair this value is the initial distance, or zero.
 *
 * Sort points by x, then from left to right add the distances of all points
 * with v lteq than current point.
 * lteq since these are the points which will increase the distance 
 * forever.
 * Use SegmentTree to sum the points.
 *
 * Add the x value of every point seen at its v-position.
 *
 * Find for every point the min v-position which contributes, 
 * (by upper_bound)
 * then get cnt(x) and sum(x) from the segtree.
 */
const int INF=1e9;
void solve() {
    cini(n);
    vector<pii> xv(n);
    vector<pii> vx(n);
    for(int i=0; i<n; i++) {
        cin>>xv[i].first;
        vx[i].second=xv[i].first;
    }
    for(int i=0; i<n; i++) {
        cin>>xv[i].second;
        vx[i].first=xv[i].second;
    }

    sort(all(xv));
    sort(all(vx));

    pii neu;
    vector<pii> data(n);
    SegmentTree<pii> seg(all(data), neu, [](pii i1, pii i2) {
        return make_pair( i1.first+i2.first, i1.second+i2.second );
    });

    int ans=0;
    for(int i=0; i<n; i++) {
        auto it=lower_bound(all(vx), make_pair(xv[i].second, INF));
        
        pii sum=seg.get(0, distance(vx.begin(), it));
        ans+=sum.first*xv[i].first-sum.second;

        it=lower_bound(all(vx), make_pair(xv[i].second, xv[i].first));
        seg.updateSet(distance(vx.begin(), it), { 1LL, xv[i].first});
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
