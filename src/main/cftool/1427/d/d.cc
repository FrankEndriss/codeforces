/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Most likely we want to let sorted blocks as is.
 * So let be  s[i] = ith sorted consecutive block which we never breakup.
 * Then we want to move first block to first position (if it is not allready there)
 * We can do this by splitting the deck in two at position just before that block.
 *
 * So, what if first block is in first position. 
 * Then we break in three blocks. The first one, and the rest that we want to change the order, 
 * ie just before second block. Note that second block cannot begin just after first block.
 * Then again, we do a swap with two blocks, breaking just before the first block.
 * This will sort the deck in at most n*2 operations.
 *
 * Since n is small we can bruteforce this.
 *
 * ***
 * But we need to consider that _at most n operations_ !
 * So, still we do not split the blocks.
 * We must somehow fix one pair with each operation.
 *
 * if a[0]!=1 we can split just after pos[a[0]-1], so after that operation
 * the elements pos[a[0]-1] and pos[a[0]] are adjacent and consecutive.
 *
 * What if a[0]==1, ie firstblock is at first position?
 * Whatever we do, firstblock will be somewhere else afterwards.
 *
 * ******
 * for odd n we need to put the middle element correct, else
 * one of the both middle elements.
 * The, we all ways let the middle part in once piece.
 * There are allways two elements we could fix.
 * That is the one left of the middle part, or the one right of the middle part.
 * 4 cases.
 * The left and right elements are right of the mp.
 * -> split the right part before the next right element and the left part
 *  ...
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    int fbsize=1; /* size of first block */
    int fbpos=-1;
    while(fbsize<52) {
        /* search position and len of firstblock */
        for(int i=0; i<n; i++) {
            if(a[i]==1) {
                fbpos=i;
                fbsize=1;
                while(i+fbsize<n && a[i+fbsize]==a[i+fbsize-1]+1) {
                    fbsize++;
                }
                break;
            }
        }
        if(fbsize==n)
            break;

        int fsecond=fbsize+1;   /* first element of second block */
        int fsecpos=-1;
        for(int i=0; i<n; i++) {
            if(a[i]==fsecond) {
                fsecpos=i;
                break;
            }
        }
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
