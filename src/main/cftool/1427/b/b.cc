/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* simple fraction implementation based on long long */
struct fract {
    ll f, s;

    fract(ll counter, ll denominator) :
        f(counter), s(denominator) {
        norm();
    }

    bool operator<(const fract &rhs) {
        return f * rhs.s < s * rhs.f;
    }

    fract& operator+=(const fract &rhs) {
        ll g = __gcd(this->s, rhs.s);
        ll lcm = this->s * (rhs.s / g);
        this->f = this->f * (lcm / this->s) + rhs.f * (lcm / rhs.s);
        this->s = lcm;
        norm();
        return *this;
    }

    friend fract operator+(fract lhs, const fract &rhs) {
        lhs += rhs;
        return lhs;
    }

    friend fract operator-(fract lhs, const fract &rhs) {
        lhs += fract(-rhs.f, rhs.s);
        return lhs;
    }

    fract& operator*=(const fract &rhs) {
        this->f *= rhs.f;
        this->s *= rhs.s;
        norm();
        return *this;
    }

    friend fract operator*(fract lhs, const fract &rhs) {
        lhs *= rhs;
        return lhs;
    }

    friend fract operator/(fract lhs, const fract &rhs) {
        return lhs * fract(rhs.s, rhs.f);
    }

    void norm() {
        int sign = ((this->f < 0) + (this->s < 0)) % 2;
        sign = -sign;
        if (sign == 0)
            sign = 1;
        ll g = __gcd(abs(this->f), abs(this->s));
        if (g != 0) {
            this->f = (abs(this->f) * sign) / g;
            this->s = abs(this->s) / g;
        }
    }
};

/* greedy.
 * We get additional points by closing gaps between WLLLW
 * First L gets 2 additional points, last 3 additional points.
 * So we want to close smaller gaps first.
 * Leftest and rightest gaps are special, because they count
 * only 12...23 points, and 22...2. So each one less.
 *
 * What about if we cannot close the whole gap?
 * Choose any bigger gap, but not the first.
 *
 * first and last gap gain cnt*2 points,
 * all others cnt*2+1
 *
 * So first close all gaps in, then the first and lasts.
 * **** 
 * it does not work like that, because a lot of the wins
 * are worth 2 points before, so we cannot increase them.
 * Only first W after L can be increased, and only once.
 *
 * So with one move we get 
 * 3 points if after a W and before a WL, else
 * 2 points if after a W or before a W
 * 1 point else, but that never happens.
 *
 * So first phase fill all WLW... for 3 points each
 * wLLW 5 points..
 * WLLLW 7
 * then place next to W for each 2 points.
 *
 * TODO
 * edgecase all "LLL...LLLL"
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);

    vi gap;
    int prev=-1;
    int cntL=0;
    int ans=0;
    for(int i=0; i<n; i++) {
        if(s[i]=='W') {
            ans++;
            if(i>0 && s[i-1]=='W')
                ans++;

            int cnt=i-prev-1;
            if(prev>=0 && cnt>0) {
                gap.push_back(cnt);
            }
            prev=i;
        } else
            cntL++;
    }

    if(cntL==n) {
        cout<<max(0LL, min(k,n)*2-1)<<endl;
        return;
    }

    sort(all(gap));

    for(size_t i=0; k>0 && i<gap.size(); i++) {
        if(gap[i]<=k) {
            ans+=(2*gap[i]+1);
            k-=gap[i];
            cntL-=gap[i];
        }
    }
    ans+=min(k,cntL)*2;

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--) 
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
