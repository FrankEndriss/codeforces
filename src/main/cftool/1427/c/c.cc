/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* Its a dp.
 * Foreach celeb we can choose to take a photo or do not.
 * So we move to that position and increase or not.
 *
 * At any point of time t we are at some position i,j (N=500)
 * having taken x photos.
 *
 * Foreach next celeb we want to move from the position with 
 * biggest x which can reach current position (i,j), so we update
 * current position with x+1.
 *
 * How to maintain x values for t-positions, and filter/sort reachable
 * positions?
 *
 * Build graph, find longest path?
 * Note that foreach celeb we need to single check at most 500 positions.
 *
 * ... Use lazy segment tree.
 * where seg[i]=max photos taken if we take photo of celeb i
 * ... turns out that timeouts.
 *
 * How can we update the dp in O(N)?
 * Ok, we can use kind of prefix sum instead of segment tree.
 * ... not really... how does this work? How to implement that range update?
 *
 *
 * ... fuck this shit :/
 * -> Ok, sol is fairly simple.
 *  We do not update all following celebs, but we do update the current
 *  celebs from the previous celebs, which are at most n*2, simply brute
 *  force max of them.
 */
constexpr int INF=1e9;
void solve() {
    cini(r);
    cini(n);
    const int k=r*2;

    vvi cel(3,vi(n+1));
    cel[0][0]=0;
    cel[1][0]=1;
    cel[2][0]=1;

    for(int i=1; i<=n; i++) {
        cin>>cel[0][i]>>cel[1][i]>>cel[2][i];
    }
    
    vi dp(n+1, -INF);    /* dp[i] = max possible photos at celeb i */
    dp[0]=0;
    int ma=-INF;
    int ans=0;

    for(int i=0; i<n+1; i++) {

        const int t0=cel[0][i];
        dp[i]=max(dp[i], ma+1);

        for(int j=max(0, i-k); j<i; j++) {
            int dist=abs(cel[1][i]-cel[1][j]) + abs(cel[2][i] - cel[2][j]);
            int tdiff=abs(cel[0][j] - t0);
            //cerr<<"i="<<i<<" j="<<j<<" dist="<<dist<<" tdiff="<<tdiff<<endl;
            
            if(tdiff>=dist)
                dp[i]=max(dp[i], dp[j]+1);
        }
        if(i-k>=0)
            ma=max(ma, dp[i-k]);

        ans=max(ans, dp[i]);
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
