/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * nonzero, not posititve!
 */
void solve() {
    cini(n);
    cinai(a,n);
    int sumneg=0;
    int sumpos=0;
    for(int i=0; i<n; i++) {
        if(a[i]<0)
            sumneg-=a[i];
        else
            sumpos+=a[i];
    }

    if(sumneg==sumpos) {
        cout<<"NO"<<endl;
        return;
    }

    if(sumneg>sumpos)
        sort(all(a));
    else
        sort(all(a), greater<int>());

    cout<<"YES"<<endl;
    for(int i : a)
        cout<<i<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
