/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int N=1e6+7;
vi xpre(N);

/* @return (i%1) ^ (i%2) ^ ... (i%n)
 */
int xtimes(int i, int n) {
    
}
/* 
 * Obs:
 * (i mod 1) ^ (i mod 2) ^ ... is there n times
 * so if(n%2==0) then that part is zero.
 * if(n%2==1) then...
 * sic :/
 *
 * The other part is p[0] ^ p[1] ^ ... ^ p[n]
 */
void solve() {
    cini(n);

    for(int i=1; i<=n; i++) {
        xpre[i]=xpre[i-1]^i;
    }

    int ans=0;
    int imod=0;
    for(int i=1; i<=n; i++) {
        cini(p);
        imod=imod ^ (i%
        ans^=p;
    }
    if(n&1) 
        ans^=imod;

    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
