/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9+7;
void solve() {
    cini(n);
    cini(m);
    cinai(a,n+m);
    cinai(b,n+m);

    vi p;
    vi t;
    t.push_back(-INF);
    for(int i=0; i<n+m; i++) {
        if(b[i])
            t.push_back(a[i]);
        else
            p.push_back(a[i]);
    }
    t.push_back(INF);

    vi ans(t.size());
    for(size_t i=0; i<p.size(); i++) {
        auto it=upper_bound(t.begin(), t.end(), p[i]);
        int idx=distance(t.begin(), it);
        if(t[idx]==INF) {
            idx--;
        } else if(t[idx]-p[i]>=p[i]-t[idx-1]) {
            idx--;
        }
        ans[idx]++;
    }

    for(size_t i=1; i+1<ans.size(); i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

