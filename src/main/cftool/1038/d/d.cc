/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * consider last two slimes,
 * one is added, one is subtracted
 * ...
 * all others are added, too.
 *
 * How to find greedy order of elements.
 */
void solve() {
    cini(n);
    int sum=0;
    int mi=1e9;
    int hasNeg=false;
    int hasPos=false;
    int first;
    for(int i=0; i<n; i++) {
        cini(aux);
        if(i==0)
            first=aux;

        if(aux<0)
            hasNeg=true;
        if(aux>0)
            hasPos=true;
        sum+=abs(aux);
        mi=min(mi, abs(aux));
    }

    if(n==1) {
        cout<<first<<endl;
        return;
    }

    int ans=sum;
    if(!hasNeg || !hasPos)
        ans-=2*mi;
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
