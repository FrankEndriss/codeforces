
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * In positions where d[i]==0 we must use a[i] or b[i]
 * Also there can be positions where a[i]==b[i]
 *
 * When going from left to right, we have positions where
 * we can use both a[i] and b[i].
 * So ans*=2
 * But of course, using some a[i] blocks that value at the
 * position where b[j]=a[i].
 * So it is that we can use the value a[i]==b[j] at position i xor j,
 * if using a[i] we cannot use b[i], so must use a[k]==b[i]
 * Consider b[i]
 *   if a[i] is used, the value corresponding to b[i] at a[k] must be used, since
 *   b[i] cannot be used.
 *   So what, some circle...somehow?
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);
    cinai(d,n);


    vb vis(n);
    for(int i=0; i<n; i++) {
        if(d[i]>0 || a[i]==b[i]) 
            vis[i]=true;
    }

    mint ans;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
