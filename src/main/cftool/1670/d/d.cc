
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider that only that trias are ok that have
 * one vertex in the center of a hexagon.
 * Not sure if this matches "equilateral".
 *
 * We want to create as much trias as possible with 
 * each line.
 * So we add each non parallel lines so that the 
 * cut each other at the center of a hex.
 * The first two lines meeting so create 2 trias.
 * The third another 4 trias.
 *
 * We have 3 directions of lines. In each direction,
 * a line crosses all lines of other directions.
 * We add lines round robin directions.
 *
 * 1: 0
 * 2: 2
 * 3: 4
 * 4: 4 (cuts two other lines, so creates 4 tris)
 * ...
 */
set<pii> dp;
void ini() {
    int ans=0;
    vi cnt(3);
    cnt[0]=1;
    cnt[1]=1;
    ans=2;
    int n=2;
    dp.emplace(n,2LL);
    int idx=2;
    const int N=1e9;
    while(n<=N) {
        int sum=0;
        for(int j=0; j<3; j++) 
            if(j!=idx)
                sum+=cnt[j];

        ans++;
        n+=sum*2;
        dp.emplace(n,ans);
        cnt[idx]++;
        idx=(idx+1)%3;
    }
}

void solve() {
    cini(n);
    if(n==1) {
        cout<<2<<endl;
        return;
    }
    auto it=dp.lower_bound({n,-1});
    cout<<it->second<<endl;
}

signed main() {
    ini();
    cini(t);
    while(t--)
        solve();
}
