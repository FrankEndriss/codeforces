
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Obviously all negative numbers must be left.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int r=n-1;
    int l=0;
    while(l<r) {
        while(l<r && a[l]<=0)
            l++;
        while(r>l && a[r]>=0)
            r--;
        if(l<r) {
            a[l]=-a[l];
            a[r]=-a[r];
            l++;
            r--;
        }
    }
    vi b=a;
    sort(all(b));
    if(a==b)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
