
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It deletes Chars until there are no more chars
 * before the first special char.
 * So the number of dels is the number of chars before
 * the first special char.
 * Or does the statement says something else?
 * How to prove that I understood the statement?
 *
 * No...
 * Each char is eaten by the next special, at step "dist to next".
 */
void solve() {
    cini(n);
    cins(s);
    cini(k);
    set<char> a;
    for(int i=0; i<k; i++) {
        cins(ss);
        a.insert(ss[0]);
    }

    int pr=0;
    int ans=0;
    for(int i=s.size()-1; i>=0; i--) {
        ans=max(ans, pr-i);

        if(a.count(s[i])) {
            pr=i;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
