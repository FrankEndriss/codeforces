/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* For each pair we must use 0, 1 or 2 changes.
 * For very small and very big k 2 must change.
 * For exact mach 0 must change.
 * Else 1 must change.
 *
 *
 * small pair (1,1)
 * big pair (k,k)
 * x==k+1 -> both can be equalized by one operation
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vi evt(2*k+2); /* evt[i]=sum events at index i */

    for(int i=0; i<n/2; i++) {
        int mi=min(a[i], a[n-1-i]);
        int ma=max(a[i], a[n-1-i]);
        evt[1]+=2;
        evt[mi+1]-=1;
        evt[mi+ma]-=1;
        evt[mi+ma+1]+=1;
        evt[ma+k+1]+=1;
    }
    
    int ans=1e18;
    int sum=0;
    for(int i=1; i<evt.size(); i++) {
        sum+=evt[i];
        ans=min(ans, sum);
    }

    cout<<ans<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

