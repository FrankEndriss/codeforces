/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s)
        c = c == ','?  ' ': c;
    stringstream ss;
    ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) {
    cerr << endl;
}
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0)
        cerr << ", ";
    stringstream ss;
    ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we need to find the shortest path from a to b,
 * and from all nodes of that path the nearest to c.
 * Count edges, and use that much smallest numbers
 * from p.
 */
void solve() {
    cini(n);
    cini(m);
    cini(a);
    cini(b);
    cini(c);
    cinai(p,m);
    vvi adj(n);
    a--;
    b--;
    c--;

    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

//cout<<"parse"<<endl;

    const int INF=1e18;
    vi dp(n,INF);
    dp[a]=0;
    queue<int> q;
    q.push(a);
    while(q.size()) {
        int node=q.front();
        q.pop();
        for(int chl : adj[node]) {
            if(dp[chl]>dp[node]+1) {
                dp[chl]=dp[node]+1;
                q.push(chl);
            }
        }
    }
//cout<<"l1"<<endl;

    vi dp2(n,INF);
    dp2[b]=0;
    q.push(b);
    while(q.size()) {
        int node=q.front();
        q.pop();
        for(int chl : adj[node]) {
            if(dp2[chl]>dp2[node]+1) {
                dp2[chl]=dp2[node]+1;
                q.push(chl);
            }
        }
    }
//cout<<"l2"<<endl;

    vi dp3(n,INF);
    dp3[c]=0;
    q.push(c);
    while(q.size()) {
        int node=q.front();
        q.pop();
        for(int chl : adj[node]) {
            if(dp3[chl]>dp3[node]+1) {
                dp3[chl]=dp3[node]+1;
                q.push(chl);
            }
        }
    }
//cout<<"l3"<<endl;

    sort(p.begin(), p.end());

    vi pp(m+1);
    for(int i=1; i<=m; i++) {
        pp[i]=pp[i-1]+p[i-1];
    }
//cout<<"l4"<<endl;

    int ans=1e18;
    for(int i=0; i<n; i++) {
        int x=dp[i]+dp2[i]+dp3[i];
        if(x<=m)
            ans=min(ans, pp[dp[i]+dp2[i]+dp3[i]] + pp[dp2[i]]);
    }
//cout<<"ans"<<endl;

    cout<<ans<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

