/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The '+' and '1' defintion does not help at all.
 * Why not just delete that sentences?
 *
 * How to find the uniquness?
 * Count how many possible, then check if more than one?
 * How to count possibilities?
 *
 * Ex:
 * "(??)"
 *
 * Consider we have to set on of each kind, how can we tell
 * if the positions are fixed or not?
 *
 * Observe that if there is a RBS we can construct it by subt
 * the first some ? by '(', and the last some by ')'.
 *  Then, try to swap the last subtituted '(' by ')', replace
 *  the next right of it by '('.
 *  If this results in RBS there is more than one possible substitution.
 *
 *  Last example, why ans=NO?
 *  ?(?)()?)
 *  (())()))    No other subst possible :/
 */
void solve() {
    cins(s);
    const int n=(int)s.size();

    string t=s;

    vi f(2);
    vi pos;
    for(size_t i=0; i<s.size(); i++)  {
        if(s[i]=='?')
            pos.push_back(i);
        else
            f[s[i]==')']++;
    }

    int left=-1;
    for(int i=0; f[0]+i<n/2; i++) {
        t[pos[i]]='(';
        left=pos[i];
    }

    int right=-1;
    for(size_t i=0; i<pos.size(); i++) {
        if(t[pos[i]]=='?') {
            t[pos[i]]=')';
            if(right<0)
                right=pos[i];
        }

    }

    if(left<0 || right<0) {
        cout<<"YES"<<endl;
        return;
    }

    //cerr<<"f[0]="<<f[0]<<" f[1]="<<f[1]<<endl;
    //cerr<<"left="<<left<<" rigth="<<right<<endl;
    //cerr<<"t="<<t<<endl;
    swap(t[left],t[right]);

    int bal=0;
    for(size_t i=0; i<s.size(); i++) {
        if(t[i]=='(')
            bal++;
        else if(t[i]==')')
            bal--;
        else
            assert(false);

        if(bal<0) {
            cout<<"YES"<<endl;
            return;
        }
    }

    cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
