/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return max(a,b);
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 * We can allways go up to the topmost reacheable row,
 * then left/right, then down.
 *
 * So just check if the max value in interval of cols
 * is smaller or bgeq the topmost reacheable row, and
 * if col distances are multiple of k.
 *
 * Check max in segment using segment tree.
 *
 * Also note that this is much simpler than C,
 * even if made artificial 'complecated' by reversing
 * usual orientation of axes.
 */
void solve() {
    cini(n);
    cini(m);

    cinai(a,m);
    stree seg(a);

    cini(q);
    for(int i=0; i<q; i++) {
        cini(xs); xs;
        cini(ys); ys--;
        cini(xf); xf;
        cini(yf); yf--;
        cini(k);

        if(abs(xs-xf)%k!=0 || abs(ys-yf)%k!=0) {
            cout<<"NO"<<endl;
            continue;
        }
        int top=n-((n-xs)%k);

        if(ys>yf)
            swap(ys,yf);
        int ma=seg.prod(ys, yf+1);
        if(ma>=top) {
            cout<<"NO"<<endl;
            continue;
        }
        cout<<"YES"<<endl;
    }
}

signed main() {
        solve();
}
