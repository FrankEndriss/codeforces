/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Understand the statement:
 * If moving from h[i] to h[j], cost=max(0, h[i]-h[j])
 * So just move from s to t and count?
 * Use prefix sums.
 *
 * One of the sickest problem statements ever, shame on you problemsetters.
 * Cluttering a trivial problem into a complecated text is allways a 
 * bad idea, and makes the problem allways worse.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    vi b=a;
    reverse(all(b));

    vi pre(n+1);
    vi pos(n+1);
    for(int i=0; i+1<n; i++) {
            pre[i+1]=pre[i]+max(0LL, a[i]-a[i+1]);
            pos[i+1]=pos[i]+max(0LL, b[i]-b[i+1]);
    }

    for(int i=0; i<m; i++) {
        cini(s); s--;
        cini(t); t--;
        if(t<s) {
            int ans=pos[n-1-t]-pos[n-1-s];
            cout<<ans<<endl;
        } else   {
            int ans=pre[t]-pre[s];
            cout<<ans<<endl;
        }
    }
}

signed main() {
        solve();
}
