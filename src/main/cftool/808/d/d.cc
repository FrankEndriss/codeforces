/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Looks like knapsack...
 * But with N=1e5,M=1e9 knapsack does not work.
 *
 * Consider the div position. Foreach such pos
 * check if the sum(right)-sum(left) is twice any element from right.
 * If yes, then we can move that element from right to left to get
 * two equal sums.
 * Maintain right elements in a multiset, so its kind of greedy.
 *
 * Note that we need to do this twice, once on reversed input.
 */
void solve() {
    cini(n);
    cinai(a,n);
    const int gsum=accumulate(all(a), 0LL);

    for(int k=0; k<2; k++) {
        multiset<int> r;
        for(int i=0; i<n; i++)
            r.insert(a[i]);

        int diff=gsum;
        for(int i=0; i<n; i++) {
            if(diff%2==0) {
                auto it=r.find(diff/2);
                if(it!=r.end()) {
                    cout<<"YES"<<endl;
                    return;
                }
            }
            diff-=2*a[i];
            auto it=r.find(a[i]);
            r.erase(it);
        }
        reverse(all(a));
    }
    cout<<"NO"<<endl;
}

signed main() {
    solve();
}
