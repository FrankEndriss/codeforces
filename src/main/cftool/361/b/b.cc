/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* Construct perm with k elements gcd(i+1, a[i])!=1
 * Note that we can move the whole perm one position
 * (left) to make all positions gcd==1.
 * Note that element at position 1 allways has gcd==1.
 *
 * So, we can move n-k consequtive elements by one position, so
 * these will have gcd==1.
 *
 * Case n is odd:
 * p[0] stays, all other shift by one, gcd(1,p[0])==1
 * case n is even:
 * all shift by one, but first and last still gcd==1
 */
void solve() {
    cini(n);
    cini(k);
    vi p(n+1);
    iota(p.begin(), p.end(), 0);

    if(n-k==0) {
        cout<<-1<<endl;
        return ;
    }

    for(int i=n-k; i>=2; i--) {
        p[i]=p[i-1];
    }
    p[1]=n-k;

    for(int i=1; i<=n; i++)
        cout<<p[i]<<" ";
    cout<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

