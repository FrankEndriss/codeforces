/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * consider c[0]
 * If c[0]==0 we can ignore it, ans[0] is 0.
 * else the first c[0] position of a[] must be 1.
 *
 * Then there must be a 0.
 * consider c[1]
 * if c[0]>=2, then there must be two 0 in prefix of len c[1]+1
 *
 * So we remove/ignore a zero prefix where all c[j]==0, 
 * ans fill up a[j] with 0s.
 * Then there is the first 1.
 *
 * Consider position i:
 *
 * if a[i]==0 then c[i] is in sorting range in the n-i last sortings.
 * In the first part of them the value was 1, and the others value was 0.
 * c[i] times it was 1, so in the prefix up to
 * i+1 there is 1 '1'
 * i+2 there are 2 '1'
 * ...
 * i+c[i] ther are c[i] '1'
 *
 * if a[i]==1 then it is same, but in the first i sortings that initial '1'
 * contributed, so c[i]-=i.
 * But how to distinguish?
 * -> if case a[i]==0 is not possible it must be a[i]==1
 *  *****
 *  What a horrible prefix/index fiddling :/
 *  How to sort that out correctly?
 *
 *  Consider c[n-1]
 *  if a[n-1]=='1' then c[n-1]=n else c[n-1]=1
 *  ...no
 *  *****
 *  Again, consider leftmost c[l]>0
 *  there are c[l] '1's in ans starting at position l, followed by a '0'
 *  Consider c[l+1], it is known to be '0' or '1' in ans. (Most likely it is '1', but
 *  also consider c[l+i])
 *  If it is 1, then the first i rounds contributed, so consider x=c[l+i]-(l+i)
 *  x is the number of rounds it contibuted after the first i rounds, then 
 *  followed n-x-i rounds with 0. So in the prefix of
 *  len=i+1 there is 1 or more '1's
 *  len=i+2 there is 2 or more '1's
 *  ...
 *  len=i+x there is exactly x '1's
 *
 * if if is 0, then the first i rounds _did not_ contribute, so x=c[i];
 */
void solve() {
    cini(n);
    cinai(c,n);

    vi pre(n+1,-1);
    pre[0]=0;
    string ans(n, ' ');
    int left=0;
    while(left<n && c[left]==0) {
        ans[left]='0';
        pre[left+1]=0;
        left++;
    }

    if(left==n) {
        for(int i=0; i<n; i++) 
            cout<<ans[i]<<" ";
        cout<<endl;
        return;
    }

    /* at least one 1 in a[] */
    ans[left]='1';
    pre[left+1]=1;

    for(int pos=left; pos<n; pos++) {
        assert(ans[pos]=='0' || ans[pos]=='1');

        int x;
        if(ans[pos]=='0')
            x=c[pos];
        else
            x=c[pos]-pos;

        /* There are n-pos sorted rounds, 
         * x of them contriuted 1 
         * n-pos-x contributed 0
         *
         **/
        //assert(x>0);
        for(int k=1; pos+k<n && k<=x; k++) {
            if(ans[pos+k]=='0' || ans[pos+k]=='1')
                continue;

            /* in prefix up to pos+k there must be at least k '1's */
            int cnt=pre[pos+k];
            if(cnt+1<k) {
                ans[pos+k]='1';
                pre[pos+k+1]=pre[pos+k]+1;
            } else {
                ans[pos+k]='0';
                pre[pos+k+1]=pre[pos+k];
            }
        }
    }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
