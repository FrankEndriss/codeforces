/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * if a<b then allways conquer one, move capital, conquer next...
 *
 * But if a>b, then it could be better to conquer x[0] and x[1]
 * cost1=    b*x[0] + b*x[1]
 * cost2=a + b*x[0] + b*(x[1]-x[0])
 *
 * Note that it allways make sense to conquer the next x[i] to
 * the right, never jump over one.
 *
 * The sum of price is conquery/move cap to some city j, then
 * conquer all other x[k>j] for the price of its prefix sum.
 *
 * If moving the capital anyway, it allways make sense to
 * move it step by step.
 * So left part is 
 * a*j + b*prefix[j]
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(a);
    cini(b);

    /* add an artifical city at position 0 */
    vi x(n+1);
    for(int i=1; i<=n; i++) 
        cin>>x[i];

    vi pre(n+1);
    for(int i=1; i<=n; i++) 
        pre[i]=pre[i-1]+x[i];

    int ans=INF;
    for(int i=0; i<n; i++) {
        int left=a*x[i]+x[i]*b;    /* move capital to city i */
        int right=((pre[n]-pre[i]) - (x[i]*(n-i)))*b; // - (x[i]*i)*b;
        //cerr<<"i="<<i<<" left="<<left<<" rigth="<<right<<" l+r="<<left+right<<endl;
        ans=min(ans, left+right);
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
