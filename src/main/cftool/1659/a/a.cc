/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Min number of consecutive R
 * 11011
 */
void solve() {
    cini(n);
    cini(r);
    cini(b);
    int val=(r+b)/(b+1);

    string ans;
    while(r>0 || b>0) {
        cerr<<"r="<<r<<" b="<<b<<endl;
        if(r>=b) {
            for(int i=0; r>0 && r>=b && i<val; i++) {
                ans.push_back('R');
                r--;
            }
            if(b>0) {
                ans.push_back('B');
                b--;
            }
        } else {
                ans.push_back('B');
                b--;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
       solve();
}
