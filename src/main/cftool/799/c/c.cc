/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * two types of fontains.
 * He can buy the best d-fontain and the best c-fontain,
 * or two d-fontains or two c-fontains.
 *
 * If buying two of one kind, consider buying any one,
 * and find the best second one by prefix sums.
 */
void solve() {
    cini(n);
    cini(c);
    cini(d);

    const int INF=1e9+7;

    /* find best pair that can be bought for m money. There are some
     * edgecases, so go like this:
     * Iterate all v.size() elements, and foreach one find the
     * best second font.
     * For that, maintain the two best beauties (and indexes) of the
     * prefix.
     **/
    function<int(vector<pii>&,int)> maxpair=[&](vector<pii> &v, int m) {
        if(v.size()<2)
            return 0LL;

        vector<pii> pre0(v.size(), {-INF,-INF}); /* best choice in prefix */
        vector<pii> pre1(v.size(), {-INF,-INF}); /* second best choice in prefix */

        pre0[0].first=v[0].second;
        pre0[0].second=0;

        for(size_t i=1; i<v.size(); i++) {
            pre0[i]=pre0[i-1];
            pre1[i]=pre1[i-1];
            if(v[i].second>pre0[i].first) {
                pre1[i]=pre0[i];
                pre0[i].first=v[i].second;
                pre0[i].second=i;
            }
        }

        int ans=0;
        for(size_t i=0; i<v.size(); i++) {
            auto it=upper_bound(all(v), make_pair(m-v[i].first, INF));
            if(it!=v.begin()) {
                it--;
                int idx=distance(v.begin(), it);
                if(pre0[idx].second!=(int)i) {
                    ans=max(ans, v[i].second+pre0[idx].first);
                } else {
                    ans=max(ans, v[i].second+pre1[idx].first);
                }
            }
        }
        return ans;
    };

    vector<pii> cc;      /* <price,beauty> of C-types */
    vector<pii> dd;

    int ansC=0;
    int ansD=0;
    for(int i=0; i<n; i++) {
        cini(b);
        cini(p);
        cins(s);
        if(s=="C" && p<=c)  {
            cc.emplace_back(p,b);
            ansC=max(ansC, b);
        } else if(s=="D" && p<=d)  {
            dd.emplace_back(p,b);
            ansD=max(ansD, b);
        }
    }
    //cerr<<"cc.size()="<<cc.size()<<" dd.size()<<"<<dd.size()<<endl;

    sort(all(cc));
    sort(all(dd));

    int ans=0;
    if(cc.size() && dd.size())
        ans=ansC+ansD;

    ans=max(ans, max(maxpair(cc,c), maxpair(dd,d)));
    cout<<ans<<endl;
}

signed main() {
        solve();
}
