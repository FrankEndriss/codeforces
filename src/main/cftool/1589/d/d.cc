/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

vi dd(67);
void test() {
    for(size_t i=0; i<dd.size(); i++) 
        dd[i]=i+1;

    int i=11;
    int j=17;
    int k=25;

    reverse(dd.begin()+i,dd.begin()+j);
    reverse(dd.begin()+j,dd.begin()+k);
}

    /*
int ask(int l, int r) {
    l--;
    r--;
    int ans=0;
    for(int i=l; i<=r; i++) 
        for(int j=i+1; j<=r; j++) 
            if(dd[i]>dd[j])
                ans++;
    cerr<<"ask l="<<l+1<<" r="<<r+1<<" ans="<<ans<<endl;
    return ans;
}
*/

int ask(int l, int r) {
    cout<<l<<" "<<r<<endl;
    int ans;
    cin>>ans;
    return ans;
}

const int N=1e5;
vi q(N);
void init() {
    for(int i=1; i<N; i++) 
        q[i]=i*(i-1);

    /*
    for(int i=1; i<20; i++) 
        cerr<<"q["<<i<<"]="<<q[i]<<endl;
        */
}

/**
 * Two consecutive segments swapped.
 * Inversions, and len:
 * 2: 1
 * 3: 3
 * 4: 6
 * 5: 10
 * i: i*(i-1)/2
 *
 * Find k and the sum of both segments with max 30 questions.
 * Then, from the sum find the both lenghts, so i.
 * Ask one more question to decide which len is left, for j.
 *
 * ...But, how to find k with max(n)==1e9?
 * x=i*(i-1)/2 + j*(j-1)/2
 * x=(i^2-i + j^2-j) / 2
 */
void solve() {
    cini(n);
    n=dd.size();

    int len1=-1, len2=-1;
    int l=1;
    int r=n+1;
    while(l+1<r) {
        int mid=(l+r)/2;
        int val=2*ask(1,mid);

        /* find q[i]+q[j]==val */
        auto it=upper_bound(all(q), val);
        it--;
        bool found=false;
        while(it!=q.begin()) {
            int v1=*it;
            auto it2=lower_bound(q.begin()+2, q.end(), val-v1);
            if(*it2+v1==val) {
                found=true;
                len1=distance(q.begin(), it);
                len2=distance(q.begin(), it2);
                break;
            }
            if(v1*2<=val)
                break;
            it--;
        }
        //cerr<<"val="<<val<<" found="<<found<<endl;
        if(found)
            r=mid;
        else
            l=mid;
    }

    assert(len1>0 && len2>0);
    if(len1!=len2) {
        int val=ask(l-len1-len2, l-len2);
        auto it=lower_bound(all(q), val);
        if(val==*it) {
            cout<<"! "<<l-len1-len2<<" "<<l-len2<<" "<<l<<endl;
        } else {
            cout<<"! "<<l-len1-len2<<" "<<l-len1<<" "<<l<<endl;
        }
    }
}

signed main() {
    //test();
    init();
    cini(t);
    while(t--)
        solve();
}
