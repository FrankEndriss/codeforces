/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* we run down some fields, then up that fields, then one right.
 * ...and sometimes we can go left/right... what a shitty problem.
 * repeat.
 * Blame on you, problemsetter!
 *
 * (n-1) * DRL
 * (n-1) * U
 * 1 * R
 * REPEAT
 * (m-1) * L
 *
 */
void solve() {
    cinll(n);
    cinll(m);
    cinll(k);

    ll ma=(n-1)*4*m+m-1+m-1;

    if(k>ma) {
        cout<<"NO"<<endl;
        return;
    }

    vector<pair<int,string>> ans;

    pii pos={0,0};
    while(k>0) {
        if(pos.first==n-1) { // last row
            ans.push_back({min(n-1, k), "U"});
            k-=min(n-1, k);
            if(k) {
                ans.push_back({1, "R"});
                k--;
            }
            pos.first=0;
            pos.second++;
        } else if(k>=3) {
            int cnt=min(n-1, k/3);
            ans.push_back({cnt, "DRL"});
            k-=cnt*3;
            pos.first+=cnt;
        } else if(k==2) {
            ans.push_back({k, "DR"});
            k-=2;
        } else {
            ans.push_back({k, "D"});
            k--;
        }
    }

    cout<<"YES"<<endl;
    cout<<ans.size()<<endl;
    for(auto p : ans) {
        cout<<p.first<<" "<<p.second<<endl;
    }

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

