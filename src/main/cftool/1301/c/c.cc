/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll gaus(ll n) {
    return n*(n+1)/2;
}

/* we want to know how many substrings exists with an '1' in it
 * if s is of len n and has m '1' in it.
 * if we have s, then
 *
 * # of all substrings:
 * n + n-1 + n-2 + n-3...
 * ==n*(n+1)/2
 * ==gaus(n)
 *
 * substring made of '0'
 * ==gaus(len('0'))
 */
void solve() {
    cini(n);
    cini(m);
    if(m==0) {
        cout<<0<<endl;
        return;
    }

/* now we need to find E(gaus(len(x))) for
 * all subsegments made of 0.
 * It is n-m 0s devided in m-1 buckets.
 * m-1 can be big.
 */
    int buckets=m+1;
    int lbound=(n-m)/buckets;   // number 0s in all buckets
    int rbound=lbound+1;
    int unbound=(n-m)-lbound*buckets;
    int smallbuckets=buckets-unbound;
    int bigbuckets=buckets-smallbuckets;

    ll ans=gaus(n)-smallbuckets*gaus(lbound)-bigbuckets*gaus(rbound);
    cout<<ans<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

