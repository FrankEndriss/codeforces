/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Base pattern
 * 00 01 10 11
 * 11 00 01 10
 * 10 11 00 01
 * 01 10 11 00
 */
void solve() {
    vvi base= {
        { 0, 1, 2, 3},
        { 4+1, 4+2, 4+3, 4+0},
        { 8+2, 8+3, 8+0, 8+1},
        { 12+3, 12+0, 12+1, 12+2}
    };

    cini(n);
    for(int i=0; i<n; i++)  {
        for(int j=0; j<n; j++) {
            int pre=(i/4)*(n/4)+(j/4);
            pre<<=4;
            cout<<base[i%4][j%4]+pre<<" ";
        }
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
