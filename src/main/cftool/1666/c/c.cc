/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Check all 3 permutations.
 *
 * Something wrong, why???
 */
const int INF=1e18;
void solve() {
    vvi a(3,vi(2));
    for(int i=0; i<3; i++) 
        for(int j=0; j<2; j++) 
            cin>>a[i][j];

    vi p={0,1,2};
    vi mip;
    int mid=INF;
    do {
        const int d=abs(a[p[0]][0]-a[p[1]][0]) + abs(a[p[0]][1]-a[p[1]][1])
                   +abs(a[p[1]][0]-a[p[2]][0]) + abs(a[p[1]][1]-a[p[2]][1]);
        if(d<mid) {
            mip=p;
            mid=d;
        }
    }while(next_permutation(all(p)));

    vector<pair<pii,pii>> ans;
    if(a[mip[0]][0]!=a[mip[1]][0])
        ans.push_back(make_pair( make_pair( a[mip[0]][0], a[mip[0]][1] ), make_pair( a[mip[1]][0], a[mip[0]][1] )));
    if(a[mip[0]][1]!=a[mip[1]][1])
        ans.push_back(make_pair( make_pair( a[mip[1]][0], a[mip[0]][1] ), make_pair( a[mip[1]][0], a[mip[1]][1] )));

    if(a[mip[1]][0]!=a[mip[2]][0])
        ans.push_back(make_pair( make_pair( a[mip[1]][0], a[mip[1]][1] ), make_pair( a[mip[2]][0], a[mip[1]][1] )));
    if(a[mip[1]][1]!=a[mip[2]][1])
        ans.push_back(make_pair( make_pair( a[mip[2]][0], a[mip[1]][1] ), make_pair( a[mip[2]][0], a[mip[2]][1] )));

    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++) 
        cout<<ans[i].first.first<<" "<<ans[i].first.second<<" "<<ans[i].second.first<<" "<<ans[i].second.second<<endl;

}

signed main() {
    solve();
}
