/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/**
 * We can brute force all possible solutions.
 */
void solve() {
    cini(n);
    cini(m);

    set<pair<pii,pii>> a;
    for(int i1=1; i1<=n; i1++)
        for(int j1=1; j1<=m; j1++) 
            for(int i2=1; i2<=n; i2++) 
                for(int j2=1; j2<=m; j2++) {
                    if(i1!=i2 || j1!=j2) {
                        vector<pii> vp={ make_pair(i1,j1),make_pair(i2,j2)};
                        sort(all(vp));
                        a.emplace(vp[0],vp[1]);
                    }
                }


    function<int(pii,pii)> dist=[&](pii p1, pii p2) {
        return abs(p1.first-p2.first)+abs(p1.second-p2.second);
    };

    set<pii> vis;
    while(a.size()>1) {
        cerr<<"a.size()="<<a.size()<<endl;
        pii p;
        bool ok=false;
        for(int i=0; i<1000; i++) {
            p={rand()%n+1, rand()%m+1};
            if(vis.count(p)==0) {
                ok=true;
                vis.insert(p);
                break;
            }
        }

        if(!ok) 
            break;

        cout<<"SCAN "<<p.first<<" "<<p.second<<endl;
        int d;
        cin>>d;

        set<pair<pii,pii>> a0;
        for(auto pp : a) {
            if(dist(pp.first, p)+dist(pp.second,p)==d)
                a0.insert(pp);
        }
        a.swap(a0);
    }

    int cnt=0;
    set<pii> digged;
    while(a.size()>0 && cnt<2) {
        auto it=a.begin();
        auto pp=*it;
        int c;
        if(digged.count(pp.first)==0) {
            cout<<"DIG "<<pp.first.first<<" "<<pp.first.second<<endl;
            cin>>c;
            cnt+=c;
            digged.insert(pp.first);
        }
        if(cnt<2 && digged.count(pp.second)==0) {
            cout<<"DIG "<<pp.second.first<<" "<<pp.second.second<<endl;
            cin>>c;
            cnt+=c;
            digged.insert(pp.second);
        }
        a.erase(it);
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
