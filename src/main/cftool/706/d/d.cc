/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * For query we need to check bit by bit, starting at higest.
 * For each bit we need to consider all numbers having that
 * bit set or notset, depending on if it is set in x or notset in x.
 *
 * We can do this with a tree like structure, where every node has
 * 2 children,
 * c0 is the subtree with all elements having that bit notset,
 * c1 is the subtree with all elements having that bit set.
 *
 * A subtree is a leaf xor a node with children.
 * So while query we need to go down the tree until there is
 * only one element left, and return that one.
 *
 * Since it is a multiset, we store together with the values
 * the number of times that value is in the set.
 *
 */
const int N=2e5;
const int B=30;
void solve() {

    struct node {
        int c0, c1, cnt;
        node():c0(-1),c1(-1),cnt(0) {
        }
    };

    int tidx=0;         // index of next free node
    stack<int> free;    // stack of freed nodes
    vector<node> tree(N*B+7); // node space

    function<void(int)> freenode=[&](int nidx) {
        free.push(nidx);
    };

    function<int()> newnode=[&]() {
        if(free.size()) {
            int ans=free.top();
            free.pop();
            tree[ans].c1=tree[ans].c0=-1;
            tree[ans].cnt=0;
            return ans;
        } else
            return tidx++;
    };


    /* query for the value in the tree that gives max(x^val)
     **/
    function<int(node&,int,int)> queryx=[&](node& th, int bmask, int x) {
        if(bmask==0)
            return 0LL;

        if(th.c1<0 || (th.c0>=0 && (x&bmask))) {
            return queryx(tree[th.c0], bmask>>1, x);
        } else {
            return bmask | queryx(tree[th.c1], bmask>>1, x);
        }
    };

    function<void(node&,int,int,int)> insert=[&](node& th, int bmask, int x, int times) {
        //cerr<<"insert, bmask="<<bmask<<" x="<<x<<endl;
        if(bmask==0) {
            th.cnt+=times;
            return;
        }

        if(x&bmask) {
            if(th.c1<0)
                th.c1=newnode();
            insert(tree[th.c1], bmask>>1, x, times);
        } else {
            if(th.c0<0)
                th.c0=newnode();
            insert(tree[th.c0], bmask>>1, x, times);
        }
    };

    function<bool(node&,int,int)> remove=[&](node& th, int bmask, int x) {
        if(bmask==0) {
            th.cnt--;
            assert(th.cnt>=0);
            return th.cnt==0;
        }

        if(x&bmask) {
            bool rm=remove(tree[th.c1], bmask>>1, x);
            if(rm) {
                freenode(th.c1);
                th.c1=-1;
            }
        } else {
            bool rm=remove(tree[th.c0], bmask>>1, x);
            if(rm) {
                freenode(th.c0);
                th.c0=-1;
            }
        }
        return th.c0<0 && th.c1<0;
    };

    cini(q);
    int root=-1;
    for(int i=0; i<q; i++) {
        cins(t);
        cini(x);
        //cerr<<"t="<<t<<" x="<<x<<endl;
        if(t[0]=='+') {
            if(root<0)
                root=newnode();
            insert(tree[root], 1LL<<B, x, 1);
        } else if(t[0]=='-') {
            assert(root>=0);
            if(remove(tree[root], 1LL<<B, x))
                root=-1;
        } else if(t[0]=='?') {
            /* note that this is completly stupid testcase.
             * there should not be a query on an empty tree!
             */
            if(root<0) {
                cout<<x<<endl;
            } else {
                int v=queryx(tree[root], 1LL<<B, x);
                /* for some reason the tree is considered to always include value 0 */
                cout<<max(x, (v^x))<<endl;
            }
        } else
            assert(false);
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
