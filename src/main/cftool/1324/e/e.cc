/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(h);
    cini(l);
    cini(r);
    cinai(a,n);

    vvi dp(n, vi(h, -1)); /* dp[i][j]== on ith day time is j and number of good sleeps so far */

    int val=0;
    if(a[0]>=l && a[0]<=r)
        val=1;
    dp[0][a[0]]=val;
    int ans=val;
    val=0;

    int h1=a[0]-1;
    if(h1>=l && h1<=r)
        val=1;
    dp[0][h1]=val;
    ans=max(ans,val);

    for(int i=1; i<n; i++) {
        for(int j=0; j<h; j++) {
            if(dp[i-1][j]>=0) {
                int lh0=(j+a[i]);
                while(lh0>=h)
                    lh0-=h;
                int g=0;
                if(lh0>=l && lh0<=r)
                    g=1;
                dp[i][lh0]=max(dp[i][lh0], dp[i-1][j]+g);
                ans=max(ans,dp[i][lh0]);

                int lh1=(j+a[i]-1);
                while(lh1>=h)
                    lh1-=h;
                g=0;
                if(lh1>=l && lh1<=r)
                    g=1;

                dp[i][lh1]=max(dp[i][lh1], dp[i-1][j]+g);
                ans=max(ans,dp[i][lh1]);
            }
        }
    }
    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

