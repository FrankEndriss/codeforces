/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Its about dfs and rerooting.
 * In first step we find the dp for root==0
 * Second step we dfs the tree, and reroot the dp in 
 * each step.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    for(int i=0; i<n; i++) 
        if(a[i]==0)
            a[i]--;

    vvi adj(n);
    for(int i=0; i+1<n; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vi dp(n,-INF);
    function<void(int,int)> dfs1=[&](int v, int p) {
        dp[v]=a[v];

        for(int chl : adj[v]) {
            if(chl!=p) {
                dfs1(chl, v);
                dp[v]+=max(0LL, dp[chl]);
            }
        }
    };

    dfs1(0,-1);
    vi ans(n);

    /* reroot from to its childs but not to p */
    function<void(int,int)> dfs2=[&](int from , int p) {
        ans[from]=dp[from];
        for(int chl : adj[from]) {
            if(chl==p)
                continue;

            dp[from]-=max(0LL, dp[chl]);
            dp[chl]+=max(0LL, dp[from]);
            dfs2(chl, from);
            dp[chl]-=max(0LL, dp[from]);
            dp[from]+=max(0LL, dp[chl]);
        }
    };

    dfs2(0, -1);

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
