/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* looks like a segment tree but is only the lazy update part. */
struct lupd {
    int n;
    vi data;
    vb isset;
    lupd(int _n):n(_n) {
        int i=1;
        while(i<n)
            i<<=1;
        n=i;
        data.resize(n*2);
        isset.resize(n*2);
    }

    /* inclusive i, exclusive r */
    void set(int didx, int nodeL, int nodeR, int l, int r, int v) {
        //cerr<<"l="<<l<<" r="<<r<<" nodeL="<<nodeL<<" nodeR="<<nodeR<<" v="<<v<<endl;
        assert(l<r && l>=nodeL && r<=nodeR);
        if(nodeL==l && nodeR==r) {
            isset[didx]=true;
            data[didx]=v;
            return;
        }

        const int mid=(nodeL+nodeR)/2;
        if(isset[didx]) {   // propagate previous value to deeper levels
            set(didx*2,   nodeL, mid, nodeL, mid, data[didx]);
            set(didx*2+1, mid, nodeR, mid, nodeR, data[didx]);
            isset[didx]=false;
        }

        if(l<mid)
            set(didx*2,   nodeL, mid, l, min(mid,r), v);
        if(r>mid) 
            set(didx*2+1, mid, nodeR, max(mid,l), r, v);
    }

    int get(int didx, int nodeL, int nodeR, int idx) {
        assert(idx>=nodeL && idx<nodeR);
        if(isset[didx])
            return data[didx];

        const int mid=(nodeL+nodeR)/2;
        if(idx<mid)
            return get(didx*2, nodeL, mid, idx);
        else
            return get(didx*2+1, mid, nodeR, idx);
    }
};

/* 
 * range updates with persistent segment tree like structure.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    cinai(b,n);

    lupd lazy(n);
    lazy.set(1,0,lazy.n, 0, lazy.n, INF);

    for(int i=0; i<m; i++) {
        cini(t);
        if(t==1) {
            cini(x);
            cini(y);
            cini(k);
            //cerr<<"set, y="<<y<<" y+k="<<y+k<<" val="<<y-x<<endl;
            lazy.set(1, 0, lazy.n, y-1, y-1+k, y-x);
        } else if(t==2) {
            cini(x); x--;
            int offs=lazy.get(1, 0, lazy.n, x);
            if(offs==INF)
                cout<<b[x]<<endl;
            else
                cout<<a[x-offs]<<endl;
        } else assert(false);
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
