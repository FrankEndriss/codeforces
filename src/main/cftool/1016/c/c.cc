/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* There are two patterns how we can move:
 * DRURDR...
 * RRRRRRDLLLLLL
 * So we can combine those patterns, which makes it kind of a dp
 * In the first pattern, every time we are going R we can continue 
 * with the up/down for one more col, or go R until last cell, then
 * go back.
 * Find max of all those combinations.
 * To calculate the horizontal sums quickly we use prefix sums
 * of the groth rates, from both directions, in both rows.
 *
 * ...Still some off by one in prefix sums,
 * most likely in the part where going from right to left.
 */
void solve() {
    cini(n);
    vvi a(2, vi(n));
    for(int i=0; i<2; i++) 
        for(int j=0; j<n; j++)
            cin>>a[i][j];

    /* initial number of mushrooms in row i, direction j, position k (from begin) */
    vvvi pre1(2, vvi(2, vi(n+1)));
    for(int i=0; i<2; i++) {
        for(int k=0; k<n; k++) {
            pre1[i][0][k+1]=pre1[i][0][k]+a[i][k]*k;
            pre1[i][1][n-1-k]=pre1[i][1][n-1-k+1]+a[i][n-k-1]*k;
        }
    }

    /* groth per minute in prefix row i, direction j, position k (from begin) */
    vvvi pre2(2, vvi(2, vi(n+1)));
    for(int i=0; i<2; i++)  {
        for(int k=0; k<n; k++) {
            pre2[i][0][k+1]=pre2[i][0][k]+a[i][k];
            pre2[i][1][n-1-k]=pre2[i][1][n-1-k+1]+a[i][n-k-1];
        }
    }


    /* contribution if starting at time t in row row and col col going R until end.  */
    function<int(int,int,int)> goR=[&](int t, int row, int col) {
        /* groth rate of cells on the way */
        const int g=pre2[row][0][n]-pre2[row][0][col];

        return pre1[row][0][n]-pre1[row][0][col] - g*col +
            t* (pre2[row][0][n]-pre2[row][0][col]);
    };
    
    /* contribution if starting at time t at end in row row and going L until col col. */
    function<int(int,int,int)> goL=[&](int t, int row, int col) {
        return pre1[row][1][col] + t*pre2[row][1][col];
    };

    function<int(int)> p2part=[&](int col) {
        const int row=col&1;
        const int t=col*2;
        return goR(t, row, col) + goL(n+col, !row, col);
    };

    int ans=0;
    int sum=0;
    int crow=0; /* current starting row */
    for(int i=0; i<n; i++) {
        int lans=sum + p2part(i);
        ans=max(ans, lans);
        sum+=i*2*a[crow][i];
        sum+=(i*2+1)*a[!crow][i];
        crow=!crow;
    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}
