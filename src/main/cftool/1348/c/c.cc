/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* greedy 
 * first letter of all string in ans must be smallest possible.
 *
 * Once we have a prefix smaller than some other, we append 
 * all remaining to that one, and use the other as ans.
 *
 * if we have all prefix same, then
 * 1. we put all remaining letters to one string, or
 * 2. evenly ditribute the letters because all are same, ans ins one of the longer
 * */
void solve() {
    cini(n);
    cini(k);
    cins(s);
    sort(s.begin(), s.end());

/* note k<=s.size() */
    vs ans(k);
    int idx=0;
    for(; idx<k; idx++)
        ans[idx]+=s[idx];

    if(ans[0]!=ans[k-1]) {
        cout<<ans[k-1]<<endl;
        return;
    }

    string ans1=ans[0]+s.substr(idx, s.size()-idx);
    while(idx<s.size()) {
        for(int i=0; i<k && idx<s.size(); i++)  {
            ans[k-1-i]+=s[idx];
            idx++;
        }
    }
    sort(ans.begin(), ans.end());

    cout<<min(ans1, ans[k-1])<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

