/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s)
        c = c == ','?  ' ': c;
    stringstream ss;
    ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) {
    cerr << endl;
}
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0)
        cerr << ", ";
    stringstream ss;
    ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* First we want to grow with max speed, then we need
 * to find a way that we hit exaclty n.
 * Note that there is allways an answer, simpley dont split at all
 * and wait n nights.
 *
 * Note masses grow, never ungrow. So, we can increase the growth
 * speed every night to at most the double as before, and min stay same.
 * (first night speed==2)
 *
 * How to find the max value to that we should increase?
 * We start at 1 and increase allways by double, so we add
 * 1, 2, 4,...,2^n per night.
 * We end up less than 2^n before n.
 * We must add that number to our list off added numbers.
 * Now construct from that list the grows per night.
 *
 **/
void solve() {
    cini(n);

    if(n==2) {
        cout<<1<<endl;
        cout<<0<<endl;
        return;
    }

    int m=1;
    int g=1;
    vi ans;
    ans.push_back(g);
    while(m+2*g<=n) {
        g+=g;
        m+=g;
        ans.push_back(g);
    }

    if(n!=m)
        ans.push_back(n-m);

    sort(ans.begin(), ans.end());

    cout<<ans.size()-1<<endl;
    for(int i=1; i<ans.size(); i++)
        cout<<ans[i]-ans[i-1]<<" ";
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

