/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We want some seq a[] which if xored, gives an
 * increasing seq b[]
 * max(a[])==d
 * What about 
 * a={1,2,4,8,...}
 * b={1,3,7,15,...};
 *
 * a={2,4,...
 * b={2,6,...
 *
 * a={1,2,5,8,...
 * b={1,3,6,...
 * a={1,2|3,4|5|6|7,...}
 *
 * We can choose one of the numbers in every position, or not 
 * choose one.
 */
void solve() {
    cini(d);
    cini(mod);

    int ans=1;
    for(int i=0; i<64; i++) {
        int v1=min(d, (1LL<<(i+1))-1);
        int v2=1LL<<i;
        int val=v1-v2+2;
        if(val<=0)
            break;
        ans*=val;
        ans%=mod;
    }
    ans-=1;
    while(ans<0)
        ans+=mod;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
