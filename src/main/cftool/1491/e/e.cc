/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int N=1e5+7;
/* Note that there are only like 23 fibs up to 1e5
 * So foreach one we can check the whole tree
 * somehow.
 *
 * So we need to calc foreach edge how much vertex
 * are there on both ends.
 *
 * A tree of 3 vertex or smaller is a fib tree.
 * else we need to cut it into the two prev
 * fib numbers.
 * But, it seems there is more than one possible
 * cutting edge :/
 * How to find all cutting edges?
 * Start at any index, count indexes in all subtrees.
 *
 * ...Why is it fib numbers?
 */
void solve() {
    cini(n);

    vi fib={ 1, 1 };
    while(fib.back()<N)
        fib.push_back(fib[fib.size()-1]+fib[fib.size()-2]);

    vector<set<int>> adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].insert(v);
        adj[v].insert(u);
    }


    /* return true if current tree with some vertex v is
     * a fib-tree.
     */
    function<bool(int,int)> go=[&](int v, int nn) {
        if(nn<=3)
            return true;

        auto it=lower_bound(all(fib), nn);
        if(it==fib.end() && *it!=nn)
            return false;

        it--;
        auto f1=*it;
        it--;
        auto f2=*it;

        vi cnt(n);  /* cnt[i]=number of vertex in subtree i */
        function<int(int,int)> dfs=[&](int vv, int pp) {
            int ans=1;
            for(int chl : adj[vv]) 
                if(chl != pp)
                    ans+=dfs(chl,vv);
            return cnt[vv]=ans;
        };

        if(dfs(v,-1)!=nn)
            assert(false);

        set<pii> edg;   /* list of edges usable for current cut */

        function<void(int,int,int)> dfs2=[&](int vv, int pp, int pcnt) {
            if(pcnt==f1 || pcnt==f2)  {
                edg.emplace(vv,pp);
            } else if(pcnt>max(f1,f2))
                return;

            int sum=0;
            for(int chl : adj[vv]) {
                if(chl==pp)
                    continue;
                sum+=cnt[chl];
            }

            for(int chl : adj[vv]) {
                if(chl==pp)
                    continue;
                if(cnt[chl]>=min(f1,f2)) {
                    dfs2(chl, vv, pcnt+sum-cnt[chl]);
                }
            }
        };

        dfs2(v,-1,0);

        for(pii e : edg) {
            adj[e.first].erase(e.second);
            adj[e.second].erase(e.first);
            bool ans1=go(e.first, cnt[e.first]);
            bool ans2=go(e.second, cnt[e.second]);
            adj[e.first].insert(e.second);
            adj[e.second].insert(e.first);
            if(ans1&&ans2)
                return true;
        }
        return false;
    };

    if(go(0,n)) {
        cout<<"YES"<<endl;
    } else {
        cout<<"NO"<<endl;
    }
}

signed main() {
    solve();
}
