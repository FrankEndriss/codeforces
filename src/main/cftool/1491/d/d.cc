/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

vi bits(int x) {
    vi ans;
    for(int i=0; i<30; i++) 
        if(x&(1<<i))
            ans.push_back(i);
    return ans;
}

/* There is a vertex from max(u,v) to s=u+v
 * if v is a submask of u.
 *
 * We can move any bit in u one position left
 * by setting that bit in v.
 * 11&01 -> 11+01=100
 * 11&11 -> 11+11=110
 *
 * So, going bits from lowest to highest, if
 * the bit is set in s we must (and can) use
 * one of the lower bits in u to construct the
 * bit in s.
 * And actually one is enough.
 *
 * But consider u=111
 * 111+001=1000
 * 111+010=1001
 * 111+100=1011
 */
void solve() {
    cini(u);
    cini(s);
    if(s<u) {
        cout<<"NO"<<endl;
        return;
    }

    vi bu=bits(u);
    vi bs=bits(s);
    int uidx=0;
    for(size_t i=0; i<bs.size(); i++) {
        if(uidx<bu.size() && bu[uidx]<=bs[i]) {
            uidx++;
        } else {
            cout<<"NO"<<endl;
            return;
        }
    }
    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
