/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Allways start on first trampoline. This will take 
 * us to the first tramp s[i]>1.
 * ans+=s[i]-n
 * Then we decrease all tramps s[n-1]...s[1] by 1
 * Then same with second tramp...
 * repeat.
 *
 * Just simulate.
 * ***
 * Just noticed that I got the jumps wrong...
 * We not jump from i to i+s[i], but to
 * s[i]-1
 */
void solve() {
    cini(n);
    cinai(s,n);
    int ans=0;
    int first=0;
    while(first<n) {
        if(s[first]==1) {
            first++;
            continue;
        }
        if(s[first]>n-first) {
            int cnt=s[first]-(n-first);
            ans+=cnt;
            s[first]-=cnt;
            continue;
        }

        ans++;
        int next=first;
        while(next<n) {
            int nnext=next+s[next];
            s[next]--;
            if(s[next]<1) {
                s[next]=1;
            }
            next=nnext;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
