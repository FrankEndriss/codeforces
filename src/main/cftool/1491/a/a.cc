/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* ok. all wrong, it is trivial.
 */
void solve() {
    cini(n);
    cini(q);

    vi a(n+1);
    int cnt=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux) {
            a[i+1]=1;
            cnt++;
        }
    }

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(x);
            if(a[x]==0) {
                a[x]=1;
                cnt++;
            } else {
                a[x]=0;
                cnt--;
            }
        } else if(t==2) {
            cini(k);
            if(k<=cnt)
                cout<<1<<endl;
            else
                cout<<0<<endl;
        } else
            assert(false);
    }
}

signed main() {
    solve();
}
