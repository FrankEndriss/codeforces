/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* if all obstacles are in one col we need 1 horizontal +1  cheaper ops.
 * else we need 1 or 0.
 * If there is no "gap" we need 1 of cheaper.
 * else 0.
 */
void solve() {
    cini(n);
    cini(u);
    cini(v);

    int ans=2;
    cinai(a,n);
    for(int i=1; i<n; i++) {
        if(a[i]==a[i-1]) {
            ;
        } else if(abs(a[i]-a[i-1])==1) {
            ans=min(ans, 1LL);
        } else
            ans=0;
    }
    if(ans==2) 
        ans=v+min(u,v);
    else if(ans==1)
        ans=min(u,v);
    else
        ans=0;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
