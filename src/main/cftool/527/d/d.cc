/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Each point i covers a segment of size 2*w[i] arround its
 * position.
 * All points not intersecting those segments are connected.
 * So ans is max number of segments we can choose without 
 * intersection.
 *
 * Find this by dp, where dp[i]=min covered position choosing i intervals.
 * Looks a bit like LIS algo.
 *
 * Impl:
 * Foreach interval sorted by right:
 * Search how much we can place left of it by lower_bound()
 * Update the max number of intervals including the current one
 * with the current right border.
 */
const int INF=1e18;
void solve() {
    cini(n);
    vector<pii> p(n);
    for(int i=0; i<n; i++) {
        cini(x);
        cini(w);
        p[i]={x+w,x-w};
    }
    sort(all(p));

    /* dp[i]=min covered position using i segments */
    vi dp(n+1, INF);
    dp[0]=-INF;
    for(int i=0; i<n; i++) {
        auto it=upper_bound(all(dp), p[i].second);
        *it=min(*it, p[i].first);
    }

    int ans=distance(dp.begin(), lower_bound(all(dp), INF))-1;
    cout<<ans<<endl;

}

signed main() {
    solve();
}
