/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MAXN = 1000;
const int MAXK = 5;
ll C[MAXN + 1][MAXK + 1];

void init_nCr() {
    C[0][0] = 1;
    for (int n = 1; n <= MAXN; ++n) {
        C[n][0] = 1;
        for (int k = 1; k < MAXK; ++k)
            C[n][k] = C[n - 1][k - 1] + C[n - 1][k];
    }
}

ll nCr(int n, int k) {
    return C[n][k];
}


/* if k==1 then ans==1 since there is no permutation with
 * one element at wrong place, but all other in right place.
 * if k==2 then there are nCr(n,2) pairs, and foreach of
 * those pairs we can create one additional permutation.
 * if k==3 then there are nCr(n,3) triples, and foreach of
 * those triples we can create 2 add perms.
 * if k==4 ... nCr(n,4) quads, foreach 9 add perms.
 *
 * the 2 and 9 are the so called "derangements", see tutorial
 */
void solve() {
    cini(n);
    cini(k);

    int ans=1;
    if(k>1)
        ans+=nCr(n,2);
    if(k>2)
        ans+=nCr(n,3)*2;
    if(k>3) 
        ans+=nCr(n,4)*9;

    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    init_nCr();
    solve();
}

