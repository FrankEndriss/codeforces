/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* Since base == 1
 * area of carot is h/2
 * so, every piece must have area 
 * h/(2*n)
 *
 * Let h0 be height of lowest piece.
 * with of lower edge is 1==base.
 * with of upper edge is 1-(h0/h)
 * Area:
 *  h0*(1+1-(h0/h))/2
 * =h0*(2-h0/h)/2
 * =h0* (1 - h0/2*h)
 * =h0 - h0^2 / 2*h
 * ...
 * Consider upper/first piece, h1.
 * w1== h1/h
 * area = w1*h1/2== h1/h * h1/2 == h1^2 / h*2
 * Binsearch h1
 *
 * second piece, h2.
 * a2=(w1+w2)*h2/2
 * w2=w1+(h2/h)
 * a2=(w1+w1+(h2/n))*h2/2
 * Binsearch h2
 *
 * other hx the same...
 *
 * ----tutorial
 *  For some reason we can simply state that
 *  xi=h*sqrt(i/n)
 *  This is, because with growing hight of the
 *  pieces, their area grows qudratic.
 *  So, to have same area in all pieces we need to 
 *  choos a smaller hight for every next one.
 *  Sad that it seems to not be possible to explain
 *  the formulars in a way someone could understand them,
 *  if not understood before.
 *  I mean, it is an educational round. What's the educational
 *  point in not being able to explain the solution?
 */
void solve() {
    cini(n);
    cini(h);

    for(int i=1; i<n; i++) {
        double ans=h*sqrt(1.0*i/n);
        cout<<setprecision(12)<<fixed<<ans<<" ";
    }
    cout<<endl;
    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

