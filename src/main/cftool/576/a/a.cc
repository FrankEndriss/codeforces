/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

void init() {

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
}


/* To tell the number x we need to know
 * that it cannot be any other number.
 * So, we need to check all numbers
 * from 2 to n.
 * Note that we cannt check 1.
 *
 * But all products of exactly two primes
 * are determined by the answers to those
 * two primes!
 *
 * The answer to 1 is obvious, but n could be
 * 1 if no other number, so we need to know
 * the state of every num up to n.
 *
 * ...No, see tutorial.
 */
void solve() {
    init();
    cini(n);

    vi ans;
    vb vis(n+1);
    for(int i=0; pr[i]<=n; i++) {
        int p=pr[i];
        while(p<=n) {
            ans.push_back(p);
            p*=pr[i];
        }
    }

    sort(ans.begin(), ans.end());
    cout<<ans.size()<<endl;
    for(int i : ans)
            cout<<i<<" ";
    cout<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

