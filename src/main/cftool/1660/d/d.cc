/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * If there is a 0 in array we can allways make it 0.
 * Else split the array in segments between the 0
 * and find max foreach such subarray.
 *
 * ... to complecated to implement :/
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi pos0;
    pos0.push_back(-1);
    for(int i=0; i<n; i++)
        if(a[i]==0)
            pos0.push_back(i);

    pos0.push_back(n);

    int ansL=0;
    int ansR=0;
    int ans=0;  /* number of twos with positive outcome */
    for(size_t i=1; i<pos0.size(); i++) {
        if(pos0[i-1]+1==pos0[i])    /* empty */
            continue;

        int two=0;
        vi neg;
        for(int j=pos0[i-1]+1; j<pos0[i]; j++) {
            if(a[j]<0)
                neg.push_back(j);

            if(abs(a[j])>1)
                two++;
        }

        if(neg.size()&1) {
            int p0=pos0[i-1]+1;
            int p1=pos0[i]-1;

            int L=0;    
            for(int j=p0; j<=neg[0]; j++)
                if(abs(a[j])>1)
                    L++;
            int R=0;
            for(int j=p1; j>=neg.back(); j--)
                if(abs(a[j])>1)
                    R++;

            if(two-L>two-R && two-L>ans) {
                ans=two-L;
                ansL=neg[0]+1;
                ansR=n-(p1+1);
            } else if(two-R>ans) {
                ans=two-R;
                ansL=p0;
                ansR=n-neg.back();
            }

        } else {
            if(two>ans) {
                ansL=pos0[i-1]+1;
                ansR=n-pos0[i];
                ans=two;
            }
        }
    }
    cout<<ansL<<" "<<ansR<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
