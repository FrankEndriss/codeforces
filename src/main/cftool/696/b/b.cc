/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each vertex position within its siblings is kind of symetric,
 * in each sibling is with same prop before or after it.
 * So, the number of iterated vertex before v is half of the 
 * siblings subtree vertex count.
 */
void solve() {
    cini(n);

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(p);
        p--;
        adj[p].push_back(i+1);
    }

    vi cnt(n);
    function<void(int)> dfs1=[&](int v) {
        cnt[v]=1;
        for(int chl : adj[v]) {
            dfs1(chl);
            cnt[v]+=cnt[chl];
        }
    };
    dfs1(0);

    vi ans(n);
    function<void(int,int)> dfs2=[&](int v, int p) {
        ans[v]=ans[p]+cnt[p]-cnt[v]+1;
        for(int chl : adj[v])
            dfs2(chl,v);
    };
    ans[0]=2;
    for(int chl : adj[0])
        dfs2(chl,0);

    cout << fixed << setprecision(1);
    for(int i=0; i<n; i++) 
        cout<<((double)ans[i])/2<<" ";
    cout<<endl;
}

signed main() {
        solve();
}
