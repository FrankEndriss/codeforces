/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Brute force:
 * gcd(a[0])=a[0] so try to use biggest a possible.
 * But, that could mean that all other gcd(...)==1.
 * So we need to try the next "group" of values...somehow.
 *
 * However, given a prefix it allways seems to be optimal to use the
 * next value as to maximize the next gcd.
 * Not, see first example.
 * But, however, same numbers will allways occur as a block,
 * so same numbers just multiply contribution.
 *
 * ...hmmm.
 * Start with something, then try to find a better sol?
 * So first thing is start with biggest a[j]
 *
 * Then add all divisors of that a[j]...
 * *****
 * We want to create a graph.
 * That is, an edge weight is the gcd of both vertex.
 * Then, the value of a path...
 * *****
 *
 * Each a[i] contributes, 1 at least.
 * Also, it never contributes more that a[i]
 * The value it contributes depends on the a[i-k] left of it.
 */
void solve() {
    cini(n);
    map<int,int> m;
    for(int i=0; i<n; i++) {
        cini(aux);
        m[aux]++;
    }

    vector<pii> p(m.size());
    int i=0;
    for(auto ent : m) {
        p[i]={ent.second*ent.first, ent.first};
        i++;
    }
    sort(all(p), greater<pii>());




}

signed main() {
    solve();
}
