/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;
#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

/* tree1, just set some bits */
S st_op(S a, S b) {
    return a|b;
}

/* This is the neutral element */
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f|x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f|g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree1=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

using S2=int; /* type of values */
using F2=int; /* type of upates */

/* tree2, unset some bits on updates */
S st_op2(S a, S b) {
    return a|b;
}

/* This is the neutral element */
S st_e2() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping2(F f, S x) {
    return f&x;
}

/* This combines two updates.  */
F st_composition2(F f, F g) {
    return f&g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id2() {
    return -1;
}

using stree1=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;
using stree2=lazy_segtree<S2, st_op2, st_e2, F2, st_mapping2, st_composition2, st_id2>;

/**
 * The coziness is
 * sum of xor of all subseqs.
 *
 * Since each element is contained in 2^(n-1) subseqs, and that is even, that xor should be 0 ???
 * No, it is _sum_ of the xor of elements.
 *
 * ...we need to work out some formular here, sic :/
 *
 * Consider the subseqs containing a[i], all 2^(n-1) of them.
 * a[i]+
 * a[i]^a[i+1]+
 * a[i]^       a[i+2]+...
 * a[i]^a[i+1]^a[i+2]+...
 * Cases:
 * a bit is set in a[i], and in no other value,
 * then contribution = b*2^(n-1)
 * a bit is not set in a[i], and not in any other value
 * then contribution = 0
 * set in a[i], and in even number of other values:
 * then contribution = b*2^(n-1)
 * ...no, its not so simple :/
 *
 * And how commes these OR segements into play?
 * -> We know all the bits that must be set in at least one
 *  element of the subarray denoted by the segment.
 * We should be able to to create a seq fullfilling
 * the constraints of these OR segs.
 * How?
 * -> Each OR value gives us a set of bits that cannot be set in that segment.
 *    All other bits must be set in at least one position of the seq.
 *    How to find positions within the segment where we are allowed to
 *    set the value?
 *    Set it on all values, and later unset the not settable ones?
 *
 * Then, how to calc the coziness of that seq?
 * We need to find, foreach bit, how much subseqs have that bit even (odd) number of times set.
 * That is, if there are odd number of numbers, its odd, else even.
 * A bit contributes its values mul 2^(n-1)
 *
 * Let y be the numbers haveing that bit set minus one.
 * Let k be the numbers that dont have it set
 *
 * Then, how much combinations are there so that there are an even 
 * number of times used?
 * It is 0, 2, 4,... usages.
 * So, y... :/
 *
 *
 * ****
 * After some hours of work...
 * Note that we only need to output the cozyness, not a sequence :/
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cini(m);

    vector<pii> lr(m);
    stree1 seg1(n);
    vi l(m);
    vi r(m);
    vi x(m);
    for(int i=0; i<m; i++) {
        cin>>l[i]>>r[i]>>x[i];
        l[i]--;
        r[i]--;

        seg1.apply(l[i], r[i]+1, x[i]);
    }

    vi data(n);
    for(int i=0; i<n; i++) 
        data[i]=seg1.get(i);

    stree2 seg2(data);

    for(int i=0; i<m; i++) {
        seg2.apply(l[i], r[i]+1, x[i]);
    }

    vi cnt(31);

    /** now calc coziness */
    for(int i=0; i<n; i++)  {
        data[i]=seg2.get(i);
        for(int j=0; j<31; j++) {
            if(data[i]&(1LL<<j))
                cnt[j]++;
        }
    }

    const mint TWO=2;

    mint ans=0;
    for(int i=0; i<n; i++) {
        mint b=1;
        for(int j=0; j<31; j++) {
            if((cnt[j]%2==1) && (data[i]&(1LL<<j))) {
                ans+=b*TWO.pow(n-1);
            }
        }
        b*=2;
    }
    cout<<ans.val()<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
