/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Bad statement, hard to understand.
 */
void solve() {
    cini(n);

    vector<pii> p(n);
    for(int i=0; i<n; i++) {
        cin>>p[i].first;
        p[i].second=i;
    }
    sort(all(p), greater<pii>());


    int ans=0;
    for(int i=0; i<n; i++) {
        int dist=(i/2)+1;
        ans+=dist*p[i].first*2;
        p[i].first=(i/2)+1;
        if(i&1)
            p[i].first*=-1;
        swap(p[i].first,p[i].second);
    }
    sort(all(p));
    cout<<ans<<endl;
    cout<<0<<" ";
    for(int i=0; i<n; i++) 
        cout<<p[i].second<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
