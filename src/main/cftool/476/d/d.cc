/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* What constraints are there so that gcd(i,j)==k it true for all pairs?
 * -all a[i] must be multiples of k
 *     so let the 4 numbers be of form m[i]*k
 * -all m[i] must be coprime since the gcd is k*gcd(m[i],m[j])
 *
 *  So it is asked to find n sets of 4 coprime numbers.
 *
 * If we want only one set, it is (1,2,3,5)
 * So, to get two sets that would result in
 * 1,2,3,5
 * 6,7,9,11
 * 12,13,15,17
 *
 * ...
 * Seems to be greedy, Use one even number in each set,
 * and the next 3 primes available.
 * ...That does not work out, because we should use the 9 in second example,
 * but do not because 9 is not prime.
 *
 * So go more greedy:
 * Use next even number
 * check next available odd numbers
 * ***
 * See tutorial, we need to choose a better even number,
 * then allways the next 3 odd numbers work, That results in
 * 6*a+1, 6*a+2, 6*a+3, 6*a+5
 */
const int N=2e5;
void solve() {
    cini(n);
    cini(k);

    vvi ans(n, vi(4));
    ans[0]={ 2, 1, 3, 5 };

    int odd=7;

    for(int i=1; i<n; i++) {
        ans[i][0]=odd+1;
        ans[i][1]=odd;
        ans[i][2]=odd+2;
        ans[i][3]=odd+4;
        odd+=6;
    }

    cout<<(odd-2)*k<<endl;
    for(int i=0; i<n; i++)  {
        for(int j=0; j<4; j++) 
            cout<<ans[i][j]*k<<" ";
        cout<<endl;
    }
}

signed main() {
    solve();
}
