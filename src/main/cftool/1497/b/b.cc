/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* find longest m-diviseable sequences in a ?
 * greedy construct the subarrays.
 **/
const int N=1e5+7;
void solve() {
    cini(n);
    cini(m);

    vi f(N);
    
    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux%m]++;
    }

    int ans=0;
    if(f[0])
        ans=1;

    if(m%2==0 && f[m/2]) {
        ans++;
        f[m/2]=0;
    }

    for(int i=1; i<m; i++) {
        int cnt=0;
        while(f[i]>0 && f[m-i]>0) {
            cnt=1;
            f[i]--;
            f[m-i]--;
        }
        if(f[i]) {
            cnt=1;
            f[i]--;
        } else if(f[m-i]) {
            cnt=1;
            f[m-i]--;
        }
        ans+=cnt;
        ans+=f[i];
        ans+=f[m-i];
        f[i]=f[m-i]=0;
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--) 
        solve();
}
