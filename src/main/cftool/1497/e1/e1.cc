/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, map<int,int> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization[d]++;
            n /= d;
        }
    }
    if (n > 1)
        factorization[n]++;
}

/*
 * Not all sums of divisors is even.
 * Consider having the divisors of all numbers.
 * So make odd number of divisors 1, even 0.
 * If any of these masks are there twice, we got 
 * a square product.
 * Unfortunatly it is like 1e6 different prime factors...
 * but still we can use a map of them to collect in a set.
 */
const int N=1e7+7;
void solve() {
    cini(n);
    cini(ign);

    int ans=1;
    set<vi> s;
    for(int i=0; i<n; i++) {
        cini(a);
        map<int,int> f;
        trial_division4(a,f);
        vi f0;
        for(auto ent : f)
            if(ent.second%2==1)
                f0.push_back(ent.first);

        if(s.count(f0)>0) {
            ans++;
            s.clear();
        }
        s.insert(f0);
    }

    cout<<ans<<endl;
}

signed main() {
    assert(pr.back()>N);
    cini(t);
    while(t--)
        solve();
}
