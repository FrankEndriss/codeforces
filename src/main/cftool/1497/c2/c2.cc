/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}
();

/* LCM<=n/2
 * -> product of all prime divisors must not be bigger n/2
 * -> we may use primefactors multiple times by
 *  using them in more than one number (or not at all).
 *
 *  But we propably can use other/bigger primefactors,
 *  by notusing some primefactors.
 *  ie if 2*3*5*7 <=n/2,
 *  mayby also 2*11<=n/2
 *
 * Simplest solution is that n%3==0,
 * then n/3, n/3, n/3
 *
 * n%3=1
 * -> 1, (n-1)/2, (n-1)/2
 *
 * n%3==2 ???
 *
 * 3: 1 1 1
 * 4: 2 1 1
 * 5: 2 2 1
 * 6: 2 2 2
 * 7: 3 3 1
 * 8: 4 2 2
 * 9: 4 4 1
 *10: 4 4 2
 *11: 5 5 1
 *
 * Now what to do for bigger k?
 * Since ans allways exists, we can somehow construct
 * it for bigger k.
 * Use all 1s.
 * if k<n, use some 2.
 * if k still smaller, use some 4, if possible.
 * else use some 3
 * Construct n with powers of 2.
 *
 * ...
 * k<=1e5
 * ...
 * What about 
 * 31 4?
 * 10 10 10 1
 */
void solve() {
    cini(n);
    cini(k);    // 3

    if(k==3) {
        if(n%4==0) {
            cout<<n/2<<" "<<n/4<<" "<<n/4<<endl;
        } else if(n%3==0) {
            cout<<n/3<<" "<<n/3<<" "<<n/3<<endl;
        } else {
            cout<<(n-1)/2<<" "<<(n-1)/2<<" ";
            int nn=n-(n-1)/2;
            nn=nn-(n-1)/2;
            cout<<nn<<endl;
        }
        return;
    }

    for(int i=1; i+1<k; i++) {
        if((n-i)%(k-i)==0) {
            for(int j=0; j<i; j++) 
                cout<<1<<" ";
            for(int j=0; j<k-i; j++) 
                cout<<(n-i)/(k-i)<<" ";
            cout<<endl;
            return;
        }
    }

    while(k) {
        if(k==1) {
            cout<<n<<endl;
            break;
        }
        int num=1;
        while(num*2+k<=n)
            num*=2;
        cout<<num<<" ";
        n-=num;
        k--;
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
