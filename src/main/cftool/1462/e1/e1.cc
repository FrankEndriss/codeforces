/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 *
 * Observe that order of a[] does not matter, so we
 * can sort it.
 * Then use two pointer to find size of segments with
 * max dist k, and sum up all segments.
 *
 * This is, foreach position we choose the left element
 * as the first in segment, and use m-1 from the other available
 * elements.
 *
 * ...
 * E1 harder than E2 since we cannot use standard nCr-function... sic.
 */
void solve() {
    cini(n);
    int m=3;
    int k=2;

    cinai(a,n);
    sort(all(a));

    int r=0;
    int ans=0;
    for(int l=0; l<n; l++) {
        while(r<n && a[r]-a[l]<=k)
            r++;

        if(r-l>=m)
            ans+=(r-l-1)*(r-l-2)/2;
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
