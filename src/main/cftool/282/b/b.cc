/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Greedy:
 * Sort eggs by distance to 500/500
 * Add bigger distances first, allways
 * on that end whis is better.
 */
void solve() {
    cini(n);

    int suma=0;
    int sumg=0;
    string ans;
    for(int i=0; i<n; i++) {
        int a,g;
        cin>>a>>g;

        int ad=abs(suma+a-sumg);
        int gd=abs(suma-(sumg+g));
        if(ad<=gd) {
            ans.push_back('A');
            suma+=a;
        } else {
            ans.push_back('G');
            sumg+=g;
        }
    }

    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

