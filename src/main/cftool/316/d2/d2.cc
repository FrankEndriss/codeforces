/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Number of possible outcome permutations.
 * For N up to 10 brute force should be possible.
 *
 * Note that the order of studends does not matter, so 
 * we only need the freq of 1s and 2s
 * And since they depend on each other, only the 2s will do.
 *
 * Examples:
 * 1 1 -> 2: 1 noswap and 1 swap
 * 1 1 1 -> 4: 1 noswap and 3 different pair can swap
 * 1 1 1 1 -> 1+6*2: 1 noswap and 6 different pairs can swap and then the last two can swap or notswap
 *
 * 1 2 -> 2
 * 2 2 -> 2
 * 1 1 2 -> possible swap seqs are:
 * 0,1;
 * 0,2;1,2;
 * 1,2;0,1;
 *
 * Note that 
 * on each swap the numbers are decreased by 2, so at most sum/2 swap 
 * can be done.
 * With only 1s a swapped ball ends up somewhere else than it starts.
 * Consider 12+1, they can do a circle swap.
 *
 * We need to find the possible number of combinations of circles, and each 
 * cirlce can stay unchanged or go in two directions. But if circle length==2 both directions
 * yield the same result.
 *
 * ...idk
 */
const int MOD=1e9+7;
void solve() {
    cini(n);
    int cnt=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux==2)
            cnt++;
    }


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
