/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/* see https://cp-algorithms.com/data_structures/disjoint_set_union.html
 * Also known as union find structure. 
 * Note that the above link has some optimizations for faster combining of sets if needed. 
 */

struct Dsu {
    vector<int> p;
    vector<int> s;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        s.resize(size, 1);
        iota(p.begin(), p.end(), 0);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members. 
 * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            s[a]+=s[b];
        }
    }
};

/*
 * 'a'-'t', 20 bit
 *
 * if B has 'a' A not has no sol
 * if B has 'b' A not has, then we must take...
 *
 * for each letter check all positions of that letter in B[].
 * All A[] there need to be lower in alphabet, and
 * if at least one lower in alphabet we need one move.
 *
 * On each move we move all letters of previous kind to current letter,
 * so we need only the number of moves of letters existing
 * greater or equal since the last move.
 *
 */
void solve() {
    cini(n);
    cins(A);
    cins(B);

    Dsu dsu('t'-'a'+1);

    for(int i=0; i<n; i++)  {
        if(A[i]>B[i]) {
            cout<<-1<<endl;
            return;
        }
        dsu.union_sets(A[i]-'a', B[i]-'a');
    }

    set<int> ss;
    for(char c : A)
        ss.insert(dsu.find_set(c-'a'));

    int ans=0;
    for(int i : ss)
        ans+=dsu.s[i]-1;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
