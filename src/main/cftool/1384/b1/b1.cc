/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * In B1 iterating K should be possible since low constraints.
 *
 * At each position we got at every tide an interval where
 * this position is "ok".
 * Some intervals are complete, ie they are like land.
 * We need to find from each such land a path to the next land.
 *
 * Each position can be entered earliest at 
 * max(t+1 from the prev position, begin of ok period)
 * and must be left at some later point of time.
 *
 * So we maintain these positions in term distance from highest point of 
 * tide, which is first time at t=k.
 *
 * So, go from left to right, and foreach position find the earliest
 * t from lowest tide point where we can be at that point.
 *
 * This finds a passage or not.
 */
void solve() {
    cini(n);
    cini(k);
    cini(l);

    //cerr<<"tc "<<n<<" "<<k<<" "<<l<<endl;

    int mi=0;
    int ma=2*k;

    bool ans=true;
    for(int i=0; i<n; i++) {
        cini(d);
        d+=k;   /* translate from lowest waterlevel to highest waterlevel */

        if(d<=l) { //  like land
            mi=0;
            ma=2*k;
        } else {
            int mi0=d-l;
            int ma0=2*k-(d-l);
            if(mi0>ma0)
                ans=false;

            if(mi0 > ma+1)
                ans=false;

            mi=max(mi+1, mi0);
            ma=ma0;

            if(mi>ma)
                ans=false;
        }

        //cerr<<" after step "<<i<<" mi="<<mi<<" ma="<<ma<<endl;
    }

    if(ans)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
