/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to check from last to first monster.
 * h[i] is the minimum power we need to use at time k[i]
 * So we go from monster to monster.
 * At each monster we need h[i]*(h[i]-1), or we had to increase the previous 
 * monsers cost.
 * ....
 * No, more complecated.
 * Foreach monster there are two cases:
 * -prev and next monster are out of reach, the current monster simply costs h[i]*(h[i]+1)/2
 * -the prev monsters h[i] is above the line starting from 1 to the current monster,
 *  so we had to extend the prev monsters costs.
 *  So current monster cost td*h[i-1]+ td*(td+1)/2
 * -the prev monster is below the line of the current monster, so the prev monster does not cost anything.
 *
 * Go from right to left checking the cases.
 * ***
 * If two monsters "overlap", we can create one monster instead.
 * Do this at the end of the stack.
 */
void solve() {
	cini(n);
	vector<pii> m(n);
		/* seconds, distinct */
	for(int i=0; i<n; i++)
		cin>>m[i].first;
	/* health , h[i]<=k[i] */
	for(int i=0; i<n; i++)
		cin>>m[i].second;

	sort(all(m));	/* sort by seconds */

	int ans=0;
	while(m.size()) {
		const int bk=(int)m.size()-1;
		if(bk>0) {
			int td=m[bk].first-m[bk-1].first;
			//int hd=m[bk].second-m[bk-1].second;

			if(m[bk].second>td) {
				m[bk-1].first=m[bk].first;
				m[bk-1].second=max(m[bk].second, m[bk-1].second+td);
				m.pop_back();
				continue;
			}

		}
		ans+=m[bk].second*(m[bk].second+1)/2;
		m.pop_back();
	}

	cout<<ans<<endl;

}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
