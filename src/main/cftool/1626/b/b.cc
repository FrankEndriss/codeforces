/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Casework:
 * find a sum of two digits with s>=10
 * From all those use the rightmost one,
 * since the reduced one is allways smaller than the orig.
 *
 * if none available then reduce the leftmost pair.
 */
void solve() {
	cins(s);
	if(s.size()==2) {
		int sum=s[0]+s[1]-'0'-'0';
		cout<<sum<<endl;
		return;
	}

	for(size_t i=0; i+1<s.size(); i++) {
		int sum=s[s.size()-i-1]+s[s.size()-i-2]-'0'-'0';
		if(sum>=10 ) {
			s[s.size()-i-2]='1';
			s[s.size()-i-1]='0'+sum-10;
			cout<<s<<endl;
			return;
		}
	}


	int sum=s[0]+s[1]-'0'-'0';
	s[0]=' ';
	s[1]=('0'+sum);
	cout<<s<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
