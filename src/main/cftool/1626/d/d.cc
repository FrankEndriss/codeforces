/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int N=4e5+7;
const int INF=1e18+7;

vi dp(N);	/* number of necessary invitations for a group of i participant */

void init() {
vi logT(N);
vi cnt(N);
	logT[0]=0;
	logT[1]=0;
	cnt[0]=0;
	cnt[1]=1;
	dp[0]=1;
	dp[1]=0;
	for(int i=2; i<N; i++) {
		logT[i]=logT[i/2]+1;
		cnt[i]=1<<(logT[i-1]+1);
		dp[i]=cnt[i]-i;
		//if(i<20)
		//	cerr<<"i="<<i<<" logT[i]="<<logT[i]<<" cnt[i]="<<cnt[i]<<" dp[i]="<<dp[i]<<endl;
	}

}

/**
 * 3 div, 3 winners
 *
 * Each division 2^x, min 1
 * Note that min number of members in divL
 * is freq of smallest weight, same for heaviest weight.
 *
 * Consider 1 pers in L, 1 in H, then n-2 in M, so 
 *
 * If we fix the left group, then we somehow want to binsearch
 * the best sizes of the other two.
 * Brute force would be simpler :/
 *
 * Think about greedy...
 * We want two big groups with cnt=2^x, and a small group.
 *
 * Consider haveing the two big groups of size of the two
 * biggest bits in n.
 * Small group size the smaller bits.
 * But we cannot create each size of groups, since weights are not distinct. :/
 * Note that n+2 is allways possible.
 *
 * So find forach ans if it is possible?
 * Note that number of final paticipants is allways cnt=2^x + 2^y + 2^z,
 * and x,y,z are determined. ans=cnt-n
 *
 * How to quickly check if x,y,z is possible?
 * -> check all 6 permutations, if they fit the given numbers of weights.
 * ******
 * No.
 * We want to construct sums of 3 1-bit numbers, that are the final sizes of the three groups.
 * There are only ~15^3 such triples.
 * Foreach one we want to find if it is possible to put all partitipants into that 
 * groups.
 * Do this by binary search on the prefix sums of numbers of partitipants in the
 * sorted list of weights.
 */
int bcnt(int i) {
	int ans=0;
	while(i) {
		if(i&1)
			ans++;
		i/=2;
	}
	return ans;
}

/*
 * still some WA.
 * But the principle is correct, just try all possible group sizes.
 * And binary search the costs on the prefix sums of possible number of participants.
 */
void solve() {
	cini(n);
	cinai(a,n);

	map<int,int> m;
	for(int i=0; i<n; i++) {
		m[a[i]]++;
	}

	vi pre(1);	/* possible numbers of participants in first or firsts groups */
	for(auto ent : m) {
		pre.push_back(pre.back()+ent.second);
	}

	int ans=dp[n]+2;
	//cerr<<"initial ans="<<ans<<endl;

	vi c(3);
	for(c[0]=1; c[0]<=n; c[0]*=2) {	/* size of group L */
		auto it1=upper_bound(all(pre), c[0]);	/* participants group L */
		it1--;
		int lans=dp[*it1];
		//cerr<<"fi group lans="<<lans<<" participants group L="<<*it1<<endl;

		for(c[1]=1; c[1]<=n; c[1]*=2) {	/* size of group M */
			auto it2=upper_bound(all(pre), (*it1)+c[1]);
			it2--;

			lans+=dp[(*it2)-(*it1)];
			//cerr<<"se group lans="<<lans<<" partitipants group M="<<(*it2)-(*it1)<<endl;

			c[2]=max(0LL, n-(*it2));
			lans+=dp[c[2]];
			//cerr<<"lans="<<lans<<endl;

			ans=min(ans, lans);
		}
	}

	cout<<ans<<endl;
}

signed main() {
	init();
	cini(t);
	while(t--)
    		solve();
}
