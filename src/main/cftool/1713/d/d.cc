/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/**
 * Consider brute force ask for all pairs:
 * 1st round there are 2^(n-1) matches, which is (2^(n+1))/4,
 * so still 2^(n-1) available
 *
 * Consider n=2, so there are 4 contestants, and there is
 * (8+2)/3=3 queries.
 * Since there are only 3 matches, we can query them all.
 *
 * Consider n=3, 8 co
 * (16+2)/3=6 queries.
 * But here are 4+2+1=7 matches, so we need to get rid on one at least.
 *   Consider matches 1,2 and 3,4, and outcome q(1,3):
 *    if 1 is better than 3 then 1 won his match.
 *      Then ask 1,4 to know the winner of them 4.
 *    if 3 is better than 1 then 3 won his match.
 *      Then ask 3,2 to knwo the winne of them 4.
 *    Same means both have lost.
 *      Then ask 2,4 to know the winner of them 4.
 */
int ask(int a, int b) {
    cout<<"? "<<a+1<<" "<<b+1<<endl;
    int ans;
    cin>>ans;
    if(ans<0)
        exit(1);
    return ans;
}

/*
vi dat={ 1,0,0,2,0,1,3,0 };
int ask(int a, int b) {
    cout<<a+1<<" "<<b+1<<endl;
    cerr<<"a="<<a<<" b="<<b<<" dat[a]="<<dat[a]<<" dat[b]="<<dat[b]<<endl;
    if(dat[a]>dat[b])
        return 1;
    else if(dat[a]<dat[b])
        return 2;
    else 
        return 0;
}
 */

void solve() {
    cini(n);

    vi p(1<<n);
    iota(all(p), 0LL);

    while(true) {
        if(p.size()==1) {
            cout<<"! "<<p[0]+1<<endl;
            return;
        } else if(p.size()==2) {
            int ans=ask(p[0],p[1]);
            if(ans==1)
                cout<<"! "<<p[0]+1<<endl;
            else if(ans==2)
                cout<<"! "<<p[1]+1<<endl;
            else
                assert(false);
            return;
        }

        vi p0;
        for(size_t i=0; i<p.size(); i+=4) {
            int ans=ask(p[i], p[i+2]);
            if(ans==1)  {
                int ans1=ask(p[i], p[i+3]);
                if(ans1==1)
                    p0.push_back(p[i]);
                else if(ans1==2)
                    p0.push_back(p[i+3]);
                else
                    assert(false);
            } else if(ans==2) {
                int ans1=ask(p[i+1],p[i+2]);
                if(ans1==1)
                    p0.push_back(p[i+1]);
                else if(ans1==2)
                    p0.push_back(p[i+2]);
                else
                    assert(false);
            } else if(ans==0) {
                int ans1=ask(p[i+1],p[i+3]);
                if(ans1==1)
                    p0.push_back(p[i+1]);
                else if(ans1==2)
                    p0.push_back(p[i+3]);
                else
                    assert(false);
            }
        }
        assert(p0.size()*4==p.size());
        p=p0;
    }

    assert(false);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
