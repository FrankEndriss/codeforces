/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A square is a square because each prime factor is there even times.
 * We have each number twice, and have to build pairs
 * Biggest possible square is sqrt(n*2)
 *
 * 1, 4, 9, 16
 *
 * 0,1,2
 * 2,1,0
 * Does 0 count as a square? -> I guess yes
 *
 * 0,1,2,3
 * 0,3,2,1
 *
 * 0,1,2,3,4
 * 4,3,2,1,0
 *
 * 0,1,2,3,4,5
 * 0,3,2,1,5,4
 *
 * if n is a square, it trivial, reverse.
 * Else the distance to the next square is < n/2, ie 8<17/2
 *
 * Note there are even squares and odd squares.
 *
 * ....
 * Greedy go from biggest to smallest, allways use smallest possible other number.
 * 0,...,17
 * 17,16,15,14,13,7,6,5,1,0
 *  8, 9,10,11,12,2,3,5,1,0
 *
 * But that fails on test2.
 * So, there seem to be some other logic...
 * That prob feels like christmas riddle, no programming problem at all.
 * What is the point here?
 *
 * ...according to the number of sols there is a trivial solution :/
 */
const int N=1e5+7;
vi sq= {0,1};

void solve() {
    cini(n);

    vb vis(n*2);
    vi ans(n);
    for(int i=n-1; i>=0; i--) {
        if(vis[i])
            continue;

        auto it=lower_bound(all(sq), i);
        int l=(*it)-i;
        assert(l>=0 && l<=i);

        int l2=l;
        for(int j=i; j>=l; j--) {
            ans[j]=l2;
            l2++;
            vis[j]=true;
        }
    }

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;


}

signed main() {
    while(sq.back()<N+N)
        sq.push_back(sq.size()*sq.size());

    cini(t);
    while(t--)
        solve();
}
