/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

#define int ll

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Observation: We need to use the biggest possible r1 to get the biggest
 * possible r2.
 * Then we should use the biggest density of p1, and the smallest density of p2.
 * For these vals calculate r2, maybe by binsearch, or quadratic closed formula.
 */
void solve() {
    cini(n);
    cinai(x,n);
    cini(m);
    cinai(y,m);
    cini(k);
    cinai(z,k);
    cini(a);
    cini(b);

    double r1=*max_element(x.begin(), x.end());
    double p1=*max_element(y.begin(), y.end());
    double p2=*min_element(z.begin(), z.end());

    double ab=(1.0*a)/b;

/* ((r1*r1 - r2*r2)*p1) / (r2*r2*p2) == ab
 **/
    double l=0;
    double r=r1;
    while(l+0.000000001<r) {
        double r2=(l+r)/2;
        double val=((r1*r1 - r2*r2)*p1) / (r2*r2*p2);
        if(val<ab) 
            r=r2;
        else
            l=r2;
    }
    cout<<setprecision(12)<<fixed<<l<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

