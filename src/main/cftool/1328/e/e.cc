/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;


/* LCA based on euler path with segment tree and tin/tout. */
struct LCA {
    vector<int> height, euler, first, segtree;
    vector<bool> visited;
    int n;
    int time;
    vector<int> tin;
    vector<int> tout;
    vector<int> parent;

    LCA(vector<vector<int>> &adj, int root = 0) {
        n = (int)adj.size();
        height.resize(n);
        first.resize(n);
        euler.reserve(n * 2);
        visited.assign(n, false);
        tin.resize(n);
        tout.resize(n);
        parent.resize(n);
        time=1;
        dfs(adj, root);
        int m = (int)euler.size();
        segtree.resize(m * 4);
        build(1, 0, m - 1);
    }

    void dfs(vector<vector<int>> &adj, int node, int h = 0) {
        tin[node]=time++;
        visited[node] = true;
        height[node] = h;
        first[node] = (int)euler.size();
        euler.push_back(node);
        for (auto to : adj[node]) {
            if (!visited[to]) {
                parent[to]=node;
                dfs(adj, to, h + 1);
                euler.push_back(node);
            }
        }
        tout[node]=time++;
    }

    void build(int node, int b, int e) {
        if (b == e) {
            segtree[node] = euler[b];
        } else {
            int mid = (b + e) / 2;
            build(node << 1, b, mid);
            build(node << 1 | 1, mid + 1, e);
            int l = segtree[node << 1], r = segtree[node << 1 | 1];
            segtree[node] = (height[l] < height[r]) ? l : r;
        }
    }

    int query(int node, int b, int e, int L, int R) {
        if (b > R || e < L)
            return -1;
        if (b >= L && e <= R)
            return segtree[node];
        int mid = (b + e) >> 1;

        int left = query(node << 1, b, mid, L, R);
        int right = query(node << 1 | 1, mid + 1, e, L, R);
        if (left == -1) return right;
        if (right == -1) return left;
        return height[left] < height[right] ? left : right;
    }

    /* @return true if anc is ancestor of node in O(1) */
    bool isAncestor(int node, int anc) {
        return tin[node]>tin[anc] && tin[node]<tout[anc];
    }

    /* @return the lca of u and v */
    int lca(int u, int v) {
        int left = first[u], right = first[v];
        if (left > right)
            swap(left, right);
        return query(1, 0, (int)euler.size() - 1, left, right);
    }
};

/* given deepest node of group path to root.
 * all other nodes in group must have all 
 * parents in that path, else not ok.
 *
 * We propably should reorder the queries and
 * sort by deepest node.
 * So, if one query runs, and deepest node is in a path
 * seen before, we do not need to create this path
 * again.
 */
void solve() {
    cini(n);
    cini(q);

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        u--; v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    LCA lca(adj);

    for(int i=0; i<q; i++) {
        cini(k);
        vi v(k);

        int depnode=-1;
        int malvl=-1;

        for(int j=0; j<k; j++) {
            cin>>v[j];
            v[j]=lca.parent[v[j]-1];

            if(lca.height[v[j]]>malvl) {
                depnode=v[j];
                malvl=lca.height[v[j]];
            }
        }

        bool ok=true;
        for(int anc : v) {
            if(anc !=depnode && !lca.isAncestor(depnode, anc)) {
                ok=false;
                break;
            }
        }

        if(!ok)
            cout<<"NO"<<endl;
        else
            cout<<"YES"<<endl;
        
    }

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

