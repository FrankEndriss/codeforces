/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Need to get k same elements.
 * We got two greedys. Increase from bottom or decrease from top.
 * Can it be required to do both? Yes. In this case there will be
 * n-k elements in distance 1 to the meeting point.
 */
void solve() {
    cini(n);
    cini(k);
    map<int,int> f;

    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux]++;
    }

    vector<pii> a;
    for(auto ent : f) {
        a.emplace_back(ent.first,ent.second);    // num,times
        if(ent.second>=k) {
            cout<<0<<endl;
            return;
        }
    }
    f.clear();

    int ansL=0;
    int cnt=a[0].second;
    int i=1;
    while(cnt<k) {
        int nelem=min(k-

        ansL+=
    }

    cout<<ansL<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

