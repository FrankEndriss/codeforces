/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * f(A)=(maxRowsum-minRowsum)^2 + (maxColsum-minColsum)^2
 *
 * a has k 1s
 *
 * Minimize f(A)
 *
 * We need to distribute 1 evenly as possible among rows
 * and among cols.
 */
void solve() {
    cini(n);
    cini(k);

    vvi a(n, vi(n));

    int k1=k/n;
    assert(k1<=n);
    int k2=k%n;

    int i=0;
    for(int kk=0; kk<k2; kk++) {
        for(int kkk=0; kkk<k1+1; kkk++) {
            a[kk][i%n]=1;
            i++;
        }
    }

    for(int kk=k2; kk<n; kk++) {
        for(int kkk=0; kkk<k1; kkk++) {
            a[kk][i%n]=1;
            i++;
        }
    }

    int ans=0;
    if(k%n!=0 && n>1)
        ans=2;

    cout<<ans<<endl;
    for(int ii=0; ii<n; ii++) {
        for(int jj=0; jj<n; jj++)
            cout<<a[ii][jj];
        cout<<endl;
    }

    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
