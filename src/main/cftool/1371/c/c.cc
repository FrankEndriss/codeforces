/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* First type vani if vani>choc
 * Secon type vani if vani<=choc
 *
 * First type eats what is more avail!
 * Second type eats what is less avail!
 * or vani.
 *
 * So when second comes both must be avail
 *
 * Note only second type can get angry, because
 * if there is no vani but choch he chooses
 * vani but cannot eat it.
 *
 * So send second type guests first, they will eat
 * choc until choc is less than vani, than a vani,
 * then again choc until none avail.
 *
 * Cases:
 * m<=a -> allright, the m eats the a, and n eats some.
 *
 * m>a ? If vani<choc we cannot send the m guests first, since they would
 *       eat all vani until no vani avail.
 *       But we can send n-guest first until vani==choc, then m-guest will
 *       eat one choc and vani alternating.
 *
 */
void solve() {
    cini(a);    /* vani */
    cini(b);    /* choc */
    cini(n);    /* first type */
    cini(m);    /* second type */

    if(m>min(a,b)) {
            cout<<"No"<<endl;
            return;
    }

    if(n+m<=a+b)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;

    
}

signed main() {
    cini(t);
    while(t--)
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
