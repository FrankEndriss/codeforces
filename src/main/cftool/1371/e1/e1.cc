/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * n enemies have a[i] candies
 *
 * fucking math shit...annoying!
 *
 * If starting with a number x a number of permutations
 * exists so that it is win.
 *
 * Count existing x which are not div by p
 * Note that for big x the number of perms is n!,
 * and that is divisable by all numbers<p, so by p, too.
 *
 * Min possible x==1, that is if a[i]={ 1, 2, 3,... }
 *
 * Weakest enemies first.
 * Every enemie has a lowest position where it can be placed,
 * which determines the number of possible positions.
 * So assume that number is used as multiplicator to calc the possibilities.
 *
 * How to calc number of permutations if every element has a
 * limited number of possible positions?
 *
 * Let lim(i) be the number of posible positions of elem i, then
 * sort by lim(i) asc, and sum is
 * lim(i) * lim(i+1)-1 * lim(i+2)-2 * ... and if that becomes zero somewhere
 * that x is impossible.
 *
 */
void solve() {
    cini(n);
    cini(p);    /* prime <2000*/

    cinai(a,n);
    sort(all(a));

    vi ans;
    for(int x=1; x<=n; x++) {
        bool ok=true;
        vi lim;
        for(int i=0; ok && i<n; i++) {
            if(x+i<a[i]) {  /* this x is impossible */
                ok=false;
            }
            lim.push_back(min(n, n-(a[i]-x)));
        }

        if(!ok)
            continue;

        sort(all(lim));
        for(int i=0; ok && i<n; i++) {
            if(lim[i]-i<=0 || (lim[i]-i)%p==0)
                ok=false;
        }
        if(ok)
            ans.push_back(x);
    }

    cout<<ans.size()<<endl;
    if(ans.size()>0) {
        for(int i : ans)
            cout<<i<<" ";
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
