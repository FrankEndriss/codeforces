/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

bool issorted(vi &a) {
    vi b=a;
    sort(all(b));
    return a==b;
}

/**
 * Number of inversion changes by 2 or 0.
 * 1 2 3 : 0
 * -> 2 3 1 : 2
 * -> 3 1 2 : 2
 *
 * 2 1 3 : 1
 * -> 1 3 2 : 1
 * -> 3 2 1 : 3
 *
 * 2 3 1 : 2
 * -> 3 1 2 : 2
 * -> 1 2 3 : 0
 *
 * What about double entries?
 * -> If there is a double entry it is allways sortable,
 *  since we can simply swap.
 */
void solve() {
    cini(n);
    cinai(a,n);

    if(issorted(a)) {
        cout<<"YES"<<endl;
        return;
    }

    vi b=a;
    sort(all(b));
    for(int i=0; i+1<n; i++) {
        if(b[i]==b[i+1]) {
            cout<<"YES"<<endl;
            return;
        }
    }

    stree seg(n+1);

    int inv=0;
    for(int i=0; i<n; i++) {
        inv+=seg.prod(a[i]+1,n+1);
        seg.set(a[i], seg.get(a[i])+1);
    }


    if(inv%2==0)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
