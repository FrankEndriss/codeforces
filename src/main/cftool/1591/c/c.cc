/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It is allways optimal to take all k bags
 * if we are at origin.
 *
 * So order of left/right does not matter.
 *
 * consider right half
 * So go to the outmost (at idx=0) and cover k positions,
 * Then fetch new bags and repeat
 * to the outermost  (at idx=k) and cover max k positions...
 *
 * ****
 * No. We need to go to the outermost, deliver k bags,
 * then return.
 * On last trip there is no need to return on second half.
 *
 * ***
 * What a horrible implementation problem.
 * Something wrong, idk :/
 */
void solve() {
    cini(n);
    cini(k);

    cinai(a,n);

    int ans=0;

    vi x;
    int zero=0;
    for(int i=0; i<n; i++) {
        if(a[i]>0)
            x.push_back(a[i]);
        else if(a[i]==0)
            zero++;
    }

    int ma=0;
    if(x.size()) {
        sort(all(x),greater<int>());

        for(int idx=0; idx<ssize(x); idx+=k)
            ans+=x[idx]*2;

        if(x.size()+zero==a.size()) {   /* no negatives */
            ans-=x[0];
            cout<<ans<<endl;
            return;
        }
        ma=x[0];
    }

    x.clear();

    for(int i=0; i<n; i++)
        if(a[i]<0)
            x.push_back(-a[i]);

    sort(all(x),greater<int>());

    for(int idx=0; idx<ssize(x); idx+=k)
        ans+=x[idx]*2;
    if(x.size()>0) {

    assert(x.size()>0);
    ans-=max(ma,x[0]);
    } else
        ans-=ma;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
