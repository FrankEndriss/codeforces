/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * we need to find an order where
 * the position of each student i is so that pos%3==a[i]%3
 * and output in order of a[i]
 * We need to check that every a[i] is lteq position.
 */
void solve() {
    cini(n);
    vector<set<pii>> a(3);
    vi cnt(3);
    for(int i=0; i<n; i++) {
        cini(aux);
        a[aux%3].insert({-aux,i+1});
    }

    int mod=n%3;
    int a0=(mod>0);
    int a1=(mod>1);

    vector<pii> ans;
    int ok=false;
    if(a[0].sizen/3()+a1==a[1].size()+a0 && a[0].size()==a[2].size()+a0) {
        ok=true;
        for(int i=0; i<n/3; i++)  {
            auto it=a[0].lower_bound({-(i*3),-1});
            if(it!=a[0].end()) {
                ans.push_back({-it->first, it->second});
                a[0].erase(it);
            } else {
                ok=false;
                break;
            }

            it=a[1].lower_bound({-(i*3+1),-1});
            if(it!=a[1].end()) {
                ans.push_back({-it->first, it->second});
                a[1].erase(it);
            } else { 
                ok=false;
                break;
            }

            it=a[2].lower_bound({-(i*3+2),-1});
            if(it!=a[2].end()) {
                ans.push_back({-it->first, it->second});
                a[2].erase(it);
            } else {
                ok=false;
                break;
            }
        }

        if(ok && a[0].size()) {
            auto it=a[0].begin();
            ans.push_back({-it->first, it->second});
            a[0].erase(it);
        }

        if(ok && a[1].size()) {
            auto it=a[1].begin();
            ans.push_back({-it->first, it->second});
            a[1].erase(it);
        }

        int offs=0;

        if(ok)
            assert(ans.size()==n);

        for(int i=0; ok && i<n; i++) {
//cerr<<"ans[i]="<<ans[i]<<" i="<<i<<" ";
            if(ans[i].first+offs>i)
                ok=false;

            offs=max(offs, i-ans[i].first);
            assert((!ok) || offs%3==0);
        }
//cerr<<endl;
    }

    if(ok) {
        cout<<"Possible"<<endl;
        for(pii i : ans)
            cout<<i.second<<" ";
    } else
        cout<<"Impossible";

    cout<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
