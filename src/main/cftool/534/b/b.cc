/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int INF=1e9;

void solve() {
    cini(v1);
    cini(v2);
    cini(t);
    cini(d);

    vi ma(t, INF);

    ma[0]=v1;
    ma[t-1]=v2;
    for(int i=1; i<t; i++) {
        ma[i]=min(ma[i], ma[i-1]+d);
        ma[t-1-i]=min(ma[t-1-i], ma[t-i]+d);
    }

    int ans=0;
    for(int i=0; i<t; i++)
        ans+=ma[i];
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

