/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* when we can jump over x
 * then if the diff is even
 * we could have made the jump
 * of half the diff in the 
 * other direction.
 * If the diff is odd, we need one more jump.
 */
void solve() {
    cini(x);
    x=abs(x);

/*
    if(x==2) {  // corner case
        cout<<3<<endl;
        return;
    }
*/

    int ans=0;
    int len=0;
    int j=1;
    while(len<x) {
        len+=j;
        ans++;
        j++;
    }
    if((len-x)&1) {
        if(j&1)
            ans++;
        else
            ans+=2;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
