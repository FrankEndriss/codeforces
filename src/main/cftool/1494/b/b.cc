/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to check if we can color all corners.
 * sic casework.
 * ...
 * simply check all 16 cases!
 * ...still sic casework :/
 *
 * foreach side check if
 * black cells >= count black corners of that side
 * max white fields>=count white corners of that side
 * ...
 * Had the index of the corners wrong :/
 */
void solve() {
    cini(n);
    cinai(a,4);

    //cerr<<"n="<<n<<" case "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<endl;
    vi i(4);
    for(i[0]=0; i[0]<2; i[0]++) {   
    for(i[1]=0; i[1]<2; i[1]++) {
    for(i[2]=0; i[2]<2; i[2]++) {
    for(i[3]=0; i[3]<2; i[3]++) {
        bool ok=true;
        //cerr<<i[0]<<" "<<i[1]<<" "<<i[2]<<" "<<i[3]<<endl;
        for(int j=0; j<4; j++) {    /* the 4 sides */
            int blCorner=  i[(j+4)%4]+i[(j+1)%4];
            int whCorner=2-blCorner;

            if(a[j]<blCorner) {
                //cerr<<"a[j]<blCorner, j="<<j<<" blCorner="<<blCorner<<endl;
                ok=false;
                break;
            }
            if(a[j]+whCorner>n) {
                //cerr<<"a[j]+whCorner>n, j="<<j<<" blCorner="<<blCorner<<endl;
                ok=false;
                break;
            }
        }
        if(ok) {
            //cerr<<"ans "<<i[0]<<" "<<i[1]<<" "<<i[2]<<" "<<i[3]<<endl;
            cout<<"YES"<<endl;
            return;
        }
    }
    }
    }
    }

    cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
