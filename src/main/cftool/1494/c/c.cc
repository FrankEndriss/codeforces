/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

//#define endl "\n"

/* dp the number of covered fields...
 *
 * How to get the prefix sums right?
 * Number of positions is huge.
 * We need to maintain prefix sums for the given position, 
 * ie per index. Then build them with a two-pointer like loop.
 * ...Thats super error prone, off by one everywhere.
 * But, how can this be implemented less complecated?
 *
 * pre1 number of initially uncovered special fields
 * Then foreach pushing position p number of uncovered sp fields eq
 * ...see code below for how to add the prefix sums.
 * Do this for both halfes foreach position, add max of both halves.
 *
 * How to check all push positions for one half?
 * Number is again huge.
 * -> Allways push up to next b[j], that is allways optimal.
 *  Then we need to maintain the right end of the pushing blocks list, ie
 *  the number of currently pushed blocks. Again, two pointer like.
 */
void solve() {
    cini(n);
    cini(m);
    vvi a(2);
    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux<0)
            a[0].push_back(-aux);
        else
            a[1].push_back(aux);
    }
    reverse(all(a[0]));

    vvi b(2);
    for(int i=0; i<m; i++) {
        cini(aux);
        if(aux<0)
            b[0].push_back(-aux);
        else
            b[1].push_back(aux);
    }
    reverse(all(b[0]));

    //cerr<<"parsed input..."<<endl;
    
    int gans=0;
    for(int i=0; i<2; i++) {
        const int nn=a[i].size();
        const int mm=b[i].size();
        //cerr<<"first/second: "<<i<<" nn="<<nn<<" mm="<<mm<<endl;

        /* pre1[k+1]=number of initially uncovered special fields up to position b[k] */
        vi pre1(mm+1);
        int aidx=0;
        for(int bidx=0; bidx<mm; bidx++) {
            while(aidx<nn && a[i][aidx]<b[i][bidx]) 
                aidx++;

            if(aidx<nn && a[i][aidx]==b[i][bidx]) {
                pre1[bidx+1]=pre1[bidx];
            } else
                pre1[bidx+1]=pre1[bidx]+1;
        }

        int pushed=0;   /* number of currently pushed blocks */
        aidx=0;
        int ans=0;
        for(int j=0; j<mm; j++) {
            while(aidx<nn && b[i][j]+pushed>=a[i][aidx]) {
                pushed++;
                aidx++;
            }

            int cnt=lower_bound(all(b[i]), b[i][j]+pushed)-b[i].begin();
            int lans=mm-j-(pre1[mm]-pre1[cnt]);
            //cerr<<"lans="<<lans<<endl;
            ans=max(ans, lans);
        }
        gans+=ans;
    }
    cout<<gans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
