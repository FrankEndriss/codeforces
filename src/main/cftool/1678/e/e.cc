
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The two tapes create circles with the colors.
 * Consider a circle of size==2, we want to give
 * it 1,n, so its worth 2*(n-1)
 * Consider a circle of size==3:
 *   1,n; n,2; 2,n-1...
 *
 * Seems we can greedy choose from both ends ans so fill
 * all circles.
 *
 * Special case single circles.
 *
 * What about odd len circles?
 * consider 
 * x y z
 * z x y
 * x=1, z=n, y=2, so middle pair count only 1
 *
 */
void solve() {
    cini(n);

    cinai(a,n);
    vi posa(n);
    for(int i=0; i<n; i++)
        posa[a[i]-1]=i;

    cinai(b,n);
    vi posb(n);
    for(int i=0; i<n; i++)
        posb[b[i]-1]=i;

    int lridx=0;
    int l=1;
    int r=n;

    vi vala(n);
    vi valb(n);

    vb vis(n);
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;

        /* new circle begins */
        stack<int> st;
        int k=i;
        int start=k;   /* first pos */
        while(true) {
            st.push(k);
            assert(vis[k]==false);
            vis[k]=true;

            int pos=posb[a[k]-1];

            if(pos==start)
                break;

            k=pos;
        }

        if(st.size()==1)
            st.pop();
        else {
            while(st.size()) {
                int v=st.top();
                st.pop();
                if(lridx==0) {
                    vala[v]=l;
                    valb[posb[a[v]-1]]=l;
                    l++;
                } else {
                    vala[v]=r;
                    valb[posb[a[v]-1]]=r;
                    r--;
                }
                lridx=1-lridx;
            }
        }
    }

    int ans=0;
    for(int i=0; i<n; i++)  {
        ans+=abs(vala[i]-valb[i]);
    }
    /*
    for(int i=0; i<n; i++)
        cerr<<vala[i]<<" ";
    cerr<<endl;
    for(int i=0; i<n; i++)
        cerr<<valb[i]<<" ";
    cerr<<endl;
    */

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
