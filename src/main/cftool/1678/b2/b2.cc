
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Greedy change ends of segments. Note that
 * last segment allways fits, since n is even.
 *
 * Leftmost segment odd len must be made 1 longer.
 * Then, if next segment is len==1, make it shorter.
 * Ie, allways, if segment len==1, make it shorter.
 *
 * Version2:
 * Should be also greedy.
 * Why does "switch single segments" does not work?
 */
void solve() {
    cini(n);
    cins(s);
    string ss=s;

    function<void(int)> swi=[&](int i) {
        if(s[i]=='0')
            s[i]='1';
        else
            s[i]='0';
    };

    int ans=0;
    stack<int> st;
    char prev='x';
    for(int i=0; i<n; i+=2) {
        if(s[i]!=s[i+1]) {
            ans++;

            if(prev=='0' || prev=='1') {
                if(s[i]==prev)
                    swi(i+1);
                else
                    swi(i);
            } else {
                st.push(i);
            }
        } else {
            prev=s[i];
            while(st.size()) {
                int ii=st.top();
                st.pop();
                s[ii]=s[i];
                s[ii+1]=s[i];
            }
        }
    }
    while(st.size()) {
        int i=st.top();
        st.pop();
        s[i]=s[i+1]='1';
    }

    int ans2=1;
    for(int i=1; i<n; i++) 
        if(s[i]!=s[i-1])
            ans2++;
    cerr<<"ss="<<ss<<endl;
    cerr<<" s="<<s<<endl;
    cout<<ans<<" "<<ans2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
