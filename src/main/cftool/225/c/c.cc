/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

const int N=1001;
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cini(x);
    cini(y);

    vvi mm(2, vi(m));
    for(int i=0; i<n; i++) {
        cins(s);
        for(int j=0; j<m; j++)
            if(s[j]=='#')
                mm[0][j]++;
            else
                mm[1][j]++;
    }

/* dp[i][j][c]=min cost if start at col i haveing j cols of col c left of it.
 */
    vvvi dp(m+1, vvi(y+1, vi(2,-1)));

    function<int(int,int,int)> cost=[&](int i, int j, int c) -> int {
        if(dp[i][j][c]>=0)
            return dp[i][j][c];

        assert(j<=y);

        if(i==m-1) {
            int ans=INF;

            if(j+1>=x && j+1<=y)    /* same color */
                ans=min(ans, mm[!c][i]);

            if(1>=x && 1<=y)    /* other color */
                ans=min(ans, mm[c][i]);

            return dp[i][j][c]=ans;
        }

        int ans=INF;
        if(j<y) /* same color */
            ans=min(ans, mm[!c][i]+cost(i+1, j+1, c));

        if(j>=x) /* other color */
            ans=min(ans, mm[c][i]+cost(i+1, 1, !c));
            
        return dp[i][j][c]=ans;
    };

    int ans=min(cost(0,y,0), cost(0,y,1));
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
