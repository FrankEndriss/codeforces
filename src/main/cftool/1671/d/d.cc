
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * If we can place some x between two a, then do it for free.
 * else put them sorted to the beginning or the end.
 */
void solve() {
    cini(n);
    cini(x);
    cinai(a,n);

    int ans=0;
    for(int i=0; i+1<n; i++) 
        ans+=abs(a[i+1]-a[i]);

    int mi=*min_element(all(a));
    int ma=*max_element(all(a));

    cerr<<"ans1="<<ans<<" mi="<<mi<<" ma="<<ma<<endl;
    
    if(mi>1)
        ans+=min(min(a.back()-1, a[0]-1), (mi-1)*2);

    if(ma<x)
        ans+=min(min(x-a.back(), x-a[0]), (x-ma)*2);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
