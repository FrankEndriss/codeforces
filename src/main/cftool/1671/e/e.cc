
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * ...operation _any_ number of times.
 *
 * Let sL(v) be the set of strings the le child can produce,
 * and sR(v) be the set of strings the re child can produce,
 * intersect(sL(v),sR(v)) the strings that are in both sets.
 *
 * Then v can produce 
 * size(sL(v))*size(sR(v))-size(intersect) 
 *
 * How to find size(intersect), without actually producing sL(v) and sL(r)?
 * Consider depth=1, there are two cases:
 * x s[0] x; cnt=1
 * A s[0] B, B s[0] A; cnt=2
 * Consider depth=2, there are two cases:
 * ...
 * Note that two different childs create an empty intersection of strings.
 *
 * There are 3 kinds of parent vertex:
 * Both childs A
 * Both childs B
 * Both childs differ, C
 *
 * Note that if two childs have same leftmost path, then all strings they
 * produce are same, else none are.
 * ...to complecated :/
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cins(s);

    vs memo1(n);
    /* @return the ABC string of v */
    function<string(int)> dfs2=[&](int v) {
        if(v*2>n)  {
            string ans=" ";
            ans[0]=s[v-1];
            return ans;
        }
        if(memo1[v-1].size()>0)
            return memo1[v-1];

        memo1[v-1]+=s[v-1];
        if(v*2<=n) {
            if(s[v*2]==s[v*2+1])
                memo1[v-1][0]=s[v*2];
            else
                memo1[v-1]="C";
            memo1[v-1]=dfs2(v*2)+memo1[v-1]+dfs2(v*2+1);
        }
        return memo1[v-1];
    };

    function<int(int)> dfs=[&](int v) {
        if(v*2>n)
            return 1LL;

        string sl=dfs2(v*2);
        int cntL=dfs(v*2);
        string sr=dfs2(v*2+1);

        if(sl==sr)
            return cntL;
        else {
            int cntR=dfs(v*2+1);
            return cntL*cntR;
        }
    };

    int ans=dfs(1);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
