/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Today as many packs as possible...
 * So no simple binary search.
 *
 * On first day we want to visit a shop with a[i]=n*x
 * second day shop with a[i]=n*x-1
 * ... but allways one with biggest n.
 *
 * There are x categories of shops.
 *
 * Visit shops in order of increasing price, but
 * reorder shops within same number of packs, so that
 * min(a[i]%x) comes first.
 * ****
 * No, its another problem, see examples.
 */
void solve() {
    cini(n);
    cini(x);
    cinai(a,n);
    sort(all(a));

    vi pre(n+1);
    for(int i=0; i<n; i++) 
        pre[i+1]=pre[i]+a[i];


    /* day1 */
    auto it=lower_bound(all(pre), x);
    if(it==pre.end() || *it>x)
        it--;

    int idx=distance(pre.begin(), it);
    int ans=0;
    int d=0;
    while(idx>0) {
        int diff=x-(pre[idx]+d*idx);
        ans+=idx*(1+diff/idx);
        d+=(1+diff/idx);

        while(idx>0 && pre[idx]+d*idx>x)
            idx--;
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
