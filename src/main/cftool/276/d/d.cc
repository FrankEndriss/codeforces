/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* construct two values.
 * Find highest bit set in l but not in r.
 * set that bit in one value, all lower bits 
 * in other value.
 * Other value should be >=r, so 
 * in xor all these bits are set.
 * 
 */
void solve() {
    cini(l);
    cini(r);
    
    if(l<r)
        swap(l,r);

    for(int i=60; i>=0; i--) {
        int b=1LL<<i;
        if((l&b) && !(r&b)) {
            cout<<(b<<1)-1<<endl;
            return;
        }
    }
    cout<<0<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
