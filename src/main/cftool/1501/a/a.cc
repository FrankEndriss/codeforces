/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 */
void solve() {
    cini(n);

    vi a(n+1);
    vi b(n+1);
    for(int i=1; i<=n; i++) 
        cin>>a[i]>>b[i];

    vi t(n+1);
    for(int i=1; i<=n; i++)
        cin>>t[i];

    if(n==1) {
        cout<<a[1]+t[1]<<endl;
        return;
    }

    int arr=0;
    int prev=0; /* departure of previous station */
    for(int i=1; i<=n; i++) {
        arr=prev+a[i]-b[i-1];
        arr+=t[i];
        prev=max(b[i], arr+(b[i]-a[i]+1)/2);
    }

    cout<<arr<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
