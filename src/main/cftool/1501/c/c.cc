/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * x+y == z+w
 * What about
 * x-w == z-y
 *
 * Find two pairs with same distance.
 * How?
 *
 * If any two numbers have freq>1 we can use that.
 * So all other numbers have freq==1.
 * With each next number we add i-1 diffs.
 * So, fairly quick we have all diffs at least once.
 * Then the next number makes a pair for freq>1 or
 * two pairs with same diff.
 *
 * So for n>~1500 there is allways such quadrupel.
 */
const int N=3e6;
void solve() {
    cini(n);

    vvi idx(N);

    vi seen;
    map<int,vector<pii>> d; /* allready seen diffs: <diff,<aux1,aux2>> */

    vi freq2;   /* numbers with freq>1 */
    for(int i=0; i<n; i++) {
        cini(aux);
        idx[aux].push_back(i+1);
        const int ff=idx[aux].size();
        if(ff==2) {
            freq2.emplace_back(aux);

            if(freq2.size()>1) {
                cout<<"YES"<<endl;
                cout<<idx[freq2[0]][0]<<" "
                    <<idx[freq2[1]][0]<<" "
                    <<idx[freq2[0]][1]<<" "
                    <<idx[freq2[1]][1]<<endl;
                return;
            }
            continue;
        } else if(ff==4) {
                cout<<"YES"<<endl;
                cout<<idx[aux][0]<<" "
                    <<idx[aux][1]<<" "
                    <<idx[aux][2]<<" "
                    <<idx[aux][3]<<endl;
                return;
        } else if(ff>2)
            continue;

        /* so here we got a new number.
         * find all diffs to all other numbers
         * and find if we have seen that diff before.
         */
        assert(idx[aux].size()==1);

        for(auto it : seen) {
            const int dd=abs(aux-it);
            assert(dd>0);
            //cerr<<"diff abs("<<aux<<"-"<<it->first<<")=="<<dd<<endl;

            auto it2=d.lower_bound(dd);
            if(it2!=d.end() && it2->first==dd) {
                for(pii pp : it2->second) {

                    vi f4={ it, aux, pp.first, pp.second };
                    sort(all(f4));

                    if(f4[1]!=f4[2]) {
                        cout<<"YES"<<endl;
                        cout<<idx[f4[0]][0]<<" ";
                        cout<<idx[f4[3]][0]<<" ";
                        cout<<idx[f4[1]][0]<<" ";
                        cout<<idx[f4[2]][0]<<" ";
                        cout<<endl;
                        return;
                    } else {
                        if(idx[f4[1]].size()==1)
                            continue;

                        cout<<"YES"<<endl;
                        cout<<idx[f4[0]][0]<<" ";
                        cout<<idx[f4[3]][0]<<" ";
                        cout<<idx[f4[1]][0]<<" ";
                        cout<<idx[f4[2]][1]<<" ";
                        cout<<endl;
                        return;
                    }
                }
            }

            d[dd].emplace_back(aux,it);
        }
        seen.push_back(aux); /* mark current number seen */
    }
    cout<<"NO"<<endl;

}

signed main() {
    solve();
}
