/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Find all divisors of sum(a[]), foreach such k
 * group every k elements by
 * moving them to median position of that group.
 *
 * Unfortunatly this seems to be to slow :/
 * Or some endless loop? It does not look that slow.
 *
 * Tutorial:
 * Note that a construction over prefix sums of a[]
 * makes it faster. Since a move is simple an 
 * increase or decrease of a prefix-sum element.
 * So we only need to check every position for s[i]%k
 * instead of creating this element array and 
 * construct medium position.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vector<pii> pos;
    int sum=0;
    for(int i=0; i<n; i++) {
        if(a[i]) {
            pos.emplace_back(i, a[i]);
            sum+=a[i];
        }
    }

    if(sum==1) {
        cout<<-1<<endl;
        return;
    }


    set<int> d;
    for(int i=2; i*i<=sum; i++) {
        while(sum%i==0) {
            d.insert(i);
            sum/=i;
        }
    }
    if(sum>1)
        d.insert(sum);

    int ans=1e18;
    for(int k : d) {
        int lans=0;
        int lcount=0;
        int median; /* position of median element in group of k */
        int used=0; /* used at current position i */
        vector<pii> elements; /* elements in group of k */
        for(size_t i=0; i<pos.size(); i++) {
            const int psec=(pos[i].second%k);   /* ignore everything more than k, no need to move these */
            if(psec==0)
                continue;

            assert(used<psec);

            if(lcount==0) {
                median=-1;
                elements.clear();
            }

            assert(lcount<k);

            const int mi=min(psec-used, k-lcount);
            lcount+=mi;
            elements.emplace_back(pos[i].first, mi);

            if(median<0 && lcount>=(k+1)/2)
                median=pos[i].first;

            used+=mi;

            if(lcount<k) {
                assert(used==psec);
                used=0;
                continue;
            }

            for(int j=0; j<elements.size(); j++)
                lans+=abs((median-elements[j].first)*elements[j].second);

            lcount=0;

            if(used<psec)
                i--;
            else
                used=0;
        }
        ans=min(ans, lans);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
