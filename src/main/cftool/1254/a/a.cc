/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it)
{
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args)
{
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

string tc="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

void solve()
{
    cini(r);
    cini(c);
    cini(k);
    cinas(s,r);
    int cnt=0;
    for(int i=0; i<r; i++) {
        for(char ch : s[i])
            if(ch=='R')
                cnt++;
        if(i&1)
            reverse(s[i].begin(), s[i].end());
    }

    int x1=cnt/k; // num rice for every chicken
    int x2=cnt%k; // num chickens getting one rice more

    int curY=0;
    int curX=0;
    int curChicken=0;
    int curChickenCells=x1;
    if(x2>0)
        curChickenCells++;

    int tag=0;

    while(curY<r) {
        if(curChickenCells==0 && s[curY][curX]=='R') {
            curChicken++;
            curChickenCells=x1;
            if(curChicken<x2)
                curChickenCells++;
            tag++;
        }

        if(s[curY][curX]=='R') {
            curChickenCells--;
        }

        s[curY][curX]=tc[tag%tc.size()];

        curX++;
        if(curX==c) {
            curX=0;
            curY++;
        }
    }

    for(int i=0; i<r; i++) {
        if(i&1)
            reverse(s[i].begin(), s[i].end());
        cout<<s[i]<<endl;
    }
}

int main()
{
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

