/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Solve b1 first.
 *
 * Find all divisors of sum(a[]), foreach such k
 * group every k elements by
 * moving them to median position of that group.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi pos;
    for(int i=0; i<n; i++)
        if(a[i])
            pos.push_back(i);

    int sum=pos.size();
    if(sum==1) {
        cout<<-1<<endl;
        return;
    }


    vi d;
    for(int i=1; i*i<=sum; i++) {
        if(sum%i==0) {
            if(i!=1)
                d.push_back(i);
            if(i*i!=sum)
                d.push_back(sum/i);
        }
    }

    int ans=1e18;;
    for(int k : d) {
        int lans=0;
        for(size_t i=0; i<pos.size(); i+=k) {
            for(size_t j=0; j<k; j++) {
                lans+=abs(pos[i+j]-pos[i+k/2]);
            }
        }
        ans=min(ans, lans);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
