/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* First, find any number with a sum of its
 * divisors equal to c.
 * Trivial solution is c-1, it it is a prime.
 *
 * A prime has two prime divisors, so the sum 
 * of all divs is p+1
 * A number with 3 prime divs has 
 * d=p[0]+p[1]+p[2]+p[0]*p[1]+p[0]*p[2]+p[1]*p[2]
 */
const int N=1e7+3;
void solve() {
    cini(n);

    vi ans(N);
    ans[1]=1;

    vi dp(N,1);
    vb vis(N);
    vis[1]=true;

    for(int i=2; i<N; i++) {
        dp[i]+=i;
        if(dp[i]<N && !vis[dp[i]]) {
            ans[dp[i]]=i;
            vis[dp[i]]=true;
        }

        for(int j=i+i; j<N; j+=i)
            dp[j]+=i;
    }

    for(int i=0; i<n; i++) {
        cini(q);
        if(!vis[q])
            cout<<-1<<endl;
        else
            cout<<ans[q]<<endl;
    }

}

signed main() {
        solve();
}
