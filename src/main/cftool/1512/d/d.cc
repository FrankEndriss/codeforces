/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Two cases:
 *
 * 1. One of the biggest two numbers was choosen as x,
 * and the other one is the sum of all smaller numbers.
 *
 * 2. Any smaller number was choosen as x and the biggest
 * number is the sum of all other numbers.
 *
 * Note that order of elements does not matter.
 */
void solve() {
    cini(n);
    cinai(b,n+2);

    sort(all(b));
    int sum=accumulate(all(b), 0LL);

    if(sum-b[n]-b[n+1]==b[n] || sum-b[n]-b[n+1]==b[n+1]) {   /* case 1 */
        //cerr<<"case 1"<<endl;
        for(int i=0; i<n; i++)  {
            cout<<b[i]<<" ";
        }
        cout<<endl;
        return;
    } else {
        int idx=-1; /* index of x */
        for(int i=0; i<=n; i++) {
            if(sum-b[i]-b.back()==b.back()) {
                idx=i;
                break;
            }
        }
        //cerr<<"case 2, idx="<<idx<<endl;
        if(idx>=0) {
            for(int i=0; i<=n; i++) 
                if(i!=idx)
                    cout<<b[i]<<" ";
            cout<<endl;
            return;
        }
    }
    cout<<-1<<endl;



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
