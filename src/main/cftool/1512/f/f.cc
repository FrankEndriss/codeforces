/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Since if we buy a b[i] it is allways most benificial
 * to buy it as early as possible we can simply 
 * to that.
 * Then find linear foreach level the number
 * of days.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(c);
    cinai(a,n);
    cinai(b,n-1);

    vi dp(n);   /* dp[i]=first possible day to work on level i */
    vi dp1(n);   /* available money on day dp[i] before first time working on that level */

    int turk=0;
    int d=0;
    for(int i=0; i<n-1; i++) {
        if(turk<b[i]) {
            /* work for cnt days */
            int cnt=(b[i]-turk+a[i]-1)/a[i];
            d+=cnt;
            turk+=cnt*a[i];
        }
        assert(turk>=b[i]);
        turk-=b[i];
        d++;    /* one day for doing the exam */
        dp[i+1]=d;
        dp1[i+1]=turk;
    }

    int ans=INF;
    for(int i=0; i<n; i++)
        ans=min(ans, (c-dp1[i]+a[i]-1)/a[i]+dp[i]);

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
