/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(a);
    cini(b);
    cins(s);
    const int n=a+b;
    assert(s.size()==n);


    vi cnt(2);
    for(char c : s) {
        if(c=='0')
            cnt[0]++;
        else if(c=='1')
            cnt[1]++;
    }

    for(int i=0, j=n-1; i<j; i++,j--) {
        if(s[i]=='0') {
            if(s[j]=='?') {
                s[j]='0';
                cnt[0]++;
            } else if(s[j]=='1') {
                cout<<-1<<endl;
                return;
            }
        } else if(s[i]=='1') {
            if(s[j]=='?') {
                s[j]='1';
                cnt[1]++;
            } else if(s[j]=='0') {
                cout<<-1<<endl;
                return;
            }
        } else if(s[j]=='0') {
            cnt[0]++;
            s[i]='0';
        } else if(s[j]=='1') {
            cnt[1]++;
            s[i]='1';
        } else 
            assert(s[i]=='?' && s[j]=='?');
    }

    for(int i=0; i<=n-1-i; i++) {
        if(s[i]=='?') {
            assert(s[n-1-i]=='?');
            if(i==n-1-i) {
                if(cnt[0]+1==a) {
                    cnt[0]++;
                    s[i]='0';
                } else {
                    cnt[1]++;
                    s[i]='1';
                }
            } else if(cnt[0]+2<=a)  {
                s[i]=s[n-1-i]='0';
                cnt[0]+=2;
            } else if(cnt[1]+2<=b) {
                s[i]=s[n-1-i]='1';
                cnt[1]+=2;
            }
        }
    }

    if(cnt[0]==a && cnt[1]==b) 
        cout<<s<<endl;
    else
        cout<<-1<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
