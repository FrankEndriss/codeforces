/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* construction of some array.
 *
 */
void solve() {
    cini(n);
    cini(l);
    cini(r);
    cini(s);

    set<int> nums;
    for(int i=1; i<=n; i++) 
        nums.insert(i);

    int cnt=r-l+1;
    int mi=cnt*(cnt+1)/2;

    int ma=n*(n+1)/2 - (n-cnt+1)*(n-cnt)/2;
    if(s<mi || s>ma) {
        cout<<-1<<endl;
        return;
    }

    vi pre(n+1);
    for(int i=l; i<=r; i++)
        pre[i]=pre[i-1]+(i-l+1);

    vi ans(n+1);
    for(int i=r; i>=l; i--) {
        int d=s-pre[i-1];
        auto it=nums.lower_bound(d);
        if(it==nums.end() || d<*it)
            it--;

        ans[i]=*it;
        nums.erase(it);
        s-=ans[i];
    }
    for(int i=1; i<=n; i++) {
        if(ans[i]==0) {
            auto it=nums.begin();
            ans[i]=*it;
            nums.erase(it);
        }
    }
    for(int i=1;i<=n; i++) 
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
