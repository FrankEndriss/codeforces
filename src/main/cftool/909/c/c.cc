
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* The statement following an f allways on the level determined by the f.
 * The then next statement is at any level from 0 to prev max level.
 * So there are three cases:
 *
 * 1. f following an s
 * dp0[i]=sum(dp[i-1..n])
 *
 * 2. f following an f
 * dp0[i]=dp[i-1]
 *
 * 3. s following an s
 * dp0[i]=sum(dp[i..n])
 *
 * 4. s following an f
 * dp0[i]=dp[i]
 * 
 */
const int MOD=1e9+7;
void solve() {
    cini(n);
    vi dp(1, 1);
    char prev='s';
    for(int i=0; i<n; i++) {
        vi dp0(dp.size());
        cins(s);
        const char c=s[0];

        if(c=='f' && prev=='s') {
            dp0.push_back(0);
            for(int i=dp0.size()-1; i>0; i--)  {
                dp0[i]=dp[i-1];
                if(i+1<dp0.size())
                    dp0[i]+=dp0[i+1];
                dp0[i]%=MOD;
            }
        } else if(c=='f' && prev=='f')  {
            dp0.push_back(0);
            for(int i=dp0.size()-1; i>0; i--) {
                dp0[i]=dp[i-1];
                dp0[i]%=MOD;
            }
        } else if(c=='s' && prev=='s') {
            for(int i=dp0.size()-1; i>=0; i--) {
                dp0[i]=dp[i];
                if(i+1<dp0.size())
                    dp0[i]+=dp0[i+1];
                dp0[i]%=MOD;
            }
        } else if(c=='s' && prev=='f') {
            dp0.swap(dp);
        }

        prev=c;
        dp.swap(dp0);
    }
    int ans=0;
    for(int i : dp)
        ans=(ans+i)%MOD;

    cout<<ans<<endl;
}

signed main() {
    solve();
}
