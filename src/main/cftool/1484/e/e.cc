/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We would like to put all negative beauty buildings
 * into one photo, and have pictures of all others
 * single.
 * But we can put only contigous segments into one pic.
 * We want to put each neg building together with a 
 * smaller pos building, to get rid of the negative
 * value.
 * With smaller constraints that would make a simple
 * O(n^2) dp.
 *
 * Consider the smallest building of all. Obviously we 
 * must take its beauty.
 * We can add arbitrary other buildings to the left and
 * to the right, which then get ignored.
 * Can we greedyly somehow add them, say whenever
 * the beauty sum is negative or the like?
 * However, after having taken that piture, the same
 * goes on with the next, again we must take the shortest 
 * buildings beauty and can add arbitray ones.
 *
 * So, how do we add buildings to the picture of the current shortest?
 * Note that it never makes sense to simply add a positive building.
 * But it allways makes sense to add a directly adjacent negative
 * building.
 * So we want to take two to the left if the sum is negative. And, if
 * there is no other way to get rid of them.
 * Consider <hight,beauty>
 * {2,1}{4,-9}{3,2}{1,1}
 * Here we must take 4, then dicide if taking also 2 and 3.
 * If there would be no 1 then it would be optimal to take 2 and 3.
 * But with 1, it is optimal to take 2 together with 1, and 3,4
 * separatly. To get all positive beautys, and no need to pay
 * the negative.
 */
void solve() {
}

signed main() {
    solve();
}
