/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Each song can remove all following ones
 * where gcd==1. Except it is remove before doing
 * so.
 * So, we need to consider only that positions where gcd==1.
 * Simulate, and maintain the relevant positions in a set,
 * and the songs in a linked list.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi l(n);    /* cyclic linked list */
    for(int i=0; i+1<n; i++) 
        l[i]=i+1;
    l[n-1]=0;

    set<int> p; /* positions of songs that delete the next one */
    for(int i=0; i<n; i++) {
        int g=gcd(a[i], a[(i+1)%n]);
        if(g==1)
            p.emplace(i);
    }

    vi ans;
    int idx=-1;
    while(p.size()) {
        auto it=p.upper_bound(idx);
        if(it==p.end())
            it=p.upper_bound(-1);
        idx=*it;
        ans.push_back(l[idx]);
        p.erase(l[idx]);
        l[idx]=l[l[idx]];
        if(gcd(a[idx], a[l[idx]])!=1) 
            p.erase(idx);
    }
    cout<<ans.size()<<" ";
    for(size_t i=0; i<ans.size(); i++) 
        cout<<ans[i]+1<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
