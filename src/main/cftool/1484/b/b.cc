/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* this seems to be crm, need to convert
 * input into format expected by the formular.
 * a[0]=s           mod m
 * a[1]=a[0]+c      mod m
 * a[2]=a[1]+c      mod m
 * ...
 * Consider a[0]=s, then m>s
 * a[1]=s+c         mod m, so m=s-s+a[i]-a[0]       (or the like...)
 * ...but that does not work out for a[2]...
 *
 * Whereever a[] increases, the increase must eq c.
 * Whereever a[] decreases, the decrease must be constructable by m.
 *
 * If all numbers are same then c==0 and m arbitrary large, ans=0.
 * If there is at least on inc pair, then c=diff. And for all decreasing
 * pairs we need to find a fitting m, which must be bigger than max(a[]).
 * This is m=a[i-1]+c-a[i] if this m<=biggest number then ans=-1.
 * Check foreach decreasing pair.
 * Else check if all m are same, and that is ans.
 *
 * What if no pairs is increasing, we do not know c?
 *
 * If it is more than one pair no sol.
 * Else m arbitrary huge.
 * How to proove?
 */
void solve() {
    cini(n);
    cinai(a,n);

    int c=-1;
    int m=-1;
    for(int i=1; i<n; i++) {
        if(a[i-1]<=a[i]) {
            int cc=a[i]-a[i-1];
            if(c>=0 && c!=cc) {
                cout<<-1<<endl;
                return;
            } else
                c=cc;
        } else if(a[i-1]>a[i]) {
            int mm=a[i-1]+c-a[i];
            if(m>0 && m!=mm) {
                cout<<-1<<endl;
                return;
            }
            m=mm;
        } else
            assert(false);
    }

    if(c<0) {   /* no increasing  pair */
        if(n>2)
            cout<<-1<<endl;
        else 
            cout<<0<<endl;
        return;
    }

    if(m>0 && (m<=c || m<=*max_element(all(a)))) {
        cout<<-1<<endl;
        return;
    }

    if(c!=0)
        cout<<m<<" "<<c<<endl;
    else
        cout<<0<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
