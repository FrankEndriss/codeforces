/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/maxflow>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We would like to use max flow, but that will TLE.
 * So there should be some greedy.
 *
 * Consider friends used on days where there is only one friend,
 * no choosing possible.
 * Then, per friend, a number of days are still available where
 * we can choose that friend.
 * ...
 * What would be the graph for maxflow?
 * S -> Friend, (M+1)/2
 * Friend -> Day, 1
 * Day -> T, 1
 * Then ok if flow==M
 * But we cannot add n*m edges...
 * ***
 * Let friends with more than (m+1)/2 availabilities
 * be strong friends. If there are two (or more) 
 * strong friends, we can greedy devide their days (how?).
 * And then consider the other days by simply choosing 
 * the first friend of each day.
 *
 * Consider there is exactly one strong friend.
 * Then we can create a maxflow.
 *
 * ...
 * Forget about all of it: The sum of all k[i] over all 
 * testcases is max 2e5, so maxflow should work. Or not?
 * Again not sure. Oh my.
 * But Edmonds-Karp is O(n*m^2) which is way out of sight.
 * idk :/
 */
void solve() {
    cini(n);
    cini(m);
    vi st;      /* list of strong friends */
    vi ff(n);   /* freq of friends availability */
    vvi d(m);   /* the days */

    const int S=n+m;
    const int T=n+m+1;
    mf_graph<int> g(T+1);
    for(int i=0; i<n; i++)
        g.add_edge(S, i, (m+1)/2);
    for(int i=0; i<m; i++) 
        g.add_edge(n+i, T, 1);

    vi edg;
    for(int i=0; i<m; i++) {    /* m days */
        cini(k);
        for(int j=0; j<k; j++) {
            cini(aux); aux--;
            edg.push_back(g.add_edge(aux, n+i, 1));
        }
    }

    int mflow=g.flow(S,T);
    if(mflow<m) 
        cout<<"NO"<<endl;
    else {
        cout<<"YES"<<endl;
        vi ans(m);
        for(size_t i=0; i<edg.size(); i++) {
            auto e=g.get_edge(edg[i]);
            if(e.flow==1) {
                ans[e.to-n]=e.from;
            }
        }
        for(int i : ans)
            cout<<i+1<<" ";
        cout<<endl;
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
