/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider we know the steps numbers where all tries happen,
 * let s[i] be the ith of them
 *
 * So with p prop we finish at s[0]
 * else with (1-p)*p prob we finish at s[1]
 * else with (1-p)*(1-p)*p we finis at s[2]
 * ...
 *
 * ans=sum( p*(1-p)^i * s[i]) for i=0..INF
 *
 * So most likely there is some closed form for this,
 * else we could not express it as a fraction.
 */
void solve() {
	cini(n);
	cini(m);
	cini(rb);
	cini(cb);
	cini(rd);
	cini(cd);
	cini(p);

}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
