/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * In first step Alice chooses 1,n
 * Bob x
 * Then Alice 1,x-1 or x+1,n
 *
 * We start with 0,n-1, then find two ranges
 * 0,x-1, x+1,n; or 0,n-2 or 1,n-1
 * Then continue same for the found ranges.
 * Use a queue.
 *
 * How to find the two ranges that result by a split of 
 * one bigger range?
 *
 */
void solve() {
	cini(n);
	set<pii> lr;
	for(int i=0; i<n; i++) {
		cini(l);
		cini(r);

		lr.emplace(l,r);
	}

	queue<pii> q;
	q.emplace(1,n);

	while(q.size()) {
		auto [l2,rr]=q.front();
		q.pop();

		if(l2==rr) {
			cout<<l2<<" "<<rr<<" "<<rr<<endl;
			continue;
		}

		auto it=lr.find({l2,rr-1});
		if(it!=lr.end()) {
			cout<<l2<<" "<<rr<<" "<<rr<<endl;
			q.emplace(l2,rr-1);
			continue;
		}
		it=lr.find({l2+1,rr});
		if(it!=lr.end()) {
			cout<<l2<<" "<<rr<<" "<<l2<<endl;
			q.emplace(l2+1,rr);
			continue;
		}

		for(int j=l2+1; j<=rr-1; j++) {
			it=lr.find({l2,j-1});
			if(it!=lr.end()) {
				auto it2=lr.find({j+1,rr});
				if(it2!=lr.end()) {
					cout<<l2<<" "<<rr<<" "<<j<<endl;
					q.emplace(l2,j-1);
					q.emplace(j+1,rr);
					continue;
				}
			}
		}
	}
}

signed main() {
	cini(t);
	while(t--) {
    		solve();
	}
}
