/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to duplicate all nodes
 * where the label is lex smaller than the next different one.
 * So first create that list.
 * Then iterate the tree (in-order) and duplecate each node
 * (and its still not duplected parents) it it needs to be duplecated
 * and can be duplecated within bound given by k.
 *
 * ...no, not that simple.
 * We need to consider that right child can only be 
 * duplecated if all parents should also be duplected :/
 */
void solve() {
    cini(n);
    cini(k);

    cins(lbl);

    vvi adj(n, vi(2));
    vi p(n,-1);	/* parent */
    for(int i=0; i<n; i++)  {
        cin>>adj[i][0]>>adj[i][1];
        adj[i][0]--;
        adj[i][1]--;
        if(adj[i][0]>=0)
            p[adj[i][0]]=i;
        if(adj[i][1]>=0)
            p[adj[i][1]]=i;
    }

    /* in order dfs */
    function<void(int, function<void(int)>)> dfs=[&](int v, function<void(int)> cb) {
        if(adj[v][0]>=0)
            dfs(adj[v][0],cb);
        cb(v);
        if(adj[v][1]>=0)
            dfs(adj[v][1],cb);
    };

    vi ord;	/* order of vertex in tree */
    dfs(0, [&](int v) {
	cerr<<v<<lbl[v]<<" ";
        ord.push_back(v);
    });
    cerr<<endl;
    vi pos(n);	/* pos[v]= position of v in order */
    for(int i=0; i<n; i++) 
	    pos[ord[i]]=i;

    vb sml(n);	/* sml[i]=true if next letter after vertex i is bigger, so we want to
			   duplicate lbl[i] */
    for(int i=n-2; i>=0; i--) {	/* iterate positions */
        if(lbl[ord[i+1]]>lbl[ord[i]]) {
            sml[ord[i]]=true;
        } else if(lbl[ord[i+1]]==lbl[ord[i]]) {
            sml[ord[i]]=sml[ord[i+1]];
        } else
            sml[ord[i]]=false;
    }
    for(int i=0; i<n; i++) 
	    cerr<<sml[ord[i]]?'1':'0';
    cerr<<endl;
    for(int i=0; i<n; i++) 
	    cerr<<lbl[ord[i]];
    cerr<<endl;

    vb vis(n);
    int cnt=0;
    for(int i=0; i<n; i++) {	/* iterate positions */
        if(!sml[ord[i]] || vis[ord[i]])
            continue;

        int lcnt=0;	/* count path to root */

        int v=ord[i];
        do {
            lcnt++;
            v=p[v];
        } while(v>=0 && !vis[v]);

        if(cnt+lcnt<=k) {
            v=ord[i];
            do {
		    assert(!vis[v]);
                vis[v]=true;
		cnt++;
                v=p[v];
            } while(v>=0 && !vis[v]);
        }
    }

    dfs(0, [&](int v) {
		    if(vis[v]) {
		    	cout<<lbl[v]<<lbl[v];
		    } else {
		    	cout<<lbl[v];
		    }
    });
    cout<<endl;
}

signed main() {
    solve();
}
