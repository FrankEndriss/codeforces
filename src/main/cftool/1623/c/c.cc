/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Binary search?
 * For some min hight we can go from right to left
 * ans allways move max possible stones.
 */
void solve() {
	cini(n);
	cinai(h,n);

	int l=0; 
	int r=1e9+1;

	while(l+1<r) {
		int mid=(l+r)/2;
		vi hh=h;
		vi dd(n);

		bool ok=true;
		for(int i=n-1; ok && i>=0; i--) {
			if(hh[i]+dd[i]<mid) {
				ok=false;
				break;
			}
			int d=min(hh[i],hh[i]+dd[i]-mid);
			assert(d>=0);
			if(i>=2) {
				dd[i-2]+=(d/3)*2;
				dd[i-1]+=(d/3);
			}
		}
		if(ok)
			l=mid;
		else
			r=mid;
	}
	cout<<l<<endl;


}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
