/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Since we can use any 0 for the subsequences
 * contributing to a00 the position of the 0s
 * does not matter.
 * So a00 determines the number of 0s.
 * Same is true for 1s and a11.
 * 00   -> 1
 * 000  -> 3
 * 0000 -> 6
 *
 * Given x 0s and y 1s we need to construct
 * a01 by putting "some" 0s before the 1s.
 *
 * Let the string be "000...000" (x times)
 * Then adding  
 * 1 at position 0 add x-0 "10"
 * 1 at position 1 add x-1 "10" and 1 "01"
 * 1 at position 2 add x-2 "10" and 2 "01"
 *
 * How to construct the correct positions?
 * Say we add all 1s at position 0, then
 * move the 1s each by one to the right.
 *
 * Each move removes one "10", and adds one "01".
 * So the sum a10+a01 must correspond to x and y,
 * it is x*y
 *
 * Note that there are several more edge cases.
 */
void solve() {
    cini(a00);
    cini(a01);
    cini(a10);
    cini(a11);

    vi dp((int)1e6+1);
    dp[2]=1;

    for(size_t i=3; i<dp.size(); i++)
        dp[i]=dp[i-1]+i-1;

    auto it=lower_bound(all(dp), a00);
    if(*it!=a00) {
        cout<<"Impossible"<<endl;
        return;
    }
    int x=distance(dp.begin(), it); /* number of 0s */
    if(x==0 && (a01!=0 || a10!=0))
        x=1;

    it=lower_bound(all(dp), a11);
    if(*it!=a11) {
        cout<<"Impossible"<<endl;
        return;
    }
    int y=distance(dp.begin(), it); /* number of 1s */
    if(y==0 && (a01!=0 || a10!=0))
        y=1;

    if(x*y!=a10+a01) {
        cout<<"Impossible"<<endl;
        return;
    }

    if(x==0 && y==0) {
        cout<<"0"<<endl;
        return;
    }

    if(x==0) {
        cout<<string(y, '1')<<endl;
        return;
    }
    if(y==0) {
        cout<<string(x, '0')<<endl;
        return;
    }

    int y0=y;   // 1s at position 0
    int y1=0;   // 1s at position x
    int yy=-1;  // position of the one 1 in between

    y1=a01/x;
    y0-=y1;
    if(a01%x>0) {
        y0--;
        yy=a01%x;
    }
    
    if(y0<0) {
        cout<<"Impossible"<<endl;
        return;
    }

    for(int i=0; i<y0; i++)
        cout<<'1';

    if(yy>=0) {
        for(int i=0; i<yy; i++)
            cout<<'0';
        cout<<'1';
        for(int i=yy; i<x; i++)
            cout<<'0';
    } else {
        cout<<string(x,'0');
    }

    for(int i=0; i<y1; i++) 
        cout<<'1';

    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
