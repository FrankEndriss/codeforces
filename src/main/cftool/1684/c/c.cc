/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We got rows where we must swap two certain columns,
 * and we got rows where we can swap certain columns.
 *
 * How to find the "must swap" cols?
 *
 * ...do not like this one, too :/
 * Some edgecase?
 */
void solve() {
    cini(n);
    cini(m);

    vvi d(n);   /* d[i][k]=index in a[i][d[i][k]]>a[i][d[i][k]+1] */
    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++)  {
            cin>>a[i][j];

            if(j>0 && a[i][j-1]>a[i][j]) {
                d[i].push_back(j-1);
            }
        }

    vector<set<pii>> ans(n);
    bool ok=true;
    bool zero=true;
    for(int i=0; ok && i<n; i++) {
        if(d[i].size()==1) { 
            zero=false;
            const int idx=d[i][0];
            int idx2=d[i][0]+1;
            while(idx2+1<n && a[i][idx2]==a[i][idx2+1])
                idx2++;

            swap(a[i][idx], a[i][idx2]);
            if(is_sorted(all(a[i]))) {
                ans[i].emplace(idx,idx2);
            } else
                ok=false;
            swap(a[i][idx], a[i][idx2]);

        } else if(d[i].size()==2) {
            zero=false;
            for(int idx1=d[i][0]; idx1<=d[i][0]+1;  idx1++) {
                for(int idx2=d[i][1]; idx2<=d[i][1]+1; idx2++) {
                    swap(a[i][idx1], a[i][idx2]);
                    if(is_sorted(all(a[i]))) {
                        ans[i].emplace(idx1,idx2);
                    }
                    swap(a[i][idx1], a[i][idx2]);
                 }
            }
        } else if(d[i].size()>2)
            ok=false;
    }

    if(!ok) {
        cout<<-1<<endl;
        return;
    }
    if(zero) {
        cout<<"1 1"<<endl;
        return;
    }

    bool first=true;
    set<pii> gans;
    for(int i=0; i<n; i++) {
        if(ans[i].size()>0) {
            if(first)
                gans=ans[i];
            else {
                set<pii> s;
                for(pii p1 : gans)
                    if(ans[i].count(p1))
                            s.insert(p1);
                gans=s;
            }
            first=false;
        }
    }

    pii sw={0,0};
    if(gans.size()>0)
        sw=*gans.begin();

    ok=true;
    for(int i=0; ok && i<n; i++) {
        swap(a[i][sw.first], a[i][sw.second]);
        if(!is_sorted(all(a[i])))
            ok=false;
    }

    if(!ok) {
        cout<<-1<<endl;
        return;
    }

    cout<<sw.first+1<<" "<<sw.second+1<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
