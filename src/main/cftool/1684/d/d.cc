/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Making k jumps, we step into n-k traps, and get pre[n-k]+ (n-k)*k damage.
 * So just iterate that formular.
 * ...no.
 * The cost of a jump depends on
 * -its position
 * -the number of later jumps
 *
 * Given a fixed number of jumps, the cost is also fixed.
 *
 * Consider the benefit of the Lth of k jumps at pos=j, it is
 * the not getted damage minus the number of remaining positions
 * that cannot be jumped:
 * a[j]-(n-j-1-(k-L))
 *
 * Note that increasing k changes the benefit of each jump by +1.
 * So we can assume k==0 and add it later.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
    vi b(n+1);
    for(int i=0; i<n; i++) 
        b[i+1]=a[i]-(n-i-1);

    sort(b.begin()+1, b.end(), greater<int>());
    for(int i=1; i<=n; i++) 
        b[i]+=b[i-1];

    const int sum=accumulate(all(a), 0LL);

    int ans=sum;
    for(int i=0; i<=k; i++) {   /* jump over i traps */
        ans=min(ans, sum - (b[i]+(i*(i-1)/2)));
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
