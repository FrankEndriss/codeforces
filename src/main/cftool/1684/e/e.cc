/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to get rid of all 0, if possible.
 * if not possible, want to get rid of 1...
 * and so on.
 * Note that each changed element is unique after.
 * So, diff(a) after k operations is min(n, set(a)+k)
 *
 * But also, if we got rid of a number, that cost us maybe one diff.
 * ...
 * ****
 * It is other way round:
 * We want _minimum cost_, so min(diff), max(mex)
 *
 * Creating 0 may increase diff, but decrease mex.
 * So create max possible mex by removing most possible
 * single elements, since so we do minimum increase of diff.
 * Every removed element > x does decrease diff by one.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    map<int,int> f;
    set<int> cnt;
    for(int i=0; i<n; i++)  {
        f[a[i]]++;
        cnt.insert(a[i]);
    }


    /* with k operations we can max mex()==x */
    int kk=k;
    int x=-1;
    for(int i=0; kk>=0 && i<=n; i++) {
        x=i;
        if(f[i]==0) {
            kk--;
        }
    }

    vector<pii> ff;
    for(auto ent : f) 
        if(ent.first>x)
            ff.emplace_back(ent.second,ent.first);

    sort(all(ff));

    int ans=((int)cnt.size())-x;

    for(size_t i=0; k>0 && i<ff.size(); i++) 
        if(k>=ff[i].first) {
            ans--;
            k-=ff[i].first;
        }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
