/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

/* We need to make
 * most deepest nodes indu citis.
 * How to find them after having levels of all cities?
 * Greedy
 * K times find optimal node.
 * From set of nodes choose the one with best contribution.
 * Contribution of node is level-chldcnt.
 * Because there are level nice citis on the way to root, but
 * one city is lost for every child.
 */
void solve() {
    cini(n);
    cini(k);

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vi lvl(n);
    vi chlcnt(n);
    function<void(int,int)> dfs=[&](int node, int parent) {
        if(node!=0)
            lvl[node]=lvl[parent]+1;

        for(auto chl : adj[node]) {
            if(chl!=parent) {
                dfs(chl, node);
                chlcnt[node]+=chlcnt[chl]+1;
            }
        }
    };

    dfs(0, -1);

/* lets list all leafs with the levels, sorted by level desc.
 * all children get added before the parents since level is bigger, 
 * and chlcnt is less.
 */
    vector<int> nset;
    for(int i=1; i<n; i++)
        nset.push_back(-lvl[i]+chlcnt[i]);

    sort(nset.begin(), nset.end());

    int ans=0;
    for(int i=0; i<k; i++)
        ans-=nset[i];

    cout<<ans<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

