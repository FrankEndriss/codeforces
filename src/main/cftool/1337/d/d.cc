/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* brute force, somehow
 * honestly, i really dont know how to implement this.
 * For some reason we need to compare squares. Why?
 * What would change if not squares but simply diferences?
 * -> without squares it would be only diff hi-lo.
 *    with squares the middle value is important, too.
 *
 * Triple pointer?
 *
 * If there would be some time left, I could try
 * to fix value from first array, choose two nearest from
 * second array, the for those the two nearest from third array.
 *
 * This triple pointer runs TLE if there are lot 
 * of values in one array in between two of the other array.
 * Fix that by using only the 'middle' ones somehow?
 *
 * */
void solve() {
    cinai(n,3);
    vvi rgb(3);
    for(int i=0; i<3; i++) {
        for(int j=0; j<n[i]; j++) {
            cini(aux);
            rgb[i].push_back(aux);
        }
    }
    for(int i=0; i<3; i++)
        sort(rgb[i].begin(), rgb[i].end());

    int ans=2e18;

    function<void(int,int,int)> check=[&](int v0, int v1, int v2) {
        const int d1=v0-v1;
        const int d2=v0-v2;
        const int d3=v1-v2;
        ans=min(ans, d1*d1+d2*d2+d3*d3);
    };

    vi perm= { 0, 1, 2 };
    do {

        int min1=0;
        int max1=0;
        int min2=0;
        int max2=0;

        for(int i0=0; i0<rgb[perm[0]].size(); i0++) {
            while(min1+1<rgb[perm[1]].size() && rgb[perm[1]][min1+1]<rgb[perm[0]][i0])
                min1++;
            while(max1+1<rgb[perm[1]].size() && rgb[perm[1]][max1]<=rgb[perm[0]][i0])
                max1++;

/* store the min2/max2 per i1 somehow, then use from there ? */
            for(int i1=min1; i1<=max1; i1++) {
                while(min2>0 && rgb[perm[2]][min2]>=rgb[perm[1]][i1])
                    min2--;
                while(max2>0 && rgb[perm[2]][max2-1]>rgb[perm[1]][i1])
                    max2--;

                while(min2+1<rgb[perm[2]].size() && rgb[perm[2]][min2+1]<rgb[perm[1]][i1])
                    min2++;

                while(max2+1<rgb[perm[2]].size() && rgb[perm[2]][max2]<=rgb[perm[1]][i1])
                    max2++;

                for(int i2=min2; i2<=max2; i2++)
                    check(rgb[perm[0]][i0], rgb[perm[1]][i1], rgb[perm[2]][i2]);
            }
        }
    } while(next_permutation(perm.begin(), perm.end()));

    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

