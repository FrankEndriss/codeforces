/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* There are only two cases,
 * we add the same number all the time,
 * or we add ...
 */
const int N=2e5;
void solve() {
    cini(n);
    cini(k);
    vb vis(N);
    set<int> num;
    int ma=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux<N)
            vis[aux]=true;
        num.insert(aux);
        ma=max(ma, aux);
    }

    int mex=0;
    for(; mex<vis.size(); mex++) 
        if(!vis[mex])
            break;

    if(mex==ma+1) {
        cout<<n+k<<endl;
        return;
    }

    for(int i=0; i<k; i++) {
        int val=(mex+ma+1)/2;
        if(num.count(val))
            break;
        num.insert(val);
        if(val<n*2)
            vis[val]=true;

        while(vis[mex])
            mex++;
    }

    cout<<num.size()<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
