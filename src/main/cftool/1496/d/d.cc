/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * On each turn, one can move to the next adjacent field
 * with smaller value.
 *
 * So first player wants to choose the position
 * with max such possible steps in both directions.
 * First wins, if
 * -both directions do not differ in length more than once
 * -there is no other increasing seq longer/equal that the length
 *
 * What about if both seqs end in same position?
 * -> then first wins instead of second
 *
 * Cases:
 * First chooses some local maximum, the min side
 * is len=mi1 in dir d, other is mi2 in dir -d.
 *
 * Second must find some other desc seq with len>=mi1.
 * Or desc seq with len>=mi-1, but ending in same position.
 * But mi2 must not be
 * Because this cuts on position from firsts seq.
 *
 * ...
 * Ok, y goes upward, not downwards. Fuck this shit.
 * After one hour and like 25 times read the text, we got 
 * another problem now. Start from scratch...
 *
 * y chooses a local minimum. If it is the same len
 * as x local max, then y wins.
 * If it is shorter, y would take the minimum where x will end.
 * But if x can choose the direction, that wont work.
 * So x wins, if the local max has the same len on both sides,
 * and there is no same or longer seq somehere else.
 *
 * ...but still WA :/
 * What about if y goes same way up as x goes down?
 * Then y wins if len is even. So y chooses the last or 
 * pre-last position to make the len even.
 * Then, x must go the other dir down.
 * So, x wins if 
 * -the longer side made odd is same or smaller than the shorter end
 */
void solve() {
    cini(n);
    cinai(a,n);
    /* find local maximum with max dec on both sides */

    vi ri(n);
    ri[0]=1;
    for(int i=1; i<n; i++) {
        if(a[i]>a[i-1])
            ri[i]=ri[i-1]+1;
        else
            ri[i]=1;
    }

    vi le(n);
    le.back()=1;
    for(int i=n-2; i>=0; i--) {
        if(a[i]>a[i+1])
            le[i]=le[i+1]+1;
        else
            le[i]=1;
    }

    int mask=~1;
    int ma=0;
    int pos=-1;
    for(int i=0; i<n; i++) {
        if(le[i]==ri[i] && le[i]>ma) {
                ma=max(le[i], ri[i]);
                pos=i;
        } else if(le[i]>ri[i] && (le[i]&mask)<=ri[i] && ri[i]>ma) {
                ma=max(le[i], ri[i]);
                pos=i;
        } else  if(le[i]<ri[i] && (ri[i]&mask)<=le[i] && le[i]>ma) {
                ma=max(le[i], ri[i]);
                pos=i;
         }
    }

    for(int i=0; i<n; i++) {
        if((le[i]>=ma && i!=pos) || (ri[i]>=ma && i!=pos)) {
            //cerr<<"le[i]="<<le[i]<<" ri[i]="<<ri[i]<<" ma="<<ma<<" i="<<i<<" pos="<<pos<<endl;
            cout<<0<<endl;
            return;
        }
    }

    cout<<1<<endl;

}

signed main() {
    solve();
}
