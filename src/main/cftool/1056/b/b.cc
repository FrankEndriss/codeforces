/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Find number of pairs (i,j) i,j<=n 
 * which are (i^2+j^2) divisable by m.
 *
 * A pair is d by m if ((i^2)%m + (j^2)%m)%m == 0
 *
 * We need to find how much numbers have
 * mod (i*i)%m==k for all i<m.
 */
void solve() {
    cini(n);
    cini(m);

/* Find number of elements in k=(1,n) with (k*k)%m==i
 * Note that (k*k)%m == (k%m * k%m)%m
 *
 * So lets first find for j=(1,m) how much (j*j)%m==i 
 */
    vi dp1(m);  /* dp1[i]== number of numbers in k=(1,n) with (k*k)%m==i */
    for(int i=1; i<=m; i++)
        dp1[(i*i)%m]++;

    const int mul=n/m;
    for(int i=0; i<m; i++)
        dp1[i]*=mul;

    for(int i=1; i<=n%m; i++)
        dp1[(i*i)%m]++;


    int ans=0;
    for(int i=0; i<m; i++) {
        int cnt1=dp1[i];
        if(i==0 || i+i==m) {
            ans+=cnt1*cnt1;
        } else {
            int cnt2=dp1[m-i];
            ans+=cnt1*cnt2;
        }
    }
    cout<<ans<<endl;
}
    

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

