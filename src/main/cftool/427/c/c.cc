/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* A recursive function to print DFS starting from v */
void DFSUtil(int v, vb& vis, vvi& adj, vi& ans) {
    vis[v] = true;
    ans.push_back(v);

    for(int i : adj[v])
        if(!vis[i])
            DFSUtil(i, vis, adj, ans);
}

void fillOrder(int v, vb& vis, stack<int> &Stack, vvi& adj) {
    vis[v] = true;

    for(int i : adj[v])
        if(!vis[i])
            fillOrder(i, vis, Stack, adj);

    Stack.push(v);
}

/* The main function that finds returns all strongly connected components */
vvi printSCCs(vvi& adj) {
    stack<int> Stack;
    vb vis(adj.size());

    // Fill vertices in stack according to their finishing times
    for(size_t i = 0; i<adj.size(); i++)
        if(!vis[i])
            fillOrder(i, vis, Stack, adj);

    // Create a reversed graph
    vvi adjT(adj.size());
    for (size_t v=0; v<adj.size(); v++) {
        for(int i : adj[v])
            adjT[i].push_back(v);
    }

    // Mark all the vertices as not visited (For second DFS)
    for(size_t i=0; i<adj.size(); i++)
        vis[i]=false;

    vvi ans;
    // Now process all vertices in order defined by Stack
    while(Stack.size()) {
        // Pop a vertex from stack
        int v = Stack.top();
        Stack.pop();

        // Print Strongly connected component of the popped vertex
        if (!vis[v]) {
            ans.resize(ans.size()+1);
            DFSUtil(v, vis, adjT, ans.back());
        }
    }
    return ans;
}

const int INF=1e9+7;
const int MOD=1e9+7;

/* circles form components.
 * from each component we need to use one junction.
 * It is the strongly connected components.
 *
 * ans1=sum of min(cost) per component
 * ans2=product of number of vertex with same min cost in component
 */
void solve() {
    cini(n);
    cinai(c,n); /* cost */
    cini(m);
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
    }

    vvi scc=printSCCs(adj);
    vi mi(scc.size(), INF);
    vi cnt(scc.size()); /* cnt of min elements */
    
    for(size_t i=0; i<scc.size(); i++) {
        for(int j : scc[i]) {
            if(c[j]<mi[i]) {
                mi[i]=c[j];
                cnt[i]=1;
            } else if(c[j]==mi[i])
                cnt[i]++;
        }
    }

    int ans1=0;
    int ans2=1;
    for(size_t i=0; i<scc.size(); i++) {
        if(mi[i]<INF) {
            ans1+=mi[i];
            ans2=(ans2*cnt[i])%MOD;
        }
    }

    cout<<ans1<<" "<<ans2<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
