/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The brute force is fairly simple:
 * Just set the bits in range l,r for all constraints,
 * then check if still all constraints are fullfilled.
 *
 * Cant implement this, since O(n^2)
 * So, how to optimize?
 *
 * A constrain may fail if and only if all positions of
 * the constraint is covered by at least one other constraint, too.
 * And that other constraint(s) must set at least one same bit
 * in all positions!
 *
 * So, we need to find for each constraint if that coverage is 
 * the case.
 * Somehow Mo?
 *
 * ***
 * Consider a single bit, that changes at some positions.
 * Find for each 1 position the rightmost 0 left of the 1.
 * Then, for each constraint for each bit not active in that
 * constraint, if the bit is set at the rightmost position of 
 * the constraint, the prev 0 must be within the contraint, too.
 * Else that constraint failed.
 */
void solve() {
    cini(n);
    cini(m);
    vector<pair<pii, pii>> evt;
    for(int i=0; i<m; i++) {
        cini(l);
        cini(r);
        cini(q);
        evt.emplace_back(make_pair(  l,-1), make_pair(r,q));    /* note reverse signs */
        evt.emplace_back(make_pair(r+1, 1), make_pair(l,q));
    }
    sort(all(evt));

    vi cnt(31); /* number of active constraints for that bit */
    vi lz(31);  /* position of last seen zero of that bit */
    vi ans(n+2, -1);
    for(size_t i=0; i<evt.size(); i++) {
        if(evt[i].first.second<0) {
            for(int j=0; j<30; j++) {
                int b=1LL<<j;
                if((evt[i].second.second&b)==0) {
                    if(cnt[j]==0)
                        lz[j]=evt[i].first.first;
                    continue;
                }

                if(cnt[j]==0)
                    lz[j]=evt[i].first.first-1;

                cnt[j]++;
            }
        } else if(evt[i].first.second>0) {
            for(int j=0; j<30; j++) {
                int b=1LL<<j;
                if((evt[i].second.second&b)!=0) {
                    cnt[j]--;
                    if(cnt[j]==0)
                        lz[j]=evt[i].first.first;
                } else {
                    if(lz[j]<evt[i].second.first) {
                        cout<<"NO"<<endl;
                        return;
                    }
                }
            }
        } else
            assert(false);

        int aans=0;
        for(int j=0; j<30; j++) {
            if(cnt[j])
                aans|=(1LL<<j);
        }
        ans[evt[i].first.first]=aans;
    }

    cout<<"YES"<<endl;
    ans[0]=0;
    for(int i=1; i<=n; i++) {
        if(ans[i]==-1)
            ans[i]=ans[i-1];
        cout<<ans[i]<<" ";
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
