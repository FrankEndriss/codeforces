/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* for nodes to be ascending we want to
 * have longest possible ascending seq on each level.
 *
 * Ok, but it is longest possible asc seq _per node_.
 * So we need to maintain the number of children of each node.
 *
 * On first iteration p==1, and the length of the asc seq starting
 * at a[1] is the number of children of 1.
 * And that is the number of nodes of level 1.
 */
void solve() {
    cini(n);
    cinai(a,n);

    assert(a[0]==1);
    vi nodesOnLevel(n+1);
    nodesOnLevel[0]=1;
    int p=1;    /* ith node on current level */

    int level=1;    /* level of current a[i] */

    bool first=true;    /* first node of new level */
    int ans=0;
    for(int i=1; i<n; i++) {
        if(first || a[i]>a[i-1]) {
            nodesOnLevel[level]++;
            ans=max(ans, level);
            first=false;
        } else {
            nodesOnLevel[level-1]--;
            if(nodesOnLevel[level-1]==0) {
                level++;
                nodesOnLevel[level]++;
                first=true;
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
