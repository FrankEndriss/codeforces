/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Looks like greedy.
 * Find the substring to make the longest possible prefix alternating, repeat.
 * Does this find optimal solution, why?
 *
 * for next symbol not alternating
 * do
 *   move the longest alternating seq ending in !s[0] to current position,
 *   by reverting the substr current,pos.
 * done
 * Still, is this optimal, why?
 * And second, how to implement this?
 * In O(n) find foreach position the len of the 
 * alternating seq ending at that pos.
 * 
 * Then ???
 * Why is s of even length?
 *
 * s[0] must be different than s[n-1]
 * let x be the last position with wrong symbol.
 * So if s[0]==s[1] then
 * Lets reverse s[1..x]... no.
 *
 * idk :/
 */
void solve() {
    cini(n);
    cins(s);

    vi dp(n);
    dp[0]=1;
    for(int i=1; i<n; i++) {
        if(s[i]==s[i-1])
            dp[i]=1;
        else
            dp[i]=dp[i-1]+1;
    }





}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
