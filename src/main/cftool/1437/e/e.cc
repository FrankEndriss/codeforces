/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * we have fixed elements in a[], and can 
 * change all others arbitrary.
 *
 * Find min possible changes to make a[] increasing.
 * So, it is the sum of min operations to make the 
 * segments between any two fixed elements ascending.
 *
 * How to do this in linear time? Or O(logn)?
 *
 * We would need foreach position find the min for the left half,
 * and min for the right half.
 * But that is O(n^2 log n)
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
    cinai(b,k);

    for(int i=1; i<k; i++) {
        int d=a[b[i]]-a[b[i-1]];
        if(d<b[i]-b[-1]) {
            cout<<-1<<endl;
            return;
        }
    }
    
    /* @return min operations making a[l],a[r] ascending,
     * with haveing a[l] a min value of x
     * and a[r] a max value of y.
     * foreach changeable a[i] there are two choices:
     * -do not change it
     * -change it to smallest value possible, ie a[i-1]+1
     */
    function<int(int,int)> go=[&](int l, int r) {
        if(r<=l)
            return 0LL;

        int ans=0;
        for(int idx=l+1; idx<r; idx++) {

        }
    };

    int ans=0;
    for(int i=1; i<k; i++)
        ans+=go(a[b[i]]+1, b[i]+1, b[i+1]-1);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
