/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* It is clear that the dishes must be put out of oven
 * in order of t[i].
 * But it is posssible that we want to do breaks in bettween.
 * Consider 1 4 4 4. Optimal minutes would be
 * 1 3 4 5
 * ... It is always optimal to use the order of t[i]
 * However.
 * It is never optimal to let the smallest t[i] longer in the oven
 * than it should.
 * So, sequence begins with a value <= min(t[i])
 *
 * Lets do a dp, starting first value at 0..min(t[i]), then each next value
 * with same constraint.
 *
 * N indicates O(n^3) solution ???
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(t,n);

    sort(all(t));

    vvi dp(2*n+1, vi(2*n, -1));
    /* @return min possible value for items starting at idx idx with minimum 
     * minute m */
    function<int(int,int)> go=[&](int m, int idx) {
        if(idx==n)
            return 0LL;

        if(dp[m][idx]>=0)
            return dp[m][idx];

        int ans=INF;
        const int mm=m;
        do {
            ans=min(ans, abs(t[idx]-m)+go(m+1, idx+1));
            m++;
        } while(m<=t[idx]);
        return dp[mm][idx]=ans;
    };

    const int ans=go(1, 0);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
