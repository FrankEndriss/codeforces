/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Say we got a given package size x.
 * That will work for customers who want to buy
 * x*3/2.
 * Example
 * x=60, customer want to buy l=90, then 90%60=30,
 * ans 30>=60/2
 *
 * x=60, l=100, then 100%60=40
 * ans 40>=60/2
 * ...
 *
 * So for given l, a good x is (l*2)/3
 * What about rounding?
 * 61*2/3==122/3=40
 * -> no, so 
 * x=((l*2)+2)/3
 *
 * And that works until 2*x-1
 *
 * Also x can be bigger than r, if x/2 is bgeq l...
 *
 * I dont like this one :/
 */
void solve() {
    cini(l);
    cini(r);

    if(l*2>r)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
