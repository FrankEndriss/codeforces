/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Ok, its a math student competition.
 * First, understand the question.
 * We got a tree with labeled nodes.
 * Each path in this tree has a value, that is the MEX of all the 
 * vertex on that path.
 * ie all path without the vertex 0 have MEX==0
 * All with vertex 0 but without vertex 1 have value 1 and so on.
 * How to count the paths?
 *
 * Number of all paths is (n*n-1)/2
 * Number of paths with vertex 0 is product of all pairs of
 * sizes of adj of vertex 0. (how to find that number in less than O(n^2)?)
 *
 * Also it is foreach adj of vertex 0 let s[i] be the size of the ith adj of v0.
 * then path without v0 is sum( s[i]*(s[i]-1)/2 )
 *
 * Then same for v1. But how to include/exclude the paths of v0?
 *
 * Then, how to find the number of paths with v0 and v1, but without v2?
 * And so on?
 */
void solve() {
    cout<<"hello darkness, my old friend"<<endl;
}

signed main() {
    solve();
}
