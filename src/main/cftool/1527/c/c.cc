/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * How comes the subsequence into play?
 * We need to count foreach group the number of subsequences
 * of that group and size.
 *
 * Consider the positions of all 1s, say x of them.
 * Then we can remove each combination of prefix and suffix of sizes up to
 * the the first/last occ of a '1' in the string to get the full number.
 * says first is at 3, and last at n-1-2, so we can remove pre of len
 * 0, 1, 2, and post of len 0,1 which makes 6 subseqs.
 * Same for x-1, but with inclusion/exclusion.
 *
 * Then we need to optimize this to be less than O(n^2)
 *
 * We have lists of indexes, and foreach size of a subarray
 * we build the sum over the product of prefixes and postfixes.
 * How to get the sum of the products of these pairs
 * without iterating them?
 *
 * We need to setup somehow the prefix and/or postfix sums
 * and considering the multiplication by m[len]
 * idk :/
 */
const int N=1e5+7;
void solve() {
    cini(n);
    cinai(a,n);

    map<int,vi> idx;
    for(int i=0; i<n; i++) 
        idx[a[i]].push_back(i);

    vi m(N);
    for(int i=2;i<N; i++) 
        m[i]=i*(i-1)/2;

    int ans=0;
    for(auto ent : idx) {
        if(ent.second.size()<2)
            continue;

        for(size_t j=0; j<ent.second.size(); j++) {
            int lans=0;
            int prev=-1;
            if(j>0)
                prev=ent.second[j-1];

            for(size_t k=2; j+k<=ent.second.size(); k++) {
                int next=n;
                if(j+k<ent.second.size())
                    next=ent.second[j+k];

                lans+=(ent.second[j]-prev)*(next-ent.second[j+k-1])*m[k];
                //cerr<<"k="<<k<<" j="<<j<<" ans="<<ans<<endl;
            }
            ans+=lans;
        }
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
