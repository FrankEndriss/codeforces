/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Strange rules...sic casework, no fun.
 *
 * Player wants to make the string a palindrome,
 * because then the other play must change it with cost 1
 * to a not palindrome, and player can reverse it.
 * So, strategy is, whenever possible reverse, else
 * make a plindrome else make a string where the other player
 * cannot make a palindrome.
 *
 * The simple/hard implies that once the string is a palindrome
 * there is kind of simple loop...somehow :/
 *
 * Consider the first player cannot make a plindrome, since we 
 * start with a palindrome. So he has cost==1.
 * We need to consider two kinds of palindromes, the ones with
 * 1 in the center and the ones with 0 in the center.
 *
 * Case 1 in center, the second player can allways make the opposite
 * move to the first player, so second wins.
 * Case 0 in center, the first player puts a 1 there and the string is
 * still a plindrome.
 * So if number of 0 is even then Draw, else second wins.
 */
void solve() {
    cini(n);
    cins(s);

    int cnt=0;
    for(char c : s)
        if(c=='0')
            cnt++;

    if(cnt==0) {
        cout<<"DRAW"<<endl;
        return;
    }

    if(n%2==0) {
        cout<<"BOB"<<endl;
    } else {
        if(s[n/2]=='0') {
            if(cnt%2==0)
                cout<<"DRAW"<<endl;
            else
                cout<<"BOB"<<endl;
        } else {
            cout<<"BOB"<<endl;
        }
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
