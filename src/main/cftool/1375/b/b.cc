/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));

    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            cin>>a[i][j];

    bool ok=true;
    if(a[0][0]>2)
        ok=false;
    else
        a[0][0]=2;

    if(a[n-1][0]>2)
        ok=false;
    else
        a[n-1][0]=2;

    if(a[0][m-1]>2)
        ok=false;
    else
        a[0][m-1]=2;

    if(a[n-1][m-1]>2)
        ok=false;
    else
        a[n-1][m-1]=2;

    for(int i=1; i<n-1; i++)  {
        if(a[i][0]>3)
            ok=false;
        else
            a[i][0]=3;

        if(a[i][m-1]>3)
            ok=false;
        else
            a[i][m-1]=3;
    }

    for(int j=1; j<m-1; j++) {
        if(a[0][j]>3)
            ok=false;
        else
            a[0][j]=3;

        if(a[n-1][j]>3)
            ok=false;
        else
            a[n-1][j]=3;
    }

    for(int i=1; i<n-1; i++)
        for(int j=1; j<m-1; j++) {
            if(a[i][j]>4)
                ok=false;
            else
                a[i][j]=4;
        }

    if(!ok)
        cout<<"NO"<<endl;
    else {
        cout<<"YES"<<endl;
        for(int i=0; i<n; i++) {
            for(int j=0; j<m; j++)
                cout<<a[i][j]<<" ";
            cout<<endl;
        }
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
