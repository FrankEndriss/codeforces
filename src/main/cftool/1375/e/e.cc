/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need to sort all inversions
 * of a[] in a way that after doing
 * all the swaps, the array is sorted.
 *
 * swap removes only one inversion.
 * This is, the distance (of values) must be minimized.
 *
 * group swaps by target.
 * from all in one group the one which puts the right one in 
 * place comes last.
 * Then go target from right to left, execute each group.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi aa=a;
    sort(all(aa));

    vector<pii> ans;
    //vector<pii> inv;
    vvi inv(n);   /* inv[i]=list of j for position i */
    for(int i=0; i<n; i++)
        for(int j=i+1; j<n; j++) 
            if(a[i]>a[j])
                inv[i].push_back(j);

    for(int i=0; i<n; i++) {
/* execute all inversion for position i, with last one so 
 * that the element aa[i] finishes there. Note that aa[] is
 * the smallest of all elements in the list of swaps of 
 * position i.
 */
        sort(all(inv[i]), [&](int i1, int i2) {
            return a[i2]<a[i1];
        });

        for(int j : inv[i]) {
assert(j>i);
            ans.emplace_back(i,j);
            swap(a[i], a[j]);
/*
cerr<<"swap i="<<i<<" j="<<j<<endl;
cerr<<"a[]=";
for(int i : a) 
    cerr<<i<<" ";
cerr<<endl;
*/
        }
    }

/*
cerr<<"a[]=";
for(int i : a) 
    cerr<<i<<" ";
cerr<<endl;
*/
    for(int i=0; i<n; i++) {
        if(a[i]!=aa[i]) {
            cout<<-1<<endl;
            return;
        }
    }

    cout<<ans.size()<<endl;
    for(int i=0; i<ans.size(); i++)
        cout<<ans[i].first+1<<" "<<ans[i].second+1<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
