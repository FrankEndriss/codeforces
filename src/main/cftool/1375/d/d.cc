/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* greedy, two phases
 * 1. always put the MEX to the postion is is the number (0 based)
 * 2. then if sorted ok, else put the MEX to the pos-1.
 *    this will end in 1, 2, 3,...,n
 *
 * other strat:
 * Put right numbers in right positions until 0 is free
 * We need to put the 0 in first position.
 *
 * ****
 * subst all 0 by other numbers, if done
 * write 0 at pos 0.
 * subst all 1 by other numbers, if done
 * write 1 at pos 1
 * ...
 *
 * idk :/
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    map<int,vi> f;

    set<int> s;
    for(int i=0; i<2*n; i++)
        s.insert(i);

    for(int i=0; i<n; i++) {
        f[a[i]].push_back(i);
        s.erase(a[i]);
    }

    vi ans;
    for(int i=0; i<n; i++) {
        if(a[i]==i)
            continue;

        for(int pos : f[i]) {
            ans.push_back(pos+1);
            auto it=s.begin();
            a[pos]=*it;
            f[a[pos]].push_back(pos);
            s.erase(it);
        }
        f[i].clear();

        a[i]=i;
        ans.push_back(i+1);
    }

cerr<<"a[]=";
for(int i : a)
    cerr<<i<<" ";
cerr<<endl;

    cout<<ans.size()<<endl;
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
