/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* Clearly its a dp.
 * dp[i][j][k]=max beauty we can get if next pic must be posted latest at i
 * and we continue to check pos j and placing k more pictures.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(kk);
    cini(x);
    cinai(a,n);

    vvvi dp(n+1+kk, vvi(n+1, vi(x+1, -INF-INF)));

    function<int(int,int,int)> go=[&](int i, int j, int k) {
        if(dp[i][j][k]>=-INF)
            return dp[i][j][k];

        if(j>i)
            return dp[i][j][k]=-INF;

        if(k==0) {
            if(i<n)
                return dp[i][j][k]=-INF;
            else
                return dp[i][j][k]=0;
        }

        if(j==n)
            return dp[i][j][k]=-INF;

            /* post current pic */
        int ans=a[j]+go(j+kk, j+1, k-1);
            /* do not post current pic */
        return dp[i][j][k]=max(ans, go(i, j+1, k));
    };

    int ans=go(kk-1, 0, x);
    if(ans<0)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
    solve();
}
