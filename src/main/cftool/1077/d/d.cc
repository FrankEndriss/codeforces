/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need to find the kth biggest freq.
 * corner:
 * Prob is definined in a way that if kth freq
 * is lteq biggest one, we can make two half sized
 * ones from the biggest.
 * Brute force?
 * We need to maintain a set of the choosen elements
 * <avail=f[c]/cnt, cnt, c>
 * ...to complected. Tutorial purposes binsearch.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(s,n);
    map<int,int> f;
    for(int c : s)
        f[c]++;

    vector<pii> ff;
    for(auto ent : f)
        ff.emplace_back(ent.second, ent.first);

    sort(all(ff), greater<pii>());

/* @return true if k cuts are possible */
    function<bool(int)> canCut=[&](int kk) {
        int cnt=0;
        for(int i=0; i<ff.size(); i++) {
            cnt+=ff[i].first/kk;
        }
        return cnt>=k;
    };

    int l=1;
    int r=1e9;
    while(l+1<r) {
        int mid=(l+r)/2;
        if(canCut(mid))
            l=mid;
        else
            r=mid;
    }

    int cnt=0;
    for(int i=0; cnt<k && i<ff.size(); i++) {
        for(int j=0; cnt<k && j<ff[i].first/l; j++)  {
            cout<<ff[i].second<<" ";
            cnt++;
        }
    }
    cout<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
