/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * ok, it is common _side or corner_
 */
void solve() {
    cini(h);
    cini(w);

    vs ans(h, string(w, '0'));
    for(int i=0; i<w; i+=2) 
        ans[0][i]='1';

    int i=2;
    for(; i<h; i+=2)
        ans[i][w-1]='1';

    i=w-3;
    for(; i>=0; i-=2) 
        ans[h-1][i]='1';

    i=h-3;
    for(; i>=2; i-=2) {
        ans[i][0]='1';
    }

    for(int i=0; i<h; i++) 
        cout<<ans[i]<<endl;
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
