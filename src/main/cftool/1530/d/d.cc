
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Every emp that is wished can be satisfied.
 * Then simply fullfill all others.
 *
 * ...unfortunatly it is not that simple :/
 *
 * See first testcase, the second '2' in input
 * needs to be fullfilled. Because fullfilling the first
 * does include not fulltilling the other two.
 * But fullfilling the second makes it possible to fullfill
 * one more.
 * So, how to know which ones need to be fullfilled in first place?
 * Somehow we need to analyse components and loops...
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi b(n, -1);
    vb vis(n);

    vi self;
    for(int i=0; i<n; i++) {
        a[i]--;
        if(!vis[a[i]] && a[i]!=i) {
            b[i]=a[i];
            vis[a[i]]=true;
        }
    }

    /* assign all others and collect self assigns. */
    int idx=0;
    for(int i=0; i<n; i++) {
        if(b[i]<0) {
            while(vis[idx])
                idx++;

            b[i]=idx;
            vis[idx]=true;
            if(i==idx)
                self.push_back(i);
        }
    }
    cerr<<"self size="<<self.size()<<endl;
    for(int i : self) 
        cerr<<i<<" ";
    cerr<<endl;

    /* handle selfs */
    if(self.size()==1) {
        b[self[0]]=(self[0]+1)%n;
        b[(self[0]+1)%n]=self[0];
    } else {
        for(size_t i=0; i<self.size(); i++) {
            assert(b[self[i]]==self[i]);
            b[self[i]]=self[(i+1)%self.size()];
        }
    }

    int ans=0;
    for(int i=0; i<n; i++) 
        if(a[i]==b[i])
            ans++;

    cout<<ans<<endl;
    for(int i=0; i<n; i++) 
        cout<<b[i]+1<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
