
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;


const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

const S INF=1e9;
S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/* After did 4 stages, the worst is thrown away.
 *
 * Just simulate with a prio queue.
 * Consider getting 100 foreach stage and opponent getting 0.
 *
 * ***
 * ...simulation is not so simple, we cannot simply throw away results.
 *
 * Result after j stages is:
 * pre[j]-pre[j/4]
 *
 * -> Use a segtree to get those prefix sums quick.
 * Or does it work simpler?
 *
 * on every n%4==0
 * Opponent get added his thrown away scors in order.
 * Me getting 100-worst result
 */
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));
    cinai(b,n);
    sort(all(b));

    const int N=n*3;
    stree sa(N);
    stree sb(N);

    const int base=n;

    for(int i=0; i<n; i++) {
        sa.set(base+i, a[i]);
        sa.set(base+base+i, 100);
        sb.set(base+i, b[i]);
    }

    int ans=0;
    while(true) {
        int suma=sa.prod(base+(ans+n)/4, base+ans+n);
        int sumb=sb.prod(base-ans+(ans+n)/4, base-ans+ans+n);

        if(suma>=sumb)
            break;

        ans++;
    }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
