/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Subsequence, not substring :/
 * if there are a then
 * move all c before all b.
 */
void solve() {
    cins(s);
    cins(t);

    sort(all(s));

    int cntA=0;
    int cntB=0;
    int cntC=0;
    for(char c : s)
        if(c=='c')
            cntC++;
        else if(c=='b')
            cntB++;
        else if(c=='a')
            cntA++;


    if(cntA==0 || cntB==0 || cntC==0) {
        cout<<s<<endl;
        return;
    }

    if(t[0]!='a')
        cout<<s<<endl;
    else {
        if(t[1]!='b')
            cout<<s<<endl;
        else {  // t=="abc"
            for(int i=0; i<cntA; i++)
                cout<<'a';
            for(int i=0; i<cntC; i++)
                cout<<'c';
            for(int i=0; i<cntB; i++)
                cout<<'b';
            for(int i=cntA+cntB+cntC; i<s.size(); i++) 
                cout<<s[i];
            cout<<endl;
        }
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
