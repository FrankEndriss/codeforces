/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


string test="0001101000001";

int ask(int a, int b, int c) {
    cerr<<"a="<<a<<" b="<<b<<" c="<<c<<endl;
    assert(a>=0 && a<test.size());
    assert(b>=0 && b<test.size());
    assert(c>=0 && c<test.size());
    //cout<<"? "<<a+1<<" "<<b+1<<" "<<c+1<<endl;
    //cini(ans);
    //return ans;
    return (test[a]-'0'+test[b]-'0'+test[c]-'0')>1;
}

/**
 * 100110000001
 * Find the 1s
 *
 * Ask 1,2,3; 2,3,4...
 * Will possibly give allways 1.
 *
 * Consider n/3+1 imposters.
 * How to find _any_ triple with at least 2 imposters in 2n queries?
 *
 * Ok, ask all triples, there must be one with at least two imp, n/3 queries.
 * And one with at most one imp.
 *
 * Combine both sets to identifiy one imposters and one crewmate.
 * Then ask all other indexes with help of that two.
 *
 * Consider having two independent 1 and 0 triples at i0 ant i1
 *
 * ...bad casework :/
 *
 */
void solve() {
    cini(n);

    int i1=-1;  /* at least two */
    int i0=-1;  /* at most one */
    for(int i=0; i2<0 || i1<0; i+=3) {
        int val=ask(i+0, i+1, i+2);
        if(val==0) 
            i0=i;
        else
            i1=i;
    }
    cerr<<"i0="<<i0<<" i1="<<i1<<endl;
    assert(i0>=0);
    assert(i1>=0);

    int ii0=-1;
    int ii1=-1;
    for(int i=0; i<3; i++) {
        /* if all three queries==1 then a[i1+i]==1 
         * else if one queries==1  then a[i1+i]==0
         **/
        int val=ask(i1+i, i2+0, i2+1)+ask(i1+i, i2+0, i2+2)+ask(i1+i, i2+1, i2+2);
        if(val==3) {
            ii1=i1+i;
        } else if(val==1) 
            ii0=i1+i;
    }

    assert(ii0>=0);
    assert(ii1>=0);
    cerr<<"ii0="<<ii0<<" ii1="<<ii1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
