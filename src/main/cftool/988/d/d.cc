/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * All choosen numbers must have same distance.
 * distance must be power of 2, ie 1,2,4,8,...
 *
 * Wrong! There cannot be sets bigger than 3 elements.
 * The distance must be 2^x and 2^x and 2^(x+1)
 *
 * So, starting from any value find a double.
 * Or triple and output it.
 */
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));

    map<int,int> pos;
    for(int i=0; i<n; i++)
        pos[a[i]]=i;

    int ans=1;
    int ansi;
    int ansj;
    const int INF=1e12+7;
    for(int j=1; j<INF; j<<=1) {
        for(int i=0; i+1<n; i++) {
            auto it=pos.find(a[i]+j);
            if(it!=pos.end()) {
                ans=2;
                ansi=i;
                ansj=j;

                auto it2=pos.find(a[i]+j+j);
                if(it2!=pos.end()) {
                    cout<<3<<endl;
                    cout<<a[i]<<" "<<a[i]+j<<" "<<a[i]+j+j<<endl;
                    return;
                }
            }
        }
    }

    if(ans==2) {
        cout<<2<<endl;
        cout<<a[ansi]<<" "<<a[ansi]+ansj<<endl;
    } else {
        cout<<1<<endl;
        cout<<a[0]<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
