/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << "=" << a << " ";
	logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

    const int MOD=1e9+7;

/* build set of prime divisors */
void primes(ll x, vi &ans) {
    for(int i=2; i*i<=x; i++) {
        if(x%i==0)
            ans.push_back(i);
        while(x%i==0)
            x/=i;
    }
    if(x>1)
        ans.push_back(x);
}

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}


int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

/* 
 * let x1 be first primefactor.
 * it contributes to ans with 
 * k==2 -> min(n, x1^3)-x1^2)/x1+1 times
 * k==3 -> min(n, x1^4)-x1^3 times 
 * NO!!
 *
 * We need to find the biggest k such that x^k <=n. That one contributes
 * 1 time.
 * x^(k-1) contributes x-1 times
 * x^(k-2) contributes x*x-(x-1)-1 times
 * But we need to consider the numbers (x^k,n), too.
 * ...
 * So count for every x the frequencies of the k,
 * and multiplicate.
 * We need to take care for overflow.
 */
void solve() {
    cini(x);
    cini(n);

    vi pr;
    primes(x,pr);

    int ans=1;
    for(int pp : pr) {
        if(pp>n)
            continue;

        int num=pp;
        int cnt=0;

        while(num<=n) {
            cnt+=n/num;
            //cnt%=MOD; We must not mod the power!
            long double dnum=num;    // overflow prediction
            dnum*=pp;
            if(dnum>n) {
                break;
            }
            num*=pp;
        }
        ans*=toPower(pp, cnt, MOD);
        ans%=MOD;
    }

    cout<<ans<<endl;
}

signed main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
		solve();
}

