/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Note
 * It is not enough to check if corners of 
 * rects are in each other.
 * We also must check the center.
 */
void solve() {
    vi x1(4);
    vi y1(4);
    vi x2(4);
    vi y2(4);

/* @return true if point in rect 1 */
    auto inR=[&](int x, int y, vi& xx, vi& yy) {
        int xmi=*min_element(all(xx));
        int xma=*max_element(all(xx));
        int ymi=*min_element(all(yy));
        int yma=*max_element(all(yy));
        return x>=xmi && x<=xma && y>=ymi && y<=yma;
    };

/* @return true if point in 45 deg rot rect 2 */
    auto inR45=[&](int x, int y) {
        vi x3=x2;
        vi y3=y2;
        for(int i=0; i<4; i++) {
            x3[i]=x2[i]-y2[i];
            y3[i]=x2[i]+y2[i];
        }
        int xx=x-y;
        int yy=x+y;
        return inR(xx, yy, x3, y3);
    };

    auto cent=[&](vi& x, vi& y) {
        int xmi=*min_element(all(x));
        int xma=*max_element(all(x));
        int ymi=*min_element(all(y));
        int yma=*max_element(all(y));
        return make_pair((xmi+xma)/2, (ymi+yma)/2 );
    };

    for(int i=0; i<4; i++)  {
        cin>>x1[i]>>y1[i];
        x1[i]*=2;
        y1[i]*=2;
    }
    for(int i=0; i<4; i++)  {
        cin>>x2[i]>>y2[i];
        x2[i]*=2;
        y2[i]*=2;
    }

    for(int i=0; i<4; i++) {
        if(inR(x2[i], y2[i], x1, y1) || inR45(x1[i], y1[i])) {
            cout<<"YES"<<endl;
            return;
        }
    }
    auto [x,y]=cent(x1,y1);
    if(inR45(x,y)) {
        cout<<"YES"<<endl;
        return;
    }
    auto [xx,yy]=cent(x2,y2);
    if(inR(xx, yy, x1, y1)) {
        cout<<"YES"<<endl;
        return;
    }
    cout<<"NO"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
