/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+7;

/* zebroid subseq
 * # staring at i
 * 1*(n-i+1)/2*
 * Assume marbles start with 0.
 */
void solve() {
    cini(n);

    if(n==1) {
        cout<<1<<endl;
        return;
    }

    vi dp0(n); /* dp0[i]=number of subseqs ending with 0 at pos i */
    vi dp1(n); /* dp1[i]=number of subseqs ending with 1 at pos i */
    dp0[0]=1;
    dp0[1]=1;
    dp1[0]=0;
    dp1[1]=2;
    for(int i=2; i<n; i++) {
        dp0[i]=dp0[i-1];
        dp1[i]=dp1[i-1];
        if(i%2==0)
            dp0[i]+=dp1[i-1]+1;
        else
            dp1[i]+=dp0[i-1]+1;

        dp0[i]%=MOD;
        dp1[i]%=MOD;
    }
    int ans=(dp0[n-1]+dp1[n-1])%MOD;
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
