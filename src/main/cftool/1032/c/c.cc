/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp[i][j]=true if possible to play note i with finger j
 */
void solve() {
    cini(n);
    cinai(a,n);

    vvi p(n, vi(5,-1));        /* p[i][j]=previous finger if played note i with finger j */
    for(int i=0; i<5; i++) 
        p[0][i]=0;


    for(int i=1; i<n; i++) {
        if(a[i-1]<a[i]) {
            for(int j=0; j<5; j++)  {
                if(p[i-1][j]>=0) {
                    for(int k=j+1; k<5; k++) 
                        p[i][k]=j;
                }
            }
        } else if(a[i-1]>a[i]) {
            for(int j=0; j<5; j++) {
                if(p[i-1][j]>=0)  {
                    for(int k=0; k<j; k++) 
                        p[i][k]=j;
                }
            }
        } else if(a[i]==a[i-1]) {
            for(int j=0; j<5; j++)  {
                if(p[i-1][j]>=0) {
                    for(int k=0; k<5; k++) 
                        if(k!=j)
                            p[i][k]=j;
                }
            }
        }
    }

    for(int k=0; k<5; k++) {
        if(p[n-1][k]>=0) {
            stack<int> ans;
            ans.push(k);
            int next=k;
            for(int i=n-1; i>0; i--) {
                next=p[i][next];
                ans.push(next);
            }
            while(ans.size()) {
                cout<<ans.top()+1<<" ";
                ans.pop();
            }
            cout<<endl;
            return;
        }
    }
    cout<<-1<<endl;
}

signed main() {
        solve();
}
