/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* We need to collect parities of k[] freqs.
 * put biggest k into one set, all other
 * in other set.
 */
void solve() {
    cini(n);
    cini(p);

    map<int,int> f;
    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux]++;
    }

    if(p==1) {
        int ans=0;
        for(auto ent : f)
            ans+=ent.second;
        cout<<ans%2<<endl;
        return;
    }

    int ans=0;
    int ma=-1;
    for(auto it=f.rbegin(); it!=f.rend(); it++) {
        if(it->second%2) {
            ans=pl(ans, toPower(p,it->first));
            ma=it->first;
            break;
        }
    }

    if(ma==-1) {
        cout<<0<<endl;
        return;
    }

    for(auto it=f.begin(); it!=f.end(); it++) {
        if(it->first>=ma)
            break;
        ans=pl(ans, -mul(it->second, toPower(p, it->first)));
    }
    cout<<ans<<endl;
    return;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
