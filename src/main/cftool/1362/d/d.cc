/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* So, if we write the blogs sorted by topic numbers
 * that should do the trick. Not?
 *
 * We need to output the blog numbers.
 *
 * We sort the blogs by desired topic-number, then
 * start writing the topics[1], then topics[2] etc
 * before each write we check if possible.
 */
void solve() {
    cini(n);
    cini(m);
    vvi adj(n+1);
    for(int i=0; i<m; i++) {
        cini(a);
        cini(b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    vvi t(n+1);
    for(int i=1; i<=n; i++) {
        cini(aux);
        t[aux].push_back(i);
    }

    vi ans;
    vi blogs(n+1, -1);      /* topics of blogs */
    bool ok=true;
    for(int i=1; i<=n; i++) {   /* write blogs with topic i */
        for(int bl : t[i]) {
            blogs[bl]=i;
/* check if possible
 * adj must contain all smaller numbers, and must not contain i.
 */
            set<int> cov;
            for(int chl : adj[bl]) {
                if(blogs[chl]==i)
                    ok=false;

                if(blogs[chl]>=1)
                    cov.insert(blogs[chl]);
            }
            if(cov.size()!=i-1)
                ok=false;

            ans.push_back(bl);
        }
    }

    if(!ok)
        cout<<-1<<endl;
    else {
        for(int i=0; i<n; i++)
            cout<<ans[i]<<" ";
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
