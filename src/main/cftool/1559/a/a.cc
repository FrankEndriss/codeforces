/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Brute force
 * Note that intervals of size 0 or 1 do not change any value.
 *
 * What is wrong?
 * -> _any number of times_
 * So it is a completly other problem.
 *
 * ->
 * Since we can pair any number with any other number we can remove
 * any bit that is not set in all numbers.
 *
 * Unfortunatly that was unclear for to long time, so I am kind of out :/
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans=a[0];
    for(int i=1; i<n; i++)
        ans&=a[i];

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
