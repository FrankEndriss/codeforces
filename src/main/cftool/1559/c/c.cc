/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * I think I do not understand the question.
 * -> We can allways go 1,2,3,...,n+1
 * But that is to trivial :/
 * ...ok, the n-1 roads are only up to village n, but 
 * we cannot simply go to village n+1.
 * So we must at some point go to n+1, and from there back...
 * and find a hamiltonian path.
 * How to do that?
 *
 * We cannot simply search hamiltonian paths, since to slow.
 * So we somehow need to take advantage of the "funny" defintion
 * of the edges.
 *
 * idk :/
 */
void solve() {
    cini(n);
    cinai(a,n);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
