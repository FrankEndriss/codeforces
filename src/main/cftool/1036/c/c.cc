/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** good for up to ~100 */
ll nCr(int n, int k) {
    ld res = 1;
    for (int i = 1; i <= k; ++i)
        res = res * (n - k + i) / i;

    cerr<<"nCr("<<n<<","<<k<<")="<<(ll)(res + 0.01)<<endl;
    return (ll)(res + 0.01);
}

/*
 * dp[i][j]= number of numbers with i digits!=0 and j digits at all.
 * dp[1][i]=9
 * dp[i][i]=9
 * dp[2][3]=9*9*2 --since there are two posible positions for the second digit
 * dp[2][i]=9*9*(i-1)
 *
 * dp[3][3]=9*9*9;
 * dp[3][i]=9^3 * nCr(i-1,2)
 *
 * Then we need to find how much classy numbers of len d(n) are not bigger than n.
 * This is foreach digit smaller than first digit of n
 * 9^2 * nCr(i-1,2), since each number where the first digit is smaller is smaller at all.
 * Then, the numbers with the same first digit, we
 * iterate the second digit position by position.
 *
 * Each digit can be 0 to 9 at each position.
 * Foreach position i we need to add the cases that we place
 * the digits 0..d[i]
 * In case we place a 0 we overcount the possibilities among the
 * positoins, so we do it only the first time.
 * ...
 * But this combinatorical solution is to complecated for me,
 * still did not get it after some hours of trying.
 *
 * So implement the "brute force", that is, generate all (~700k)
 * numbers, and binsearch the result.
 */

vi a;
void init() {
    vi l(20);
    l[0]=1;
    for(size_t i=1; i<l.size(); i++)
        l[i]=l[i-1]*10;

    for(int i=0; i<18; i++) {
        for(int i0=1; i0<=9; i0++) {
            a.push_back(i0*l[i]);
            for(int j=0; j<i; j++) {
                for(int j0=1; j0<=9; j0++) {
                    a.push_back(i0*l[i]+j0*l[j]);
                    for(int k=0; k<j; k++) {
                        for(int k0=1; k0<=9; k0++) {
                            a.push_back(i0*l[i]+j0*l[j]+k0*l[k]);
                        }
                    }
                }
            }
        }
    }
    a.push_back(l[18]);
}

int solve1(int n) {
    return distance(a.begin(), upper_bound(all(a), n));
}

void solve() {
    cini(l);
    cini(r);
    int ans=solve1(r)-solve1(l-1);
    cout<<ans<<endl;
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
