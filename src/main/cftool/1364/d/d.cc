/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

struct Dsu {
    vector<int> p;
    int cnt;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        iota(p.begin(), p.end(), 0);
        cnt=size;
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            cnt--;
            p[b] = a;
        }
    }
};

/* we can check components with dsu
 * if more than k components use first.
 *
 * Else search the smallest cycle, report.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    Dsu dsu(n);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;

        dsu.union_sets(u,v);
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    if(dsu.cnt>=(k+1)/2) {
        cout<<1<<endl;
        vb vis(n);
        int cnt=0;
        for(int i=0; cnt<(k+1)/2 && i<n; i++)  {
            int s=dsu.find_set(i);
            if(!vis[s]) {
                vis[s]=true;
                cout<<i+1<<" ";
            }
        }
        cout<<endl;
    }

/* now find a small cycle.
 * start anywhere doing dfs, memoizing p[i] and depth[i] */

    vi p(n,-1);
    vi d(n,-1);

    int mi=1e9;
    int ansc;
    function<void(int,int)> dfs=[&](int v, int pp) {
        if(d[v]>=0) {
            int clen=d[pp]-d[v]+1;
            if(clen<mi) {
                mi=clen;
                ansc=v;
                p[v]=pp;
                return;
            }
        }
        if(pp>=0) {
            p[v]=pp;
            d[v]=d[pp]+1;
        }

        for(int chl : adj[v])
            if(chl!=pp)
                dfs(chl,v);
    };
    
    d[0]=0;
    dfs(0, -1);

    cout<<2<<endl;
    cout<<mi<<endl;
    for(int i=0; i<mi; i++) {
        cout<<ansc<<" ";
        ansc=p[ansc];
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
