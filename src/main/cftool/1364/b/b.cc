/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* minlen==2
 * subseq
 * We need to find the most alternating subseq.
 * dp ???
 *
 * We can start with 1,n and n,1
 * If p is sorted then we are done.
 * Asume pos[1]<pos[n]
 * If not sorted there is some a[k]>a[k+j], ie bigger value 
 * left of smaller one, then we can add those two.
 *
 * Note:
 * If first element is not in subseq, subseq allways gets better if
 * first element is added. So always start with first element.
 *
 * Calc from right to left...
 * dp[i]=max(dp[i+1]+abs(a[i]-a[i+1]), ..., dp[n-1]+a[n-1]-a[i])
 * this is O(n^2) :/
 *
 * What about that permutation, why is it one?
 *
 * Greedy!
 * Start at first, find biggest while asc or dsc,
 * then alternate!
 */
void solve() {
    cini(n);
    cinai(p,n);

    vi ans;
    ans.push_back(p[0]);
    bool asc=p[1]>p[0];

    for(int i=1; i<n-1; i++) {
        if(asc && p[i]>p[i+1]) {
            ans.push_back(p[i]);
            asc=!asc;
        } else if(!asc && p[i]<p[i+1]) {
            ans.push_back(p[i]);
            asc=!asc;
        }
    }
    ans.push_back(p[n-1]);

    cout<<ans.size()<<endl;
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
