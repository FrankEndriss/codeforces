/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Example:
 * a={0}
 * b={1}
 *
 * a={0,1}
 * b={2,0}
 *
 * a={1}
 * b={0}
 *
 * a={2}
 * b=impossible
 *
 * foreach a[i] there must be a[i] elements in b smaller than a[i]
 * So go from back:
 * for a[n-1] put all numbers smaller than a[n-1] in the set.
 * What if to small? -> put larger ones into the set.
 */
void solve() {
    cini(n);
    cinai(a,n); // sorted

    set<int> b;
    for(int i=0; i<a.back(); i++)
        b.insert(i);

    int num=a[n-1]+1;
    while(b.size()<n)
        b.insert(num++);

    vi ans(n,-1);
    for(int i=n-2; i>=0; i--) {
        if(b.count(a[i])) {
            ans[i+1]=a[i];
            b.erase(a[i]);
        } else {
            auto it=b.rbegin();
            ans[i+1]=*it;
            b.erase(*it);
        }
    }
    ans[0]=*b.begin();

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
