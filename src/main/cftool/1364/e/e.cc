/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int query(int i, int j) {
    cout<<i<<" "<<j<<endl;
    cini(ans);
    return ans;
}

/* So one of the elements is 0, find it, then 
 * query all others with it.
 *
 * How to find 0?
 * How to find any pair with result not all bits set?
 * Say we query 1,2; 3,4; etc.
 * and all get allbits. 1024 queryies.
 * Then 1,3; 2,4; 5,7;...
 * For these pairs at least one bit is not set.
 * put the numbers in a set denoting the missing bits.
 * then use pairs of numbers from.
 *
 * For every number every bit has three states:
 * 0, 1, unknown
 *
 * We need to combine numbers with mostly independent unknown 
 * bits. So we make them mostly known.
 * Once found the zero everything is simple.
 *
 * How to match the mostly independent unknown set?
 * ->Count known bits in pairs.
 *
 *  ... to complecated
 */

struct num {
    vi bits;
    int known;
    int idx;
    num(int bb, int pidx) {
        bits.resize(bb, -1);
        idx=pidx;
        known=0;
    }

    void set(int lidx, int val) {
        assert(bits[lidx]==-1);
        assert(val==0 || val==1);
        bits[lidx]=val;
        known++;
    }

    bool operator<(num& other) {
        if(other.known==known)
            return other.bits<bits;
        return other.known<known;
    }
};

struct numpair {
    num n1;
    num n2;

    numpair(num& pn1, num& pn2):n1(pn1), n2(pn2) {
    }

    bool operator<(const numpair& other) const {
        return other.known0()<known0();
    }

    const int known0() const {
        int ans=0;
        for(int i=0; i<n1.bits.size(); i++) {
            if(n1.bits[i]==0 || n2.bits[i]==0)
                ans++;
        }
        return ans;
    }
};

void solve() {
    cini(n);

    int b=1;
    while((1<<b)<n)
        b++;

    vector<num> nums;
    for(int i=0; i<n; i++)
        nums.emplace_back(b, i);

    priority_queue<numpair> q;
    while(true) {
        for(int i=0; i<n; i++) {
            for(int j=i+1; j<n; j++) {
                q.insert(numpair(nums[i], nums[j]));
            }
        }
    }
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
