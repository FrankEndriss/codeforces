/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

template<typename T>
void floyd_warshal(vector<vector<T>> &d, int n) {
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}

/*
 * We search a subseq of p[], with the property that:
 * Whenever a vertex is on the shortest path from current
 * vertex to next vertex, that vertex is removed.
 */
const int INF=1e9;
void solve() {
    cini(n);
    vvi d(n, vi(n, INF));
    for(int i=0; i<n; i++) {
        d[i][i]=0;
        cins(s);
        for(int j=0; j<n; j++) {
            if(s[j]=='1')
                d[i][j]=1;
        }
    }
    cini(m);
    cinai(p,m);
    for(int i=0; i<m; i++)
        p[i]--;

    floyd_warshal(d, n);

    vi ans;
    int cur=0;
    while(cur<m) {
        ans.push_back(p[cur]);

        int prev=cur;
        cur++;
        while(cur+1<m && d[p[prev]][p[cur+1]]==d[p[prev]][p[cur]]+d[p[cur]][p[cur+1]])
            cur++;
    }

    cout<<ans.size()<<endl;
    for(int i: ans)
        cout<<i+1<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
