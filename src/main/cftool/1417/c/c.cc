/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* all subseqs of size k
 * -> min number
 *  -> k-amazing
 *  no common element: -1
 *
 * We need to find foreach element the max dist between two same elements,
 * ie the biggest subarray without that number.
 * Then for k bgeq that dist that element is in all subarrays.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    const int minofall=*min_element(all(a));

    /* madi[i]= max array size without i */
    map<int,int> madi;
    /* left[i]= index of i left of current position */
    map<int,int> left;
    for(int i=0; i<n; i++)  {
        auto it=left.find(a[i]);
        int dist=i;
        if(it!=left.end()) {
            dist=i - it->second - 1;
        }

        auto it2=madi.find(a[i]);
        if(it2!=madi.end()) {
            it2->second=max(it2->second, dist);
        } else
            madi[a[i]]=dist;

        left[a[i]]=i;
    }

    for(auto ent : left)
        madi[ent.first]=max(madi[ent.first], n-1-ent.second);

    /* ans[i]= min element for dist i */
    map<int,int> ans;
    for(auto ent : madi) {
        auto it=ans.find(ent.second);
        if(it==ans.end())
            ans[ent.second]=ent.first;
    }

    int mi=1e9;
    for(int i=0; i<n; i++) {
        auto it=ans.upper_bound(i);
        if(it==ans.begin())
            cout<<-1<<" ";
        else {
            it--;
            mi=min(mi, it->second);
            cout<<mi<<" ";
        }
    }
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
