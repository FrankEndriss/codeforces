/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* misfortune = number of pairs with sum==T
 *
 * So, if two numbers add up to T one should be 
 * white, one should be black.
 */
void solve() {
    cini(n);
    cini(T);
    cinai(a,n);

    map<int,vi> m;
    for(int i=0; i<n; i++)
        m[a[i]].push_back(i);

    vi cnt(2);
    vi ans(n, -1);

    for(auto it=m.begin(); it!=m.end(); it++) {
        if(it->first*2==T) {
            int offs=0;
            if(cnt[0]>cnt[1])
                offs=1;
            for(size_t i=0; i<it->second.size(); i++) {
                cnt[(i+offs)%2]++;
                ans[it->second[i]]=(i+offs)%2;
            }
            it->second.clear();
            continue;
        }

        auto it2=m.find(T - it->first);
        if(it2!=m.end()) {
            /* put bigger set into smaller group */
            if(it->second.size() > it2->second.size()) {
                int offs=0;
                if(cnt[1]<cnt[0])
                    offs=1;
                for(auto i : it->second) {
                    cnt[offs]++;
                    ans[i]=offs;
                }
                for(auto i : it2->second) {
                    cnt[!offs]++;
                    ans[i]=!offs;
                }
            } else {
                int offs=1;
                if(cnt[0]<cnt[1])
                    offs=0;

                for(auto i : it2->second) {
                    cnt[offs]++;
                    ans[i]=offs;
                }
                for(auto i : it->second) {
                    cnt[!offs]++;
                    ans[i]=!offs;
                }
            }
        }
    }
    for(int i=0; i<n; i++) {
        if(ans[i]<0) {
            if(cnt[0]<n/2) {
                ans[i]=0;
                cnt[0]++;
            } else
                ans[i]=1;
        }
    }

    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
