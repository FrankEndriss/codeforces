/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Which x? a[i]=a[i]-x*i
 * So it must hold x*i<=a[i]
 *
 * Note that sum of array stays same. We want to make all equals a0.
 *
 * Let d[] be the diff d[i]=a[i]-a0
 *
 * Greedy
 *
 * We would like to always use a[0] as i, and some other ...
 * We would like to utilize smaller i with bigger j.
 *
 * 3n operations...
 * Lets first fix from bigger j the elements >a0, ie the ones
 * where we would use negative x.
 * Use them together with one or two smaller i, ie a[0]
 * -> Utilize a[0]
 *
 *  ... x must be positiv!
 * How to fix a[n-1], if a[n-1]>a0?
 * d[n-1]
 * Choose some i so that d[n-1] is divi by i, then
 * We must choos i=n-1, else cannot make d[n-1]==0.
 *
 * Greedy, do this for all numbers which are a[i]>a0.
 *
 * Time runs out... :/
 */
void solve() {
    cini(n);
    cinai(a,n);

    int sum=accumulate(all(a), 0LL);
    if(sum%n!=0) {
        cout<<-1<<endl;
        return;
    }

    const int a0=sum/n;
    //cerr<<"a0="<<a0<<endl;

    vvi ans;
    /* the sum of all d[i]==0 */
    set<pii> d;
    for(int i=0; i<n; i++)
        d.insert({ a[i]-a0, i});

    for(int j=n-1; j>=1; j--) {
        if(a[j]>a0) {
            int x=(a[j]-a0)/j;
            auto it=d.begin();

            ans.push_back({ 1, it->second+1, -x});
            a[j]-=x;
            a[0]+=x;
        }
    }

    /*
    cerr<<"after: ";
    for(int i=0; i<n; i++) {
        cerr<<a[i]<<" ";
    }
    cerr<<endl;
    */

    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++) 
        cout<<ans[i][0]<<" "<<ans[i][1]<<" "<<ans[i][2]<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
