/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to find the biggest odd product,
 * so ans=product of all odd numbers,
 * bcs it must not have a factor 2 in it.
 * The length of the subseq is the number of odd numbers.
 * ...
 * **** 
 * Ok, no, not at all. It is sum%n==1, not sum%2==1.
 *
 * Can we bruteforce this?
 * How can we check if x%n==1 without calculating x?
 * This is propably some chinese remainder problem,
 * we can somehow check foreach number if it can be used in
 * one of the crm formulars...somehow.
 *
 * Consider small n, n=3
 *
 * Each multiplication of a number mod some other number
 * has an outcome from 0 to n-1
 *
 * So let there be an graph with n-1 vertex (the remainders), and
 * we want to find the longest path ending at 1 and having each
 * edgevalue at most once.
 * The edges are the multiplications by the n-1 numbers.
 * ...
 *
 *
 */
void solve() {
    cini(n);

    if(n%2==1) { /* all smaller, but not the divisors */
        vi ans;
        ans.push_back(1);
        for(int i=2; i<=n-2; i++)
            if(n%i!=0)
                ans.push_back(i);
        cout<<ans.size()<<endl;
        for(int i : ans)
            cout<<i<<" ";
        cout<<endl;
        return;
    } else { /* all odd but not the divisors */
        vi ans;
        ans.push_back(1);
        for(int i=3; i<n; i+=2)
            if(n%i!=0)
                ans.push_back(i);
        cout<<ans.size()<<endl;
        for(int i : ans)
            cout<<i<<" ";
        cout<<endl;
        return;
    }

    // do some brute force
    
}

signed main() {
    solve();
}
