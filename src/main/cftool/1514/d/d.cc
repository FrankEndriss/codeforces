/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


using S=signed; /* type of values */

S st_op(S a, S b) {
    return max(a,b);
}

const S INF=1e9;
S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

const int BLOCK_SIZE=256;

bool mo_cmp(pair<int, int> p, pair<int, int> q) {
    if (p.first / BLOCK_SIZE != q.first / BLOCK_SIZE)
        return p < q;
    return (p.first / BLOCK_SIZE & 1) ? (p.second < q.second) : (p.second > q.second);
}

const int N=3e5+7;
stree seg(N);     /* freqs of numbers */
vi a;                   /* data */

inline void moadd(int idx) {
    seg.set(a[idx], seg.get(a[idx])+1);
}

inline void moremove(int idx) {
    seg.set(a[idx], seg.get(a[idx])-1);
}

/* freq of most freq element */
inline int moget_answer() {
    return seg.prod(0,N);
}

/* (l,r) both inclusive, 0 based */
struct Query {
    int l, r, idx;

    bool operator<(Query other) const {
        return mo_cmp(make_pair(l, r), make_pair(other.l, other.r));
    }
};

vector<pii> mo(vector<Query>& queries) {
    vector<pii> answers(queries.size());
    //sort(queries.begin(), queries.end(), mo_cmp);
    sort(queries.begin(), queries.end());
    int cur_l = 0;
    int cur_r = -1;

    /* invariant: data structure will always reflect the range [cur_l, cur_r] */
    for (Query q : queries) {
        while (cur_l > q.l) {
            cur_l--;
            moadd(cur_l);
        }
        while (cur_r < q.r) {
            cur_r++;
            moadd(cur_r);
        }
        while (cur_l < q.l) {
            moremove(cur_l);
            cur_l++;
        }
        while (cur_r > q.r) {
            moremove(cur_r);
            cur_r--;
        }
        answers[q.idx].first = moget_answer();
        answers[q.idx].second = q.r-q.l+1;
    }
    return answers;
}

/* Queries are independend, can be rearranged.
 * foreach query
 *    Only the subarray l,r matters
 *    "no value occurs strictly more than Tx/2T" means for 
 *    x=1 -> max 1
 *    x=2 -> max 1
 *    x=3 -> max 2 etc, (x+1)/2 times
 *
 * Ok, we could check the freqs of some subarray with a segment tree,
 * but how to find the number of partitions we need?
 * Using the full array there is max one value making problems.
 * So we need to care for that one in the first place.
 *
 * Let f be the freq of that one number having to much occs.
 * We need to put f-Tx/2T of them into single element seqs, and
 * all other into one remaining. So that is ans.
 *
 * Implement using mo, maintain map of freq of elements.
 * ...that seems to notwork with mo, TLE :/
 * ***
 * Lets rethink.
 * We need per query the freq of the most freq element in range l,r.
 * How can we make this a dp?
 *
 */
void solve() {
    cini(n);
    cini(q);

    a.resize(n);
    for(int i=0; i<n; i++)
        cin>>a[i];

    vector<Query> qu(q);
    for(int i=0; i<q; i++) {
        cini(l); l--;
        cini(r); r--;
        qu[i]={l,r,i};
    }

    vector<pii> ans=mo(qu);

    for(int i=0; i<q; i++) {
        int ma=(ans[i].second+1)/2;
        int lans=1+max(0,ans[i].first-ma);
        cout<<lans<<endl;
    }


}

signed main() {
    solve();
}
