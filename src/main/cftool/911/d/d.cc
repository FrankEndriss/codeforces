/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Every position in a[] is part of n-1 pairs.
 * Some are inversions, some are not.
 *
 * It seems that each reversion of a even length segment
 * switches the number of inversions of all elements in
 * this interval an odd number of times.
 *
 * So we need to count how much inversions there
 * where at begin, plus number of reverses.
 *
 * ...this is somehow simply wrong :/
 * **************
 * Correct is that gaus(n) pairs change their order,
 * where n is the size of the reversed interval.
 * So, it that number is odd then parity changes.
 */
void solve() {
    cini(n);
    int cnt=0;
    vi a;
    for(int i=0; i<n; i++) {
        cini(aux);
        for(size_t j=0; j<a.size(); j++)
            if(a[j]>aux)
                cnt++;
        a.push_back(aux);
    }

    int par=cnt%2;
    cini(m);
    for(int i=0; i<m; i++) {
        cini(l);
        cini(r);
        cnt=r-l+1;
        cnt=cnt*(cnt-1)/2;
        if(cnt%2)
            par^=1;

        if(par)
            cout<<"odd"<<endl;
        else
            cout<<"even"<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
