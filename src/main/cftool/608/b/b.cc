/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to count the freq of a[i]-1 in subarray b[i...i+len(b)-len(a)]
 */
void solve() {
    cins(a);
    cins(b);

    const int sa=a.size();
    const int sb=b.size();
    const int p=sb-sa+1;    /* number of possible positions of a */

    vi cnt(2);
    for(int i=0; i<p; i++) 
        cnt[b[i]-'0']++;

    int ans=0;
    for(size_t i=0; i<sa; i++) {    
        //cerr<<"i="<<i<<endl;
        ans+=cnt[1-(a[i]-'0')];

        cnt[b[i]-'0']--;
        if(i+p<sb)
            cnt[b[i+p]-'0']++;
    }

    cout<<ans<<endl;


}

signed main() {
        solve();
}
