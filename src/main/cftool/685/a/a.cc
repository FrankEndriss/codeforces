/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* works like next_permutation(), but on a permutation like seq in p, with max value i.
 * usually i != p.size(); 
 * Usefull to create all subsets of a given size of a set of values.
 * vi data(n);  // some data...
 * vi idx(m);   // subset of size m; m<=n;
 * iota(idx.begin(), idx.end(), 0);
 * do {
 *      ...something using data readonly...
 * } while(next(idx, n-1));
 *
 * example for m=3, n=4:
 * 0 1 2 
 * 0 1 3 
 * 0 2 3 
 * 1 2 3
 **/
bool next(vi &p, int i) {
    const int k=(int)p.size();

    if(p[k-1]<i) {
        p[k-1]++;
        return true;
    }
    if(k==1)
        return false;

    int incidx=k-2;
    while(incidx>=0 && p[incidx]==p[incidx+1]-1)
        incidx--;

    if(incidx<0)
        return false;

    p[incidx]++;
    for(int j=incidx+1; j<k; j++)
        p[j]=p[j-1]+1;

    return true;
}

/* number of digits */
int dig(int i) {
    int ans=0;
    while(i) {
        ans++;
        i/=7;
    }
    return max(ans,1LL);
}
/**
 * There are only 7 digits, 0..6
 * So if size(n)+size(m)>6, then there is no sol.
 * Else we could iterate all permutations of the digits, 
 * and check each one for overflow of hour or minute.
 *
 * Lets try dp
 * dp[mask][i]=number of possible numbers up to pos i while having used mask digits
 * ...no, to complecated.
 * Lets iterate all numbers and check each one.
 */
void solve() {
    cini(h); h--;
    cini(m); m--;

    int sh=dig(h);
    int sm=dig(m);

    if(sh+sm>7) {
        cout<<0<<endl;
        return;
    }
    const int M=sh+sm;

    //cerr<<"sh="<<sh<<" sm="<<sm<<endl;

    vi idx(M);
    iota(idx.begin(), idx.end(), 0LL);

    int ans=0;
    do {    /* iterate all subsets of the 7 digits of size sh+sm */
        //cerr<<"subset: ";
        //for(int i=0; i<M; i++) 
        //    cerr<<idx[i]<<" ";
        //cerr<<endl;

        vi ii(M);  /* iterate all permuations of of this subset */
        iota(all(ii), 0LL);
        do {
            /* build the current hour value by using the first sh digits in perm order */
            int hh=0;
            for(int i=0; i<sh; i++) {
                hh*=7;
                hh+=idx[ii[i]];
            }
            if(hh<=h) { /* if hour field in range */
                int mm=0;   /* build current value of minute field */
                for(int i=sh; i<sh+sm; i++) {
                    mm*=7;
                    mm+=idx[ii[i]];
                }
                if(mm<=m)
                    ans++;

                //cerr<<"match: ";
                //for(int i=0; i<M; i++) 
                //    cerr<<idx[ii[i]]<<" ";

            }
        }while(next_permutation(all(ii)));
    }while(next(idx, 6));

    cout<<ans<<endl;
}

signed main() {
        solve();
}
