/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int query(int u, int v) {
    cout<<"? "<<u<<" "<<v<<endl;
    cini(resp);
    return resp;
}

/* shit.
 * query two leaves.
 * if response if one of them, that is ans.
 * else remove those two leaves.
 * repeat.
 */
void solve() {
    cini(n);
    vector<set<int>> adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        adj[u].insert(v);
        adj[v].insert(u);
    }

    vb vis(n);
    while(true) {
        int n1=-1;
        int n2=-1;
        for(int i=0; i<n; i++) {
            if(vis[i])
                continue;

            if(adj[i].size()<=1) {
                if(n1<0)
                    n1=i;
                else {
                    n2=i;
                    break;
                }
            }
        }

        if(n2>=0) {
            int res=query(n1+1, n2+1);
            if(res-1==n1 || res-1==n2) {
                cout<<"! "<<res<<endl;
                return;
            }
        } else {
            cout<<"! "<<n1+1<<endl;
            return;
        }

        vis[n1]=true;
        vis[n2]=true;

        for(int i : adj[n1])
            adj[i].erase(n1);
        adj[n1].clear();
        for(int i : adj[n2])
            adj[i].erase(n2);
        adj[n2].clear();

    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

