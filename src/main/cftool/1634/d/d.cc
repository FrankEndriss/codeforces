/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int q(int i, int j, int k) {
	cout<<"? "<<i+1<<" "<<j+1<<" "<<k+1<<endl;
	int ans;
	cin>>ans;
	return ans;
}

/**
 * Note that the zero is the smallest element in the array.
 * So, what can we say about i,j,k after haveing the query result?
 * Well, the difference between max and min element of the three.
 * Note that if one of i,j,k is zero, the we will never get a bigger
 * result of max(a[i],a[j],a[k])
 * Consider
 * q[]={
 *   query(1,2,3)
 *   query(1,2,4)
 *   query(1,3,4)
 *   query(2,3,4)
 * };
 * Can we tell which one is smallest,biggest of those 4?
 * If all four numbers are distinct, then there are two 
 * results smaller than the other two.
 * The two smaller ones are those where min and max of the 
 * four are missing.
 * Else the zero is not in that quadrupel, because there is only one zero.
 * Consider 0,1,1,1
 * q[]={1,1,1,0}
 * Consider 0,1,2,2
 * q[]={2,2,2,1}
 * Consider 0,1,2,3
 * q[]={2,3,3,2}
 * ...bad casework :/
 *
 * Then narrow down all of them until only two remain.
 */
void solve() {
	cini(n);

	vi id(n);
	iota(all(id), 0LL);

	while(id.size()>2) {
		vi id0;
		for(size_t i=0; i<id.size(); i+=4) {
			vector<pii> qq={
				{ q(i+1, i+2, i+3), 0},
				{ q(i+0, i+2, i+3), 1},
				{ q(i+0, i+1, i+3), 2},
				{ q(i+0, i+1, i+2), 3}
			};
			sort(all(qq));
			id0.push_back(qq[0].second);
			id0.push_back(qq[1].second);
		}
		if(id.size()%4!=0) {
			vector<pii> qq={
				{ q(id.size()-1, id.size()-2, id.size()-3), id.size()-4},
				{ q(id.size()-0, id.size()-2, id.size()-3), id.size()-3},
				{ q(id.size()-0, id.size()-1, id.size()-3), id.size()-2},
				{ q(id.size()-0, id.size()-1, id.size()-2), id.size()-1}
			};
			sort(all(qq));
			id0.push_back(qq[0].second);
			id0.push_back(qq[1].second);
		}
		set<int> ss;
		for(int i : id0)
			ss.insert(i);
		id.clear();
		for(int i : ss)
			id.push_back(i);
	}

	assert(id.size()<=2);
	while(id.size()<2)
		id.push_back((id.back()+1)%n);

	cout<<"! "<<id[0]+1<<" "<<id[1]+1<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
