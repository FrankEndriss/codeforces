/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider one shelf.
 * The sum of two adjacent numbers must be div by 2,
 * so must have same parity. So one shelf can hold only numbers of one parity.
 *
 * Consider 3 numbers with same parity
 *
 * 1 3 5
 * 2 4 6
 * also there are trivial arrangements for k==1
 *
 * Can we somehow brute force all?
 * Or prove that there are none?
 *
 * An example for k==4?
 * 1 3 5 7
 * 2 4 6 8
 *
 * An example for n>2?
 * 1 5
 * 2 6
 * 3 7
 * 4 8
 *
 * 1 5 9
 * 2 6 10
 * 3 7 11
 * 4 8 12
 * How is the rule, and why?
 *
 * Consider above schema to be the only one working, we can construct the first
 * shelf as
 * 1, 1+n, 1+2*n,...,1+k*n
 * and each consecutive shelf by incrementing the previous one.
 * Perhaps we should check if the first shelf really works.
 * If so, the others will, too.
 * How to find if there are other schemas of construction that work?
 * Or if there are none?
 */
void solve() {
	cini(n);
	cini(k);

	vi a(k);
	a[0]=1;
	for(int i=1; i<k; i++) 
		a[i]=a[i-1]+n;

	for(int len=2; len<k; len++) {
		int sum=0;
		for(int j=0; j<len; j++)
			sum+=a[j];

		if(sum%len!=0) {
			cout<<"NO"<<endl;
			return;
		}

		for(int j=len; j+len<k; j++)  {
			sum-=a[j-len];
			sum+=a[j];
			if(sum%len!=0) {
				cout<<"NO"<<endl;
				return;
			}
		}
	}
	cout<<"YES"<<endl;
	for(int i=0; i<n; i++) {
		for(int j=0; j<k; j++) 
			cout<<a[j]+i<<" ";
		cout<<endl;
	}
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
