/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* for any node there are two options
 *-destroy by min(A, B*na*l)
 *-destroy the halves separate by cost(left)+cost(right)
 * Simply implement this recursive, what can go wrong?
 * ->2^30 is to much.
 *  We need to optimize this somehow.
 *  There are max 1e5 avengers, ie most of 2^30 is zeros.
 */
void solve() {
    cini(n);
    cini(k);
    cini(A);
    cini(B);

    cinai(a,k);
    sort(all(a));

    function<int(int,int)> cntA=[&](int l, int r) {
        auto it1=lower_bound(all(a), l);
        auto it2=upper_bound(all(a), r);
        return distance(it1,it2);
    };

    function<int(int,int)> destroy=[&](int l, int r) {
        int cnt=cntA(l,r);

        int ans;
        if(cnt==0)
            ans=A;
        else {
            int d=r-l+1;
            if(d==1)
                ans=B*cnt;
            else {
                int mid=(r+l)/2;
                ans=min(destroy(l, mid)+destroy(mid+1, r), B*cnt*d);
            }
        }
//cerr<<"dest, l="<<l<<" r="<<r<<" ans="<<ans<<endl;
        return ans;
    };

    int ans=destroy(1, 1LL<<n);
    cout<<ans<<endl;
        
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
