/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, vector<ll> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization.push_back(d);
            n /= d;
        }
    }
    if (n > 1)
        factorization.push_back(n);
}

/* a/b == a%b
 *
 * number of b to a given a:
 * a,2 if a==3
 * a,x if a==1*x+1
 * a,x if a==2*x+2
 * a,x if a==3*x+3
 * ...
 *
 * a=42
 * a,41
 * a,20
 * a,13
 *
 * ...So it is kind of number of divisors or the like :/
 * ...
 * Number of divisors d of a with d<=x and d<=sqrt(a)
 * And if b>a then all multiples of a that are <=b.
 */
void solve() {
    cini(x);
    cini(y);

    vi d;
    trial_division4(x, d);
    int ans=0;
    for(int dd : d) {
        if(dd*dd<=x && dd<=y) {
            ans++;
        }
    }
    ans+=y/x;
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
