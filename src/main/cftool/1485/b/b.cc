/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* each position can change in a given
 * number of ways, limited by the next value and prev value.
 * ans=sum of those values in range,
 * plus some diff for the first and last number in the range.
 *
 * Shitty index fiddling again... I do not like this kind of problems at all. 
 * On a friday afternoon after hard week of work that is no fun :/
 */
void solve() {
    cini(n);
    cini(q);
    cini(k);

    /* add a first and a last element, 1 and k */
    vi a(n+2);
    for(int i=1; i<=n; i++)
        cin>>a[i];
    a[0]=1;
    a[n+1]=k;

    vi dInc(n+2);    /* diff a[i+1]-a[i] */
    vi dDec(n+2);    /* diff a[i]-a[i-1] */
    for(int i=1; i<=n; i++) {
        dDec[i]=max(0LL, a[i]-a[i-1]-1);
        dInc[i]=max(0LL, a[i+1]-a[i]-1);
    }

    /* prefix sums */
    vi preInc(n+2);
    vi preDec(n+2);
    for(size_t i=1; i<preInc.size(); i++) {
        preInc[i]=preInc[i-1]+dInc[i];
        preDec[i]=preDec[i-1]+dDec[i];
    }

    for(int i=0; i<q; i++) {
        cini(l);
        cini(r);
        if(l==r) {
            cout<<k-1<<endl;
            continue;
        }

        int ans=preInc[r-1]-preInc[l-1];
        ans+=k-a[r];

        ans+= preDec[r]-preDec[l];
        ans+=a[l]-1;

        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}
