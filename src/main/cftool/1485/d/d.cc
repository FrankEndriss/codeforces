/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* k^4
 * 1
 * 16
 * 81
 * 256
 * 625
 * 36*36
 * 49*49
 * 4096
 * 82*81
 * 10000
 * ...
 * aprox 32 lines until 1e6.
 *
 *
 * Brute force?
 * Just use any number from a[][] and try all multiples.
 * Then the adj cells must be some multiple of that, and some diff of the quads.
 * There cannot be that much solutions.
 *
 * ...
 * How to speed this up?
 * Note that a[i][j]<=16
 * ...which I did not notice for more than an hour :/
 * How to utilize that?
 */
const int N=1e6+7;
void solve() {
    cini(n);
    cini(m);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++)
            cin>>a[i][j];


    vvi ans(n, vi(m));

    vb quad(N);
    int i=1;
    while(i*i*i*i<N) {
        quad[i*i*i*i]=true;
        i++;
    }

    function<bool(int,int)> go=[&](int i, int j) {
        if(i>=n) {
            for(int i=0; i<n; i++)  {
                for(int j=0; j<m; j++)
                    cout<<ans[i][j]<<" ";
                cout<<endl;
            }
            return true;
        }

        for(int k=a[i][j]; k<=(int)1e6; k+=a[i][j]) {
            if(i>0) {
                const int d=abs(ans[i-1][j]-k);
                if(d>=N || !quad[d])
                    continue;
            }
            if(j>0) {
                const int d=abs(ans[i][j-1]-k);
                if(d>=N || !quad[d])
                    continue;
            }
            ans[i][j]=k;
            int jj=j+1;
            int ii=i;
            if(jj==m) {
                jj=0;
                ii++;
            }
            if(go(ii,jj))
                return true;
        }
        return false;
    };

    go(0,0);
}

signed main() {
    solve();
}
