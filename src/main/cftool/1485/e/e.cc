
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * ...seems like kind of missunderstood the problem.
 */
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=2; i<=n; i++) {
        cini(p);
        p--;
        adj[i-1].push_back(p);
        adj[p].push_back(i-1);
    }

    vi a(n);
    for(int i=1; i<n; i++)
        cin>>a[i];

    cerr<<"did read input"<<endl;

    vvi d(n+1);   /* d[i]=list of numbers written level i */

    function<void(int,int,int)> dfs=[&](int v, int p, int l) {
        assert(l<=n);
        d[l].push_back(a[v]);
        for(int chl : adj[v]) 
            if(chl !=p)
                dfs(chl, v, l+1);
    };
    dfs(0, -1, 0);

    int ans=0;
    for(int i=1; i<=n; i++) {
        if(d[i].size()>0) {
            ans+=*max_element(all(d[i]));
            ans-=*min_element(all(d[i]));
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
