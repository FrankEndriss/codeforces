
import java.util.*

fun solve() {
    val n = readLine()!!.toInt()
    val minA=(n/2)+1;
    val maxA=n-1;
    val ans=kotlin.math.max(0, maxA-minA+1);
    println("$ans");
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
    
}
