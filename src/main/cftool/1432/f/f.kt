
import java.util.*

/*  its greedy
* we do only two kinds of jumps, shortjump is one position,
* and longjump is d positions.
* Whenever we are at the end of a platform, we do longjump
* to the next platform, then shortjump to the end of that 
* platform.
*
* Finally do stupid platform reconstruction.
* do all array indexing in terms of ans, ie start at -1 end at n
*/
fun solve() {
    val arg = readLine()!!.split(" ").map { it.toInt() }
    val n=arg[0];
    val m=arg[1];
    val d=arg[2];

    val c=readLine()!!.split(" ").map { it.toInt() }
    
    val ansC=(1..m).map { 1000000 }.toMutableList()   /* left/start position of platforms */

    var pos=-1;
    var i=0;    /* index into c */
    while(pos<n && i<m) {
        pos=kotlin.math.min(pos+d, n)
        ansC[i]=pos
        pos+=c[i]-1;
        i++;
    }
    pos=kotlin.math.min(pos+d, n)

    if(pos<n) {
        println("NO")
        return;
    }

    i=m-1;
    var sum=0;
    while(i>=0) {
        sum+=c[i]
        ansC[i]=kotlin.math.min(ansC[i], n-sum);
        i--;
    }

    val ans=(1..n).map { -1 }.toMutableList()
    i=0;
    while(i<m) {
        var j=ansC[i];
        while(j<ansC[i]+c[i])
            ans[j++]=i
        i++
    }

    println("YES")
    ans.forEach { print("${it+1} ") }
}

fun main(args: Array<String>) {
        solve()
}
