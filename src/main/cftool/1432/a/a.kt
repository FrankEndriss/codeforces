
import java.util.*

fun solve() {
    val a = readLine()!!.split(" ").map { it.toInt() }
    println("${a[0]+a[1]}");
}
fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
    
}
