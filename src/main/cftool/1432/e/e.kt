
import java.util.*

/* kotlin anoying string operations :/ */
fun solve() {
    val arg = readLine()!!.split(" ").map { it.toLong() }
    val n=arg[0];
    var k=arg[1];
    val s=readLine()!!
    val ans=StringBuilder(s);

    var l=0;
    while(l<n && s[l]=='0')
        l++;

    if(l>=n) {
        println(s);
        return;
    }

    /* l points to leftmost '1' */

    val ss=s.length-1
    var i=l+1;
    while(k>0 && i<=ss) {
        if(ans[i]=='0') {
            val cnt=kotlin.math.min(k, (i-l).toLong());

            ans[(i-cnt).toInt()]='0';
            ans[i]='1';
            k-=cnt;
            l++;
        }
        i++;
    }

    println(ans)
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
}
