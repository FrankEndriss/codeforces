
import java.util.*

fun solve() {
    val n = readLine()!!.split(" ").map { it.toInt() }

    val s="abcdefghijklmnopqrstuvwxyz";

    var ans="";
    for(i in 0..n[0]-1) {
        ans+=s[i%n[2]];
    }

    println("$ans");
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
}
