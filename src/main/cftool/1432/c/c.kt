
import java.util.*

fun solve() {
    val n = readLine()!!.toInt()
    val a = readLine()!!.split(" ").map { it.toInt() }

    val s=a.sum();
    if(s%n==0) {
        val ans=s/n
        println("$ans");
    } else {
        val ans=s/n+1;
        println("$ans");
    }
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
    
}
