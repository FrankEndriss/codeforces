/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * On cheap days she wants to buy as much
 * as possible, on expensive days as less
 * as possible.
 * So Greedy.
 * First buy all min amount. For the rest until
 * k buy cheapest possible.
 *
 *
 * KOTLIN!!!
 */
void solve() {
    cini(n);
    cini(k);
    vi a(n);
    vi b(n);
    vi c(n);

    int cnt=0;
    int sum=0;  /* sum cost */
    for(int i=0; i<n; i++) {
        cin>>a[i]>>b[i]>>c[i];
        cnt+=a[i];
        sum+=a[i]*c[i];
    }

    if(cnt>k) {
        cout<<-1<<endl;
        return;
    }

    vi id(n);
    iota(all(id), 0);
    sort(all(id), [&](int i1, int i2) {     /* cheapest first */
        return c[i1]<c[i2];
    });

    for(int i=0; cnt<k && i<n; i++) {
        int lcnt=min(k-cnt, b[id[i]]-a[id[i]]);
        cnt+=lcnt;
        sum+=c[id[i]]*lcnt;
    }

    if(cnt==k)
        cout<<sum<<endl;
    else
        cout<<-1<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
