
/* We need to find the number of numbers n from 1 to t
 * where n%w == n%b
 *
 * We cannot iterate them because big N.
 *
 * It is obvious that for all n%lcd(w,b)==0 it is a tie.
 * Also for all numbers y*lcd(w,b)+k where k<min(w,b).
 *
 * So, the contributing numbers are in segments, each such segment
 * starts at y*lcd(w,b) and has length min(w,b)
 * Note that the first element in first segment is 0, but that is no valid
 * race len, so we need to subtract 1 in the end.
 *
 * Since there are overflow issues we cannot simply calculate the lcd(w,b) :/
 * Use lll numbers? yes
 *
 * How to avoid off by one errors?
 * -> there is no clean way, sic math.
 * However, consider the numbers of possible race length up to t, it is t+1.
 * So work with that.
 *
 * ...Still wrong, and editorial formulars are not implementable :/
 * So, copied sol from https://codeforces.com/contest/592/submission/20825885
 * to get rid of it in lists. And that fails with overflow, too ;)
 * Hereby it is declared to be a shitty prob I do not want to spend more time on.
 */

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>
using namespace std;

using lll=__int128;

lll hcf1(lll a, lll b) {
    if(a==0) {
        return b;
    } else {
        return hcf1(b%a,a);
    }
    return (lll)0;
}

int main() {
    long long t,w,b;
    cin>>t>>w>>b;
    lll to=t;
    lll hcf=hcf1(w,b);
    lll lcm=(w/hcf)*b;
    bool check=false;
    if(lcm/b==w/hcf) {
        check=true;
    }
    lll ans1=0;
    if(check==true) {
        lll k=1;
        k=t/lcm;
        ans1=(min(w,b))*k;
        t=(long long)(t-lcm*k);
    }
    lll ans2=min(min(w,b)-1,t);
    lll gcd=hcf1(ans1+ans2,to);
    cout<<(long long)((ans1+ans2)/gcd)<<"/"<<(long long)(to/gcd)<<endl;
    return 0;
}
