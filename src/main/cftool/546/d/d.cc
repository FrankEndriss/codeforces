/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need to find the number
 * of primefactors of the numbers
 * in range (b+1,a)
 *
 * So first create a[N] of
 * a[i]=number of divisors of i
 * How?
 *
 * Then make prefix sum of a[]
 * Then ans=p[a]-p[b]
 *
 * Note that IO runs TLE if we use cout<<endl;
 */
const int N=5e6+3;
void solve() {

    vi a(N+1);

    for(int i=2; i<N; i++) {
        if(a[i]!=0)
            continue;
        for(int k=i; k<N; k*=i)
            for(int j=k; j<N; j+=k)
                a[j]++;

        a[i]=1;
    }

    vi p(N+1);
    for(int i=1; i<N; i++)
        p[i]=p[i-1]+a[i];

    cini(t);
    while(t--) {
        cini(aa);
        cini(bb);
        int ans=p[aa]-p[bb];
        cout<<ans<<"\n";
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
