/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* An item is a seq of numbers, which extends infinite with
 * appended 0s.
 * We can create a new item by incrementing any of the numbers!=0,
 * or the first 0, if such a list does not allready exists.
 *
 * We need to create a list with n lines, where the last item added
 * has an a[i] as last element.
 * Input data is in a way that there exists a solution.
 *
 * The first item in the list is 1.
 * if the number increases, the last number of the previous item
 * was increased.
 * if it decreases, one of the more left numbers was increased.
 *
 * What are the rules to create sublists? How to implement?
 *
 * if a[i]==1 we need to add a 1 to some existing item
 * and insert that element just after the current item.
 *
 * Else we need to increment the end of some item,
 * and inset that element just after all longer items.
 * ...
 * No. We must allways create an item at the end of the list.
 * But we propably have to adjust the previously existing items.
 * How to do that?
 */
void solve() {
    cini(n);
    cinai(a,n);

    vector<vector<pii>> li(1);
    li[0].emplace_back(1,0);
    assert(a[0]==1);

    for(int i=1; i<n; i++) {
        for(int j=0; j<li.size(); j++) {
            if(a[i]==1 && (li[j].back().second&1)==0) {
                li[j].back().second|=1;
                int jj=j+1;   /* find index to insert */
                auto it=li.insert(li.begin()+jj, li[j]);
                it->emplace_back(1,0);
                break;
            } else if(li[j].back().first==a[i]-1 && (li[j].back().second&2)==0) {
                li[j].back().second|=2;

                int jj=j+1;   /* find index to insert */
                while(jj<li.size() && li[jj].size()>li[j].size())
                    jj++;

                auto it=li.insert(li.begin()+jj, li[j]);
                it->back().first++;
                it->back().second=0;
                break;
            }
        }
    }
    assert(li.size()==(size_t)n);

    /*
    sort(all(li), [&](vector<pii> &v1, vector<pii> &v2) {
            for(int j=0; j<min(v1.size(), v2.size()); j++)
                if(v1[j].first!=v2[j].first)
                    return v1[j]<v2[j];

            return v1.size()<v2.size();
    });
    */

    for(size_t i=0; i<li.size(); i++) {
        for(size_t j=0; j<li[i].size(); j++) {
            if(j>0)
                cout<<'.';
            cout<<li[i][j].first;
        }
        cout<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
