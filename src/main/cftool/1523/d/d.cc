/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * The choosen currencies build a mask, and that mask must
 * be a submask of at least the half of the friends.
 * We want to find the mask with the most bits turned on.
 *
 * We cannot check all masks, since 60 bits.
 * How comes p=15 into play?
 *
 * Consider two currencies, each must have at least n/2 bits set.
 * Can we create a bitset per currentcy, then combine pairs?
 */
void solve() {
    cini(n);
    cini(m);
    cini(p);

    vi cnt(m);
    for(int i=0; i<n; i++) {
        cins(s);
        for(int j=0; j<m; j++) 
            if(s[j]=='1')
                cnt[j]++;
    }
    string ans(m, '0');
    for(int i=0; i<m; i++) 
        if(cnt[i]>=(n+1)/2)
            ans[i]='1';

    cout<<ans<<endl;
}

signed main() {
    solve();
}
