/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to negate all numbers, so actually subtract its value twice.
 * Not more than 5*n operations.
 * All positive -> all negative
 * Consider two numbers, { 1, 10 }:
 * ---
 *  a[0]+=a[1] { 11,10 }
 *  a[0]+=a[1] { 21,10 };
 *  a[1]-=a[0] { 21, -11 };
 *  a[0]+=a[1] { 10, -11 };
 *  a[0]+=a[1] { -1, -11 };
 *  a[1]-=a[0] { -1, -10 }
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    cout<<(n/2)*6<<endl;
    for(int i=0; i<n; i+=2) {
        cout<<"1 "<<i+1<<" "<<i+2<<endl;
        cout<<"1 "<<i+1<<" "<<i+2<<endl;
        cout<<"2 "<<i+1<<" "<<i+2<<endl;
        cout<<"1 "<<i+1<<" "<<i+2<<endl;
        cout<<"1 "<<i+1<<" "<<i+2<<endl;
        cout<<"2 "<<i+1<<" "<<i+2<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
