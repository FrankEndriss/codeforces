/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Note that the candies of different station do not
 * interact with each other in any way.
 * So answer to one position is the max cost of
 * delivering the candies of any station.
 *
 * To deliver the candies of one station we need to load
 * num_candies times. Best is obviously to load to
 * candy with the shortest way last.
 * Then cost is cyc*(num_candies-1)+startToStation+shortestLastWay
 *
 * How to calc that values quick?
 * Find shortestLastWay in O(m) per station. Then brute force
 * min values for every start position, O(n^2)
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    vvi st(n);
    for(int i=0; i<m; i++) {
        cini(a);
        a--;
        cini(b);
        b--;
        st[a].push_back(b);
    }

    vi slw(n, INF);
    for(int i=0; i<n; i++) {
        for(int j : st[i]) {
            int dist=j-i;
            if(dist<0)
                dist+=n;
            slw[i]=min(slw[i], dist);
        }
        if(slw[i]==INF)
            slw[i]=0;
    }

    for(int start=0; start<n; start++) {
        int ans=0;
        for(int j=0; j<n; j++) {
            if(st[j].size()>0) {
                int ma=j-start;
                if(ma<0)
                    ma+=n;

                ma+=n*(st[j].size()-1);
                ma+=slw[j];
                ans=max(ans, ma);
            }
        }
        cout<<ans<<" ";
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
