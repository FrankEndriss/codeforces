/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * On each 0 we first must do the rooms left of the 0.
 */
void solve() {
    cini(n);
    cinai(a,n);
    a[n-1]=0;

    int l=0;
    while(l<n && a[l]==0)
        l++;
    int r=l+1;
    int ans=0;
    while(l<n-1) {
        while(a[r]>0)
            r++;
        while(l<=r && a[l]==0)
            l++;

        if(r==n-1) {
            break;
        } else {
            ans++;
            a[l]--;
            a[r]++;
        }
    }

            for(int i=0; i+1<n; i++) {
                ans+=a[i];
            }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
