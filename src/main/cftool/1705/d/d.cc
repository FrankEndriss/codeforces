/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * 011 -> 001
 * 001 -> 011
 * 100 -> 110
 * 110 -> 100
 *
 * So obviously we cannot switch first and last bulb.
 *
 * Consider the 1s.
 * We can double a 1, and move a 1
 * We cannot remove a 1.
 *
 * Some examples:
 * 000010
 * 000110
 * 000100
 * 001100
 * 001000
 * ...
 * 001100
 * 011100
 * ...
 *
 * 000011
 * 000001
 * 000011
 * 000111
 * 001111
 *
 * 010100 -> 011110
 * 010110
 * 010010
 * 011010
 * xxx
 * We cannot close a gap between two 1s, and we cannot remove it.
 * So, number of groups of 0s and 1s must fit in first place, 
 * then we can simply move/adjust groups from left to right.
 */
void solve() {
    cini(n);    /* n>=3 */
    cins(s);    /* binary string */
    cins(t);    /* final state */

    if(s[0]!=t[0] || s.back()!=t.back()) {
        cout<<-1<<endl;
        return;
    }

    int scnt=1;
    int tcnt=1;
    for(int i=1; i<n; i++) {
        if(s[i]!=s[i-1])
            scnt++;
        if(t[i]!=t[i-1])
            tcnt++;
    }
    if(scnt!=tcnt) {
        cout<<-1<<endl;
        return;
    }

    /* now iterate the groups in t, and adjust s according to it */
    int tl=-1;
    int tr=0;

    int sl=-1;
    int sr=0;

    //cerr<<"t="<<t<<endl;
    int ans=0;
    while(tr<n-1) {
        tl++;
        while(tl>0 && t[tl]==t[tl-1])   /* adjust to first of next group */
            tl++;
        tr=tl;
        while(tr+1<n && t[tr+1]==t[tr]) /* adjust to last of next group */
            tr++;

        //cerr<<"tl="<<tl<<" tr="<<tr<<endl;

        sl++;
        while(sl>0 && s[sl]==s[sl-1])   /* adjust to first of next group */
            sl++;
        sr=sl;
        while(sr+1<n && s[sr+1]==s[sr]) /* adjust to last of next group */
            sr++;

        ans+=abs(sl-tl);
        ans+=abs(sr-tr);
    }
    cout<<ans/2<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
