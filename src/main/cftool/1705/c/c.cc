/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Since the string is up to 40 times doubled, we cannot
 * create that string, MLE and TLE.
 *
 * So, foreach query, we need to go back the copy operations.
 * That is, we have the length of the string after each 
 * operation, and so can transform the index into the 
 * index of that letter before the copy.
 *
 * So queryies get answered in O(q*40);
 */
void solve() {
    cini(n);
    cini(c);
    cini(q);

    vi len(c+1);
    vi add(c);
    vi l(c);
    vi r(c);
    cins(s);
    len[0]=s.size();
    for(int i=0; i<c; i++) {
        cin>>l[i]>>r[i];
        l[i]--;
        r[i]--;
        add[i]=r[i]-l[i]+1;
        len[i+1]=len[i]+add[i];
    }

    for(int i=0; i<q; i++) {
        cini(k);
        k--;    // zero based

        for(int i=c-1; i>=0; i--) {
            if(k<len[i]+add[i]) {
                if(k>=len[i])
                    k=l[i]+k-len[i];
            }
        }

        cout<<s[k]<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
