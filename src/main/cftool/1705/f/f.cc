/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Some binary search variant.
 *
 * Consider query all F, then we get number of F in final ans.
 *
 * Then we can swap each single position, and query again.
 * If the number increases it was a good swap, if it decreases
 * it was bad. But that needs n queries, we need to optimize.
 *
 * Can we switch two? If both wrong or both right we know, 
 * else we do not know, but we know FT or TF.
 * Consider we swap next two; same again, we know TT or FF,
 * or know that it switches again
 *
 */
void solve() {
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
