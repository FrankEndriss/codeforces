/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that if freq[i]==y, we can ...
 *
 * Consider maintaining the numbers in freq map
 *
 * Then on each level i, there are available
 * avail(i)=f[i]+avail(i-1)/2
 *
 *
 * But, since there are the updates, we cannot initially resolve all :/
 *
 * But, note also, since the frequency halves on each level,
 * we do not need to consider to much levels.
 * ...
 * No, consider f[i]=2, f[i+1]=1, f[i+2]=1,....,f[i+k]=1
 * then 2 times i gets converted to one i+1, which then 
 * gets i+2... so that can go over 1e5 levels!
 *
 * Even if we would some find an upper bound to up to where
 * a level can influence another level it would not help
 * so much, since we would still have to calc the whole
 * chain afterwards again.
 * ...or not the whole chain?
 * So consider we have memoized the avail(i) values for all a[i].
 * Goinng from min(a[]), once we produce the same value
 * as before, we can stop.
 * ...but still, if there is a long chain, the chain can 
 * switch on each upate :/
 *
 * How?
 */
void solve() {
    cini(n);
    cini(q);

    cinai(a,n);
    map<int,int> f;
    for(int i=0; i<n; i++)
        f[-a[i]]++;

    function<int(int)> avail=[&](int l) {
        cerr<<"avail, l="<<l<<endl;
        if(l<=0)
            return 0LL;

        return f[-l]+avail(l-1)/2;
    };

    for(int i=0; i<q; i++) {
        cini(k);
        cini(l);
        k--;
        f[-a[k]]--;
        a[k]=l;
        f[-a[k]]++;

        auto it=f.begin();
        int ans=it->first * -1;
        int cnt=avail(ans);
        while(cnt>1) {
            ans++;
            cnt/=2;
        }
        cout<<ans<<endl;
    }
}

signed main() {
        solve();
}
