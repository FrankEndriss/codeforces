/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider the freqs, and number of different elements=k
 * Also, if we remove on element, we can allways remove
 * all of the elements x and result is still a plindrome.
 *
 * So different elements in result is k-1, or k
 *
 * If a is a palindrome we can remove all a[0], and it is still a plindrome.
 * Consider remove first element, if result is not a palindrome,
 * then remove element at first non matching position.
 * Maybe we have to try both elements at the first non matching position?
 */
void solve() {
    cini(n);
    cinai(a,n);

    function<pii(int)> go=[&](int rm) {

        vi aa;
        for(int elem : a)
            if(elem!=rm)
                aa.push_back(elem);

        int l=0;
        int r=aa.size()-1;
        while(l<r) {
            if(aa[l]==aa[r]) {
                l++;
                r--;
            } else {
                return  make_pair(aa[l],aa[r]);
            }
        }
        return make_pair(-1,-1);
    };

    int mi=*min_element(all(a));
    pii p=go(mi-1);
    if(p.first<0) {
        cout<<"YES"<<endl;
        return;
    }
    pii p1=go(p.first);
    pii p2=go(p.second);
    if(p1.first<0 || p2.first<0)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
