/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Let p[] be some ids of friends.
 * Consider the subsequence b[p[]] of invited friends.
 * At position p[k] it must hold that b[p[k]]>=k, since there are k-1 poorer guests.
 *
 * Or otherwise:
 * if we choose guest i, then
 * we can choose max b[i]-1 guests left of i, and max a[i] guests right of i
 * And then?
 * Consider the number of overlapping segments...somehow :/
 *
 * ***
 * Maintain a thougt set of invited persons, then check all persons sorted by richness,
 * poorest first.
 * If we invite this person, it will be on first position in our thougt set, 
 * so its a[] and b[] must be according to this.
 * Repeat.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    /* consider inviting x persons, so our thougt set has size x 
     * and must be filled. */
    function<bool(int)> check=[&](int x) {
        int pos=0;
        for(int i=0; i<n; i++) {
            if(b[i]>=pos && a[i]>=x-pos-1)
                pos++;
        }
        return pos>=x;
    };

    int l=1;    /* note that we allways can invite one person */
    int r=n+1;
    while(l+1<r) {
        int mid=(l+r)/2;
        if(check(mid)) {
            l=mid;
        } else 
            r=mid;
    }
    cout<<l<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
