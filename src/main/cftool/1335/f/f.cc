/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

struct Dsu {
    vector<int> p;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* combines the sets of two members. 
 * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};

/* greedy
 * we need to find the loops, and the size of the loops.
 * we can fill the loops with robots.
 * Then, there are "intopathes" to the loops.
 * We can use robots from white loop cells and place them 
 * on black intopathes cells.
 */
void solve() {
    cini(n);
    cini(m);

    vs bw(n);
    for(int i=0; i<n; i++)
        cin>>bw[i];

    vs a(n);
    for(int i=0; i<n; i++)
        cin>>a[i];
    
    Dsu dsu(n*m);
    vvb vis(n, vb(m));

    function<void(int,int)> dfs=[&](int i, int j) {
        if(vis[i][j])
            return;
        vis[i][j]=true;
        if(a[i][j]=='U') {
            dsu.union_sets(i*n+j, (i-1)*n+j);
            dfs(i-1, j);
        } else if(a[i][j]=='D') {
            dsu.union_sets(i*n+j, (i+1)*n+j);
            dfs(i+1, j);
        } else if(a[i][j]=='L') {
            dsu.union_sets(i*n+j, i*n+j-1);
            dfs(i, j-1);
        } else if(a[i][j]=='R') {
            dsu.union_sets(i*n+j, i*n+j+1);
            dfs(i, j+1);
        } else
            assert(false);
    };

    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            if(vis[i][j])
                continue;

            dfs(i,j);
        }
    }

/* now each set set in the dsu has a loop or not.
 * if not the "loop" is the last field only.
 * We need to know the black fields of the sets, and the loops of the sets.
 */
    map<int, vi> loops;
    map<int, vi> blacks;
    map<int, vi> whites;
    set<int> sets;

/* how to find the loops ?
 * dfs somewhere in set.
 * at some step we find vis node.
 * then number of step gives length of loop.
 * loop allways exists, min len 2.
 */

    vvi depth(n, vi(m));

    pii loopend;
    //int loopsz;
    function<void(int,int,int)> dfs2=[&](int i, int j, int d) {
        if(depth[i][j]>0)  {
            loopend={i,j};
            //loopsz=d-depth[i][j];
            return;
        }
        depth[i][j]=d;

        d++;
        if(a[i][j]=='U')
            dfs2(i-1, j, d);
        else if(a[i][j]=='D')
            dfs2(i+1, j, d);
        else if(a[i][j]=='L')
            dfs2(i, j-1, d);
        else
            dfs2(i, j+1, d);
    };

    int curSet;
    vvb vis3(n, vb(m));
    function<void(int,int)> dfs3=[&](int i, int j) {
        if(vis3[i][j])
            return;
        vis3[i][j]=true;

        loops[curSet].push_back(i*n+j);
        if(a[i][j]=='U')
            dfs3(i-1, j);
        else if(a[i][j]=='D')
            dfs3(i+1, j);
        else if(a[i][j]=='L')
            dfs3(i, j-1);
        else
            dfs3(i, j+1);
    };
    
    for(int i=0; i<n; i++) {    
        for(int j=0; j<n; j++) {
            int s=dsu.find_set(i*n+j);
            if(bw[i][j]=='0')
                blacks[s].push_back(i*n+j);
            else
                whites[s].push_back(i*n+j);

            if(sets.count(s))
                continue;

            sets.insert(s);

            depth=vvi(n, vi(m));
            dfs2(i,j,1);
            curSet=s;
            vis3=vvb(n, vb(m));
            dfs3(loopend.first, loopend.second);
        }
    }

/*
 * we need to find the loops, and the size of the loops.
 * we can fill the loops with robots.
 * Then, there are "intopathes" to the loops.
 * We can use robots from white loop cells and place them 
 * on black intopathes cells.
 */

    int ansCells=0;
    int ansBlack=0;
    for(int s : sets) {
        ansCells+=loops[s].size();
        ansBlack+=blacks[s].size();
    }

    cout<<ansCells<<" "<<ansBlack<<endl;
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

