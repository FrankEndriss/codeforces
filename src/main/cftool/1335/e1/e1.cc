/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cinai(a,n);

    vvi dp(27, vi(n));
    vvi idx(27);
    for(int i=0; i<n; i++) {
        if(i>0) {
            for(int j=0; j<27; j++)
                dp[j][i]=dp[j][i-1];
        }
        dp[a[i]][i]++;
        idx[a[i]].push_back(i);
    }

    int ma=0;
    for(int j=0; j<27; j++) {
        /* use j as a */
        for(int i=0; i<idx[j].size()/2; i++) {
            int l=idx[j][i];
            int r=idx[j][idx[j].size()-1-i]-1;
            /* find max b */
            for(int k=0; k<27; k++) {
                if(k==j)
                    continue;

                int cnt=dp[k][r]-dp[k][l];
                ma=max(ma, cnt+i+i+2);
            }
        }
        ma=max(ma, (int)idx[j].size());
    }

    cout<<ma<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

