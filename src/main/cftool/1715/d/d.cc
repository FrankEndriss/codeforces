/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * WLG consider i<j
 * Then we collect all rules, and 
 *
 * What about i==j?
 */
void solve() {
    cini(n);
    cini(q);

    vector<vector<pii>> adj(n);
    for(int i=0; i<q; i++) {
        cini(u); u--; 
        cini(v); v--;
        cini(x);
        adj[min(u,v)].emplace_back(max(u,v), x);
    }

    vi a(n);
    for(int i=0; i<n; i++) {
        for(auto [j,x] : adj[i]) {
            /* we must set in a[j] all bits of x that are not set in a[i] */
            for(int k=0; k<=30; k++) {
                int b=(1LL<<k);
                if((x&b) && !(a[i]&b))
                    a[j]|=b;
            }
        }
    }
    for(int i=0; i<n; i++) 
        cout<<a[i]<<" ";
    cout<<endl;
}

signed main() {
        solve();
}
