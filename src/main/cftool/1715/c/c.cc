/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Brute force is trivial, but way TLE.
 * So, how can we optimize this?
 *
 * Consider the array to be list of values and size, also start index.
 * let start[i] be index of first element of block i
 * 
 * The first block contributes start[i] (start[i] 0 based):
 * n-start[0]-1 + n-start[0]-2 + n-start[0]-3 +...+ n-start[0]-sz[0]
 * Second block:
 * n-start[1]-1 + n-start[1]-2 + n-start[1]-3 +...+ n-start[1]-sz[1]
 * ...
 * So we can insert the contribution of each block in a segtree at position
 * of start[i].
 *
 * Maintain the blocks in a set<tuple<start,sz,val>>
 *
 * ....
 * **********
 * There is (of course) a simpler structure.
 * Think about maintaining the positions of block-changes.
 * Then each update removes/adds at most two such
 * positions, and we can most simply maintain them
 * by maintaining the array a[]
 */
void solve() {
    cini(n);
    cini(m);

    map<int,pii> a; /* [start]=<size,val>  */
    int prev=-1;
    int cnt=0;
    int pstart=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux==prev || i==0)
            cnt++;
        else {
            a[pstart]={cnt, prev};
            cnt=1;
            pstart=i;
        }

        prev=aux;
    }
    a[pstart]={cnt, prev};  /* last block */

    segtree seg(n);
    for(auto ent : a) {
        const int st=ent.first;
        const int sz=ent.second.first;
        const int val=ent.second.second;
        seg.set(st, (n-st)*(n-st-1)/2 - (n-st-sz)*(n-st-sz-1)/2);
    }

    /* update segtree at position i */
    function<void(int)> go=[&](int i) {
        auto it=a.find(i);
        assert(it!=a.end());

        if(it->second.first==0) {
            seg.update(it->first,0);
        } else {
            set.update(it->first,
                    (n-it->second.first)*(n-it->second.first-1)/2 - 
                    (n-it->second.first-it->second.second)*(n-it->second.first-it->second.second-1)/2);
        }
    };

    for(int i=0; i<m; i++) {
        cini(idx);
        cini(x);

        auto it=a.lower_bound(idx);
        assert(it!=a.begin());
        it--;
        idx--;  /* zero based */

        /* several main cases, with subcases:
         * if(x==value of segment of idx), then no update neccessary.
         * else
         * 1. idx=first element of segment it
         *      if x==left value, move one element from current to left segment
         *      else create new single element segment
         * 2. idx=last element of segment it
         *      same as first, but on right side
         * 3. idx=some inner element of segment it
         *      split segment into 3 segments
         *
         * ..if current segment has size 0 remove it.
         *
         * But, actually, thats pain to implement. So much ifs, so much off-by-one errors :/
         * How can Neal write this in like 4 minutes?
         */
        const int st=it->first;
        const int sz=it->second.first;
        const int val=it->second.second;
        if(val!=x) {
            it->second.first--;
            go(it->first);

            if(st!=0) {
                auto itL=it;
                itL--;
                const int stL=itL->first;
                const int szL=itL->second.first;
                const int valL=itL->second.second;
                if(valL==x) { /* move one element from current segment to left segment */
                    it->second.first--;
                    itL->second.first++;

                    if(it->second.first==0)
                        a.erase(it);
                }
            }
            it=a.erase(it);

            ... TODO
                a bazillion other cases :/
        }


        const int ans=seg.prod(0,n);

        cout<<ans<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
