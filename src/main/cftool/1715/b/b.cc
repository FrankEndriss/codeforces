/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to add "neutral" values to a[i].
 * First grader math... :/
 */
void solve() {
    cini(n);
    cini(k);
    cini(b);
    cini(s);

    vi a(n);
    a[0]=b*k;
    int ss=b*k;

    if(ss>s) {
        cout<<-1<<endl;
        return;
    }

    for(int i=0; ss<s && i<n; i++) {
        int val=min(k-1, s-ss);
        a[i]+=val;
        ss+=val;
    }

    if(ss!=s)
        cout<<-1<<endl;
    else {
        for(int i=0; i<n; i++) 
            cout<<a[i]<<" ";
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
