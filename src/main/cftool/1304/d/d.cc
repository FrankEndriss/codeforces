/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Max len LIS is max elements to right, smallest to left.
 *
 * Min len LIS is the other way, max elements to left,
 * smallest to right.
 *
 * How to distribute numbers greedily?
 * Permutation.
 * How to find biggest/smallest possible numbers for positions?
 *
 * We need to consider the increasing parts.
 * For maxlen we use the iota, and reverse the decreasing parts.
 *
 * For minlen we use the iota from backwards, then do the same.
 */
void solve() {
    cini(n);
    cins(s);

    vi b(n);
    iota(all(b), 1);
    reverse(all(b));

/* min lis, reverse increasing parts. */
    int prev=0;
    int sz=1;
    for(int i=0; i<s.size(); i++) {
        if(s[i]=='<')
            sz++;
        else {
            if(sz>0) {
//cerr<<"reverse prev="<<prev<<" sz="<<sz<<endl;
                reverse(b.begin()+prev, b.begin()+prev+sz);
            }
            prev=i+1;
            sz=1;
        }
    }
//cerr<<"last reverse, prev="<<prev<<endl;
    reverse(b.begin()+prev, b.end());

    for(int i=0; i<b.size(); i++)
        cout<<b[i]<<" ";
    cout<<endl;

/* max lis, reverse decreasing parts. */
    iota(all(b), 1);
    prev=0;
    sz=1;
    for(int i=0; i<s.size(); i++) {
        if(s[i]=='>')
            sz++;
        else {
            if(sz>0) {
//cerr<<"reverse prev="<<prev<<" sz="<<sz<<endl;
                reverse(b.begin()+prev, b.begin()+prev+sz);
            }
            prev=i+1;
            sz=1;
        }
    }
//cerr<<"last reverse, prev="<<prev<<endl;
    reverse(b.begin()+prev, b.end());

    for(int i=0; i<b.size(); i++)
        cout<<b[i]<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
