/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* @return the interval to calc the sum of the overlapping part of both segments */
inline pii overlap(int j0, int j1, int k) {
    if(j0>j1)
        swap(j0,j1);

    if(j0+k<=j1)
        return { 0LL, 0LL };
    else 
        return { j1, j0+k };
}

/* dp
 * foreach day we iterate all all possible segments.
 * There are 3 ranges of segments:
 * - todays segment completly left of yesterdays segment
 *   -> max(range(yesterday))+ pre[...sum today and tomorrow...])
 * - todays segemnt completly right of yesterdays segment
 *   -> max(range(yesterday))+ pre[...sum today and tomorrow...])
 * - todays segemtn overlaps with yesterdays
 *   foreach position in m there are k*2-1 overlaps.
 *
 *
 * How to find the max of yesterday in some range?
 * -> it is "only" prefix max and/or suffix max, so it is O(m).
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vvi a(n+1, vi(m));  /* one extra row with zeros */
    vvi pre(n+1, vi(m+1));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) {
            cin>>a[i][j];
            pre[i][j+1]=pre[i][j]+a[i][j];
        }

    vi dp(m);   /* dp[j]=max score if using the camera on ith day left at segment j */
    for(int j=0; j+k<=m; j++) {
        dp[j]=pre[0][j+k]-pre[0][j];   /* first row */
        dp[j]+=pre[1][j+k]-pre[1][j];  /* second row */
    }

    for(int i=1; i<n; i++) {
        vi dp0(m);
        vi maL(m);
        vi maR(m);

        maL[0]=dp[0];
        maR[m-1]=dp[m-1];
        for(int j=1; j<m; j++) {
            maL[j]=max(maL[j-1], dp[j]);
            maR[m-j-1]=max(maR[m-j], dp[m-j-1]);
        }

        for(int j=0; j+k<=m; j++) {
            const int today=pre[i][j+k]-pre[i][j];
            const int tomor=pre[i+1][j+k]-pre[i+1][j];
            if(j-k>=0) {    /* yesterday complete left of today */
                dp0[j]=max(dp0[j], today+tomor+maL[j-k]);
            }

            if(j+k+k<=m) {  /* yesterday complete right of today */
                dp0[j]=max(dp0[j], today+tomor+maR[j+k]);
            }

            /* overlap of yesterday and today */
            for(int j1=max(0LL, j-k+1); j1<j+k && j1+k<=m; j1++) {
                auto [l,r]=overlap(j,j1,k);
                dp0[j]=max(dp0[j], today+tomor - (pre[i][r]-pre[i][l]) + dp[j1]);
            }
        }

        dp.swap(dp0);
    }

    int ans=*max_element(all(dp));
    cout<<ans<<endl;


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
