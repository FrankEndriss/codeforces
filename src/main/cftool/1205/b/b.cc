/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * If there are 3 numbers haveing the same bit set then this is the shortest cycle.
 * Else n<60*3 and we can dfs the cycle.
 *
 * How to find a shortest cycle?
 * Usual algos (Floyed Warshal) will find the shortest cycle as len==2, but that is 
 * not what we want.
 * Since number of edges is small we can brute force.
 *
 * Note edgecase a[i]==0
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);

    vi b(64);
    for(int i=0; i<n; i++) {
        for(int j=0; (1LL<<j)<=INF; j++) {
            if(a[i]&(1LL<<j)) {
                b[j]++;
                if(b[j]>=3) {
                    cout<<3<<endl;
                    return;
                }
            }
        }
    }

    vector<pii> edg;
    vector<set<int>> adj(n);
    for(int i=0; i<n; i++) {
        if(a[i]==0)
            continue;
        for(int j=i+1; j<n; j++) {
            if(a[i]&a[j]) {
                adj[i].insert(j);
                adj[j].insert(i);
                edg.emplace_back(i,j);
            }
        }
    }

    int ans=INF;
    for(size_t i=0; i<edg.size(); i++) {
        adj[edg[i].first].erase(edg[i].second);
        adj[edg[i].second].erase(edg[i].first);

        queue<int> q;
        q.push(edg[i].first);
        vi dp(n, INF);
        dp[edg[i].first]=0;
        while(q.size()) {
            int v=q.front();
            q.pop();
            for(int chl : adj[v]) {
                if(dp[chl]>dp[v]+1) {
                    dp[chl]=dp[v]+1;
                    q.push(chl);
                }
            }
        }

        ans=min(ans, dp[edg[i].second]+1);

        adj[edg[i].first].insert(edg[i].second);
        adj[edg[i].second].insert(edg[i].first);
    }

    if(ans==INF)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
