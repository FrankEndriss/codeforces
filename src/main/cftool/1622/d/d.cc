/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp[i][j]=number of possible outcomes of substr i,j while changing i and j
 *
 * Consider max substrings, that is foreach i the longest substring starting at i
 * and having k 1 in it.
 *
 * Then iterate the max substrings left to right, calculating the number of outcomes
 * foreach non overlapping segment.
 * Note that each segment is a subsegment of a subarray, so there are allways k 1s 
 * available, and from all subarrays is contained in, the max number of 0s.
 *
 * Then, foreach such segment, since the two outermost positions are fixed to change,
 * the number of ways is nCr(size-2, available1s)
 *
 * Use sweepline to construct the segments.
 */
using mint=modint998244353;
void solve() {
	cini(n);
	cini(k);
	cins(s);

	// TODO implement...somehow
}

signed main() {
    solve();
}
