/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Move the biggest "1" to n, second biggest to n-1...
 * Move the smallest "0" to 1, the second smallest to 2...
 */
void solve() {
	cini(n);
	cinai(a,n);
	cins(s);

	vector<pii> sm;
	vector<pii> bg;
	for(int i=0; i<n; i++)  {
		if(s[i]=='0')
			sm.emplace_back(a[i],i);
		else
			bg.emplace_back(a[i],i);
	}

	sort(all(sm));
	sort(all(bg), greater<pii>());

	vi ans(n);
	for(int i=0; i<(int)sm.size(); i++) {
		ans[sm[i].second]=i+1;
	}

	for(int i=0; i<(int)bg.size(); i++) {
		ans[bg[i].second]=n-i;
	}
	for(int i: ans)
		cout<<i<<" ";
	cout<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
