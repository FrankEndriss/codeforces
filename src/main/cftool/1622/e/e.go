package main

import (
	"bufio"
	. "fmt"
	"log"
	"os"
	"sort"
	"strconv"
	//"container/heap"
)

var _ = Scan // reference fmt

type ll int64

const _0 ll = 0
const _1 ll = 1
const INF ll = 1e18 + 7

type Sll []ll

func (s Sll) Sort() {
	sort.Slice(s, func(i, j int) bool { return s[i] < s[j] })
}

func btoi(b bool) ll {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 ll, i2 ll) ll { return Sll{i1, i2}[btoi(i1 < i2)] }
func min(i1 ll, i2 ll) ll { return Sll{i2, i1}[btoi(i1 < i2)] }
func abs(i1 ll) ll        { return max(i1, -i1) }
func absi(i1 int) int {
	if i1 > 0 {
		return i1
	} else {
		return -i1
	}
}

func makeii(n ll, m ll) []Sll {
	a := make([]Sll, n)
	for i := _0; i < n; i++ {
		a[i] = make(Sll, m)
	}
	return a
}

var r *bufio.Reader
var wr *bufio.Writer
var er *bufio.Writer

func cini() ll {
	s := cins()
	ans, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		log.Panic("cini failed, s=", s)
	}
	return ll(ans)
}
func cinai(n ll) Sll {
	ans := make(Sll, n)
	for i := range ans {
		ans[i] = cini()
	}
	return ans
}

// read a WS delimited word, works also on atcoder with long strings
func cinb() []byte {
	s := make([]byte, 0)
	for {
		b, err := r.ReadByte()
		if err != nil {
			return s
		}
		if b == 10 || b == 13 || b == '\t' || b == ' ' {
			continue
		} else {
			s = append(s, b)
			break
		}
	}
	for {
		b, err := r.ReadByte()
		if err != nil || b == 10 || b == 13 || b == '\t' || b == ' ' {
			break
		}
		s = append(s, b)
	}

	return s
}
func cins() string {
	return string(cinb())
}

// Each value p[i] adds p[i] points to the studends j having s[j][i]==true
// There are only 10 students, consider we want to
// maximize/minimize each one, 1024 configurations.
// Then each question has a min/max scoring, order the questions by that,
// and assign the p[i] according to that ordering.
// One of the 2^10 configurations gives max ans.
func solve() {
	var n, m int
	Fscan(r, &n, &m)

	x := make([]int, n)
	for i := range x {
		Fscan(r, &x[i])
	}

	vis := make([]string, n)
	for i := range vis {
		Fscan(r, &vis[i])
	}

	var ansp []int
	ans := int(-1e9)
	for i := 0; i < (1 << n); i++ {
		sc := make([]ll, m) // score of the m questions
		for k := 0; k < m; k++ {
			for j := 0; j < n; j++ {
				if i&(1<<j) > 0 { // student j should be maximized
					if vis[j][k] == '1' {
						sc[k]++
					}
				} else { // student j should be minimized
					if vis[j][k] == '1' {
						sc[k]--
					}
				}
			}
		}

		p := make([]ll, m)
		for j := range p {
			p[j] = ll(j)
		}
		sort.Slice(p, func(i, j int) bool { return sc[p[i]] < sc[p[j]] })

		pp := make([]int, m)
		for j := range pp {
			pp[p[j]] = j + 1
		}

		xx := make([]int, n)
		for j := 0; j < n; j++ {
			for k := 0; k < m; k++ {
				xx[j] += pp[k] * int(vis[j][k]-'0')
			}
		}
		lans := 0
		for j := 0; j < n; j++ {
			lans += absi(xx[j] - x[j])
		}
		if lans > ans {
			ans = lans
			ansp = pp
		}
	}
	for i := range ansp {
		Fprintf(wr, "%d ", ansp[i])
	}
	Fprintln(wr)
}

func main() {
	r = bufio.NewReader(os.Stdin)
	wr = bufio.NewWriter(os.Stdout)
	er = bufio.NewWriter(os.Stderr)
	defer wr.Flush()
	defer er.Flush()
	for t := cini(); t > 0; t-- {
		solve()
	}
}
