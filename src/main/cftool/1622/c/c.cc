/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* ternary search, see https://en.wikipedia.org/wiki/Ternary_search */
/* return the point in [l,r] where f(x) is max */
int terns(int l, int r, function<int(int)> f) {
    while(l+2<r) {
        const int l3=l+(r-l)/3;
        const int r3=r-(r-l)/3;
        if(f(l3)<f(r3))
            l=l3;
        else
            r=r3;
    }
    return (l+r)/2;
};

/**
 * Its allways optimal to first do the decrease steps on the smallest 
 * element, then the copies.
 * Prefix sums and binary search on decrease steps...somehow.
 */
const int INF=1e18;
void solve() {
	cini(n);
	cini(k);
	cinai(a,n);

	if(n==1) {
		cout<<max(0LL, a[0]-k)<<endl;
		return;
	}

	sort(all(a));

	const int sum=accumulate(all(a), 0LL);
	if(sum<=k) {
		cout<<0<<endl;
		return;
	}

	const int diff=sum-k;

	vi pre(n+1);
	for(int i=0; i<n; i++) {
		pre[i+1]=pre[i]+a[n-1-i]-a[0];
	}

	int ans=INF;
	/* now try y copy steps, and calc decreases. */
	for(int y=n-1; y>=0; y--) {
		/* now find the number of needed decreases if copies==y.
		 * foreach decrease we get y+1 points */
		int need=diff-pre[y];
		if(need<=0) {
			ans=min(ans, y);
		} else {
			int val=(need+y)/(y+1);
			ans=min(ans, y+val);
		}
	}


	cout<<ans<<endl;
}

signed main() {
	cini(t);
	while(t--) 
    		solve();
}
