/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* greedy
 * we need to find the longest subarray with max
 * 1 silver.
 * We record all S indexes, and greedy search the best triple.
 *
 * Corner cases: 
 * ans<=cntG
 * All S => ans=0
 */
void solve() {
    cini(n);
    cins(s);
    vi pos;
    pos.push_back(-1);
    int cntG=0;
    for(int i=0; i<n; i++)
        if(s[i]=='S')
            pos.push_back(i);
        else
            cntG++;

    pos.push_back(n);
    if(pos.size()==n+2) {
        cout<<0<<endl;
        return;
    }

    int ans=pos[1]-pos[0]-1;
    for(size_t i=1; i+1<pos.size(); i++) {
        int g1=pos[i]-pos[i-1]-1;
        int g2=pos[i+1]-pos[i]-1;
        ans=max(ans, g1+g2+1);
    }
    cout<<min(cntG, ans)<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
