/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int dig(int i) {
    int ans=0;
    while(i>0) {
        ans++;
        i/=10;
    }
    return ans;
}
/* A number on the right contributes a[i]%k,
 * number on the left contributes (a[i]*10*dig(a[j]))%k
 *
 * So, find for every number
 * %k
 * num digits
 * %k if used on the left with a 1-10 digit number.
 * Then we can simply add up the results.
 * Consider pairs a[i]*a[i] somehow.
 *
 * What about a[i]==1e9?
 * Resulting number does not fit into longlong
 * since 20 digits.
 * -> always use %k
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vvi rem(11);
    int ans=0;
    vi ad(n);
    for(int i=0; i<n; i++) {
        const int digi=dig(a[i]);
        ad[i]=digi;
        int aa=a[i]%k;
        for(int j=0; j<=10; j++) {
            rem[j].push_back(aa);

            if(j==digi && (aa+a[i])%k==0)
                ans--;

            aa=(aa*10)%k;
        }
    }

    for(size_t i=0; i<rem.size(); i++)
        sort(all(rem[i]));

    for(int i=0; i<n; i++) {
/* Note here we need to multiply a[i] with 10^digits.
 * But I dont get it why.
 */
        auto it1=lower_bound(all(rem[ad[i]]), a[i]%k);
        auto it2=upper_bound(all(rem[ad[i]]), a[i]%k);
        ans+=distance(it1, it2);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
