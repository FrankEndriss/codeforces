/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* 
 * Observe that the n-intersection is == min(r),max(l)
 *
 * So we try to remove every single segment, and find
 * n-intersection of remaining intervals.
 * How to find that value if one segment is removed?
 * Build prefix/postfix of min/max values.
 * Then we can create the min/max values from prefix before
 * i, and postfix after i in O(1), so it runs O(n)
 *
 * Note that this is error prone to off by ones, so we
 * use prefix sum arrays of same size as data arrays
 * to make the code understandable.
 */
const int INF=1e9+7;
void solve() {
    cini(n);
    vi l(n);
    vi r(n);
    vi minL(n, INF);
    vi maxL(n);
    vi minR(n, INF);
    vi maxR(n);

    for(int i=0; i<n; i++)
        cin>>l[i]>>r[i];

    minL[0]=r[0];
    maxL[0]=l[0];
    minR[n-1]=r[n-1];
    maxR[n-1]=l[n-1];

    for(int i=1; i<n-1; i++) {
        minL[i]=min(minL[i-1], r[i]);
        maxL[i]=max(maxL[i-1], l[i]);
        minR[n-1-i]=min(minR[n-i], r[n-1-i]);
        maxR[n-1-i]=max(maxR[n-i], l[n-1-i]);
    }

    int ans=0;
    for(int i=0; i<n; i++) {
        int mir=INF;
        int mal=0;
        if(i>0) {
            mir=minL[i-1];
            mal=maxL[i-1];
        }
        if(i<n-1) {
            mir=min(mir, minR[i+1]);
            mal=max(mal, maxR[i+1]);
        }
        ans=max(ans, mir-mal);
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

