/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Once the prefix product eq 0 it stays 0, so if we can
 * use 0 at all, we can use it in the last position.
 * Also, prefix product 1 should be used with first
 * number, else 1 cannot be used at all.
 * So all seqs start with 1, end end with product eq 0,
 * so if 0 is used then in last position.
 *
 * ???
 */
void solve() {
    cini(n);
    cini(m);
    set<int> num;
    for(int i=2; i<m; i++) 
        num.insert(i);

    vb vis(m);
    for(int i=0; i<n; i++) {
        cini(aux);
        num.erase(aux);
        if(aux==0)
            vis[0]=true;
    }

    cout<<1<<" ";
    int e=1;
    vis[1]=true;

    int prod=1;
    while(num.size()) {
        auto it=num.begin();
        if(e==1) {
            it=num.end();
            it--;
        }

        if(vis[prod*(*it)%m]) {
            num.erase(it);
            continue;
        }

        cout<<*it<<" ";
        prod=(prod*(*it))%m;
        vis[prod]=true;
        e=1-e;
    }
    if(!vis[0])
        cout<<"0 ";
    cout<<endl;
}

signed main() {
        solve();
}
