/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Since the power charger can be plugged arbitraryly
 * we can assume it charges several devices simultaniusly.
 *
 * Binary search.
 *
 * Each device has some t[i] from where it needs
 * a[i] power from 'somewhere'.
 * So for time t0 we know how much that device would
 * need. We add these values up for all devices.
 * If sum is more than charger can produce until t, then
 * t is not possible, else t is possible.
 */
void solve() {
    cini(n);
    cini(p);
    vector<pii> ab(n);
    int suma=0;
    for(int i=0; i<n; i++) {
        cin>>ab[i].first>>ab[i].second;
        suma+=ab[i].first;
    }

    if(suma<=p) {
        cout<<-1<<endl;
        return;
    }


    function<bool(double)> poss=[&](double t) {
        double e0=0;
        for(int i=0; i<n; i++)
            e0+=max(0.0, ab[i].first*t-ab[i].second);

        double e1=p*t;
        return e1>=e0;
    };

    double l=0;
    double r=1e17;

    while(l+0.00001<r) {
        double t=(l+r)/2;
        if(poss(t))
            l=t;
        else
            r=t;
    }

    cout<<l<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
