/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* len of resulting seq is
 * len of seq + more symbols.
 *
 * Obviously a non dec seq begins at a
 * 1, which is first of a block of 1,
 * then all 2s are added until next one.
 *
 * We need to find the length of these blocks,
 * and the max sum of the consecutive elements.
 * So, compress the list to
 * x, y,x1,y1...
 * all x are 1s, all y are 2.
 * c is prop padded with 0 at left and/or right.
 *
 * We can move any pair right of some pair directly after
 * the left pair, ie
 * x,y,...y1,x1,y2
 * -> x,y,y1,...
 * -> x,x1,y1,...
 *  x,y,x1,y1 -> x,x1,y,y1
 *
 * So ans is max of 
 * all pairs<x,y>+max(y right of that pair), and
 * all pairs<x,y>+max(x left  of that pair)
 * all 4 consecutive starting with x, since we can swap middle pair
 * make prefix sum from left and right.
 *
 * ************************************************************
 * -> Ok, once again misunderstood subsequence for subarray :/
 * Now solve again for subseq:
 *
 * Without swap is is for all positions i
 * max((1s <=i)+(2s >=i))
 *
 * Make prefix sums so we can say for any interval how much
 * 1s and 2s are there.
 *
 * Then after swaps we got 3 parts
 * <unswapped><swappped><unswapped>
 * Numbers can be distributed like:
 * 1,1,1
 * 1,1,2
 * 1,2,2
 * 2,2,2
 * Loop over all part sizes, find max of the 4 posis.
 * Ignore the 1st and 4th, it is same for all part sizes.
 *
 * ********************************************************
 * All wrong :/ :/ :/
 * See second example. The middle part is worth the 
 * max dec subseq of that part. Not the number of 1s or 2s.
 *
 * So, what we need is, for the whole list,
 * the longest dec or nondec subseq for all possible intervals.
 * How to get that?
 * ->just create in O(n^2);
 */
void solve() {
    cini(n);
    cinai(a,n);

    vvi pre(2, vi(n+1));
    for(int i=0; i<n; i++) {
        pre[0][i+1]=pre[0][i]+(a[i]==1);
        pre[1][i+1]=pre[1][i]+(a[i]==2);
    }

/* first=(0,i]; second=(i, j], third=(j,n] */
    auto cnt=[&](int sym, int l, int r) {
        assert(l<=r);
        return pre[sym][r]-pre[sym][l];
    };

    vvi dp(n+1, vi(n+1));   /* dp[i][j]=len of longest dec subseq in interval (l,r] */

    for(int i=0; i<n; i++) {
        vi ldp(2); /* ldp[0]=longest so far ending in 1, ldp[1]=ending in 2 */
        for(int j=i+1; j<=n; j++) {
            if(a[j-1]==2)
                ldp[1]++;
            else if(a[j-1]==1)
                ldp[0]=max(ldp[1]+1, ldp[0]+1);
            else
                assert(false);

            dp[i][j]=max(ldp[0], ldp[1]);
        }
    }

    int ans=max(pre[0][n], pre[1][n]);   /* 1,1,1 or 2,2,2 */

    for(int i=0; i<n; i++) {
        int first=cnt(0,0,i);
        for(int j=i; j<=n; j++) {
            int third=cnt(1,j,n);
            ans=max(ans, first+third+dp[i][j]);
        }
    }
    
    cout<<ans<<endl;
}

/* simpler, faster O(n) solution, see tutorial
 * The final subseq consists of 4 subparts in 
 * original list
 * num 1s, num 2s, num 1s, num 2s
 * So iterate the list once and simply count.
 */
void solve2() {
    cini(n);

    vvi dp(n+1, vi(4));
    for(int i=1; i<=n; i++)  {
        cini(a);

        if(a==1) {
            dp[i][0]=dp[i-1][0]+1;
            dp[i][1]=max(dp[i-1][1], dp[i][0]);
            dp[i][2]=max(dp[i-1][2]+1, dp[i-1][1]+1);
            dp[i][3]=max(dp[i-1][3], dp[i-1][2]);
        } else if(a==2) {
            dp[i][0]=dp[i-1][0];
            dp[i][1]=max(dp[i-1][1]+1, dp[i][0]+1);
            dp[i][2]=max(dp[i-1][2], dp[i-1][1]+1);
            dp[i][3]=max(dp[i-1][3]+1, dp[i-1][2]+1);
        } else 
            assert(false);
    }

    int ans=0;
    for(int i=0; i<4; i++) 
        ans=max(ans, dp[n][i]);

    cout<<ans<<endl;
}

signed main() {
    solve2();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
