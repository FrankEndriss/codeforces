/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Meh, that no fun.
 * Try to find a simple ruleset so that if satisfied.
 * ...not possible.
 *
 * Consider all possible segments, that is
 * b[0] is a cnt field,
 * or it is not but one of the following fields.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vb vis(n+1);
    vb dp(n+1);

    /* check if seq starting at i is a network seq */
    function<bool(int)> go=[&](int i) {
        if(vis[i])
            return dp[i];

        vis[i]=true;
        if(i==n) 
            return dp[i]=true;

        bool ans=false;
        if(i+a[i]+1<=n)
            ans=go(i+a[i]+1);

        for(int j=1; i+j<n; j++) {
            if(a[i+j]==j)
                ans=ans || go(i+j+1);
        }
        return dp[i]=ans;
    };

    bool ans=go(0);
    if(ans)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
