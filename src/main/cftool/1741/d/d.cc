/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Check segments by increasing size.
 * Each segment must contain numbers starting by a multiple
 * of size, and contain no numbers out of segment.
 */
const int INF=1e9;
void solve() {
    cini(m);
    cinai(p,m);
    for(int i=0; i<m; i++)
        p[i]--;

    int ans=0;
    for(int sz=2; sz<=m; sz*=2) {
        for(int i=0; i<m; i+=sz) {
            int mi=INF;
            int ma=-1;
            for(int j=0; j<sz; j++) {
                mi=min(mi, p[i+j]);
                ma=max(ma, p[i+j]);
            }
            if(ma-mi+1!=sz)  {
                cout<<-1<<endl;
                return;
            }

            bool sw=false;
            for(int j=0; j<sz; j++) {
                if(p[i+j]!=mi+j)
                    sw=true;
            }
            if(sw)
                ans++;

            sort(p.begin()+i, p.begin()+i+sz);
        }
    }
    if(ans<0)
        cout<<-1<<endl;
    else
        cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
