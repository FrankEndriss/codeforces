/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int val(string s) {
    if(s=="M")
        return 0;
    
    int cnt=1;
    for(int i=0; i<s.size(); i++) 
        if(s[i]=='X')
            cnt++;
    if(s.back()=='S')
        return -cnt;
    else if(s.back()=='L')
        return cnt;
    else 
        assert(false);
}
/**
 */
void solve() {
    cins(s);
    cins(t);

    int v1=val(s);
    int v2=val(t);
    if(v1<v2)
        cout<<"<"<<endl;
    else if(v1>v2)
        cout<<">"<<endl;
    else
        cout<<"="<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
