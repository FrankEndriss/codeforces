/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return max(a,b);
}

S st_e() {
    return 0LL;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Iterate colors, and
 * consider two sets of segments, one with the segments of 
 * current color, one of all others.
 *
 * Then find foreach seg of current color the nearest from the 
 * other set. Check both endpoints is simple binsearch.
 *
 * But how to check if there is an intersection?
 * When compress the segements we can use a segtree, 
 * store right endpoint at pos of left endpoint.
 * Then we can find for all position left of current seg
 * if there exist one with right endpoint left of it.
 * Also we can check if any other seg begins in current seg.
 *
 * So, 
 * -compress
 * -group by color
 * -put all segemnts into segtree
 * -iterate colors, foreach
 *  --remove all of current color from segtree
 *  --check all of current color
 *
 *  How to handle same left endpoints?
 *  -> For same left endpoints, use max right endpoint.
 *  -> Also maintain an extra-multiset with all endpoints
 *
 *  ...no, that segtree is shit.
 *  How can we find intersections using sets<pii> ?
 *  -maintain one multiset with all endpoints
 *  -maintian one multiset ordered by left
 *  -maintian one multiset ordered by right
 *
 *  ...idk :/
 */
void solve() {
    cini(n);

    /* compress */
    using t3=tuple<int,int,int>;
    vector<t3> data;
    map<int,int> id;
    for(int i=0; i<n; i++) {
        cini(l);
        cini(r);
        cini(c);
        data.emplace_back(l,r,c);
        id[l]=1;
        id[r]=1;
    }
    int val=1;
    for(auto it=id.begin(); it!=id.end(); it++) 
        it->second=val++;

    map<int,vector<pii>> byc;   /* by color */
    multiset<int> ep;   /* all endpoints  */

    stree seg(n+1);
    for(int i=0; i<n; i++) {
        auto [l,r,c]=data[i];
        l=id[l];
        r=id[r];

        byc[c].emplace_back(l,r);
        ep.insert(l);
        ep.insert(r);

        seg.set(l, max(seg.get(l), r));
    }

    for(auto it=byc.begin(); it!=byc.end(); it++) {
        int c=it->first;
        for(size_t i=0; i<it->second.size; i++) {
            auto [l,r]=it->second[i];
            ep.erase(l);
            ep.erase(r);
        }

        ...something
        How to remove the segments from the segtree?
        We would have to use another set maintaining
        which segment was entered into the segtree...thats no fun.
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
