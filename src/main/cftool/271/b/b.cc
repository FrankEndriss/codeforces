/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

void init() {

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
}


void solve() {
    notpr[1]=true;
    cini(n);
    cini(m);
    vvi a(n, vi(m));
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            cini(aux);
            int aux2=aux;
            int ops=0;
            while(notpr[aux]) {
                ops++;
                aux++;
            }
            a[i][j]=ops;
        }

    int ans=1e9;
    for(int i=0; i<n; i++) {
        int sum=0;
        for(int j=0; j<m; j++) {
            sum+=a[i][j];
        }
        ans=min(ans, sum);
    }
    for(int j=0; j<m; j++) {
        int sum=0;
        for(int i=0; i<n; i++) {
            sum+=a[i][j];
        }
        ans=min(ans, sum);
    }

    cout<<ans<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    init();
        solve();
}

