/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We need to find the number of segments
 * with max k 1s and we need to find repetitions.
 *
 * First create pre[] with number of bad chars in 
 * prefix of len i.
 *
 * With two pointer find foreach position the max
 * position for good subarray.
 * So all subarrays starting at i up to j are good in first place.
 * Simply put them into a map?
 * We can start with the longest until first match.
 *
 * ...We should use a trie or something.
 */

int tnodecnt=0;

struct tnode {
    tnode *chld[26];
};

string s;

void insert(tnode *node, int first, int len) {
    if(len==0) {
        return;
    }
    if(node->chld[s[first]-'a']==0) {
        node->chld[s[first]-'a']=new tnode();
        tnodecnt++;
    }

    insert(node->chld[s[first]-'a'], first+1, len-1);
}

void solve() {
    cin>>s;
    const int n=(int)s.size();
    cins(bs);
    cini(k);

    tnode *root=new tnode();

    for(int i=0; i<n; i++) {
        int kk=0;
        int len=0;
        while(i+len<n)  {
            if(bs[s[i+len]-'a']!='1')
                kk++;
            if(kk>k)
                break;
            len++;
        }
        insert(root, i, len);
    }
    cout<<tnodecnt<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
