/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** 
 * Note that a[0]!=b[0]
 * So we must make a[0]<b[0]
 * We need to move some element from a[i] to a[0] 
 * and some from b[j] to b[0]
 * Find the pair whith smallest possible i+j
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);


    if(a[0]<b[0]) {
        cout<<0<<endl;
        return;
    }

    vi preA(n); /* min a[i] in prefix */
    preA[0]=a[0];
    vi preB(n); /* max b[i] in prefix */
    preB[0]=b[0];
    for(int i=1; i<n; i++)  {
        preB[i]=max(b[i], preB[i-1]);
        preA[i]=min(a[i], preA[i-1]);
    }

    int ans=INF;
    for(int i=0; i<n; i++) {
        /* use a[i] and b[j], find smallest possible j by binary search */
        int l=-1;
        int r=n-1;

        while(l+1<r) {
            int mid=(l+r)/2;
            if(preB[mid]<preA[i])
                l=mid;
            else
                r=mid;
        }
        ans=min(ans, i+r);
    }
    cout<<ans<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
