/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



/* see https://cp-algorithms.com/graph/finding-cycle.html
 * Note that this finds a cylce of size 2. To skip cylces of size 2
 * add a parent parameter to dfs(v,p).
 *
 * For directed graph modification see https://cses.fi/problemset/task/1678/
 * It adds a check so that in first place the dfs is started for 'root'
 * vertex only, that are vertex without an incoming edge.
 **/

vector<vector<int>> adj;
vector<char> color;
vector<int> parent;
int cycle_start, cycle_end;
vector<int> cycle;

bool dfs(int v) {
    color[v] = 1;
    for (int u : adj[v]) {
        if (color[u] == 0) {
            parent[u] = v;
            if (dfs(u))
                return true;
        } else if (color[u] == 1) {
            cycle_end = v;
            cycle_start = u;
            return true;
        }
    }
    color[v] = 2;
    return false;
}

/* set graph in adj[][], result cycle found in cycle[] */
void find_cycle(int n) {
    color.assign(n, 0);
    parent.assign(n, -1);
    cycle_start = -1;

    for (int v = 0; v < n; v++) {
        if (color[v] == 0 && dfs(v))
            break;
    }

    if (cycle_start != -1) {
        cycle.push_back(cycle_start);
        for (int v = cycle_end; v != cycle_start; v = parent[v])
            cycle.push_back(v);
        cycle.push_back(cycle_start);
        reverse(cycle.begin(), cycle.end());
    }
}

/**
 * First check if there is a loop in dependencies.
 * If yes then ans=-1;
 *
 * Then 
 * Foreach book, go dfs into all dependent books, and return
 * max depth.
 * If the dependent chapter is bigger than the current, add
 * 1 to its depth.
 *
 * ...
 * Seems like tree algos are not my best loved kind of problems... :/
 */
const int INF=1e9;
void solve() {
    cini(n);

    adj.clear();
    adj.resize(n);
    for(int i=0; i<n; i++) {
        cini(k);
        for(int j=0; j<k; j++) {
            cini(aij);
            aij--;
            assert(aij!=i);
            adj[i].push_back(aij);
        }
    }

    find_cycle(n);

    if(cycle_start!=-1) {
        cout<<-1<<endl;
        return;
    }

    vi dp(n, 1); /* dp[i]=number of readings to understand chapter i */
    vb vis3(n);
    function<void(int)> dfs2=[&](int v) {
        if(vis3[v])
            return;

        vis3[v]=true;

        for(int chl : adj[v]) {

            dfs2(chl);

            if(chl<v) {
                dp[v]=max(dp[v], dp[chl]);
            } else {
                dp[v]=max(dp[v], dp[chl]+1);
            }
        }
    };

    for(int i=0; i<n; i++) 
        dfs2(i);

    const int aans=*max_element(all(dp));
    cout<<aans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
