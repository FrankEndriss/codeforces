/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that the operation does not change
 * the XOR of the whole array.
 *
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    int x=0;
    for(int aa : a)
        x^=aa;

    if(x!=0) {
        cout<<"NO"<<endl;
        return;
    }

    /* Consider a[0] 
     * We must make a[1]^a[2] == a[0], so choosing i=0 will set
     * a[0]=0
     * Then repeat on a[1] etc
     */

    /* @return number of ops needed to make the element
     * at position idx eq val, and also do it.
     * Note that this should allways be possible.
     */
    vi vans;
    bool imp=false;
    function<void(int,int)> go=[&](int idx, int val) {
        if(a[idx]==val)
            return;

        //cerr<<"go idx="<<idx<<" val="<<val<<endl;
        if(idx+2>n-1) {
            imp=true;
            return;
        }

        if((a[idx]^a[idx+1]^a[idx+2])==val) {
            vans.push_back(idx+1);
            a[idx]=a[idx+1]=a[idx+2]=(a[idx]^a[idx+1]^a[idx+2]);
            return;
        }
        go(idx+2, a[idx]^val^a[idx+1]);
        vans.push_back(idx+1);
        a[idx]=a[idx+1]=a[idx+2]=(a[idx]^a[idx+1]^a[idx+2]);
    };

    for(int i=0; i+2<n; i++) {
        go(i,0);
        if(imp) {
            cout<<"NO"<<endl;
            return;
        }
    }
    cout<<"YES"<<endl;
    cout<<vans.size()<<endl;
    for(int ans : vans)
        cout<<ans<< " ";
    if(vans.size()>0)
        cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
