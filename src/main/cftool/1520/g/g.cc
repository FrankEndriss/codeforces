/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Two kinds of steps:
 * -to adj cell for cost w
 * -to portal cell for cost of portals sum
 *
 * There are up to 9M portals, so we cannot check
 * all portal to portal edges.
 * How can we find the ones which are actually faster
 * than the simple steps?
 *
 * Consider portals sorted by cost.
 * The portal cost (divided by w) implies on which
 * cells the jump might be usefull.
 *
 * Note that a jump from one portal to another is allways
 * cheaper than to jump to another portal in between.
 * So, all the grid is one component, or we want to jump
 * from first component to last component directly.
 *
 * Assume all one component:
 * There are some cells reacheable by portal jumps cheaper
 * than by normal steps. How to find them faster than
 * dijkstra?
 *
 * ***
 * Note that in all the path is at most one portal jump, since if there
 * would be more, then it would be cheaper to jump from first
 * portal cell to last directly.
 * So, how does this help?
 *
 * We can do simple dijkstra from D and S, and then find the cheapest
 * jump from any d[i][j] to s[i][j]
 *
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    cini(w);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) 
            cin>>a[i][j];

    vi di={ -1, 1, 0, 0};
    vi dj={ 0, 0, -1, 1};
    function<void(vvi&,int,int)> dijkstra=[&](vvi &dp, int i0, int j0) {
        queue<pii> q;
        q.emplace(i0,j0);
        dp[i0][j0]=0;
        while(q.size()) {
            auto [i,j]=q.front();
            q.pop();
            for(int k=0; k<4; k++) {
                const int ii=i+di[k];
                const int jj=j+dj[k];
                if(ii>=0 && ii<n && jj>=0 && jj<m && a[ii][jj]>=0 && dp[i][j]+w<dp[ii][jj]) {
                    dp[ii][jj]=dp[i][j]+w;
                    q.emplace(ii,jj);
                }
            }
        }
    };

    vvi d(n,vi(m, INF));
    dijkstra(d, 0, 0);
    vvi s(n,vi(m, INF));
    dijkstra(s, n-1, m-1);

    int ans=min(d[n-1][m-1], s[0][0]);  /* without jump */

    /* now find the cheapest jump of both parts, and connect them */

    int bestD=INF;
    int bestS=INF;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(a[i][j]>0) {
                bestD=min(bestD, d[i][j]+a[i][j]);
                bestS=min(bestS, s[i][j]+a[i][j]);
            }
        }
    }
    ans=min(ans, bestD+bestS);
    if(ans==INF)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
    solve();
}
