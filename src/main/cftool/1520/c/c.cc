/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* n=2
 * -> -1
 * n=3
 * 8, 3, 5
 * 4, 6, 2
 * 7, 1, 8
 * Swap on diagonal??
 * 1, 2, 3
 * 4, 5, 6
 * 7, 8, 9
 *
 * 1, 3, 5
 * 7, 9, 2
 * 4, 6, 8
 *
 * 1, 3, 5, 7
 * 9, 11, 13, 15
 * 2, 4, 6, 8
 * 10, 12, 14, 16
 * etc
 *
 */
void solve() {
    cini(n);
    if(n==2) {
        cout<<-1<<endl;
        return;
    }
    int r=0;
    int c=0;
    vvi ans(n, vi(n));
    for(int i=1; i<=n*n; i+=2) {
        ans[r][c]=i;
        c++;
        if(c==n) {
            r++;
            c=0;
        }
    }

    for(int i=2; i<=n*n; i+=2) {
        ans[r][c]=i;
        c++;
        if(c==n) {
            r++;
            c=0;
        }
    }

    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) 
            cout<<ans[i][j]<<" ";
        cout<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
