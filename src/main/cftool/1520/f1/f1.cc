/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int req(int l, int r) {
    cout<<"? "<<l<<" "<<r<<endl;
    int resp;
    cin>>resp;
    return resp;
}
/*
string test="000110011100";
int req(int l, int r) {
    int ans=0;
    for(int i=l; i<=r; i++) 
        if(test[i-1]=='1')
            ans++;

    return ans;
}
*/

/* Hard to understand...
 * What is t?
 *
 * We need to find the position of the kth '0'.
 *
 * Req l r 
 * -> number of 1s in range l,r
 */
void solve() {
    cini(n);
    cini(t);

    assert(t==1);
    cini(k);

    /* kth 0 is somewhere between ansL+1 and ansR */
    int ansL=0;
    int ansR=n;

    while(ansL+1<ansR) {
        int mid=(ansL+ansR)/2;
        int cnt=mid-req(1, mid);
        if(cnt<k)
            ansL=mid;
        else
            ansR=mid;

    }
    cout<<"! "<<ansR<<endl;
}

signed main() {
    solve();
}
