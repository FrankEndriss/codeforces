/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Casework
 * n=1 -> -1
 * n==2 && (a[1]-a[0])%2==0
 * n>1 && a[] is arithmetic -> 2: both ends
 * n>1 && a[] is atithmetic except one position: excase
 * else -> 0
 */
void solve() {
    cini(n);
    cinai(a,n);

    if(n==1) {
        cout<<-1<<endl;
        return;
    }

    sort(all(a));
    if(n==2 && (a[1]-a[0])%2==0 && a[0]!=a[1]) {
        cout<<3<<endl;
        cout<<a[0]-(a[1]-a[0])<<" "<<a[0]+(a[1]-a[0])/2<<" "<<a[1]+(a[1]-a[0])<<endl;
        return;
    }

    map<int,int> d;
    for(int i=0; i+1<n; i++)
        d[a[i+1]-a[i]]++;

    if(d.size()>2) {
        cout<<0<<endl;
        return;
    }

    if(d.size()==1) {
        int dd=a[1]-a[0];
        if(dd>0) {
            cout<<2<<endl;
            cout<<*min_element(all(a))-dd<<" "<<*max_element(all(a))+dd<<endl;
        } else {
            cout<<1<<endl;
            cout<<a[0]<<endl;
        }
        return;
    }

    int d1=d.begin()->first;
    int d2=d.rbegin()->first;
    if(d1*2==d2 && d.rbegin()->second==1) {
        sort(all(a));
        for(int i=0; i+1<n; i++) {
            if(a[i+1]-a[i]==d2) {
                cout<<1<<endl;
                cout<<a[i]+d1<<endl;
            }
        }
    } else 
        cout<<0<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
