/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It seems to be about min/max values.
 * So we find a range of values foreach b[i], then assign
 * in order of c[] from low to high.
 *
 * l-a[i] <= b[i] <= r-a[i]
 * also, if we need to choose the values so that each c[i] 
 * is distinct.
 *
 * So start with smallest c[i], and assign smallest possible b[i].
 * Then iterate c[] ascending, each assigned b[i] must be so 
 * that b[i] is in range, and c[pos[i+1]]>c[pos[i]]
 */
void solve() {
    cini(n);
    cini(l);
    cini(r);
    assert(l<=r);
    cinai(a,n);
    cinai(p,n);

    vi pos(n);
    for(int i=0; i<n; i++) 
        pos[p[i]-1]=i;

    vi cc(n);   /* cc[i]=b[i]-a[i] */
    vi b(n);

    cc[pos[0]]=l-a[pos[0]];
    b[pos[0]]=l;

    for(int i=1; i<n; i++) {
        cc[pos[i]]=l-a[pos[i]];
        cc[pos[i]]+=max(0LL, cc[pos[i-1]]-cc[pos[i]]+1);
        b[pos[i]]=a[pos[i]]+cc[pos[i]];
        if(b[i]>r) {
            cout<<-1<<endl;
            return;
        }
    }


    for(int i=0; i<n; i++)
        cout<<b[i]<<" ";
    cout<<endl;
    
}

signed main() {
        solve();
}
