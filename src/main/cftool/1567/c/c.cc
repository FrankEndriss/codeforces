/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 *
 * Note that the odd and even positions do not interact,
 * they are independent.
 * So make n1 and n2, build by those digits.
 *
 * Consider digits from lowest to highest.
 *
 * At each position there are 10*10 possible 
 * pairs of digits a and b.
 * Each such pair that results in digit of n 
 * constructs a carry of 0 or 1 at pos+2.
 * Count them.
 */
const int N=20;
void solve() {
    cini(n);

    vvi nn(2);;
    while(n) {
        nn[0].push_back(n%10);
        n/=10;
        nn[1].push_back(n%10);
        n/=10;
    }

    vi nnn(2);
    while(nn[0].size()) {
        nnn[0]*=10;
        nnn[0]+=nn[0].back();
        nn[0].pop_back();
    }
    while(nn[1].size()) {
        nnn[1]*=10;
        nnn[1]+=nn[1].back();
        nn[1].pop_back();
    }


    vi ans(2);

    for(int k=0; k<2; k++) {
        vvi dp(N, vi(2));
        for(int a=0; a<10; a++) 
            for(int b=0; b<10; b++) {
                if(a+b==nnn[k]%10)
                    dp[0][0]++;
                else if(a+b-10==nnn[k]%10)
                    dp[0][1]++;
            }

        for(int pos=1; pos<N; pos++) {
            nnn[k]/=10;
            for(int a=0; a<10; a++) 
                for(int b=0; b<10; b++) {
                    if(a+b==nnn[k]%10)
                        dp[pos][0]+=dp[pos-1][0];
                    else if(a+b+1==nnn[k]%10)
                        dp[pos][0]+=dp[pos-1][1];
                    else if(a+b-10==nnn[k]%10)
                        dp[pos][1]+=dp[pos-1][0];
                    else if(a+b+1-10==nnn[k]%10)
                        dp[pos][1]+=dp[pos-1][1];
                }

        }
        ans[k]=dp[N-1][0];
    }
    cout<<ans[0]*ans[1]-2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
