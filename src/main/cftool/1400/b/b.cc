/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* For both person we need
 * to find max possible weight
 * by trying all possible numbers of
 * swords, and calc the axes.
 *
 * How to combine both?
 * It is allways optimal to carry as much as possible.
 * But we cannot check all possibilities from second person.
 *
 * Greedy:
 * It is allways optimal to carry as much as possible of
 * the lighter thing.
 */
void solve() {
    cini(capaP);
    cini(capaF);
    cini(cntS);
    cini(cntA);
    cini(wS);
    cini(wA);

    if(wS>wA) {
        swap(wS, wA);
        swap(cntS, cntA);
    }
    /* assume wS is smaller */
    
    int ans=0;
    for(int i=0; i<=cntS; i++) {
        int pS=min(i, capaP/wS);
        int pA=min(max(0LL, capaP-pS*wS)/wA, cntA);
        int ansP=pS+pA;

        int lcntS=cntS-pS;
        int lcntA=cntA-pA;

        int fS=min(lcntS, capaF/wS);
        int fA=min(max(0LL, capaF-fS*wS)/wA, lcntA);
        int ansF=fS+fA;
        ans=max(ans, ansP+ansF);
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
