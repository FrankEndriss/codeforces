/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Assume whole array consists of same number.
 * How to not TLE?
 *
 * let dp[i]=vi of bigger indexes of a[i]
 *
 * Somehow we need to sum up the overlapping segments...
 *
 * What is ans if a[] has n same numbers?
 * It is n*n-1 pairs of i,k
 * And for pair 1,3
 * 2,4; 2,5; ... n-3
 * for pair 1,4
 * 2,5; 2,6;... n-4
 * 3,5; 3,6;....n-4
 *
 * So mayby brute force works.
 * **********
 * We need to find number of two same pairs, because the formular 
 * translates to
 * a[i],a[j]==a[k],a[l]
 * So iterate over all numbers collecting and matching pairs.
 */
void solve() {
    cini(n);
    cinai(a,n);


    vvi f(n+1, vi(n+1));

    int ans=0;
    for(int j=0; j<n; j++) {
        for(int k=j+1; k<n; k++)
            ans+=f[a[j]][a[k]];

        for(int i=0; i<j; i++)
            f[a[i]][a[j]]++;
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
