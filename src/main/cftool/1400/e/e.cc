/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We can allways remove all in n steps with second operation.
 * How can we be faster by using first operation?
 *
 * With first operation it is always better to use with biggest
 * segment possible.
 *
 * So, we need to find min operations on initial segments, which 
 * are bounded by zero positions.
 */
void solve() {
    cini(n);
    cinai(f,n);

    vector<pii> segs;

    function<int(int,int,int)> cnt=[&](int beg, int end, int rem) {
        int cnt1=0; /* only first operation */
        for(int i=beg; i<end; i++) 
            if(f[i]>rem)
                cnt1++;

        int mi=*min_element(f.begin()+beg, f.begin()+end);
        assert(mi>rem);

        int cnt2=mi-rem;
        int prev=-1;
        for(int i=0; cnt2<cnt1 && i<n; i++) {
            if(f[i]==mi) {
                if(prev>=0) {
                    cnt2+=cnt(prev, i, mi+rem);
                    prev=-1;
                }
            } else if(prev<0)
                prev=i;
        }
        return min(cnt1,cnt2);
    };

    int prev=-1;
    for(int i=0; i<n; i++) {
        if(f[i]==0) {
            if(prev>=0) {
                segs.push_back({prev,i});
                prev=-1;
            }
        } else if(prev<0)
            prev=i;
    }
    if(prev>=0) 
        segs.push_back({prev, n});

    int ans=0;
    for(size_t i=0; i<segs.size(); i++) {
        ans+=cnt(segs[i].first, segs[i].second, 0LL);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
