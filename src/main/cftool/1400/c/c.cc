/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Set all 0s, all others must be 1s.
 * Then check if s fits to ans.
 */
void solve() {
    cins(s);
    cini(x);

    const int n=s.size();

    string ans(n, '1');
    for(int i=0; i<n; i++) {
        if(s[i]=='0') {
            if(i-x>=0)
                ans[i-x]='0';
            if(i+x<n)
                ans[i+x]='0';
        }
    }


    //cerr<<"x="<<x<<endl;
    bool ok=true;
    for(int i=0; ok && i<n; i++) {
        bool l1=i-x>=0 && ans[i-x]=='1';
        bool r1=i+x<n && ans[i+x]=='1';

        //cerr<<"s[i]="<<s[i]<<" l1="<<l1<<" r1="<<r1<<endl;

        if((l1 || r1) && s[i]=='0')
            ok=false;
        if(!(l1 || r1) && s[i]=='1')
            ok=false;
    }
    if(!ok)
        cout<<-1<<endl;
    else
        cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
