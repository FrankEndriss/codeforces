/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int nom, const int denom) {
    return mul(nom, inv(denom));
}

/* super complecated problem construction...
 * try to find what is what.
 *
 * We want to find the sum of different merging
 * sequences of all possible pairs of two substrings,
 * one from x, one from y.
 *
 * So, how to find the number of merging seqs for one
 * pair of two strings?
 * -> We take first letter from one of the strings, then
 *  the following letters may be choosen from any string
 *  if current position combined with last taken sym allows this.
 * Then its a dp[i][j][b]=number of possible seqs if
 * taken i syms from x and j syms from y and last of x/y was b.
 *
 * And with such table we also get all numbers for all substrings
 * of x and y starting at 0 in O(n^2).
 *
 * How to get the numbers for all substrings?
 * Simply divide the numbers by the prev numbers...somehow?
 *
 * ...
 * The values for substrings starting at x[1] and y[0] are
 * ans0/dp[b][1][0] ... or something similar :/
 */
void solve() {
    cins(x);
    cins(y);

    vvvi dp(2, vvi(x.size()+1, vi(y.size()+1)));

    dp[0][1][0]=dp[1][0][1]=1;
    for(size_t i=1; i<=x.size(); i++) {
        for(size_t j=1; j<=y.size(); j++) {
            /* take from x at pos i-1 */
            if(i>1 && x[i-1]!=x[i-2])
                dp[0][i][j]+=dp[0][i-1][j];
            if(x[i-1]!=y[j-1])
                dp[0][i][j]+=dp[1][i-1][j];

            /* take from y at pos j-1 */
            if(j>1 && y[j-1]!=y[j-2])
                dp[1][i][j]+=dp[1][i][j-1];
            if(y[j-1]!=x[i-1])
                dp[1][i][j]+=dp[0][i][j-1];

            dp[0][i][j]%=MOD;
            dp[1][i][j]%=MOD;

        }
    }

    int sum=0;
    for(size_t i=1; i<=x.size(); i++) {
        for(size_t j=1; j<=y.size(); j++) {
            sum+=dp[0][i][j];
            sum+=dp[1][i][j];
        }
    }

    int ans=0;
    int num=0;
    for(size_t i=1; i<x.size(); i++) {
        for(size_t j=1; j<y.size(); j++) {
            int ssum=sum;
            if(num>0)
                ssum=mul(ssum, inv(num));

            ans=pl(ans,ssum);

            num=pl(num, pl(dp[0][i][j], dp[1][i][j]));
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
