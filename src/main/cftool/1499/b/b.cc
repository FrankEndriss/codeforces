/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* ...so, by complecated statement rules
 * it is not allowed to remove the zeros in "11001",
 * because they are adjacent. Also we cannto remove
 * the leading "11".
 * Ans=YES if there is no double 0 after the first double 1.
 */
const int INF=1e9;
void solve() {
    cins(s);

    int f1=INF;
    int f0=-1;
    char prev='x';
    for(int i=0; i<(int)s.size(); i++) {
        if(s[i]==prev) {
            if(s[i]=='1')
                f1=min(f1, i);
            else if(s[i]=='0')
                f0=max(f0, i);
            else
                assert(false);
        }
        prev=s[i];
    }

    if(f1<f0)
        cout<<"NO"<<endl;
    else
        cout<<"YES"<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
