/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Pairs a,b
 * lcm(a,b)>=gcd(a,b) && <=a*b
 * gcd(a,b)<=min(a,b)
 * also, gcd(a,b) = y* lcm(a,b)
 *
 * let g=gcd(a,b)
 *
 * Consider 
 * a==b==g
 *
 * x0=c*g-d*g = (c-d)*g
 * So, if x=c-d then lcm(a,b)==gcd(a,b)==1, 
 * which is true for the pair a==1 && b==1
 *
 * So, x is a multiple of gcd(a,b), so let
 * d=divisors of x
 */
void solve() {
    cini(c);
    cini(d);
    cini(x);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
