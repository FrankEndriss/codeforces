/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* well, we want to use the cheapest possible segment
 * as often as possible.
 *
 * ...
 * seems to be a dp.
 * let dp[i] be the price if starting at i,i.
 * Since we most likely want to go allways same
 * amount of vert/horz, we can simply put it into 
 * one dimension (and double the result).
 *
 * But that O(N^2) :/
 * ...
 * No, the cost is not the cost of the length of the 
 * segments, but the cost of the order, the ith step costs c[i]*2
 *
 * So we need to calc foreach direction separate.
 *
 * That is, foreach number of steps in one direction, the costs
 * are the prefix sum+the cheapest of the segs times the 
 * remaining distance.
 *
 * Then we can combine all pairs of number of steps difering not
 * more that by one.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(c,n);

    vi dpH; /* dpH[i]==min price to go i+1 steps until n */
    int mi=INF;
    int pre=0;

    for(int i=0; i<n; i+=2) {
        mi=min(mi, c[i]);
        pre+=c[i];
        dpH.push_back((pre-mi)+mi*(n-i/2));
    }

    vi dpV; /* dpH[i]==min price to go i+1 steps until n */
    mi=INF;
    pre=0;

    for(int i=1; i<n; i+=2) {
        mi=min(mi, c[i]);
        pre+=c[i];
        dpV.push_back((pre-mi)+mi*(n-(i/2)));
    }

    int ans=dpH[0]+dpV[0];
    for(int i=1; i<(int)dpH.size(); i++) {
        ans=min(ans, dpH[i]+dpV[i-1]);
        if(i<(int)dpV.size())
            ans=min(ans, dpH[i]+dpV[i]);
    }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
