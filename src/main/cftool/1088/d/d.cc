/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

bool swapped=false;
int A=2123;
int B=323;
q(int c, int d) {
    if(swapped)
        swap(c,d);

    int l=A^c;
    int r=B^d;
    if(l>r)
        return 1;
    else if(l<r)
        return -1;
    else 
        return 0;
}

/* first query with 0,0 which one is bigger.
 *
 * query bits from left to right by setting
 * this one bit in c and in d.
 * if(res==-1) bit is set in a but not in b
 * if(res== 1) bit is set in both or none
 *   query c=that bit and d=0 to find if it
 *   was set.
 * always query with bitmask according to 
 * previously found bits.
 */
void solve() {
cerr<<"   A="<<bitset<16>(A)<<endl;
cerr<<"   B="<<bitset<16>(B)<<endl;
    int res=q(0,0);
    if(res==0) {
        int ans=0;
        for(int i=0; i<30; i++) {
            if(q(1LL<<i, 0)!=0)
                ans|=(1LL<<i);

            cout<<"! "<<ans<<" "<<ans<<endl;
            return;
        }
    }

    if(res<0)
        swapped=true;

    int ansa=0;
    int ansb=0;
/* assume a>b from now on */
/* Find highest bit set in a but not in b */
    for(int i=29; i>=0; i--)  {
        const int bit=1LL<<i;
        res=q(ansa|bit, ansb|bit);
        if(res<0) {
            ansa|=bit;
cerr<<"ansa="<<bitset<16>(ansa)<<endl;
cerr<<"ansb="<<bitset<16>(ansb)<<endl;
        } else if(res>0) {
            res=q(ansa|bit, ansb);
            if(res<0) {
                ansa|=bit;
                ansb|=bit;
cerr<<"ansa="<<bitset<16>(ansa)<<endl;
cerr<<"ansb="<<bitset<16>(ansb)<<endl;
            }
        } else
            assert(false);
    }

    if(swapped)
        swap(ansa, ansb);

    cout<<ansa<<" "<<ansb<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
