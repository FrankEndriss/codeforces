/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* adjust as needed */
using S=struct {
    int unfixed, fixed, cnt0, cnt1;
}

S st_op(S a, S b) {
    return { 
        a.unfixed+b.unfixed,
        a.fixed+b.fixed,
        a.cnt0+b.cnt0,
        a.cnt1+b.cnt1
    };
}

S st_e() {
    return { 0, 0, 0, 0 };
}


/**
 * We need to find fixed positions.
 *
 * We can fix positions with 0 for every query t=0 and x=0
 * Else we can fix positions with 1, for every queery t=0 ans x=1,
 * if [l,r]==|1| or the number of fixed 0 positions is |[l,r]|-1
 *
 * So we maintain state in a segment tree.
 * cnt unfixed
 * cnt fixed
 * cnt 0
 * cnt 1
 *
 * ***
 * But  note that we also need to maintain subtractions of sets.
 * That is, if [l1,r1] to be known to contain a 1, and 
 * we get other requests so that there is only 1 position left in [l1,r1]
 * that is unknown, then we know it is a 1.
 * How to implement that?
 */
void solve() {
    cini(n);
    cini(q);

    vector<S> data(n);
    for(int i=0; i<n; i++)
        data[i].unfixed=1;

    set<int> unf;
    for(int i=0; i<n; i++) 
        unf.insert(i);

    stree seg(data);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(idx);
            S s=seg.get(idx-1);
            if(s.fixed==1) {
                if(s.cnt1==1) 
                    cout<<"YES"<<endl;
                else if(s.cnt0==1)
                    cout<<"NO"<<endl;
                else
                    assert(false);
            } else {
                cout<<"N/A"<<endl;
            }
        } else if(t==0) {
            cini(l); l--;
            cini(r); r--;
            cini(x);

            if(x==0) {
                auto it=unf.lower_bound(l);
                while(it!=unf.end() && *it<=r) {
                    S s={ 0, 1, 1, 0 };
                    seg.set(*it, s);
                    it=unf.erase(it);
                }
            } else {
                S s=seg.prod(l,r+1);
                if(s.unfixed==1) {
                    auto it=unf.lower_bound(l);
                    // TODO...
                }
            }
        } else 
            assert(false);
    }
}

signed main() {
    solve();
}
