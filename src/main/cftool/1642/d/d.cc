/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Maybe...
 * We need to build the resulting array
 * from outside to inside.
 *
 * Can we somehow estimate a minimum number of operations?
 * Or how to know in how many parts we want to split the result?
 *
 * It makes never sense to create a part only by added symbols, 
 * so each part contains at least two original numbers (since size is even).
 * So there are at most n/2 parts.
 * Note that we do not have to minimize operations.
 *
 * How can we create a tandem from first two symbols.
 * Assume these are different.
 *
 * Consider 
 * 1 2
 * We actually cannot make it great, since we can never create on sequence
 * of odd number of equal symbols.
 * But we can replace it by some other seq:
 * 1 2 1 1
 * 1 2 1 2 2 1
 *         2 1
 * 
 * 1 2 1
 * 1 2 1 2 2
 *         2
 * 
 * 1 2 3
 * 1 2 3 1 1 
 * 1 2 3 1 2 2 1 
 * 1 2 3 1 2 3 3 2 1
 *             3 2 1
 *
 * So we can reverse any substring.
 * and replace [�x  x] by [b]
 *
 * The number of all symbols must be even, then 
 * we can build pairs in the beginning of the list,
 * and remove those pairs.
 * So write a method to reverse an arbitrary substring.
 *
 * How to implement this?
 * Go from left to right, maintain an offset of done prefix,
 * and the original postfix.
 */
void solve() {
    cini(n);
    cinai(a,n);

    map<int,int> cnt;
    for(int i=0; i<n; i++) 
        cnt[a[i]]++;
    for(auto ent : cnt) {
        if(ent.second%2!=0) {
            cout<<-1<<endl;
            return;
        }
    }

    vi anst;
    vector<pii> ans;
    int offs=0;
    /* outputs operations to reverse substring (l,r] */
    function<void(int,int)> rev=[&](int l, int r) {
        for(int i=l; i<r; i++) {
            ans.emplace_back(offs+r+2*i, a[l]);
        }
        offs+=(r-l)*2;
    };

    for(int i=0; i+1<n; i++) {
        if(a[i]==a[i+1]) {
            i++;
            continue;
        }

        bool done=false;
        for(int j=i+1; j<n; j++) {
            if(a[i]==a[j]) {
                rev(i+1, j+1);
                offs+=2;
                reverse(a.begin()+i+1, a.begin()+j+1);
                i++;
                done=true;
                break;
            }
        }
        assert(done);
    }
    cout<<ans.size()<<endl;
    for(auto p : ans) 
        cout<<p.first<<" "<<p.second<<endl;
    int c=n/2+ans.size();
    cout<<c<<endl;
    for(int i=0; i<c; i++) 
        cout<<2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
