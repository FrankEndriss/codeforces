/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Can change empty set into any set with sum<=d,
 * so we can get any item worth-d<=sum of all prev items.
 * let that item be maxItem.
 *
 * And we need to find the min days to put the items together.
 * How does this work? 
 * Since we need to get all items up to maxItem, we need to 
 * create sum worth of all item up to maxItem.
 *
 * We want to make most profitable deals, to minimize
 * number of deals.
 * Problem is, with n=50 items we have 2^50 ways to memoize.
 *
 * ->knapsack, greedy
 * We simulate the exchange process. 
 * Once we find all possible sums of all items.
 * Then, per step we find all possible sums of current
 * set plus virtual element d.
 * Then we exchange the max possible.
 */
void solve() {
// todo
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
