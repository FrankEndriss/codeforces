/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Make first array sorted permutation.
 * then we need to find two elements we can
 * make equal.
 * Such two elements must not be compared directly
 * because that would give different results in both arrays.
 *
 * Such two elements should be next to each other (value diff==1)
 * because then all other comparations still work.
 *
 * So, find first positions not compared to first position.
 * Then put the value of that position minus one to first position,
 * and the 1 to position of the -1.
 * Then adjust the value of first position +1.
 */
void solve() {
    cini(n);
    cini(m);

    vi a(n);
    vi b(n);
    for(int i=0; i<n; i++) {
        a[i]=i;
        b[i]=i;
    }

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        adj[min(u,v)].push_back(max(u,v));
    }
    const int INF=1e9;
    for(int i=0; i<n; i++)
        adj[i].push_back(INF);

    bool done=false;
    for(int i=0; !done && i<n; i++) {
        sort(all(adj[i]));
        int val=i+1;
        for(int j=0; !done && j<adj[i].size(); j++) {
            if(val>=n || val==adj[i][j]) {
                val=adj[i][j]+1;
            } else  {
//cerr<<"found uncompared pair, i="<<i<<" val="<<val<<endl;
                /* found uncompared pair, i and val
                 * now swap the smaller one so that its
                 * value equals bigger one -1.
                 **/
                int mi=min(val, i);
                int ma=max(val, i);
                if(mi+1==ma) {
                    b[mi]++;
                } else {
                    swap(a[mi], a[ma-1]);
                    swap(b[mi], b[ma-1]);
                    b[mi]++;
                }
                done=true;
            }
        }
    }

    if(!done)
        cout<<"NO"<<endl;
    else {
        cout<<"YES"<<endl;
        for(int i : a)
            cout<<i+1<<" ";
        cout<<endl;
        for(int i : b)
            cout<<i+1<<" ";
        cout<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
