/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Brute force would be...
 *
 * Find par for leftmost substring.
 * Count the changes in b from i+1 to i.
 * Then move one to right, with
 * - consider num changes
 * - first symbol next to rightmost
 * - last symbol
 * Repeat.
 */
void solve() {
    cins(a);
    cins(b);

    int chg=0;
    for(size_t i=1; i<b.size(); i++) 
        if(b[i-1]!=b[i])
            chg=!chg;

    int par=0;
    for(size_t i=0; i<b.size(); i++) {
        if(b[i]!=a[i])
            par=!par;
    }

    int ans=!par;
    for(size_t i=b.size(); i<a.size(); i++) {
        if(a[i]!=b.back())
            par=!par;
        if(a[i-b.size()]!=b[0])
            par=!par;

        if(chg)
            par=!par;

        ans+=!par;
    }
    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
