/*
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(k);
    map<int, set<pii>> evt;  // <pos, <idx, +-1>>

    vi l(n);
    vi r(n);

    for(int i=0; i<n; i++) {
        cin>>l[i]>>r[i];
        evt[l[i]].insert({i, 1});
        evt[r[i]+1].insert({i, -1});
    }

    int ans=0;
    vi ansv;
    set<pii> seg;   // <-r[i], i> current segments

    for(auto &ent : evt) {
        for(pii evt : ent.second) {
            if(evt.second==1)
                seg.insert({-r[evt.first], evt.first});
            else
                seg.erase({-r[evt.first], evt.first});
        }
        while(seg.size()>k) {
            auto it=seg.begin();
            evt[-it->first].erase({it->second, -1});
            ansv.push_back(it->second);
            seg.erase(it);
            ans++;
        }
    }
    cout<<ans<<endl;
    for(int i:ansv)
        cout<<i+1<<" ";
    cout<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

