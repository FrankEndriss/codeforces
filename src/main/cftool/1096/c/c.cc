/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Sum of angels: (n-2)*180
 * Angel at v: (180*(n-2))/n
 * let av angel at v.
 * So, ang of vertex 1,2,3: 
 * (180-av)/2 at 1 and 3, and av at 2
 * ang of vertex 1,2,4:
 * at 2: av-(180-av)/2
 * ...
 *
 * Since there are only 179 different inputs, there
 * are only 179 different outputs.
 *
 * We could brute force the number of vertex, and for each one
 * find all possible integer angels, until all 179 are found.
 *
 * How to find the integer angels?
 *
 * -> Tutorial does not really help :/
 */
void solve() {
    cini(a);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
