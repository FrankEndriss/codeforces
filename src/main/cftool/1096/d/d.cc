/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we may ignore all chars other than "hard"
 * What can we remove:
 * -all a after first h
 *  --remove some h from begin and all a after the first remaining h
 * -all r after first a
 *  --remove some a from begin and all r after the first a
 * -all d after first r
 *  --remove some r from begin and all d after the first a
 *...
 * make a dp[i][j]=
 * min cost of having a prefix of s of len i with no occurence of prefix of "hard" of len j 
 * so dp[n][4]==ans
 */
const int MOD=998244353;
const int INF=1e18;
void solve() {
    cini(n);
    cins(s);
    cinai(a,n);

    const string hard="hard";
    vvi dp(n+1, vi(5, INF));

    for(int i=1; i<5; i++) {
        dp[0][i]=0;
        for(int j=0; j<n; j++) {
            if(s[j]==hard[i-1]) {
                dp[j+1][i]=min(dp[j][i]+a[j], dp[j][i-1]);
            } else {
                dp[j+1][i]=min(dp[j][i], dp[j][i-1]);
            }
        }
    }

    cout<<dp[n][4]<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
