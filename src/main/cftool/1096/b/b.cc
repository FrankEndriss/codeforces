/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll

#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MOD=998244353;

/* cases, string:
 * aaXX: freq(a)+1
 * XXbb: freq(b)+1
 * aaXXaa:freq(aL)+1 + freq(aR)+1 + (freq(aL)-1)*(freq(aR)-1)
 * aaaa: all substring, ie n*(n+1)/2
 */
void solve() {
    cini(n);
    cins(s);

    int f1=true;
    for(int i=1; f1 && i<n; i++)
        if(s[i]!=s[i-1])
            f1=false;

    if(f1) {
        int ans=n*(n+1)/2;
        ans%=MOD;
        cout<<ans<<endl;
        return;
    }

    int fL=1;
    for(int i=1; i<n; i++)  {
        if(s[i]==s[i-1])
            fL++;
        else
            break;
    }
    int fR=1;
    for(int i=n-2; i>=0; i--)  {
        if(s[i]==s[i+1])
            fR++;
        else
            break;
    }

    if(s[0]==s[n-1]) {
        int ans=fL+fR+1 + fL*fR;
        ans%=MOD;
        cout<<ans<<endl;
    } else {
        int ans=fL+fR+1;
        ans%=MOD;
        cout<<ans<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

