/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
//#include <atcoder/dsu>
//using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Find number of connected components.
 *
 * Sorry, that statement is wrong, missleading.
 * We want to search for duplicate dominoes,
 * nothing more.
 *
 * And still, is dominoe {a,b} other than {b,a} or same?
 *
 * ...
 * We want to split into two groups, so that no number
 * is twice in any group, that means if a dominoe x,y
 * is in one group, no other domino with either x or y
 * can be in the same group.
 *
 * So iterate unsorted dominoes.
 * Put it into first group.
 * Put it onto q.
 * while(q)
 *     Put all connected into other group, check them, put on queue.
 */
void solve() {
    cini(n);

    vi a(n);
    vi b(n);
    vvi idx(n);
    bool ok=true;
    for(int i=0; i<n; i++) {
        cin>>a[i]>>b[i];
        a[i]--;
        b[i]--;
        if(a[i]!=b[i]) {
            idx[a[i]].push_back(i);
            idx[b[i]].push_back(i);
        } else {
            ok=false;
        }
    }
    if(!ok) {
        cout<<"NO"<<endl;
        return;
    }

    vi g(n,-1);
    queue<int> q;

    for(int i=0; i<n; i++) {
        if(g[i]>=0)
            continue;

        q.emplace(i);
        g[i]=0;
        while(q.size()) {
            int v=q.front();
            q.pop();
            assert(g[v]>=0);

            for(int chl : idx[a[v]]) {
                if(chl!=v && g[v]==g[chl]) {
                    cout<<"NO"<<endl;
                    return;
                } else if(g[chl]==-1) {
                    g[chl]=1-g[v];
                    q.push(chl);
                }
            }
            for(int chl : idx[b[v]]) {
                if(chl!=v && g[v]==g[chl]) {
                    cout<<"NO"<<endl;
                    return;
                } else if(g[chl]==-1) {
                    g[chl]=1-g[v];
                    q.push(chl);
                }
            }
        }
    }

    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
