/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that the multiplication is irreversible,
 * the division is not.
 *
 * So, adjust a so that all numbers are bgeq than the 
 * biggest in b.
 * Then resolve each b[i]
 *
 * ...
 * Adjust all a[i] to a[i]/2 while even, so that 
 * all a[i] are odd.
 *
 * Then Its greedy, match all b[i] allways with the biggest
 * possible a[i]
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    multiset<int> msa;
    multiset<int> msb;
    for(int i=0; i<n; i++) {
        while(a[i]%2==0)
            a[i]/=2;

        msa.insert(a[i]);
        msb.insert(b[i]);
    }

    for(int v : msb) {
        bool done=false;
        do {
            auto it=msa.find(v);
            if(it!=msa.end()) {
                msa.erase(it);
                done=true;
                break;
            }
            v/=2;
        }while(v);

        if(!done) {
            cout<<"NO"<<endl;
            return;
        }
    }

    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
