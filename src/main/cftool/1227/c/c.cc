/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* the reverse thing is simply like swapping.
 * if we want to change a symbol we search
 * linear next occur of other  symbol, then
 * reverse.
 * So, how to create k prefixes?
 * make
 * ()()()...((((()))))
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);
    log(n,k,s);

    vector<pii> ans;

    function <void(int,char)> mk=[&](int idx, char c) {
        int i2=idx;
        while(s[i2]!=c)
            i2++;

        if(idx!=i2)  {
            reverse(s.begin()+idx, s.begin()+i2+1);
            ans.emplace_back(idx, i2);
            log(idx, i2, s);
        }
    };

    int idx=0;
    for(int i=1; i<k; i++) {
        mk(idx++,'(');
        mk(idx++,')');
    }

    int cnt=(s.size()-idx)/2;
    log(idx, cnt, s);
    for(char sym : {
                '(', ')'
            }) {
        for(int i=0; i<cnt; i++) {
            mk(idx++, sym);
        }
    }
    cout<<ans.size()<<endl;
    for(pii p:ans) {
        cout<<p.first+1<<" "<<p.second+1<<endl;
    }
}


int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}


