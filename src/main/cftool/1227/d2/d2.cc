/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

/* 
 * ***************
 * Note: a[] is not distinct, ie we need to put elements more than once into K.
 * Obviously in order of the positions.
 * ...And that is all we need to do !!!
 * Process queries offline in order of k.
 */
void solve() {
    cini(n);
    cinai(a,n);

    map<int,vi> apos;
    for(int i=0; i<n; i++)
        apos[a[i]].push_back(i);

    vi poslist;
    for(auto it=apos.rbegin(); it!=apos.rend(); it++)
        for(int idx : it->second)
            poslist.push_back(idx);

    assert(poslist.size()==n);

    cini(m);
    vector<pair<pii,int>> q(m); /* q[i]=<<sizeof(K), pos>, idxQ> */
    for(int i=0; i<m; i++) {
        cin>>q[i].first.first>>q[i].first.second;
        q[i].second=i;
    }
    sort(all(q));

    indexed_set K;
    int pidx=0;

    for(int i=0; i<m; i++) {
assert(q[i].first.first>=q[i].first.second);

//cerr<<"query i="<<i<<" k="<<q[i].first.first<<" pos="<<q[i].first.second<<endl;
        while(K.size()<(size_t)q[i].first.first)
            K.insert(poslist[pidx++]);

assert(K.size()>=(size_t)q[i].first.first);

assert(q[i].first.second-1<(int)K.size());
        q[i].first.first=*K.find_by_order(q[i].first.second-1);

//cerr<<"ans="<<q[i].first.first<<endl;
    }

    vector<pii> ans(m);
    for(int i=0; i<m; i++)  {
        ans[i].first=q[i].second;
        ans[i].second=q[i].first.first;
    }
    sort(all(ans));
    for(int i=0; i<m; i++)
        cout<<a[ans[i].second]<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

