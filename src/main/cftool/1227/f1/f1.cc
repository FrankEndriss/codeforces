/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

constexpr int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    v1%=mod;
    v2%=mod;

    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

constexpr int N=1e5+7;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

/* We want to find the number of possiblilities where the shift by one position
 * yields more matches than the not shifted one.
 *
 * We got positions where the shifted ans equals the orig one. These position can
 * be ignored, since the do not contribute anyhow.
 * Lets call the other fields contribution positions, cpos.
 *
 * Stictly more matches are yielded if from all cpos the shifted matches are
 * more than the orig ones.
 *
 * So in each configuration there are 3 kinds of cpos:
 * match orig
 * match shifted
 * no match at all
 * foreach possible number of orig matching cpos (num cpos / 2 since if more the shifted matches 
 * can never be more)
 * We need to find the number of possible configurations of all other fields so that
 * there are at least that number+1 cpos with shift match.
 *
 * ... need to consult tutorial
 */
void solve() {
    cini(n);
    cini(k);
    cinai(h,n);
    //cerr<<"n="<<n<<" k="<<k<<endl;

    int cntcpos=0;
    for(int i=0; i<n; i++) 
        if(h[i]!=h[(i+1)%n])
            cntcpos++;

    int ans=0;

    for(int omatch=0; omatch<=cntcpos/2; omatch++) {
        /* find number of configs with more smatch than omatch */

        for(int i=1; omatch*2+i<=cntcpos; i++) {
            /* number of ways to choose the omatch fields */
            int lans=nCr(cntcpos, omatch);
            /* number of ways to choose the smatch fields */
            lans=mul(lans, nCr(cntcpos-omatch, omatch+i));
            //cerr<<"omatch="<<omatch<<" i="<<i<<" lans="<<lans<<endl;

            /* number of ways to set values in the other fields */
            lans=mul(lans, toPower(k-2, cntcpos-omatch-omatch-i));

            ans=pl(ans, lans);
        }
    }

    ans=mul(ans, toPower(k, n-cntcpos));

    cout<<ans<<endl;
}

signed main() {
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
