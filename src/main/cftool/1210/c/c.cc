/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int mygcd(int i1, int i2) {
    if(i1==0)
        return i2;
    else if(i2==0)
        return i1;
    else
        return gcd(i1, i2);
}

/*
 * Dfs from the root.
 * while dfs, each child contributes the beauty
 * with all paths to all ancestors.
 * But that cant be that much different ones...since it is gcds.
 * (-> it turns out is is max O(logn) different ones)
 * Hand them down as map<val,cnt>, then simply count.
 *
 * Note edgecases with 0.
 */
const int MOD=1e9+7;
void solve() {
    cini(n);
    cinai(x,n);
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    int ans=0;
    function<void(int,int,map<int,int>&)> dfs=[&](int v, int p, map<int,int>& m) {
        map<int,int> m2;
        for(auto ent : m) {
            int g=mygcd(ent.first, x[v]);
            ans+=g*ent.second;
            ans%=MOD;
            m2[g]+=ent.second;
        }
        ans+=x[v];
        ans%=MOD;
        m2[x[v]]++;

        for(int chl : adj[v])
            if(chl!=p)
                dfs(chl, v, m2);
    };

    map<int,int> m;
    dfs(0,-1,m);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
