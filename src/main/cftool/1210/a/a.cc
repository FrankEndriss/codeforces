/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * If n<=6 we can simply place a domino on every edge.
 *
 * If n==7 we need to brute force which two vertex get the same
 * number. Then we try to place as much dominos as possible.
 */
void solve() {
    cini(n);
    cini(m);

    vi u(m);
    vi v(m);
    for(int i=0; i<m; i++) {
        cin>>u[i]>>v[i];
        u[i]--;
        v[i]--;
    }

    if(n<=6) {
        cout<<m<<endl;
        return;
    }
    assert(n==7);

    int ans=0;
    for(int ii=0; ii<=5; ii++) {
        for(int i=ii+1; i<=5; i++) {
            set<pii> dom;
            for(int j=0; j<=5; j++)
                for(int k=j; k<=5; k++)
                    dom.insert({j,k});

            vi vec(7);
            int num=1;
            for(int l=0; l<vec.size(); l++) {
                if(l==ii || l==i)
                    vec[l]=0;
                else {
                    vec[l]=num;
                    ++num;
                }
            }

            int lans=0;

            for(int l=0; l<m; l++) {
                pii p1= {vec[u[l]], vec[v[l]]};
                pii p2= {vec[v[l]], vec[u[l]]};
                if(dom.count(p1) || dom.count(p2)) {
                    lans++;
                    dom.erase(p1);
                    dom.erase(p2);
                }
            }
            ans=max(ans, lans);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
