/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Let f[i]=frequncy of a[i].
 * We can put every student into the group with an f[i]>1.
 * Then we can add all students which are submasks of 
 * these a[i].
 * Brute force.
 */
void solve() {
    cini(n);
    vi a(n);
    map<int,vi> f;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        f[a[i]].push_back(i);
    }
    cinai(b,n);


    vi aa;
    int ans=0;
    for(auto it=f.rbegin(); it!=f.rend(); it++) {
        if(it->second.size()>1) {
            aa.push_back(it->first);
            for(int idx : it->second)
                ans+=b[idx];
        } else {
            for(int mask : aa) {
                if((mask | it->first) == mask) {
                    ans+=b[it->second[0]];
                    break;
                }
            }
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
