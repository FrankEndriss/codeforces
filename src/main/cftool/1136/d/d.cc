/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* let pre[i] be the list of v which would change 
 * place whith i.
 *
 * It is allways optimal for nastya to change place
 * with the one right in front of her if possible.
 *
 * So nastja can change pos with a person i if
 * pre[nastya] contains i, and
 * all j between pos[nastya] and pos[i] have i
 * in pre[j]
 */
void solve() {
    cini(n);
    cini(m);
    cinai(p,n);

    vector<set<int>> adj(n+1);
    for(int i=0; i<m; i++) {
        cini(u); 
        cini(v);
        adj[u].insert(v);
    }

    set<int> s;
    s.insert(p[n-1]);
    int ans=0;
    for(int i=n-2; i>=0; i--) {
        int ok=true;
        for(int ss : s) {
            if(adj[p[i]].count(ss)==0) {
                ok=false;
                break;
            }
        }

        if(ok)
            ans++;
        else
            s.insert(p[i]);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
