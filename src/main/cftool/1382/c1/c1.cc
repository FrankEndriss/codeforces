/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* If last position is correct dont change.
 *
 * a=0001
 *  0,2: 0011
 *  0,1: 1111
 *  0,4: 0000
 * b=0000
 */
void solve() {
    cini(n);
    cins(a);
    cins(b);

    function<void(int)> doflip=[&](int idx) {
        for(int i=0; i<idx; i++) {
            if(a[i]=='0')
                a[i]='1';
            else
                a[i]='0';
        }
        reverse(a.begin(), a.begin()+idx);
    };

    vi ans;
    for(int i=n-1; i>0; i--) {
        if(a[i] !=b[i]) {
           if(a[0] == a[i]) {
                ans.push_back(i+1);
                doflip(i+1);
            } else {
                ans.push_back(1);
                doflip(1);
                ans.push_back(i+1);
                doflip(i+1);
            }
        }
    }
    if(a[0]!=b[0]) {
        ans.push_back(1);
        doflip(1);
    }
    
    cout<<ans.size()<<" ";
    for(int k : ans) 
        cout<<k<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
