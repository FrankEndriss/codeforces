/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * On last pile player takes all stones,
 * else player leaves one stone.
 *
 * What if there is a pile with 1 stone in next pile?
 * -> Player takes all stones in current pile, so it
 *  is same as leaving one stone.
 * What if there are two consecutive 1 in next two piles?
 * -> Player leafs one stone, so last of the seq of 1s goes to
 *  second player.
 *
 * What if this last one is last of all?
 *
 * Leading ones change the first player.
 */
void solve() {
    cini(n);
    cinai(a,n);

    bool first=true;
    int prev=0;
    int lead=0;
    for(int i=0; i<n; i++) {
        if(a[i]==1)
            lead++;
        else 
            break;
    }

    if(lead==n) {
        if(lead%2==1)
            cout<<"First"<<endl;
        else
            cout<<"Second"<<endl;
    } else {
        if(lead%2==0)
            cout<<"First"<<endl;
        else
            cout<<"Second"<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
