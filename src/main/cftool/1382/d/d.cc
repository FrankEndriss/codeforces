/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Consider last two elements of p
 * if p[n-2]<p[n-1] then
 * they can be 
 * the end of a[], 
 * the end of b[] or 
 * one end of a[] and one end of b[]
 * if(p[n-1]>p[n-1] then
 * they must have been both at the end of a[] or b[], and
 * the other array must have been empty before last two.
 *
 * Consider other direction, p[0]
 * That element was at a[0] or b[0]. So assume a[0].
 * Consider a[1].
 * If p[1]>p[0] it can be a[1] or b[0]
 * If p[1]<p[0] it must be a[1]
 *
 * So, increasing subarr in p[] can be put onto a[] and b[] in arbitrary 
 * order.
 * If decreasing number it means it is on same list as prev number.
 *
 * What about keeping track of number of fixed numbers in a[] and b[]
 * ....
 * ***************
 * Howto dp?
 * Put first in a
 * loop over 2n elements in p, foreach
 * dp[ai][prevStack][lastOtherElement]= ai elements on arr a other elements on arr b, 
 *      if prevStack previous element went to stack A else B, last element of other stack
 */
void solve() {
    cini(n);
    cinai(p,2*n);

    int fa=1;   /* first element goes to a by convention */
    int fb=0;
    for(int i=1; i<n; i++) {
        if(p[i]>p[i-1])
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
