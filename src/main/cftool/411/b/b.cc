/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    cini(m);
    cini(k);

    vb lco(n);  // locked cores
    vb lme(k);  // locked memory cells

    vvi inst(n, vi(m));
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            cin>>inst[i][j];
            inst[i][j]--;
        }

    vi ans(n);
    for(int j=0; j<m; j++) {
        vi cnt(k);
        for(int i=0; i<n; i++) {
            if(inst[i][j]<0)
                continue;
            if(lco[i])
                continue;

            cnt[inst[i][j]]++;
        }
        for(int i=0; i<n; i++) {
            if(inst[i][j]<0 || lco[i])
                continue;

            if(lme[inst[i][j]] || cnt[inst[i][j]]>1) {
                lme[inst[i][j]]=true;
                if(!lco[i]) {
                    lco[i]=true;
                    ans[i]=j+1;
                }
            }
        }
    }

    for(int i=0; i<n; i++)
        cout<<ans[i]<<endl;


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
