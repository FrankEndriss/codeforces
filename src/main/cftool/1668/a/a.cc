/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * 0-indexed.
 * i+j is allways even
 * If moved to 1,0, we cannot move to 2,0, but we can
 * 0,0, 1,1, 1,-1
 * So if we want to 2,0, we need to 
 * 2-Steps:
 * -1,-1; -1,1; 1,-1; 1,1;
 *
 * Let n<=m
 * So go to n,n in 2*n steps, then
 * RURDRURD 
 *
 * Edgecases: n=1
 */
void solve() {
    cini(n); n--;
    cini(m); m--;
    if(n>m)
        swap(n,m);

    if(n==0) {
        if(m==0)
            cout<<0<<endl;
        else if(m==1)
            cout<<1<<endl;
        else
            cout<<-1<<endl;
        return;
    }
    int ans=2*n;
    ans+=(m-n)+((m-n)/2)*2;
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
