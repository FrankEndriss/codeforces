/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * b[0]=0;
 * But maybe we can make things faster be subtracting a prefix?
 * So one element witll be b[i]==0, iterate all of them.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);

    int ans=INF;
    for(int zero=0; zero<n; zero++) {
        int lans=0;
        vi b(n);
        b[zero]=0;
        /* go right */
        for(int j=zero+1; j<n; j++) {
            int cnt=(b[j-1]+a[j])/a[j];
            lans+=cnt;
            b[j]=a[j]*cnt;
        }
        for(int j=zero-1; j>=0; j--) {
            int cnt=abs(b[j+1]-a[j])/a[j];
            lans+=cnt;
            b[j]=a[j]*-cnt;
        }
        ans=min(ans, lans);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
