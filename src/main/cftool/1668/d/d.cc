/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to build as big as possible positive subarrays,
 * then subtract 1 foreach remaining negative number.
 *
 * Consider each positive and negative contribute.
 * How can we optimize?
 * -put 0 into positive segements if possible
 * -positive segments can "swallow" 0 and negative elements, 
 *  make them grow to left and right while possible.
 *
 *  ...so much edgecases...how to grow if both adj of a pos
 *  segment are same? -> We need to grow in that direction that offers
 *  more zeros... sic implemenation.
 *  Fairly happy that I did not partitipate, so I do not have to implement this :)
 *  *******************
 *  Maybe there is a simpler aproach?
 *  We go alternating segments 
 *  pos,neg,pos,... OR
 *  neg,pos,neg,...
 *  If the sum of two adj pos is bg the neg in between, the
 *  three can be merged, sum.
 *  But, what
 *  -If we can merge a seg to the left OR to the right, but not both?
 *  -If we can merge not a whole neg segment, but some positions of it?
 *   How to know if we want to grow to the left or the right?
 *   -> do several passes:
 *      Go from left to right, grow to the left. Then
 *      Go again from left to right, move/grow to the 
 *      right, use best position.
 *      What if two pos segments touch? How to know if merge is better
 *      or a previous position was better?
 */
void solve() {
    cini(n);
    cinai(a,n);

    function<int(int)> sign=[](int i) {
        if(i<0)
            return -1;
        else if(i==0)
            return 0;
        else 
            return 1;
    }

    int sfirst=0;
    for(int i=0; i<n; i++) {
        if(a[i]<0) {
            sfirst=-1;
            break;
        } else if(a[i]>0) {
            sfirst=1;
            break;
        }
    }
    if(sfirst==0) { // all 0
        cout<<0<<endl;
        return;
    }

    vvi pos;
    vvi neg;

    int sprev=0;
    vi empty;
    stack<int> zero;
    for(int i=0; i<n; i++) {
        if(a[i]<0) {
            if(sprev>=0)
                neg.push_back(empty);
            else {
                while(zero.size()) {
                    neg.push_back(zero.top());
                    zero.pop();
                }
            }
            neg.back().push_back(i);
            sprev=-1;
        } else if(a[i]>0) {
            if(sprev<0)
                pos.push_back(empty);

            while(zero.size()) {
                pos.push_back(zero.top());
                zero.pop();
            }
            pos.back().push_back(i);
        } else if(a[i]==0) {
            zero.push(i);
        }
    }
    if(sprev<0) {
        while(zero.size()) {
            neg.push_back(zero.top());
            zero.pop();
        }
    } else {
        while(zero.size()) {
            pos.push_back(zero.top());
            zero.pop();
        }
    }

    /** now try to grow all pos segments in both directions */
    for(size_t i=0; i<pos.size(); i++) {
        int sum=0;
        sort(all(pos[i]));
        if(pos[i].size()==1) 
            pos[i].push_back(pos[i][0]);
        else {
            pos[i][1]=pos[i].back();
            pos[i].resize(2);
        }

        for(int j=pos[i][0]; j<=pos[i][1]; j++) 
            sum+=a[j];

        while(true) {
            int l=INF;
            if(pos[i][0]-1>=0 && a[pos[i][0]-1]<=0)
                l=a[pos[i][0]-1];
            int r=INF;
            if(pos[i][1]+1<n && a[pos[i][0]+1]<=0)
                r=a[pos[i][0]+1];

            if(l==0) {
                pos[i][0]--;
            } else if(r==0) {
                pos[i][1]++;
            } else {
                if(l!=INF && r!=INF) {
                    if(l>=r)
                }

            }
        }
                
    }


    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
