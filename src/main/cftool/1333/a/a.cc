/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* if a*b is odd then simply alternate colors.
 * if a*b is even, one white must be hidden.
 */
void solve() {
    cini(a);
    cini(b);

    vs ans(a);
    for(int i=0; i<a; i++) {
        for(int j=0; j<b; j++) {
            char c='B';
            if((i+j)%2)
                c='W';
            ans[i]+=c;
        }
    }
    if((a*b)%2==0) {
        ans[0][0]='W';
        for(int i=0; i<a; i++)
            for(int j=0; j<b; j++)
                if(ans[i][j]=='W')
                    ans[i][j]='B';
                else
                    ans[i][j]='W';
    }

    for(int i=0; i<a; i++)
        cout<<ans[i]<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

