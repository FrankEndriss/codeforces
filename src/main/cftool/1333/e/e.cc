/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* find 3x3 board */
void help() {
    vi p= { 1, 2, 3, 4, 5, 6, 7, 8, 0 };
    do {
        vector<pii> m(9);   /* m[i]= position of ith number */
        for(int i=0; i<3; i++)
            for(int j=0; j<3; j++)
                m[p[i*3+j]]= {i,j};

        if(m[0]==make_pair(1LL,1LL)) /* do not start in middle */
            continue;

        /* rook */
        pii pos=m[0];
        int vunsR=0;
        set<int> s;
        for(int i=1; i<=8; i++)
            s.insert(i);

        for(int k=0; k<8; k++) {    /* 8 moves */
            bool moved=false;
            for(int next : s) {
                if(pos.first==m[next].first || pos.second==m[next].second) {
                    pos=m[next];
                    s.erase(next);
                    moved=true;
                    break;
                }
            }
            if(!moved) {
                vunsR++;
                pos=m[*s.begin()];
                s.erase(s.begin());
            }
        }

        /* queen */
        pos=m[0];
        int vunsQ=0;
        s.clear();
        for(int i=1; i<=8; i++)
            s.insert(i);

        for(int k=0; k<8; k++) {    /* 8 moves */
            bool moved=false;
            for(int next : s) {
                if(pos.first==m[next].first || pos.second==m[next].second || abs(pos.first-m[next].first)==abs(pos.second-m[next].second) ) {
                    pos=m[next];
                    s.erase(next);
                    moved=true;
                    break;
                }
            }
            if(!moved) {
                vunsQ++;
                pos=m[*s.begin()];
                s.erase(s.begin());
            }
        }

        if(vunsR<vunsQ) {
            cout<<"found it"<<endl;
            for(int i=0; i<3; i++) {
                for(int j=0; j<3; j++) {
                    cout<<p[i*3+j]<<" ";
                }
                cout<<endl;
            }
            return;
        }


    } while(next_permutation(p.begin(), p.end()));
}

void solve() {
    cini(n);
    if(n<3) {
        cout<<-1<<endl;
        return;
    }

    vvi _3x3= {
        { 2, 3, 4 },
        { 5, 7, 9 },
        { 8, 1, 6 }
    };

    vvi ans(n, vi(n));
    for(int i=0; i<3; i++) {
        for(int j=0; j<3; j++) {
            ans[i][j]=_3x3[i][j]+n*n-9;
        }
    }
    
    if(n>3) {
        ans[3][1]=n*n-9;
        ans[3][0]=n*n-10;
        ans[3][2]=n*n-11;
        ans[3][3]=n*n-12;
        ans[2][3]=n*n-13;
        ans[1][3]=n*n-14;
        ans[0][3]=n*n-15;

        int v=n*n-16;
        for(int i=4; i<n; i++) {
            if(i%2==0) {
                for(int j=0; j<=i; j++)
                    ans[j][i]=v--;
                for(int j=i-1; j>=0; j--)
                    ans[i][j]=v--;
            } else {
                for(int j=0; j<=i; j++) 
                    ans[i][j]=v--;
                for(int j=i-1; j>=0; j--)
                    ans[j][i]=v--;
            }
        }
    }

    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++)
                cout<<ans[i][j]<<" ";
        cout<<endl;
    }
    

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

