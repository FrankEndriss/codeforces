/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/*
 *
 * How to find the sum==0 subarrays?
 * Prefix sum, store in map<sum,index>.
 * If any prefix-sum repeats, we can remove the previous
 * prefix-sum to get a zero interval.
 * This interval starts at position just after the previous
 * seen prefix-sum.
 *
 * We count subarrays ending in position i for all positions.
 * Per position that are all starting after the begin of the
 * rightmost zero interval.
 * ie for each position we maintain the biggest index of left bound
 * of all zero intervals.
 *
 * Note that for a[i]=0 position i does not contribute at all, since
 * all subarrays ending in i contain the single element subarray a[i..i],
 * which is a zero interval.
 */
void solve() {
    cini(n);

    map<int,int> pre;   /* pre[sum]= first index after sum, ie first countable index */

    int ma=0;   /* min index right of left bound of zero interval, ie first countable index */
    int sum=0;
    int ans=0;
    pre[0]=-1;

    for(int i=1; i<=n; i++) {
        cini(aux);
        sum+=aux;

        if(pre.find(sum)!=pre.end()) {
            if(sum==0 && pre[0]==-1) {
                ma=max(ma, 1LL);
            } else {
                ma=max(ma, pre[sum]+1);
            }
        }
        pre[sum]=i;
        ans+=i-ma;
    }

    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

