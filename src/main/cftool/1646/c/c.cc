/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that it is allways possible to
 * represent n as the number of its set bits.
 *
 * Can we construct all sums of some factorials up to n,
 * and count if there is some difference with less bits?
 */
const int N=1e12+7;
void solve() {
    cini(n);

    vi fac(1);
    fac[0]=1;
    for(int i=1; fac.back()<N; i++)
        fac.push_back(fac.back()*i);


    int ans=40;
    int j=1LL<<(fac.size());
    for(int i=0; i<j; i++) {
        int sum=0;
        int cnt=0;
        for(size_t k=0; k<fac.size(); k++) 
            if((1LL<<k)&i) {
                sum+=fac[k];
                cnt++;
            }

        if(sum<=n) {
            int d=n-sum;
            while(d) {  /* count bits of d */
                cnt+=(d&1);
                d/=2;
            }
            ans=min(ans, cnt);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
