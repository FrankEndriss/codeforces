/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int primeAproxFactor=2;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

#define endl "\n"

/**
 * 2^4==4^2
 * and 1^x==1
 * but all others are distinct
 * ...no
 *
 * note that
 * 4^x == 2^(2*x) == 8^x/2
 * and so on
 * 
 * What about 8^2 == 2^6???
 * Iterate all powers of all numbers up to sqrt(n);sqrt(m)
 *
 * ...Note that 16 is there 3 times
 * 16^1; 4^2; 2^4
 *
 * 81:
 * 81^1; 3^4; 9^2
 *
 * if we can half j we need to square i
 *
 * Consider col j=4 all numbers x are there again in row x*x in col j/2
 *
 * What about 3?
 * col==y, all numbers x are there again in rows x*x*x in col y/3
 *
 * Consider all dividers of a number...
 * ???
 * idk :/
 */
int pp(int i, int p) {
    int ii=1;
    for(int j=0; j<p; j++)
        ii*=i;
    return ii;
}

void solve() {
    cini(n);
    cini(m);

    int ans=n*m;
    ans-=(m-1);     /* first row */

    for(int p : pr) {
        if(p>m)
            break;

        for(int row=2; row*row<=n; row++) {
            for(int col=p; col<=m; col+=p) {
                ans--;
            }
        }
    }

    cout<<ans<<endl;
}

signed main() {
    //cerr<<"pr[0]="<<pr[0]<<" pr[1]="<<pr[1]<<endl;
    solve();
}
