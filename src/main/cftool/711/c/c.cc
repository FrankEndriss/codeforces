/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* complecated construction of problem...
 * Cases:
 * Colored trees between two other colored trees
 * are always a component, we cannot make number
 * of components smaller.
 * In the end all trees must be colored.
 *
 * Coloring a tree between two trees of diff color
 * can increase the number of components.
 * Coloring a tree between two trees of same color
 * can increase the number of components by 2.
 *
 * So first we need to find the number of increases
 * we need to construct.
 *
 * Other way is to start with cheapest painting,
 * then repair cheapest possible to match k.
 *
 * Note that each gap is independent of each other gap.
 * A gap is defined by left color, size and right color.
 * So there are at most 100^3 different gaps.
 * Note also that left and right color is interchangeable.
 *
 * For every gap we have a price of coloring it in a way
 * increasing the number of components by some kk.
 *
 * And we have a kkk which we need to implement.
 * We need to find the cheapest combi of g[kk] which sums
 * up to kkk, which is knapsack.
 *
 * How to find cheapest g[kk]? Greedy?
 * Coloring the whole gap in cheapest color is cheapest ever,
 * and kk==0 or kk==1
 * ...several casework for 2, 3 etc
 * Gaps with one border color different from gaps with two 
 * border colors. Max kk eq size of gap.
 * ...
 * Tutorial: use dp[i][j][k] min cost for i trees with beauty j and last color k 
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
