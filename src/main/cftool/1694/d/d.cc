/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to place numbers in the leafs first.
 * We need to place numbers in all vertex.
 * But vertex, where the sum of the childs is bgeq the
 * number in the vertex can be filled while filling the child.
 * Note that it is not the sum of subtree, but the sum of the 
 * direct childs only.
 *
 * What about l[] and r[].
 * While we go from leafs up we allways want to choose the biggest 
 * possible number on our vertex, because this maximized the chance
 * to fill the parent together with the child.
 *
 * Do dfs and place numbers in vertex in post order.
 */
void solve() {
    cini(n);
    cinai(p,n-1);
    vi l(n);
    vi r(n);
    for(int i=0; i<n; i++) 
        cin>>l[i]>>r[i];

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        adj[i+1].push_back(p[i]-1);
        adj[p[i]-1].push_back(i+1);
    }

    int ans=0;
    function<int(int,int)> dfs=[&](int v, int pp) {
        int sum=0;
        for(int chl : adj[v])
            if(chl!=pp)
                sum+=dfs(chl,v);

        if(sum<l[v]) {
            ans++;
            return r[v];
        } else
            return min(sum, r[v]);
    };

    dfs(0,-1);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
