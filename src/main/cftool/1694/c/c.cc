/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Can we do it in one pass, from left to right?
 * -> no
 * look the examples:
 * sum must be 0, and postfix sum must not be positive.
 * ...But that is not sufficient.
 *
 * Obviously remove all trailing 0s.
 * Then it is like two pointer:
 * Left points to a positive, right to the next negative.
 * The positive must allways
 *
 * ...We can do it allways from left to right.
 */
void solve() {
    cini(n);
    cinai(a,n);

    while(a.size()>0 && a.back()==0) {
        a.pop_back();
        n--;
    }

    if(n==0) {
        cout<<"Yes"<<endl;
        return;
    } else if(n==1) {
        cout<<"No"<<endl;
        return;
    }

    vi b(n);
    for(int i=0; i+1<n; i++) {
        int d=a[i]-b[i];
        if(i>0)
            d++;

        if(d<=0) {
            cout<<"No"<<endl;
            return;
        }

        b[i]+=d;
        b[i+1]-=(d-1);
    }
    b.back()--;

    if(b.back()==a.back())
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
