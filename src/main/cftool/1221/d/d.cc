/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * It can make sense to go 1 up or two up.
 * Or on up and adj one up. Or two up.
 * Or thats adj one up. etc...
 */
void solve() {
    cini(n);
    vi a(n);
    vi b(n);
    for(int i=0; i<n; i++)
        cin>>a[i]>>b[i];

    const int INF=1e18;
    vvi dp(n,vi(3, INF));   /*  cost up to index i if  i increased by 0,1,2 */

    dp[0][0]=0;
    dp[0][1]=b[0];
    dp[0][2]=b[0]*2;
    for(int i=1; i<n; i++) {
        for(int j=0; j<3; j++) {
            for(int k=0; k<3; k++) {
                if(a[i-1]+j!=a[i]+k)
                    dp[i][k]=min(dp[i][k], dp[i-1][j]+b[i]*k);
            }
        }
    }
    int ans=min(dp[n-1][0], min(dp[n-1][1], dp[n-1][2]));
    cout<<ans<<endl;
}

signed main() {
    cini(q);
    while(q--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
