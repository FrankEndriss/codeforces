/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * When we get a command, it is executed or not.
 * So we know where the robot will be after
 * the given time.
 *
 * For each point in time we 
 * 1. wait for next command, or
 * 2. move to next target.
 *
 * So foreach t[i] it is case 1 or 2.
 *
 * Case 1 we take the command and start to move.
 * if we will reach target before t[i+1] then ans++.
 *
 * Case 2 we are moveing in dir to target.
 * if x is in interval (pos,target), then ans++
 *
 * I dont get it, why WA???
 */
const int INF=1e9;
void solve() {
    cini(n);

    int pos=0; /* current position, set to next pos in each step. */
    int target;
    int dir=0;
    int ans=0;
    int blocked=-1;

    vi t(n);
    vi x(n);
    for(int i=0; i<n; i++)
        cin>>t[i]>>x[i];

    t.push_back(3*INF);
    x.push_back(3*INF);

    for(int i=0; i<n; i++) {
        if(t[i]>=blocked) { /* not blocked, case 1 */
            int diff=abs(x[i]-pos);
            blocked=t[i]+diff;
            target=x[i];
            if(t[i+1]-t[i]>=diff) {
                ans++;
                pos=x[i];
                dir=0;
            } else {
                dir=1;
                if(x[i]<pos)
                    dir=-1;
                pos=pos+dir*(t[i+1]-t[i]);
            }
        } else { /* blocked, check if x is reached anyway */
            assert(i>0);
            int diff=abs(target-pos);

            int steps=min(diff, t[i+1]-t[i]);
            int npos=pos+dir*steps;

            if((pos<=npos && x[i]>=pos && x[i]<=npos) || (pos>=npos && x[i]<=pos && x[i]>=npos))
                ans++;

            pos=npos;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
