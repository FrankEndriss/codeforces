/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find min and max x.
 * We have a permutation of len even 2*n.
 * n pairs.
 *
 * Greedy find the max number of min pairs,
 * and the min number of min pairs as the max number of max pairs.
 */
void solve() {
    cini(n);
    cinai(b,n);

    set<int> a;
    set<int> aa;
    for(int i=1; i<=2*n; i++) {
        a.insert(i);
        aa.insert(-i);
    }

    sort(all(b));
    for(int i : b) {
        a.erase(i);
        aa.erase(-i);
    }

    int mi=0; /* max number of min pairs */
    for(int i=0; i<n; i++) {
        auto it=a.upper_bound(b[i]);
        if(it!=a.end()) {
            mi++;
            a.erase(it);
        } else {
            break;
        }
    }

    int ma=0;   /* max number of max pairs */
    reverse(all(b));
    for(int i=0; i<n; i++) {
        auto it=aa.upper_bound(-b[i]);
        if(it!=aa.end()) {
            ma++;
            aa.erase(it);
        } else {
            break;
        }
    }

    mi=n-mi;    /* min number of max pairs */
    int ans=abs(mi-ma)+1;
    cout<<ans<<endl;




}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
