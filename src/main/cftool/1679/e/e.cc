/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider all substrings of orig string,
 * and the set of positions with an opposite
 * position that is fixed.
 * We need to resolve all those fixed positions.
 * So each substring belongs to one of 2^17 classes,
 * and each query set resolves all classes
 * of its mask and all submasks.
 *
 * We can create the n^2 substring masks in O(n^2),
 * and sort into 2^17 categories.
 *
 * Then we need to find foreach mask the sum with all 
 * submasks...should be iterable.
 *
 * Then answer in O(1)
 *
 * ....
 * Ok, we do not have to count the the possible substrings
 * in orig string (as the statement suggests), but the 
 * number of different _buildable_ substrings.
 * So we also need to count the positions where we are free to 
 * choose the char, that is where both corresponding
 * palindrome possitions eq '?' (or the center of an odd len palin).
 * But that means we not only have 2^N classes, but foreach substring
 * also need to maintain the number of free positions.
 * Free positions can be up to (n+1)/2
 * Since a free positions means "multiply by sym^fp" we cannot simple
 * sum them up.
 * But, ok, since it is at most 17 symbols, we cann maintain 17
 * counters, and directly add those numbers.
 */
const int N=17;
const int NN=1<<N;
using mint=modint998244353;
void solve() {
    cini(n);
    cins(s);
    cini(q);

    vector<vector<mint>> mask(N, vector<mint>(NN));

    for(int i=0; i<n; i++) {
        /* palindromes center at i */
        int m=0;
        int f=0;
        if(s[i]=='?')
            f++;
            //m=m|(1<<(s[i]-'a'));

        for(int j=1; j<=N; j++)
            mask[j-1][m]+=mint(j).pow(f);

        //cerr<<"m1="<<m<<endl;
        for(int j=1; i-j>=0 && i+j<n; j++) {
            if(s[i-j]=='?' && s[i+j]!='?') {
                m=m|(1<<(s[i+j]-'a'));
                //cerr<<"right, s[i+j]="<<s[i+j]<<" m="<<m<<endl;
            } else if(s[i-j]!='?' && s[i+j]=='?') {
                m=m|(1<<(s[i-j]-'a'));
                //cerr<<"left, s[i-j]="<<s[i-j]<<" m="<<m<<endl;
            } else if(s[i-j]!='?' && s[i+j]!='?' && s[i-j]!=s[i+j]) {
                break;
            } else if(s[i-j]=='?' && s[i+j]=='?')
                f++;

            for(int k=1; k<=N; k++) 
                mask[k-1][m]+=mint(k).pow(f);
            //cerr<<"m1="<<m<<endl;
        }

        /* palindromes center at i,i+1 */
        m=0;
        f=0;
        for(int j=0; i-j>=0 && i+1+j<n; j++) {
            if(s[i-j]=='?' && s[i+1+j]!='?') {
                m=m|(1<<(s[i+1+j]-'a'));
            } else if(s[i-j]!='?' && s[i+1+j]=='?') {
                m=m|(1<<(s[i-j]-'a'));
            } else if(s[i-j]!='?' && s[i+1+j]!='?' && s[i-j]!=s[i+1+j]) {
                break;
            } else if(s[i-j]=='?' && s[i+1+j]=='?')
                f++;

            for(int k=1; k<=N; k++) 
                mask[k-1][m]+=mint(k).pow(f);
            //cerr<<"m2="<<m<<endl;
        }
    }

    
    vector<vector<mint>> ans(N, vector<mint>(NN));
    for(int i=0; i<NN; i++) {
        for(int j=i; j>0; j=(j-1)&i) {
            for(int k=0; k<17; k++) 
                ans[k][i]+=mask[k][j];
        }
        for(int k=0; k<17; k++) 
            ans[k][i]+=mask[k][0];
    }

    for(int i=0; i<q; i++) {
        cins(t);
        int val=0;
        for(char c : t)
            val|=(1<<(c-'a'));

        cerr<<"t="<<t<<" val="<<val<<endl;

        cout<<ans[t.size()-1][val].val()<<endl;
    }

}

signed main() {
    solve();
}
