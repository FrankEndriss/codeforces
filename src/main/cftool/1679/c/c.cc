/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 */
void solve() {
    cini(n);
    cini(q);

    stree segR(n);
    stree segC(n);

    vi cntR(n);
    vi cntC(n);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(x); x--;
            cini(y); y--;
            cntR[x]++;
            cntC[y]++;
            segR.set(x,1);
            segC.set(y,1);
        } else if(t==2) {
            cini(x); x--;
            cini(y); y--;
            cntR[x]--;
            cntC[y]--;
            if(cntR[x]==0)
                segR.set(x,0);
            if(cntC[y]==0)
                segC.set(y,0);
        } else if(t==3) {
            cini(xL); xL--;
            cini(yL); yL--;
            cini(xR);
            cini(yR);

            int r=segR.prod(xL,xR);
            int c=segC.prod(yL,yR);

            if(r==xR-xL || c==yR-yL)
                cout<<"Yes"<<endl;
            else 
                cout<<"No"<<endl;

            /*
            int ans=c*(xR-xL) + r*(yR-yL) - c*r;
            cout<<ans<<endl;
            */

        } else 
            assert(false);
    }


}

signed main() {
    solve();
}
