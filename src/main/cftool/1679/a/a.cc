/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * omg, why did I submit in first place?
 *
 *
 */
void solve() {
    cini(n);

    if(n%2 || n<4) {
        cout<<-1<<endl;
        return;
    }
    n/=2;

    int ansMi=n/3;
    if((n-ansMi*3)==1)
        ansMi--;
    ansMi=ansMi+(n-ansMi*3)/2;

    int ansMa=0;
    if(n%2==1) {
        ansMa=(n-3)/2 + 1;
    } else
        ansMa=n/2;

    cout<<ansMi<<" "<<ansMa<<endl;



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
