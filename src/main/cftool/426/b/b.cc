/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++)
            cin>>a[i][j];

    while(true) {
        if(a.size()%2==1) {
            cout<<a.size()<<endl;
            return;
        }
        bool ok=true;
        for(int i=0; i<a.size()/2; i++) 
            if(a[i]!=a[a.size()-1-i])
                ok=false;

        if(ok) {
            a.resize(a.size()/2);
        } else {
            cout<<a.size()<<endl;
            return;
        }
    }
    assert(false);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
