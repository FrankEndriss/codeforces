/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);    /* num students */
    cini(k);    /* scored students */
    cini(l);    /* min student */
    cini(r);    /* max student */
    cini(sall)  /* sum all */
    cini(sk)    /* sum k scored students */

    vi ans(n);

    int used=0;
/* First distribute sumK to group k */
    for(int i=n-k; i<n; i++) {
        int val=(sk-used)/(n-i);
//cerr<<"i="<<i<<" val="<<val<<endl;
        ans[i]=val;
        used+=val;
    }

/* Then do the same for group not k */
    for(int i=0; i<(n-k); i++) {
        int val=(sall-used)/(n-k-i);
//cerr<<"i="<<i<<" val="<<val<<endl;
        ans[i]=val;
        used+=val;
    }
    
    for(int a : ans)
        cout<<a<<" ";
    cout<<endl;
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

