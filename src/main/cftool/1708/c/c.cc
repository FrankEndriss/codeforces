/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Well, what is what?
 * We got some q that decreases or does not change.
 *
 * And we need to decide if she chooses the contests foreach day.
 * Condition is that q must not go below 0, so 
 * she can test only while positive q.
 *
 * Obviously she can test all days i that are initially a[i]<q,
 * also can test all contests on the right end.
 * Let a[j] be all the a[i]<q positions, then she can 
 * additionally test n-min(j-(q-a[j])) contests.
 *
 * Can she do better? If not, why cant she do better?
 * Well, according to examples this seems to be sufficient.
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    int l=0; 
    for(int i=0; i<n; i++)
        if(a[i]<=q)
            l=max(l, i-(q-a[i]));

    string ans(n,'0');
    for(int i=0; i<n; i++)
        if(a[i]<=q || i>=l)
            ans[i]++;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
