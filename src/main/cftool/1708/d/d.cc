/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * So, after one operation
 * a[1][0]=a[0][1]-a[0][0]
 * a[2][0]=a[1][1]-a[1][0]
 *        =a[0][2]-a[0][1]-(a[0][1]-a[0][0])
 *        =a[0][2]-a[0][0]
 * ...
 * a[n-1][0]=a[0][n-1]-a[0][0]
 *
 * But, how comes "we sort b[]" into play?
 * Note that also first array is sorted.
 *
 * It like doing a reverse prefix sum creation...somehow.
 * Looks actually like a math problem, too.
 */
void solve() {
    cini(n);
    cinai(a,n);
    int ans=a[n-1]-a[0];
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
