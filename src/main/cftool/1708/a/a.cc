/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How?
 * Substitute by the difference means that a[2] must 
 * be set to 0 as last operation, else we cannot ever
 * change a[3].
 * So go from right to left.
 * ...
 * No :/
 * Seems to be some math puzzle somehow...and a simple
 * one as there are allready 5000 AC.
 *
 * Ok, why is ans=NO in last example?
 *
 * It seems we must make all elements same, then make them 0.
 * Why?
 * -> Bcs first element is fixed, and we must make a[2]==a[1]
 *  to be able to make a[2]==0, so we also have to 
 *  make a[2]==a[3]... and so on.
 */
void solve() {
    cini(n);
    cinai(a,n);

    for(int i=2; i<n; i++) {
        if(a[i]<a[0] || a[i]%a[0]!=0) {
            cout<<"NO"<<endl;
            return;
        }
    }
    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
