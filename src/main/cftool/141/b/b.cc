/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(a);
    cini(x);
    cini(y);

    if(abs(x)>=a || y<0 || y%a==0) {
        cout<<-1<<endl;
        return;
    }

    y-=a;
    if(y<0) {
        if(abs(x*2)<a)
            cout<<1<<endl;
        else 
            cout<<-1<<endl;
        return;
    }

    int h=y%(2*a);
    int m=y/(2*a);
    int ans=1+3*m;
    if(h<a) { /* single rect */
        if(abs(2*x)>=a) {
            cout<<-1<<endl;
            return;
        }
        ans++;
    } else {    /* two rects */
        if(x==0) {
            cout<<-1<<endl;
            return;
        }

        if(x<0)
            ans+=2;
        else
            ans+=3;
    }

    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

