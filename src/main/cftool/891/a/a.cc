/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* If there is at least one 1 in the array we need
 * one operation per element !=1.
 * Else we need to find the shortest subsegment with gcd==1,
 *  we need sizeof(subsegment)-1 operations to create a 1.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int gcdA=a[0];  /* gcd of all numbers */
    int cnt1=0; /* count of 1 in a */

    int minSeg=n;
    for(int i=0; i<n; i++) {
        if(a[i]==1)
            cnt1++;

        gcdA=__gcd(gcdA, a[i]);


        int lgcd=a[i];
        for(int j=i+1; lgcd!=1 && j<n; j++) {
            lgcd=__gcd(lgcd, a[j]);
            if(lgcd==1)
                minSeg=min(minSeg, j-i+1);
        }
    }

    if(gcdA!=1) 
        cout<<-1<<endl;
    else if(cnt1>0)
        cout<<n-cnt1<<endl;
    else
        cout<<minSeg-1+n-1<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

