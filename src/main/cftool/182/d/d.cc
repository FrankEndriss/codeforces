/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void divs(int n, set<int> &ret) {
    for(int i=1; i*i<=n; i++) {
        if(n%i==0) {
            ret.insert(i);
            ret.insert(n/i);
        }
    }
}

/* let k be the length of smallest repeated string in s, and 
 * xs the number of times it repeats in s, and
 * xt the number of times it repeats in t, then
 * ans=number of divisors which are common of xs and xt.
 */
void solve() {
    cins(s);
    cins(t);

    if(s.size()>t.size())
        s.swap(t);

    int klen=-1;
    for(int k=1; k<=s.size(); k++) {
        if(s.size()%k!=0)
            continue;
        
        bool rep=true;
        for(int j=0; rep && j<k; j++) {
            for(int i=j+k; rep && i<s.size(); i+=k) {
                if(s[i]!=s[i-k])
                    rep=false;
            }
        }
        if(rep) {
            klen=k;
            break;
        }
    }

    if(klen<1 || t.size()%klen!=0) {
        cout<<0<<endl;
        return;
    }

/* check if t is multiple of substring of len klen */
    bool tok=true;
    for(int i=0; i<klen; i++)
        if(s[i]!=t[i])
            tok=false;

    for(int i=0; i<klen; i++) {
        for(int j=i+klen; j<t.size(); j+=klen) {
            if(t[i]!=t[j])
                tok=false;
        }
    }

    if(tok) {
        set<int> ss;
        divs(s.size()/klen, ss);
        set<int> st;
        divs(t.size()/klen, st);

        int ans=0;
        for(int i : ss) 
            ans+=st.count(i);

        cout<<ans<<endl;
        
    } else {
        cout<<0<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

