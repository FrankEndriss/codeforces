/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Only simple dijkstra?
 * Implement what is written.
 * But, we cannot maintain n^2 dp states.
 * So only maintain the 
 * dp[j]=max health after used ladder j
 */
const int INF=1e18;
void solve() {
	cini(n);
	cini(m);
	cini(k);

	cinai(x,n);

	vector<tuple<int,int,int,int,int>> rl(k); /* rl[i]={a,b,c,d,h} */
	vector<vector<tuple<int,int,int,int,int>>> l(n+1); /* l[a][x]={i,b,c,d,h} */
	for(int i=0; i<k; i++) {
		cini(a); a--;
		cini(b); b--;
		cini(c); c--;
		cini(d); d--;
		cini(h);
		l[a].emplace_back(i,b,c,d,h);
		rl[i]={a,b,c,d,h};
	}

	vi dp(k, -INF);

	priority_queue<pii> q;
	for(size_t j=0; j<l[0].size(); j++) {
		auto [i,b,c,d,h]=l[0][j];
		dp[i]=h-x[0]*b;
		q.emplace(-dp[i], i);
	}

	while(q.size()) {
		auto [hhh,i]=q.top();
		q.pop();
		if(dp[i]!=-hhh)
			continue;

		auto [a,b,c,d,h]=rl[i];

		for(auto [ii,bb,cc,dd,hh] : l[c]) {
			int val=dp[i]+hh-x[c]*abs(d-bb);
			if(dp[ii]<val) {
				dp[ii]=val;
				q.emplace(-dp[ii], ii);
			}
		}
	}
	// hello?

	int ans=-INF;
	for(int j=0; j<k; j++) {
		auto [a,b,c,d,h] = rl[j];
		if(c==n-1)
			ans=max(ans, dp[j]-x[n-1]*abs(m-1-d));
	}

	if(ans==-INF)
		cout<<"NO ESCAPE"<<endl;
	else
		cout<<-ans<<endl;


}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
