/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * So Rahul will try to sit in center,
 * Tina will paint cells near to center.
 *
 * Casework odd/even.
 * ...and even more bad casework, consider last row/col :/
 *
 * So center area start to be 1 cell, 2 cell or 4 cell,
 * depending on parity of n,m.
 * Once the complete center is painted, the dist increases
 * by 1.
 * then the center extends by 1 cell in each direction.
 *
 * Also consider edge-cases with n=1 and m=1
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    vvi dp(n, vi(m, INF));
    dp[0][0]=0;
    dp[0][m-1]=0;
    dp[n-1][0]=0;
    dp[n-1][m-1]=0;

    queue<tuple<int,int>> q;
    q.emplace(0,0);
    q.emplace(0,m-1);
    q.emplace(n-1,0);
    q.emplace(n-1,m-1);

    const vi dx= { -1, 1, 0, 0};
    const vi dy= { 0, 0, -1, 1};
    while(q.size()) {
        auto [i,j]=q.front();
        q.pop();
        for(int k=0; k<4; k++) {
            const int ii=i+dx[k];
            const int jj=j+dy[k];
            if(ii>=0 && ii<n && jj>=0 && jj<m && dp[ii][jj]>dp[i][j]+1) {
                dp[ii][jj]=dp[i][j]+1;
                q.emplace(ii,jj);
            }
        }
    }

    vi f(n+m);
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            f[dp[i][j]]++;

    while(f.back()==0)
        f.pop_back();
    assert(f.size()>0);

    int ans=n/2+m/2;
    for(int i=0; i<n*m; i++)  {
	    if(f.back()==0) {
		    ans++;
		    f.pop_back();
	    }
	    f.back()--;

	    cout<<ans<<" ";
    }
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
