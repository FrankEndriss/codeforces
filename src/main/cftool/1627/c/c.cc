/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* find the biggest group of p<1e5 where each
 * sum of two elements is also a prime.
 * How?
 * ...
 * 2 3
 * 2 5
 * 2 11
 * Note that 2 is the only even prime, so all prime sums are even,
 * so no prime.
 * There are only "some" primes where p+2 is also a prime.
 * So if there is a vertex with 3 edges, there is no sol.
 * Else we can alternate 2,3
 *
 * How to construct the output?
 */
void solve() {
	cini(n);
	int ma=0;
	vector<set<int>> adj(n);
	vector<pii> edg(n-1);
	vi cnt(n);
	for(int i=0; i<n-1; i++) {
		cini(u); u--;
		cini(v); v--;
		edg[i]={u,v};
		cnt[u]++;
		cnt[v]++;
		ma=max(ma, cnt[u]);
		ma=max(ma, cnt[v]);

		adj[u].insert(i);
		adj[v].insert(i);
	}

	if(ma>2) {
		cout<<-1<<endl;
		return;
	}


	int root=-1;
	for(int i=0; i<n; i++) 
		if(adj[i].size()==1)
			root=i;
	assert(root>=0);

	vi ans(n-1);
	int val=2;
	for(int i=0; i<n-1; i++) {
		int e=*adj[root].begin();
		ans[e]=val;

		auto [u,v]=edg[e];

		adj[u].erase(e);
		adj[v].erase(e);

		if(val==2)
			val=3;
		else 
			val=2;

		if(root==u)
			root=v;
		else
			root=u;
	}

	for(int i=0; i<n-1; i++) 
		cout<<ans[i]<<" ";
	cout<<endl;


}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
