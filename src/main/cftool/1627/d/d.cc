/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/**
 * Sorting does not matter.
 * We need to somehow optimize for speed, cannot check all pairs,
 * also N=1e6.
 *
 * But all a[i]<=N.
 * So build groups of numbers with gcd>1
 *
 * Consider the multiples of 2
 * If 2 is not in the array, and we find two multiples of 2,
 * then 2 can be created.
 *
 * So start at N/2, and find if it can and/or should be constructed,
 * and so on until 2.
 */
const int N=1e6+7;
void solve() {
	cini(n);
	cinai(a,n);

	vb vis(N);
	for(int i=0; i<n; i++)
		vis[a[i]]=true;

	int ans=0;
	for(int i=N/2; i>=1; i--) {
		if(vis[i])
			continue;

		int g=-1;
		for(int j=2; j*i<N; j++) {
			if(!vis[j*i])
				continue;

			if(g<0)
				g=j*i;
			else
				g=gcd(g, j*i);
		}

		if(g==i) {
			ans++;
			vis[i]=true;
		}
	}

	cout<<ans<<endl;
}

signed main() {
    solve();
}
