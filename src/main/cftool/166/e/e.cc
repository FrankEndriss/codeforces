/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#define XOX

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MOD=1e9+7;

/* 4 vertex, edge from all to all
 * min path len==2
 * every other pathlen possible, since from any vertex
 * we can go back to begin.
 * let s(n)= number of simple pathes, ie pathes without
 * going over start vertex.
 * s(n)=2^(n-2)
 *
 * number of all pathes:
 * f(1)= 0
 * f(2)= s(2) = 3 = 2^0
 * f(3)= s(3) = 3*2 = 3*2^1
 * ...
 *
 * We do dp solution.
 * dp[i][j]=pathes ending in vertex i after j steps
 * Since abc are same i==2, i=0 is d, i=1 one of abc
 **/
void solve() {
    cini(n);

    vvi dp(2, vi(n+1));
    dp[1][1]=3;

    for(int i=2; i<=n; i++) {
        dp[0][i]=dp[1][i-1];
        dp[1][i]=(dp[1][i-1]*2 + dp[0][i-1]*3)%MOD;
    }
    cout<<dp[0][n]<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

