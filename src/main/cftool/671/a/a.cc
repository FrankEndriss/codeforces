/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * For most bottles the walking distance is twice the dist
 * to the recl bin, independend of anything else.
 * So what matters is the first bottle of the both 
 * people.
 * These should simply be the two with smallest distance.
 * --> Nah, choosing a bottle as first makes a difference of
 *  plus the person to the bottle minus the bottle to the bin.
 *
 * So, find the two nearest bottles per starting position, then
 * if the both nearest are different ones, choose them,
 * else choose the nearest to one of them, and the better one
 * of the both seconds.
 *
 * How to handle all edgecases?
 * n==1 : Use smalller person dist + dist to reclbin.
 * else
 * put up to best two bottles per person into set, then choose
 * every combination of set members to the two persons.
 *
 * Another edge case is that better sol is if one of person
 * does nothing at all, because startposition so bad.
 */
const int INF=1e18;
void solve() {
    cini(ax);
    cini(ay);
    cini(bx);
    cini(by);
    cini(tx);
    cini(ty);
    cini(n);

    pair<double,int> an1, an2, bn1, bn2;
    an1.first=-INF;
    an1.second=-1;
    an2.first=-INF;
    an2.second=-1;
    bn1.first=-INF;
    bn1.second=-1;
    bn2.first=-INF;
    bn2.second=-1;

    double sum=0.0;
    vd td(n);
    vd x(n);
    vd y(n);
    for(int i=0; i<n; i++) {
        cin>>x[i]>>y[i];

        td[i]=hypot(tx-x[i], ty-y[i]);
        sum+=td[i];

        double da=td[i]-hypot(ax-x[i], ay-y[i]);
        //cerr<<"i="<<i<<" da="<<da<<endl;
        if(an1.first<da)  {
            an2=an1;
            an1.first=da;
            an1.second=i;
        } else if(an2.first<da) {
            an2.first=da;
            an2.second=i;
        }

        da=td[i]-hypot(bx-x[i], by-y[i]);
        //cerr<<"i="<<i<<" db="<<da<<endl;
        if(bn1.first<da)  {
            bn2=bn1;
            bn1.first=da;
            bn1.second=i;
        } else if(bn2.first<da) {
            bn2.first=da;
            bn2.second=i;
        }
    }

    sum*=2;
    if(n==1) {
        double ans=sum-max(an1.first, bn1.first);
        cout<<ans<<endl;
        return;
    }

    double ans=min(sum-an1.first, sum-bn1.first); // case only one worker

    //cerr<<"opt a1="<<an1.second<<" d="<<an1.first<<endl;
    //cerr<<"opt a2="<<an2.second<<" d="<<an2.first<<endl;
    //cerr<<"opt b1="<<bn1.second<<" d="<<bn1.first<<endl;
    //cerr<<"opt b2="<<bn2.second<<" d="<<bn2.first<<endl;
    set<int> opts;
    opts.insert(an1.second);
    opts.insert(an2.second);
    opts.insert(bn1.second);
    opts.insert(bn2.second);
    assert(*opts.begin()>=0);
    vi vopts(all(opts));
    assert(vopts.size()>=2);
    for(int i : vopts) {
        for(int j : vopts) {
            if(i==j)
                continue;

            ans=min(ans, sum - td[i] - td[j]
                    +hypot(bx-x[i], by-y[i])
                    +hypot(ax-x[j], ay-y[j]));
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
