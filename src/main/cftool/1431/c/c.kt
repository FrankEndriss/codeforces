
import java.util.*

/* we need to buy the most expensive items
 * to get the most for free.
 * But we do not know how much, so we try
 * all possible amounts.
*/
fun solve() {
    val ar = readLine()!!.split(" ").map { it.toInt() }
    val n=ar[0];
    val k=ar[1];
    val p = readLine()!!.split(" ").map { it.toInt() }.toMutableList()

/* reverse */
    var i=0
    p.forEach { 
        if(i<n-1-i) {
            val aux=p[i]
            p[i]=p[n-1-i]
            p[n-1-i]=aux;
        }
        i++
    }

    var ans=0
    
    i=0
    p.forEach {
        if(i>0)
            p[i]+=p[i-1]
    
        val cnt=(i+1)/k
        if(cnt>0) {
            if(k==1)
                ans=p[i]
            else
                ans=kotlin.math.max(ans, p[i]-p[i-cnt])
        }

        i++
    }

    println("$ans");
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
}
