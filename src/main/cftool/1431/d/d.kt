
import java.util.*

/*  greedy
* sort lecturers by a[], then one end has the biggest val, other end smallest.
* if next lecture would make the smallest lecturer to use a new pen then let him
* hold the lecture, else let the biggest one hold it.
*/
fun solve() {
    val ar = readLine()!!.split(" ").map { it.toInt() }
    val n=ar[0];

    val a = readLine()!!.split(" ").map { it.toInt() }

    var aa=mutableListOf<Pair<Int,Int>>()
    var i=0
    while(i<a.size) {
        aa.add(Pair(a[i], i+1))
        i++
    }
    aa.sortBy({it.first})

    var ans=mutableListOf<Int>()
    
    var l=0;
    var r=n-1
    var cnt=0
    while(l<=r) {
        if(cnt>=aa[l].first) {
            ans.add(aa[l].second)
            l++
            cnt=1
        } else {
            ans.add(aa[r].second)
            r--
            cnt++
        }
    }

    ans.forEach {
        print("$it ");
    }

    println()
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
}
