
import java.util.*

/* brute force
* sort both teams by strength and use a offset
* So map a[0] to b[0+offs] and so on.
* One of the possible offsets will result in best solution.
*/
fun solve() {
    var ar = readLine()!!.split(" ").map { it.toInt() }
    val n=ar[0];

    val a = readLine()!!.split(" ").map { it.toInt() }
    val aa= a .mapIndexed { idx, it ->  Pair(it,idx) }.sortedBy({ it.first })
    val b = readLine()!!.split(" ").map { it.toInt() }
    val bb= b .mapIndexed { idx, it ->  Pair(it,idx) }.sortedBy({ it.first })

    var gmi=0
    var miOffs=0

    for(offs in 0..n-1) {
        var mi=1000000000
        for(i in 0..n-1) {
            val lans=kotlin.math.abs(aa[i].first-bb[(i+offs)%n].first)
            mi=kotlin.math.min(mi, lans)
        }

        if(mi>gmi) {
            gmi=mi
            miOffs=offs
        }
    }

    var ans=mutableListOf<Int>()
    while(ans.size<n)
        ans.add(0)

    for(i in 0..n-1) {
        ans[aa[i].second]=bb[(i+miOffs)%n].second
    }

    ans.forEach {
        print("${it+1} ");
    }
    println()
    
    
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
}
