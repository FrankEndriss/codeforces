
import java.util.*

fun solve() {
    val n = readLine()!!.toInt()
    val a = readLine()!!.split(" ").map { it.toLong() }.sorted()

    var ans : Long =0

    var i=0;
    a.forEach {
        val lans : Long=it*(n-i);
        ans=kotlin.math.max(ans, lans)
        i++
    }

    println("$ans");
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
}
