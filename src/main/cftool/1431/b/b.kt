
import java.util.*

/* Not sure about the rules.
* But is seems the statement implies that
* of three consecutive letters the middle one
* must be underlined.
* ...no
* A single w must be underlined, too.
* So, we must underline each w, and from each 
* sequence of v in between len/2.
*/
fun solve() {
    val s = readLine()!!

    var ans=0;
    var cnt=0;
    s.forEach {
        if(it=='w') {
            ans++
            ans+=cnt/2
            cnt=0
        } else
            cnt++
    }
    ans+=cnt/2

    println("$ans");
}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
}
