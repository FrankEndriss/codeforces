
import java.util.*

/*
*   Removing one element changes all blocks right of it.
* Note that the last block has size (n-k)%x, so it can be 
* as small as a single element.
* But we need to care for the biggest block.
* Binary search for smallest possible blocksize?
* -> yes

* The check function must take sizeofblock elements into
* the set, then while to big remove the biggest and
* insert the next.
* count removed elements.
* if all in all to much where removed, then mid is not 
* possible.
* How to implement, in c++ we would use a set or priority_queue ?
* -> there is SortedSet
*/
fun solve() {
    var ar = readLine()!!.split(" ").map { it.toInt() }
    val n=ar[0];
    val k=ar[1];
    val x=ar[2];

    val a = readLine()!!.split(" ").map { it.toLong() }

    var l=0
    var r:Long=a.sum()

    while(l+1<r) {
        val mid=(l+r)/2
        var cnt=0
        var sum=0
        var ss=SortedSet<Pair<Int,Int>>()
        for(i in 0..n-1) {

            if(ss.size==k && sum<=mid) {
                ss=SortedSet<Pair<Int,Int>>()
                sum=0
            } else if(ss.size == k && sum>mid) {
                ss=ss.headSet(ss.last())
                cnt++
// somehow add an element to a SortedSet... sic :/
            }
        }
        // TODO
    }

}

fun main(args: Array<String>) {
    var t = readLine()!!.toInt()
    while(t-->0)
        solve()
}
