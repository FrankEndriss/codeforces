/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

pii pp(int i, int j) {
    return {min(i,j), max(i,j)};
}

/* We need to count the number of times
 * each edge contributes, then assign 
 * the available numbers in that order.
 * How to find?
 * Each edge has two ends, the number of times it contributes is
 * the product of the tree size of both ends.
 * dfs()
 * We collect the size of one end simply while dfs, the other end is
 * n-oneEnd
 *
 * Available numbers are the n-2 smallest given primes, and the product of 
 * all other primes.
 */
const int MOD=1e9+7;
void solve() {
    cini(n);
    vvi adj(n);
    vector<pii> edg;
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
        edg.emplace_back(u,v);
    }


    vi cnt;
    function<int(int,int)> dfs=[&](int v, int p) {
        int ans=1;
        for(int c : adj[v]) {
            if(c!=p) {
                int val=dfs(c,v);
                cnt.push_back(val);
                ans+=val;
            }
        }

        return ans;
    };

    cini(m);
    cinai(p,m);
    while(p.size()<n-1)
        p.push_back(1);

    sort(all(p));
    for(int i=n-1; i<m; i++) {
        p[n-2]*=p[i];
        p[n-2]%=MOD;
    }

    dfs(0, -1);
    assert(cnt.size()==n-1);
    for(int i=0; i<n-1; i++)
        cnt[i]*=(n-cnt[i]);

    sort(all(cnt));

    int ans=0;
    for(int i=0; i<n-1; i++) {
        cnt[i]%=MOD;
        ans+=(p[i]*cnt[i])%MOD;
        ans%=MOD;
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
