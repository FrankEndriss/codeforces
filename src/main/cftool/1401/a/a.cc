/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * What sick mind has fun on stupid index fiddling shit problem like this one?
 *
 * abs(abs(n-b)-abs(b))==k;
 *
 * So, b can be 
 * a-(2*b)==k || (2*b)-a==k
 *
 * k-a==-(2*b)
 *
 *
 * ....shit
 *
 * 
 *
 */
void solve() {
    cini(n);
    cini(k);

    int xa=2*k;
    int ans=min(abs(n-xa), n);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
            solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
