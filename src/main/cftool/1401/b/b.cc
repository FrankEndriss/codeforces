/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We put
 * a[i]==2 and b[i]==1
 * a[i]==2 and b[i]==2
 * a[i]==0 and b[i]==2
 * a[i]==0 and b[i]==1
 * a[i]==1 and b[i]==0
 * a[i]==1 and b[i]==1
 * a[i]==1 and b[i]==2
 *
 * Sic again. I bet nearly nobody has a proof, the vast majority
 * does it with trial and error.
 */
void solve() {
    vi a(3);
    vi b(3);
    for(int i=0; i<3; i++)
        cin>>a[i];
    for(int i=0; i<3; i++)
        cin>>b[i];

    assert(a[0]+a[1]+a[2]==b[0]+b[1]+b[2]);

    int ans=0;

    int cnt=min(a[2],b[1]);
    ans=cnt*2;
    a[2]-=cnt;
    b[1]-=cnt;

    cnt=min(a[2], b[2]);
    a[2]-=cnt;
    b[2]-=cnt;

    cnt=min(a[0], b[2]);
    a[0]-=cnt;
    b[2]-=cnt;

    cnt=min(a[0], b[1]);
    a[0]-=cnt;
    b[1]-=cnt;

    cnt=min(a[1], b[1]);
    a[1]-=cnt;
    b[1]-=cnt;

    cnt=min(a[1], b[0]);
    a[1]-=cnt;
    b[0]-=cnt;

    cnt=min(a[1], b[2]);
    a[1]-=cnt;
    b[2]-=cnt;
    ans-=2*cnt;


    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
