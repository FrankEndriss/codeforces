/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Really wiered statement.
 * x can obviously be any value.
 * What is the question?
 * ->
 *  Ok, the orig array is "shifted" permutation, each values added l-1.
 *  Then we do xor with some x and result is a[].
 *
 *  Consider if lowest bit in x is set or not.
 *  We can know by number of elements if r-l+1 is odd. Else if even we do not know.
 *
 *  ...
 *  Since l=0 for D1, one of the values must be the x.
 *  Also consider that there is an element 1 in a[], so one of the values
 *  must be x^1. Can there be more such pairs?
 *  Note for 
 *  a[p[0]]==a[p[1]]^1
 *  a[p[2]]==a[p[3]]^1
 *  ...
 *  a[p[0]]==a[p[2]]^2
 *  a[p[0]]==a[p[4]]^4
 *  a[p[0]]==a[p[8]]^8
 *  ...
 *
 *  a[p[0]]==a[p[3]]^3
 *  ...
 *  a[p[0]]==a[p[r]]^r
 *
 *
 *  Of course, consider elements a[]^2 and a[]^3, the differ
 *
 *  Sort a[], then there should be two consecutive parts.
 *  x=1001
 *  a[]=
 *  9=0000
 *  8=0001
 * 10=0011
 *  1=1000
 *  0=1001
 *  3=1010
 *  2=1011
 *  5=1100
 *  4=1101
 *  7=1110
 *  6=1111
 *  if r==2^k then it is consecutive, and x can be any value, i.e.0
 *  else there is some break somewhere, and the first element after that break is 1.
 *
 *  *************************
 *  It is much more trivial:
 *  Count foreach bit the number of times it is set, prefix count.
 *  Then, simply count the bits in a[], and choose one of the available options.
 *
 *  But somehow does now work :/
 */
const int N=17;
vvi pre(1<<N+1, vi(N));
void init() {
    for(int i=0; i<(1<<N); i++)
        for(int j=0; j<N; j++) 
            pre[i+1][j]=pre[i][j]+((i>>j)&1);
}

void solve() {
    cini(l);
    cini(r);

    cinai(a,r-l+1);
    vi cnt(N);
    for(int i=0; i<r-l+1; i++) 
        for(int j=0; j<N; j++) 
            cnt[j]+=((a[i]>>j)&1);

    int ans=0;
    for(int j=0; j<N; j++) 
        if(pre[r+1][j]-pre[l][j]==cnt[j])
            ans|=(1<<j);

    cout<<ans<<endl;
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
