/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider the c[j]=n, which is the ordered permutation.
 * Consider c[j]=1, which is all permutations start with p[0]=n
 * So we know where n is.
 *
 * let p[pmax]=n
 * c[j]=2 are the shifts where p[0]>p[1]>p[2]>...<p[pmax]
 * c[j]=3 ...
 * There must not be any wholes.
 *
 * ...But we also have to consider the ordering somehow...
 * to complected :/
 */
void solve() {
    cini(n);
    cinai(c,n);

    vi f(n+1); /* f[i]=positions j of a[j]=i */
    for(int i=0; i<n; i++)
        f[c[i]]++;

    int cnt=0;
    for(int i=1; cnt<n && i<=n; i++) {
        cnt+=f[i];
        if(cnt<n && f[i]==0) {
            cout<<"NO"<<endl;
            return;
        }
    }
    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
