/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* foreach a[i]>a[i+1] it means that both numbers
 * come from same array, and number of other array
 * is bg a[i]. So all following numbers <a[i] are
 * also from first array.
 *
 * foreach a[i]<a[i+1] numbers can come from any 
 * array.
 *
 * All numbers are in same order as in input given.
 * It is just that any number is from array a or b.
 *
 * So, compress the input to blocks, and try
 * to find a distribution of the blocks on both
 * arrays so that they are same size.
 */
void solve() {
    cini(n);

    int aMin=-1;
    int cnt=0;
    vi dp;

    for(int i=0; i<2*n; i++) {
        cini(aux);
        if(aMin==-1) {
            aMin=aux;
            cnt=1;
        } else if(aMin>aux) {
            cnt++;
        } else {
            dp.push_back(cnt);
            cnt=1;
            aMin=aux;
        }
    }

    /* knapsack */
    vb dp2(n+1, false);
    dp2[0]=true;

    for(int i : dp) {
        for(int j=n; j>=i; j--) {
            if(dp2[j-i])
                dp2[j]=true;
        }
    }
    if(dp2[n])
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
