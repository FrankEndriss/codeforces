/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 *  * We create the b[] from last to first index.
 *   * foreach index if a[last] is correct then do nothing,
 *    * else if a[0]==a[last] simply flip a[0],a[last]
 *     * else first flip a[0], then a[0],a[last]
 *      *
 *       * So, whenever we decrement last, the prefix before it
 *        * is in flipped state or not.
 *         * if flipped, the last element is the first, else
 *          * the last is the last.
 *           * So we cut the first or last.
 *            *
 *             * ...this index fiddling problems are really hard for me...idk :/
 *              */
void solve() {
    cini(n);
    cins(a);
    cins(b);

    vi ans;
    pii seg= {0,n-1};
    int swp=0;

    for(int i=n-1; i>0; i--) {
        int idx=seg.second;
        if(swp)
            idx=seg.first;

        if((a[idx]^swp)!=b[i]) {
            int idxF=seg.first;
            if(swp)
                idxF=seg.second;

            if((a[idxF]^swp)==b[i])
                ans.push_back(1);

            ans.push_back(i+1);
            swp=!swp;

            if(!swp)
                seg.second--;
            else
                seg.first++;

        } else {
            if(!swp)
                seg.second--;
            else
                seg.first++;
        }
    }
    if((a[seg.first]^swp) !=b[0])
        ans.push_back(1);

    cout<<ans.size()<<" ";
    for(int k : ans)
        cout<<k<<" ";
    cout<<endl;
}

signed main() {
    cini(t)
    while(t--)
        solve();
}
