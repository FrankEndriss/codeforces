/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A rotation by 1 should do the job, since then we need n-1 swaps.
 * What about indistinct elements?
 * Move each element to an position different from previous. So rotate
 * by max(freq) positions. Note that this also works if freq>n/2.
 * ...no.
 * Instead, foreach element, find next position of an other element.
 * ...
 * Not so simple.
 * Actually we want to use the postions of the most frequent elements
 * first. Because then they are most likely all moved away.
 *
 * Use some two pointer construct on a set.
 * Think again, how to construct this most likely eulerian path
 * without moving any element to a position with same element?
 * Foreach element, we want to move it to a position
 * of the most frequent other element. That way if possible, all are
 * moved to such position.
 * How to implement?
 * Foreach element, find most frequent next element.
 * Why this so complecated?
 */

void solve() {
    cini(n);
    cinai(a,n);

    vvi pos(n+1);
    for(int i=0; i<n; i++)
        pos[a[i]].push_back(i);

    using t3=tuple<int,int,int,int>;
    set<t3> nextpos0;   /* -freq,value,idx */

    for(int i=1; i<=n; i++)
        if(pos[i].size()>0) {
            int sz=(int)pos[i].size();
            nextpos0.emplace(-sz, 0, i, 0);
        }

    set<t3> nextpos;
    int cnt=0;
    for(auto [f,ord,i,idx] : nextpos0)
        nextpos.emplace(f,cnt++,i,idx);



    vi ans(n);

    auto it=nextpos.begin();
    auto [f0,ord0,val0,idx0]=*it;
    nextpos.erase(it);
    int pos0=pos[val0][idx0];
    int val00=val0; /* most frequent element */
    while(nextpos.size()) {
        it=nextpos.begin();

        auto [f2,ord2,val2,idx2]=*it;
        nextpos.erase(it);
        ans[pos[val2][idx2]]=val0;

        if(f0<-1)
            nextpos.emplace(f0+1, ord0, val0, idx0+1);

        f0=f2;
        val0=val2;
        idx0=idx2;
        ord0=ord2;
    }
    ans[pos0]=val0;

    for(int i=0; i<n; i++) {
        if(ans[i]==0) 
            ans[i]=val00;
    }

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
