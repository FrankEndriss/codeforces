/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Cylcle to the left one position
 * How?
 * Consider no pairs in a, then yes if a==b
 * Else pairs in a[i],a[j].
 * Rotating makes i=j+1, so no further rotation of this pair posible.
 *
 * We can find pairs of a[i]==a[i+1] in b[], that do not exist in a[].
 *
 * Consider going from left to right.
 * a[0]==b[0]
 *
 * We would somehow go from outermost to innermost...somehow :/
 *
 * Observe that the outermost rotation created a b[i]==b[i+1] that
 * does not exist in a[].
 * Search it, undo the rotation, then repeat.
 * But note that we actually cannot undo the rotation since TLE :/
 * How to implement that check anyways?
 *
 * Note that if there is a block of same values, and that rotates with
 * the next block of same values, it does not matter which elements
 * of the two blocks where used. In result allways
 * -one element from left block goes to right block, no other change
 *
 * So, consider only relative order of blocks in b[], it must be a subseq
 * of the blocks in a[].
 * ...no, more complecated.
 * bb[] must be a postfix of aa[], but we can jump over allready seen elements in aa[]
 *
 * How is this wrong???
 * -> Consider Input
 * 4
 * 2 1 2 2
 * 2 2 1 2
 * This will output YES but should NO :/
 * So we actually need to count elements, implement "backwards" simulation.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    vi aa;
    for(int i=0; i<n; i++)
        if(i==0 || a[i]!=a[i-1])
            aa.push_back(a[i]);

    vi bb;
    for(int i=0; i<n; i++)
        if(i==0 || b[i]!=b[i-1])
            bb.push_back(b[i]);

    /* check if bb is postfix "with jumps" of aa */
    //int idxb=(int)bb.size()-1;
    vb vis(n+1);
    while(aa.size()) {
        if(bb.size() && aa.back()==bb.back()) {
            vis[aa.back()]=true;
            bb.pop_back();
        }

        if(!vis[aa.back()]) {
            cout<<"NO"<<endl;
            return;
        }
        aa.pop_back();
    }

    if(bb.size()==0)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
