/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int ask(int w) {
    cout<<"? "<<w<<endl;
    cini(h);
    return h;
}

/**
 * First, find first by binaray search sum of all all wordlengths.
 * then try splitting by 2, 3, 4,...
 *
 * How to consider the gaps?
 */
void solve() {
    cini(n);

    int CNT=n+30;
    int l=0; 
    int r=1e9;
    while(l+1<r) {
        int mid=(l+r)/2;
        CNT--;
        int h=ask(mid);
        if(h!=1)
            l=mid;
        else
            r=mid;
    }

    const int len=r;

    /* now len is sum size words +n-1 
     * Consider one long string with the spaced between the words.
     * If the length is odd, and we split this string in the middle,
     * the both resulting strings witll be x/2 (rounded down), since
     * we got rid of one space.
     *
     * Same goes for split at two positions. Since the min possible 
     * max wordlen== x-(n-1)/n, n tries is enough.
     **/

    int ans=len;
    for(int i=0; i<CNT; i++) {
        const int parts=i+2;
        const int gaps=i+1;
        int h=ask(len/parts);
        if(h==0)
            break;
        else
            ans=min(ans, h*(len/parts));
    }
    cout<<"! "<<ans<<endl;
}

signed main() {
    solve();
}
