/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* First (Mono) wants to make diff.
 * Second wants to make same.
 *
 * Both halfs starts at a value and have some k "?"
 *
 * First tries to make the diff as big as possible, by 
 * adding the biggest num (9) to to bigger sum, or 0
 * to the less sum.
 * Second tries to make the diff as small as possible,
 * by adding 0 to the bigger one, and 9 or less
 * to the lower one.
 *
 * Both halfs have current val cv and a max reachable val, 
 * which is cv+9*k[i]
 *
 * Once these intervals do not intersect first wins.
 *
 * First can raise the lower bound by ((k[i]+1)/2)*9
 * Or can decrease the upper bound by ((k[i]+1)/2)*9
 * On both halfs separate. If this makes the intervals non intersecting,
 * then first will win.
 *
 * *************
 * Case work:
 * If number of '?' in one half are even second can make the sum stay same.
 * If number of '?' in one half are odd, first can increas by up to 9.
 */
void solve() {
    cini(n);
    cins(s);

    vi sum(2);
    for(int i=0; i<n; i++) {
        if(s[i]!='?')
            sum[i<n/2]+=(s[i]-'0')*2;
        else
            sum[i<n/2]+=9;
    }

    if(sum[0]!=sum[1]) 
        cout<<"Monocarp"<<endl;
    else
        cout<<"Bicarp"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
