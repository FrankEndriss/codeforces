/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** We can/should allways use the operation on the
 * bigger of the two values, do that greedy and count.
 * ....No, we can also reorder the numbers.
 *
 * 1. remove pairs that are same
 * 2. do one operation on all other numbers >=10
 * 3. remove pairs that are same
 * 4. do one operation on all other numbers >1
 */
void solve() {
    cini(n);

    //cerr<<"n="<<n<<endl;

    multiset<int> a;
    multiset<int> b;
    for(int i=0; i<n; i++) {
        cini(aux);
        a.insert(aux);
    }
    for(int i=0; i<n; i++) {
        cini(aux);
        b.insert(aux);
    }

    function<int(int)> f=[](int i) {
        int cnt=0;
        while(i) {
            cnt++;
            i/=10;
        }
        return cnt;
    };

    int ans=0;
    /* remove allready same pairs */
    for(auto it=a.begin(); it!=a.end(); ) {
        auto itB=b.find(*it);
        if(itB!=b.end()) {
            it=a.erase(it);
            b.erase(itB);
        }

        if(it==a.end())
            break;
        else
            it++;
    }

    while(a.size()>0 && *a.rbegin()>=10) {
        auto it=a.end();
        it--;
        int val=f(*it);
        //cerr<<"op A: "<<*it<<" -> "<<val<<endl;
        a.erase(it);
        a.insert(val);
        ans++;
    }
    while(b.size()>0 && *b.rbegin()>=10) {
        auto it=b.end();
        it--;
        int val=f(*it);
        //cerr<<"op B: "<<*it<<" -> "<<val<<endl;
        b.erase(it);
        b.insert(val);
        ans++;
    }

    for(auto it=a.begin(); it!=a.end(); ) {
        auto itB=b.find(*it);
        if(itB!=b.end()) {
            it=a.erase(it);
            b.erase(itB);
        } else 
            it++;
    }

    for(int val : a)
        if(val>1)
            ans++;
    for(int val : b)
        if(val>1)
            ans++;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
