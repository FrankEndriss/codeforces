/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Sort the complecated statement...
 * The pepper has not price...so why the different shops?
 * -> Because of bag sizes.
 *
 * In each shop, it is possible to by a certain amount of
 * red pepper, so that the number adds up to n packages.
 * From all these red/black combinations we want to choose
 * the one with best outcome.
 *
 * Calc the outcome by sort the ab[i] by diff a[i]=b[i]
 * All positive want a[] pepper, all negative b[] pepper,
 * outcome is the prefix sum.
 *
 * We cannot iterate all combinations for all shops.
 * so find the number of positive diffs, then 
 * find the two nearest possible numbers of red peppers.
 * Iterate the lcm(x[j],y[j])
 *
 * ...
 * not sure about lcm...to tired, this one no fun :/
 *
 * ***
 * That lcm is wrong.
 * Consider n=8, and x=5, y=3.
 * So we have to buy 5 a[] pepper, and 3 b[] pepper.
 * Consider n=11, how to find x=5, y=6?
 * Is it somehow diophantine equations or something like that?
 * Or can we simply iterate somehow, because...some sqrt blah, blah?
 * boring number theory...
 */
void solve() {
    cini(n);
    vector<pair<int,pii>> ab(n);
    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        ab[i].first=b-a;
        ab[i].second.first=a;
        ab[i].second.second=b;
    }
    sort(all(ab));

    vi preA(n+1);
    vi preB(n+1);
    int mid=n-1;    /* first elements that wants b */
    for(int i=0; i<n; i++) {
        preA[i+1]=preA[i]-ab[i].first;
        preB[i+1]=preB[i]-ab[n-1-i].first;
        if(ab[i].first>=0)
            mid=min(mid, i);
    }

    cini(m);
    for(int i=0; i<m; i++) {
        cini(x);    /* a[] pepper packages */
        cini(y);    /* b[] pepper packages */

        int lc=lcm(x,y);
        if(n%lc!=0) {
            cout<<-1<<endl;
            continue;
        }

        int v0=mid/lc;
        int v1=v0+lc;

        int ans=preA[v0*lc]+preB[n-v0*lc];
        if(v1*lc<=n)
            ans=max(ans, preA[v1*lc]+preB[n-v1*lc]);

        cout<<ans<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
