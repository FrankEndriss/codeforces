/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Play optimally by greedy take the smaller letter. 
 * In case of tie, take the one that let the other player
 * the bigger letter. If that also tie dont care.
 * Simulate, casework.
 */
void solve() {
    cins(s);
    vs a(2);

    int l=0;

    int p=0;
    while(l<s.size()) {
        if(s[l]<s.back()) {
            a[p]+=s[l];
            l++;
        } else if(s[l]>s.back()) {
            a[p]+=s.back();
            s.pop_back();
        } else if(s.size()>2) {
            if(s[l+1]<s[l]) {
                a[p]+=s.back();
                s.pop_back();
            } else {
                a[p]+=s[l];
                l++;
            }
        }
        p=1-p;
    }

    reverse(all(a[0]));
    reverse(all(a[1]));
    if(a[0]<a[1])
        cout<<"Alice"<<endl;
    else if(a[0]>a[1])
        cout<<"Bob"<<endl;
    else
        cout<<"Draw"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
