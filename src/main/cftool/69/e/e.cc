/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

set<int> gans;
map<int,int> f;
vi a;

inline void moremove(int idx) {
    if(--f[a[idx]]==1)
        gans.insert(a[idx]);
    else
        gans.erase(a[idx]);
}

inline void moadd(int idx) {
    if(++f[a[idx]]==1)
        gans.insert(a[idx]);
    else
        gans.erase(a[idx]);
}

void moget_answer() {
    if(gans.size()==0)
        cout<<"Nothing"<<endl;
    else
        cout<<*gans.rbegin()<<endl;
}

/**
 * We want to solve this with Mo.
 * ...No, its not queries. It is just subarray.
 * Like queries are sorted prior.
 * So simple go from left to right, maintaining 
 * freq array and set of values in freq array.
 */
void solve() {
    cini(n);
    cini(k);

    for(int i=0; i<n; i++) {
        cini(aux);
        a.push_back(aux);
    }

    for(int i=0; i<k; i++) 
        moadd(i);

    for(int i=k; i<n; i++) {
        moget_answer();
        moremove(i-k);
        moadd(i);
    }
    moget_answer();

}

signed main() {
        solve();
}
