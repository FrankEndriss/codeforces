/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want a 1 in last position,
 * and a 1 in first position,
 * else dont care.
 *
 * Edgecase: there are less than two '1'
 * Edgecase: Move is possible only in one direction caused 
 *  by limit k.
 */
void solve() {
    int n,k;
    cin>>n>>k;
    string s;
    cin>>s;

    int c=0;
    for(int i=0; i<n; i++)
        c+=(s[i]=='1');

    if(c==0) {
        cout<<0<<endl;
        return;
    }

    int cnt1=0;
    for(int i=n-1; i>=0; i--) {
        if(s[i]=='0')
            cnt1++;
        else
            break;
    }
    if(cnt1>0 && cnt1<=k) {
        swap(s[n-1], s[n-cnt1-1]);
        k-=cnt1;
    }

    if(c-(s[n-1]-'0')>0)  {

        int cnt2=0;
        for(int i=0; i+1<n; i++) {
            if(s[i]=='0')
                cnt2++;
            else
                break;
        }

        if(cnt2>0 && cnt2<=k)
            swap(s[0], s[cnt2]);
    }

    int ans=0;
    for(int i=0; i+1<n; i++)
        ans+=(s[i]-'0')*10 + (s[i+1]-'0');

    if(n==1)
        ans=s[0]-'0';

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
