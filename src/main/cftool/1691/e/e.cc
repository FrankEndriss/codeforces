/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * O(n^2) is trivial using dsu.
 *
 * Go left to right, maintain the elements of one component
 * in one set (maybe separated by color).
 * Foreach set also maintain the rightmost position,
 * sort sets by rightmost position.
 *
 * Then, foreach new segment, find the leftmost of other color,
 * and merge all of them in the set right from the found one.
 * Also remove them from the set except the rightmost one.
 *
 * Not sure if correct :/
 */
void solve() {
    cini(n);
    using t3=tuple<int,int,int>;
    vector<t3> a;
    for(int i=0; i<n; i++) {
        cini(c);
        cini(l);
        cini(r);
        a.emplace_back(r,l,c);
    }

    sort(all(a));

    dsu d(n);
    vector<set<t3>> s(2);   /* <r,l,id> */
    for(int i=0; i<n; i++) {
        auto [r,l,c]=a[i];

        t3 x={l,0,0};
        auto it=s[1-c].lower_bound(x);
        t3 last={-1,-1,-1};
        while(it!=s[1-c].end()) {
            auto [r1,l1,i1]=*it;
            d.merge(i,i1);
            last={r1,l1,i1};
            it=s[1-c].erase(it);
        }
        if(get<0>(last)>=0)
            s[1-c].insert(last);

        /* how can we know if this new segment is 
         * allready connected to some previous
         * ones in the set.
         */
        s[c].emplace(r,l,i);
    }
    int ans=d.groups().size();
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
