/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * To make segments of len=2 possible,
 * every second number must be <=0.
 * Also, for 3 numbers where two are positive
 * (the middle is neg), the absolute value of the
 * neg must be bgeq the smaller one of the two positive.
 *
 * Consider the prefix sums psum[], and the prefix max pmax[].
 * pmax[i]-psum[i]>=0
 * Is this enough to check? Why?
 * ...no, dont think so.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi pmax(n);
    vi psum(n);
    vi pmaxd(n);
    pmax[0]=a[0];
    psum[0]=a[0];
    pmaxd[0]=a[0];

    for(int i=1; i<n; i++) {
        pmax[i]=max(pmax[i-1], a[i]);
        psum[i]=psum[i-1]+a[i];
        if(pmax[i]-psum[i] < 0) {
            cout<<"NO"<<endl;
            return;
        }
        pmaxd[i]=max(pmaxd[i-1], pmax[i]-psum[i]);
        if(pmax[i]-psum[i]-pmaxd[i] < 0) {
            cout<<"NO"<<endl;
            return;
        }
    }
    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
