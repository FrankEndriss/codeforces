/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* max is simpler,
 * we are left with a 1X1 tower of n elements.
 *
 * Min should be at most equal possible distribution.
 * n/5 X 2n/5 * 2n/5
 *
 * Note that n must be multiple of 3 ints.
 * Brute force all triples.
 */
void solve() {
    cini(n);

    int ma=n+1;
    ma*=3;
    ma*=3;

    int mi=ma;
    for(int a=1; a*a*a<=n; a++) {
        for(int b=a; a*b*b<=n; b++) {
            if(n%(a*b)==0) {
                int c=n/(a*b);
                vi abc={ a, b, c};
                for(int i=0; i<3; i++) {
                    mi=min(mi, (abc[0]+2)*(abc[1]+2)*(abc[2]+1));
                    rotate(abc.begin(), abc.begin()+1, abc.end());
                }
            }
        }
    }
    cout<<mi-n<<" "<<ma-n<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
