/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* 
 * The diff between two qudratic numbers is odd.
 * x == ((x+1)/2)^2 - (x/2)^2
 *
 * if n is even we can divide by two as often as needed.
 */
const int N=1e5+1;
void solve() {
    cini(n);

    if(n<3) {
        cout<<-1<<endl;
        return;
    }
    if(n==3) {
        cout<<"4 5"<<endl;
        return;
    }

    int mul=1;
    while(n%2==0) {
        if(n==4) {
            cout<<5*mul<<" "<<3*mul<<endl;
            return;
        }
        mul*=2;
        n/=2;
    }

    n*=n;
    int a=((n+1)/2);
    int b=n/2;
    cout<<b*mul<<" "<<a*mul<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

