/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* greedy ... does not work for some reason :/
 *
 * try dp[i][pl][second]=min number of skip points to kill ith boss by pl with second move
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n); /* boss types */

    if(n<=3) {
        cout<<a[0]<<endl;
        return;
    }

    vvvi dp(n, vvi(2, vi(2, INF)));
    dp[0][0][0]=a[0];
    dp[1][0][1]=a[0]+a[1];
    dp[1][1][0]=dp[0][0][0];

    for(int i=2; i<n; i++) {
        dp[i][0][0]=min(dp[i-1][1][0]+a[i], dp[i-1][1][1]+a[i]);
        dp[i][0][1]=dp[i-1][0][0]+a[i];

        dp[i][1][0]=min(dp[i-1][0][0], dp[i-1][0][1]);
        dp[i][1][1]=dp[i-1][1][0];
    }

    int ans=min(dp[n-1][0][0], dp[n-1][0][1]);
    ans=min(ans, dp[n-1][1][0]);
    ans=min(ans, dp[n-1][1][1]);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
