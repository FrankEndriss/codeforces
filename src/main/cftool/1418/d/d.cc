/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Moving some piles into one pile needs
 * x[0]-x[n-1] steps.
 * So, creating 2 piles in min moves means we need to move
 * x[n-1] - x[0] - biggest_gap_between_any_2_piles
 *
 * So, we need to maintain a list of the gaps, and query max of them.
 * Also min/max of all elements.
 *
 * How to maintain the gaps?
 * Simply kth order statistics?
 *
 * We can use a set to maintain them, 
 * together with the gap to the next pile (left or) right of it.
 *
 * Also, use a second set to maintain the pairs sorted by gapsize.
 *
 * ***
 * Note that it would be simpler to use a set<int> of positions,
 * and multiset<int> for gaps, instead of pairs for both.
 * see tutorial
 */
void solve() {
    map<int,int> pos;   /* <pos,gapLeft> */
    set<pii> gap;   /* <gapLeft,pos> */

    cini(n);
    cini(q);

    cinai(p, n);
    sort(all(p));

    pos[p[0]]=0;

    for(int i=1; i<n; i++) {
        const int g=p[i]-p[i-1];
        pos[p[i]]=g;
        gap.emplace(g, p[i]);
    }

    for(int i=0; i<=q; i++) {
        const int mi=pos.size()>0?pos.begin()->first:0;
        const int ma=pos.size()>0?pos.rbegin()->first:0;
        const int g=gap.size()>0?gap.rbegin()->first:0;
        cout<<ma-mi-g<<endl;

        if(i==q)
            break;

        cini(t);
        cini(x);
        if(t==0) {  /* remove. 
                       Two cases: There is an element right of the removed one, or
                       there is none.
                    */
            auto it=pos.lower_bound(x);
            assert(it!=pos.end());
            const int gg=it->second;
            gap.erase({it->second, it->first});
            pos.erase(it);

            it=pos.lower_bound(x);
            if(it!=pos.end())  {
                gap.erase({it->second, it->first});
                it->second+=gg;
                if(it!=pos.begin())
                    gap.emplace(it->second, it->first);
            }
        } else if(t==1) {   /* insert */
            auto it=pos.lower_bound(x);
            pii R={ -1, 0 };
            if(it!=pos.end()) 
                R=*it;

            pii L={ -1, 0 };
            if(it!=pos.begin()) {
                it--;
                L=*it;
            }

            pii ins={ x, 0 };
            if(L.first>0)
                ins.second=ins.first-L.first;

            if(R.first>0) {
                pos[R.first]=R.first-ins.first;
                gap.erase({R.second,R.first});
                gap.emplace(R.first-ins.first, R.first);
            }

            pos[ins.first]=ins.second;

            if(ins.second>0)
                gap.emplace(ins.second, ins.first);

        } else 
            assert(false);
    }
}

signed main() {
    solve();
}
