/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to find number of pretty subarrays of s.
 * O(n^2) seems to be ok.
 *
 * We can calc bal of all substrings.
 * How to count '?' in bal?
 * To avoid negative balance we want to replace '?'  by '('.
 * But we need to make balance 0, not positive.
 * So question is, can we replace the '?' in a way that
 * all positions bal[0..i-1]>=0 and bal[i]==0
 * If we count bal without '?', and it gets negative, we must
 * put '(' before that position.
 *
 * Loop over start positions. For each
 * Loop over end positions. For each
 * assume the '?' are substituted to create
 * a balanced count, (ie x '(' and y ')'), so
 * we subtitute the _first_ x '?' by '(', and
 * the others by ')'.
 * Now we need to quickcheck if this creates a
 * cbs. How without O(n^3)?
 * *************************
 * The substitution is simple:
 * We allways substitute so that bal is zero in the end,
 * and just use the _first_ x '(', and the last y ')'.
 * The question is, does this create a vbs?
 *
 * Solution:
 * We need to know the number of '?' in a subarray beforehand,
 * than we can calc x and y, and simply count the bal at
 * every positon! But this is O(n^3) :/
 *
 * **************************
 * Lets iterate possible substring lengths, foreach
 * iterate start positions, and foreach
 *  Check first substring.
 *  Move all other substrings by one position.
 *  Maintain bal values in segment tree.
 *  Update bal values after each move, which requires range updates,
 *  use lazy segment tree.
 *  Select min bal from tree, if zero increment ans.
 * endloops
 * **************************
 * See tutorial, simpler:
 * Iterate from all start positions:
 * Whenever the number of '?' is bigger than balance substitute
 * one '?' by a '(', ie qcount--, bal++.
 * With this, simply count and check for positive balance.
 */
void solve() {
    cins(s);

    int ans=0;
    for(size_t i=0; i<s.size(); i++) {
        int bal=0;
        int q=0;
        for(size_t j=i; j<s.size(); j++) {
            if(s[j]=='(')
                bal++;
            else if(s[j]==')')
                bal--;
            else if(s[j]=='?')      
                q++;
            else
                assert(false);

            if(bal<0) {
                if(q>0) {
                    q--;
                    bal++;
                } else
                    break;
            }

            if(q>bal) {
                q--;
                bal++;
            }

            if(q>=bal && (j-i+1)%2==0)
                ans++;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
