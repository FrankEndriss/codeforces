/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/* @return len of border pre/posfix */
int calculate_border(string& s) {
    int i = 1;
    int j = -1;
    int n = (int)s.size();

    vi border(s.size());
    border[0] = -1;
    while(i < n) {
        while(j >= 0 && s[i] != s[j+1])
            j = border[j];
        if (s[i] == s[j+1])
            j++;
        border[i++] = j;
    }
    return border.back()+1;
}

/* We must start with t,
 * then search for longest prefix of
 * t wich is a postfix of t, too.
 * Repeat with that part as often
 * as possible.
 *
 * How to find if a prefix is a postfix, too?
 */
void solve() {
    cins(s);
    cins(t);

//cerr<<"before fixlen, t="<<t<<endl;
    int fixlen=calculate_border(t);
//cerr<<"after fixlen="<<fixlen<<endl;
//for(int i=0; i<t.size(); i++)
    //cerr<<border[i]<<" ";
//cerr<<endl;

    vi cnt(2);
    for(char c : s)
        cnt[c-'0']++;

    size_t i=0;
    for(; i<min(s.size(), t.size()); i++) { /* copy t */
        int c=t[i]-'0';
        if(cnt[c]) {
            s[i]=c+'0';
            cnt[c]--;
        } else {
            if(cnt[0]) {
                s[i]='0';
                cnt[0]--;
            } else {
                s[i]='1';
                cnt[1]--;
            }
        }
    }

    assert(fixlen<(int)t.size());
    int cpsize=t.size()-fixlen;
    for(size_t ii=0; ii+i<s.size(); ii++) { /* copy everything after border */
        int c=t[fixlen+ii%cpsize]-'0';
        if(cnt[c]) {
            s[i+ii]=c+'0';
            cnt[c]--;
        } else {
            if(cnt[0]) {
                s[i+ii]='0';
                cnt[0]--;
            } else {
                s[i+ii]='1';
                cnt[1]--;
            }
        }
    }
    cout<<s<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
