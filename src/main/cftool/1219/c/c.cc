/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* three cases:
 * 1. s.size() is not divisable by l, then ans=100..00100...00 l times
 * 2. the first l digits of s can be repeated size()/l times,
 *    then ans= s[0,l-1] l times
 * 3. else s[0,l-1]+1 l times
 * 3.a if A==9999...999 then we need to construct 100100100...100 with s.size()+l digits.
 */
void solve() {
    cini(l);
    cins(s);

    if(s.size()%l!=0) { /* case 1 */
        string ans0="1";
        for(int i=1; i<l; i++)
            ans0+='0';
        int len=0;
        while(len<s.size()) {
            cout<<ans0;
            len+=l;
        }
        cout<<endl;
        return;
    }

    bool ok=false;
    for(int i=0; i<s.size(); i++) {
        if(s[i]>s[i%l]){
            ok=false;
            break;
        } else if(s[i]<s[i%l]) {
            ok=true;
            break;
        }
    }
    if(ok) {    /* case 2 */
        for(int i=0; i<s.size()/l; i++) 
            cout<<s.substr(0,l);
        return;
    }

    string ss=s.substr(0, l);
    int carry=0;
    if(ss.back()=='9') {
        ss.back()='0';
        carry=1;
    } else {
        ss.back()++;
    }
    int idx=ss.size();
    idx-=2;
    while(carry && idx>=0) {
        if(ss[idx]=='9')
            ss[idx]='0';
        else {
            ss[idx]++;
            carry=0;
        }
        idx--;
    }
    if(carry==0) {   /* case 3 */
        for(int i=0; i<s.size()/l; i++) 
            cout<<ss;
        cout<<endl;
    } else {    /* case 3a */
        int len=s.size()+l;
        string sss="1";
        while(sss.size()<l)
            sss+='0';
        for(int i=0; i<len/l; i++)
            cout<<sss;
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
