/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* We would try brute force, maybe that works...
 * Unsolvable, see tutorial.
 *
 * On every level, we want to make the number
 * equal to (lvl*(lvl+1))^2
 * because it is a square of a multiple of lvl+1, and funny thing:
 * Since it is a multple of lvl and previous number is, too, all
 * constrainst are fullfilled.
 *
 *
 */
void solve() {
    cini(n);
    
    cout<<2<<endl;
    for(int lvl=2; lvl<=n; lvl++) {
        cout<<(((lvl+1)*(lvl+1)*lvl)-lvl+1)<<endl;
    }
}

signed main() {
    solve();
}

