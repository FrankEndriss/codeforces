/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The balance of citizen i after all events
 * is the maximum of the last t=1 event for
 * that citizen, or any t=2 event after that last
 * t=1 event.
 *
 * So store that balance of the last t=1 event and
 * the time when that was.
 * Store also all t=2 event, so we can say from any
 * point of time how much was the highest t=2 event
 * after that point of time.
 * Ans is the max of both values for each person.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi at(n);
    
    cini(q);
    vi postx(q);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(p);
            cini(x);
            a[p-1]=x;
            at[p-1]=i;
        } else if(t==2) {
            cini(x);
            postx[i]=x;
        } else
            assert(false);
    }

    for(int i=q-2; i>=0; i--)
        postx[i]=max(postx[i+1], postx[i]);

    for(int i=0; i<n; i++)
        cout<<max(a[i], postx[at[i]])<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
