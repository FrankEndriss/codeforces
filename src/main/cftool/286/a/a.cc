/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

//#define endl "\n"

/**
 * Consider p[1]==1         | 1 based index
 * p[p[0]]=n    -> contradiction
 *
 * p[x]=1
 * p[p[x]]=n
 *
 * So, simpley
 * 5 4 3 2 1
 *
 * Why "2 1" is no solution?
 * p[p[1]]=1 == 2-1+1
 * p[p[2]]=2 == 2-2+1
 *
 * Case n=3
 * p[p[1]]= 3-1+1 == 3
 * So, the 3 must be at position pointed to by p[1],
 * So, if p[1]==1, 3 must be at 1...not possible
 * if p[1]==3, 3 must be at 3, but is not...not possible
 * if p[1]==2, 3 must be at 2
 * p[2]=3
 * -> { 2, 3, 1 }
 *
 *  p[p[1]]=3
 *  p[p[2]]=1 -> not ok
 *  p[p[3]]=2 -> not ok
 *
 *  ...
 *  so 
 *  p[1] must point to the position with value == n
 *  p[2] must point to the position with value == n-1
 *  ...
 *  p[n] must point to the position with value == 1
 *
 *  Consider p[1]==2, then
 *  p[2]==n, then
 *  p[n]==n-1, then
 *  p[n-1]=1, then
 *  p[1]=2 -> ok
 *
 *  p[3]=4, then
 *  p[4]=n-3, then
 *  p[
 *
 *  It seems n%4==0 work, else do not.
 *  Edgecase n=1
 */
void solve() {
    cini(n);

    vi ans(n);
    ans[n/2]=(n+1)/2;
    if(n%4==0 || n%4==1) {
        int l=0;
        int r=n-1;
        while(l+2<r) {
            ans[l]=l+1+1;
            ans[l+1]=r+1;
            ans[r]=r-1+1;
            ans[r-1]=l+1;
            l+=2;
            r-=2;
        }
    } else {
        cout<<-1<<endl;
        return;
    }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<" ";

    cout<<endl;

    return;

}

signed main() {
    solve();
    exit(0);
}
