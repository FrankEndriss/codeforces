/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* bfs works in rounds.
 * Tricky trick:
 * Sort the adj matrix entries by the order
 * given in a[].
 * Then bfs must create same list as a[].
 */
void solve() {
    cini(n);

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vi pos(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        aux--;
        pos[aux]=i;
    }
    
    for(int i=0; i<n; i++) {
        sort(all(adj[i]), [&](int i1, int i2) {
            return pos[i1]<pos[i2];
        });
    }

    queue<int> q;   
    q.push(0);
    vb vis(n);
    vis[0]=true;
    int i=0;
    while(q.size()) {
        int v=q.front();
        q.pop();
        if(pos[v]!=i) {
            cout<<"No"<<endl;
            return;
        }
        i++;

        for(int c : adj[v])
            if(!vis[c]) {
                q.push(c);
                vis[c]=true;
            }
    }

    cout<<"Yes"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
