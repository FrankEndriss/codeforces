/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=998244353;

/* strange rules
 * a[0]<=a[i]
 * a[n-2]>=a[n-1]
 * a[i]<=max(a[i-1], a[i+1]);
 *
 * example
 * 1 5 5 4 1 2 3 3 1 7 8 8
 *
 * dp, brute force
 * for each position find the number of possibilities.
 *
 * ***
 * Rule:
 * Initial direction is up.
 * Whenever two consecutive numbers are same dir can change to down.
 * At every position dir can change to up.
 *
 * let dp[k][i]=number of posi if
 * k==0 dir is down
 * k==1 last two numbers are same
 * k==2 dir is up
 * and the last number is i
 */
#define DOWN 0
#define EQ 1
#define UP 2

void solve() {
    cini(n);
    cinai(a,n);

    vvi dp(3, vi(201));
    
    if(a[0]==-1) {
        for(int j=1; j<=200; j++) 
            dp[UP][j]=1;
    } else {
        dp[UP][a[0]]=1;
    }

    for(int i=1; i<n; i++) {
        vvi dpx(3, vi(201));
        
        if(a[i]==-1) {
            int sumSmaller=0;

            dpx[DOWN][200]=0;
            for(int j=199; j>=1; j--)
                dpx[DOWN][j]=(dpx[DOWN][j+1] + dp[EQ][j+1]+dp[DOWN][j+1])%MOD;
            
        
            for(int j=1; j<=200; j++) {
                /* j is same number as number before */
                dpx[EQ][j]= (dp[UP][j]+dp[EQ][j]+dp[DOWN][j])%MOD;

                dpx[UP][j]=sumSmaller; /* sum of posi that prev number is smaller number */

                sumSmaller+=dp[DOWN][j]+dp[EQ][j]+dp[UP][j];
                sumSmaller%=MOD;
            }
        } else {
            dpx[EQ][a[i]]= (dp[UP][a[i]]+dp[EQ][a[i]]+dp[DOWN][a[i]])%MOD;
            for(int j=200; j>a[i]; j--) 
                dpx[DOWN][a[i]]=(dpx[DOWN][a[i]]+dp[DOWN][j]+dp[EQ][j])%MOD;
            for(int j=1; j<a[i]; j++) 
                dpx[UP][a[i]]=(dpx[UP][a[i]]+dp[DOWN][j]+dp[UP][j]+dp[EQ][j])%MOD;
        }
        dp.swap(dpx);
    }

    int ans=0;
    for(int i=1; i<=200; i++) 
        ans=(ans+dp[DOWN][i]+dp[EQ][i])%MOD;

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
