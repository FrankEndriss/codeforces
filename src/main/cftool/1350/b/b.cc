#include<bits/stdc++.h>
using namespace std;

#define SCD(t) scanf("%d",&t)
#define SCLD(t) scanf("%ld",&t)
#define SCLLD(t) scanf("%lld",&t)
#define SCC(t) scanf("%c",&t)
#define SCS(t) scanf("%s",t)
#define SCF(t) scanf("%f",&t)
#define SCLF(t) scanf("%lf",&t)
#define MEM(a, b) memset(a, (b), sizeof(a))
#define FOR(i, j, k, in) for (int i=j ; i<k ; i+=in)
#define RFOR(i, j, k, in) for (int i=j ; i>=k ; i-=in)
#define FO(i, j) FOR(i, 0, j, 1)
#define RFO(i, j) RFOR(i, j, 0, 1)
#define all(cont) cont.begin(), cont.end()
#define rall(cont) cont.end(), cont.begin()
#define FOREACH(it, l) for (auto it = l.begin(); it != l.end(); it++)
#define IN(A, B, C) assert( B <= A && A <= C)
#define MP make_pair
#define PB push_back
#define INF (int)1e9
#define EPS 1e-9
#define PI 3.1415926535897932384626433832795
#define MOD 1000000007
typedef pair<int, int> PII;
typedef pair<long long, long long> PLL;
typedef vector<int> VI;
typedef vector<long long> VLL;
typedef vector<string> VS;
typedef vector<PII> VII;
typedef vector<VI> VVI;
typedef map<int,int> MPII;
typedef map<long long,long long> MPLL;
typedef set<int> SETI;
typedef set<long long> SETLL;
typedef multiset<int> MSETI;
typedef long int int32;
typedef unsigned long int ul;
typedef long long int ll;
typedef unsigned long long int  ull;
#define OJ \
  freopen("input.txt", "r", stdin); \
  freopen("output.txt", "w", stdout);
#define FIO ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
#define in(t) cin>>t
#define out(t) cout<<t
template<typename... T>
void read(T&... args) {
    ((cin >> args), ...);
}

template<typename... T>
void write(T&&... args) { //rvalue reference is new to C++
    ((cout << args << " "), ...);
}

int n;
ll s[100001];

int recFun(vector<int>& dp, int i) {
    if (dp[i] != -1)
        return dp[i];

    int ret = 1;
    for(int j = i * 2; j <= n; j += i) {
        if (s[i] < s[j]) {
            ret = max(ret, recFun(dp,j) + 1);
        }
        //else ret = max(ret, recFun(dp,j));
    }
    dp[i] = ret;
    return ret;
}



int main() {
    int t;
    cin>>t;
    while(t--) {
        cin>>n;
        for(int i=1; i<=n; i++)
            cin>>s[i];

        vector<int> dp(100001, -1);
        int ans=0;
        for(int i=n; i>=1; i--)
            ans=max(ans, recFun(dp,i));

        cout<<ans<<endl;
    }

    return 0;
}
