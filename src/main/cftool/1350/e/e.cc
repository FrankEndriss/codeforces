/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* once a cell is blinking, it will blink forever.
 * So question is, when does it blink first time.
 * Any colorchange moves manhattan-wise throug
 * the field. So, the distance to the next
 * other colored field is the step with the first
 * blink.
 *
 * DFS, first with all white fields, second with all black fields.
 */
void solve() {
    cini(n);
    cini(m);
    cini(t);
    cinas(s,n);

    const int INF=1e9;
    vvi dp1(n, vi(m, INF)); /* dpW[i]==nearest 1 field */
    vvi dp0(n, vi(m, INF)); /* dpB[i]==nearest 0 field */

    queue<pii> q0;
    queue<pii> q1;

    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) {
            if(s[i][j]=='1') {
                dp1[i][j]=0;
                q1.push({i,j});
            } else {
                dp0[i][j]=0;
                q0.push({i,j});
            }
        }

    const vector<pii> d={ { -1, 0}, { 1,0 }, { 0, -1}, { 0,1}};
    while(q0.size()) {
        pii p=q0.front();
        q0.pop();
        for(int di : d) {
            pii n={ p.first+di.first, p.second+di.second };
            if(n
        }
        
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS, SOLVE ONE AFTER THE OTHER
