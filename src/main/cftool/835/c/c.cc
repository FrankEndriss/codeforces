/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

const int N=11;
void vadd(vi& v1, vi& v2) {
    v1.resize(N);
    v2.resize(N);
    for(size_t i=0; i<N; i++)
        v1[i]+=v2[i];
}

struct fenwick {
    vector<vi> fenw;
    int n;

    fenwick(int _n) : n(_n) {
        fenw.resize(n);
    }

    void update(int x, vi& v) {
        while (x < n) {
            vadd(fenw[x],v);
            x |= (x + 1);
        }
    }

    /* get sum of range (0,x), including x */
    vi query(int x) {
        vi v(N);
        while (x >= 0) {
            vadd(v, fenw[x]);
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

struct fenwick2d {
    vector<fenwick> fenw2d;
    int n, m;

    fenwick2d(int x, int y) : n(x), m(y) {
        fenw2d.resize(n, fenwick(m));
    }

    void update(int x, int y, vi& v) {
        while (x < n) {
            fenw2d[x].update(y, v);
            x |= (x + 1);
        }
    }

    vi query(int x, int y) {   // range 0..x/y, including x/y
        x=min(x, n-1);
        y=min(y, m-1);
        vi v(N);
        while (x >= 0) {
            vi v2=fenw2d[x].query(y);
            vadd(v, v2);
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

/* We got only N different star types, so we can
 * create fenwick2d of vi(N)
 *
 * On query we get the number of the N star types in rect,
 * and sum up the brightness at time t[i].
 *
 * That should be possible using prefix sums, too,
 * and be much faster.
 */
void solve() {
    cini(n);
    cini(q);
    cini(cc);

//cerr<<"did begin n="<<n<<" q="<<q<<" cc="<<cc<<endl;
    fenwick2d fen(101, 101);
//cerr<<"did created fen"<<endl;

    vvvi sky(100, vvi(100, vi(N)));
    for(int i=0; i<n; i++) {
        cini(x);x--;
        cini(y);y--;
        cini(s);
        sky[x][y][s]++;
    }
    
//cerr<<"did read sky"<<endl;

    for(int i1=0; i1<100; i1++) 
    for(int i2=0; i2<100; i2++)
        fen.update(i1, i2, sky[i1][i2]);
//cerr<<"did update sky"<<endl;
//vi a=fen.query(100, 100);
//assert(accumulate(all(a), 0)==n);

    for(int i=0; i<q; i++) {
        cini(t);
        cini(x1); x1--;
        cini(y1); y1--;
        cini(x2); x2--;
        cini(y2); y2--;
        vi vans=fen.query(x2,y2);
//cerr<<"sum vans="<<accumulate(all(vans), 0LL)<<endl;;
        vi s1=fen.query(x1-1,y2);
        vi s2=fen.query(x2,y1-1);
        vi a1=fen.query(x1-1,y1-1);
        int ans=0;
        for(int j=0; j<N; j++) {
            int cont=(j+t)%(cc+1);
            vans[j]=vans[j]-s1[j]-s2[j]+a1[j];
            ans+=vans[j]*cont;
        }
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
