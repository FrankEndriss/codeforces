/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * a[i] must be distinct, even after moving one of them.
 *
 * We can move one of them in the middle of the prev/next, or in
 * the middle of the max next-prev.
 * Then the next smallest one will be smallest.
 *
 * Also consider edgecase "last element", since after last there is no rest. 
 * How to implement?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(days);
    vi a(n+1);
    a[0]=0;
    for(int i=1; i<=n; i++)
        cin>>a[i];

    vector<pii> d;
    for(int i=1; i<=n; i++) {
        int val=a[i]-a[i-1]-1;
        d.emplace_back(val,i);
    }
    sort(all(d));

    int ans=d[0].first; /* if no move at all */

    /* move min element to last day */
    vi aa=a;
    aa[d[0].first]=days;
    sort(all(aa));
    int lans=INF;
    for(int i=1; i<=n; i++) 
        lans=min(lans, a[i]-a[i-1]-1);
    ans=max(ans, lans);


    if(d[0].second<n) {
        /* move element to middle of prev/next segment */
        int l=a[d[0].second-1];
        int r=a[d[0].second+1];
        ans=max(ans, min(d[1].first, (r-l-1)/2));
    }

    /* move element to middle of biggest gap */
    ans=max(ans, min(d[1].first, (d.back().first-1)/2));
    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
