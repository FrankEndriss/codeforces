/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

const int MOD = 1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}

/* after 1m 
 * (x*2 + x*2-1)/2 == (2*x-1)/2
 * (2x + 2x-1) / 2
 *
 * after 2m (2*((2*x-1)/2)-1)/2
 * 2*2x=4x
 * 2*(2x-1)=4x-2
 * (2*2x)-1=4x-1
 * (2*(2x-1))-1=4x-3
 * (4x + 4x-1 + 4x-2 + 4x-3)/4
 *
 * after k month:
 * ii=2^k
 * iii=ii*(ii-1)/2
 * ans=(ii*ii*x -iii)/ii
 */
void solve() {
    cini(x);
    cini(k);

    if(x==0) {
        cout<<0<<endl;
        return;
    }

    x%=MOD;

    int ii=toPower(2,k);
    int iii=mul(mul(ii, pl(ii,-1)), inv(2));

    int ans=mul(ii, ii);
    ans=mul(ans,x);
    ans=pl(ans, -iii);
    ans=mul(ans, fraction(2,ii));
    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
