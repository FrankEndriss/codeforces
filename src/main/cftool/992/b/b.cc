/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;


map<int,int> pf(int n) {
    map<int,int> ans;
    for(int i=2; i*i<=n; i++) {
        while(n%i==0) {
            ans[i]++;
            n/=i;
        }
    }
    if(n>1)
        ans[n]++;
    return ans;
}

/* find a,b in (l,r) so that
 * gcd(a,b)==x
 * lcm(a,b)==y
 *
 * lcm(a,b)=a*b/gcd(a,b)
 *
 * Let be pf(a) the multiset of primefactors of a.
 * pf(gcd(a,b)) is intersection of pf(a) and pf(b)
 * pf(lcm(a,b)) is the union of pf(a) and pf(b)
 *
 * So pf(x) has to be subset of pf(a) and subset of pf(b)
 * We can start with a=x and b=x and then have to add
 * all pf(y) to a or b, but not both because that would change gcd(a,b)
 * Let np the number of distinct primefactors in y which are not in 
 * x, then since every primefactor can go to a or b there are
 * 2^np ways to distribute them.
 * We need to check all of them for the bounds (l,r)
 */
void solve() {
    cini(l);
    cini(r);
    cini(x);
    cini(y);

    if(y%x!=0) {
        cout<<0<<endl;
        return;
    }

    map<int,int> pfx=pf(x);
    map<int,int> pfy=pf(y);

    for(auto ent : pfx) {
        pfy[ent.first]-=min(pfy[ent.first], ent.second);
        if(pfy[ent.first]==0)
            pfy.erase(ent.first);
    }
    vi pfyv;
    for(auto ent : pfy) {
        int v=ent.first;
        for(int i=1; i<ent.second; i++)
            v*=ent.first;
        pfyv.push_back(v);
    }

    int ans=0;
    int cnt=1<<pfyv.size();
    for(int mask=0; mask<cnt; mask++) {
        int a=x;
        int b=x;
        for(int i=0; i<pfyv.size(); i++) {
            if(mask&(1LL<<i))
                a*=pfyv[i];
            else
                b*=pfyv[i];
        }
        assert(lcm(a,b)==y);
        assert(gcd(a,b)==x);

        if(a>=l && a<=r && b>=l && b<=r) {
                ans++;
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
