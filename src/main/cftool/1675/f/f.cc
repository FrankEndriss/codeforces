
/** * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Shortest path in tree over k vertices.
 * Kind of traveling salesman in a tree.
 * ???
 *
 * Consider the path from x to y.
 * In each subtree of x other than the path to y might be an a, or not
 * if yes, traverse it.
 * Same for y.
 * Also add dist x,y.
 * two dfs
 * 1. find y
 * 2. collect a[]
 *
 * ...but thats wrong.
 * We need to consider all shortest path from all a[] to any
 * vertex on path x->y
 * :/ how?
 *
 * 1. construct path x->y
 * 2. check dfs2 all other childs of x,y
 * 3. traverse path x->y
 *  foreach vertex, check all other childs dfs2
 * ... no fun to implement :/
 */
void solve() {
    cini(n);
    cini(k);
    cini(x); x--;
    cini(y); y--;

    vb a(n);
    for(int i=0; i<k; i++) {
        cini(aux);
        aux--;
        a[aux]=true;
    }

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    int dxy=0;
    function<bool(int,int,int)> dfs1=[&](int v, int p, int yy) {
        dxy++;
        if(v==yy)
            return true;

        for(int chl : adj[v]) 
            if(chl!=p && dfs1(chl,v,yy))
                return true;

        return false;
    };

    function<int(int,int)> dfs2=[&](int v, int p) {
        int ans=0;
        for(int chl :adj[v]) {
            if(chl!=p)
                ans+=dfs2(chl, v);
        }

        if(a[v] ||ans)
            return ans+1;
        else
            return 0LL;
    };

    int ans=0;
    int ldist=0;
    for(int chl : adj[x]) {
        dxy=0;
        if(!dfs1(chl, x, y)) {
            ans+=dfs2(chl,x);
        } else
            ldist=dxy;
    }
    ans+=ldist;

    for(int chl : adj[y]) {
        if(!dfs1(chl, y, x))
            ans+=dfs2(chl,y);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
