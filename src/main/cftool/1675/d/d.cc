
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);

    int root=-1;
    vvi adj(n);
    for(int i=0; i<n; i++) {
        cini(p); p--;
        if(p!=i)
            adj[p].push_back(i);
        else
            root=p;
    }

    vi d(n);
    /* @return max depth */
    function<int(int,int)> go1=[&](int v, int dd) {
        int ma=0;
        for(int chl : adj[v]) {
            ma=max(ma, 1+go1(chl, dd+1));
        }
        return d[v]=ma;
    };

    go1(root, -1);

    //vi vis(n);
    stack<int> st;

    vvi ans;
    function<void(int)> split=[&](int v) {
        ans.back().push_back(v+1);
        //cout<<v+1<<" ";
        if(adj[v].size()==0)
            return;

        vi id(adj[v].size());
        iota(all(id), 0LL);
        sort(all(id), [&](int i1, int i2) {
                return d[adj[v][i1]]>d[adj[v][i2]];
        });

        split(adj[v][id[0]]);

        for(size_t i=1; i<adj[v].size(); i++)
            st.push(adj[v][id[i]]);
    };

    st.push(root);
    while(st.size()) {
        int v=st.top();
        st.pop();
        vi e;
        ans.push_back(e);
        split(v);
    }

    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++) {
        cout<<ans[i].size()<<endl;
        for(int j : ans[i])
            cout<<j<<" ";
        cout<<endl;
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
