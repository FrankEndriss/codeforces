
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * if k>maxChar then we can produce aaa...
 * else we need to make 
 * 1. first char a
 * 2. second char a...
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);

    vector<char> m(256);
    for(char c='a'; c<='z'; c++) {
        m[c]=c;
    }

    char conf='a';
    char last='a';
    char to='a';
    for(int i=0; k>0 && i<n; i++) {
        int dd=s[i]-conf;
        if(dd>0 && dd<=k) {
            conf=s[i];
            k-=dd;
        } else if(dd>k) {
            last=s[i];
            to=s[i]-k;
            k=0;
        }
    }

    for(size_t i=0; i<n; i++)  {
        if(s[i]<=conf)
            cout<<'a';
        else if(s[i]<=last && s[i]>=to)
            cout<<to;
        else
            cout<<s[i];
    }
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
