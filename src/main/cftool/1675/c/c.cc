
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * If any tells 1 after some 0, the 1 is the thief.
 * Thief was before or at the first one telling 0.
 * Thief is the 0 or the 1 just before.
 */
void solve() {
    cins(s);
    const int n=(int)s.size();

    int p1=0;
    for(int i=0; i<n; i++) {

        if(s[i]=='1') {
            p1=i;
        }  else if(s[i]=='0') {
            cout<<i-p1+1<<endl;
            return;
        }
    }
    cout<<n-p1<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
