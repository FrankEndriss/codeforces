/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The moves form circles, ans=lcm(circleLengths[])
 *
 * But we also need to consider the chars in the string.
 * The first time the chars of one circle equals the orig
 * circle might be a fraction of the whole circle size.
 * So build, rotate and check these circles, by building
 * a string and rotate/compare it on each position.
 */
void solve() {
    cini(n);
    cins(s);
    //cerr<<"s="<<s<<endl;
    vi p(n);
    for(int i=0; i<n; i++) {
        cin>>p[i];
        p[i]--;
    }

    vi vis(n);
    vi c;
    int ans=1;
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;

        string str;
        int v=i;
        do {
            str.push_back(s[v]);
            vis[v]=true;
            v=p[v];
        }while(v!=i);
        //cerr<<"str="<<str<<endl;

        string t=str;
        bool done=false;
        for(size_t j=0; j<str.size(); j++) {
            rotate(str.begin(), str.begin()+1, str.end());
            if(str==t) {
                //cerr<<"csize="<<j+1<<endl;
                ans=lcm(ans, j+1);
                done=true;
                break;
            }
        }
        assert(done);

    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
