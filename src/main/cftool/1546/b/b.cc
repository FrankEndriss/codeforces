
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* String pairs, one string "to much".
 * Initial list of strings with "swaps".
 *
 * How to find from a pair of string the two initial strings?
 * -> We know the original string, and from that can find a 
 *  freq of letters foreach position.
 *  Then remove from that freq-table all the found letters,
 *  and afterwards the table contains m position with freq==1
 *
 *  Nearly 30 minutes to understand the problem based on a really
 *  bad statement. Sic :/
 */
void solve() {
    cini(n);
    cini(m);

    vvi f(26, vi(m));
    for(int i=0; i<n; i++) {
        cins(s);
        for(int j=0; j<m; j++) 
            f[s[j]-'a'][j]++;
    }

    for(int i=0; i<n-1; i++) {
        cins(s);
        for(int j=0; j<m; j++) 
            f[s[j]-'a'][j]--;
    }

    for(int i=0; i<m; i++) {
        for(int j=0; j<26; j++) {
            if(f[j][i]==1) {
                cout<<(char)('a'+j);
                break;
            }
        }
    }
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--) 
        solve();
}
