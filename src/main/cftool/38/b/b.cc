/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cins(rook);
    cins(knight);

    int ri=rook[0]-'a';
    int rj=rook[1]-'1';

    int ki=knight[0]-'a';
    int kj=knight[1]-'1';

    int ans=0;
    for(int i=0; i<8; i++) {
        for(int j=0; j<8; j++) {
            if(i==ri || j==rj)
                continue;

            if(i==ki && j==kj)
                continue;

            int d1=abs(i-ri);
            int d2=abs(j-rj);
            if(d1>d2)
                swap(d1,d2);
            if(d1==1 && d2==2)
                continue;

            d1=abs(i-ki);
            d2=abs(j-kj);
            if(d1>d2)
                swap(d1,d2);
            if(d1==1 && d2==2)
                continue;

            ans++;
        }
    }
    cout<<ans<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

