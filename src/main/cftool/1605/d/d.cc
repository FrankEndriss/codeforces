/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Once the token is moved, it cannot change direction.
 * Note that token can move only between numbers with 
 * the same highest bit.
 *
 * One way to win for Alice is to create leafs with "bad" parents,
 * no move available.
 * But the parents of these leafs are loosing positions, since
 * Bob can move to the leaf.
 * ...
 * By pidgeonhole there are at least (n+1) numbers with
 * the same highest bit.
 * How do we place the numbers in the tree, what to do in the dfs?
 *
 */
void solve() {
}

signed main() {
    solve();
}
