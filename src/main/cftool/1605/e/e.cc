/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The value of b[1] means that we need to increment/decrement all elements of the array.
 * Then, obviously we need to adjust the elements in increasing order.
 * Note that "multiple" numbers need to be extra adjusted...somehow.
 * Consider a[6], it is multiple of a[2] and a[3].
 * Let aa be the adjustment on all numbers caused by b[1], and x[j] the
 * initial adjustment of a[j].
 * a[2] needs to be adjusted by x[2]+aa
 * a[3] needs to be adjusted by x[3]+aa
 * a[4] ... x[4]+aa-(x[2]+aa) == x[4]-x[2]
 * a[6] ... x[6]+aa-(x[2]+aa) - (x[3]+aa) == x[6]-aa-x[2]-x[3]
 *
 * So, there is this inclusion/exclusion pattern... complecated.
 */
void solve() {
}

signed main() {
    solve();
}
