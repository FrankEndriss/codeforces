/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to reverse each 111...000
 * Which means, each such seq counts once.
 *
 * ...It is a _subsequence_.
 * Can we sort better using that fact?
 * Well, maybe yes.
 *
 * Consider the string starting at the first '1'.
 * We want to fix as most as possible '1' in on operation.
 * So we swap as much '1' with as much '0' as available right of them.
 * Do so until there are none available, then finish.
 *
 *
 *
 */
void solve() {
    cini(n);
    cins(s);

    vvi ans;
    int idx=0;

    int l=0;
    int r=n-1;
    while(l<n && s[l]=='0')
        l++;
    while(r>=0 && s[r]=='1')
        r--;

    while(l<r) {
        vi lans;
        while(l<r) {
            if(s[l]=='1' && s[r]=='0') {
                lans.push_back(l);
                lans.push_back(r);
                l++;
                r--;
            }
            while(l<n && s[l]=='0')
                l++;
            while(r>=0 && s[r]=='1')
                r--;
        }
        if(lans.size()==0)
            break;
        else
            ans.push_back(lans);
    }

    cout<<ans.size()<<endl;
    for(int j=0; j<ans.size(); j++) {
        cout<<ans[j].size()<<" ";
        sort(all(ans[j]));
        for(int i : ans[j])
            cout<<i+1<<" ";
        cout<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
