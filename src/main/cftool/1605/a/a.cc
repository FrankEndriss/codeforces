/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * a[1],a[3] nothing happens
 * a[1],a[2] incrememt avg by .5, a[2] by -1, so a[2]-=3/2 
 * a[2],a[3] decrement avg by .5, a[2] by 1, so a[2]+=3/2
 */
void solve() {
    cini(a);
    cini(b);
    cini(c);
    int s1=a+c;
    int s2=b*2;
    int d=(s1-s2)*2;
    if(d%3==0)
        cout<<0<<endl;
    else
        cout<<1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
