/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need to choose exactly k intervals of length
 * exactly m so that the sum of a[i] is maximized
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    cinai(a,n);

/* dp[i][j]== best sum if i intervals starting at index j */
    vvi dp(k+1, vi(n+1,-1));

    function<int(int,int)> calc=[&](int kk, int nn) {
        if(kk==0)
            return 0LL;

        if(dp[kk][nn]>=0)
            return dp[kk][nn];

//    cerr<<"kk="<<kk<<" nn="<<nn<<" m="<<m<<endl;
        assert(n-nn>=kk*m);

        int sum=0;
        for(int i=0; i+1<m; i++) 
            sum+=a[nn+i];
        int ans=0;
        int masum=0;
        for(int i=nn; i+(kk*m)<=n; i++) {
            sum+=a[i+m-1];
            if(sum>masum)
                ans=max(ans, sum+calc(kk-1, i+m));
            masum=max(masum,sum);
            sum-=a[i];
        }
        return dp[kk][nn]=ans;
    };

    int ans=calc(k,0);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
