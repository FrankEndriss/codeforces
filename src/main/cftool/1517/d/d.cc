/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vvvvi= vector<vvvi>;
using vs= vector<string>;

#define endl "\n"

/* |i1-i2| + |j1-j2| == 1 means
 * Only the first or the second coordinate differ by exactly one.
 * ie the cells share a side.
 *
 * Input format is wierded, but we can construct a simple graph
 * with weighted edges from it.
 *
 * Answer is the min cost path of len k foreach vertex in the grid.
 *
 * dp[i][j]=min boredness at vertex i if comming from vertex j after current step
 * Since k<=20 we need to maintain only 40*40 vertex... Still 1600,
 * so ~2e8 states... not good.
 *
 * Consider brute force for one vertex.
 * We need a grid of size k*k
 * Then simply floyd warshal like find the cost to be
 * at any position after current step.
 * So why not? should work...no
 * It is 500*500 * 20*20*20 *4 steps ~8e9
 * ***
 * Kind of binary lifting?
 * How much paths are there of len k ending at same position?
 * Cant be that much. Simply check all.
 *
 * Consider paths of len k/2, there are 4^10 different ones.
 * Each one has an offset, and can be matched with each of
 * the opposite offset.
 * Note that it is at most k*k offsets
 * Then we can check foreach position all 2^10 paths,
 * which makes it run in 
 *
 * *** 
 * What about bitmasks, k<=20.
 * There are 4^10 half paths, but at most 100 cells reacheable.
 * From each of the 100 cells there are like 1000 paths back to origin.
 *
 * Find from each vertex the cheapest path to any of the 100 vertex arround?
 * Then, foreach vertex, add them. Like meet in the middle.
 *
 * Note also that k is even.
 * ***
 * Actually it is fairly simple:
 * dp[i][j][k]= min cost path of len k ending in i,j
 */
const int INF=1e10;
void solve() {
    cini(n);
    cini(m);
    cini(k);

    /* N, S, W, E */
    const vi di={ -1, 1, 0, 0};
    const vi dj={ 0, 0, -1, 1};
    /* cost[i][j][d]=cost to go from cell i,j in dir d */
    vvvi cost(n, vvi(m, vi(4)));

    for(int i=0; i<n; i++) {
        for(int j=0; j<m-1; j++)  {
            cini(aux);
            cost[i][j][3]=aux;
            cost[i][j+1][2]=aux;
        }
    }

    for(int i=0; i<n-1; i++) {
        for(int j=0; j<m; j++)  {
            cini(aux);
            cost[i][j][1]=aux;
            cost[i+1][j][0]=aux;
        }
    }

    if(k&1) {
        for(int i=0; i<n; i++)  {
            for(int j=0; j<m; j++) 
                cout<<"-1 ";
            cout<<endl;
        }
        return;
    }


    vvi dp(n, vi(m));

    for(int kk=1; kk<=k/2; kk++)  {
        vvi dp0(n, vi(m, INF));
        for(int i=0; i<n; i++)
            for(int j=0; j<m; j++)
                for(int d=0; d<4; d++) {
                    const int ii=i+di[d];
                    const int jj=j+dj[d];
                    if(ii>=0 && ii<n && jj>=0 && jj<m) 
                        dp0[ii][jj]=min(dp0[ii][jj], dp[i][j]+cost[i][j][d]);
                }
        dp.swap(dp0);
    }

    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++) 
            cout<<dp[i][j]*2<<" ";
        cout<<endl;
    }


}

signed main() {
    solve();
}
