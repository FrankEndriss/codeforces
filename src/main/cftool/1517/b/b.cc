/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* n checkpoints
 * m runners
 * What a wierded statement text.
 * We need to minimize the "tiredness".
 * Output is the length of the path foreach of the j runners.
 * ...
 * These kind of "text understanding" problems are among the worst of all.
 * I do not understand why these are accepted by coordinators. It is annoying.
 * ...
 * However, each one of the runners wants to choose _one_ mostly short path.
 * So, since we are free to choose, we simply choose the j shortest ones.
 *
 * How to construct the answers?
 * We start by adding the shortest m edges to each runner one.
 * Then add all other edges to the runners in any order, so that
 * each runner has each checkpoint once.
 *
 * ...
 * and FUCKFUCKFUCK: We do not output the length of the path, 
 * but the length of each edge! SICSICSIC!!!
 */
void solve() {
    cini(n); /* checkpoints */
    cini(m); /* runners */

    using t3=tuple<int,int,int>;
    set<t3> a;
    vvi b(n, vi(m));    /* b[i][j]= jth edge of checkpoint i */
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            cin>>b[i][j];
            a.emplace(b[i][j], i, j);
        }
    }

    /* ans[i][j]=checkpoint j of runner i vis */
    vvb ans(m, vb(n));
    /* len[i][j]= length of path of runner i at checkpoint j */
    vvi len(m, vi(n));

    for(int i=0; i<m; i++) {
        auto it=a.begin();
        /* ii=checkpoint, jj=edgeindex */
        auto [v,ii,jj]=*it;
        a.erase(it);
        ans[i][ii]=true;
        len[i][ii]=b[ii][jj];
        b[ii][jj]=-1;
    }

    for(int i=0; i<m; i++)  {
        for(int j=0; j<n; j++) {
            for(int k=0; !ans[i][j] && k<m; k++) {
                if(b[j][k]>=0) {
                    ans[i][j]=true;
                    len[i][j]=b[j][k];
                    b[j][k]=-1;
                }
            }
        }
    }

    for(int j=0; j<n; j++) {
        for(int i=0; i<m; i++) {
            cout<<len[i][j]<<" ";
        }
        cout<<endl;
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
