/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Can we greedy fill the cells, starting left upper?
 * Allways choose the topmost, leftmost cell.
 */
void solve() {
    cini(n);
    vvi ans(n, vi(n));
    for(int i=0; i<n; i++)
        cin>>ans[i][i];

    for(int i=0; i<n; i++) {
        int num=ans[i][i];
        pii idx={i,i};

        for(int j=1; j<num; j++) {
            /* check go left */
            if(idx.second>0 && ans[idx.first][idx.second-1]==0) {
                idx.second--;
                /* else go down */
            } else if(idx.first+1<n) {
                idx.first++;
                /* else we are in left down corner */
            } else {
                /*
    for(int i=0; i<n; i++) {
        cerr<<"err ";
        for(int j=0; j<=i; j++)  
            cerr<<ans[i][j]<<" ";
        cerr<<endl;
    }
    */

                assert(false);
                /*
                assert(idx.first==n-1);
                idx={i,i};
                while(ans[idx.first][idx.second-1]>0)
                    idx.first++;
                    */
            }
            ans[idx.first][idx.second]=num;
        }
    }

    for(int i=0; i<n; i++) {
        for(int j=0; j<=i; j++)  
            cout<<ans[i][j]<<" ";
        cout<<endl;
    }



}

signed main() {
    solve();
}
