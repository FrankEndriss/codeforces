/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

typedef tree<long long,null_type,less<long long>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to find foreach position j
 * the number of a[i] left of it and a[i]>a[j]
 * the number of a[k] right of it and a[k]<a[i]
 *
 * Do this once from 0..n and once n..0 
 * using a set.
 *
 * Then build sum.
 */
void solve() {
    cini(n);
    cinai(a,n);

    indexed_set sl;
    vi prea(n);
    for(int i=0; i<n; i++) {
        sl.insert(a[i]);
        int idx=sl.order_of_key(a[i]);
        prea[i]=sl.size()-(idx+1);
    }

    sl.clear();
    vi posta(n);
    for(int i=n-1; i>=0; i--) {
        sl.insert(a[i]);
        int idx=sl.order_of_key(a[i]);
        posta[i]=idx;
    }

    int ans=0;
    for(int i=0; i<n; i++) {
        ans+=prea[i]*posta[i];
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
