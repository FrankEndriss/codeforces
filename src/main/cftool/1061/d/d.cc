/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+7;
const int INF=1e18;
/*
 * We assume each show on own tv. Then we can optimise
 * by watching two shows on same tv if the end of one
 * matches the start of the other.
 * So we want to find max valuable pairing.
 *
 * Greedy: For every start find best matching previous end.
 *
 * Note that it does not work to find for every end
 * the best matching start. For some reason :/
 */
void solve() {
    cini(n);
    cini(x);
    cini(y);

    set<pair<pii,int>> sta;

    int ans=0;
    for(int i=0; i<n; i++) {
        cini(s);
        cini(e);
        sta.insert({{s,e},i});
        ans+=(e-s)*y;
        ans%=MOD;
    }

    set<pii> end;
    while(sta.size()) {
        auto it=sta.begin();

/* find latest ending before start of it */
        auto it2=end.upper_bound({-1*it->first.first, INF});
        if(it2!=end.end()) {
            int val=(it->first.first + it2->first)*y;
            assert(val>0);
            if(val<x) {
                ans+=val;
                end.erase(it2);
            } else
                ans+=x;

        } else {
            ans+=x;
        }

        ans%=MOD;
        end.insert({-1*it->first.second,it->second});
        sta.erase(it);
    }

/* other algo (find matching start for every end) does not work
    while(end.size()) {
        auto it=end.begin();
        sta.erase({{it->first.second,it->first.first},it->second});
        ans+=x;
        ans%=MOD;
        int e=it->first.first;
        end.erase(it);

        while(true) {
            auto it2=sta.upper_bound({{e,INF},-1LL});
            if(it2==sta.end())
                break;

            int val=y*(it2->first.first-e);
            if(val>=x)
                break;

            ans+=val;
            ans%=MOD;
            e=it2->first.second;

            end.erase({{it2->first.second,it2->first.first},it2->second});
            sta.erase(it2);
        };
    }
*/

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
