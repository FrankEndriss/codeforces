/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int reflect(vi& a) {
    int i1=0;
    int i2=0;
    for(int k : a) {
        i1*=10;
        i1+=k;
        i2*=10;
        i2+=(9-k);
    }
    return i1*i2;
}

/* see editorial:
 * Since the number is between l and r, and should be most near to
 * its reflection number, there are 3 possible solutions: 
 * l, r or 5*10^k
 */
void solve() {
    cini(l);
    cini(r);

    vi vl;
    vi vr;
    while(l||r) {
        vl.push_back(l%10);
        l/=10;
        vr.push_back(r%10);
        r/=10;
    }
    reverse(all(vl));
    reverse(all(vr));

    int ans=max(reflect(vl), reflect(vr));
    vi ten={ 5 };
    while(ten.size()<=vl.size() && vl<=ten && ten<=vr)  {
        ans=max(ans, reflect(ten));
        ten.push_back(0);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
