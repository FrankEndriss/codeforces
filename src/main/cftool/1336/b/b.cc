/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int INF=4e18;

/* We need to find the three gems "most near" to each other.
 * If we fix the middle one we can choose the opt left and right.
 */
void solve() {
    cinai(n,3);
    vvi rgb(3);
    for(int i=0; i<3; i++)  {
        rgb[i].resize(n[i]);
        for(int j=0; j<n[i]; j++)
            cin>>rgb[i][j];
        sort(all(rgb[i]));
    }

    vi p={0,1,2};

    function<int()> mi=[&]() {
        int ans=INF;
        for(int m : rgb[p[1]]) {
            auto it=upper_bound(all(rgb[p[0]]), m);
            if(it!=rgb[p[0]].begin())
                it--;
            int l=*it;
            it=lower_bound(all(rgb[p[2]]), m);
            int r=-1;
            if(it==rgb[p[2]].end())
                r=rgb[p[2]].back();
            else
                r=*it;

            int d1=abs(m-l);
            int d2=abs(m-r);
            int d3=abs(l-r);
            ans=min(ans, d1*d1+d2*d2+d3*d3);
        }
        return ans;
    };

    int ans=INF;
    do {
        ans=min(ans, mi());
    }while(next_permutation(all(p)));

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
