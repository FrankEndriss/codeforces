
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Its a dp.
 * Consider using x red cubes. There are a number of possible ways to use 
 * x cubes, and the number of green cubes is determined.
 * So add the numbers of all possible x configurations.
 *
 * Find the number for all x configrations by a knapsack like dp.
 **/
const int MOD=1e9+7;
void solve() {
    cini(r);
    cini(g);
    if(r>g)
        swap(r,g);

    int sum=0;  /* number of cubes in max hight tower */
    int h=1;  /* max hight */
    while(sum+h<=r+g) { 
        sum+=h;
        h++;
    }
    h--;

    vi dp(r+1);         /* find all possible configurations for smaller value */
    dp[0]=1;
    for(int y=1; y<=h; y++) {
        for(int i=r; i-y>=0; i--) {
            dp[i]+=dp[i-y];
            dp[i]%=MOD;
        }
    }

    int ans=0;
    for(int i=max(0LL,sum-g); i<=r; i++) {  /* use i reds and sum-i greens */
        if(sum-i<=g) {
            ans+=dp[i];
            ans%=MOD;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
