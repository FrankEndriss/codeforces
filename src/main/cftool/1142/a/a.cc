/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* It is about divisors and gcd.
 * There are k distinct startpoints.
 *
 * Let c=n*k
 * Number of full rounds is r=lcm(l,c)/c
 * So number of stops is (r*c)/l
 * or simple lcm(l,c)/l
 * which is same as c/gcd(l,c)
 *
 * From a we know 2 possible startpoints. They can degenerate to 1.
 * From b we know 2*n possible firststop points, can be 1.
 *
 * Find all possible l and collect min/max.
 */
void solve() {
    cini(n);
    cini(k);
    cini(a);
    cini(b);

    int s1=a;
    int s2=k-a;

    int f1=b;
    int f2=k-b;

    int mi=1e18;
    int ma=0;
    vi d(4);
    d[0]=f1-s1;
    d[1]=f2-s1;
    d[2]=f1-s2;
    d[3]=f2-s2;

    for(int i=0; i<4; i++)  {
        while(d[i]<=0)
            d[i]+=k;
    }

    for(int j=0; j<n; j++) {
        for(int i=0; i<4; i++)  {
            //d[i]%=(n*k);
            //int ans=lcm(d[i],n*k)/d[i];
            int ans=(n*k)/gcd(d[i], n*k);
            mi=min(mi, ans);
            ma=max(ma, ans);
            d[i]+=k;
        }
    }
    cout<<mi<<" "<<ma<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
