/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* see https://cp-algorithms.com/data_structures/disjoint_set_union.html
 * Also known as union find structure. 
 * Note that the above link has some optimizations for faster combining of sets if needed. 
 */

struct Dsu {
    vector<int> p;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        iota(p.begin(), p.end(), 0);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members. 
 * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};
/* Greedy
 * If there is an triangle in the graph, that triangle determines
 * the whole component.
 *
 * Components are independend of each other.
 *
 * We can start at any vertex and dfs. Then each connected vertex gets a value
 * relative to the first vertex.
 * It is always the edgeval minus the parents vertex value.
 *
 * This val is like a color, if we get two different colors for one vertex there is no sol.
 *
 * We end up with a value for each vertex, in form of y[i]-x0 or y[i]+x0, where y[i] is a constant.
 * We need to maintain the sign of x0.
 * We can end up with multiple formulars for x0, ie x1=y[i]+x0; x1=y[j]-x0;
 * This compiles to y[i]+x0=y[j]-x0 -> y[j]-y[i]=2*x0 -> (y[i]-y[j])/2 = x0
 * This is the case one solution.
 *
 * If several valid solutions, how to find the one with smallest absolute sum?
 *  Note that we can optimze the sum formular to not iterate all vertex on each loop.
 *  Then we end up with something like |x0| + 1-|x0| + 1+|x0|
 *  Then x0==0 is best solution unless the consts are negative, in this case
 *  x0=sum(consts)/cntVertex
 */

void solve() {
    cini(n);
    cini(m);
    map<pii,int> edg;
    vector<vector<pii>> adj(n);
    for(int i=0; i<m; i++) {
        cini(a);
        a--;
        cini(b);
        b--;
        cini(c);    /* 1,2, sum of weights of the two nodes */
        auto it=edg.find({min(a,b),max(a,b)});
        if(it!=edg.end()) {
            if(c!=it->second) {
                cout<<"NO"<<endl;
                return;
            }
        }
        adj[a].push_back({b,c});
        adj[b].push_back({a,c});
        edg[ {min(a,b),max(a,b)}]=c;
    }

    /* collect the node values determined by x0 */
    vb vis(n);
    function<void(int,double,vd&)> pans=[&](int v, double x, vd& ans) {
        if(vis[v])
            return;
        vis[v]=true;
        ans[v]=x;
        for(pii chl : adj[v])
            pans(chl.first, (double)chl.second-x, ans);
    };

    Dsu dsu(n);
    vvi comp(n);
    bool imp=false;     /* impossible, no solution flag */
    bool fini=false;    /* finshed, solution found flag */
    double x0;          /* value of node x0 if solution found */
    vi y(n);    /* y[i]= value for node i */
    vi s(n);    /* s[i]= sign of x0 at node i, 0=unset */
    /* vertex; y-value iherited by parent; sign of x0; */
    function<void(int,int,int)> dfs=[&](int v, int yp, int sp) {
        /* some cases: */
        /* if v was seen before with same yp,sp then just ignore */
        /* if v was seen before with same negative sp and -yp!=y[v] then this determines the
         * absolute value of x0, and hence the whole component. */

        if(s[v]!=0) {   /* seen before */
            if(s[v]==sp && yp==y[v]) /* but same info */
                return;

            if(s[v]==sp && yp!=y[v]) {  /* different info, no solution */
                imp=true;
                return;
            }

            /* We got two formulars for x0, calculate it! */
            /* This compiles to y[i]+x0=y[j]-x0 -> y[j]-y[i]=2*x0 -> (y[j]-y[i])/2 = x0 */
            int y1, y2;
            if(sp>0) {
                y1=yp;
                y2=y[v];
            } else {
                y1=y[v];
                y2=yp;
            }
            x0=(y2-y1)/2.0;
            fini=true;
            return;
        }

        y[v]=yp;
        s[v]=sp;
        for(pii chl : adj[v]) {
            dsu.union_sets(v, chl.first);
            comp[dsu.find_set(chl.first)].push_back(chl.first);

            dfs(chl.first, chl.second-yp, -sp);
            if(imp || fini)
                return;
        }
    };

    vd ans(n);

    for(int i=0; i<n; i++)  {
        if(s[i]==0) {
            comp[i].push_back(i);
            fini=false;  
            dfs(i, 0, 1);

            //cout<<"root="<<i<<" imp="<<imp<<" fini="<<fini<<" x0="<<x0<<endl;
            if(imp) {
                cout<<"NO"<<endl;
                return;
            }
            if(fini) {
                pans(i, x0, ans);
            } else {
/* how to minimize: |x0| + |1-x0| + |1+x0| ???*/
                pans(i, 0.0, ans);
            }
        }
    }

    cout<<"YES"<<endl;
    for(int i=0; i<n; i++) 
        cout<<ans[i]<<" ";
    cout<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
