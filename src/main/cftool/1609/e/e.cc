/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



using Sa=int; /* type of values */

Sa st_opa(Sa a, Sa b) {
    return a+b;
}

Sa st_ea() {
    return 0;
}

using streea=segtree<Sa, st_opa, st_ea>;


using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}

/* This is the neutral element */
const S INF=1e9;
S st_e() {
    return INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * How to find output for one given string?
 * -> We need to make the string so that
 *  before rightmost c, there must not be any subseq "a..b", and that is,
 *  there must be no 'a' left of the rightmost b left of the rightmost c.
 *  Consider all 'c' positions starting from right.
 *
 *  Removing that c means cnt['c'] changes.
 *  Otherwise we can remove a number of 'b' left of it.
 *  Each not removed 'b' costs the number of 'a' left of that b.
 *
 *  Build prefix sums to quickly find the numbers of abc in prefixes.
 *
 *  How to handle updates?
 *  Obviously the prefix sums change, so we could use segtree instead.
 *
 * Consider maintaining segment trees as:
 * sega(n) : sum, 1 at pos of 'a'
 * segb(n) : min, segb[i]=number of 'a' left of 'b' at position i plus number of b right of that pos.
 * segc(n) : min, segc[i]=segb.prod(0,i-1)+number of c right of that pos.
 */
void solve() {
    cini(n);
    cini(q);
    cins(s);

    streea sega(n);
    streea segab(n);
    streea segac(n);

    stree segb(n);
    stree segc(n);

    vi cnt(3);
    for(int i=n-1; i>=0; i--) {
        cnt[s[i]-'a']++;
        if(s[i]=='a')
            sega.set(i,1);
        else if(s[i]=='b')
            segab.set(i,1);
        else if(s[i]=='c')
            segac.set(i,1);
    }

    int cntB=0;
    for(int i=n-1; i>=0; i--) {
        int val=sega.prod(0,i);
        segb.set(i, val+cntB);
        if(s[i]=='b') {
            cntB++;
        }
    }

    int cntC=0;
    for(int i=n-1; i>=0; i--) {
        int val=segb.prod(0,i);
        segc.set(i, val+cntC);
        if(s[i]=='c')
            cntC++;
    }

    for(int i=0; i<q; i++) {
        cini(pos);
        pos--;
        char c;
        cin>>c;

        if(s[i]==c) {   /* no update */
            ;
        } else {
            if(s[pos]=='a') {
                sega.set(pos,0);
                segb.apply(pos+1, n, -1);
                segc.apply(pos+1, n, -1);
            } else if(s[pos]=='b') {
                segab.set(pos,0);
                segb.apply(0, pos+1, -1);
                segb.set(pos, INF);
            } else if(s[pos]=='c') {
                segac.set(pos,0);
                segc.set(pos, INF);
            }

            if(c=='a') {
                sega.set(pos,1);
                segb.apply(pos+1, n, 1);
                segc.apply(pos+1, n, 1);
            } else if(c=='b') {
                segb.apply(0, pos, 1);
                int numB=42;    /* number of 'b' right of pos */
                segb.set(pos, sega.prod(0,pos)+numB);
            } else if(c=='c') {
                segc.apply(0, pos, 1);
                int numC=42;    /* number of 'c' right of pos */
                segc.set(pos, segb.prod(0,pos)+numC);
            }
        }

        int ans=segc.prod(0,n);
        cout<<ans<<endl;
    }

}

signed main() {
    solve();
}
