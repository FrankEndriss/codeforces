/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Assume dsu.
 * After adding a connection x,y we need to output the 
 * size of the biggest component.
 * At least I think thats what they want :/
 *
 * No, it seems otherwise.
 * Maybe it is asked that if x,y do not belong to same component,
 * wiliam can introduce _any_ to other people instead?
 * If so, then let k be the number of _any-connection, and
 * we need to find the number of sum of the sized of the k+1 biggest components.
 */
void solve() {
    cini(n);
    cini(d);

    dsu ds(n);

    multiset<int> csizes;
    for(int i=0; i<n; i++) 
        csizes.insert(1);

    int cnt=1;
    for(int i=0; i<d; i++) {
        cini(x); x--;
        cini(y); y--;

        int sx=ds.size(x);
        int sy=ds.size(y);

        if(!ds.same(x,y)) {
            auto it1=csizes.find(sx);
            csizes.erase(it1);
            auto it2=csizes.find(sy);
            csizes.erase(it2);

            csizes.insert(sx+sy);
            ds.merge(x,y);
        } else {
            cnt++;
        }
        auto it=csizes.rbegin();
        int ans=0;
        for(int j=0; j<cnt && it!=csizes.rend(); j++) {
            ans+=*it;
            it++;
        }
        cout<<ans-1<<endl;
    }

}

signed main() {
    solve();
}
