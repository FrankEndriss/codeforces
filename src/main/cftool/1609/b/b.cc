/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Brute force?
 */
void solve() {
    cini(n);
    cini(q);

    cins(s);

    int cnt=0;
    for(int i=0; i+2<n; i++) {
        if(s[i]=='a' && s[i+1]=='b' && s[i+2]=='c') {
            cnt++;
        }
    }

    for(int i=0; i<q; i++) {
        cini(idx);
        idx--;
        char c;
        cin>>c;

        vector<char> old(5, ' ');
        for(int k=0; k<5; k++) {
            if(idx-2+k>=0 && idx-2+k<n)
                old[k]=s[idx-2+k];
        }

        vector<char> nuw=old;
        nuw[2]=c;

        int ocnt=0;
        int ncnt=0;
        for(int k=0; k<3; k++) {
            if(old[k]=='a' && old[k+1]=='b' && old[k+2]=='c') {
                ocnt++;
            }
            if(nuw[k]=='a' && nuw[k+1]=='b' && nuw[k+2]=='c') {
                ncnt++;
            }
        }
        s[idx]=c;

        cnt+=ncnt;
        cnt-=ocnt;

        cout<<cnt<<endl;

    }
}

signed main() {
    solve();
}
