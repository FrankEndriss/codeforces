/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=300000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/**
 * ???
 * We need to find some pairs of numbers i,j.
 *
 * How can product of something be a prime number?
 * Ok, to be the product be a prime
 * all but one of the other numbers must be eq 1.
 * Or all eq 1.
 * And the one number must be prime.
 *
 * Brute force is ok, but consider a lot of 1 and small e.
 * ???
 * We can split the seq a[] into e seqs.
 *
 * Then we need to find number of segments with all 1s, and 
 * exactly one prime.
 *
 * Consider each subseq mod e separate.
 * But still hard to implement :/
 *
 * Each of these subseqs consist of prime, notprime and 1s,
 * assume notprime=0, prime=2 and 1s=1
 * Then we search for number of seqs
 * with at least one 1, and
 * exactly one 2, and
 * without 0
 *
 * Go from right to left, maintain positions of the next
 * items at right of current position.
 */
void solve() {
    cini(n);
    cini(e);

    vvi a(e);
    int idx=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux!=1) {
            if(notpr[aux])
                aux=0;
            else
                aux=2;
        }
        a[idx].push_back(aux);
        idx++;
        if(idx==e)
            idx=0;
    }

    int ans=0;
    for(int i=0; i<e; i++) {
        if(a[i].size()<2)
            continue;

        /* find number of subseqs without 0 and exactly one 2 
         **/
        int zero=a[i].size();
        int two=a[i].size();
        int two2=a[i].size();
        for(int j=a[i].size()-1; j>=0; j--) {
            if(a[i][j]==0)
                zero=j;

            if(a[i][j]==2) {
                two2=two;
                two=j;
            }

            if(a[i][j]==1)  {
                if(two<a[i].size())
                    ans+=max(0LL, min(two2,zero)-two);

            } else if(a[i][j]==2) {
                int r=min(two2,zero);
                if(j+1<r)
                    ans+=r-j-1;
            }
        }
    }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
