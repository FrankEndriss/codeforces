/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    cini(m);
    vvi d(n+1); /* d[i]=list of divisors of colider j */
    vi dcol(n+1);

    for(int i=2; i<=n; i++)
        for(int j=i; j<=n; j+=i)
            d[j].push_back(i);

    vb on(n+1);

    vi dvis(n+1);   /* dvis[i]=j -> dividor i locked by colider j */

    for(int i=0; i<m; i++) {
        cins(s);
        cini(k);

        if(s=="+") {    
            if(on[k]) {
                cout<<"Already on"<<endl;
            } else {
                int conf=0;
                
                for(int dd : d[k]) {
                    if(dvis[dd]>0) {
                        conf=dvis[dd];
                        break;
                    }
                }
                if(conf>0) {
                    cout<<"Conflict with "<<conf<<endl;
                } else {
                    cout<<"Success"<<endl;
                    for(int dd : d[k]) {
                        dvis[dd]=k;
                    }
                    on[k]=true;
                }
            }
        } else if(s=="-") {
            if(!on[k]) 
                cout<<"Already off"<<endl;
            else {
                cout<<"Success"<<endl;
                for(int dd : d[k]) 
                    dvis[dd]=0;
                on[k]=false;
            }
        }
    }
            
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
