/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * we need to remove vertices (and their edges)
 * so that no path is longer than 1.
 * This is, the left nodes are roots (with max 2
 * edges leaving), or leafs with no outgoing edges.
 *
 * Greedy
 * Let exist the vertex with most childs.
 * Remove the apropriate edges.
 * Repeat.
 */
void solve() {
    cini(n);
    cini(m);

    vector<set<int>> adj(n);
    vector<set<int>> adjR(n);
    for(int i=0; i<m; i++) {
        cini(x); x--;
        cini(y); y--;
        adj[x].insert(y);
        adjR[y].insert(x);
    }

    
    set<pii> deg;
    for(int i=0; i<n; i++) {
        deg.insert({ adj[i].size(), i });
    }

    vector<int> ans;
    while(deg.size()) {
        auto it=deg.rbegin();
        if(it->first==0)
            break;

        int v=it->second;
        deg.erase(*it);

        for(int c : adj[v]) {
            deg.erase({adj[c].size(), c});
            adj[c].clear();
        }
    

        for(int p : adjR[v]) {
            ans.push_back(p);
            deg.erase({adj[p].size(), p});
        }
    }

    sort(all(ans));
    ans.erase(unique(all(ans)), ans.end());
    cout<<ans.size()<<endl;
    for(auto p : ans) 
        cout<<p+1<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
