/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* simulate the board.
 * freeing cell again is a circle.
 * dfs.
 * Note that a cell can be freed in only one way,
 * but with a free cell, propably more than one cell can be 
 * freed.
 * Note that the stones can be placed in 3 positions only.
 * Pos of two free cells determine the whole board.
 *
 * How to efficiently implement simulation?
 * dfs, but the dominoes and positions?
 * model a domino by 
 * orig pos of L/D and current shift
 * have a second list with possible target pos and index into first list.
 *
 * Did not finish in time, but
 * brute force wont work anyway :/
 */
void solve() {
    cini(n);
    cini(m);

    
    vector<pii> hd; /* hd[i]=horizontal dominoes in ith row */
    vector<pii> vd; /* vd[j]=vertiacal dominoes in jth col */

    vector<vector<set<int>>> thd(n, vector<set<int>>(m)); /* thd[i][j]=set of hd dominoes movable to [i,j] */
    vector<vector<set<int>>> tvd(n, vector<set<int>>(m)); /* tvd[i][j]=set of vd dominoes movable to [i,j] */

    vs grid(n);
    for(int i=0; i<n; i++)
        cin>>grid[i];

    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            if(grid[i][j]=='L') {
                hd[i].push_back({i,0});
                if(j>0)
                    thd[i][j-1].insert(hd[i].size()-1);
                if(j+1<m)
                    thd[i][j+1].insert(hd[i].size()-1);
                }
            } else if(grid[i][j]=='D') {
                vd[j].push_back({j,0});
                if(i>0)
                    tvd[i-1][j].insert(vd[j].size()-1);
                if(i+1<n)
                    tvd[i+1][j].insert(vd[j].size()-1);
            }
        }
    }

    set<pair<pii,pii>> vis;   /* seen positions */

    function<void(pii,pii)> dfs=[&](pii f1, pii f2) {
        if(!vis.insert({f1,f2}))
            return;

/* we need to remove the dom from thd, maybe from other thd,
 * and insert it into new thd, maby two new thd.
 * then update shift in hd.
 */
        for(auto it : thd[f1.first][f1.second].begin(), it!=thd[f1.first][f1.second].end(); it++) {
            if(hd[*it].first<f1.second) {   //shift to R
                hd[*it].second++;
                if(hd[*it].second==0) {
                    if(f1.second<m)
                        thd[f1.first][f1.second+1].insert(*it);
                    if(f1.second-2>=0)
                        thd[f1.first][f1.second-2].insert(*it);
                    it=thd[f1.first][f1.second].erase(it);
                } else if(hd[*it].second==1) {
                    if(f1.second-2>=0)
                        thd[f1.first][f1.second-2].insert(*it);
                    it=thd[f1.first][f1.second].erase(it);
                }
            } else if(hd[*it].first>f1.second) { // shift to L
//...
            }
        }
    };
    

    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
