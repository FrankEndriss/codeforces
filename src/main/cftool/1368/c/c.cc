/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* n=1
 * xxx
 * x x
 * xxxxx
 *   x x
 *   xxx
 */
void solve() {
    cini(n);

    vvi g={
        { 0,1,2},
        { 0, 2},
        { 0,1,2}
    };

/*
    vs dbg(n*2+3);
    for(int i=0; i<dbg.size(); i++)
        while(dbg[i].size()<n*2+3)
                dbg[i]+=" ";
*/

    cout<<(n+1)*7+1<<endl;
    for(int i=0; i<3; i++) {
        for(int j : g[i]) {
            cout<<i<<" "<<j<<endl;
            //dbg[i][j]='x';
        }
    }

    int offI=2;
    int offJ=2;
    for(int nn=0; nn<n; nn++) {
        for(int i=0; i<3; i++) 
            for(int j : g[i])
                if(i!=0 || j!=0) {
                    cout<<i+offI<<" "<<j+offJ<<endl;
                    //dbg[i+offI][j+offJ]='x';
                }

        offI+=2;
        offJ+=2;
    }

//for(int i=0; i<dbg.size(); i++)
//    cerr<<dbg[i]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
