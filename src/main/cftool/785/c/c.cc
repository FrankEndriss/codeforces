/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

int gaus(int d) {
    return d*(d+1)/2;
}

/* @return the min d for which gaus(d) >=n */
int gausinv(int n) {
    int l=0;
    int r=2e9;

    while(l+1<r) {
        int mid=(l+r)/2;
        if(gaus(mid)<n)
            l=mid;
        else
            r=mid;
    }
    return r;
}

/* Lets find the first dayx the barn 
 * is not filled completly.
 * This is on day m+1, since there are comming
 * m+1 sparrows.
 * So after day m there are
 * n-m
 * And after day m+i
 * n-gaus(i)
 *
 * Binary search for i?
 */
void solve() {
    cini(n); // capacity
    cini(m); // dayly grow

    if(n<=m) { // barn gets filled completly everyday, so on nth day n sparrows make it empty
        cout<<n<<endl;
        return;
    }

    int i=gausinv(n-m);
    cout<<m+i<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
