/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * There are two cases:
 * first chess, then programming, or
 * the other way.
 * So, find l1 with smallest r, combine with l2 with biggest l,
 * and the other way.
 * So we need to maintain the min and max values.
 */
const int INF=1e9+7;
void solve() {
    cini(n);

    int l1mi=INF;
    int l1ma=-INF;
    int r1mi=INF;
    int r1ma=-INF;

    for(int i=0; i<n; i++) {
        cini(l);
        cini(r);
        l1mi=min(l1mi, l);
        l1ma=max(l1ma, l);
        r1mi=min(r1mi, r);
        r1ma=max(r1ma, r);
    }

    cini(m);
    int l2mi=INF;
    int l2ma=-INF;
    int r2mi=INF;
    int r2ma=-INF;

    for(int i=0; i<m; i++) {
        cini(l);
        cini(r);
        l2mi=min(l2mi, l);
        l2ma=max(l2ma, l);
        r2mi=min(r2mi, r);
        r2ma=max(r2ma, r);
    }

    int ans=0;
    ans=max(ans, l2ma-r1mi);
    ans=max(ans, l1ma-r2mi);
    cout<<ans<<endl;
}

signed main() {
        solve();
}
