/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that given some build, we can construct some more builds
 * by moving one of the slots one position down.
 * One of the so constructed builds will actually be the one
 * following the current one in the sorted-by-strength list.
 *
 * So, we maintain a priority-queue with current builds,
 * starting with the strongest build.
 *
 * Then we take the strongest from the Q. If it is not banned it is ans.
 * Else we construct the (at most n) next builds, and if not seen
 * before push them on the queue.
 */
void solve() {
    cini(n);
    vvi a(n);

    for(int i=0; i<n; i++) {
        cini(c);
        for(int j=0; j<c; j++) {
            cini(aa);
            a[i].push_back(aa);
        }
    }

    cini(m);
    set<vi> banned;
    for(int i=0; i<m; i++) {
        vi b(n);
        for(int j=0; j<n; j++) {
            cin>>b[j];
            b[j]--;
        }
        banned.insert(b);
    }

    vi b(n);
    int sum=0;
    /* strongest build */
    for(int i=0; i<n; i++)  {
        b[i]=a[i].size()-1;
        sum+=a[i][b[i]];
    }

    priority_queue<pair<int,vi>> q;
    q.push({sum,b});

    set<vi> vis;
    vis.insert(b);

    while(true)  {
        auto p=q.top();
        q.pop();

        if(banned.count(p.second)==0) {
            for(int i=0; i<n; i++)
                cout<<p.second[i]+1<<" ";
            cout<<endl;
            return;
        }

        vi bb=p.second;
        for(int i=0; i<n; i++) {
            if(bb[i]>0) {
                bb[i]--;
                if(vis.count(bb)==0) {
                    sum=p.first-a[i][bb[i]+1]+a[i][bb[i]];
                    q.push({sum,bb});
                    vis.insert(bb);
                }
                bb[i]++;
            }
        }
    }
    assert(false);

}

signed main() {
    solve();
}
