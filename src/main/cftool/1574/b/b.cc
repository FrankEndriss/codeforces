/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A letter with x pairs has at least x+1 occ and at most 2x.
 *
 * Let x[i] be number of continous groups of a[i].
 * The number of pairs in a[i] is a[i]-x[i]
 *
 * Let cnt=sum(a[])
 * We need to find a configuration of x[] so that...
 *
 * Consider the string like "aaaabbbcc"
 * If we move one 'a' between the b and c, we decrease the number of pairs by 1.
 * If we move one 'a' between two b, we decrease the number of pairs by 2.
 * And so on...its complecated.
 * How are the simple rules?
 *
 * Lets consider that greedy:
 * Sort, so that cnt(a)>=cnt(b)>=cnt(c)
 * Then 
 * -if there is an b pair take an a 
 * -put between two b
 * -if there is an c pair, take an a
 * -put between two c
 *
 *
 * Lets build "abcabcabc..."
 * The remaining is "abc...aaabb"
 *
 * That greedy should work
 */
void solve() {
    cinai(a,3);
    sort(all(a), greater<int>());
    cini(m);

    /* max number of pairs */
    const int p=a[0]-1+a[1]-1+a[2]-1;
    if(p<m) {
        cout<<"NO"<<endl;
        return;
    }

    if(a[0]-1>=a[1]+a[2]) {
        /* move all b and c between two a */
        int r=a[1]-1+a[2]-1+a[1]+a[2];
        if(r+m>=p)
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    } else {
        cout<<"YES"<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
