/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * One dragon attacks with x[i] power and y[i] defense.
 *
 * We have n heros with a[i] strength.
 *
 * Consider dragon i and sending hero j, then the costs are:
 * max(0LL, x[i]-a[j])  // we must increase strength of hero
 * + max(0LL, y[i]-(sum(a[])-a[j])) // we must increase strength of defenders
 * Cases:
 * y[i]>=sum(a[]) -> We can send any hero with a[j]<=x[i]
 * y[i]<sum(a[]) -> 
 * We want to send the hero with smallest strength possible to kill the dragon,
 * or the one smaller hero.
 **/
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);

    sort(all(a));
    const int sum=accumulate(all(a), 0LL);

    cini(m);    /* dragons */
    for(int i=0; i<m; i++)  {
        cini(x);
        cini(y);

        if(y>=sum) {
            int ans=y-(sum-a[0]) + max(0LL, x-a[0]);
            cout<<ans<<endl;
        } else {
            auto it=lower_bound(all(a), x);
            int ans=INF;
            if(it!=a.end()) {
                ans=min(ans, max(0LL, y-(sum-*it)));
            }
            if(it!=a.begin()) {
                it--;
                ans=min(ans, x-*it + max(0LL, y-(sum-*it)));
            }
            cout<<ans<<endl;
        }
    }


}

signed main() {
    solve();
}
