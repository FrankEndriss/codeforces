/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * for all pyramids:
 * V = 1/3 * A * h
 * where A is base area and h is hight.
 *
 * Base area pentagon:
 * 5 * 1/2 * l5 * tan(54) * l5/2
 *
 * https://de.wikipedia.org/wiki/Pyramide_(Geometrie)#Formeln
 * n=3
 * h=l3/3 * sqrt(6)
 *
 * n=4
 * h=l4/2 * sqrt(2)
 *
 * n=5
 * h=l5/10 * sqrt(50-10*sqrt(5))
 */
void solve() {
    cini(l3);
    cini(l4);
    cini(l5);

    double A3=sqrt(3)*l3*l3/4.0;
    double h3=l3/3.0 * sqrt(6);
    double V3=A3*h3/3;

    double A4=l4*l4;
    double h4=sqrt(2)*l4/2.0;
    double V4=A4*h4/3;

    double A5=tan(54.0/180*PI)*l5*l5*5/4.0;
    double h5=sqrt(50-10*sqrt(5))*l5/10.0;
    double V5=A5*h5/3;

    double ans=V3+V4+V5;
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
