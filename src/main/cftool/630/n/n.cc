/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* a*x^2 + b*x + c = 0
 *   double v1=(-b-sqrt(b*b-4*a*c))/(2*a);
 *   double v2=(-b+sqrt(b*b-4*a*c))/(2*a);
 */
void solve() {
    cind(a);
    cind(b);
    cind(c);

    double v1=(-b-sqrt(b*b-4*a*c))/(2*a);
    double v2=(-b+sqrt(b*b-4*a*c))/(2*a);
    cout<<setprecision(8)<<fixed<<max(v1,v2)<<endl<<min(v1,v2)<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

