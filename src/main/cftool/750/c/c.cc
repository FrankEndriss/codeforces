/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

const int D1=1900;

/* find possible min/max at begin, then add all changes.
 * On every r-change there is a div, which gives an upper or lower bound
 * of current rating. Subtract the prefix so far.
 *
 * Note the misleading statement: Ratings can be negative here, too!
 */
void solve() {
    cini(n);
    int mi=-1e18;
    int ma=1e18;
    int sum=0;
    for(int i=0; i<n; i++) {
        cini(c);
        cini(d);

        if(d==1)
            mi=max(mi, D1-sum);
        else
            ma=min(ma, D1-1-sum);

        sum+=c;
    }

    if(mi>ma)
        cout<<"Impossible"<<endl;
    else if(ma>(int)1e9)
        cout<<"Infinity"<<endl;
    else
        cout<<ma+sum<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
