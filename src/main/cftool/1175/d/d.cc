/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Each element count for itself.
 * We can somehow choose for some elements in which
 * multiplier they belong.
 * it is clear that for i<j f(i)<=f(j)
 *
 * a[0] belongs to first segment,
 * a[n-1] to the last.
 * Consider k==2
 * For elements 1..n-2 we can choose.
 * If there is a positive postfix, then it is better
 * to double the contribution of this postfix.
 * So find the biggest postfix (>0), and use that for the last segment.
 *
 * Consider k=big
 * Same strategy, choose biggest possible postfix for the biggest
 * multiplied segment.
 * Then continue with the remaining part.
 * How to implement?
 * Simple implementation would be O(k^2).
 * But can we start at both ends?
 * Because if it goes very slow from one end, it goes fast from the other.
 * We can use a segment tree since we need to calculate the prefix array
 * only once.
 *
 * What about positive and negative minimum?
 * We want to put anything into the first segment that would be worse
 * in higher multiple segments. That is the minimum prefix sum that is 
 * less than zero.
 * What if there is none?
 * Then there are postfix-sums that are > 0, choose the biggest of them 
 * for the last segment.
 *
 * Implement using two prefix arrays (from both ends) and put them into
 * segment trees. Make it possible to search for min/max and the index
 * of that value.
 * *****
 * No, see tutorial.
 * We need to choose the position 1, and positions of k-1 other biggest suffix sums.
 * Because:
 * let p[i] be the postfix sum of postfix of len n-i, and b[i] be begin of segment i,
 * so b[1]=1
 * sumall=1*(p[b[1]]-p[b[2]]) + 2*(p[b[2]]-p[b[3]]) + ... + k*p[b[k]]-0
 * =p[b[1]] + p[b[2]] + p[b[3]] + ... + p[b[k]]
 */
void solve() {
    cini(n);
    cini(k);

    cinai(a,n);
    vi pre(n+1);
    for(int i=n-1; i>=0; i--)
        pre[i]=pre[i+1]+a[i];

    sort(pre.begin()+1, pre.end()-1, greater<int>());

    int ans=accumulate(pre.begin(), pre.begin()+k, 0LL);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
