/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* idk, see https://en.wikipedia.org/wiki/Eulerian_path#Hierholzer's_algorithm
 * But it is unclear how to calculate the (l,r) constraint.
 *
 * First, we need to find kind of a proof on how to create lex min path.
 * ->
 * 121314..1n1  of len 2*(n-1)+1
 * and before last 1, we insert
 * 232425..2n
 *
 */
void solve() {
    cini(n);
    cini(l);
    cini(r);

    int pcnt=0;
    int cnt=0;
    for(int i=1; i<n; i++) {
        cnt+=2*(n-i);
        while(l<=r && l<=cnt) {
            if(l&1)
                cout<<i<<" ";
            else {
                int idx=(l-pcnt)/2;
                cout<<idx+i<<" ";
            }
            l++;
        }
        pcnt=cnt;

        if(l>r)
            break;
    }
    if(l<=r)
        cout<<"1";
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

