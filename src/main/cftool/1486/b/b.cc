/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Find min dist as
 * median of x, median of y
 * Then construct all possible median locations.
 * Go left and right as long as possible.
 * Go up and down as possible.
 * ans=product
 */
void solve() {
    cini(n);
    vi x(n);
    vi y(n);

    for(int i=0; i<n; i++) {
        cin>>x[i]>>y[i];
    }
    sort(all(x));
    sort(all(y));

    int minX=n/2;
    int maxX=n/2;
    if(n%2==0) {
        minX--;
    }

    int minY=n/2;
    int maxY=n/2;
    if(n%2==0)
        minY--;

    int ans=(x[maxX]-x[minX]+1) * (y[maxY]-y[minY]+1);
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
