/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Brute force would be to iterate all pairs i,j (j>=i+k)
 * maintaining the median of the current list,
 * running in O(n^2 logn)
 *
 * Somehow binsearch?
 * Consider we want to find a subarray with median >= x
 * Then that subarray must have at least len/2 numbers a[i]>=x
 *
 * Consider pre[i] be the sum of prefix of len i
 * Then the sum of subarray i,j is 
 * pre[j+1]-pre[i]
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    function<bool(int)> go=[&](int x) {
        vi pre(n+1);
        for(int i=0; i<n;i++)  {
            if(a[i]>=x)
                pre[i+1]=pre[i]+1;
            else
                pre[i+1]=pre[i]-1;
        }
        int mi=0;
        int ma=-INF;
        for(int i=k; i<=n; i++) {
            mi=min(mi, pre[i-k]);
            ma=max(ma, pre[i]-mi);
        }
        return ma>0;
    };

    int l=0;
    int r=n+1;
    while(l+1<r) {
        int mid=(l+r)/2;
        if(go(mid))
            l=mid;
        else
            r=mid;
    }
    cout<<l<<endl;
}

signed main() {
    solve();
}
