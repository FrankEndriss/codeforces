/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We would like to transform the graph 
 * into an graph haveing the calculated
 * connections.
 * Just do that?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    vector<vector<pii>> adj1(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        cini(w);
        u--; v--;
        adj1[u].emplace_back(v,w);
        adj1[v].emplace_back(u,w);
    }
    //cerr<<"parsed input"<<endl;

    vector<unordered_map<int,int>> adj2(n);
    for(int i=0; i<n; i++) {
        for(pii chl : adj1[i]) {
            for(pii chl2 : adj1[chl.first]) {
                if(chl2.first==i)
                    continue;

                auto it=adj2[i].find(chl2.first);
                if(it==adj2[i].end())
                    adj2[i][chl2.first]=(chl2.second+chl.second)*(chl2.second+chl.second);
                else
                    it->second=min(it->second, (chl2.second+chl.second)*(chl2.second+chl.second));
            }
        }
    }
    //cerr<<"optimized graph"<<endl;

    vi dp(n, INF);
    dp[0]=0;
    priority_queue<pii> q;
    q.emplace(0, 0);
    while(q.size()) {
        auto [p,v]=q.top();
        q.pop();
        if(-p!=dp[v])
            continue;

        for(pii chl: adj2[v]) {
            if(dp[chl.first]>dp[v]+chl.second) {
                dp[chl.first]=dp[v]+chl.second;
                q.emplace(-dp[chl.first], chl.first);
            }
        }
    }
    //cerr<<"did dijrkstra"<<endl;

    for(int i=0; i<n; i++) 
        if(dp[i]==INF)
            cout<<-1<<" ";
        else
            cout<<dp[i]<<" ";

    cout<<endl;

}

signed main() {
    solve();
}
