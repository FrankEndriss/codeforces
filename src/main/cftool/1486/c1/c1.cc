/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int q(int l, int r) {
    cout<<"? "<<l<<" "<<r<<endl;
    int ans;
    cin>>ans;
    return ans;
}

/*
vi mydata={ 7, 5, 3, 2, 4, 6 };
int q(int l, int r) {
    assert(l<r);
    vi ldata;
    for(int i=l; i<=r; i++)
        ldata.push_back(mydata[i-1]);
    sort(all(ldata));
    for(int i=l; i<=r; i++) 
        if(mydata[i-1]==ldata[ldata.size()-2])
            return i;
    assert(false);
}
*/

/* with last query we would like to query two biggest elements,
 * and the not returned one is ans.
 *
 * Note that we will never get the biggest element as result.
 *
 * So first find position of second biggest, then 
 * find half with the biggest.
 * Then binsearch position of biggest.
 */
void solve() {
    cini(n);
    // test
    //n=(int)mydata.size();
    int sec=q(1,n);
    int lsec=-1;
    if(sec>1)
        lsec=q(1,sec);

    if(lsec==sec) { /* max in 1,sec-1 */
        int l=1;
        int r=sec-1;
        while(l!=r) {
            int mid=(l+r)/2+1;
            int pos=q(mid,sec);
            cerr<<"left"<<endl;
            cerr<<"l="<<l<<" r="<<r<<" mid="<<mid<<" sec="<<sec<<" pos="<<pos<<endl;
            if(pos==sec) {
                l=mid;
            } else {
                r=mid-1;
            }
        }
        cout<<"! "<<l<<endl;
    } else {    /* max in sec+1, n */
        int l=sec+1;
        int r=n;
        while(l!=r) {
            int mid=(l+r)/2;
            int pos=q(sec, mid);
            cerr<<"right"<<endl;
            cerr<<"l="<<l<<" r="<<r<<" sec="<<sec<<" mid="<<mid<<" pos="<<pos<<endl;
            if(pos==sec) {
                r=mid;
            } else {
                l=mid+1;
            }
        }
        cout<<"! "<<l<<endl;
    }
}

signed main() {
    solve();
}
