/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Its one of the ...idk... methods,
 * simplex, or something.
 *
 * Or some kind of dp?
 *
 * dp[i]=max number of task finishable at time i.
 *
 * ...more greedy.
 * Assign workers to 1-cost tasks...somehow?
 *
 * We want to minimize the sum of times the
 * max worker works.
 * Binary search?
 * M
 * How to check if we can finish in y time?
 *
 * Try to assign each task its proficient worker.
 * That might make some worker work to much.
 * Maintain a queue of other and finished worker,
 * which can do jobs for cost of 2.
 * Assign them to later scheduled jobs.
 * Note that ans<=4e5
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,m);

    vvi w(n, vi(1,0));
    for(int i=0; i<m; i++) {
        w[a[i]-1].push_back(w[a[i]-1].size());
    }

    set<pii> s; /* max end of worker, workerid */
    for(int i=0; i<n; i++)
        s.emplace(w[i].back(), i);

    while(true) {
        auto itL=s.begin();
        auto itR=s.end();
        itR--;
        if(itL->first+2<itR->first) {
            auto [e1, i1]=*itL;
            auto [e2, i2]=*itR;

            s.erase(itL);
            s.erase(itR);
            w[i1].push_back(e1+2);
            s.emplace(e1+2, i1);
            w[i2].pop_back();
            s.emplace(w[i2].back(), i2);
        } else {
            break;
        }
    }
    auto it=s.end();
    it--;
    cout<<it->first<<endl;;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
