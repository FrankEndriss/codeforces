/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Should be also doable by max-flow.
 * But also, number of edges can be to big :/
 * Consider b[] includes a lot of 0.
 * ...
 * At each position we can calculate a segment 
 * of numbers that can be used there.
 *
 * So then we can go from left to right, adding 
 * all segments starting at current position,
 * and using the segment with the smallest r.
 * Repeat.
 *
 * How to calculate smallest and biggest possible value
 * at position i?
 *
 * Consider position i=2,
 * Then 
 * b[i]=0 -> a[i]=i+1 .. n
 * b[i]=1 -> a[i]=2 .. 2
 * b[i]=2 -> a[i]=1 .. 1
 * b[i]=3 -> impossible
 *
 * i=17
 * b[i]=0 -> a[i]=i+1 .. n
 * b[i]=1 -> a[i]=9..17
 * b[i]=2 -> a[i]=6..8
 * b[i]=3 -> a[i]=...
 *
 * i=15
 * b[i]=1 -> a[i]=8..15
 * b[i]=2 -> a[i]=6..7
 * b[i]=3 -> a[i]=4..5
 * b[i]=4 -> impossible
 * ...
 */
void solve() {
    cini(n);
    cinai(b,n);

    using t3=tuple<int,int,int>;    /* min, max, position */
    vector<t3> s(n);

    for(int i=0; i<n; i++) {
        int ma=n;
        int mi=1;
        if(b[i]>0)
            ma=(i+1)/b[i];

        mi=(i+1)/(b[i]+1)+1;

        //cerr<<"i+1="<<i+1<<" b[i]="<<b[i]<<" mi="<<mi<<" ma="<<ma<<endl;
        s[i]={
            mi,   /* smallest possible value */
            ma,   /* biggest possible value */
            i
        };
    }

    sort(all(s));
    set<pii> open;
    int idx=0;
    vi ans(n);
    for(int i=0; i<n; i++) {
        while(idx<n) {
            auto [l,r,v]=s[idx];
            if(l>i+1)
                break;
            open.emplace(r,v);
            idx++;
        }

        assert(open.size());
        auto it=open.begin();
        auto [r,v]=*it;
        open.erase(it);
        ans[v]=i+1;
    }

    /* double check */
    /*
    for(int i=0; i<n; i++) {
        if(ans[i]/(i+1)!=b[i]) 
            cerr<<"ans[i]="<<ans[i]<<" i="<<i<<" b[i]="<<b[i]<<endl;

        assert((i+1)/ans[i]==b[i]);
    }
    */

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
