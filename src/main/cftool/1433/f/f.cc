/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vvd= vector<vd>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * per row max m/2 elements, 
 * sum of all choosen elements
 * must be divi by k, and max possible.
 *
 * Lets choose per row max possible numbers, that is
 * the m/2 biggest ones.
 * Then we got a diff to next smaller multiple of k, ie
 * diff=sum%k.
 * Try to find changes per line so that the sum of changes == diff
 *
 * How to find that, knapsack somehow?
 * Per row we can choose x=1..m/2 elements to change agains x other 
 * elements. 
 * If x==1 then there are m/2 * m/2 possible swap pairs.
 * if x==2 then there are (m/2*(m/2-1))^2 possible swap pairs.
 * ... to much
 * And not that we can just remove the numbers, too.
 *
 * But the values are all leq 70... 
 * So the sum over all cannot be bigger than 70 * (n/2) * m ~ 150000
 * And k<=70, too.
 *
 * Dp states:
 * dp0[i][j]=true if i is possible sum with j summands
 * from that get
 * dp1[i][j]=max sum after row i with remainder j
 *
 */
const int N=70;
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vi dp0(k, -1);   /* dp0[i]= max sum with remainder i after current row */
    dp0[0]=0;
    for(int i=0; i<n; i++) {
        /* dp[j][k]=true if after j elements used sum k is possible */
        vector<bitset<N*N/2+1>> dp(m/2+1);
        dp[0][0]=true;

        for(int idx=0; idx<m; idx++) {
            cini(aux);
            for(int j=m/2; j>0; j--)
                dp[j]=dp[j] | (dp[j-1]<<aux);
        }
        for(int j=1; j<=m/2; j++)
            dp[0] |= dp[j];


        vi dp1(k, -1);
        for(int j=0; j<=N*N/2; j++) {
            if(dp[0][j])
                dp1[j%k]=j;
        } 

        vi dp2(k,-1);
        for(int j=0; j<k; j++) {
            if(dp0[j]<0)
                continue;

            for(int jj=0; jj<k; jj++) {
                if(dp1[jj]<0)
                    continue;

                dp2[(j+jj)%k]=max(dp2[(j+jj)%k], dp0[j]+dp1[jj]);
            }
        }
        dp0=dp2;
    }

    cout<<dp0[0]<<endl;


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
