/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int fac(int i) {
    assert(i>0);
    if(i==1)
        return 1;
    else
        return i*fac(i-1);
}

/* First find number of possible distributions into two groups.
 * Then find number of different distributions foreach group.
 *
 * Assume person 1 is always in first group to get rid of 
 * mirrored groups.
 * Let n2=n/2
 * Since there are n2 group members we got
 * 1*(n-1) * (n-2) * ... * (n-n/2+1)
 *
 * Then, a group of k people. Assume the circle starts with
 * person 1 to get rid of cylces.
 * if k>2 the second person can go in two directions.
 * if(k<=2) it is only 1 distribution. Else
 * 1*(k-1)*(k-2)*...*1
 * So (k-1)! circle distributions, in each circle, multiply.
 */
void solve() {
    cini(n);    /* even */

    int ans=1;
    for(int i=n-1; i>n/2; i--) 
        ans*=i;

    if(n<=4) {
        cout<<ans<<endl;
        return;
    }

    for(int k=n/2-1; k>1; k--)
        ans*=k;

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
