/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* casework
 * if b>=a (foot better than drive) it is better to always walk.
 * if a>b but t+(2*k*a)>2*k*b it is still better to always walk,
 * except the first k interval.
 *
 * if(t+k*a<k*b) it is better to drive, and last interval maybe
 * better to walk.
 *
 * What if t+k*a > k*b??
 *
 *
 * **********
 * let r(d) be point left of d where the car did break down
 * the last time. (note r(d)%k==0)
 * let tt(d) be time to point d.
 * Then the tt(r(d)) = (r(d)/k-1)*t + r(d)*a + min(t+d-r(d)*a, d-r(d)*b)
 *
 * But it could still be better to use the previus break-point...hmmm.
 *
 * Consider starting at first breakpoint.
 * On every breakpoint we can choose to repair the car
 * once again, or walk the remaining distance.
 * This is dp.
 * But we have to much possible breakpoints
 *
 * There are two cases:
 * 1. k*a+t>=k*b
 * It is better to always walk.
 * 2. k*a+t<k*b
 * It is better to always repair.
 */
void solve() {
    cini(d);
    cini(k);
    cini(a);
    cini(b);
    cini(t);

    if(d<=k) {  /* simple case */
        cout<<min(d*a, d*b)<<endl;
        return;
    }

    /* first segment */
    int ans=0;
    d-=k;
    ans+=k*min(a,b);

    if(k*a+t>=k*b) {    /* case 1 */
        ans+=d*b;
    } else {            /* case 2 */
        ans+=(d/k)*(t+a*k);
        ans+=min((d%k)*b, t+(d%k)*a);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
