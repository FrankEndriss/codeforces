/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

struct Dsu {
    vector<int> p;
    vector<int> s;
    /* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        s.resize(size);
        iota(p.begin(), p.end(), 0);
        for(int i=0; i<size; i++)
            s[i]=1;
    }

    /* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

    /* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

    /* combines the sets of two members.
     *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            s[a]+=s[b];
        }
    }
};

int npair(int i) {
    return i*(i-1)/2;
}

/* We need to find the sizes of the components
 * in the tree if all adges with weight > q[i] are removed.
 * How to do that?
 * Process queries offline sorted by weight.
 * before every q add all edges up to its weight.
 * maintain number of pairs.
 */
void solve() {
    cini(n);
    cini(m);

    vector<pair<int,pii>> edges(n-1);

    for(int i=0; i<n-1; i++)
        cin>>edges[i].second.first>>edges[i].second.second>>edges[i].first;

    sort(all(edges));

    vector<pii> q(m);
    for(int i=0; i<m; i++) {
        cin>>q[i].first;
        q[i].second=i;
    }
    sort(all(q));

    Dsu dsu(n);

    int ans=0;
    int idx=0;
    for(int i=0; i<m; i++) {
        while(idx<edges.size() && edges[idx].first<=q[i].first) {
            int s1=dsu.find_set(edges[idx].second.first-1);
            int s2=dsu.find_set(edges[idx].second.second-1);
            ans-=npair(dsu.s[s1]);
            ans-=npair(dsu.s[s2]);
            ans+=npair(dsu.s[s1]+dsu.s[s2]);
            dsu.union_sets(edges[idx].second.first-1, edges[idx].second.second-1);
            idx++;
        }
        q[i].first=ans;
    }
    for(int i=0; i<m; i++)
        swap(q[i].first, q[i].second);
    sort(all(q));
    for(int i=0; i<m; i++)
        cout<<q[i].second<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
