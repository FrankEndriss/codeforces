/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

string ans1(char c1, char c2, char c3, int n) {
    string a;
    a+=c1;
    a+=c2;
    a+=c3;
    string ans;
    for(int i=0; i<n; i++) 
        ans+=a;
    return ans;
}

string ans2(char c1, char c2, char c3, int n) {
    return string(n,c1)+string(n,c2)+string(n,c3);
}

/* There are several cases, we tacle them all by brute force.
 */
void solve() {
    cini(n);
    cinas(s,2);

    vi p={0, 1, 2};
    string abc="abc";
    do {
        string a1=ans1(abc[p[0]], abc[p[1]], abc[p[2]], n);
        string a2=ans2(abc[p[0]], abc[p[1]], abc[p[2]], n);
        bool ok1=true;
        bool ok2=true;
        for(size_t i=0; i+1<3*n; i++) {
            if((a1[i]==s[0][0] && a1[i+1]==s[0][1]) || (a1[i]==s[1][0] && a1[i+1]==s[1][1]))
                ok1=false;
            if((a2[i]==s[0][0] && a2[i+1]==s[0][1]) || (a2[i]==s[1][0] && a2[i+1]==s[1][1]))
                ok2=false;
        }
        if(ok1) {
            cout<<"YES"<<endl;
            cout<<a1<<endl;
            return;
        }
        if(ok2) {
            cout<<"YES"<<endl;
            cout<<a2<<endl;
            return;
        }
    }while(next_permutation(all(p)));

    cout<<"NO"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
