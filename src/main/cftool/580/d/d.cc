/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to find the best order for m dishes.
 * So there are two dimensions of the problem:
 * Which m dishes, and in what order.
 * Since m,n<=18 it seems to be some bitmask thing.
 *
 * How to brute force this?
 * There are 2^m different possible sets of dishes we can choose,
 * and each cost can be calculated by checking all the possible
 * submasks.
 *
 * dp[i][j]= best val we can get by choosing the i dishes and last one is j
 * This is O(2^18 * 18), and runtime O(2^18 * 18*18/2) steps.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    cinai(a,n);

    vvi c(n, vi(n));    /* the cost matrix */
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            c[i][j]=a[j];

    for(int i=0; i<k; i++) {
        cini(x); x--;
        cini(y); y--;
        cini(aux);
        c[x][y]+=aux;
    }

    /* dp[i][j]= best val we can get by choosing the i dishes and last one is (1<<j) */
    vvi dp(1<<n, vi(n, -1));
    for(int i=0; i<n; i++) 
        dp[1<<i][i]=a[i];

    function<int(int,int)> go=[&](int ii, int jj) {
        if(dp[ii][jj]>=0)
            return dp[ii][jj];
        if((ii&(1<<jj))==0)
            return dp[ii][jj]=0;

        const int prev=ii^(1<<jj);  /* prev state */
        assert(prev!=ii);
        for(int i=0; i<n; i++)     /* i= last one added to prev */
            if(prev&(1<<i))
                dp[ii][jj]=max(dp[ii][jj], go(prev,i)+c[i][jj]);

        dp[ii][jj]=max(dp[ii][jj], 0LL);
        return dp[ii][jj];
    };

    int ans=0;
    for(int i=0; i<(1<<n); i++) 
        if(__builtin_popcount(i)==m) 
            for(int j=0; j<n; j++)
                ans=max(ans, go(i, j));

    cout<<ans<<endl;
}

signed main() {
    solve();
}
