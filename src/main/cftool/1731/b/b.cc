
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * right, then down:
 * n! * n^n * n!
 *
 * right, down, right, down,... seems to be optimal
 *
 * 1*(0+1) + 2*(1+2) + 3*(2+3) + 4*(3+4) + ... + n*(n-1+n)
 * sum(i=1..N, i*i) + sum(i=1..N, i*(i-1))
 *
 * How to calc these sums?
 *
 *
 * Lets visualize for n=3:
 *
 * 1    2   3 
 * 2    4   6
 * 3    6   9
 *
 * ...idk
 * *** 
 * Just calc for some small n, then search on OEIS.
 * -> https://oeis.org/A002412
 *  a(n) = n(n + 1)(4n - 1)/6
 */
void test() {
    int sum=1;
    for(int i=1; i<=10; i++)  {
        cerr<<sum<<" ";
        sum+=(i*(i+1));
        sum+=(i+1)*(i+1);
    }
    cerr<<endl;
}

using mint=modint1000000007;

mint a(mint n) {
    return n*(n+1)*(4*n-1)/6;
}

void solve() {
    cini(n);

    mint ans=a(n)*2022;
    cout<<ans.val()<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
