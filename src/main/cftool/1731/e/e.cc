
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How much pairs of numbers 1..n exist for gcd(i,j)==p
 * n/p ???
 *
 * Obviously i=y*p, ans j>i, so there are
 * n/p pairs with i==p, and
 * (n/p)/2 pairs with i==p*2, and
 * (n/p)/3 pairs with i==p*3, and
 * ...
 * But note that since we can choose k only if there are at least
 * k+1 edges, consider k=sqrt(n)+1, then there are
 * n/k pairs with i==k, but this is less than k
 * ...but also n/2k with i==2k
 * That seems to be somethine like e*sqrt(n) at most for i.
 *
 * Also, the bigger k is, the cheaper it gets.
 *
 * So just calc the number for all possible k, and 
 * choose the cheapest ones possible.
 * For big M we want to start with biggest possible k, else at M.
 *
 * But how to calc the number of pairs quickly?
 * -> See the Moebius function
 */
void solve() {
    cini(n);
    cini(m);

    int ans=0;
    const int kk=max(20LL, sqrt(n)*3); /* aprox euler constant */
    for(int i=min(kk-1,m); i>=2; i--) {
        /* try to choose k=i-1 */
        int d=0;    /* number of pairs with gcd(k,j)==i k<j */
        for(int j=i; j<=n; j+=i) {
            // TODO
        }
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
