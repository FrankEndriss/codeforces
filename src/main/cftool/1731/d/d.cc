
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It seems to be some dp
 * Observe that once we found some l, we need to check only bigger j>l.
 */
void solve() {
    cini(n);
    cini(m);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) 
            cin>>a[i][j];

    function<bool(int)> go=[&](int l) {

        /* rowwise window sum over window of size l */
        vvi bb(n, vi(m));
        for(int i=0; i<n; i++) {
            int sum=0;
            for(int j=0; j<m; j++)  {
                sum+=(a[i][j]>=l);
                if(j>=l)
                    sum-=(a[i][j-l]>=l);
                bb[i][j]=sum;
            }
        }

        /* colwise window sum over window of size l */
        for(int j=0; j<m; j++) {
            int sum=0;
            for(int i=0; i<n; i++)  {
                sum+=bb[i][j];
                if(i>=l)
                    sum-=bb[i-l][j];

                if(sum==l*l)
                    return true;
            }
        }

        return false;
    };

    int l=0; 
    int r=min(n,m)+1;

    while(l+1<r) {
        int mid=(l+r)/2;
        if(go(mid)) {
            l=mid;
        } else
            r=mid;
    }
    cout<<l<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
