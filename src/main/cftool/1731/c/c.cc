
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that all numbers have even number of divisors,
 * except quads.
 *
 * So how to find the number of subarrays with XORsum is a quad?
 *
 * Consider the LSB of a quad n*n, if n is odd b==1, 
 * else if n is even b==0
 * Consider second bit: 
 *
 * Note that a[i] has only ~25 bits.
 * So there at most sqrt(2e5) different quads.
 *
 * How does it help?
 * ******
 * Note that we can find the number of xorsums eq to some
 * fixed number k goes in O(n), by checking all prefixes
 * (and count the number of allready seen prefixes with p[i]^k
 *  make sure to also count the empty prefix).
 *
 * We can do this for all sqrt(2e5) possible quads in O(n*sqrt(n))
 */
void solve() {
    cini(n);
    cinai(a,n);

    const int N=2*n+7;
    vi cnt(N);

    int p=0;
    cnt[p]++;

    int ans=0;

    for(int i=0; i<n; i++) {
        p=p^a[i];
        for(int s=0; s*s<N; s++)
            if((p^(s*s))<N)
                ans+=cnt[p^(s*s)];
        cnt[p]++;
    }
    
    ans=n*(n+1)/2-ans;
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
