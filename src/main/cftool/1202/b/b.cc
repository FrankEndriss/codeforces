/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * greedy for each pair of positions find the number
 * of min numbers we would have to insert there.
 * Since there are only 100 pairs we should use
 * a cache.
 *
 * And note that order of x,y does not matter.
 *
 * How to calc one value?
 */
void solve() {
    cins(s);

    vi freq(10);
    for(size_t i=1; i<s.size(); i++) {
        int val=s[i]-s[i-1];
        if(val<0)
            val+=10;
        freq[val]++;
    }

    function<int(int,int)> calc=[&](int x, int y) {
        vi dp(10, -1);
        queue<int> q;
        q.push(x);
        dp[x]=0;
        q.push(y);
        dp[y]=0;
        while(q.size()) {
            int cur=q.front();
            q.pop();

            if(dp[(cur+x)%10]<0) {
                q.push((cur+x)%10);
                dp[(cur+x)%10]=dp[cur]+1;
            }
            if(dp[(cur+y)%10]<0) {
                q.push((cur+y)%10);
                dp[(cur+y)%10]=dp[cur]+1;
            }
        }

        int ans=0;
        for(int diff =0; diff<10; diff++) {
            if(freq[diff]==0)
                continue;

            int val=dp[diff];
            if(val==-1)
                return val;
            ans+=val*freq[diff];
        }
        return ans;
    };

    vvi ans(10, vi(10));
    for(int i=0; i<10; i++)
        for(int j=i; j<10; j++) {
            ans[i][j]=ans[j][i]=calc(i,j);
        }

    for(int i=0; i<10; i++)  {
        for(int j=0; j<10; j++) {
            cout<<ans[i][j]<<" ";
        }
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
