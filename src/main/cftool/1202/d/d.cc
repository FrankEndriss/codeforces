/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Start with "1337"
 * append 7 adds number of '133'
 * insert '3' before last 7 increases num 133 by num '1'
 * append '37' add number of '13'
 * append '337' add number of '1'
 * ...
 * Adding 1 by inserting '7' after first '133'
 * Adding 2 by inserting '77' after first '133'
 * Adding 3 by inserting '7' after first '1333'
 * ...
 * 1337 -> 1
 * 13337 -> 2+1=3
 * 133337 -> 3+2+1=6
 * ...
 * So we add 3s until no more possible, then we
 * add 7s between the 3 until finished.
 *
 * Then construct the string.
 */
void solve() {
    cini(n);
    int x3=2;
    int val=1;
    while(val+x3<=n) {
        val+=x3;
        x3++;
    }

    int s7=n-val;

    cout<<"133";
    for(int i=0; i<s7; i++)
        cout<<'7';
    for(int i=0; i+2<x3; i++)
        cout<<'3';
    cout<<"7"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
