/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

/* if k==1 all possible strings count, ie ans=m^n
 * if 1 < k < n all one letter strings count, ie ans=m
 * if k==n all palindromes of size n count, ie m^((n+1)/2)
 *
 * Note for k>n: There are 0 substrings longer than the string itself,
 * so all substrings are palindromes. So all strings are ok.
 * Case odd k: Strings of form "ababa..." are ok.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    int ans;
    if(k==n)
        ans=toPower(m, (n+1)/2);
    else if(k==1 || k>n)
        ans=toPower(m, n);
    else if(k>1 && k<n) {
        if(k%2==0)
            ans=m;
        else
            ans=toPower(m,2);
    } else if(k>n)
        ans=0;
    else
        assert(false);

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
