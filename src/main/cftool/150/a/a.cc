/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* first who _cant_ make a move wins.
 * So, winning position is to let only one move available,
 * ie letting two divisors available, ie have more than two
 * available.
 * If no div at all, ans=0
 * if 2 ans=2
 * if 3 or more ans=1, and move is product of all removed divs,
 * ie n/d[0]
 */
void solve() {
    cini(n);
    vi div;
    int nn=n;
    for(int i=2; i*i<=nn; i++) {
        while(nn%i==0) {
            div.push_back(i);
            nn/=i;
        }
    }
    if(nn>1 && nn!=n)
        div.push_back(nn);
    
    assert(div.size()!=1);

    if(div.size()==0) {
        cout<<1<<endl;
        cout<<0<<endl;
    } else if(div.size()==2)
        cout<<2<<endl;
    else {
        cout<<1<<endl;
        cout<<div[0]*div[1]<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS, SOLVE ONE AFTER THE OTHER
