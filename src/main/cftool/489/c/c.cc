/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Greedy
 * smallest:
 * put max possible digit into last position, dec position.
 *
 * biggest:
 * put max possible digit into first position, inc position.
 */
void solve() {
    cini(m);
    cini(s);

    if(s==0) {
        if(m==1)
            cout<<"0 0"<<endl;
        else
            cout<<"-1 -1"<<endl;
        return;
    }
    if(s>9*m) {
        cout<<"-1 -1"<<endl;
        return;
    }

    int ss=s;
    string ans(m,'x');
    for(int i=m-1; i>=1; i--) {
        int d=min(ss-1, 9LL);
        ans[i]='0'+d;
        ss-=d;
    }
    ans[0]='0'+ss;

    ss=s;
    string ans2(m, '9');
    for(int i=0; i<m; i++) {
        int d=min(ss, 9LL);
        ans2[i]='0'+d;
        ss-=d;
    }

    cout<<ans<<" "<<ans2<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
