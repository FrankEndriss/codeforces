/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Min number.
 * If all pathlen are odd or n=2 then mi=1
 * else mi=3
 *
 * max== n-1 - number of leaf siblings
 */
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(a); a--;
        cini(b); b--;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    vi d(n);
    function<void(int,int)> dfs=[&](int v, int p) {
        if(p>=0)
            d[v]=d[p]+1;
        else
            d[v]=0;

        for(auto chl : adj[v]) {
            if(chl!=p) {
                dfs(chl,v);
            }
        }
    };

    for(int i=0; i<n; i++) {
        if(adj[i].size()==1) {
            dfs(i, -1);
            break;
        }
    }

    int hasOdd=false;
    for(int i=0; i<n; i++) 
        if(adj[i].size()==1 && d[i]%2==1)
            hasOdd=true;

    int ansmi=0;
    if(!hasOdd)
        ansmi=1;
    else
        ansmi=3;

/* count leaf siblings. for every non-leaf count leaf childs-1. */
    int ansma=0;
    
    for(int i=0; i<n; i++) {
        if(adj[i].size()>1) {
            int cnt=0;
            for(int c : adj[i])
                if(adj[c].size()==1)
                    cnt++;

            ansma+=max(0LL, cnt-1);
        }
    }
    

    cout<<ansmi<<" "<<n-1-ansma<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
