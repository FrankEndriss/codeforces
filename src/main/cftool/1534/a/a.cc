/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    cini(m);

    cinas(s,n);

    queue<pii> q;
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            if(s[i][j]!='.')
                q.emplace(i,j);
        }
    if(q.size()==0) {
        q.emplace(0,0);
        s[0][0]='W';
    }

    const vi dx={ 0, 0, -1, 1 };
    const vi dy={ 1, -1, 0, 0 };
    while(q.size()) {
        auto [i,j]=q.front();
        q.pop();

        for(int k=0; k<4; k++) {
            const int ii=i+dx[k];
            const int jj=j+dy[k];
            if(ii>=0 && ii<n && jj>=0 && jj<m) {
                if(s[ii][jj]!='.' && s[ii][jj]==s[i][j]) {
                    cout<<"NO"<<endl;
                    return;
                }
                if(s[ii][jj]=='.') {
                    q.emplace(ii,jj);
                    if(s[i][j]=='R')
                        s[ii][jj]='W';
                    else if(s[i][j]=='W')
                        s[ii][jj]='R';
                    else
                        assert(false);
                }
            }
        }
    }
    cout<<"YES"<<endl;
    for(int i=0; i<n; i++)
        cout<<s[i]<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
