/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We can swap a whole component, if it exists.
 * Let c be number of such components, then ans=2^c
 * since we can swap each one or not swap each one.
 * How to find min sized components?
 *
 * Just start with next unused pair and add all pairs
 * that are connected.
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    vi pa(n);   /* pa[i]=position of i+1 in a[] */
    vi pb(n);   /* pb[i]=position of i+1 in b[] */

    for(int i=0; i<n; i++) {
        pa[a[i]-1]=i;
        pb[b[i]-1]=i;
    }

    vb vis(n);
    int c=0;
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;
        c++;

        int idx=i;
        do {
            vis[idx]=true;
            idx=pa[b[idx]-1];
        }while(!vis[idx]);
    }

    mint ans(2);
    ans=ans.pow(c);
    cout<<ans.val()<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
