/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to trigger all blocks.
 * Just simulate.
 * It is allways optimal to trigger any block in the northmost row.
 * How to simulate efficiently?
 *
 * Lets binsearch the columns for the next row that is triggered.
 * But we need to trigger also to the North of each triggered block :/
 *
 * ...simply put all blocks on the prio_queue, and while pop
 * check if they stillexist.
 * Each block ist pushed at most once.
 *
 * ....
 * Note example 2, it does matter in which order we trigger the blocks
 * of the current topmost row.
 *
 * How to find the correct order?
 * We need to do some kind of topological ordering...
 *
 * We need to do this for all blocks, then ans=number of vertex
 * without incoming edge.
 * and after that number of components, since the graph can be circular.
 *
 *
 * What if there are two circles, but we can go only from one to the other,
 * like two vertical rows are completly filled.
 *
 * Each one build a cirlce, but we need to start on the correct circle :/
 * How to find the 'topological order of circles'?
 *
 * Can we somehow use the number of reachable vertex? How to find that number?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cinas(s,n);

    map<pii,vector<pii>> adj;
    vvi cols(m);

    for(int j=0; j<m; j++) {
        int prev=-1;
        for(int i=0; i<n; i++) {
            if(s[i][j]=='#') {
                cols[j].push_back(i);
                if(prev>=0) {
                    adj[ {prev,j}].emplace_back(i,j);
                    if(prev==i-1)
                        adj[ {i,j}].emplace_back(prev,j);
                }
                prev=i;
            }
        }
    }

    for(int j=0; j<m; j++) {
        for(int i=0; i<n; i++) {
            if(j>0 && s[i][j]=='#') {
                auto it=lower_bound(all(cols[j-1]), i);
                if(it!=cols[j-1].end())
                    adj[{i,j}].emplace_back(*it,j-1);
            }
            if(j+1<m && s[i][j]=='#') {
                auto it=lower_bound(all(cols[j+1]), i);
                if(it!=cols[j+1].end())
                    adj[{i,j}].emplace_back(*it,j+1);
            }
        }
    }

    function<void(int,int)> kick=[&](int i, int j) {
        queue<pii> q;
        q.emplace(i,j);
        while(q.size()) {
            auto [ii,jj]=q.front();
            q.pop();
            for(pii p : adj[{ii,jj}])
                q.push(p);
            adj.erase({ii,jj});
        }
    };

    set<pii> adjR;
    for(auto ent : adj) {
        for(auto p : ent.second)
            adjR.insert(p);
    }

    int ans=0;
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            if(s[i][j]=='#') {
                int cnt=adjR.count({i,j});
                if(cnt==0) {
                    ans++;
                    kick(i,j);
                }
            }
        }

    /* how to find the correct vertex here ??? 
     * simply using begin() might trigger the wrong one :/
     **/
    while(adj.size()) {
        ans++;
        auto it=adj.begin();
        kick(it->first.first, it->first.second);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
