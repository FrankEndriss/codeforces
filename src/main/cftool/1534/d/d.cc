/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

vi query(int v) {
    cout<<"? "<<v+1<<endl;
    cinai(a,n);
    assert(a[v]==0);
    return a;
}

/* We can directly store edges for all vertex with dist=1
 * Then ask for edges of dist=2, and connect them over
 * the common dist=1 edges.
 * ...
 * But what about a star tree?
 * We would query all but one vertex.
 * So we need to ...do "more", somehow.
 */
void solve() {
    cini(n);

    vvi adj(n);
    vector<set<int>> d1(n); /* d1[i]=adj of i */
    vector<set<int>> d2(n); /* d2[i]=vectors in dist2 of i */

    queue<int> q;
    q.push(0);

    while(q.size()) {
        int v=q.front();
        q.pop();
        if(adj.size()>0) 
            continue;

        vi d=query(v);
        for(int i=0; i<n; i++) {
            if(d[i]==1) {
                adj[v].push_back(i);
                adj[i].push_back(v);
                d1[v].insert(i);
            } else if(d[i]==2) {
                d2[v].insert(i);
                if(adj[i].size()==0)
                    q.push(i);
            }
        }
    }


}

signed main() {
    solve();
}
