/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we can simply color black/white, then choose the smaller
 * half. Since we colored alternate every vertex has at least
 * on with other color.
 */
void solve() {
    cini(n);
    cini(m);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vi color(n, -1);
    vi ccnt(2);

    function<void(int,int)> dfs=[&](int v, int col) {
        color[v]=col;
        ccnt[col]++;
        for(int chl : adj[v])
            if(color[chl]<0)
                dfs(chl, !col);
    };

    dfs(0,0);
    int cc=0;
    if(ccnt[0]>ccnt[1])
        cc=1;
    cout<<ccnt[cc]<<endl;
    for(int i=0; i<n; i++) 
        if(color[i]==cc)
            cout<<i+1<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
