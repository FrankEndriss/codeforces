/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * ans in 0,1,2
 *
 * Fraction calculation :/
 *
 * normalize both fraction.
 * if same then ans=0
 * else extend to same denominator.
 *
 * ...
 * Samples work, but absolutly unsure if there are edgecases...
 * whatever. Unfortunatly that is enough to quit the contest.
 * Sic problem A, dont do that, problemsetters.
 * Instead, if you do not find good A problem then simply start with B.
 */
void solve() {
    cini(a);
    cini(b);
    cini(c);
    cini(d);
    int g=gcd(a,b);
    a/=g;
    b/=g;
    g=gcd(c,d);
    c/=g;
    d/=g;

    if(a==c && b==d) {
        cout<<0<<endl;
    } else if(a==0 || c==0) {
        cout<<1<<endl;
    } else {
        int l=lcm(b,d);
        a=a*(l/b);
        c=c*(l/d);

        if(gcd(a,c)==min(a,c)) 
            cout<<1<<endl;
        else
            cout<<2<<endl;
    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
