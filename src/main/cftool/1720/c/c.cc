/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that once we removed one L, we can allways find a operation
 * removing only 1 cell.
 * So we want to minimize erased cells in first move.
 * So find two adj 0 cells, or or diagonal
 */
void solve() {
    cini(n);
    cini(m);
    cinas(s,n);

    bool one=false;
    bool two=false;
    int cnt=0;
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) {
            if(s[i][j]=='0') {
                one=true;
                if(i+1<n && s[i+1][j]=='0') 
                    two=true;
                if(j+1<m && s[i][j+1]=='0')
                    two=true;
                if(i+1<n && j+1<m && s[i+1][j+1]=='0')
                    two=true;
                if(i+1<n && j-1>=0 && s[i+1][j-1]=='0')
                    two=true;
            } else
                cnt++;
        }

    if(two)
        cout<<cnt<<endl;
    else if(one)
        cout<<cnt-1<<endl;
    else
        cout<<cnt-2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
