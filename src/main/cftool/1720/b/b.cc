/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * a segment devides the set of numbers into two partitions,
 * maximize max(p1)-min(p1)+max(p2)-min(p2);
 *
 * Note that it is -min(p).
 *
 * So put one max number into one partition, the the both max
 * are optimized.
 * Also put min or second min into same partition, so both min are minimized,
 * which is allways possible, since one of max, secondmax is allways next to min,secondmin.
 *
 * ...so A harder than B :/ double sic.
 */
void solve() {
    cini(n);
    cinai(a,n);
    vi b=a;
    sort(all(b));

    int ans=b[n-1]+b[n-2]-b[1]-b[0];
    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
