/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* We need to find 
 * all paths from s1 to t1 faster than l1, and
 * all paths from s2 to t2 faster that l2, and
 * find the pair of both paths which uses the
 * min number of edges.
 * Remove all other edges.
 * How to implement that?
 * Think of the H.
 */
void solve() {
    cini(n);
    cini(m);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--; v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    const int INF=1e9;
    vvi d(n, vi(n, INF));    // distance i,j
    for(int i=0; i<n; i++) {
        queue<int> q;
        q.push(i);
        while(q.size()) {
            int node=q.front();
            q.pop();
            for(int chl : adj[node]) {
                if(d[i][chl]>d[i][node]+1) {
                    d[i][chl]=d[chl][i]=d[i][node]+1;
                    q.push(chl);
                }
            }
        }
    }

    // dfs shortest paths s1 -> t1, s2 -> t2

        
    // if intersect, find H
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

