/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

/*
 * Find number of posible v[], where elements
 * of v denote the multiplier of a[]
 * Sum of v[] must be m;
 * sum of sum(v[i]*a[i])<=b
 *
 * dp[i][j]==number of ways the current programmer can write code resulting in i loc with j bugs.
 *
 * ****
 * By loc
 * The previous line of code resulted in a current programmer and a number of bugs.
 * dp[i][j]=num ways to the ith programmer delivers code with j bugs
 * The next line of code can be written by same programmer
 * dp2[i][j+a[i]]+=dp[i][j];
 * Or by next programmer
 * dp2[i+1][j+a[i+1]]+=dp[i][j];
 * Only if j+a[i]<=b
 */
void solve() {
    cini(n);
    cini(m);
    cini(b);
    cini(mod);
    cinai(a,n);

    vvi dp(m+1, vi(b+1));
    dp[0][0]=1;

    for(int p=0; p<n; p++) {    /* current programmer */
        vvi dp2=dp;
        for(int i=0; i<=m; i++) {   /* writes lines of code */
            for(int j=0; j<=b; j++) {   /* bugs presented from previous programmer */
                if(i>0 && j>=a[p]) {
                    dp2[i][j]+=dp2[i-1][j-a[p]];
                    dp2[i][j]%=mod;
                }
            }
        }
        dp.swap(dp2);
    }

    int ans=0;
    for(int i=0; i<=b; i++) {
        ans+=dp[m][i];
        ans%=mod;
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
