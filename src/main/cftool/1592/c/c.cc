/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * foreach edge we can produce the left and right XORsum.
 *
 * Do this for all edges, then one of the values will be the searched XORsum.
 * Foreach one find if there is a way to produce that sum with less components
 * as possible, check if less than k.
 *
 * Note that we must be able to cut a set starting at some leaf.
 *
 * Note that the resulting forrest has at most 3 components, since if it has 4,
 * then we can put 3 of them together.
 * So, if there is an edge with both ends equal then we can simply remove that edge, fini.
 *
 * Else let xx be the one ends sum.
 * There might be an edge where one end eq 0, and within that 0 subtree another edge
 * with xx/0 sum.
 * So, we need to find some x where there exist at least two edges x/0.
 *
 * How to find the xorsums of both ends of all edges without TLE?
 * idk :/
 */
using t3=tuple<int,int,int>;
void solve() {
    cini(n);
    cini(k);
    k--;
    cinai(a,n);
    int xorsum=0;
    for(int i=0; i<n; i++)
        xorsum^=a[i];

    map<pii,int> edg;
    vvi adj(n);
    vector<set<int>> adj2(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);

        adj2[u].insert(v);
        adj2[v].insert(u);
    }

    map<pii,int> sums;

    /* s== xorsum of parent tree */
    function<void(int,int,int)> dfs=[&](int v, int p, int s) {
        sums[{p,v}]=s^xorsum;

        for(auto it=adj2[v].begin(); it!=adj2[v].end(); it++) {
            if(*it==p)
                continue;

            int chl=*it;
            it=adj2[v].erase(it);
            dfs(chl, v, s^a[v]); /* this is wrong 
                                    We need to xor all other childs...somehow.
            */
            if(it==adj2[v].end())
                break;
        }
    };

    /* do dfs start at all leafs */
    for(int i=0; i<n; i++)  {
        if(adj[i].size()==1) {
            dfs(i,adj[i][0], a[i]);
            dfs(adj[i][0],i, a[i]^xorsum);
        }
    }

    /* check two equal edge ends */
    for(auto it=sums.begin(); it!=sums.end(); it++) {
        if(it->second==sums[{it->first.second,it->first.first}]) {
            cout<<"YES"<<endl;
            return;
        }
    }

    if(k==1) {
        cout<<"NO"<<endl;
        return;
    }


    /* x,set<edge> */
    map<int,set<pii>> edg0;
    for(auto it=sums.begin(); it!=sums.end(); it++) {
        if(it->second!=0 && sums[{it->first.second,it->first.first}]==0)
            edg0[it->second].insert(it->first);
        else if(it->second==0) {
            int lx=sums[{it->first.second,it->first.first}];
            edg0[lx].insert({it->first.second,it->first.first});
        }
    }

    for(auto it=edg0.begin(); it!=edg0.end(); it++) {
        if(it->second.size()>1) {
            cout<<"YES"<<endl;
            return;
        }
    }
    cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
