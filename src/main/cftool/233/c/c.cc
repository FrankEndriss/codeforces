/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* we add one node after the other, counting circles. 
 * Since min(k)==1 we start with 3 vertex graph which has one ciycle. */
void solve() {
    cini(k);
    vvi adj(3);
    adj[0].push_back(1);
    adj[0].push_back(2);
    adj[1].push_back(0);
    adj[1].push_back(2);
    adj[2].push_back(0);
    adj[2].push_back(1);

    k--;
    while(k) {
//cout<<"k="<<k<<" adj.size()="<<adj.size()<<endl;
        adj.resize(adj.size()+1);
        int v=adj.size()-1;  // new vertex
        adj[0].push_back(v);
        adj[1].push_back(v);
        adj[v].push_back(0);
        adj[v].push_back(1);
        k--;
        for(int i=2; k>=i && i<v; i++) {
            adj[i].push_back(v);
            adj[v].push_back(i);
            k-=i;
//cout<<"added v="<<v<<" to i="<<i<<" k="<<k<<endl;
        }
    }
    cout<<adj.size()<<endl;
    vvb ans(adj.size(), vb(adj.size()));
    for(int i=0; i<adj.size(); i++) {
        for(int j : adj[i]) {
            ans[i][j]=true;
            ans[j][i]=true;
        }
    }
    for(int i=0; i<adj.size(); i++) {
        for(int j=0; j<adj.size(); j++) {
            if(ans[i][j])
                cout<<"1";
            else
                cout<<"0";
        }
        cout<<endl;
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

