/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

void solve() {
    cini(n);
    cini(m);
    cini(k);

    ll ans=0;
/* sol has two parts.
 * if intersection of first and last square is bgeq k, then there are sols nCr(is, k) 
 * answers with k dots in this intersection.
 * 
 * Else we place dots in a pattern of n numbers (sum(numers)==k) in the n rows
 * of the square. The pattern repeats.
 * For any pattern there are sols(pattern) solutoins
 * So, ans==sum(sols(patterns))
 * Since m is huge we cannot dp this.
 * let p[i] be the num dots in ith col of the pattern.
 * There are nCr(n,p[i]) pos for that col.
 * So there are product(nCr(n,p[i])) pos for the first square.
 * let y[i]=num of times col i is drawn
 * ans=ans*nCr(n,p[i])^y[i]
 * */
    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

