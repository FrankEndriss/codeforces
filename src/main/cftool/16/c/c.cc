/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* after reduction it should hold that
 * a*y==b*x
 *
 * So we need to reduce at least to bigger of 
 * those values.
 *
 * If that does not help we need to reduce the smaller one, too.
 * How?
 * We calc max value for both.
 * Then we need to find the biggest value
 * dividable by the ratio values smaller than
 * the maximum.
 */
void solve() {
    cini(a);
    cini(b);
    cini(x);
    cini(y);

    int g=__gcd(x,y);
    x/=g;
    y/=g;

    if(a*y<=b*x) {  /* must reduce b */
        int maxA=a-(a%x);
        cout<<maxA<<" "<<(maxA/x)*y<<endl;
    } else {  /* must reduce a */
        int maxB=b-(b%y);
        cout<<(maxB/y)*x<<" "<<maxB<<endl;
    }
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
