/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Observe that in each not-ok triple two of the elements have a[i]==a[j]
 * and two have b[j]==b[k].
 * And i!=k because each pair (a[i],b[i]) is distinct.
 *
 * So, try each element as j, and calculate how many i,k can be used.
 * Google for formular how to calculate nCr without MOD.
 */
void solve() {
    cini(n);
    vi fa(n);
    vi fb(n);
    vi a(n);
    vi b(n);

    for(int i=0; i<n; i++)  {
        cini(aa); aa--;
        cini(bb); bb--;
        fa[aa]++;
        fb[bb]++;
        a[i]=aa;
        b[i]=bb;
    }

    int ans=n*(n-1)*(n-2) / 6;
    cerr<<"n="<<n<<" nCr(n,3)=="<<ans<<endl;

    for(int i=0; i<n; i++)
        ans-=(fa[a[i]]-1)*(fb[b[i]]-1);

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
