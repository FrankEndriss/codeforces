/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We search some combination of two cols where
 * the number of 1 in two cols is >= n/2
 */
void solve() {
    cini(n);
    vvi a(n, vi(5));
    for(int i=0; i<n; i++) 
        for(int j=0; j<5; j++)
            cin>>a[i][j];

    for(int d1=0; d1<5; d1++) {
        for(int d2=d1+1; d2<5; d2++) {
            vi cnt(2);
            int c=0;
            for(int i=0; i<n; i++)  {
                int ok=0;
                if(a[i][d1]) {
                    cnt[0]++;
                    ok=1;
                }
                if(a[i][d2]) {
                    cnt[1]++;
                    ok=1;
                }
                c+=ok;
            }
            if(c==n && cnt[0]>=n/2 && cnt[1]>=n/2) {
                cout<<"YES"<<endl;
                return;
            }
        }
    }
    cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
