/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that the mean must be 
 * an integer number after doubling it,
 * else ans=0
 */
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));


    map<int,int> cnt;
    int sum=0;
    for(int i=0; i<n; i++) {
        a[i]*=2;
        cnt[a[i]]++;
        sum+=a[i];
    }

    if(sum%n!=0) {
        cout<<0<<endl;
        return;
    }

    const int m=sum/n;

    int ans=0;
    for(auto it=cnt.begin(); it!=cnt.end(); it++) {
        int aa=it->first;
        if(aa>m)
            break;
        int other=m*2-aa;
        if(aa!=other)
            ans+=it->second*cnt[other];
        else
            ans+=(it->second*(it->second-1))/2;
    }

    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
