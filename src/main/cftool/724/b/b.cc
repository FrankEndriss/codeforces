/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we need to put all cols under each other.
 * then only two cols may be allowed to be
 * in wrong place.
 * So lets try all pairs of cols if we can
 * change the order of all rows so that
 * they can be build.
 */
void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            cin>>a[i][j];

    function<bool(vvi&)> check=[&](vvi &aa) {
        bool ok=true;
        for(int i=0; i<n; i++) {
            int cnt=0;
            for(int j=0; j<m; j++) {
                if(aa[i][j]!=j+1)
                    cnt++;
            }
            if(cnt>2)
                ok=false;
        }
        return ok;
    };


    /* first try without col swap */
    if(check(a)) {
        cout<<"YES"<<endl;
        return;
    }

    for(int s1=0; s1<m; s1++) {
        for(int s2=s1+1; s2<m; s2++) {
            vvi aa=a;
            for(int i=0; i<n; i++)
                swap(aa[i][s1], aa[i][s2]);

            if(check(aa)) {
                cout<<"YES"<<endl;
                return;
            }
        }
    }
    cout<<"NO"<<endl;

}


signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

