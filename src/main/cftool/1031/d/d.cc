/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* foreach cell find the number of non-a letters in it.
 * Consider the cells with k of them, and the dist from orig.
 * From all of them with the longest dist, find greedy the 
 * lex smallest path to nm.
 *
 * How to implement?
 * First do a dp for the prefixes, then bfs the smallest path.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);

    cinas(s,n);

    using t3=tuple<int,int,int>;
    /* BFS */
    string ans;
    vector<t3> q;   /* current positions reacheable by ans with c changes */
    q.emplace_back(0,0,0);
    vvi dp(n, vi(n, INF));  /* min changes needed to reach i,j */
    dp[0][0]=0;

    while(q.size()) {
        vector<t3> next;
        char mi='z'+1;
        for(auto [i,j,c] : q) {
            if(c<k)
                mi='a';
            else
                mi=min(mi, s[i][j]);
        }
        ans+=mi;

        for(auto [i,j,c] : q) {
            if(s[i][j]!=mi && c>=k)
                continue;

            if(dp[i][j]!=c)
                continue;

            int nc=c+(s[i][j]!=mi);
            if(i+1<n) {
                if(dp[i+1][j]>nc)
                    next.emplace_back(i+1, j, dp[i+1][j]=nc);
            }
            if(j+1<n) 
                if(dp[i][j+1]>nc)
                    next.emplace_back(i, j+1, dp[i][j+1]=nc);
        }

        q.swap(next);
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
