/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* For all primes in (l,r) we need
 * to find the number of x[i] divisable by that prime.
 *
 * ie for all primes we want to know how much x[i] exist
 * where the prime devides x[i]
 */
const int N=1e6+3;
const int NN=1e7+3;
void solve() {
    cini(n);

    vb ispr(NN, true);
    vi cnt(NN);
    vi inx(NN);

    for(int i=0; i<n; i++) {
        cini(aux);
        inx[aux]++;
    }

    for(int i=2; i<NN; i++) {
        if(ispr[i]) {
            for(int j=i; j<NN; j+=i) {
                ispr[j]=false;
                cnt[i]+=inx[j];
            }
        }
    }

    vi pre(NN);
    for(int i=1; i<NN; i++)
        pre[i]=pre[i-1]+cnt[i];
    

    cini(m);
    for(int i=0; i<m; i++) {
        cini(l);
        cini(r);
        l=min(l, NN-1);
        r=min(r, NN-1);
        int ans=pre[r]-pre[l-1];
        cout<<ans<<"\n";
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
