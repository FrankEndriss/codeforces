/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* parse input
 * parse words
 * process words in order
 * if seen before, find length of common prefix
 *
 * ***
 * Just simulate. But how to efficiently to that?
 * -> We need to check while parsing the length
 *  of some common prefix.
 *  Then, when the word ended we need to see if
 *  exactly that word was seen before, and if yes
 *  the size of comon prefix with another word.
 *
 *  Build a tree with
 *  -one letter per node
 *  -size of subtree per node
 *  -list of children per node
 *
 *  How to maintain which nodes are prefixes?
 *  When a word ends in current node we have cases:
 *  -word seen first time -> idx
 *  -word seen again but longer word exists -> idx
 *  -word seen again, so val==longest shorter word
 *   seen so far + 1
 *
 *   ...
 *  The judge seems to be broken, first example is not explainable.
 *  How does the autocomplete work?
 */
int treeidx=1;
const int N=3e5+7;

struct node {
    int cnt;    /* cnt words ending here */
    bool hasChld;    /* true if at least one child exists */
    vi adj;   /* children */

    node() {
        adj.resize(26);
    }
};

vector<node> tree(N);

/* @return number of clicks */
int add(int nidx, string& str, int idx, int last) {
//cerr<<"nidx="<<nidx<<" str="<<str<<" str.size()="<<str.size()<<" idx="<<idx<<" last="<<last<<endl;
    assert(nidx>=0 && nidx<N);
    assert(idx>=0 && idx<=str.size());

    if(idx==str.size()) {
        tree[nidx].cnt++;

        if(tree[nidx].cnt>1 && !tree[nidx].hasChld && last>-1) {
cerr<<"shortening ww="<<str<<" at idx="<<idx<<" last="<<last<<endl;
            return last+1;
        } else {
            return idx;
        }
    }

    assert(str[idx]-'a'>=0 && str[idx]-'a'<26);

    if(tree[nidx].adj[str[idx]-'a']==0)
        tree[nidx].adj[str[idx]-'a']=treeidx++;

    if(tree[nidx].cnt)
        last=idx;

    tree[nidx].hasChld=true;
    return add(tree[nidx].adj[str[idx]-'a'], str, idx+1, last);
}


void solve() {
    string s;
    int ans=0;
    for(int i=0; i<tree[0].adj.size(); i++) 
        tree[0].adj[i]=treeidx++;

    while(true) {
        s.clear();
        getline(cin,s);
        cerr<<"line="<<s<<endl;
        if(s.size()==0)
            break;

        ans++;      /* click per line */
        string cw;
        vs words;
        for(char c : s) {
            if(c>='a' && c<='z')
                cw.push_back(c);
            else {
                ans++;  /* click per other char */
                if(cw.size()>0) {
                    words.push_back(cw);
                    cw.clear();
                }
            }
        }
        if(cw.size())
            words.push_back(cw);


        for(string& ww : words) {
            ans+=add(tree[0].adj[ww[0]-'a'], ww, 0, -1);
            //cerr<<"tree.add, ww="<<ww<<" ans="<<ans<<endl;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
