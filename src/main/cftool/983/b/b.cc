/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Tutorial:
 * We create a dp[i][j]= max value if
 * i+1==len of sequence and j==begin of sequence
 *
 * So, first we calculate the f() values for those sequences, 
 * then we find the max values among all of them.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vvi dp(n+1, vi(n+1));
    for(int i=0; i<n; i++) 
        dp[0][i]=a[i];

/* calc f() values */
    for(int i=1; i<n; i++) {
        for(int j=0; j+1<=n; j++) {
            dp[i][j]=dp[i-1][j]^dp[i-1][j+1];
        }
    }

/* find max for all intervals */
    for(int i=1; i<n; i++) {
        for(int j=0; j+1<n; j++) {
            dp[i][j]=max({dp[i][j], dp[i-1][j], dp[i-1][j+1]});
        }
    }

    cini(q);
    for(int i=0; i<q; i++) {
        cini(l);
        cini(r);
        cout<<dp[r-l][l-1]<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
