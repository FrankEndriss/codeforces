/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to maintain the indexes of sideway cells
 * foreach column.
 * Then simulate.
 * That works, since at most k times the col will change.
 *
 * Note that in simulation, on each col change we need to find 
 * the next line in adj col lower_bound the current line.
 *
 * There is some other "funny" edgecase?
 * Again, thats one of the most annoying things setters can do.
 * Give a more or less trivial problem with a most stupid edgecase.
 * Dont do that :/
 *
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vector<set<pii>> d(m+2);
    for(int i=0; i<n; i++) {
        for(int j=1; j<=m; j++) {
            cini(c);
            if(c==1)
                d[j].emplace(i,1);
            else if(c==3)
                d[j].emplace(i,-1);
        }
    }

    for(int i=0; i<k; i++) {
        cini(c);
        int row=0;
        while(true) {
            auto it=d[c].lower_bound({ row, -100 });
            if(it!=d[c].end()) {
                const int cc=c+it->second;
                row=it->first;
                d[c].erase(it);
                c=cc;
                continue;
            }
            break;
        }
        cout<<c<<" ";
    }
    cout<<endl;
}

signed main() {
    solve();
}
