/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Foreach idx we can remove it or not.
 *
 * So going from left to right we have in each position
 * a list of <cnt_elements,max_contrib>
 *
 * Then we add the next element. That adds 1 to max_contrib
 * for the cnt_elements==a[i]-1
 * This is, any list of less than a[i] elements can be extended by the current element.
 * The one list with a[i]-1 elements gets extended by on element by this operation.
 *
 * ...and more edgecases :/
 *
 * So I am out. That kind of problems is no fun, and not much to learn.
 */
const int N=2e5+7;
void solve() {
    cini(n);

    /* dp[i]=max_contrib in a subseq of len i */
    vi dp(N, -1); 
    dp[0]=0;

    int ma=0;
    for(int i=0; i<n; i++) {
        cini(a);

        if(dp[a-1]>=0) {
            dp[a]=max(dp[a], dp[a-1]+1);
            ma=max(ma, dp[a]);
        }

        dp[i+1]=max(dp[i+1],dp[i]);
        ma=max(ma, dp[i+1]);

    }
    cout<<ma<<endl;
}

signed main() {
    solve();
}
