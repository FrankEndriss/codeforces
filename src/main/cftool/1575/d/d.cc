/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * First construct the patterns producing the last two digits as
 * 00, 25, 50, 75, and foreach one
 * the number of patterns for the other digits.
 *
 * Having a problem with some "funny" edgecase is most likely
 * the most demotivating thing cp.
 * So, dear problemsetters, if you do not want people to have fun,
 * then setup such problems. Haha, very funny :/
 */
void solve() {
    cins(s);
    if(s.size()<2) {
        if(s[0]=='0' || s[0]=='X' || s[0]=='_')
            cout<<1<<endl;
        else
            cout<<0<<endl;
        return;
    }

    reverse(all(s));



    bool hasx=false;
    for(char c : s)
        if(c=='X')
            hasx=true;

    const vvi dig={{'0','0'},{'5','2'},{'0','5'},{'5','7'}};
    int ans=0;
    for(int i=0; i<4; i++) {

        for(int x='0'; x<='9'; x++) {
            if(!hasx && x>'0')
                break;

            string t=s;
            if(t.back()=='X' && x=='0')
                continue;

            for(size_t j=0; j<t.size(); j++) 
                if(t[j]=='X')
                    t[j]=x;

            int lans=0;
            if((t[0]==dig[i][0] || t[0]=='_') && 
               (t[1]==dig[i][1] || t[1]=='_') )
            {
                if(dig[i][0]!='0' || dig[i][1]!='0' || s.size()>2) 
                    lans=1;

                for(int j=2; j<t.size(); j++) {
                    if(t[j]=='_') {
                        if(j+1==t.size()) 
                            lans*=9;
                        else
                            lans*=10;
                    }
                }
            }
            ans+=lans;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
