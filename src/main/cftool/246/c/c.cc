/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
    sort(all(a));

    map<int,vi> sums;
    vi empty;
    sums[0]=empty;

    int cnt=0;
    for(int i=0; cnt<k && i<n; i++) {
        map<int,vi> sums2=sums;

        for(auto ent : sums) {
            if(cnt==k)
                break;

            if(sums2.find(ent.first+a[i])==sums2.end()) {
                ent.second.push_back(a[i]);
                cout<<ent.second.size()<<" ";
                for(int val : ent.second)
                   cout<<val<<" ";
                sums2[ent.first+a[i]]=ent.second;
                cnt++;
            }
        }
        sums=sums2;
    }
    assert(cnt==k);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
