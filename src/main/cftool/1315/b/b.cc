/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9;

void solve() {
    cini(a);
    cini(b);
    cini(p);
    cins(s);


    map<char,ll> price;
    price['A']=a;
    price['B']=b;

    vll dp(s.size(), INF);
    dp[s.size()-1]=0;

    /* @return cost if at pos pos in state state */
    function<ll(int)> cost=[&](int pos) {
        if(pos>=s.size()-1)
            return 0LL;

        /* find how far we can go with current transport */
        int end=pos+1;
        while(end<s.size() && s[pos]==s[end])
            end++;

        dp[pos]=price[s[pos]]+cost(end);
        return dp[pos];
    };

    cost(0);

    for(int i=0; i<s.size() ;i++) {
        if(dp[i]<=p) {
            cout<<i+1<<endl;
            break;
        }
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

