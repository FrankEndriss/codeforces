/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

#define MOD 1000000007

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

/* we need to find number of 0,1 in (l,r),
 * let cnt[0]/cnt[1]
 * then it is optimal to first eat all k
 * which are initial 1. They contribute 1+2+4+8..+2^k-1 == (2^k)-1
 * after that all initial 0 which adds 
 * Let x=2^k-1, then
 * x + 2*x + 4*x + 8*x .. 2^(cnt[0]-1)*x == ((2^cnt[0])-1) * x
 */
void solve() {
    cini(n);
    cini(q);

    cins(s);
    vi a(n);
    vi p(n+1);
    for(int i=0; i<n; i++)  {
        a[i]=s[i]-'0';
        p[i+1]=p[i]+a[i];
    }

    for(int i=0; i<q; i++) {
        cini(l);
        cini(r);
        int cnt1=p[r]-p[l-1];
        int ans=pl(toPower(2, cnt1), -1);
        int cnt0=r-l+1-cnt1;
//cerr<<"l="<<l<<" r="<<r<<" cnt0="<<cnt0<<" cnt1="<<cnt1<<endl;
        if(cnt0>0) {
            ans=pl(ans, mul(pl(toPower(2,cnt0), -1), ans));
        }
        cout<<ans<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

