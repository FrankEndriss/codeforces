/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to consider all 0s before and after
 * the first and last 1
 * So, "simply" count.
 *
 * Note: k==0 is extra case
 */
void solve() {
    cini(k);
    cins(s);

    if(k==0) {
        int ans=0;
        int cnt=0;
        for(int i=0; i<s.size(); i++) {
            if(s[i]=='1') {
                ans+=cnt*(cnt+1)/2;
                cnt=0;
            } else
                cnt++;
        }
        ans+=cnt*(cnt+1)/2;
        cout<<ans<<endl;
        return;
    }

    vi kpos;
    for(size_t i=0; i<s.size(); i++) 
        if(s[i]=='1')
            kpos.push_back(i);

    int ans=0;
    int l=0; 
    int r=0;
    while(r<s.size()) {
        while(r-l+1<k && r<kpos.size())
            r++;

        if(r==kpos.size())
            break;

        int l0=0;
        if(l>0)
            l0=kpos[l]-kpos[l-1]-1;
        else
            l0=kpos[0];

        int r0=0;
        if(r+1<kpos.size())
            r0=kpos[r+1]-kpos[r]-1;
        else
            r0=s.size()-kpos.back()-1;

        ans+=(l0+1)*(r0+1);
        l++;
    }

    
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
