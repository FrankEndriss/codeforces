/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << "=" << a << " ";
	logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct Dsu {
    vector<int> p;
    vector<int> sz;
    int cnt;

/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        sz.resize(size, 1);
        iota(p.begin(), p.end(), 0);
        cnt=size;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            sz[a]+=sz[b];
            cnt--;
        }
    }
};

/* We need to find the number of components connected by the
 * 0-edges, and connect them using 1-edges.
 * So, ans==numComponents-1
 * How to find number of components in time? Number of 
 * non-mentioned edges is huge.
 * So, we need to find somehow if there is a "separation"
 * within the 1-edges.
 *
 * We find for all vertices the count of 1-edges to vertices
 * with smaller number.
 * if that count shows that not all nodes of that component
 * are connected to current vertex, then at least one is 
 * connect with a 0-vetex, so we add component of the current
 * vertex to that component.
 * In the end we will get all 0-Components.
 */
void solve() {
    cini(n);
    cini(m);
    vvi adj(n);

    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        if(u>v)
            swap(u,v);
        adj[v].push_back(u);
    }

    Dsu dsu(n);
    set<int> zeroComps; // sets of zero components so far

    for(int i=0; i<n; i++) {
        zeroComps.insert(i);
        map<int,int> e2sets;    // cnt of 1-edges from current vetex to zero-components

        for(int chl : adj[i]) 
            e2sets[dsu.find_set(chl)]++;

        vi rm;  // sets to remove
        for(int s : zeroComps) {
            int ss=dsu.find_set(s);
            if(ss!=s)
                rm.push_back(s);
            if(dsu.sz[ss]>e2sets[ss]) {
                int iset=dsu.find_set(i);
                dsu.union_sets(ss, iset);
                if(iset!=ss)
                    rm.push_back(iset);
            }
        }
        for(int s : rm)
            zeroComps.erase(s);
    }

    cout<<dsu.cnt-1<<endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
		solve();
}

