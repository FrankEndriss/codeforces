/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* may delete one of 10
 * 111000111
 * trailing 1s +1
 * or n if all same
 */
void solve() {
    cini(n);
    cins(s);

    vi cnt(2);
    for(int i=0; i<n; i++) 
        cnt[s[i]-'0']++;
    if(cnt[0]==0 || cnt[1]==0) {
        cout<<s<<endl;
        return;
    }

    int tr=0;
    for(int i=n-1; i>=0; i--)
        if(s[i]=='1')
            tr++;
        else
            break;

    int lea=0;
    for(int i=0; i<n; i++)
        if(s[i]=='0')
            lea++;
        else
            break;
    
    string mid="";
    if(lea+tr<n)
        mid="0";

    string ans=string(lea, '0')+mid+string(tr, '1');
    cout<<ans<<endl;
    
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
