/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Greedy
 * Obviously every one of the k friends needs to 
 * get on of the k biggest a[i]
 * Then, the friend with most ints get the
 * lowest a[i]... next friend etc.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
    cinai(w,k);

    sort(all(a));
    sort(all(w));

    int ans=0;
    for(int i=0; i<k; i++) {
        ans+=a[n-1-i];
//cerr<<"ans0="<<ans<<" i="<<i<<" w[i]="<<w[i]<<endl;
        w[i]--;
        if(w[i]==0) {
            ans+=a[n-1-i];
//cerr<<"ans1="<<ans<<endl;
        }
    }

//cerr<<"ans2="<<ans<<endl;

    int idx=0;
    for(int i=k-1; i>=0; i--) {
//cerr<<"idx="<<idx<<" w[i]="<<w[i]<<endl;
        if(w[i]>0) {
            ans+=a[idx];
            idx+=w[i];
        }
    }
    cout<<ans<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
