/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * Greedy?
 * We need to send a friend into kitchen with one food
 * only he likes, then the next with the other food etc.
 * Repeat.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(w, n);

    vector<set<pii>> adj(n);

    for(int i=0; i<m; i++) {
        cini(x); x--;
        cini(y); y--;
        adj[x].insert({y,i});
        adj[y].insert({x,i});
    }

    queue<int> q;

    for(int i=0; i<n; i++)
        if(adj[i].size()<=w[i])
            q.push(i);

    vi ans;
    while(q.size()) {
        int v=q.front();
        q.pop();
        if(adj[v].size()==0)
            continue;

        if(w[v]==0)
            continue;

        pii p={-1,-1};
        for(auto it=adj[v].begin(); it!=adj[v].end(); it++) {
            if(w[it->first]<=0)
                continue;
            p=*it;
            adj[v].erase(it);
            break;
        }
        if(p.first==-1)
            break;

        w[v]--;

        ans.push_back(p.second);
        adj[p.first].erase({v,p.second});
        if(adj[p.first].size()==1) 
            q.push(p.first);

//cerr<<"food="<<p.second<<" w[food]="<<w[p.second]<<" friends="<<adj[p.second].size()<<endl;
    }

    if(ans.size()==m) {
        cout<<"ALIVE"<<endl;
        for(int i : ans)
            cout<<i+1<<" ";
        cout<<endl;
    } else 
        cout<<"DEAD"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
