/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* make a directed graph and find longest path, BFS
 * ...TLE
 *
 * We need to use only some of the fitting.
 * After the first fitting use only ones with smaller wh.second
 * than the first fitting. etc
 * blah... no.
 *
 * Simple dp
 * dp[i]=longest chain up to envelope i
 **/
const int INF=1e9;
void solve() {
    cini(n);
    cini(sw)
    cini(sh);

    vector<pair<pii,int>> wh(n);
    for(int i=0; i<n; i++) {
        cin>>wh[i].first.first>>wh[i].first.second;
        wh[i].second=i;
    }
    sort(all(wh));

    int ans=0;
    int alast=-1;
    vi dp(n, -INF);
    for(int i=0; i<n; i++) {
        if(wh[i].first.first>sw && wh[i].first.second>sh) {
            dp[i]=1;
            ans=1;
            alast=i;
        }
    }

    vi p(n, -1);
    for(int i=0; i<n; i++) {
        for(int j=0; j<i; j++) {
            if(wh[i].first.first>wh[j].first.first && wh[i].first.second>wh[j].first.second) {
                if(dp[i]<dp[j]+1) {
                    dp[i]=dp[j]+1;
                    p[i]=j;
                    if(dp[i]>ans) {
                        ans=dp[i];
                        alast=i;
                    }
                }
            }
        }
    }

    cout<<ans<<endl;
    if(ans>0) {
        stack<int> st;
        while(alast>=0) {
            st.push(alast);
            alast=p[alast];
        }

        while(st.size()) {
            cout<<wh[st.top()].second+1<<" ";
            st.pop();
        }
        cout<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
