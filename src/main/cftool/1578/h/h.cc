/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Just parse recursive.
 * Which is kind of idotic to implement :/
 * Especially because it looks like scala, the most useless
 * programming language ever invented.
 */
void solve() {
    cins(s);

    /* @return <order,pos> */
    function<pii(int)> go=[&](int pos) {
        //cerr<<"go, pos="<<pos<<endl;
        assert(pos==s.size() || s[pos]=='(');
        if(pos==s.size()) {
            pii ret= {0LL,pos};
            return ret;
        }

        int o1=0;
        pos++;
        if(s[pos]=='(') {
            pii p=go(pos);
            o1=p.first;
            pos=p.second;
        }

        assert(s[pos]==')');
        pos++;

        if(pos==s.size() || s[pos]==')') {
            pii ret= {o1,pos};
            return ret;
        }

        assert(s[pos]=='-');
        pos++;
        assert(s[pos]=='>');
        pos++;

        pii p=go(pos);
        int o2=p.first;
        pos=p.second;

        pii ret= {max(o1+1,o2),pos};
        return ret;
    };

    //cerr<<"s="<<s<<endl;
    pii ans=go(0);
    cout<<ans.first<<endl;
}

signed main() {
    solve();
}
