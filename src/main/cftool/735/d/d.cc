/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* see https://en.wikipedia.org/wiki/Goldbach%27s_conjecture
 * and notice the fact that
 * odd number-2 might be prime, and
 * every number can be build as sum of not more than 3 primes.
 */
void solve() {
    cini(n);

    bool ispr=true;
    for(int j=2; ispr && j*j<=n; j++) {
        if(n%j==0)
            ispr=false;
    }
    if(ispr) {
        cout<<1<<endl;
        return;
    }

    if(n%2==0) {    // goldbach, every even number is sum of two primes
        cout<<2<<endl;
        return;
    }


    // if odd and n-2 is prime, it is sum of two primes, too
    ispr=true;
    for(int j=2; ispr && j*j<=n-2; j++) {
        if((n-2)%j==0)
            ispr=false;
    }
    if(ispr) {
        cout<<2<<endl;
        return;
    }



    cout<<3<<endl;
}

signed main() {
    solve();
}
