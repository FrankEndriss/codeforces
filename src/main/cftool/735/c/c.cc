/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* if last one had t matches, the other finalist had min t-1 matches.
 * The opnent before had t-2 matches, so had the oponent of the other finalist.
 *
 * If a player reaches t matches there had to be another player with
 * t-1 matches.
 */
void solve() {
    cini(n);
    
    int pp=1;
    int p=2;
    int ans=1;
    while(p+pp<=n) {
        ans++;
        int pp2=p;
        p=p+pp;
        pp=pp2;
    }
        
    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

