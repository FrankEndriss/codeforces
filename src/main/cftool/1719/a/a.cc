/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * So its two independent stacks...
 * nim somehow was xor :/
 *
 * ...see https://cp-algorithms.com/game_theory/sprague-grundy-nim.html#game-description
 * Why a problem like "you have to google grundy formular."?
 * ***
 * Got the problem wrong.
 * They can move the chip _in each move_ by odd number of cells, so 
 * it is another problem.
 * However, still do not see how it works :/
 * ...parity of distance in both directions change on each move,
 * So, parity of number of moves is fixed.
 */
void solve() {
    cini(n);
    cini(m);
    int p=(n+m)%2;

    if(p)
        cout<<"Burenka"<<endl;
    else
        cout<<"Tonya"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
