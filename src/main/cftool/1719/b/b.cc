/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * More number theory.
 *
 * So we got permutation 1..n, and want to create pairs, so 
 * ((a+k)*b )%4==0
 * (a*b + k*b)%4==0
 *
 * Use smallest possible number so that (k+b)%4==0, then same for (a+b)%4
 * If that does not work use 3/1, then 2/2.
 * Why does this allways work?
 * No.
 * Use number 1..n/2 for a. Then allways the smallest number from second 
 * half for b.  ...seems to notwork :/
 *
 * Ok, a quarter of numbers can be used as b and make every a+k working,
 * since it is a multiple of 4. This will be good for half of the pairs.
 * 4,8,12,...
 *
 * Then the other halve of the pairs, we can choose (b%4)==1, so 1,5,9,...
 * and the fitting a to them is 3,7,11...
 *
 * This works if n%4==0.
 *
 * What if n%4==2?
 * ***********
 * (a+k)%4==0 || b%4==0 || ((a+k)%4==2 && b%2==2)
 *
 * We can and want allways use 4,8,12,... for b, and
 * in this case some a that we cannot get rid of.
 *
 * if k==0 we cannot get rid of the a[i]%1 and a[i]%3 numbers.
 * if k==1 we can use 3,7,11,.. for a, pair with 1,5,9,...
 *    and 2,6,10 with 4,8,12.
 *    So works if n%4==0
 * if k==2 we can use 2,6,10,.. for a, pair with 1,5,9
 *    and 3,7,11 with 4,8,12.
 *    So works if n%2==0
 * if k==3 we can use 1,5,9... for a, pair with 2,6,10,...
 *    ans 3,7,11 with 4,8,12
 *    So works if n%2==0
 */
void solve() {
    cini(n);
    cini(k);
    k%=4;

    if(k==0) {
        cout<<"NO"<<endl;
    } else if(k==1) {
        if(n%4!=0)
            cout<<"NO"<<endl;
        else {
            cout<<"YES"<<endl;
            for(int i=3; i<n; i+=4) 
                cout<<i<<" "<<i-2<<endl;
            for(int i=2; i<n; i+=4) 
                cout<<i<<" "<<i+2<<endl;
        }
    } else if(k==2) {
            cout<<"YES"<<endl;
            for(int i=2; i<n; i+=4) 
                cout<<i<<" "<<i-1<<endl;
            for(int i=3; i<n; i+=4) 
                cout<<i<<" "<<i+1<<endl;
    } else if(k==3) {
            cout<<"YES"<<endl;
            for(int i=1; i<n; i+=4) 
                cout<<i<<" "<<i+1<<endl;
            for(int i=3; i<n; i+=4) 
                cout<<i<<" "<<i+1<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
