/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It does not take long, then the strongest fighter is
 * in first position, and wins all fights.
 * So simulate the first n fights and count.
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    vvi win(n);
    deque<int> d;
    for(int i=0; i<n; i++) 
        d.push_back(i);

    for(int i=0; i<n; i++) {
        int i1=d.front();
        d.pop_front();
        int i2=d.front();
        d.pop_front();
        if(a[i1]>a[i2]) {
            win[i1].push_back(i);
            d.push_front(i1);
            d.push_back(i2);
        } else {
            win[i2].push_back(i);
            d.push_front(i2);
            d.push_back(i1);
        }
    }

    for(int i=0; i<q; i++) {
        cini(id);
        id--;
        cini(k);
        int ans=distance(win[id].begin(), lower_bound(all(win[id]), k));
        if(id==d.front())
            ans+=max(0LL, k-n);

        cout<<ans<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
