/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
    
    sort(a.begin(), a.end(), [&](int i1, int i2) {
        return i2%10<i1%10; // sort by 10-i%10 desc
    });

    for(int i=0; i<n; i++) {
        int mod=a[i]%10;
        if(mod>0 && 10-mod<=k) {
            a[i]+=10-mod;
            k-=10-mod;
        }
    }

    for(int i=0; k>=10 && i<n; i++) {
        if(a[i]<100) {
            int ma=min(k, 100-a[i]);
            a[i]+=ma;
            k-=ma;
        }
    }
    int ans=0;
    for(int i=0; i<n; i++)
        ans+=a[i]/10;

    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

