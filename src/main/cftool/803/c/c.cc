/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * We need to find a set of distinct numbers
 * which sum up to a divisor of n.
 * The smaller the divisor the better.
 *
 * ie we need to find the smallest devisor bgeq
 * k*(k+1)/2
 * Then create any set of distinct numbers summing 
 * up to d, and multiplying every number with
 * n/d.
 */
void solve() {
    cini(n);
    cini(k);

    int sum=k*(k+1)/2;
    if(k>2e5 || sum>n) {
        cout<<-1<<endl;
        return;
    }

    set<int> d;
    for(int i=1; i*i<=n; i++) 
        if(n%i==0) {
            d.insert(i);
            d.insert(n/i);
        }

    const int ndd=n/(*d.lower_bound(sum));

    int ssum=0;
    vi ans;
    for(int i=1; i<k; i++) {
        cout<<i*ndd<<" ";
        ssum+=i*ndd;
    }
    cout<<n-ssum<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
