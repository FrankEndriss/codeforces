/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* What is the smallest letter we can produce in s[0]?
 *
 * 1. We can rotate s[0] up or down, or 
 * Then go on like begin.
 * 2. swap s[1] to its position and rotate that, or
 * Then rotate or do nothing. And go on like begin at s[2]
 * 3. swap s[2] to its position.
 * Then go on like begin at s[3]
 *
 * Nah, there is some twist with first position or not first position...
 *
 * So what if 1,2 and/or 3 give same results in position 0?
 * Then prefer 1 over 2 and 3, and 2 over 3.
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);

    sting ans=s;

    function<char(char)> xrot=[&](char c) {
        char up=((c+1-'a')%k)+'a';
        char down=((c-1-'a'+k)%k)+'a';
        return min(c,min(up, down));
    };

    int pos=0;
    while(pos<n) {
        char c1=xrot(s[pos]);
        char c2=pos+1<n?xrot(s[pos+1]):'z';
        char c3=pos+2<n?s[pos+2]:'z';

        if(c1<=c2 && c1<=c3)  {
            ans[pos]=c1;
            pos++;
        } else if(c2<c3) {
            ans[pos]=c2;
            ans[pos+1]=s[pos];
            pos+=2;
        } else {
            ans[pos]=c3;
// ...
            pos+=3;
        }


    }


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
