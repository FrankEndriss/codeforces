/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Alice x, Bob y 
 * Note: Winner starts next play.
 *
 * We got one who serves, and one who returns.
 * That roles may change.
 * Assume A serves.
 * If A makes the point nothing changes else B serves.
 * So if a>b then B needs to find a way to make points.
 * So he waits until A has same stamina, while A wins a-b points.
 *
 * If a==b B can win a point by allways return. So he will.
 * Since A knows that, A wont return the second ball.
 * So first point goes to B, then roles change, they still have same points,
 * but B serves. So second point goes to A, third to B...
 *
 * Else A serves and B has more.
 * B can win a point (and make A not win one) by returning, so he does.
 * A gives away that point. Then the roles change as in case a>=b.
 *
 * But that for some reason WA :/
 *
 *
 * So b let a make all points exept the last, then makes b points.
 */
void solve() {
    cini(a);
    cini(b);



    cout<<a-1<<" "<<b<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
