/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to make the x two pairs of same,
 * and the y two pairs of same.
 * No, it must be a square!
 *
 * The center of the square would have to be placed
 * int the center of x and center of y.
 *
 * Then somehow binsearch/quadsearch the size of the square.
 */
void solve() {
    vi x(4);
    vi y(4);
    for(int i=0; i<4; i++)
        cin>>x[i]>>y[i];

    int cx=accumulate(all(x), 0LL)/4;
    int cy=accumulate(all(y), 0LL)/4;

    /* ans if sidelen of square is sz */
    function<int(int)> dist=[&](int sz) {
        // ...
    };

    int l=0;
    int r=1e9;

    while(l+1<r) {
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
