/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


int pp_pal(string& s) {
    int ans=0;
    for(int i=0; i<s.size()/2; i++)
        if(s[i]==s[s.size()-1-i])
            ans++;
        else
            break;

    return ans;
}

/* see https://stackoverflow.com/questions/3833103/longest-palindrome-prefix */
const int mod1=1e9+7;
const int mod2=1e9+9;
const int base1=1e6+7;
const int base2=1e6+9;

int lp(string& a) {
    int n=a.size();
    int match = n - 1;
    int ha1 = 0, ha2 = 0, hr1 = 0, hr2 = 0;
    int m1 = 1, m2 = 1;
    for ( int i = 0; i<n; ++i ) {
        ha1 = (ha1 + m1*a[i]) % mod1;
        ha2 = (ha2 + m2*a[i]) % mod2;

        hr1 = (a[i] + base1*hr1) % mod1;
        hr2 = (a[i] + base2*hr2) % mod2;

        m1 *= base1, m1 %= mod1;
        m2 *= base2, m2 %= mod2;

        if ( ha1 == hr1 && ha2 == hr2 )
            match = i;
    }
    return match+1;
}

/* Some possibilities:
 * -palin at start of s
 * -palin at end of s
 * -common start and end
 */
void solve() {
    cins(s);

    int blen=pp_pal(s);
    string ss=s.substr(blen, s.size()-blen*2);
    int bprelen=lp(ss);
    reverse(all(ss));
    int bpostlen=lp(ss);
    reverse(all(ss));

    string ans=s.substr(0, blen);
    if(bprelen>=bpostlen) {
        ans+=ss.substr(0,bprelen);
    } else {
        ans+=ss.substr(ss.size()-bpostlen,bpostlen);
    }
    ans+=s.substr(s.size()-blen, blen);

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
