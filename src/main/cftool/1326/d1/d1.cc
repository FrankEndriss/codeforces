/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* create a palindrome from s
 */
void solve() {
    cins(s);
    string t=s;
    reverse(t.begin(), t.end());

    int i1=0; 
    while(i1<s.size() && s[i1]==t[i1])
        i1++;

    if(i1==s.size()) {
        cout<<s<<endl;
        return;
    }

/* find longest palindrome starting at i2 
 * in s _or_ t.
 * Note that it as least on char long. */
    int i2=i1;
    int ansS=1;
    int ansT=1;
    int len=1;
    while(i1*2+len<s.size()) {
        bool okt=true;
        bool oks=true;
        for(int i=0; i<len; i++) {
            if(s[i1+i]!=s[i1+len-1-i])
                oks=false;
            if(t[i1+i]!=t[i1+len-1-i])
                okt=false;
        }
        if(oks)
            ansS=max(ansS, len);
        if(okt)
            ansT=max(ansT, len);

        len++;
    }

    if(ansS>=ansT)  {
        string ans=s.substr(0,i1+ansS)+s.substr(s.size()-i1);
        cout<<ans<<endl;
    } else {
        string ans=s.substr(0,i1)+s.substr(s.size()-i1-ansT);
        cout<<ans<<endl;
    }
    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

