/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* there must be allways an even number of consecutive
 * platforms set.
 * But index frickling-level is extremly high... :/
 */
#define fore(i, l, r) for(int i = int(l); i < int(r); i++)
#define sz(a) int((a).size())

#define x first
#define y second

const int INF = int(1e9);

int h, n;
vector<int> p;

inline bool read() {
    if(!(cin >> h >> n))
        return false;
    p.resize(n);
    fore(i, 0, n)
    cin >> p[i];
    return true;
}

inline void solve() {
    int ans = 0;

    int lf = 0;
    fore(i, 0, n) {
        if (i > 0 && p[i - 1] > p[i] + 1) {
            if (lf > 0)
                ans += (i - lf) & 1;
            else
                ans += 1 - ((i - lf) & 1);
            lf = i;
        }
    }
    if (p[n - 1] > 1) {
        if (lf != 0)
            ans += (n - lf) & 1;
        else
            ans += 1 - ((n - lf) & 1);
    }

    cout << ans << endl;
}

int main() {
#ifdef _DEBUG
    freopen("input.txt", "r", stdin);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(0), cout.tie(0);
    cout << fixed << setprecision(15);

    int tc;
    cin >> tc;
    while(tc--) {
        read();
        solve();
    }
    return 0;
}
