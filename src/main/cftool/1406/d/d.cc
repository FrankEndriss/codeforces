/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* a[i] = b[i] + c[i]
 * minimize max(b[i], c[i]), ie make b[i] and c[i] equal
 *
 * b[] increasing
 * c[] decreasing
 * b[0] must be smaller
 * c[0] must be bigger
 *
 * First for initial seq:
 * Let a[j] be the max(a[])
 * Then we might make b[j]=a[j]/2 and c[j]=a[j]/2
 * So we can go to left for same c which makes small b,
 * and to the right for same b, which makes small c.
 * Note c[0] is the biggest number is the set, so we need to minimize c[0].
 * If all numbers in c[] == c[0], then b[i]=a[i]-c[0]
 * So at the position a[max] there is b[max]
 * So c[0] must not be smaller that a[max]/2;
 *
 * On the other hand, let d=a[max]-a[i] with i<max
 * Then b[i]=a[i]-c[0];
 * and  b[i]+d=b[max]
 * But b[i] must not be bigger than b[max]
 *
 * So, if we increase c[0] by q, then
 * b[max]=a[max]-c[0]-q
 * And we can increase b[max] by q...
 *
 * ...
 * I dont get it for one sequence...how could I find solution including problem with changes?
 *
 */
void solve() {
    cini(n);
    cinai(a,n);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
