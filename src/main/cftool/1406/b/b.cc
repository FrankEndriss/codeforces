/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to choose an odd number of negatives, if possible
 * Else better with 0 than negative.
 * Then choose the abs biggest ones.
 */
void solve() {
    cini(n);
    vi apos;
    vi aneg;
    vi zero;
    vi a;

    for(int i=0; i<n; i++) {
        cini(aux);
        a.push_back(aux);
        if(aux<0)
            aneg.push_back(i);
        else if(aux>0)
            apos.push_back(i);
        else
            zero.push_back(i);
    }

    sort(all(aneg), [&](int i1, int i2) {
        return a[i1]<a[i2];
    });

    sort(all(apos), [&](int i1, int i2) {     // greater
        return a[i2]<a[i1];
    });

    const int INF=1e18;
    int ans=-INF;
    for(int i=0; i<=5; i++) {   // count of negative numbers
        int neg=min(i, (int)aneg.size());
        int pos=min(5-neg, (int)apos.size());

        int lans=1;

        if(neg%2==1) {  // choose smallest numbers
            for(int j=0; j<neg; j++)
                lans*=a[aneg[aneg.size()-j-1]];
            for(int j=0; j<pos; j++)
                lans*=a[apos[apos.size()-j-1]];

            if(zero.size())
                lans=0;
        } else {
            for(int j=0; j<neg; j++)
                lans*=a[aneg[j]];
            for(int j=0; j<pos; j++)
                lans*=a[apos[j]];
        }

        if(neg+pos<5) {
            if(zero.size())
                lans=0;
            else
                lans=-INF;
        }
        ans=max(ans, lans);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
