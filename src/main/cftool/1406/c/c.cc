/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* if number of vertex is odd then there is allways
 * one centroid.
 * Else n is even.
 * We need to find the centroids by dp the tree, foreach node
 * the centSize==max(size(chld));
 * if from all nodes there are two equal the max value, then 
 * these are adj to each other.
 * Then cut of the childs of one of them, and put the childs as a next
 * child to the other one.
 * What about
 * example: 1 - 2 - 3 - 4
 * -> 1 - 2 - 3
 *          \ 4
 */
const int INF=1e9;
void solve() {
    cini(n);

    vvi adj(n);
    vvi dp(n);
    for(int i=0; i<n-1; i++) {
        cini(x); x--;
        cini(y); y--;
        adj[x].push_back(y);
        dp[x].push_back(-1);
        adj[y].push_back(x);
        dp[y].push_back(-1);
    }

    if(n%2==1) {
        cout<<"1 "<<adj[0][0]+1<<endl;
        cout<<"1 "<<adj[0][0]+1<<endl;
        return ;
    }


    /* @return size of the subtree at v */
    function<int(int,int)> dfs=[&](int v, int p) {
        int ans=1;
        for(int i=0; i<adj[v].size(); i++) {
            if(adj[v][i]!=p) {
                if(dp[v][i]<0)
                    dp[v][i]=dfs(adj[v][i], v);
                ans+=dp[v][i];
            }
        }

        return ans;
    };

    for(int i=0; i<n; i++)
        dfs(i, -1);

    vi vmacent;
    int macent=INF;
    for(int i=0; i<n; i++) {
        int ma=*max_element(all(dp[i]));

        if(ma<macent) {
            macent=ma;
            vmacent.clear();
        } 
        
        if(ma==macent)
            vmacent.push_back(i);
    }

    assert(vmacent.size()<3);
    if(vmacent.size()==1) {
        int v=vmacent[0];
        cout<<v+1<<" "<<adj[v][0]+1<<endl;
        cout<<v+1<<" "<<adj[v][0]+1<<endl;
    } else {
        int v1=vmacent[0];
        int v2=vmacent[1];
        int v3=adj[v2][0];
        if(v3==v1)
            v3=adj[v2][1];

        cout<<v2+1<<" "<<v3+1<<endl;
        cout<<v1+1<<" "<<v3+1<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
