/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The biggest numbers is element of a[]
 * Remove all entries made up by this and previous known numbers.
 * Repeat.
 */
void solve() {
    cini(n);
    map<int,int> f;
    for(int i=0; i<n*n; i++)  {
        cini(aux);
        f[-aux]++;
    }

    vi ans;
    while(f.size()) {
        auto it=f.begin();
        if(it->second==0) {
            f.erase(it);
            continue;
        }

        int a=-(it->first);

        for(size_t i=0; i<ans.size(); i++) {
            int g=-gcd(a, ans[i]);
            f[g]-=2;
            assert(f[g]>=0);
        }

        ans.push_back(a);
        f[-a]--;
    }

    assert(ans.size()==n);
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
