/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need even number of sticks with same length.
 * So, whenever we find an odd freq, we want
 * to make it even.
 */
const int N=1000001;
void solve() {
    cini(n);
    vi f(N);

    for(int i=0; i<n; i++) {
        cini(aux);
        f[aux]++;
    }

    int carry=0;
    for(int i=N-1; i>0; i--) {
        if(f[i]%2==1 && f[i]-carry>0) {
            f[i]--;
            f[i-1]++;
            carry=1;
        } else
            carry=0;
    }

    int ans=0;
    stack<int> st;
    for(int i=N-1; i>0; i--) {
        if(st.size() && f[i]>=2) {
            ans+=st.top()*i;
            st.pop();
            f[i]-=2;
        }

        ans+=i*i*(f[i]/4);
        f[i]%=4;

        if(f[i]>=2)
            st.push(i);

        assert(st.size()<2);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
