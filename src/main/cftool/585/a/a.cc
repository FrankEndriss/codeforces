/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* what a fucking horror story ;)
 *
 * we need to do simulation of the process.
 * Two kinds of screams.
 *
 * The ones without distance are put in one sum.
 *
 * The ones with distance. We count the number.
 * We put the stepnumber until it is heard into
 * a set.
 * On each steap we subtract count.
 * On stepnumber found in set we decrease number.
 *
 * On every child, we add the screams heard
 * so far. That results in current child 
 * beeing cured or not, and scream of 
 * current child.
 */
void solve() {
    cini(n);

    vi v(n);
    vi d(n);
    vi p(n);
    vb done(n);

    for(int i=0; i<n; i++) 
        cin>>v[i]>>d[i]>>p[i];

    queue<int> q;

    function<void(int)> scream1=[&](int idx) {
        for(int i=idx+1; v[idx]>0 && i<n; i++) {
            if(!done[i]) {
                p[i]-=v[idx];
                v[idx]--;
                if(p[i]<0) {
//cerr<<"sc1 killed i="<<i<<" by idx="<<idx<<endl;
                    done[i]=true;
                    q.push(i);
                }
            }
        }
        while(q.size()) {
            idx=q.front();
            q.pop();
            for(int i=idx+1; i<n; i++) {
                if(!done[i]) {
                    p[i]-=d[idx];
                    if(p[i]<0)  {
                        done[i]=true;
                        q.push(i);
                    }
                }
            }
        }
    };

    vi ans;
    for(int i=0; i<n; i++) {
        if(!done[i]) {
            ans.push_back(i+1);
            scream1(i);
        }
    }

    cout<<ans.size()<<endl;
    for(int i : ans) 
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
