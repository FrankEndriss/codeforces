/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void guess(int u, int v) {
    cout<<"! "<<u<<" "<<v<<endl;
    cins(s);
cerr<<s<<endl;
}

pii q(int c, vi& data) {
    cout<<"? "<<c<<" ";
    for(int i=0; i<c; i++)
        cout<<data[i]<<" ";
    cout<<endl;
    cini(x);
    cini(d);
    return { x, d };
}

/* How?
 * dfs the levels of the nodes
 * choose a root which makes the biggest level as big as possible (longest path).
 * Query.
 * if d is small we can remove nodes, if d is big we can remove nodes, too.
 * repeat...somehow.
 */
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    set<int> ans;
    for(int i=0; i<n; i++)
        ans.insert(i);

    vi lvl(n);
    function<void(int,int)> dfs=[&](int v, int p) {
        if(p>=0) 
            lvl[v]=lvl[p]+1;

        for(int c : adj[v])
            if(c!=p)
                dfs(c, v);
    };

    while(ans.size()>2) {
        dfs(*ans.begin(), -1);
        vi data(1);
        data[0]=*ans.begin();
        auto [x,dist]=q(1, data);

        for(auto it=ans.begin(); it!ans.end(); it++) {
// todo...
        }
    };
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
