/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Ash first
 * Winning pos are all odd numbers.
 * So try to divide a number to an even number.
 * Choose one where no further such divide is possible.
 *
 * Is it always possible to divide to wining pos?
 * Assume 4 -> no odd div, so has to subtr -> loos
 * Assume 12 -> 3 is div, so  win
 * Assume 24 -> 3 -> 8 win
 * Assume 36 -> 9 -> 4 win
 *
 * So, if n has odd divisor then it is win, since we can
 * use product of all odd prime-divisors, which only leaves
 * even divisors.
 */
void solve() {
    cini(n);
    if(n==1) {
        cout<<"FastestFinger"<<endl;
        return;
    } else if(n==6) {
        cout<<"FastestFinger"<<endl;
        return;
    } else if(n==2) {
        cout<<"Ashishgup"<<endl;
        return;
    }

    if(n&1) {
        cout<<"Ashishgup"<<endl;
        return;
    } else {
        for(int i=2; i*i<=n; i++) {
            if(n%i==0) {

                if(i%2==1 && n/i!=2 && (n/i)%2==0) {
                    cout<<"Ashishgup"<<endl;
                    return;
                }

                int ii=n/i;
                if(ii%2==1 && n/ii!=2 && (n/ii)%2==0) {
                    cout<<"Ashishgup"<<endl;
                    return;
                }
            }
        }
    }
    cout<<"FastestFinger"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
