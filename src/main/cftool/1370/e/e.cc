/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* So, obviously we want to make as much
 * position as possible correct with one move.
 * Repeat.
 *
 * So, how to do that?
 * Find first incorrect position.
 * Find next incorrect positon needing prev symbol.
 * Find next...
 * But the chain must be of even len.
 * Repeat
 *
 * Impl:
 * We need two sets with positions of incorrect symbols.
 */
void solve() {
    cini(n);
    cins(s);
    cins(t);

    vector<set<int>> inco(2);  /* positions of incorrect 0s and 1s in s */

    vi cnts(2);
    vi cntt(2);
    for(int i=0; i<n; i++) {
        cnts[s[i]-'0']++;
        cntt[t[i]-'0']++;

        if(s[i]!=t[i])
            inco[s[i]-'0'].insert(i);
    }

    if(cnts[0]!=cntt[0]) {
        cout<<-1<<endl;
        return;
    }
    assert(inco[0].size()==inco[1].size());

    function<void(int)> swp=[&](int idx) {
        if(s[idx]=='1')
            s[idx]='0';
        else
            s[idx]='1';
    };

    int ans=0;
    for(int i=0; i<n; i++) {
        if(s[i]!=t[i]) {
            ans++;
            int sym=s[i]-'0';
            int pos=i;
            swp(pos);
            inco[sym].erase(pos);
            pos=*inco[!sym].upper_bound(pos);   /* always exists */
            swp(pos);
            inco[!sym].erase(pos);

            /* now try to find more pairs of sym/!sym to swap in this one move */
            while(true) {
                auto it=inco[sym].upper_bound(pos);
                if(it!=inco[sym].end()) {
                    auto it2=inco[!sym].upper_bound(*it);
                    if(it2!=inco[!sym].end()) {
                        swp(*it);
                        swp(*it2);
                        inco[sym].erase(it);
                        pos=*it2;
                        inco[!sym].erase(it2);
                    } else
                        break;
                } else
                    break;
            }
        }
    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
