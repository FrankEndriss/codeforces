/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vi b=a;
    sort(all(b));

/* @return true if we can put maval (or smaller) as max element in odd/even positions.
 * first position is given one. */
    function<bool(int,int)> check=[&](int maval, int odd) {
        int idx=1;
        for(int i=0;  i<n; i++) {
            if(idx%2==odd && a[i]<=maval) {
                idx++;
            } else if(idx%2!=odd) {
                idx++;
            }
            if(idx==k+1)
                return true;
        }
        return false;
    };

    int l=-1;
    int r=n-1;
    while(l+1<r) {
        int mid=(l+r)/2;
        if(check(b[mid], 0) || check(b[mid], 1))
            r=mid;
        else
            l=mid;
    }
    cout<<b[r]<<endl;
}

/* we need to minimize max(s[oddpos])
 * or max(s[evenpos])
 *
 * we can throw away n-k elements.
 *
 * Lets make two runs, one even, one odd.
 * How to minimize for even indexes?
 * Note that even could be one more than odd for case odd k.
 *
 * dp[i][j]== min max element if postfix starting at i and j remaining elements must be choosen 
 * ... that does not fit in space.
 *
 * binsearch?
 * let mid be the max element on odd (or even) indexes.
 * try to fit all other elements into lists.
 * report ok if possible.
 */
signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
