/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cinai(a,n);

    int s1=0;
    vi t1;
    int s2=0;
    vi t2;

    for(int i=0; i+1<n; i+=2) {
        int idxMa=i;
        int idxMi=i+1;
        if(a[idxMa]<a[idxMi]) {
            swap(idxMa, idxMi);
        }

        if(s1>s2) {
            s1+=a[idxMi];
            t1.push_back(idxMi);
            s2+=a[idxMa];
            t2.push_back(idxMa);
        } else {
            s1+=a[idxMa];
            t1.push_back(idxMa);
            s2+=a[idxMi];
            t2.push_back(idxMi);
        }
    }
    if(n&1) {
        if(s1<s2)
            t1.push_back(n-1);
        else
            t2.push_back(n-1);
    }

    cout<<t1.size()<<endl;
    for(int i : t1)
        cout<<i+1<<" ";
    cout<<endl;
    cout<<t2.size()<<endl;
    for(int i : t2)
        cout<<i+1<<" ";
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

