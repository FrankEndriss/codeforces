/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The less components we have, the simpler is the construction.
 * So, what about 4x100
 *
 * Pattern
 * Fill a row with the most freq which must be possible for given input.
 * Repeat
 * Then do the same again
 * ABCBCD
 * BBCCCC
 * CBCBCD
 * BBCCCC
 * ABCBCD
 * BBCCCD
 * ABCBC
 * .
 * .
 * .
 * *******************
 * Simpler:
 * define 4 areas, use 1 background color for each.
 * And put all others elements in single cells in that area.
 */
void solve() {
    cini(a);    
    cini(b);
    cini(c);
    cini(d);
    a--;
    b--;
    c--;
    d--;

    string ch="ABCD";
    vector<string> ans(48, string(50, ' '));
    for(int i=0; i<48; i++)
        for(int j=0; j<50; j++)
            ans[i][j]=ch[i/12];

    for(int i=0; i<12; i+=2)
        for(int j=0; i%2+j<50; j+=2) 
            if(b-->0)
                ans[i][i%2+j]='B';

    for(int i=12; i<24; i+=2)
        for(int j=0; i%2+j<50; j+=2) 
            if(c-->0)
                ans[i][i%2+j]='C';

    for(int i=24; i<36; i+=2)
        for(int j=0; i%2+j<50; j+=2) 
            if(d-->0)
                ans[i][i%2+j]='D';

    for(int i=36; i<48; i+=2)
        for(int j=0; i%2+j<50; j+=2) 
            if(a-->0)
                ans[i][i%2+j]='A';


    cout<<"48 50"<<endl;
    for(string str : ans)
        cout<<str<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
