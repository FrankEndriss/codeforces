/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to put all 'a' to ans,
 * then all available 'b'.
 * Available means all occ after last 'a',
 * or at position right before block of 
 * last 'a'.
 */
void solve() {
    cins(s);
    vi last('z'-'a'+1, -1);
    for(size_t i=0; i<s.size(); i++) 
        last[s[i]-'a']=i;
 
    string ans;
    string t;

    int idx=0;
    for(char c='a'; c<='z'; c++) {
        while(t.size() && t.back()<=c) {
            ans+=t.back();
            t.pop_back();
        }

        for(; idx<=last[c-'a']; idx++)  {
            if(s[idx]==c)
                ans+=c;
            else
                t+=s[idx];
        }
    }

    reverse(all(t));
    cout<<ans<<t<<endl;
}
 
signed main() {
    solve();
}
 
// FIRST THINK, THEN CODE
