/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * 2*y+1 is
 * leftshift by 1 and set last bit.
 * Which basically means each number, extended by 1 on the right end.
 * 4*y means leftshift by 2,
 * Which basically means each number, extended by 00 on the right end.
 *
 * So we need to count the patterns on the right end, and propably find 
 * doubles with existing numbers in s.
 * But we can also simply create all included numbers up to 2^30
 *
 * Then we need to find foreach of that numbers the length in bits, and from
 * that the number of postfixes we can append.
 *
 * The number of postfixes up to len p:
 * All sequences of numbers 1,2 with sum<=p
 * Simple dp
 *
 * ...getting confused by bitshift operations :/
 * **********************
 * Collecting all buildable numbers goes MLE :/
 * So, we better collect foreach a[i] the
 * numbers that can act as a source to construct them.
 * That are much less, so its much less resource consuming.
 */
using mint=modint1000000007;
int N=1e9+1;
void solve() {
    cini(n);
    cini(p);

    cinai(a,n);
    sort(all(a));

    if(p<31) {
        N=min(N, 1LL<<p);  
    }
    //cerr<<"N="<<N<<endl;

    set<int> s;
    vi s2;

    for(int i=0; i<n; i++)  {
        if(s.count(a[i])>0)
            continue;

        s2.push_back(a[i]);

        int aux=a[i];
        queue<int> q;
        q.push(aux);
        while(q.size()) {
            int v=q.front();
            q.pop();
            s.insert(v);
            int v1=v*2+1;
            if(v1<N && s.count(v1)==0) {
                s.insert(v1);
                q.push(v1);
            }
            int v2=(v*4);
            if(v2<N && s.count(v2)==0) {
                s.insert(v2);
                q.push(v2);
            }
        }

    }

    vector<mint> dp(p+1);
    vector<mint> dp1(p+2);
    dp[0]=1;
    for(int i=0; i<p; i++) {
        if(i+1<dp.size())
            dp[i+1]+=dp[i];
        if(i+2<dp.size())
            dp[i+2]+=dp[i];

        dp1[i+1]=dp1[i]+dp[i];
    }

    mint ans=0;
    //cerr<<"s.size()="<<s.size()<<endl;
    for(int v : s2) {
        if(v>=N)
            continue;

        int b=0;    /* number of used bits in v */
        int vv=1;
        while(vv<=v) {
            vv*=2;
            b++;
        }

        //cerr<<bitset<31>(v)<<" b="<<b<<" ans+="<<dp[p-b].val()<<endl;

        ans+=dp1[p-b+1];
    }
    cout<<ans.val()<<endl;

}

signed main() {
    solve();
}
