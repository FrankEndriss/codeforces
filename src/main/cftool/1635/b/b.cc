/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

constexpr int INF = 1e18;
vi idx;
/* auxilary list of lis, last element is p[ans], prelast is p[p[ans]] etc while p[ans]>=0 */
vi p;

vi dd;

/* see https://cp-algorithms.com/sequences/longest_increasing_subsequence.html
 *  * @return length of longest increasing subseq */
int lis(vector<int> const& a) {
    const int n = a.size();
    vector<int> d(n+1, INF);
    d[0] = -INF;
    p.resize(max(idx.size(), a.size()+1));
    idx.resize(max(idx.size(), a.size()+1));
    idx[0]=-1;

    int ans = 0;
    for (int i=0; i<n; i++) {
        int j = lower_bound(d.begin(), d.end(), a[i]) - d.begin();
        d[j] = a[i];
        idx[j]=i;
        p[i]=idx[j-1];

        if(j>ans)
            ans=j;
    }

    dd=d;
    return ans;
}

/**
 * Whole array must be in the end non-increasing or non decreasing
 * 
 * let lis(a[]) be length of longest increasing subseq.
 * ans=n-lis(a[])
 * Also try inverse(a[])
 *
 * ...
 * Ok, solved the wrong problem once again.
 * The array must not contain
 * increasing part and then decreasing part.
 * But it is ok to decrease, then increase.
 *
 * We want to have the smallest possible value at each position,
 * because then its allways increasing. ??
 *
 * I dont see it :/
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans1=lis(a);
    for(int i=0; i<n; i++) 
        a[i]=-a[i];
    int ans2=lis(a);

    int ans=ans2;
    int mul=1;
    if(ans1>ans2)  {
        for(int i=0; i<n; i++) 
            a[i]=-a[i];

        lis(a);
        ans=ans1;
        cerr<<"orig"<<endl;
    } else  {
        cerr<<"inve"<<endl;
        mul=-1;
    }

    /*
    cerr<<"a[]=";
    for(int i : a) 
        cerr<<i<<" ";
    cerr<<endl;
    */
    int val=*max_element(all(a));
    dd.push_back(val);
    /*
    cerr<<"dd[]= ";
    for(int i=1; i<=ans; i++) {
        cerr<<dd[i]<<" ";
    }
    cerr<<endl;
    */

    dd[0]=dd[1];
    vi idx; /* positions of dd[i] */
    int didx=1;
    for(int i=0; didx<=ans && i<n; i++) {
        if(a[i]==dd[didx]) {
            idx.push_back(i);
            didx++;
        }
    }
    assert(didx==ans+1);

    int prev=0;
    for(int j=0; j<idx[0]; j++) 
        a[j]=a[idx[0]];
    for(int j=idx.back()+1; j<n; j++) 
        a[j]=a[idx.back()];

    for(int j=1; j<idx.size(); j++) {
        for(int k=idx[j-1]+1; k<idx[j]; k++)
            a[k]=a[idx[j]];
    }

    cout<<ans<<endl;

    for(int i : a) 
        cout<<i*mul<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
