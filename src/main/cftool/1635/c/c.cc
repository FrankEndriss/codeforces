/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can never change the last two elements.
 * Then we need to operate from right to left.
 *
 * We cannot simply double the value in each step.
 * On contrair, we need to minimize the step size
 * from one number to the next.
 *
 * Consider the third last element.
 * We can subst by a[n-2]-a[n-1], or not subst.
 * Note that each subst results allways in a negative value.
 * And we can never use a negative value as a[z], since that
 * would make the array decreasing.
 * So make all positions same?
 *
 * Use the longest non-decreasing postfix,
 * then subst all numbers before that postfix
 * with one value obtained by the postfix.
 * So just use the last two ones anyway.
 *
 * If last value is positive we can simply subst all by 
 * the diff of the last two.
 *
 * So case last two like 
 * -1, -1
 *  Then we cannot subst 3rd-last, since -1-(-1)=0.
 *  So then it only works if the array is sorted anyway.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi b=a;
    sort(all(b));
    if(a==b) {
        cout<<0<<endl;
        return;
    }

    if(a[n-1]<a[n-2]) {
        cout<<-1<<endl;
        return;
    }

    if(a[n-1]>=0) {
        cout<<n-2<<endl;
        for(int i=0; i<n-2; i++)
            cout<<i+1<<" "<<n-1<<" "<<n<<endl;
        return;
    }

    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
