/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* if n/k is odd the middle segement must be plaindrome, too.
 *
 * the k segments must be palindromes, too.
 *
 * the letters x, x+(k*y), ...half... ,n-1-(k*y)-x, n-1-x
 * must be same, so count that freqs.
 *
 **/
void solve() {
    cini(n);
    cini(k);
    cins(s);

/* there are (k+1)/2 positions */
    vvi f(26, vi((k+1)/2)); // f[i][j]== freq of i at position j

    for(int seg=0; seg<n/k; seg++) {
        for(int i=0; i<k; i++) {
            int idx=min(i, k-1-i);
            f[s[seg*k+i]-'a'][idx]++;
        }
    }

    int ans=0;
    for(int i=0; i<(k+1)/2; i++) {
        int sum=0;
        int ma=0;
        for(int j=0; j<26; j++) {
            ma=max(ma, f[j][i]);
            sum+=f[j][i];
        }
        ans+=sum;
        ans-=ma;
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

