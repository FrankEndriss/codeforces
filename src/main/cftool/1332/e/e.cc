/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MOD=998244353;

/* we need to find the number of grids having 
 * odd sized hole in it.
 *
 * A hole is an area surrounded by stacks of
 * size R.
 * Note that the borders of the grid cound as surrounding, too.
 *
 * Let the inner size of a hole be the nuber of stacks.
 * Let the outer size of a hole the inner size plus the
 * surrounding stacks.
 *
 * let L=0 and R max height
 * Then the number of different holes of one size is:
 * (innerSize*R+1)/2
 * The number of outside worlds for such holes is:
 * R^^(n*m-outerSize)
 *
 * How to find number of holes of some size?
 * Holes of size 1:
 * four corners, ie 1;n*m-3
 * (n-2)*2 + (m-2)*2 edges, ie 1;n*m-4
 * (n-2)*(m-2) solits, ie 1;n*m-5
 *
 * Holes of size 2x1:
 * 8 corners
 * ...
 * this does not work, n,m are up to 1e9
 */
void solve() {
    cini(n);    // rows
    cini(m);    // cols
    cini(L);    // min height
    cini(R);    // max height

    int ans=0;

/
    for(int i=1; i<=n; i++) {
        for(int j=1; j<=m; j++) {
/* calc the holes of shape (and inner size) i*j */

            /* four of them are in the corners*/
            int cornercnt=4;
            if(i==n || j==m)
                cornercnt=2;
            if(i==n && j==m)
                cornercnt=1;

            int holesize=i*j;
            int holedeep=(holesize*R+1)/2;
            
        }
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

