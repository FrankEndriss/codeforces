/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;


/* we need to find the biggest set of indexes with common divisor, color it.
 * 11 primes:
 */
const vi pr={ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 };
void solve() {
    cini(n);
    cinai(a,n);

    vi ans(n);  // colors of elements
    vb used(pr.size());

    int col=1;
    for(int p : pr) {
        for(int i=0; i<n; i++) {
            if(ans[i]==0 && a[i]%p==0) {
                ans[i]=col;
                used[col-1]=true;
            }
        }
        if(used[col-1])
            col++;
    }

    int cnt=0;
    for(bool b : used)
        if(b)
            cnt++;
    

    cout<<cnt<<endl;
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

