/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(l);
    cini(r);
    cini(d);
    cini(u);

    cini(startX);
    cini(startY);
    cini(x1);
    cini(y1);
    cini(x2);
    cini(y2);

    int xdist=r-l;
    int ydist=u-d;

    if(abs(x1-x2)==0 && r>0) {
        cout<<"No"<<endl;
        return;
    }
    if(abs(y1-y2)==0 && u>0) {
        cout<<"No"<<endl;
        return;
    }

    if(startX+xdist>=x1 && startX+xdist<=x2 && startY+ydist>=y1 && startY+ydist<=y2)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while (t--)
        solve();
}

