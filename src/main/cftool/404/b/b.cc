/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cind(a);
    cind(d);
    cind(n);

    cout<<setprecision(10)<<fixed;
    const double len=n*d+.5;
    int i=1;
    for(int i=1; d*i<len; i++) {
        /* find at which of the four sides we are */
        int sides=(d*i)/a;
        int side=sides%4;

        double dist=d*i-sides*a;
        if(side==0) {
            cout<<dist<<" "<<0.0<<"\n";
        } else if(side==1) {
            cout<<a<<" "<<dist<<"\n";
        } else if(side==2) {
            cout<<a-dist<<" "<<a<<"\n";
        } else
            cout<<0.0<<" "<<a-dist<<"\n";
    }
    cout<<endl;
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

