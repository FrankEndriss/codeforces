/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * All the s[i]==-1 get a[i]=0.
 * so such s[i]=s[p[i]].
 *
 * This creates a consistent tree, but not an optimal one.
 * For even levels we can increment the a[] values, and
 * decrement the a values of the childs by at max the min child
 * val. Since that child becomes a[chl]=0.
 */
void solve() {
    cini(n);
    vi p(n);
    vvi adj(n);
    for(int i=1; i<n; i++) {
        cin>>p[i];
        p[i]--;
        assert(p[i]>=0 && p[i]<n);
        adj[p[i]].push_back(i);
        adj[i].push_back(p[i]);
    }

    cinai(s, n);

    const int INF=1e18;
    vi a(n,-1);
    a[0]=s[0];

    function<void(int,int,int)> dfs=[&](int node, int pp, int lvl) {
        if(s[node]==-1) {
            s[node]=s[pp];
            a[node]=0;
        } else {
            if(a[node]==-1)
                a[node]=s[node]-s[pp];
        }

        int mi=INF;
        for(int chl : adj[node]) {
            if(chl!=pp) {
                dfs(chl, node, lvl+1);
                mi=min(mi, a[chl]);
            }
        }

        if(lvl%2==0) {
            if(mi!=INF && mi>0) {
                int dec=mi;
                a[node]+=dec;
                s[node]+=dec;
                for(int chl : adj[node]) {
                    if(chl!=pp)
                        a[chl]-=dec;
                }
            }
        }
    };

    dfs(0, -1, 1);

    int mi=*min_element(all(a));

    if(mi<0) {
        cout<<-1<<endl;
    } else {
        int ans=accumulate(all(a), 0LL);
        cout<<ans<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
