/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);
    cinai(a,n);

    vector<pii> ans;
    for(int i=0; i<n; i++) {
        vi d;

        function<bool(int)> check=[&](int dd) {
            for(int j=0; j<d.size(); j++) {
                if(gcd(d[j]+dd, a[i])==1) {
                    ans.emplace_back(d[j],dd);
                    return true;
                }
            }
            return false;
        };

        bool fini=false;
        for(int j=2; j*j<=a[i]; j++) {
            if(a[i]%j==0) {
            
                if(check(j)) {
                    fini=true;
                    break;
                }
                d.push_back(j);

                if(j*j!=a[i]) {
                    if(check(a[i]/j)) {
                        fini=true;
                        break;
                    }
                    d.push_back(a[i]/j);
                }
            }
        }

        if(!fini)
            ans.emplace_back(-1, -1);
    }

    for(int i=0; i<ans.size(); i++) {
        cout<<ans[i].first<<" ";
    }
    cout<<endl;
    for(int i=0; i<ans.size(); i++) {
        cout<<ans[i].second<<" ";
    }
    cout<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
