/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=998244353;

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* First subarr can end anywhere after first b[0].
 * Second subarr anywhere after then next b[1].
 * ...
 * Find number of ways.
 * min positions from left
 * max positions from right
 * then count multiples.
 *
 * First subarr is from 0 
 * to dpMin[0] to dpMax[0], second
 * to dpMin[1] to dpMax[1]
 * etc
 *
 * How to combine min and max?
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    cinai(b,m);

    vi dpMin(m);
    int idx=0;
    for(int i=0; idx<m && i<n; i++) {
        if(a[i]==b[idx]) {
            dpMin[idx]=i;
            idx++;
        }
    }
    if(idx<m) {
        cout<<0<<endl;
        return;
    }


    vi dpMax(m);
    idx=m-1;
    for(int i=n-1; idx>=0 && n>=0; i--) {
        if(a[i]==b[idx]) {
            dpMax[idx]=i;
            idx--;
        }
    }

/* set elem i starting at idx */
    function<int(int,int)> cnt=[&](int i, int idx) {
cerr<<"cnt, i="<<i<<" idx="<<idx<<endl;
        int ans=0;
        for(int ii=max(idx, dpMin[i]); ii<dpMax[i]; ii++) {
            if(i==m-1)
                ans=pl(ans,1);
            else
                ans=pl(ans, cnt(i+1, ii+1));
        } 
        return ans;
    };

    int ans=0;
    for(int i=dpMin[0]+1; i<=dpMax[0]+1; i++) 
        ans=pl(ans, cnt(1, i));

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
