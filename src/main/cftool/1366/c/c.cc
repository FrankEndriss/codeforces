/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Every path is palin if all cells
 * in all diagonals are same color.
 * Since ith step is allways on ith diagonal.
 * It is n+m-1 steps.
 *
 * Let color of ith step be s[i], then it must be
 * s[i] == s[n+m-1-i]
 *
 * Fuck it.
 * Brute force, we fill the grid with stepnumbers.
 */
void solve() {
    cini(n);
    cini(m);
    vvi a(n, vi(m));
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            cin>>a[i][j];
        }
    }

    const int INF=1e9;
    vvi s(n, vi(m, INF));
    queue<pii> q;
    q.push({0,0});
    s[0][0]=0;

    while(q.size()) {
        pii p=q.front();
        q.pop();

        if(p.first+1<n && s[p.first+1][p.second]>s[p.first][p.second]+1) {
            s[p.first+1][p.second]=s[p.first][p.second]+1;
            q.push({p.first+1, p.second});
        }

        if(p.second+1<m && s[p.first][p.second+1]>s[p.first][p.second]+1) {
            s[p.first][p.second+1]=s[p.first][p.second]+1;
            q.push({p.first, p.second+1});
        }
    }

/*
cerr<<"stepmat"<<endl;
for(int i=0; i<n; i++) {
    for(int j=0; j<m; j++)
        cerr<<s[i][j]<<" ";
    cerr<<endl;
}
*/

    vector<vector<pii>> ps(n+m-1);
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) 
            ps[s[i][j]].emplace_back(i,j);

    int ans=0;
    for(int k=0; k<ps.size()/2; k++) {
        vi cnt(2);
        for(auto p : ps[k])
            cnt[a[p.first][p.second]]++;

        for(auto p : ps[ps.size()-1-k])
            cnt[a[p.first][p.second]]++;

        ans+=min(cnt[0], cnt[1]);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
