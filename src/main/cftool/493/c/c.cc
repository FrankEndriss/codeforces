/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Count event sorted by 
 * distance, collect max balance.
 */
void solve() {
    cini(n);
    vector<pii> t(n);
    for(int i=0; i<n; i++) {
        cin>>t[i].first;
        t[i].second=-1;
    }
    cini(m);
    for(int i=0; i<m; i++) {
        cini(aux);
        t.emplace_back(aux, 1);
    }
    sort(all(t));

    int a=n*3;
    int ansa=a;
    int b=m*3;
    int ansb=b;
    int bal=a-b;
    int mabal=bal;
    for(size_t i=0; i<t.size(); i++) {
        bal+=t[i].second;
        if(t[i].second==-1)
            a--;
        else
            b--;

        if(bal>mabal) {
            ansa=a;
            ansb=b;
            mabal=bal;
        }
    }
    cout<<ansa<<":"<<ansb<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
