/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Editorial:
 * The first thing to notice is that at the i-th step, all repainted cells will
 * lie in a rectangle with vertices in the cells (i, i), (ni, mi) (if we assume
 * that (1,1) is the left upper cell, and (n, m) - lower right). Then the solution
 * follows from here: you need to calculate the number of cells of such a type that
 * the minimum distance to the side is exactly x. That is, for the cell (a, b),
 * min (min (a, b), min (n - a + 1, m - b + 1)) = x should be satisfied.
 */
void solve() {
    cini(n);
    cini(m);
    cini(x);

    x--;
    int ans=0;
    for(int i=0; i<n; i++) {    
        for(int j=0; j<m; j++) {
            if((i+j)&1)
                continue;

            if(min(min(i, n-1-i), min(j, m-1-j))==x)
                ans++;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
