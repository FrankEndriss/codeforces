/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0LL;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Note that we can swap all bits at a position,
 * or let it like it is.
 *
 * Consider the MSB, and pairs of the bit at some
 * pair of indexes i,j:
 * if 1,0 its an inversion
 * if 0,1 its no inversion
 * if 0,0 or 1,1 it could be an inversion, depending on the lower bits.
 *
 * Consider greedy choose the MSB to have least possible inversions
 * in that bit.
 * Then choose both bitgroups, and do the same for the MSB-1 bit which
 * creates 4 groups... and so on.
 * ...does not work :/
 * ****
 * Editorial uses a trie...somehow.
 */
void solve() {
    cini(n);
    cinai(a,n);

    /* return 1 if flip creates less inversion than notflip, else 0 */
    function<int(vvi&,int)> go=[&](vvi &idx, int b) {
        vi ans(2); /* notflipped, flipped */
        for(size_t j=0; j<idx.size(); j++)  {
            vi cnt(2); /* notflipped, flipped */
            for(int i : idx[j]) {
                int bb=(a[i]&b)!=0;
                if(bb==0)
                    ans[0]+=cnt[0];
                if(bb==1)
                    ans[1]+=cnt[1];
                cnt[0]+=bb;
                cnt[1]+=(1-bb);
            }
        }
        if(ans[1]<ans[0])
            return 1LL;
        else
            return 0LL;
    };

    vvi id(1, vi(n));
    iota(all(id[0]), 0LL);

    int ans=0;
    for(int i=30; i>=0; i--) {
        const int b=(1LL<<i);
        int flip=go(id, 1LL<<i);
        ans|=(flip<<i);
        vvi id0;
        for(size_t j=0; j<id.size(); j++) {
            size_t cnt=0;
            for(int k : id[j])
                cnt+=(a[k]!=0);

            if(cnt==0 || cnt==id.size()) 
                id0.push_back(id[j]);
            else {
                vi e;
                id0.push_back(e);
                id0.push_back(e);
                for(int k : id[j])
                    id0[id0.size()-1-(1-(a[k]!=0))].push_back(k);
            }
        }
    }

    /* compress to then count inversion using a segment tree */
    map<int,int> m;
    stree seg(n+1);
    int ans0=0;
    for(int i=0; i<n; i++)  {
        a[i]^=ans;
        int val=m[a[i]];
        if(val>0)
            a[i]=val;
        else {
            m[a[i]]=m.size()+1;
            a[i]=(int)m.size();
        }
        ans0+=seg.prod(a[i], n+1);
        seg.set(a[i], seg.get(a[i])+1);
    }
    cout<<ans0<<" "<<ans<<endl;


}

signed main() {
        solve();
}
