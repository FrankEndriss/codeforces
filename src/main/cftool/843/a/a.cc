/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* Note that elements do _not_ form a permutation.
 * We need to put all elements of one cycle of the
 * permutation into one group.
 */
void solve() {
    cini(n);
    vi a(n);
    map<int,int> f;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        f[a[i]]=i+1;
    }

/* first make a a permutation */
    int p=1;
    for(auto ent : f)
        a[ent.second-1]=p++;

/* then find positions of elements */
    vi pos(n);
    for(int i=0; i<n; i++)
        pos[a[i]-1]=i;

    vvi ans;
    vb vis(n);
    int ma=0;
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;

        ans.push_back(vi());

/* now we collect all elements of the cycle in the permutation
 * which includes current element (ie. i+1)
 */
        set<int> path;
        int curElement=i+1;
        while(path.count(curElement)==0) {
            path.insert(curElement);
            vis[curElement-1]=true;
            ans.back().push_back(pos[curElement-1]+1);
            curElement=a[curElement-1];
        }
    }

    cout<<ans.size()<<endl;
    for(int i=0; i<ans.size(); i++) {
        cout<<ans[i].size()<<" ";
        for(int j : ans[i])
            cout<<j<<" ";
        cout<<endl;
    }
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

