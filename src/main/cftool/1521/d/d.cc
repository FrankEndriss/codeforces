/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* from all vertex with deg>2 we need to remove edges
 * So it seems optimal to start with the longest path in the
 * tree as the first bamboo.
 * Then cut all subtrees from the bamboo and connect the longest path of 
 * the subtree to the end of the bamboo.
 *
 * a dfs works O(n), and each search for the longest path cuts the tree
 * in smaller peaces. If the longest path is short there are a lot of subtrees,
 * it the longest path is long then the size of the bamboo grows quickly.
 * So this runs in O(n*sqrt(n)) or the like.
 *
 * Finding the longest path in the subtree by twice dfs.
 */
void solve() {
    cini(n);
    vector<set<int>> adj;
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].insert(v);
        adj[v].insert(u);
    }

    vi l(n);    /* level */
    function<int(int)> go=[&](int v) {
        ... TODO
    };
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
