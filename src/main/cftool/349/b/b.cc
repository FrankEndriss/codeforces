/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Greedy
 * We need to find max number of possible digits,
 * v/min(a[i]).
 * Then, consider extra paint, and try to paint as much
 * biggest numbers as possible.
 */
void solve() {
    cini(v);
    cinai(a,9);

    int mi=*min_element(all(a));
    int d=v/mi;
    if(d==0) {
        cout<<-1<<endl;
        return;
    }

    int idx=-1;
    for(int i=0; i<9; i++) 
        if(a[i]==mi)
            idx=i;
    assert(idx>=0);

    string ans(d, '1'+idx);
    int extra=v-d*mi;
    for(int i=0; i<d; i++) {
        for(int k=8; k>idx; k--) {
            if(extra+mi>=a[k]) {
                ans[i]='1'+k;
                extra-=a[k];
                extra+=mi;
                break;
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
