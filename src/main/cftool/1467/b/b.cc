/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* hill==local max
 * vall==local min
 * We want to minimize the number of hill+vall.
 *
 * Consider 1 2 1 2 1
 * Change the center 1 to get -3
 * ... Simpley check all positons.
 *
 * Cosider changing each position to the value of the
 * 4 surrounding values, and count the v/h afterwards.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vb h(n);
    vb v(n);
    int ans=0;
    for(int i=1; i+1<n; i++) {
        h[i]=a[i]>a[i-1] && a[i]>a[i+1];
        v[i]=a[i]<a[i-1] && a[i]<a[i+1];

        if(h[i])
            ans++;
        if(v[i])
            ans++;
    }

    function<int(int)> cnt=[&](int i) {
        int res=0;
        if(i>=2) {
            if(a[i-2]>a[i-1]&&a[i]>a[i-1])
                res++;
            if(a[i-2]<a[i-1]&&a[i]<a[i-1])
                res++;
        }

        if(a[i-1]>a[i]&&a[i+1]>a[i])
            res++;
        if(a[i-1]<a[i]&&a[i+1]<a[i])
            res++;

        if(i+2<n) {
            if(a[i]>a[i+1]&&a[i+2]>a[i+1])
                res++;
            if(a[i]<a[i+1]&&a[i+2]<a[i+1])
                res++;
        }
        return res;
    };

    int ma=0;
    for(int i=1; i+1<n; i++) {
        int v1=cnt(i);
        vi vals;
        if(i-2>=0)
            vals.push_back(a[i-2]);

        vals.push_back(a[i-1]);
        vals.push_back(a[i+1]);
        if(i+2<n)
            vals.push_back(a[i+2]);

        const int aa=a[i];
        int v2=v1;
        for(int j : vals) {
            a[i]=j;
            v2=min(v2, cnt(i));
            a[i]=aa;
        }

        int d=v1-v2;
        ma=max(ma,d);
    }

    cout<<max(0LL, ans-max(1LL,ma))<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
