/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/*
 * Consider cell j at position j, 
 * there is no need to consider j<i-k and j>i+k
 * Each cell contributes once for beeing the first cell of a path.
 *
 * Then consider the number of times it is the second cell:
 * a[0] and a[n-1] are once, all other twice.
 * third cell, consider paths:
 * i, i-1, i;
 * i-2, i-1, i;
 * i+2, i+1, i;
 * fourth, paths:
 * i-3, i-2, i-1, i;
 * i-1, i-2, i-1, i;
 * ...and so on.
 * number of times cell i is jth cell in a path, dp0[i]=
 * dp[i-1]+dp[i+1]
 *
 * dp2[i]=number of times cell i contributes.
 *
 * Then, ans=sum(a[i]*dp2[i])
 * So, if a[i] changes, we need to adjust ans by diff*dp2[i]
 *
 */
void solve() {
    cini(n);
    cini(k);
    cini(q);

    cinai(a,n);

    vi dp(n, 1);    /* dp[i] in kth round, number of times cell i is kth cell in path */
    vi dp2(n, 1);   /* dp2[i] number of times cell i contributed until current k */

    for(int i=0; i<k; i++) {
        vi dp0(n);
        for(int j=0; j<n; j++) {
            if(j>0)
                dp0[j]=pl(dp0[j], dp[j-1]);
            if(j+1<n)
                dp0[j]=pl(dp0[j], dp[j+1]);

            dp2[j]=pl(dp2[j], dp0[j]);
        }
        dp.swap(dp0);
    }

    cerr<<"dp2=";
    for(int i : dp2)
        cerr<<" "<<i;
    cerr<<endl;

    int ans=0;
    for(int i=0; i<n; i++) 
        ans=pl(ans, mul(a[i], dp2[i]));

    for(int i=0; i<q; i++) {
        cini(idx);
        idx--;
        cini(x);
        int diff=pl(x, -a[idx]);

        ans=pl(ans, mul(diff, dp2[idx]));
        cout<<ans<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
