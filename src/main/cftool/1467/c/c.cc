/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Let n=n1+n2+n3
 * There are n-1 remove operations in sets a[],b[],c[].
 *
 * There are two cases how to minimize the negative contribution
 * elements, use what is better:
 * 1. use two (smallest) elements from two different bags.
 * 2. use all elements of one bag
 * see editorial for proof.
 */
void solve() {
    cini(n1);
    cini(n2);
    cini(n3);
    cinai(a,n1);
    cinai(b,n2);
    cinai(c,n3);
    vi sums={ accumulate(all(a), 0LL), accumulate(all(b), 0LL), accumulate(all(c),0LL) };
    int sum=accumulate(all(sums), 0LL);
    sort(all(sums));

    vi mi={ *min_element(all(a)), *min_element(all(b)), *min_element(all(c)) };
    sort(all(mi));

    int ans=max(sum-(mi[0]+mi[1])*2, sum-sums[0]*2);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
