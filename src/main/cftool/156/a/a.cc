/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Levenstein distance.
 *  it is the maximum len common subseq...
 *
 * Brute force, simply check levenstein dist for all substring?
 * it is 2e6 with a avg len of ~300.
 * No, levenstein is O(n^2)
 *
 * Note that the sum of ld of some substrings of s and u
 * is same to the lv dist of the whole strings.
 * So, we can match 
 * s(0,i] to u(0,j], and
 * s(i,n] to u(j,m]
 * How to find optimal borders of substrings?
 *
 *
 * Greedy
 * We start with empty t, ld==u.size()
 * if we add any char from s to t which is in u, then ld--.
 * All those 1 char strings can propably be extended by second
 * char which leaves aus with a set of 2-char t.
 * Repeat until only one string left.
 * But: By adding one char, the lv can inc, dec, or stay same. So
 * we end up checking all substrings...brute force.
 *
 * Or is it simly to substring from s with the longest
 * common subseq with u, where the number of changes is
 * abs(t.size()-u.size())+ t.size()-lcs.size()
 * So we can find optimal t for increasing size of t.
 *
 * optimal t of len l is that t with longest lcs with u.
 * How to find that?
 *
 * Iterate over incresing size of lcs. Foreach find shortest t.
 * How?
 * lcs of size==1 is every letter in s common to a letter in u.
 * ???
 *
 * -> Tutorial
 *  It turns out that the optimal t is allways of len u.size(),
 *  so we just check all substrings in s of that size and count
 *  the diff positions. Ans is the min of it.
 *  Example:
 *  s=12345
 *  u=1245
 *  t=1234 | 2345 whith both a dist of 2
 */
void solve() {
    cins(s);
    cins(u);

    string ss(u.size(), '#');
    s=ss+s+ss;

    int ans=u.size();
    for(size_t i=0; i+u.size()<=s.size(); i++) {
        int lans=0;
        for(size_t j=0; j<u.size(); j++) 
            if(s[i+j]!=u[j])
                lans++;

        ans=min(ans, lans);
    }
    cout<<ans<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
