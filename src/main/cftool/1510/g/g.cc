/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* starting at root each subtree has
 * 1. a certain number of vertex
 * 2. a min lenght path to cover all of them and return.
 * 3. From 1 and 2 we can calculate the min length path
 *    for a given number of vertex.
 *    Note that we need that number for "without return" 
 *    and "with return".
 *
 * Counting the subtree nodes is trivial.
 * Finding min length path to cover all is dp, its sum of all childrens
 * all(c) + 2*numChildren
 *
 * So, to calc the min length path of subtree x with pathlen<cnt(x) we do:
 * find the cheapest combination of childs/cnt to cover
 * x-1 vertex.
 */
void solve() {
    cini(n);
    cini(k);
    vvi adj(n);

    for(int i=1; i<n-1; i++) {
        cini(p);
        p--;
        adj[p].push_back(i);
    }

    vi cnt(n);
    function<void(int)> gocnt=[&](int v) {
        cnt[v]=1;
        for(int chl : adj[v])
            cnt[v]+=gocnt(chl);
    };
    gocnt(0);

    /* dp[j][i][k]=min cost to get kk vertex starting at i 
     * j==0:without return,
     * j==1 with return
     */
    vvvi dp(2, vvi(n, vi(n+1, -1)));
    for(int i=0; i<n; i++)  {
        dp[0][i][0]=0;
        dp[1][i][0]=0;
        dp[0][i][1]=1;
        dp[1][i][1]=2;
    }

    /* min cost to get kk vertex starting at v. */
    function<int(int,int)> go=[&](int v, int kk) {
        if(dp[v][kk]>=0)
            return dp[v][kk];

        vi ks(kk+1, INF);
        for(int i=0; i<adj[v].size(); i++) {
            // TODO
        }
    };
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
