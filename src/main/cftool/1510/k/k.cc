/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void op1(vi &a) {
    for(size_t i=0; i<a.size(); i+=2)
        swap(a[i], a[i+1]);
}

void op2(vi &a) {
    const size_t n=a.size()/2;
    for(size_t i=0; i<n; i++)
        swap(a[i], a[n+i]);
}

/* Nice story :)
 * How to sort the array anyway?
 * -> idk
 * How to move the smallest number in first position?
 * Op1 swaps odd with even positions.
 * Case n is odd Op2 does not swap odd/even, too.
 * Case n is even, Op2 does swap odd/even.
 *
 * So, for odd n there are only 4 configurations possible.
 *
 * Case even n:
 * Here, Op1 makes an element...
 * complecated :/
 *
 * ***
 * Observe that it never makes sense to do the same operation twice
 * in a row, it makes only sense to alternate.
 * So there are only two seqs of operations
 * 0101010...
 * 1010101...
 * Just try both of them until the right one is found, 
 * or a previously seen one occurs again.
 */
void solve() {
    cini(n);
    cinai(a,2*n);
    vi aa=a;
    vi sa=a;
    sort(all(sa));

    set<vi> seta;
    set<vi> setaa;
    seta.insert(a);
    setaa.insert(aa);
    int ans=0;
    while((a.size()>0 || aa.size()>0) && a!=sa && aa!=sa) {
        if(ans%2==0) {
            op1(a);
            op2(aa);
        } else {
            op1(aa);
            op2(a);
        }
        ans++;

        if(seta.count(a))
            a.clear();
        seta.insert(a);
        if(setaa.count(aa)) 
            aa.clear();
        setaa.insert(aa);
    }
    if(a==sa || aa==sa) {
        cout<<ans<<endl;
    } else
        cout<<-1<<endl;
}

signed main() {
    solve();
}
