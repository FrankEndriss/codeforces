/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* To construct some last digit d0
 * we need to consider only the last digit of all numbers,
 * and the count of them.
 * So, 
 * 1*2==2, 2*2==4, 3*2==6, 4*2==8 etc
 * 1 2 3 4 5 6 7 8 9
 * 2 4 6 8 2
 * 3 9 7 1 3
 * 4 8 6 4 
 * 5 5
 * 6 6
 * 7 9 3 1 7
 * 8 4 2 6 8
 * 9 1 9
 *
 * So we can construct the searched digit by multing all 
 * numbers, but removing a certain number.
 *
 * ***
 * Consider a[i] categorized by last digit.
 * There are certain compibations of numbers we must not use.
 * i.e
 * 1. if d!=0 we must not use any number ending in 0
 * 2. if d!=0 ... a 2 and a 5
 * So we need to make two cases, d==0 and d!=0.
 * if d==0 we can use all numbers, it that does not end in 0
 * there is no sol.
 * Else d!=0
 * ...complecated graph construct...
 *
 * ** 
 * What about prime fact?
 * -> Its basically the same as last digit, but more complecated.
 * ** 
 * Define "must not" rules, then choose greedyly the cheapest ones.
 * consider d!=0
 * 1. no 2 and 5
 * ...actually no other rule :)
 *
 * But, we need to find the cheapest set of not use-numbers.
 * let ad be the product of all numbers (except the 2s, or except the 5s)
 * Then, staring at ad, we can build the above adj and get
 * ending numbers by removing certain numbers. Obviously we remove
 * the smallest ones first.
 * So we search a path ending in d with min product of all used vertex.
 *
 * But, there is a problem in comparing the numbers, since the product
 * can be fairly big. However, not so big, since the path if not 
 * very long. So use BigInt?
 * And how to implement the search, dfs?
 *
 */
void solve() {
}

signed main() {
    solve();
}
