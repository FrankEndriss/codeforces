/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Each password needs as much presses as it has digits.
 * But we can save a password if it is a subset of another one.
 * If one pw is a subset of another, then we can
 * save that pw by entering the following one starting at the first.
 * So, if there is a graph with edges from subsets to supersets, we
 * want to go the longest possible paths in it.
 * There are up to 1e4 passwords, so we can build the adj matrix in O(n^2)
 * Then just do a bfs on the reversed graph, starting at the leafs.
 * ...no
 * We need to optimize the graph somehow.
 * consider
 * a - b - d 
 *   \   /
 *     c - e
 * We must not use the path dca, since then
 * b and e would be not saveable.
 * But if using dba we can save e, too.
 * Additionally all vertex have weights (the number of set bits).
 * Basically we want to find a set of paths covering all vertex
 * exactly once
 * with minmum sum(cost). Cost of one path is len(path)+max(weight).
 * Looks like maxflow somehow. Mayby level by level?
 * complecated, idk :/
 */
void solve() {
    cini(d);
    cini(n);
    vi a(n);
    vi cnt(n);
    for(int i=0; i<n; i++) {
        cins(s);
        int j=0;
        int cnt=0;
        for(char c : s) {
            a[i]<<=1;
            if(c=='1') {
                cnt[i]++;
                a[i]|=1;
            }
        }
    }

    vi deg(n);  /* indegree of i */
    vvi adj(n); /* adj[i][j] -> a[i] is subset of a[j] */
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            if((a[i]&a[j])==a[i]) {
                adj[i].push_back(j);
                deg[j]++;
            }
        }
    }

    for(int i=0; i<n; i++)  {
        sort(all(adj[i]), [](int i1, int i2) {
            return __builtin_popcount(i1)<__builtin_popcount(i2);
        });
    }

    vb vis(n);
    int ans=0;
    bool first=true;
    for(int i=0; i<n; i++) 
        if(deg[i]>0)
            continue;
    // ...

        if(!first)  /* press reset */
            ans++;
        first=false;


    }

}

signed main() {
    solve();
}
