/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* even length square has odd number of 1s
 * We would need to put the ones
 * at the borders.
 *
 * Is there a way to make a 4*4 matrix good?
 *
 * 1 0 1 0
 * 0 0 1 1
 * 1 0 0 1
 * 0 0 0 0 
 * ->No
 *
 * 1 0 1
 * 0 0 0
 * 1 0 1
 * 0 0 0 
 * ...
 *
 * 0 1 0
 * 0 0 0
 * 0 1 0
 * 0 0 0
 * ...
 *
 * 1 0
 * 0 0
 * 0 1
 * 1 1
 * 1 0
 * 0 0 
 * ...
 * 
 *
 **/
void solve() {
    cini(n);
    cini(m);

    cinas(s,n);

    if(n>=4 && m>=4) {
        cout<<-1<<endl;
        return;
    }


    if(m<n) {
        vs ss(m);
        for(int i=0; i<m; i++) {
            ss[i]=string(n,' ');
            for(int j=0; j<n; j++) 
                ss[i][j]=s[j][i];
        }
        s.swap(ss);
    }

    if(n==1) {
        cout<<0<<endl;
        return;
    }

    /* n in 2,3 */
    if(n==2) {
        int cnt=0;
        int odd=(s[0][0]=='1' + s[1][0]==1)%2;
        for(int col=1; col<m; col++) {
            int odd2=(s[0][col]=='1' + s[1][col]==1)%2;
            if(odd==odd2)
                cnt++;
            odd=!odd;
        }
        ans=min(cnt, m-cnt);
        cout<<ans<<endl;
    } else if(n==3) {
        //... there are four pattern possible...
        // next time do not waste that much time with 
        // stupid math problems, instead solve
        // problem D. :/
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
