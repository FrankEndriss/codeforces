/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We build a cycle by combining 3 numbers in descending order.
 * So we need to find the number of perms have at least one of them.
 * Also we can find the number of perms that
 * have no such triple, and subtract from n!.
 *
 * No such triple means, foreach a[i] there is at most 1 a[j] right of it
 * with smaller value.
 * So, by pidgeon, in first position there can be 1 or 2.
 * if 2, then in second position there can be 1 or 3
 * if 1, then in second position there can be 2 or 3
 * and so on.
 * ans=n!-2^n-1
 *
 * ...the above "prove" is completly broken :)
 * However, the result is ok.
 */
using mint=modint1000000007;
void solve() {
    cini(n);

    mint ans=1;
    for(int i=2; i<=n; i++) 
        ans*=i;

    mint two=2;
    ans-=two.pow(n-1);
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
