/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find all pathes that lead to lost.
 * Then we need to find a way to modifiy such path
 * after last starting element so that it is no
 * loss any more.
 *
 * How to find min modifications to do that for
 * one path if not possible with
 * one operation?
 *
 * We need to find min manhattan distance from 
 * any cell in any bad path after the last
 * starting element.
 *
 * How to implement?
 * Lost paths are of two kind:
 * -out of border
 * -circles
 *
 * Collect the components.
 * Each component is one of 
 * -good
 * -out of border
 * -circle
 *  Circle ones are complecated...we need to consider
 *  the path into the circle and the circle itselft, 
 *  for each lagguage.
 *
 *
 *  NOTE: We have no special starting points, all points are starting points!
 *  ****************
 *  blah, just fix last row and righmost col
 */
void solve() {
    cini(n);
    cini(m);

    cinas(s,n);

    int ans=0;
    for(int i=0; i<m; i++)
        if(s[n-1][i]=='D')
            ans++;
    for(int i=0; i<n; i++)
        if(s[i][m-1]=='R')
            ans++;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
