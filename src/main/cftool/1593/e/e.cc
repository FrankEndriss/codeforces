/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** multi bfs starting at all leafs will mark
 * all vertices with the distance to the nearest leaf.
 * Count them.
 *
 * How can this be wrong?
 * Some super stupid edge case?
 *
 *
 * ...Oh no :/
 * Leafs are cut, but that does not mean that parents of leafs are
 * cut the next round. How stupid is that :/
 */
void solve() {
    cini(n);
    cini(k);
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    queue<int> q;
    vi dp(n, -1);
    for(int i=0; i<n; i++) 
        if(adj[i].size()<2) {
            dp[i]=0;
            q.push(i);
        }

    while(q.size()) {
        const int v=q.front();
        q.pop();
        for(int chl : adj[v]) {
            if(dp[chl]<0) {
                dp[chl]=dp[v]+1;
                q.push(chl);
            }
        }
    }

    int ans=0;
    for(int i=0; i<n; i++) {
        if(dp[i]<k)
            ans++;
    }
    cout<<n-ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
