/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Find the biggest possible gcd of half of the numbers.
 * How?
 * Since n<=40...
 * sum(n)<100 implies some O(n^3) solution.
 * Or single n<40 some bitset 10^6 solution.
 *
 * If we have all prime-factors how to find the biggest-product-subset of at least
 * half the numbers?
 *
 * Why n is even?
 *
 * Print -1 if initial half the numbers are same.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int mi=*min_element(all(a));
    int g=-1;
    for(int i=0; i<n; i++) {
        if(a[i]!=mi) {
            if(g<0)
                g=a[i]-mi;
            else
                g=gcd(g, a[i]-mi);
        }
    }
    cout<<g<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
