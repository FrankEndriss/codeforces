/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Brute force?
 * Make last two digits 00, 25, 50, 75
 */
const int INF=1e9;
void solve() {
    cini(n);
    vi d;
    int nn=n;
    while(nn) {
        d.push_back(nn%10);
        nn/=10;
    }

    int ans=INF;
    for(string s : { "00", "25", "50", "75"}) {
        int i1=-1;
        for(int i=0; i1<0 && i<d.size(); i++) 
            if(d[i]==s[1]-'0')
                i1=i;

        if(i1>=0) {
            int i2=-1;
            for(int i=i1+1; i2<0 && i<d.size(); i++) 
                if(d[i]==s[0]-'0')
                    i2=i;

            if(i2>=0) {
                ans=min(ans, i2-1);
            }
        }
    }

    assert(ans<INF);
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
