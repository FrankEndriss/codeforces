/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * After adding a[i]+d[i], the result is sorted.
 * So lets sort both arrays first.
 *
 * Then, at each position of a[], we want to map that a[i]
 * to the biggest possible b[bg], and the smallest possible b[k].
 * But of course, we need to check if there is a way to map
 * all the other elements.
 *
 * First of all, after sorting, all a[i]<=b[i] or there is no solution.
 * two problems, min and max.
 *
 * Min:
 * We want to move some b[k<i] to position i, and rotate all
 * elements in segment [k,i] by one position.
 * It holds that b[k]>=a[i] (since d[i] must not be negative), and _all_
 * b[k+1..i] must be "shiftable" one position to the left.
 * So lets find all unshiftable positions (b[i-1]<a[i]) and mark them.
 * let unsh(i) be the rightmost position of unshiftable b[] left of i.
 * Then, the smallest element useable at a[i]: b[max(k,unsh(i)+1)]
 * We can find both using binary search.
 *
 * Max:
 * Basically the same, but here we shift in opposite direction:
 * unshiftable: b[i+1]<a[i]
 * 
 * ...we need to visualize this to not get brain cancer by thinking about it:
 * a={ 1 2 3 4 5 6 7 }
 * b={ 4 4 5 5 5 6 7 }
 *
 * unshL={ 5 };   
 * we cannot shift a[5]=6 to the left, which means all positions
 * left of 5 can be mostly added up to b[5]
 */
void solve() {
    cini(n);

    vector<pii> a(n);
    for(int i=0; i<n; i++) {
        cin>>a[i].first;
        a[i].second=i;
    }
    cinai(b,n);
    sort(all(a));
    sort(all(b));

    vi unshMin;   /* positions of b[i] that cannot be shifted left */

    for(int i=1; i<n; i++) 
        if(b[i]<a[i-1].first)
            unshMin.push_back(i);

    vi ansMin(n); 

    ansMin[a[0].second]=b[0]-a[0].first;
    for(int i=1; i<n; i++) {
        /* Find the position of the smallest number in b[] that
         * is bgeq current a[i], we want to use that.
         * Note that is allways left or up to current a[i].second.
         *
         * But, we cannot use it if some position between it and i 
         * is not shiftable.
         * In this case, we can use the lowest shiftable position.
         */
        auto itB=lower_bound(all(b), a[i].first);
        ansMin[a[i].second]=(*itB)-a[i].first;
        int posB=distance(b.begin(), itB);

        auto it2=lower_bound(all(unshMin), posB);
        int posUnsh=n;
        if(it2!=unshMin.end())
            posUnsh=*it2;
        if(posUnsh<=a[i].second)
            ansMin[a[i].second]=b[posUnsh]-a[i].first;
    }
    for(int i=0; i<n; i++) 
        cout<<ansMin[i]<<" ";
    cout<<endl;

    /* now same for max 
     * here we allways want to use b.back(), but cannot if
     * there is some unshiftable position between current a[i] and right end.
     * In this case we can use the smallest such unshiftable position
     * bgeq the current position.
     * */
    vi unshMax;   /* unshiftable positions for max */
    for(int i=0; i+1<n; i++) 
        if(b[i]<a[i+1].first)
            unshMax.push_back(i);

    vi ansMax(n); 
    for(int i=0; i<n; i++) {
        auto it=lower_bound(all(unshMax), a[i].second);
        if(it==unshMax.end())
            ansMax[a[i].second]=b.back()-a[i].first;
        else
            ansMax[a[i].second]=b[*it]-a[i].first;
    }

    for(int i=0; i<n; i++)
        cout<<ansMax[i]<<" ";

    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
