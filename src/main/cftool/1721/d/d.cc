/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * To switch a bit on in ans we have to find a matching
 * of a[] and b[] so that all pairs are 0/1 or 1/0, which 
 * means the freq of the bit must be mirror/same, else
 * we cannot set that bit in ans.
 *
 * Check MSB to LSB.
 * So, if first bit freq matches, b[] is split into two 
 * halfes, the a[][msb]=0 and a[][msb]=1
 *
 * So, recursively fix for segment and bit-position.
 * Whenever a bit-position cannot be fixed, unset it in ans.
 *
 * ***
 * Consider test:
 * 3
 * 27 30 0
 * 0 5 9
 *
 *  0=00000
 * 27=11011
 * 30=11110
 *
 *  9=01001
 *  5=00101
 *  0=00000
 *
 * *** 
 * We cannot do this recursive, since if the segment is splitted, 
 * one halve may succeed, and is split again by this, but
 * the other does not succeed, and the whole bit is removed.
 * So, maintain all segments, and loop over them foreach bit?
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    vb ans(30, true);

    function<void(int,int,int)> go=[&](int l, int r, int B) {
        const int n=r-l;
        if(n==0 || B<0)
            return;

        int cntA=0;
        int cntB=0;
        const int bb=(1LL<<B);
        for(int i=l; i<r; i++)  {
            if(a[i]&bb)
                cntA++;
            if(b[i]&bb)
                cntB++;
        }

        if(cntA!=n-cntB) {
            cerr<<"false B="<<B<<" l="<<l<<" r="<<r<<" cntA="<<cntA<<" cntB="<<cntB<<endl;
            ans[B]=false;
            for(int i=l; i<r; i++) {
            if(a[i]&bb)
                a[i]-=bb;
            if(b[i]&bb)
                b[i]-=bb;
            }
            go(l,r,B-1);
        } else {
            cerr<<"true B="<<B<<" splits: l="<<l<<" r0="<<l+cntB<<" l0="<<l+cntB<<" r="<<r<<endl;
            sort(a.begin()+l, a.begin()+r);
            sort(b.begin()+l, b.begin()+r, greater<int>());
            go(l, l+cntB, B-1);
            go(l+cntB, r, B-1);
        }

    };

    go(0,n,29);

    int ansi=0;
    for(int i=0; i<30; i++)
        if(ans[i])
            ansi|=(1LL<<i);

    cout<<ansi<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
