/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * no, if there is a way, then we can reach n,m
 * in n-1+m-1 steps.
 *
 * So we check which of the 4 sides are reached by the laser,
 * and we can go two ways:
 * left/bottom 
 * top/right
 * If none of those combinations possible then ans=-1
 */
void solve() {
    cini(n);
    cini(m);
    cini(sx);
    cini(sy);
    cini(d);

    bool top=sx-d>1;
    bool bot=sx+d<n;
    bool lef=sy-d>1;
    bool rig=sy+d<m;
    if((top&&rig) || (lef&&bot)) 
        cout<<n-1+m-1<<endl;
    else
        cout<<-1<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
