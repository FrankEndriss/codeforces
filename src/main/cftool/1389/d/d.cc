/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * It does not matter if we extend the upper bound of the lower
 * segment, or the lower bound of the upper segment.
 * The a and b segments have an initial overlap.
 * The a and b segments have an initial distance.
 * The a and b segments have an initial max extend so that each overlap costs one step.
 * Once they overlap we can mak them overlapping one more with
 * one step until max(size(a), size(b))
 * To make further overlaps we need two steps to get one overlap.
 *
 * How to implement?
 * First correct I by the initial overlap.
 * Then iterate the segments sorted by distance.
 * Foreach, find if it is cheaper to extend the allready intersecting
 * segments (for the cost of two steps per one intersect), or use
 * the next segment. This depends on the initial distance and the max initial overlap.
 * After all segments they max intersect. The still remaining I costs two
 * steps per overlap.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(k);
    vi l(2);
    vi r(2);
    cin>>l[0]>>r[0]>>l[1]>>r[1];
    if(l[0]>l[1]) {
        swap(l[0],l[1]);
        swap(r[0],r[1]);
    }

    /* the size of the initial overlap of both segments */
    const int over=max(0LL, min(r[0]-l[0], min(r[1]-l[1], r[0]-l[1])));
    k-=n*over;

    if(k<=0) {
        cout<<0<<endl;
        return;
    }

    /* the initial distance between the both segments */
    const int dist=max(0LL, l[1]-r[0]);

    /* the number of overlaps we get for cost==1 once we have 
     * closed the dist */
    const int extend=max(r[0], r[1])-l[0] - over;

    int ans=INF;
    for(int i=0; i<n; i++) {
        int lans=(i+1)*dist+i*extend+min(k,extend);
        k=max(k-extend,0LL);
        ans=min(ans, lans+k*2);
    }

    cout<<ans<<endl;
}


signed main() {
    cini(t);
    while(t--)
        solve();
}
