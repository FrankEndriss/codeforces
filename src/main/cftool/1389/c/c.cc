/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * resulting string must be all one digit,
 * or even len alternating two digits.
 */
void solve() {
    cins(s);


    int ans=0;  /* max len remaining */
    for(char c1='0'; c1<='9'; c1++) {
        for(char c2=c1; c2<='9'; c2++) {

            char cur=c1;
            int cnt=0;
            for(size_t i=0; i<s.size(); i++) {
                if(s[i]==cur) {
                    cnt++;
                    if(cur==c1)
                        cur=c2;
                    else
                        cur=c1;
                }
            }
            if(cur!=c1)
                cnt--;

            ans=max(ans, cnt);

            cur=c2;
            cnt=0;
            for(size_t i=0; i<s.size(); i++) {
                if(s[i]==cur) {
                    cnt++;
                    if(cur==c1)
                        cur=c2;
                    else
                        cur=c1;
                }
            }
            if(cur!=c2)
                cnt--;
            ans=max(ans, cnt);
        }
    }
    cout<<s.size()-ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
