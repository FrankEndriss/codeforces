/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * no two left
 * exaclty k
 * sum left <= z
 *
 * moving once left at pos==i increased the score by a[i-1]+a[i]
 * calc every end position
 * We can go max z times one the left.
 */
void solve() {
    cini(n);
    cini(k);
    cini(z);

    cinai(a,n);
    vi pre(n);
    pre[0]=a[0];
    for(int i=1; i<n; i++)
        pre[i]=pre[i-1]+a[i];

    int ans=0;
    for(int i=1; i<min(k+1,n); i++) {
        int extra=k-i;
        int sum=pre[i];
        int idx=i;
        for(int j=0; j<z && extra>0; j++) {
            sum+=a[i-1];
            extra--;
            idx=i-1;
            if(extra) {
                sum+=a[i];
                extra--;
                idx=i;
            }
        }

        if(extra>0)
            sum=sum+pre[idx+extra]-pre[idx];

        ans=max(ans, sum);
    }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
