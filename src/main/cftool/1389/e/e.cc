/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * weekdays of 1st of months are:
 * 0*d%w, 1*d%w, 2*d%w,...,m*d%w
 *
 * There are w/gcd(d,w) different months.
 *
 * The offset between two adjacent months is d%w
 *
 * Let diff=y-x;
 * A pair contributes if 
 * (diff*offset)%w == diff%w
 */
void solve() {
    cini(m);
    cini(d);
    cini(w);

    int offset=d%w;

    int ans=0;
    for(int x=0; x<=min(m,d); x++) {
        for(int y=x+1; y<=min(m,d); y++) {
            int diff=y-x;
            if((diff*offset)%w == diff%w)
                ans++;
        }

    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
