/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void d(int x, stack<int> &ans) {
    while(x) {
        ans.push(x%10);
        x/=10;
    }
}

void solve() {
    cini(x);
    cini(y);

    if(x==y) {
        cout<<"="<<endl;
        return;
    }

    if(x==1) {
        cout<<"<"<<endl;
        return;
    }

    if(y==1) {
        cout<<">"<<endl;
        return;
    }

    if(x==2) {
        if(y==3) {
            cout<<"<"<<endl;
        } else if(y==4)
            cout<<"="<<endl;
        else 
            cout<<">"<<endl;
        return;
    }

    if(y==2) {
        if(x==3) {
            cout<<">"<<endl;
        } else if(x==4)
            cout<<"="<<endl;
        else 
            cout<<"<"<<endl;
        return;
    }


    if(x<y) {
        cout<<">"<<endl;
    } else
        cout<<"<"<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

