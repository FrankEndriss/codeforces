
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Whenever two cols share a cell in same row, we can combine 
 * all cells of both cols, so we can use dsu to connect
 * all colls forming a component.
 *
 * Then we need one other element to connect two of those 
 * components, so ans=groups().size()-1
 *
 * But also we need to consider empty rows.
 * Each empty row needs one element.
 */
void solve() {
    cini(n);
    cini(m);
    cini(q);

    vvi adj(n);
    for(int i=0; i<q;  i++) {
        cini(r); r--;
        cini(c); c--;
        adj[r].push_back(c);
    }

    dsu d(m);
    int ans=0;
    for(int i=0; i<n; i++)  {
        for(size_t j=0; j+1<adj[i].size(); j++)
            d.merge(adj[i][j], adj[i][j+1]);
        if(adj[i].size()==0)
            ans++;
    }

    cout<<ans+d.groups().size()-1<<endl;
}

signed main() {
       solve();
}
