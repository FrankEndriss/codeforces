/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* We can create a dp:
 * dp[k][i][j]=min cost to get in prefix (0,i)  j local maximas
 * with
 * k=0 -> i is not local maximum
 * k=1 -> i is a local maximum
 * see https://codeforces.com/blog/entry/60920?#comment-448239
 *
 *
 * Init:
 * dp[0][0][0]=0
 * dp[1][1][1]=0;
 *
 * But something wrong... :/
 * to complecated
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);
    a.push_back(-1);

    vvvi dp(2, vvi(n+1, vi((n+5)/2, INF)));
    dp[0][0][0]=0;
    dp[1][0][1]=0;

    for(int i=0; i<n; i++) {
        for(int j=0; j<=(n+1)/2; j++) {
            /* not building a house at position i+1 */
            dp[0][i+1][j]=min(dp[0][i][j], dp[1][i][j] + max(0LL, a[i+1]-a[i]+1));
            /* building a house at position i+1, only possible
             * if there is no house at pos i, cost is what we need to excavate at pos i */
            dp[1][i+1][j+1]=dp[0][i][j]+max(0LL, a[i]-a[i+1]+1);
        }
    }

    for(int i=1; i<=(n+1)/2; i++) 
        cout<<min(dp[0][n-1][i], dp[1][n-1][i])<<" ";
    cout<<endl;


}

signed main() {
    solve();
}
