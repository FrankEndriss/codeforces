/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;
const int MOD=1e9+7;


int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/*
 * Go from biggest k_i to smallest, adding or
 * subtracting next number.
 *
 * Next level case
 * 1. current diff==0, so add
 * 2. current diff>0, so subtract. Result is zero or positive.
 *
 * Note: edgecase for p==1!
 */

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

void solve() {
    cini(n);
    cini(p);

    cinai(k,n);

    if(p==1) {
        int ans=n%2;
        cout<<ans<<endl;
        return;
    }

    sort(all(k), greater<int>());

    int diffe=0;    /* exponent of diff */
    int diffm=0;    /* multiplicator of diff */
    int ans=0;      /* absolute value of diff */
    for(int i=0; i<n; i++) {
        if(diffe==-1) {
            ans=pl(ans, -toPower(p, k[i]));
        } else {
            if(diffm==0) {
                diffm=1;
                diffe=k[i];
            } else {
                while(diffe>k[i] && diffm<(1LL<<22)) {
                    diffm*=p;
                    diffe--;
                }
                if(diffe>k[i]) {   /* switch to abs */
                    ans=mul(diffm%MOD, toPower(p, diffe));
                    ans=pl(ans, -toPower(p, k[i]));
                    diffe=-1;
                    diffm=0;
                } else {
                    diffm--;
                }
            }
        }
    }

    if(diffm>0)
        ans=mul(toPower(p,diffe), diffm%MOD);

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
