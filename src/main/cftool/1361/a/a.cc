/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Bad problem statement.
 * Obviously it is a graph of n vertex.
 * A vertex is a blog or a topic, and an edge
 * is between two of them.
 *
 * Sow how about blogs and topics, how do the relate?
 * So it seems every vertex (a blog) has a number written
 * in it (a topic).
 * Connected vertex must have different numbers written.
 *
 * The rule to write numbers is: write smallest number possible
 * into vertex.
 *
 * We need to find the order of vertex so that a given numbering
 * is fullfilled. ie. we write first the 0s, then the 1s etc.
 * And with every write we need to check
 * * if all lower numbers are covered by adj
 * * the number is not covered by adj
 *
 * Simply simulate?
 * For every number we need to check if any of the adj vertex
 * was within the group of vertex.
 */
void solve() {
    cini(n);
    cini(m);
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(a);
        a--;
        cini(b);
        b--;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    cinai(t,n);
    vi id(n);
    iota(all(id), 0);
    sort(all(id), [&](int i1, int i2) {
        return t[i1]<t[i2];
    });

    vi lbl(n);
    for(int i=0; i<n; i++) {
        /* we want to write number t[id[i]] into vertex id[i] */
        vb vis(t[id[i]]);
        vis[0]=true;
        int cnt=1;
        for(int chl : adj[id[i]]) {
            if(lbl[chl]==0) {
                continue;
            } else if(lbl[chl]==t[id[i]]) {
                cout<<-1<<endl;
                return;
            } else if(!vis[lbl[chl]]) {
                vis[lbl[chl]]=true;
                cnt++;
            }
        }
        if(cnt!=t[id[i]]) {
            cout<<-1<<endl;
            return;
        }
        lbl[id[i]]=t[id[i]];
    }
    for(int i=0; i<n; i++)
        cout<<id[i]+1<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
