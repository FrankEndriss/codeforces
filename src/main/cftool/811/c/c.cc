/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The problem asks to create
 * subarrays where in one subarray must be 
 * all same numbers, and if one instance of one number
 * is in an segment, all numbers must be in this segment.
 * So, having one big segment with all numbers in it
 * is always ok.
 *
 * For all other numbers check the min segment size.
 * for each number put leftmost and rightmost instance
 * into the segment, and all leftmost and rightmost
 * of all numbers in the segment, repeat. Until no
 * more new elements. Thats the min size of the segment
 * of the contained numbers.
 *
 * Create all such possible segments, and its comfort worth.
 * Finally find the most valueable disjoint subset of the
 * segments.
 * Thatfor sort segments by left and foreach choose or notchoose it
 * knapsack like.
 */
const int N=5001;
void solve() {
    cini(n);
    cinai(a,n);

    vi le(N, -1);
    vi re(N, -1);
    for(int i=0; i<n; i++) {
        if(le[a[i]]==-1)
            le[a[i]]=i;
        re[a[i]]=i;
    }

    set<pii> seg;    /* possible segments */

    for(int i=0; i<N; i++) {
        int l=le[i];
        int r=re[i];
        if(l<0)
            continue;

/* note that this while loop with q is overkill.
 * we can simply check if all contained a[j] in segment (l,r)
 * are completly within (l,r).
 * If yes use (l,r) as a segement, else dont.
 * This works since all usable segments are framed by a pair
 * of le[i],re[i].
 */
        set<int> q;
        for(int j=l; j<=r; j++)
            q.insert(a[j]);

        while(q.size()) {
            auto it=q.begin();
            int cur=*it;

            int l2=le[cur];
            if(l2<l) {
                for(int j=l2; j<l; j++)
                    q.insert(a[j]);
                l=l2;
            }
            int rr=re[cur];
            if(rr>r) {
                for(int j=r+1; j<=rr; j++) 
                    q.insert(a[j]);
                r=rr;
            }
            q.erase(cur);
        }
        seg.insert({l,r});
    }

/* dp[i]=max comfort value with rightmost occupied index ==i
 */
    vi dp(N, -1);
    dp[0]=0;
    for(auto ent : seg) {
        int cc=0;   /* comfort value for current segment */
        vb vis(N);
        for(int j=ent.first; j<=ent.second; j++) {
            if(!vis[a[j]])
                cc^=a[j];
            vis[a[j]]=true;
        }

        dp[ent.second]=max(dp[ent.second], cc);
        for(int j=0; j<ent.first; j++) {
            if(dp[j]>=0) {
                dp[ent.second]=max(dp[ent.second], dp[j]+cc);
            }
        }
    }
    cout<<*max_element(all(dp))<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
