/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We can create a prefix (and then print it)
 * if the postfix is lex eqbt than a next prefix.
 *
 * Since x always starts with 1 possible cut points
 * are just before every 1.
 * Then we need to try every prefix length, dfs.
 *
 * min len is numerical value of last prefix used plus
 * number of times a prefix was printed.
 *
 * Assume final prefixes of some given length
 * are distinct.
 *
 * Tutorial: we need to find some dp, not recurse
 */
void solve() {
    cins(x);    /* big binary number */

    struct res {
        int p;      /* number of times print action used in shortest seq */
        pii pre;    /* smallest longest prefix used in shortest seq */
        int difseq; /* number of different seqs */
    };

/* @return ans if started at pos pos with prefix pre.
 * pre if of form <first,last> 
 * cntp print actions so far
 **/
    function<res(int,int)> calc=[&](int pos, int cntp, pii pre) {
        res ans;
        int prelen=pre.second-pre.first+1;
        bool diff=false;
        for(int i=0; !diff && i<prelen; i++) {
            if(x[pos+i]!=x[pre.first+i])
                diff=true;
        }
        if(!diff) {
            ans=calc(pos+prelen, cntp+1, pre);
        }

// todo loop...
    };

    res ans=calc(0,0,{0,0});

    cout<<ans.first<<" "<<ans.second<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
