/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(k);

    vi d;
    int ma=0;
    while(k) {
        d.push_back(k%10);
        ma=max(ma, d.back());
        k/=10;
    }

    int fini=false;
    vi vans;
    while(!fini) {
        fini=true;
        int ans=0;
        for(int i=d.size()-1; i>=0; i--) {
            ans*=10;
            if(d[i]!=0) {
                fini=false;
                ans++;
                d[i]--;
            }
        }
        if(!fini)
            vans.push_back(ans);
    }

    cout<<vans.size()<<endl;
    for(int i:vans)
        cout<<i<<" ";
    cout<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
