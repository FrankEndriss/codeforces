/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We need to find for every string i the number 
 * of strings j where in the sum of the freqs
 * at most 1 position is odd.
 *
 * So count parity of the 26 bits per string.
 * Then, for every string find the number of 
 * good pairings by searching the constructed 
 * bitmasks.
 * -where zero bits are odd, or
 * -where 1 bit is odd
 */
void solve() {
    cini(N);
    vi f(N);
    map<int,int> ff;
    for(int i=0; i<N; i++) {
        cins(s);
        vi cnt(26);
        for(char c : s) {
            cnt[c-'a']++;
        }
        for(int j=0; j<26; j++) 
            if(cnt[j]&1) 
                f[i] |= (1<<j);

        ff[f[i]]++;
    }

    int ans=0;
    for(int i=0; i<N; i++) {
        ff[f[i]]--;
        int lans=ff[f[i]];  // zero odds

        for(int j=0; j<26; j++)
            lans+=ff[f[i]^(1<<j)];  // one odd

        ans+=lans;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
