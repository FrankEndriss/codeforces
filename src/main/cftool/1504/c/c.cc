/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Its greedy.
 * both seqs must start with '('.
 * Then try to make both counts most possibly equal.
 * ..
 * On '0' we need to increase the oc in one, and decrease in the 
 * other.
 * On '1' we increase or decrease both.
 * At end both oc must be same.
 * ****
 * At each position i we have lower bound of oc==0, 
 * and upper bound of oc ==(n-i)
 * Further, sum of both oc do not change on s[i]==0,
 * so upper bound is remainingNumbeOf1*2.
 */
void solve() {
    cini(n);
    cins(s);

    int cnt1=0;
    for(char c : s)
        if(c=='1')
            cnt1++;

    string a;
    string b;

    vi cnt(2);
    for(int i=0; i<n; i++) {
        int upper=cnt1*2;
        if(s[i]=='1') {
            if(cnt[0]+cnt[1]+2<=upper) {
                a+='(';
                b+='(';
                cnt[0]++;
                cnt[1]++;
            } else {
                a+=')';
                b+=')';
                cnt[0]--;
                cnt[1]--;
            }
        } else {    /* different */
            if(cnt[0]<cnt[1]) {
                a+='(';
                b+=')';
                cnt[0]++;
                cnt[1]--;
            } else {
                a+=')';
                b+='(';
                cnt[0]--;
                cnt[1]++;
            }
        }
        if(s[i]=='1')
            cnt1--;

        if(cnt[0]<0 || cnt[1]<0) {
            cout<<"NO"<<endl;
            return;
        }
    }

    if(cnt[0]==cnt[1] && cnt[0]==0) {
        cout<<"YES"<<endl;
        cout<<a<<endl;
        cout<<b<<endl;
    } else {
        cout<<"NO"<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
