/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

//#define endl "\n"


int notcol(int c1, int c2) {
    if(c1!=1 && c2!=1)
        return 1;
    else if(c1!=2 && c2!=2)
        return 2;
    else
        return 3;
}

/* Consider chessboard coloring.
 * We want to put one color onto all white, and 
 * another on all black cells.
 * But alice knows that, and the wants to break that strategy.
 * She can force us to not use a certain color.
 *
 * ...then I would better did not stopped thinking :/ 
 * It is kind of trivial.
 * We fill white cells with c1, and black cells with c2.
 * Note that one of c1 or c2 once fills all cells of its color.
 * After that, we can place any color in any remaining field.
 */
void solve() {
    cini(n);

    vvi m(n, vi(n, -1));

    function<void(int,int,int)> set=[&](int c, int i, int j) {
        assert(c>0);
        cout<<c<<" "<<i+1<<" "<<j+1<<endl;
        m[i][j]=c;
    };

    for(int i=0; i<n*n; i++) {
        cini(ca);   /* color of alice */
        bool done=false;
        if(ca==1) {
            for(int j=0; !done && j<n; j++)  {
                for(int k=0; !done && k<n; k++) {
                    if((j+k)&1 && m[j][k]<0) {
                        set(2, j, k);
                        done=true;
                    }
                }
            }
        } else {
            for(int j=0; !done && j<n; j++)  {
                for(int k=0; !done && k<n; k++) {
                    if((j+k)%2==0 && m[j][k]<0) {
                        set(1, j, k);
                        done=true;
                    }
                }
            }
        }

        if(!done) {
            for(int j=0; !done && j<n; j++)  {
                for(int k=0; !done && k<n; k++) {
                    if(m[j][k]<0) {
                        int c2=j>0?m[j-1][k]:m[j+1][k];
                        set(notcol(c2,ca), j, k);
                        done=true;
                    }
                }
            }
        }
    }
}

signed main() {
    solve();
}
