/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cins(s);
    cini(x);
    int h=(s[0]-'0')*10+s[1]-'0';
    int m=(s[3]-'0')*10+s[4]-'0';

    int hh=h;
    int mm=m;
    int ans=0;
    do {
        m+=x;
        h+=m/60;
        m%=60;
        h%=24;

        if(h/10==m%10 && h%10==m/10)
            ans++;
    }while(hh!=h || mm!=m);

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
