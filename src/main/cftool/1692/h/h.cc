/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 *  We want to find the subarray with the most positive
 *  freq on any number.
 *
 *  Note that by choosing r==l we can allways double the money.
 *
 */
void solve() {
    cini(n);
    cinai(x,n);

    int ans=1;
    int ansa=x[0];
    int ansl=0;
    int ansr=0;

    map<int,pii> f; /* <val, <cnt,l>> */
    for(int i=0; i<n; i++) {
        map<int,pii> ff;
        bool done=false;
        for(auto ent : f) {
            if(x[i]==ent.first) {
                ent.second.first++;
                done=true;
                if(ent.second.first>ans) {
                    ans=ent.second.first;
                    ansa=x[i];
                    ansl=ent.second.second;
                    ansr=i;
                }
                ff[ent.first]=ent.second;
            } else {
                ent.second.first--;
                if(ent.second.first>0)
                    ff[ent.first]=ent.second;
            }
        }

        if(!done)
            ff[x[i]]={1,i};

        ff.swap(f);
    }

    cout<<ansa<<" "<<ansl+1<<" "<<ansr+1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
