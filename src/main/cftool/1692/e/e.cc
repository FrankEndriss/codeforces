/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    cini(s);
    cinai(a,n);

    int cnt=accumulate(all(a), 0LL);

    if(cnt<s) {
        cout<<-1<<endl;
        return;
    }

    vi p1(n+1);
    vi p2(n+1);
    for(int i=0; i<n; i++)  {
        p1[i+1]=p1[i]+a[i];
        p2[i+1]=p2[i]+a[n-1-i];
    }

    int ans=1e9;
    for(int i=0; i<=n; i++) {
        int cnt0=p1[i];
        if(cnt-cnt0>s) {
            auto it=lower_bound(all(p2), cnt-cnt0-s);
            ans=min(ans, (int)(i+it-p2.begin()));
        } else 
            ans=min(ans, i);
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
