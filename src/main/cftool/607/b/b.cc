/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Looks like dp.
 * If the both outermost colors are same
 * we can remove them instantly with the last
 * one between them. So we can count them
 * as removed.
 * Or we can split the interval and solve
 * both parts separate.
 *
 * Splits can happen somewhat everywhere.
 * So we have dp[500][500]
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(c,n);

    vvi dp(n, vi(n, INF));/* dp[i][j]=min moves to destruct interval (i,j) */

    for(int i=0; i<n; i++) {
        dp[i][i]=1;
        if(i+1<n) {
            if(c[i]==c[i+1])
                dp[i][i+1]=1;
            else
                dp[i][i+1]=2;
        }
    }

    for(int k=3; k<=n; k++) {
        for(int i=0; i+k-1<n; i++) {
            if(c[i]==c[i+k-1])
                dp[i][i+k-1]=min(dp[i][i+k-1], dp[i+1][i+k-2]);
            for(int l=1; l<k; l++)
                dp[i][i+k-1]=min(dp[i][i+k-1], dp[i][i+l-1]+dp[i+l][i+k-1]); 
        }
    }

    cout<<dp[0][n-1]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
