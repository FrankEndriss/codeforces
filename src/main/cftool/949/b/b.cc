/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each number is moved 0 to k times.
 * The first time i is moved, it is moved by n-1 positions, ie
 * from pos[i] to pos[i]-(1+2*(n-1-i))
 * ...
 * Lets define one 'fold' as the process to mix a continous
 * postfix of len k into the k gaps left of it, in reverse order.
 * If we go from left to right, we can find from which position
 * the number came that is in that position.
 * If the position is odd (1 based index), then it was there from start,
 * else the gaps where filled from right to left by sizes:
 * 1, 3, 7, 15, ..., 2*s[i]+1
 * ...
 * Consider the length of movement, a[] 1-based:
 * a[1]=1
 * a[2]=m(a[n+1])
 * a[3]=2
 * a[4]=m(a[n+2])
 * ...
 * odd n: a[n]=(n+1)/2
 * even n: a[n]=
 * ...to complecated :/
 * *****
 * Editorial: Once seen, the formular is pretty simple:
 * At time the even cell i is filled, there are n-(i/2) numbers to the
 * right of that cell, so the number comes from cell i+(n-i/2)
 */
void solve() {
    cini(n);
    cini(q);

    for(int i=0; i<q; i++) {
        cini(x);
        while(x%2==0)
            x+=n-(x/2);
        cout<<(x+1)/2<<endl;
    }
}

signed main() {
        solve();
}
