/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * foreach day:
 *  dp[k]=min hours after day i if j lessons skipped
 *
 * foreach day consider spending 0..m hours at the
 * university.
 * How to find min number of skipped lessons?
 * -> linear search on prefix sums
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vi dp(k+1);
    for(int i=0; i<n; i++) {
        cins(s);
        vi dp0(k+1, INF);
        vi pre(m+1);
        for(int j=0; j<m; j++)
            pre[j+1]=pre[j]+s[j]-'0';

        int prev=INF;
        for(int h=0; h<=m; h++) {   /* spending h hours at the uni */
            int miss=INF;
            for(int f=0; f+h<=m; f++) {     /* first hour at uni */
                const int seen=pre[f+h]-pre[f];
                miss=min(miss, pre[m]-seen);
            }
            if(miss!=prev) {
                for(int kk=0; kk+miss<=k; kk++)
                    dp0[kk+miss]=min(dp0[kk+miss], dp[kk]+h);
            }
            prev=miss;
        }
        dp.swap(dp0);
    }
    int ans=*min_element(all(dp));
    cout<<ans<<endl;
}

signed main() {
    solve();
}
