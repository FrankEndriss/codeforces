
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    cini(k);

    multiset<int> p;
    vector<pii> m(n);
    for(int i=0; i<n; i++) {
        cin>>m[i].first;
    }
    for(int i=0; i<n; i++) {
        cin>>m[i].second;
        p.insert(m[i].second);
    }
    sort(all(m));

    int r=0;
    int d=0;
    while(k>0 && p.size()) {
        d+=k;
        auto it=lower_bound(all(m), make_pair( d+1, -1LL ));
        int idx=it-m.begin();
        for(int j=r; j<idx; j++)  {
            auto it2=p.find(m[j].second);
            assert(it2!=p.end());
            p.erase(it2);
        }

        r=idx;
        if(p.size())
            k-=*p.begin();
    }

    if(p.size()==0)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
