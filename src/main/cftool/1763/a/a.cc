
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * optimize max:
 * Use one element, and swap all 1 in.
 * optimize min:
 * Use one element, and swap all 0 in
 *
 * Let maximize a[0], minimize a[1]
 */
void solve() {
    cini(n);
    cinai(a,n);

    for(int b=0; b<20; b++) {
        /* max a[0] */
        for(int i=1; (a[0]&(1<<b))==0 && i<n; i++) {
            if(a[i]&(1<<b)) {
                a[0]+=(1<<b);
                a[i]-=(1<<b);
            }
        }

        for(int i=0; (a[1]&(1<<b))>=1 && i<n; i++) {
            if(i==1)
                continue;

            if((a[i]&(1<<b))==0) {
                a[i]+=(1<<b);
                a[1]-=(1<<b);
            }
        }
    }

    cout<<a[0]-a[1]<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
