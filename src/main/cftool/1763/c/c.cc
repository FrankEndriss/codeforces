
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Observe that applied the operation twice to some segment, makes 
 * all number in that segemnt eq 0.
 * Also note that no number can ever increase.
 * So, if n>=4 we can make all number eq max(a[]), see
 * 2 2 5 2
 * 0 0 5 2
 * 5 5 5 2
 * 5 5 0 0
 * 5 5 5 5
 *
 * So, for n==2 and n==3 we need to find max possible ans;
 *
 * And this is talking with typing ....
 * This is only typing...withc ZOOOOOM :)
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans=accumulate(all(a), 0LL);
    if(n==2) {
        ans=max(ans, abs(a[0]-a[1])*2);
    } else if(n==3) {
        /* guess that it is never beneficial to do more than 3 of them */
        vector<pii> ops={ {0,1}, {0,2}, {1,2} };
        function<int(vi,int)> go=[&](vi aa, int d) {
            if(d==5)
                return 0LL;

            int ans=accumulate(all(aa), 0LL);
            for(int i=0; i<3; i++) {
                vi a2=aa;
                int v=max(a2[ops[i].first], a2[ops[i].second]) -
                        min(a2[ops[i].first], a2[ops[i].second]);
                for(int k=ops[i].first; k<=ops[i].second; k++) 
                    a2[k]=v;

                ans=max(ans, go(a2, d+1));
            }
            return ans;
        };

        ans=max(ans, go(a, 0));
    } else {
        ans=n*(*max_element(all(a)));
    }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
