
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atocoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

const int N=1e6;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

/**
 * Two cases:
 * y>x
 * y<x
 *
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cini(i);    /* b[i]==x */
    cini(x);
    cini(j);    /* b[j]==y */
    cini(y);

    mint ans;

    if(y>x) {   /* increase until j or right of it */
        for(int k=j+1; k<n; k++) {
            /* a[j]=y a[k]==n: k>j
             * 1. segment a[0]...a[i]==x, we can use
             *   all numers<x: nCr(x-1, i)
             * 2. segment a[i+1]...a[j]==y 
             *   all numbers k>x && k<y: nCr(y-x-1, (j-1)-(i+1)-1)
             * 3. segment a[j+1]...a[k]==n
             *   all numbers k>y && k<=n: nCr(n-y-1, k-j-1)
             * 4. segement remaining numbers descending
             * */
            ans+=nCr(x-1, i)*nCr(y-x-1, (j-1)-(i+1)-1)*nCr(n-y-1, k-j-1);
        }
    } else {    /* x>y, so increase until x or right of it < j */
        for(int k=i+1; k<j; k++) {
            /* a[k]==n: k>i && k<y
             * 1. segment a[0]...a[i]==x
             *  all number <x: nCr(x-1, i)
             * 2. segemnt a[i+1]...a[k]
             *  all number <n && >x
             * 3. segment: a[k+1]...a[j]
             *  all number... NOT all, only the unused :/
             * */
            ans+=nCr(x-1, i)*nCr(n-x-1, k-i-1)*nCr(...
        }

    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
