/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vbb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const vs digits={ "1110111", "0010010", "1011101", "1011011", "0111010", "1101011", "1101111", "1010010", "1111111", "1111011" };

/* @return number of sticks to use to make number num burning,
 * -1 if impossible */
int canCreate(int num, string &s) {
    int cnt=0;
    for(int i=0; cnt>=0 && i<7; i++) {
        if(s[i]=='1' && digits[num][i]=='0')
            cnt=-1;
        else if(s[i]=='0' && digits[num][i]=='1')
            cnt++;
    }

    return cnt;
}

/* So we need to find the possible number of sticks we can add in every position,
 * and the prefix sum of it from the end.
 * Then we choose the first biggest number we can build and repeat the same
 * in next position.
 */
void solve() {
    cini(n);
    cini(k);
    cinas(s,n); /* the burning sticks */

    vbb posK(n+1, vb(k+1)); /* posK[i][j]==true if there is a solution starting with j sticks at pos i */

    posK[n][0]=true;
    for(int i=n-1; i>=0; i--) {
        vb m(8);
        for(int d=0; d<=9; d++) {
            int cnt=canCreate(d, s[i]);
            if(cnt>=0)
                m[cnt]=true;
        }

        for(int j=0; j<=k; j++) {
            if(!posK[i+1][j])
                continue;

            for(int mm=0; mm<8; mm++) {
                if(m[mm] && j+mm<=k)
                    posK[i][j+mm]=true;
            }
        }
    }

    vi ans(n);
    function<bool(int,int)> build=[&](int idx, int kk) {
        if(idx==n)
            return kk==0;

        if(!posK[idx][kk])
            return false;

        for(int d=9; d>=0; d--) {
            int cnt=canCreate(d, s[idx]);
            
            if(kk>=cnt && cnt>=0 && posK[idx+1][kk-cnt]) {
                if(!build(idx+1, kk-cnt))
                    assert(false);
                ans[idx]=d;
                return true;
            }
        }
        assert(false);
    };

    if(build(0, k)) {
        for(int i : ans)
            cout<<i;
        cout<<endl;    
    } else {
        cout<<-1<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

