/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we need to find all peeks.
 * then find the segment of len k with
 * max number of peaks. (window)
 * How to find peaks? just check O(n)
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vi p(n);
    for(int i=1; i<n-1; i++) {
        if(a[i]>a[i-1] && a[i]>a[i+1])
            p[i]=1;
    }

    /* window of len k-2 since peaks at border to not count as peak */
    queue<int> q;
    int cnt=0;
    int ansT=0;
    int ansL=-1;
    for(int i=0; i<n; i++) {
        cnt+=p[i];
        q.push(p[i]);
        if(q.size()==k-2)  {
            if(cnt>ansT) {
                ansT=cnt;
                ansL=i+1-(k-2);
            }

            int e=q.front();
            q.pop();
            cnt-=e;
        }
    }
    ansL=max(ansL, 1LL);
    cout<<ansT+1<<" "<<ansL<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

