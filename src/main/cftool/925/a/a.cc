/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * There are some ways we can take:
 * if on same floor
 *   direct
 * else choose left or right stairs
 *   use stairs and then walk to dest
 * else choose left or right elevator
 *   use elevator and then walk to dest
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    cini(cntS); /* stair sections */
    cini(cntE); /* elevator sections */
    cini(v);    /* levels per timeunit */

    cinai(S, cntS);
    sort(all(S));
    cinai(E, cntE);
    sort(all(E));

    cini(q);
    for(int i=0; i<q; i++) {
        cini(x1);
        cini(y1);
        cini(x2);
        cini(y2);

        int ans=INF;
        if(x1==x2)
            ans=abs(y1-y2);
        else {
            auto it=lower_bound(all(S), y1);
            if(it!=S.end()) {
                ans=abs(y1-*it);
                ans+=abs(x1-x2);
                ans+=abs(y2-*it);
            } 
            if(it!=S.begin()) {
                it--;
                int lans=abs(y1-*it);
                lans+=abs(x1-x2);
                lans+=abs(y2-*it);
                ans=min(ans, lans);
            }

            it=lower_bound(all(E), y1);
            if(it!=E.end()) {
                int lans=abs(y1-*it);
                lans+=(abs(x1-x2)+v-1)/v;
                lans+=abs(y2-*it);
                ans=min(ans, lans);
            } 
            if(it!=E.begin()) {
                it--;
                int lans=abs(y1-*it);
                lans+=(abs(x1-x2)+v-1)/v;
                lans+=abs(y2-*it);
                ans=min(ans, lans);
            } 
        }
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
