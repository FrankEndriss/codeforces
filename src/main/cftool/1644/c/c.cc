/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to find the max sum of _any_ subarray, 
 * after having added x to k positions.
 *
 * So for k==0 we find the max sum subarray.
 *
 * Consider the len of this subarray, we can add
 * x to all positions.
 *
 * But we need to consider longer subarrays.
 * Also, a subarray extends to left/right, if the 
 * elements to left/right are big enough.
 *
 * Ok, first find all positive subarrays.
 * Then, foreach of them, we can extends to 
 * left or right, use the bigger number.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(x);
    cinai(a,n);

    vi s;
    vi e;
    vi sums;

    int sum=0;
    for(int i=0; i<n; i++) {
            s.push_back(i);
            e.push_back(i+1);
            sums.push_back(a[i]);

        int nsum=sum+a[i];

        if(nsum>=0 && s.size()==e.size()) {
            s.push_back(i);
            sums.push_back(nsum);
        } else if(nsum>=0) {
            sums.back()=nsum;
        } else if(nsum<0 && e.size()<s.size()) {
            e.push_back(i);
        }

        sum=nsum;
    }
    if(e.size()<s.size())
        e.push_back(n);

    assert(s.size()==e.size());
    assert(s.size()==sums.size());

    int gans=-INF;
    for(int k=0; k<=n; k++) {
        int ans=0;
        for(size_t j=0; j<s.size(); j++) {
            int sz=e[j]-s[j];
            if(sz<k) {
                int vals=-INF;
                if(s[j]>0)
                    vals=a[s[j]-1];
                int vale=-INF;
                if(e[j]<n)
                    vale=a[e[j]];

                if(vals>vale) {
                    s[j]--;
                    sums[j]+=a[s[j]];
                } else {
                    sums[j]+=a[e[j]];
                    e[j]++;
                }
            }
            ans=max(ans, sums[j]+x*k);
        }
        gans=max(gans, ans);
        cout<<gans<<" ";
    }
    cout<<endl;
}

/* First find the max sums for all sizes subarrays.
 * Then add sz*k to each subarray, and output the
 * max value sum.
 */
void solve2() {
    cini(n);
    cini(x);

    cinai(a,n);

    vi dp(n+1, -INF); /* dp[i]=max sum of subarray of size i */

    for(int sz=1; sz<=n; sz++) {
        int sum=0;
        int cnt=0;
        for(int i=0; i<n; i++) {
            sum+=a[i];
            cnt++;

            if(cnt>sz) {
                sum-=a[i-sz];
                cnt--;
            }

            if(cnt==sz)
                dp[sz]=max(dp[sz], sum);

        }
    }

    int ans=0;
    for(int k=0; k<=n; k++) {
        for(int j=0; j<=n; j++) 
            ans=max(ans, dp[j]+x*min(k,j));
        cout<<ans<<" ";
    }
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve2();
}
