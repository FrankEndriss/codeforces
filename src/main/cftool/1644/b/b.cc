/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * n, n-1, n-2,..., 1
 * 3,2,1
 * 3,1,2
 * 1,3,2
 *
 * 6,5,4,3,2,1
 * 3,2,1,6
 *
 *
 *
 */
void solve() {
    cini(n);

    vi a(n);
    iota(all(a), 1);
    reverse(all(a));

    int sum=0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) 
            cout<<a[j]<<" ";
        cout<<endl;

        int val=a.back();
        a.pop_back();
        sum+=val;
        a.insert(a.begin(), val);
        if(sum==n) {
            swap(a[i],a[i+1]);
        }
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
