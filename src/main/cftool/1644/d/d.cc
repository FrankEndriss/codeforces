/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider first operation, obviously we can paint
 * n*m positions in k colors.
 * ans[1]=n*m*k
 *
 * Then second op, we can basically do the same, but one of the options
 * does not change the picture. So
 * ans[2]=ans[1]*(ans[1]-1)
 *
 * Third op, again the prev op again does not change the pic.
 * But also, hitting the same spot as first operation does
 * yield a pic like after two operations.
 * ans[3]=ans[2]*(n*m*k-2)
 *
 * So, whenever same position as before is choosen, that one before move is 
 * completly erased, ie does not create new possibilities.
 * ans[x]=ans[k-1]*(n*m-k+1)*k)
 */
using mint=modint998244353;

/* We need to consider "overrides" 
 * of following operations.
 * An override is, if both axes of an
 * operation where overwritten.
 * In this case, that operations are overwritten,
 * and must be made inverse.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    cini(q);

    vi x(q);
    vi y(q);
    for(int i=0; i<q; i++)
        cin>>x[i]>>y[i];

    set<int> xx;
    set<int> yy;
    mint ans=1;
    for(int i=q-1; i>=0; i--) {
        if((xx.count(x[i])==0 && yy.size()<m) || (yy.count(y[i])==0 && xx.size()<n))
            ans*=k;
        xx.insert(x[i]);
        yy.insert(y[i]);
    }

    cout<<ans.val()<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
