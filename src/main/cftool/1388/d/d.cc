/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to start each seq of b[i], b[b[i]], ... at the first
 * position, then go throu all of them.
 * Since that way the a[i] contribute most often.
 *
 * Find the seqs reverse. That is, start at b[j]=-1, and 
 * find the b[i] pointing to b[j] (if there is one).
 * So we preprocess create posb[], where bosb[i]=b[i].
 *
 * What to do with "trees", say b[]={ 1, 2, 5, 4, 5, -1 }?
 * 0 - 1 - 2 - 5
 *     3 - 4  /
 * -> At each crosspoint we need to integrate the childs
 *  by order, biggest first.
 *  So we start at root and dfs, returning the value of the last step.
 *  That is, on every vertex we can sort the childs by value desc.
 *
 *  ...
 *  And there is another difficulty. a[i] can be negative :/
 *  -> In the dfs, we choose the child node before the parent
 *    whenever the childs value is positive.
 *    Else afterwards.
 *
 * So while dfs we build the list of vertex using a dqueue
 * ***
 * Tutorial:
 * Consider vertex with no incoming edge.
 * If such one has posive value, process first, if negative process last.
 * Just implement that simulation and collect first/last in a dqueue
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);
    for(int i=0; i<n; i++) 
        b[i]--;

    //vector<set<int>> adj(n);
    vector<set<int>> adjR(n);
    for(int i=0; i<n; i++) {
        if(b[i]>=0) {
            //adj[i].insert(b[i]);
            adjR[b[i]].insert(i);
        }
    }

    queue<int> q;   /* vertex with no incoming edge */
    for(int i=0; i<n; i++) {
        if(adjR[i].size()==0)
            q.push(i);
    }

    int ans=0;
    deque<int> d1;
    deque<int> d2;

    while(q.size()) {
        int v=q.front();
        q.pop();
        ans+=a[v];
        if(a[v]<=0) {
            d2.push_front(v);
        } else {
            d1.push_back(v);
            if(b[v]>=0)
                a[b[v]]+=a[v];
        }

        if(b[v]>=0) {
            //adj[v].erase(b[v]);
            adjR[b[v]].erase(v);
            if(adjR[b[v]].size()==0)
                q.push(b[v]);
        }
    }

    cout<<ans<<endl;
    for(int i : d1)
        cout<<i+1<<" ";
    for(int i : d2)
        cout<<i+1<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
