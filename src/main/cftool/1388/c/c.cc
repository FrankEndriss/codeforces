/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* dfs and count the people per vertex.
 *
 * Then for each vertex calc number of
 * -max bad mood people that can enter the city, which is same as
 * -min bad mood people that can leave the city.
 *
 * Check if the child vertex can handle that much. (sum)
 *
 * We need to check parities of h[v].
 * If parity possible for given cntP[v]
 * */
void solve() {
    cini(n);
    cini(m);
    cinai(p,n);
    cinai(h,n);

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }


    vi cntP(n);

    /* number of people traveling throu city v */
    function<void(int,int)> cntPeople=[&](int v, int pp) {
        cntP[v]=p[v];
        for(int c : adj[v]) {
            if(c!=pp) {
                cntPeople(c, v);
                cntP[v]+=cntP[c];
            }
        }
        cerr<<"cntPeople, v="<<v<<" cntP[v]="<<cntP[v]<<endl;
    };

    cntPeople(0LL, -1);

    /* parity check */
    for(int i=0; i<n; i++) {
        if((cntP[i]%2) != (abs(h[i])%2)) {
cerr<<" ok=false in parity check, v="<<i<<endl;
cerr<<" cntP[i]="<<cntP[i]<<" h[i]="<<h[i]<<endl;
            cout<<"NO"<<endl;
            return;
        }
    }

    /* max number of bad mood people that can travel throug this city
     * good-bad==h[v]
     * good+bad==cntP[v]
     * good=cntP[v]-bad
     * cntP[v]-bad-bad==h[v]
     * h[v]-cntP[v]==-2*bad
     * bad=(cntP[v]-h[v])/2
     **/
    bool ok=true;
    vi maxB(n);
    function<void(int,int)> cntMaxB=[&](int v, int pp) {
        int mid=cntP[v]/2;
        if(cntP[v]%2==0) {
            int good=mid+(h[v]/2);
            maxB[v]=cntP[v]-good;
        } else {
            int good=mid+((h[v]+1)/2);
            maxB[v]=cntP[v]-good;
        }
        for(int c : adj[v])
            if(c!=pp)
                cntMaxB(c,v);

        if(maxB[v]<0) {
            cerr<<"maxB[v]="<<maxB[v]<<" at v="<<v<<endl;
            ok=false;
        }
    };

    function<void(int,int)> dfs=[&](int v, int pp) {
        int num=maxB[v];
        for(int c : adj[v]) {
            if(c!=pp) {
                num-=maxB[c];
                dfs(c,v);
            }
        }
        if(num>0) {
cerr<<" ok=false in dfs, v="<<v<<endl;

            ok=false;
        }
    };

    cntMaxB(0LL,-1LL);
    dfs(0LL, -1LL);

    if(ok)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
