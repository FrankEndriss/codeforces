/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* n digits
 *
 * Note that 9 and 8 have more bin digits than 7.
 * So it is
 * 999... + last digit
 * which results in bin number of len n*3;
 *
 * 2 -> 98 -> 10011000 -> 100110
 * 3 -> 998 -> 100110011000 -> 100110011
 * 4 -> 9999 -> 1001100110011001 -> 100110011001
 *
 * What a fucking shit...edgecases :/
 */
void solve() {
    cini(n);

    for(int i=0; i+1<n; i++)
        cout<<'9';

    if(n%4==0)
        cout<<'9';
    else
        cout<<'8';

    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
