/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

bool isnp(int p) {
    for(int i=0; pr[i]*pr[i]<p; i++) {
        if(p%pr[i]==0 && !notpr[p/pr[i]])
            return true;
    }
    return false;
}

const int N=1e5+7;
vi np;
/* Assume we have all nearly primes up to 1e5
 *
 * Then use the 3 smallest ones, and the diff to the rest.
 */
void solve() {
    cini(n);

    for(int i1=0; i1<np.size(); i1++) {
        if(np[i1]*3>=n) {
                    cout<<"NO"<<endl;
                    return;
        }
        for(int i2=i1+1; i2<np.size(); i2++)  {

            for(int i3=i2+1; i3<np.size(); i3++) {
                int s=np[i1]+np[i2]+np[i3];
                if(s>=n) {
                    break;
                }
                int f=n-s;
                if(f!=np[i1] && f!=np[i2] && f!=np[i3]) {
                    cout<<"YES"<<endl;
                    cout<<np[i1]<<" "<<np[i2]<<" "<<np[i3]<<" "<<f<<endl;
                    return;
                }
            }
        }
    }

    assert(false);
}

signed main() {
    cini(t);
    for(int i=1; i<N; i++) 
        if(isnp(i))
            np.push_back(i);

    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
