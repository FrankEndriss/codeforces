/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* It is about 2s and 5s.
 * Since there are a lot of 2s in the numbers,
 * it is just about 5s.
 *
 * 5 adds one zero.
 * 10 adds one zero.
 * 15 adds one zero.
 * 20 adds one zero.
 * 25 adds two zeros!
 * and 30 adds one zero...
 * 
 * So every fife multiples of 5 add 6 zeros.
 **/
void solve() {
    cini(m);    // number of trailing zeros

    int first=0;
    int cnt=0;
    int mod=0;

    while(true) {
        if(cnt==m) {
            cout<<5<<endl;
            for(int i=0; i<5; i++)
                cout<<first+i<<" ";
            cout<<endl;
            return;
        } else if(cnt>m) {
            cout<<0<<endl;
            return;
        } else {
            first+=5;
            int x=first;
            while(x%5==0) {
                cnt++;
                x/=5;
            }
        }
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

