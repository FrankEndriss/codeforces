
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* First compress the input string.
 * Then dp.
 * With cost 1 we can remove first or 
 * first and the next equal char.
 */
void solve() {
    cini(n);
    cins(s);
    string ss;
    char prev=' ';
    for(char c : s) {
        if(c!=prev)
            ss+=c;
        prev=c;
    }

    vvi dp(ss.size(), vi(ss.size(), -1));   /* dp[i][j] min cost to remove [i,j] */
    function<int(int,int)> go=[&](int l, int r) {
        if(l>r)
            return 0LL;

        if(l==r)
            return 1LL;

        if(l+1==r) {
            if(ss[l]==ss[r])
                return 1LL;
            else
                return 2LL;
        }

        if(dp[l][r]>=0)
            return dp[l][r];

        int ans=1LL+go(l+1, r);

        for(int j=l+1; j<=r; j++)
            if(ss[l]==ss[j]) 
                ans=min(ans, go(l+1,j-1)+go(j,r));

        return dp[l][r]=ans;
    };

    cout<<go(0, ss.size()-1)<<endl;
}

signed main() {
    solve();
}
