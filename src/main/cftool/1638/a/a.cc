/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Start the permutation on the first element
 * not in sorted position,
 * and end it on the smallest element of all remaining.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(p,n);

    vi a=p;
    sort(all(a));
    int l=-1;
    for(int i=0; i<n; i++) 
        if(a[i]!=p[i]) {
            l=i;
            break;
        }

    if(l>=0) {
        int r=-1;
        int val=INF;
        for(int i=l+1; i<n; i++) {
            if(val>p[i]) {
                val=p[i];
                r=i;
            }
        }
        reverse(p.begin()+l, p.begin()+r+1);
    }

    for(int i=0; i<n; i++) 
        cout<<p[i]<<" ";
    cout<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
