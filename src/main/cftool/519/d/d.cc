/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find (i,j) where
 * a[i]==a[j]
 * pre[j]-pre[i]==0
 *
 * Go from left to right, maintain freq[{a[i],pre[j]}]
 * Be careful for off by one.
 */
void solve() {
    cinai(a,26);
    cins(s);

    vi pre(s.size());
    pre[0]=a[s[0]-'a'];
    for(size_t i=1; i<s.size(); i++)
        pre[i]=pre[i-1]+a[s[i]-'a'];

    map<pair<char,int>,int> f;
    int ans=0;
    for(size_t i=0; i<s.size(); i++) {
        if(i>0)
            ans+=f[make_pair(s[i], pre[i-1])];

        f[make_pair(s[i], pre[i])]++;
    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
