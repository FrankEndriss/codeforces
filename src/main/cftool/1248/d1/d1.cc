/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* How to find number of shifts of a seq
 * to get a correct bracket seq?
 * If we got one shift with a correct bracket seq (CBS),
 * then is is the number of CBS prefixes
 * since we can allways shift a whole CBS.
 *
 * Let bal(i)==balance of open/close at pos i. Then
 * a CBS of len n has positive bal(i) for all i,
 * and bal(n)==0.
 *
 * Since all CBS start with open bracket, lets start search at
 * first open bracket.
 * Then, if adding next position, bal stays pos or becomes
 * negative. If it becomes negative, we extends search to the left, until
 * it becomes positive again. Repeat.
 * After left and rigth meet, left points to begin of CBS.
 *
 * Should be enough for D1, since we can do that search
 * for all possible swaps. O(n^3) at max(n)==500.
 *
 *
 * PS: countShifts() can also be implemented counting "the number of minimums"
 * in bal, which might run faster but still O(n).
 */
int countShifts(const string &s) {
    const int n=s.size();
#define inc(i) (i=(i+1)%n)
#define dec(i) (i=(n+i-1)%n)
    int l=0;
    while(s[l]!='(')
        l++;

    int r=l;
    inc(r);
    int bal=1;
    while(l!=r) {
        if(s[r]=='(')
            bal++;
        else
            bal--;
        inc(r);
        while(bal<0 && l!=r) {
            l=dec(l);
            if(s[l]=='(')
                bal++;
            else
                bal--;
        }
    }
    if(bal!=0)
        return 0;

    int ans=0;
    bal=0;
    for(int i=0; i<n; i++) {
        if(s[(i+l)%n]=='(')
            bal++;
        else
            bal--;
        assert(bal>=0);
        if(bal==0)
            ans++;
    }
    return ans;
}

void solve() {
    cini(n);
    cins(s);
    int bal=0;
    for(int i=0; i<n; i++)
        bal+=(s[i]=='('?1:-1);
    if(bal!=0) {
        cout<<"0\n1 1"<<endl;
        return;
    }

    int ans=countShifts(s);
    int ansI=0;
    int ansJ=0;
    for(int i=0; i<n-1; i++) {
        for(int j=i+1; j<n; j++) {
            if(s[i]!=s[j]) {
            swap(s[i], s[j]);
            int cnt=countShifts(s);
            if(cnt>ans) {
                ans=cnt;
                ansI=i;
                ansJ=j;
            }
            swap(s[i], s[j]);
            }
        }
    }
    cout<<ans<<endl;
    cout<<ansI+1<<" "<<ansJ+1<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

