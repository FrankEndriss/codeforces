/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Note that the numbers form a permutation.
 * So we can create a list of size n and mark
 * every position whith 1 or 2 or 3
 * Then we need to find number of moves nessecary
 * to make the list ordered.
 * That number of moves is simply n - length
 * of the longest non decreasing subsequence.
 */
void solve() {
    cinai(k, 3);

    vi a(k[0]+k[1]+k[2]);
    for(int i=0; i<3; i++) 
        for(int j=0; j<k[i]; j++) {
            cini(aux);
            aux--;
            a[aux]=i;
        }

    vi dp(3);   /* dp[i]=longest subseq ending in i */
    for(size_t i=0; i<a.size(); i++) {
        if(a[i]==0)
            dp[0]++;
        else if(a[i]==1)
            dp[1]=max(dp[1]+1, dp[0]+1);
        else if(a[i]==2) 
            dp[2]=max(max(dp[2]+1, dp[1]+1), dp[0]+1);
    }
    int ans=*max_element(all(dp));
    cout<<a.size()-ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
