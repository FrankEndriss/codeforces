/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Some dp.
 *
 * dp[i]=number of ways to create b[] up to index i.
 *
 * Since the elements are distinct, it is fixed which
 * elements of a[] are moved to b[]
 * But we may choose the left t[i] or right t[j]
 * We cannot choose if left or right is a fixed a[i], too.
 *
 * Since we must create b[] from left to right we are stuck
 * if we "catch" an a[i] between to elements later in b.
 * How to detect?
 *
 * Note that fixed elements get "unfixed" after having them moved to b[].
 * How to simulate?
 *
 * For each of the k elements we have max two choices. So we cannot list states.
 * But, if once there is an free element between two fixed elements, that
 * wont go away, since when it does, one of the fixes becomes free.
 * So we got foreach fixed element 0, 1 or 2 possibilities.
 */
constexpr int MOD=998244353;
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
    cinai(b,k);

    map<int,int> posa;
    for(int i=0; i<n; i++)
        posa[a[i]]=i;

    map<int,int> posb;
    for(int i=0; i<k; i++) 
        posb[b[i]]=i;

    int ans=1;
    for(int i=0; i<k; i++) {
        auto it=posa.find(b[i]);
        assert(it!=posa.end());

        int cnt=0;
        if(it->second>0) {  /* there is a left element */
            int idx=it->second-1;
            auto itL=posb.find(a[idx]);
            if(itL==posb.end() || itL->second<i)
                cnt++;
        }
        if(it->second+1<n) {  /* there is a right element */
            int idx=it->second+1;
            auto itR=posb.find(a[idx]);
            if(itR==posb.end() || itR->second<i)
                cnt++;
        }
        ans*=cnt;
        ans%=MOD;
    }

    cout<<ans<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
