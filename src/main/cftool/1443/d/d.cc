/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We must have at most one min extreme somewhere,
 * else it is not possible.
 * ...No.
 * ***
 * First we need to minimize center elements.
 * So check foreach center element if that 
 * works out.
 * The smallest value a center element can be is
 * a[center]-min(a[left]) 
 * a[center]-min(a[right]) 
 * So, foreach element there must be smallest elements left and right
 * of it beeing at least same size.
 * No.
 * ***
 * We can decrement from left by the min element from a[0]..a[i].
 * This results in all those elements beeing a[i]-min(a[0..i])
 * Then we can decrement from right, but only a non-increasing
 * sequence.
 * So we need to find from right the value each a[i] must be
 * decreased, then find from left if we can decrease to that
 * value.
 *
 * ...No, still WA :/
 * ***
 * Simpler. if we got two local maximums we do not get rid of them.
 *
 * ;) Both. two local maxima, and the sum of the left and right min values.
 * ***
 *
 * No...what else?
 *
 * If there would be a counterexample. Hence
 * Having two local maximas and can be removed. Or
 * haveing one maxima with left+right min values smaller than a[i]
 *
 * Or having only one maxima and left+right>=a[i], but still not
 * working for some reason.
 *
 * ...idk :/
 * Some stupid corner case, like n==1?
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi aa;
    aa.push_back(a[0]);
    for(int i=1; i<n; i++) 
        if(a[i]!=a[i-1])
            aa.push_back(a[i]);

    /* number of local maximas */
    int cnt=0;
    for(int i=1; i+1<aa.size(); i++) 
        if(aa[i-1]<aa[i] && aa[i]>aa[i+1])
            cnt++;

    vi le(n);
    int miL=1e9;
    vi ri(n);
    int miR=1e9;
    for(int i=0; i<n; i++)  {
        le[i]=miL;
        miL=min(miL, a[i]);

        ri[n-1-i]=miR;
        miR=min(miR, a[n-1-i]);
    }

    bool ok=true;
    for(int i=0; i<n; i++)  {
        if(ri[i]+le[i]<a[i])
            ok=false;
    }

    if(cnt<2 && ok)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
