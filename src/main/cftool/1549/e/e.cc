/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* The first time an attack can be done is
 * at t=(x+2)/3, since then there are at least x pigs.
 *
 * And there are nCr(x, t*3) plans.
 * At time t+1 there are plans involving one of the 3 new pics
 * and all other pics, so 
 * nCr(x-1,(t+1)*3-3) with the first of the three pics,
 * nCr(x-1,(t+1)*3-2) with the second of the three pics,
 * nCr(x-1,(t+1)*3-1) with the third of the three pics.
 *
 * At time t+2 ... same.
 * Until t+i==n.
 *
 * Unfortunatly that is (at least) O(n^2)
 * How to optimize?
 * Most likely we can somehow summarize the nCr :/
 */
void solve() {
}

signed main() {
    solve();
}
