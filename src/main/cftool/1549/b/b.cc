/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to find the max cols.
 * So go from left to right, consider the last two cols, and 
 * wich cols they cover.
 * But how?
 *
 * We try to move our pawn i to i-1, if not possible to i, 
 * if not possible to i+1
 */
void solve() {
    cini(n);
    cins(t);
    t="0"+t+"0";
    cins(s);
    s="0"+s+"0";


    /* dp[i]=there is a pawn moved to col i-1 */
    vb dp(n+2);

    for(int i=1; i<=n; i++) {
        if(s[i]=='1' && !dp[i-1] && t[i-1]=='1') /* move left */
            dp[i-1]=true;
        else if(s[i]=='1' && !dp[i] && t[i]=='0')  /* move streight */
            dp[i]=true;
        else if(s[i]=='1' && !dp[i+1] && t[i+1]=='1') /* move right */
            dp[i+1]=true;
    }

    int ans=0;
    for(int i=0; i<n+2; i++)
        if(dp[i])
            ans++;

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
