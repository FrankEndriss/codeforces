/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, set<ll> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization.insert(d);
            n /= d;
        }
    }
    if (n > 1)
        factorization.insert(n);
}

/*
 * List all primefactors and find longest seq of...what?
 *
 * There is surely some wiered property.
 *
 * Consider the first two values.
 * They have common primefactors that can be used as m, 
 * or m is a factor of the difference abs(a[i]-a[j]), 
 * but bigger than min(a[i],a[j])
 * ...somehow :/
 *
 * We can use all factors of the diff of two numbers.
 *
 * m=abs(a[i]-a[j])
 *
 * But we cannot check all factors, that are to much.
 * But it should be ok to check only the primefactors.
 *
 * What if the diff==0?
 * Not possible, a[] is distinct.
 *
 * ....
 * BUT we cannot factorize all a[i], since a[i]<=1e18 :/
 * Is this some application of extended gcd()?
 * sic :/
 *
 * Can we use the gcd of the diffs?
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans=2;
    map<int,int> m;

    for(int i=1; i<n; i++) {
        map<int,int> m0;

        int dd=abs(a[i]-a[i-1]);
        set<int> d;
        trial_division4(dd, d);

        for(int j : d) {
            auto it=m.find(j);
            if(it==m.end())
                m0[j]=2;
            else {
                m0[j]=it->second+1;
                ans=max(ans, it->second+1);
            }
        }
        m.swap(m0);
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
