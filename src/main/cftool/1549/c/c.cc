/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * After each query we have a forrest, and ans=number of leafs
 *
 * Edges allways go from lower to higher power.
 * So count the number of vertex without outgoing edges.
 */
void solve() {
    cini(n);
    cini(m);

    vector<set<int>> out(n+1);   /* out[i]=outgoing edges of vertex i */
    int ans=n;
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);

        if(out[min(u,v)].size()==0)
            ans--;

        out[min(u,v)].insert(max(u,v));
    }

    cini(q);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(u);
            cini(v);
            if(u>v)
                swap(u,v);
            if(out[u].size()==0)
                ans--;
            out[u].insert(v);
        } else if(t==2) {
            cini(u);
            cini(v);
            if(u>v)
                swap(u,v);
            out[u].erase(v);
            if(out[u].size()==0)
                ans++;
        } else {
            cout<<ans<<endl;
        }
    }

}

signed main() {
    solve();
}
