/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* the change of the first two elements
 * determine the whole sequence.
 * Try all 9 possibilities .
 */
void solve() {
    cini(n);
    cinai(b,n);

    if(n<=2) {
        cout<<0<<endl;
        return;
    }

    const int INF=1e9;
    int ans=INF;

    for(int i=-1; i<=1; i++) {
        for(int j=-1; j<=1; j++) {
            int lans=0;
            int diff=(b[1]+j) - (b[0]+i);
            if(i!=0)
                lans++;
            if(j!=0)
                lans++;

            int prev=b[1]+j;
            for(int k=2; k<n; k++) {
                if(prev+diff==b[k]-1) {
                    lans++;
                } else if(prev+diff==b[k]+1) {
                    lans++;
                } else if(prev+diff==b[k]) {
                    ;
                } else {
                    lans=INF;
                    break;
                }
                prev+=diff;
            }
            ans=min(ans, lans);
        }
    }

    if(ans==INF)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

