/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we need to find the 
 * components in the grid.
 * then every component must not have
 * white cells between black cells.
 * If no between white cells exist,
 * num of components is ans.
 *
 * How to find components?
 * two black cells in same row connect all cols in between.
 * two black cells in same col connect all rows in between.
 * Every row and col can belong to only one component.
 *
 * How to find in between cells?
 * Simply check all white cells of a component.
 *
 * Note that there might be zero components.
 *
 * A component is defined by first row, number of rows,
 * first col and number of cols.
 *
 */
void solve() {
    cini(r);
    cini(c);
    cinas(a,r);

    vector<pii> cbegin;
    vector<pii> csize;

    for(int i=0; i<r; i++) {
        int firstc=-1;
        int lastc=-1;
        for(int j=0; j<c; j++) {
            if(a[i][j]=='#') {
                firstc=j;
                break;
            }
        }
        for(int j=0; j<c; j++) {
            if(a[i][j]=='#') {
                lastc=j;
            }
        }
        if(firstc==-1)  // row belongs to no component
            continue;

        cbegin.push_back({i,firstc});
        csize.push_back({1,lastc-firstc+1});
        for(int lr=r-1; lr>=i; lr--) {
            for(int j=firstc; j<=lastc; j++) {
                if(a[lr][j]=='#') {
                    csize.back().frist=lr-i+1;
                    i=lr;
                    break;
                }
            }
        }
    }

    /* now check components for white cells in between.
 * find for every row and col first and last black,
 * check all fields in between for color. */
    for(int idx=0; idx<cbegin.size(); idx++) {  // all components
        for(int i=cbegin[idx].first; i<cbegin[idx].first+csize[idx].first; i++) {
            // ... netsted loops, no fun..
        }
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS, SOLVE ONE AFTER THE OTHER
