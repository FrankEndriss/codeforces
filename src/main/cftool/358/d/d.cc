/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* need to find opt permutation of indexes.
 * dp... how?
 *
 * hare a and n are a or b, never c
 *
 * Calc cost of each hare of feeding it late.
 */
void solve() {
    cini(n);
    cinai(a,n); // zero full
    cinai(b,n); // one full 
    cinai(c,n); // both full

    vvi dp(n, vi(2, -1));
/* @return max value starting at i if left of i is full or not full */
    function<int(int,int)> calc=[&](int i, int leftFull) {
        if(i==n-1) {
            if(leftFull)
                return b[i];
            else
                return a[i];
        }

        if(dp[i][leftFull]>=0)
            return dp[i][leftFull];

        if(leftFull)
            return dp[i][leftFull]=max(b[i]+calc(i+1, 1), c[i]+calc(i+1, 0)); 
        else
            return dp[i][leftFull]=max(a[i]+calc(i+1, 1), b[i]+calc(i+1, 0)); 
    };

    int ans=calc(0, 0);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
