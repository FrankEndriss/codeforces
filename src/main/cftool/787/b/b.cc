/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* if rick and morty from one universe are in one group,
 * that group has at least one loyal member.
 * Else that group could be of traters only!
 */
void solve() {
    cini(n);    // number of universes
    cini(m);    // number of groups

    bool gok=true;
    for(int i=0; i<m; i++) {    /* check each group */
        cini(k);
        vi g(n+1);      
        bool ok=false;
        for(int j=0; j<k; j++) {
            cini(a);
            if(a>0)  {
                g[a]|=1;
                if(g[a]==3)
                    ok=true;
            } else {
                g[-a]|=2;
                if(g[-a]==3)
                    ok=true;
            }
        }
        if(!ok)
            gok=false;
    }

    if(gok)
        cout<<"NO"<<endl;
    else
        cout<<"YES"<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

