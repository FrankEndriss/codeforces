
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* five letters
 *
 * consider each one the most freq letter,
 * then choose best solution.
 *
 * The best solution is that we choose
 * the words with the best ratio first, then 
 * add words as much as possible.
 *
 * Sorting seems complecated :/
 * How to simply consider 0 values in nominator and denominator?
 */
void solve() {
    cini(n);
    cinas(s,n);



    int ans=0;
    for(char c='a'; c<='e'; c++) {

        vector<int> f(n);
        for(int i=0; i<n; i++) {
            for(char cc : s[i]) {
                if(cc==c)
                    f[i]++;
                else
                    f[i]--;
            }
        }

        /* compare fractions,
         * 3 cases;
         * 0, x, INF */
        sort(all(f), greater<int>());

        int cnt=0;
        int cnt2=0;
        int lans=0;
        for(int i=0; i<n; i++) {
            cnt+=f[i];
            if(cnt>0)
                lans++;
            else
                break;
        }
        ans=max(ans, lans);
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
