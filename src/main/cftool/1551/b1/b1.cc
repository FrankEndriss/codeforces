
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Let x=number of unique letters.
 * We can paint x/2 red and x/2 green.
 */
void solve() {
    cins(s);

    vi f(26);
    for(int i=0; i<s.size(); i++) 
        f[s[i]-'a']++;

    int cnt1=0;
    for(int i=0; i<26; i++) 
        if(f[i]>1)
            cnt1++;

    int ans=cnt1;
    int cnt2=0;
    for(int i=0; i<26; i++) 
        if(f[i]==1)
            cnt2++;

    ans+=cnt2/2;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
     solve();
}
