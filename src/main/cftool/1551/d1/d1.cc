
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* k horizontal
 * m cols
 *
 * We need to place the k horz so that below
 * them there is an even number of rows, where we can place the vert ones.
 *
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vector<string> ans(n, string(m, ' '));

    function<char(int,int)> letter=[&](int ii, int jj) {
        assert(ans[ii][jj]==' ');
        for(char cc='a'; cc<='z'; cc++) {
            if(ii>0) {
                if(jj>0 && ans[ii-1][jj-1]==cc)
                    continue;
                if(ans[ii-1][jj]==cc)
                    continue;
                if(jj+1<m && ans[ii-1][jj+1]==cc)
                    continue;

            }
            if(jj>0 && ans[ii][jj-1]==cc)
                continue;

            if(ii+1<n && jj>0) {
                if(ans[ii+1][jj-1]==cc)
                    continue;
            }

            return cc;
        }
        assert(false);
        return ' ';
    };

    int start=0;
    if(n&1) {   /* m is even, place one row of horz */
        start=1;
        for(int j=0; k && j<m; j+=2) {
            char c=letter(0, j);
            ans[0][j]=c;
            ans[0][j+1]=c;
            k--;
        }
    }

    if(k&1) {
        cout<<"NO"<<endl;
        return;
    }

    /* now even rows, so allways place two rows */
    for(int i=start; i<n; i+=2) {
        for(int j=0; k && j+1<m; j+=2) {
            char c=letter(i,j);
            ans[i][j]=c;
            ans[i][j+1]=c;
            c=letter(i+1,j);
            ans[i+1][j]=c;
            ans[i+1][j+1]=c;
            k-=2;
        }
    }
    if(k>0) {
        cout<<"NO"<<endl;
        return;
    }

    /* place the verts */
    for(int i=0; i+1<n; i++) {
        for(int j=0; j<m; j++) {
            if(ans[i][j]!=' ')
                continue;

            char c=letter(i,j);
            ans[i][j]=c;
            ans[i+1][j]=c;
        }
    }

    bool ok=true;
    for(int i=0; i<n; i++) 
        for(int j=0; j<m; j++) 
            if(ans[i][j]==' ')
                ok=false;

    if(!ok)
        cout<<"NO"<<endl;
    else {
        cout<<"YES"<<endl;
    }
        

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
