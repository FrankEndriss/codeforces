
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider a set of cells to paint.
 * They 
 * 3 of same number and 3 colors
 * 3 pairs of same number and 3 colors
 * 3 single numbers not used anywhere
 *
 * How to implement?
 * sic, its to hard :/
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vvi f(n);
    for(int i=0; i<n; i++) 
        f[a[i]-1].push_back(i);

    vi ans(n);
    for(int i=0; i<n; i++) {
        if(f[i].size()>=3) {
            int cnt=f[i].size()/k;
            for(int j=0; j<cnt; j++) {
                for(int kk=0; kk<k; kk++)
                    ans[f[i][j*k+kk]]=kk+1;
            }
        }
    }
    vi tri;
    for(int i=0; i<n; i++) {
        if(f[i].size()==1)
            tri.push_back(f[i][0]);
        if(tri.size()==k) {
            for(int kk=0; kk<k; kk++) {
                ans[tri[kk]]=kk+1;
            }
        }
    }

    /* todo handle case 3 pairs... */



    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
     solve();
}
