/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider one '0' anywhere.
 * Obviosly we can choose two 1s and the zero, to make 3 zeros.
 * If that does not work out (because one 1 left), we can 
 * add one operation and make four 1s out of the 3 last, then
 * resolve two by two.
 *
 * Any other edgecases?
 * 1001 -> Any move will make it 1111
 * 1100 -> Solveable
 *
 * 10001
 * 11101
 * 11000
 * 00000
 *
 * Invariant?
 * We allways remove 2 ones, or add 2 ones.
 *
 * Then, 
 * Consider a[0]==0
 *   go from left to right, find the first 1, operate there. That makes it
 *   0, or three ones. If three then step 1 pos back, repeat.
 *
 * Consider a[0]==1
 *   We have to remove the prefix of 1s. If even len, just remove them two
 *   by two. If odd then add two 1s until the next 1 is reached.
 *
 * If both ends have odd number of 1s no sol.
 *
 * Complecated cases, no fun :/
 * Is there a simpler construction?
 * -> see editorial: no, its 2500
 *
 * Consider
 * 100111, ans=NO
 */
void solve() {
    cini(n);
    cinai(a,n);
    int cnt=accumulate(all(a));
    if(cnt%2==1 || cnt==n) {
        cout<<"NO"<<endl;
        return;
    }

    if(cnt==0) {
        cout<<"YES"<<endl<<"0"<<endl;
        return
    }
    if(n==3) {
        cout<<"YES"<<endl<<"1 1"<<endl;
        return;
    }

    if(n==4) {  /* if there are two 1s next to each other then yes, else no. */
        // TODO
        return;
    }



}

signed main() {
    cini(t);
    while(t--)
        solve();
}
