/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Lets count the horizontal and vertical steps.
 */
void solve() {
    cini(n);

    int ans=0;
    for(int i=-(n/2+1); i<=(n/2+1); i++) 
        for(int j=-(n/2+1); j<=(n/2+1); j++) {
            int ii=i+1000;
            int jj=j+1000;
            if(n%2==0) { /* same x and y */
                int x=n/2;
                if(ii%2==x%2 && jj%2==x%2)
                    ans++;
            } else {
                int x=n/2+1;
                int y=x-1;
                if((ii%2==x%2 && jj%2==y%2) || (ii%2==y%2 && jj%2==x%2))
                    ans++;
            }
        }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
