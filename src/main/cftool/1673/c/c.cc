
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Construct all palindromes up to len 5, then do knapsack,
 * then answer all queries.
 *
 * ...remember how to knapsack with multiple occ of values?
 */
using mint=modint1000000007;
const int N=4e4+7;
void solve() {

    vector<mint> dp(N);
    dp[0]=1;

    for(int i=1; i<=9; i++) {
        int val=i;
        for(int k=0; k+val<N; k++)
            dp[k+val]+=dp[k];

        val=i*10+i;
        for(int k=0; k+val<N; k++)
            dp[k+val]+=dp[k];

        for(int j=0; j<=9; j++) {
            val=i*100+j*10+i;
            for(int k=0; k+val<N; k++)
                dp[k+val]+=dp[k];

            val=i*1000+j*100+j*10+i;
            for(int k=0; k+val<N; k++)
                dp[k+val]+=dp[k];

            for(int j2=0; j2<=9; j2++) {
                val=i*10000+j*1000+j2*100+j*10+i;
                for(int k=0; k+val<N; k++)
                    dp[k+val]+=dp[k];
            }
        }
    }

    cini(t);
    for(int i=0; i<t; i++) {
        cini(n);
        cout<<dp[n].val()<<endl;
    }

}

signed main() {
    solve();
}
