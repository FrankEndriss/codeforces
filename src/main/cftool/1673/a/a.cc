
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Stupid long statement, I dont like.
 */
void solve() {
    cins(s);

    int sum=0;
    for(char c : s)
        sum+=c-'a'+1;

    int al=0;
    int bo=0;
    if(s.size()%2==0)
        al=sum;
    else {
        al=max(sum-(s[0]-'a'+1), sum-(s.back()-'a'+1));
        bo=sum-al;
    }
    if(al>bo)
        cout<<"Alice "<<al-bo<<endl;
    else
        cout<<"Bob "<<bo-al<<endl;
        

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
