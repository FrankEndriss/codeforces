/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each rule says
 * i==j or i!=j
 *
 * for i!=j create direct connect, else create connect using 
 * an intermediate vertex.
 * Then two-color the graph, check if impossible.
 * Finally count the colored orig vertex, bigger number is ans.
 */
void solve() {
    cini(n);
    cini(m);

    vvi adj(n);

    for(int k=0; k<m; k++) {
        cini(i); i--;
        cini(j); j--;
        cins(s);
        if(s=="crewmate") {
            vi inter;
            adj.push_back(inter);
            adj[i].push_back(ssize(adj)-1);
            adj.back().push_back(i);
            adj.back().push_back(j);
            adj[j].push_back(ssize(adj)-1);
        } else {
            adj[i].push_back(j);
            adj[j].push_back(i);
        }
    }

    /* color of vertex */
    vi c(adj.size(), -1);

    bool ok=true;
    vi cnt(2);
    function<void(int,int)> dfs=[&](int v, int p) {
        for(int chl : adj[v]) {
            if(chl==p)
                continue;
            if(c[chl]>=0) { 
                if(c[chl]==c[v])
                    ok=false;
            } else {
                c[chl]=c[v]^1;
                if(chl<n)
                    cnt[c[chl]]++;
                dfs(chl, v);
            }
        }
    };

    int ans=0;
    for(int i=0; ok && i<n; i++) {
        if(c[i]>=0)
            continue;

        c[i]=0;
        cnt[0]=1;
        cnt[1]=0;
        dfs(i, -1);
        ans+=max(cnt[0],cnt[1]);
    }

    if(!ok)
        cout<<-1<<endl;
    else {
        cout<<ans<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
