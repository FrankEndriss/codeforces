/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We have a number of positions, ie numbers a[], and
 * search some x[] so that each number in a is at least
 * not div by one x[k]
 * So we choose x[0]=n-1, and x[1]=n
 *
 * if a[] has only one element, x[0]=a[0]-1,
 * else
 * x[0]=n
 * x[1]=n-1
 */
void solve() {
    cini(n);
    char c;
    cin>>c;
    cins(s);

    vi a;
    for(int i=0; i<ssize(s); i++) {
        if(s[i]!=c)
            a.push_back(i+1);
    }

    if(a.size()==0) {
        cout<<0<<endl;
    } else if(a.size()==1) {
        cout<<1<<endl;
        if(a[0]<3)
            cout<<3<<endl;
        else
            cout<<a[0]-1<<endl;
    } else if(a.size()>1) {
        /* check all numbers > n/2 if we can use it for single operation */
        for(int i=a.back()/2+1; i<=n; i++)  {
            auto it=lower_bound(all(a), i);
            if(it==a.end() || *it!=i) {
                cout<<1<<endl;
                cout<<i<<endl;
                return;
            }
        }
        cout<<2<<endl;
        cout<<n<<" "<<n-1<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
