/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int col(string &s) {
    if(s=="white")
        return 0;
    else if(s=="yellow")
        return 1;
    else if(s=="green")
        return 2;
    else if(s=="blue")
        return 3;
    else if(s=="red")
        return 4;
    else if(s=="orange")
        return 5;
}

/**
 * Each child can be one of 4 colors the parent allows.
 * So, ans=6*4^(n-1)
 *
 * E2:
 * Other than e1 the colors of a node are not only limited by the parent,
 * but also by the children, and mayby given.
 * Also we are not able to iterate the whole tree :/
 */
using mint=modint1000000007;
void solve() {
    cini(k);

    // TODO

    int cnt=1;
    int num=1;
    k--;
    while(k) {
        num*=2;
        cnt+=num;
        k--;
    }

    mint ans=4;
    ans=ans.pow(cnt-1);
    ans*=6;
    cout<<ans.val()<<endl;


}

signed main() {
    solve();
}
