/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that it does not make a diff 
 * where the segment is.
 * Consider k animals in some pens.
 * Then the other s-k animals must be in the other pens.
 *
 * So, there are 1..k k-pens,
 * and 
 * if(s==k)
 * 0 other pens,
 * or if(s>k)
 *  1..s-k other pens
 */
void solve() {
    cini(s);
    cini(n);
    cini(k);

    // that is ans for question, is there a lucky distribution?
    // unfortunatly it gives green on the testcases, shame on you problemsetters :/
    if(k>s)
        cout<<"NO"<<endl;
    else if(k==s) {
        if(k<n)
            cout<<"NO"<<endl;
        else
            cout<<"YES"<<endl;
    } else if(k<s) {
        if(n<2)
            cout<<"NO"<<endl;
        else if(n>k+s)
            cout<<"NO"<<endl;
        else
            cout<<"YES"<<endl;
    } else 
        assert(false);

    /* So, how to check if there is a not-lucky distribution? 
     * ...to much cases  :/
     **/
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
