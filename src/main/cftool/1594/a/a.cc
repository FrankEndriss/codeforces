/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Why there allways exist an answer?
 *
 * Consider some l, and r=l+x, 
 * Then we increase the sum by x+1 by moving both
 * numbers one up.
 *
 * Consider n to be a big prime...
 * ???
 *
 * Somewhat annoying to know that there is a trivial solution
 * because it is an A, and then not see it :/
 */
void solve() {
    cini(n);

    int l=-(n-1);
    cout<<l<<" "<<n<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
