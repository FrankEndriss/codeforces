/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We cannot iterate all special numbers, to much of them :/
 *
 * So binary search somehow?
 * ...Ah, n is given :/
 *
 * So first is n^0
 * 2. is n^0 + n^1
 * 3. is n^0 + n^1 + n^2
 * and so on...
 * so we search 
 * sum(i=0..k, n^i)
 *
 * No, only the ones there the bit is set.
 */
using mint=modint1000000007;

void solve() {
    cini(n);
    cini(k);

    mint nn=n;
    mint ans=0;
    int p=0;
    while(k) {
        if(k&1) 
            ans+=nn.pow(p);
        p++;
        k/=2;
    }

    cout<<ans.val()<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
