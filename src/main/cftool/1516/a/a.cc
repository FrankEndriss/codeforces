/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to make the first element smallest possible.
 * So we subtract 1 from the first element as often as 
 * possible. If 0 then from the second etc.
 * And we add to the last element.
 * However, edgecase if there are not that much elements.
 * So we can undo each operation by the next operation.
 * ...
 * No edgecases, it is _at most_ K operations, not exactly k operations.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    for(int i=0; k>0 && i+1<n; i++) {
        int d=min(a[i], k);
        a[i]-=d;
        a.back()+=d;
        k-=d;
    }

    for(int i=0; i<n; i++) 
        cout<<a[i]<<" ";
    cout<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
