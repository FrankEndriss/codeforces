/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider all multi-elements to be there once or twice.
 * We need to check foreach element if all other elements
 * XOR-sum is equal.
 * ...No.
 *
 * If there are multiple elements, then two cases:
 * Odd:  We need to check if the sum is zero.
 * Even: We need to check if sum eq the value.
 * ...
 * Ok, it is about _adjacent_ elements, it is another problem.
 * The observation is that we have all elements equal if
 * there are two or three partitions with equals xor-sum.
 * If it is two partitions, then it is because the whole array
 * has xorsum==0
 */
void solve() {
    cini(n);

    int sum=0;
    vi a(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        sum^=aux;
        a[i]=sum;
    }

    if(a.back()==0) {
        cout<<"YES"<<endl;
        return;
    }

    for(int i=1; i<n; i++) { /* end of first partition */
        for(int j=i+1; j<n; j++) { /* end of second partition */
            int sum1=a[i-1];
            int sum2=a[j-1]^a[i-1];
            int sum3=a.back()^a[j-1];
            if(sum1==sum2 && sum2==sum3)  {
                cout<<"YES"<<endl;
                return;
            }
        }
    }

    cout<<"NO"<<endl;

}


signed main() {
    cini(t);
    while(t--)
        solve();
}
