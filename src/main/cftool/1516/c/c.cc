/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Ok, if the sum is odd then there is no partition, hence the array is good.
 * If sum is even we can remove 1 odd element.
 * So, consider all elements are even.
 *
 * Check per knapsack if we can build the half of the sum.
 * If yes then remove a multiple of 2 that is not a multiple of 4.
 * ie an element where a[i]/2 is odd.
 *
 * If no such element exists, then all elements are multiples of 4.
 * So, again, do the check...
 *
 * Example 
 * {2,2,2}
 * No need to remove anything
 * {2,2,2,2}
 * Remove one element
 * {2,2,6,2}
 * One element
 * {1,1,1,2,3}
 * Remove on odd
 * {2,2,2,4,6}
 * ???
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    int sum=accumulate(all(a), 0LL);

    for(int i=0; i<22; i++) {
        vb dp(sum/2+1);
        dp[0]=true;
        for(int j=0; j<n; j++) {
            for(int k=sum/2; k>=a[j]; k--)
                if(dp[k-a[j]])
                    dp[k]=true;
        }

        if((sum&1) || !dp[sum/2]) {
            cout<<0<<endl;
            return;
        } else {
            for(int j=0; j<n; j++) {
                if(a[j]&1) {
                    cout<<"1 "<<endl<<j+1<<endl;
                    return;
                }
                a[j]/=2;
            }
        }

        sum/=2;
    }

    assert(false);

}

signed main() {
    solve();
}
