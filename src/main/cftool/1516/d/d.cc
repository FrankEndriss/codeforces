/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=10000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, set<ll> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization.insert(d);
            n /= d;
        }
    }
    if (n > 1)
        factorization.insert(n);
}

/* 
 * Here, each element has a right of it element that
 * must not be in same range in the end.
 * So first step is to find this "upper bound".
 * We go from right to left maintaining the last seen
 * index of the prime factors of a[i].
 * Upper bound is min of the primes indexes of current number,
 * and with min of the number one above.
 *
 * Then we need to create a jump table, binary lifting, to quickly
 * find the min number of jumps we can to do go from left to right.
 */
const int N=1e5+7;
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    vvi r(20, vi(n+1, n));    /* r[i]=next element we must start a new partition after i */
    vi f(N, n);               /* f[p]=current jump target for prime p */

    for(int i=0; i<n; i++)  {
        r[0][n-1-i]=r[0][n-i];

        set<int> s;
        trial_division4(a[n-1-i], s);

        for(int p : s) {
            r[0][n-1-i]=min(r[0][n-1-i], f[p]);
            f[p]=n-1-i;
        }
    }

    for(int j=1; j<20; j++)
        for(int i=0; i<n; i++)
            r[j][i]=r[j-1][r[j-1][i]];

    for(int i=0; i<q; i++)  {
        cini(l); l--;
        cini(rr);

        int ans=0;
        while(l<rr) {
            int lans=0;
            while(r[lans+1][l]<rr)
                lans++;

            ans+=(1<<lans);
            l=r[lans][l];
        }
        cout<<ans<<endl;

    }

}

signed main() {
    solve();
}
