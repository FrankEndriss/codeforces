/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* a[i]*a[j] == i+j
 *
 * Consider a[j]
 * a[j]*a[i]==j+i
 * a[j]=(j+i)/a[i]
 * a[j]=j/a[i] + i/a[i]
 *
 * a[i]=(j+i)/a[j]
 * a[i]=j/a[j] + i/a[j]
 *
 * x=a[j]-j/a[j]
 * a[i]-x = i/a[j]
 *
 * ???
 * x=a[j], y=j
 * x*a[i]=i+y
 * x*a[i]-i=y
 *
 * Why the value of a[i] is limited to 2*n?
 *
 * Note that i+j<2*n and a[i]>0, so foreach a[j]
 * there is only a limited number of a[i] possible anyway.
 *
 * i+j is in range j+1..j+j-1
 * consider a[j]==1,
 * then we need to find all a[i] with 1*a[i]-i=j
 * consider a[j]==2
 * then we need to find all a[i] with 2*a[i]-i=j
 * consider a[j]==3...
 * then we need to find all a[i] with 3*a[i]-i=j 
 *
 * We can store them up to say 500.
 * And for a[j]>=500 we can iterate all possible positions.
 */
void solve() {
    cini(n);
    vi a(n+1);
    for(int i=1; i<=n; i++) 
        cin>>a[i];

    int ans=0;
    for(int i=1; i<n; i++) {
        for(int val=1; a[i]*val<=2*n; val++) {
            int sum=a[i]*val;
            int idx=sum-i;
            if(idx>i && idx<=n && a[idx]*a[i]==sum)
                ans++;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();

}
