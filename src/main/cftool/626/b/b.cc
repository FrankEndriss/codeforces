/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << "=" << a << " ";
    logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* To end with R we need to have at least one G and one B
 */
void solve() {
    cini(n);
    cins(s);
    vi f(3);
    for(char c : s) {
        if(c=='B')
            f[0]++;
        else if(c=='G')
            f[1]++;
        else    
            f[2]++;
    }

    set<int> ans;
    for(int i=0; i<3; i++) {
        int c0=i;
        int c1=(i+1)%3;
        int c2=(i+2)%3;

        if((f[c0]>0 && f[c1]>1) || f[c0]>0 && f[c2]>1) 
            ans.insert(c0);

        if(f[c1]>0 && f[c2]>0)
            ans.insert(c0);

        if(f[c0]>0 && f[c1]==0 && f[c2]==0)
            ans.insert(c0);
    }

    string sans;
    for(int i : ans) {
        if(i==0)
            sans+="B";
        else if(i==1)
            sans+="G";
        else
            sans+="R";
    }

    cout<<sans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

