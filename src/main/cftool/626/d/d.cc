/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to count the number of pairs foreach difference of the two numbers.
 */
const int N=5001;
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a), greater<int>());

    /* dp[i]=number of pairs with diff i, i<N */
    vi dp(N);   /* n*(n-1)/2 pairs */
    for(int i=0; i<n; i++) 
        for(int j=i+1; j<n; j++) 
            dp[a[i]-a[j]]++;

    /* dp2[i]=number of pairs of pairs with diff i, i<2*N */
    vi dp2(2*N);    /* (n*(n-1)/2)^2 pairs of pairs */
    for(int i=1; i<N; i++) 
        for(int j=1; j<N; j++)
            dp2[i+j]+=dp[i]*dp[j];

    const int cntall=accumulate(all(dp2), 0LL); /* number of pairs of pairs */

    /* make dp[] be prefix sums, so dp[i]=number of pairs with sum up to i */
    for(int i=1; i<N; i++) 
        dp[i]+=dp[i-1];

    /* Foreach sum we want to find the number of pairs where Jerry gets bigger diff in last draw.
     */
    double dans=0;
    for(int i=0; i<N; i++)
        dans+=(((ld)dp2[i])/cntall)*(dp.back()-dp[i])/dp.back();

    cout<<dans<<endl;
}

signed main() {
        solve();
}
