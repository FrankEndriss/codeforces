/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cini(m);
    cinai(a,n+1);
    cinai(b,m+1);
 
    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());
    while(a.size()<b.size())
        a.push_back(0);
    while(b.size()<a.size())
        b.push_back(0);
    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());

    int signa=1;
    for(int i=0; i<a.size(); i++) {
        if(a[i]!=0) {
            if(a[i]<0) {
                signa*=-1;
            }
            break;
        }
    }

    int signb=1;
    for(int i=0; i<b.size(); i++) {
        if(b[i]!=0) {
            if(b[i]<0) {
                signb*=-1;
            }
            break;
        }
    }
 
    for(size_t i=0; i<a.size(); i++) {
        if(b[i]==0) {
            string si="";
            if((signa<0 && signb>0) || (signa>0 && signb<0))
                si="-";

            cout<<si<<"Infinity"<<endl;
            return;
        } else if(a[i]!=0) {
            if(b[i]<0) {
                b[i]=-b[i];
                a[i]=-a[i];
            }
            int g=__gcd(abs(a[i]), abs(b[i]));
            cout<<a[i]/g<<"/"<<b[i]/g<<endl;
            return;
        } else {
            cout<<"0/1"<<endl;
            return;
        }
    }
    cout<<"0/1"<<endl;
 
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

