/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* The problem ask for the max number of
 * numbers in one row.
 * It is obvious that we can always put one soldier per row.
 * (except the sum/number of soldiers is to small)
 * Is it monotonic, ie if 3 works, and 5 works, does it
 * mean 4 works, too?
 * Obviously yes, since if 5 works we can simply remove on soldier
 * per row to get 4.
 * So binary search should work.
 *
 * How to check one k?
 * Sort by size.
 * Put smallest possible into first/next row.
 * repeat.
 */
void solve() {
    cini(n);
    cini(k);

    cinai(c,n);
    if(n==1) {
        c.push_back(0);
        n++;
    }

    int l=0;
    int r=1e18;

    while(l+1<r) {
        int mid=(l+r)/2;

        int rows=0;
        if(mid==0) {
            rows=k;
            break;
        }
        int avail=c[0];

        for(int i=1; i<n && rows<k; i++) {
            rows+=(avail+c[i])/mid;
            avail=min(c[i], (avail+c[i])%mid);
        }

        if(rows>=k)
            l=mid;
        else
            r=mid;
    }
    
    cout<<l*k<<endl;
    
}

signed main() {
    cini(t)
    while(t--)
       solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
