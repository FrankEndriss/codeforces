/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* We need to build pairs, biggest team
 * with smallest team etc.
 * Then we loop, taking out the biggest
 * team from all pairs, and build new pairs.
 * This is O(n^2) which should work since
 * max(k)==8000
 */

void solve() {
    cini(n);
    cini(k);
    vll ts(k);    // team sizes
    for(int i=0; i<n; i++) {
        cini(t);
        ts[t-1]++;
    }
    sort(ts.begin(), ts.end(), greater<int>());

    int offs=k&1;   // offset to first pairable team
    ll ans=ts[0]*k;
    while(offs<k) {
        ll s=offs?ts[0]:ts[0]+ts.back();

        for(int i=0; i+offs<k-i; i++) {
            s=max(s, ts[i+offs]+ts[k-1-i]);
        }
        assert((k-offs)%2==0);
        ll cost=s*(offs+(k-offs)/2);
        ans=min(ans, cost);
        offs+=2;
    }

    cout<<ans<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

