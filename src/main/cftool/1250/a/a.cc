/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
    cout << *it << " = " << a << endl;
    logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)
#define forn(i,n) forn(i,0,n)
#define forn(n) forn(i,0,n)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct node {
    int mi, ma;
    int prev;   // id of node on previous position
};

/* we need to find nodes by id (to get the a node)
 * and by position (to get the node prev to a) */

void solve() {
    cini(n);
    vi mi(n);
    vi ma(n);
    vi pos(n);  // pos[i]==position of node i
    vi q(n);    // q[i]=node at pos i

    for(int i=0; i<n; i++) {
        mi[i]=i;
        ma[i]=i;
        pos[i]=i;
        q[i]=i;
    }

    cini(m);
    for(int i=0; i<m; i++) {
        cini(a);
        a--;
        if(pos[a]>0) {
            int prev=q[pos[a]-1];
            pos[a]--;
            pos[prev]++;

            swap(q[pos[a]], q[pos[prev]]);

            mi[a]=min(mi[a], pos[a]);
            ma[prev]=max(ma[prev], pos[prev]);
        }
/*
for(auto ent : q) 
    cout<<ent.pos+1<<" ";
cout<<endl;
*/
    }

    for(int i=0; i<n; i++)
        cout<<mi[i]+1<<" "<<ma[i]+1<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

