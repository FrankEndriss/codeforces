/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    vb b(n);    // isAuction
    for(int i=0;i<m; i++) {
        cini(aux);
        aux--;
        b[aux]=true;
    }

    vi id(n);
    iota(id.begin(), id.end(), 0);
    sort(id.begin(), id.end(), [&](int i1, int i2) {
        if(b[i1]!=b[i2])
            return b[i1]<b[i2];
        else
            return a[i2]<a[i1];
    });

    int ans=0;
    for(int i=0; i<n; i++) {
        if(!b[id[i]])
            ans+=a[id[i]];
        else {
            ans+=max(ans, a[id[i]]);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

