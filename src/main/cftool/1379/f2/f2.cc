/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

template <typename E>
class SegmentTree {
  private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

  public:
    /** SegmentTree. Note that you can hold your data in your own storage and give
     * an Array of indices to a SegmentTree.
     * @param beg, end The initial data.
     * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
     * @param pCumul The cumulative function to create the "sum" of two nodes.
    **/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=(int)distance(beg, end);
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Updates all elements in interval(idxL, idxR].
    * iE add 42 to all elements from 4 to inclusive 7:
    * update(4, 8, [](int i1) { return i1+42; });
    */
    void updateRange(int pIdxL, int pIdxR, function<E (E)> apply) {
        pIdxL+=treeDataOffset;
        pIdxR+=treeDataOffset;
        for(int i=pIdxL; i<pIdxR; i++)
            tree[i]=apply(tree[i]);

        while (pIdxL != ROOT) {
            pIdxL=parent(pIdxL);
            pIdxR=max(pIdxL, parent(pIdxR-1));
            for(int i=pIdxL; i<=pIdxR; i++)
                tree[i]=cumul(tree[leftChild(i)], tree[rightChild(i)]);
        }
    }

    /** Updates the data at dataIdx to value returned by apply. */
    void updateApply(int dataIdx, function<E(E)> apply) {
        updateSet(dataIdx, apply(get(dataIdx)));
    }

    /** Updates the data at dataIdx by setting it to value. */
    void updateSet(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return value at single position, O(1) */
    E get(int idx) {
        return tree[idx+treeDataOffset];
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

/*
 * Two consecutive rows form a left and a right row,
 * or a first and a second.
 * We need to place m kings within each such pair of rows.
 * This means, we need to place one king in every pair
 * of even/odd cols.
 * if we place the prev king in the even cell then we can
 * choose, else we must use the odd cell.
 *
 * So, starting in pair first/second row, we place the kings
 * leftmost in row==0 until a cell in row==0 is blocked.
 * At that point we switch to second row, and place them 
 * there for the rest of the row.
 *
 * Then we repeat in row 2,3.
 * So we again place the kings leftmost/topmost as possible.
 * If previous doublerow placed a king in second rows, 
 * we need to place in second row, too.
 * And Again, place all following kings of that row in second row.
 *
 * So in every doublerow we can place in first row if there
 * is no blocked cell in first row in that col or prior to 
 * this col.
 * In other words, a block in first row blocks all first rows
 * in that cell.
 *
 * Then we need to consider second rows blocks. They are ok
 * in rows before first row blocks, but not ok in later rows.
 *
 * How to implement?
 *
 * To check a double row, we need to find the leftmost
 * first-row block, and if a second row-block exists in current
 * row bigger than the leftmost one.
 *
 * But we do not want to check all rows after for each query.
 * We need to find if there is a second row block prior 
 * to some first row block, or not.
 *
 * Leftmost first-row block is query a set of blocks.
 * But for every new one we need to check all consecutive rows.
 * So we need leftmost second-row-block of all remaining 
 * rows. Would be a segmentTree(min) of size m.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cini(q);

    vi data(m);
    SegmentTree<int> seg(all(data), INF, [](int i1, int i2) {
        return i1<i2;
    });

    for(int i=0; i<q; i++) {
        cini(r); r--;
        cini(c); c--;

        pii cc={r,c};
        if(r%2==0)  {
            if(beve.count(cc))
                beve.erase(cc);
            else
                beve.insert(cc);
        } else {
            if(bodd.count(cc))
                bodd.erase(cc);
            else
                bodd.insert(cc);
        }

        if(beve.size()==0 || bodd.size()==0)
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    }
}

/* 3 2 10
 * (4,2) -> (4,2) == YES
 * (6,4) -> [(4,2),(6,4)] ==YES
 * (1,3) -> [(4,2),(6,4)],[(1,3)] ==NO
 * (4,2) -> [(6,4)],[(1,3)] ==NO
 * (6,4) -> [],[1,3]==YES
 * (2,2) -> [2,2][1,3]==NO
 */

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
