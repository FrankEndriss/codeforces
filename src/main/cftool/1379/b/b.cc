/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * m= n*a + b - c
 * m-b+c=n*a
 * (m-b+c)/a=n
 *
 * Let 
 *  minD=l-r
 *  maxD=0
 *
 * So we want to find number n with
 * n*a = m+-max(d)
 *
 * ***
 * Note: l<=r
 */
void solve() {
    cini(l);
    cini(r);
    cini(m);

    int minD=l-r;
    int maxD=r-l;
    for(int a=l; a<=r; a++) {
        int i=(m/a);
        for(int n=max(1LL,i); n<=i+1; n++) {
            if(n*a+minD<=m && n*a+maxD>=m) {
                int d=m-n*a;
                int b,c;
                if(d<0) {
                    b=l;
                    c=b-d;
                } else {
                    c=l;
                    b=l+d;
                }

                assert(n*a+b-c==m);
                cout<<a<<" "<<b<<" "<<c<<endl;
                return;
            }
        }
    }

    assert(false);
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
