/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Assume maximum second.
 * It always makes sense to choose all seconds
 * from one flower type.
 * If choosen that second type it is better to 
 * use all first flower-types better than that
 * second type, and the first of the fixed second
 * type.
 *
 * So calc ans for every second type, choose max.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    vector<pii> a(m);
    for(int i=0; i<m; i++)
        cin>>a[i].first>>a[i].second;

    sort(all(a));

    vi posta(m+1);
    for(int i=m-1; i>=0; i--)
        posta[i]=posta[i+1]+a[i].first;

    int ans=posta[max(0LL, m-n)];   /* only firsts */
    for(int i=0; i<m; i++) {
        /* idx of first flower with first better than current second */
        auto it=upper_bound(all(a), make_pair(a[i].second, INF ));
        int idx=distance(a.begin(), it);

        if(m-idx>n)
            continue;

        if(idx>i) { /* current first not included */
            int lans=posta[idx];
            if(m-idx+1>n)
                continue;
            lans+=a[i].first;
            lans+=(n-(m-idx)-1)*a[i].second;
            ans=max(ans, lans);
            continue;
        }

        int lans=posta[idx]+a[i].second*(n-(m-idx));
        ans=max(ans, lans);
    }

    cout<<ans<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
