/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that foreach bit we got same number of numbers
 * with 0 bit as we got with 1 bit.
 *
 * So, to set the bits of k, pair two numbers that have that bits set,
 * but all other numbers different.
 *
 * ...somehow :/
 */
void solve() {
	cini(n);
	cini(k);

	int mask=n-1;

	if(k==mask) {
		cout<<-1<<endl;
		return;
	}

	const int nn=n/2;
	vector<pii> ans;
	vb vis(n);

	for(int i=0; (1<<i)<n; i++)  {
		if((k|(1<<i)) !=k) {
			ans.emplace_back(k, k|(1<<i));
			vis[k]=true;
			vis[k|(1<<i)]=true;
			break;
		}
	}
	for(int i=0; i<n; i++) {
		if(!vis[i]) {
			bool done=false;
			for(int j=i+1; j<n; j++) {
				if(!vis[j] && (i&j)==0) {
					ans.emplace_back(i,j);
					vis[i]=true;
					vis[j]=true;
					done=true;
					break;
				}
			}
			if(!done) {
				cout<<-1<<endl;
				return;
			}
		}
	}

	assert(ans.size()==n/2);
	for(int i=0; i<nn; i++) 
		cout<<ans[i].first<<" "<<ans[i].second<<endl;


}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
