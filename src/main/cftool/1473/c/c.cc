/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* k=3 n=4
 * a[]=1 2 3 2
 *
 * Since b[] should be lex maximum
 * we do need to care about inversions.
 *
 * What is the lex biggest possible
 * b[] with not more inversions than a[]?
 * a[]=1 2 3 2
 * b[]=2 1 2 3
 *
 * a[]=1 2 3 2 1
 * b[]=2 1 2 3 1
 *
 * a[]=1 2 3 4 3
 *
 * Number of inversions in a:
 * (n-k) * (n-k+1) / 2
 *
 * How to create the lex max array with
 * given number of inversions?
 *
 * -> Again, I do not see the educational aspect in this. WTF???
 * There should be some super simple observation...
 * We can "move" numbers bey some positions.
 * By moving one of the right numbers we remove 1 inversion.
 *
 * So, if there are nore than n-1 inversions, then 
 * we can use k as first element.
 *
 * ***
 * Ok, there are two parts in array a[]
 * It starts with 1,2,3...
 * then the second part looks like
 * ...4 5 6 5 4
 *
 * In the first part we cannot change anything, because number of inversions
 * would increase.
 * But in the second part we want to change "inner to outer", 
 * which means the above would become
 * ...6 5 4 5 6
 * So we need to reverse that part in p[]
 */
void solve() {
    cini(n);
    cini(k);

    vi p(k);
    iota(all(p), 1);

    reverse(p.end()-(n-k+1), p.end());

    for(int i : p) 
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
