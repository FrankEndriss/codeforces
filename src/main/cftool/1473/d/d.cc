/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;

//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


#include <atcoder/segtree>
using namespace atcoder;


using S=pii; /* type of values */

S st_op(S a, S b) {
    return { min(a.first, b.first), max(a.second, b.second) };
}

const signed INF=1e9;
S st_e() {
    return { INF, -INF };
}

using segt=segtree<S, st_op, st_e>;


/* After 0,l-1 instruction max-min values can be reached.
 * then, instructions r,n make seg.prod(r,n) min/max difference
 * to the value given at pinc[r].
 *
 * Its an annoying index fiddling. Not educational at all. :/
 * ****
 * We want min/max inc/dec in some range l,r.
 * So we query offset at l-1, and adjust
 * the min/max of the range by that offset.
 * */
void solve() {
    cini(n);
    cini(m);
    cins(s);

    //cerr<<"n="<<n<<" m="<<m<<" s="<<s<<endl;

    vi inc(n);
    for(int i=0; i<n; i++) {
        if(s[i]=='+')
            inc[i]=1;
        else
            inc[i]=-1;
    }

    vi pinc(n+1);
    for(int i=1; i<=n; i++)
        pinc[i]=pinc[i-1]+inc[i-1];
    pinc.push_back(pinc.back());

    vector<pii> data(n+1);
    for(int i=0; i<=n; i++)
        data[i]= { pinc[i], pinc[i] };
    data.push_back(data.back());

    segt seg(data);

    for(int i=0; i<m; i++) {
        cini(l);
        cini(r);

        //cerr<<"l="<<l<<" r="<<r<<endl;
        pii p1=seg.prod(0, l);
        //cerr<<"prod 0,"<<l<<" = "<<p1.first<<" "<<p1.second<<endl;
        pii p2=seg.prod(r+1, n+2);
        //cerr<<"prod "<<r+1<<" "<<n+1<<" = "<<p2.first<<" "<<p2.second<<endl;

        int offs=pinc[r];
        int offs2=pinc[l-1];

        p2.first-=offs;
        p2.second-=offs;
        p2.first+=offs2;
        p2.second+=offs2;


        int ans=1+max(p1.second, p2.second)-min(p1.first,p2.first);
        if(ans<0)
            ans=0;
        cout<<ans<<endl;

    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
