/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* X gets smaller with each operation.
 * So it makes sense to start with the 
 * biggest number.
 * Second x must be the biggest number.
 * So we can choose which number to remove
 * whith that x. Then all other numbers
 * are determined. So simply choose all.
 */
void solve() {
    cini(n);

    multiset<int> aa;
    int ma=0;
    for(int i=0; i<2*n; i++) {
        cini(aux);
        ma=max(ma, aux);
        aa.insert(aux);
    }

    set<int> b;
    for(int i : aa)
        b.insert(i);

    for(int i : b) {
        multiset<int> a=aa;

        int x=ma+i;
        vector<pii> ans;
        while(a.size()>0) {
            pii p;
            auto it=a.end();
            it--;
            p.first=*it;
            a.erase(it);
            it=a.lower_bound(x-p.first);
            if(it!=a.end() && *it==x-p.first) {
                p.second=*it;
                a.erase(it);
                ans.push_back(p);
                x=max(p.first, p.second);
            } else 
                break;
        }
        if(a.size()==0) {
            cout<<"YES"<<endl;
            cout<<ma+i<<endl;
            for(auto p : ans)
                cout<<p.first<<" "<<p.second<<endl;
            return;
        }
    }
    cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
