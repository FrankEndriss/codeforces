/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* How to remove all stones?
 *
 * Sum of all stones must be even.
 * No pile must be bigger than the sum of its two neigbours.
 * Outermost piles must not be bigger than its neigbour.
 * Let local maximum be p. let left of p be pL and other pR
 * We must use at least p-pR from pL, and p-pL from pR.
 * Check if this is allways possible. 
 *
 * How to check the swaps?
 *
 */
void solve() {
    cini(n);
    cinai(a,n);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
