/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* dp[i]= min number of missed swaps to end in pos i
 *
 * -> should be kotlin :/
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    const int INF=1e9;
    vi dp(n+1, INF);
    dp[k]=0;
    for(int i=0; i<m; i++) {
        cini(x);
        cini(y);

        /* if ball is at x, we need to add one miss to stay at x.
         * but we can be at y with the same number as x. */

        int ny=INF, nx=INF;

        if(dp[x]>=0)
            ny=min(dp[y]+1, dp[x]);

        if(dp[y]>=0)
            nx=min(dp[x]+1, dp[y]);

        dp[y]=ny;
        dp[x]=nx;
    }

    for(int i=1; i<=n; i++)
        if(dp[i]==INF)
            cout<<-1<<" ";
        else
            cout<<dp[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
