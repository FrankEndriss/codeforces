/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/*
 * foreach x[i]
 * Sum of all pairs with AND mul sum of all pairs with OR
 *
 * example 1:
 * 1 7
 * le[0]=2;
 * le[1]=1;
 * le[2]=1;
 *
 * 1&1 * 1|1
 * 1&1 * 1|7
 * 7&1 * 1|1
 * 7&1 * 1|7
 *
 * 1&7 * 7|1
 * 1&7 * 7|7
 * 7&7 * 7|1
 * 7&7 * 7|7
 */
const int N=61;
void solve() {
    cini(n);
    cinai(x,n);

    vi le(N); /* number of bits in all numbers */

    for(int i=0; i<n; i++) {
        for(int j=0; j<N; j++) {
            if(x[i]&(1LL<<j))
                le[j]++;
        }
    }

    int ans=0;
    for(int i=0; i<n; i++) {

        int mAND=0;
        int mOR=0;
        for(int j=0; j<N; j++) {
            int b=(1LL<<j);
            int bb=b%MOD;
            if(b & x[i]) {
                assert(le[j]>0);
                mAND=pl(mAND, mul(le[j], bb));
                mOR=pl(mOR, mul(n, bb));
            } else {
                mOR=pl(mOR, mul(le[j], bb));
            }
        }
        ans=pl(ans, mul(mAND,mOR));
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
