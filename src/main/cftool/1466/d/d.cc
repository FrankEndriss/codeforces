/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* what is a k-coloring here?
 * A 1-coloring includes all vertices, ie sum of all vertices
 * ans[0] is that.
 *
 * The opt 2-coloring sets the colors so that the tree is cut in 
 * 2 components, then 1 of the vertex count twice, all others one time.
 * The vertex where we cut must not be a leaf, because then
 * it would count only once.
 * It never makes sense to cut the subgraph of one color in two 
 * components, because it would allways be better to color
 * one of the halfs with one of the adjacent colors of that component.
 *
 * The opt 3 coloring cuts the tree in 3 components so that 2 vertex
 * count twice (or one three times), again, no leaf allowed.
 *
 * That continues until only leafs remain, then each more color
 * cuts the min(w) leaf.
 */
void solve() {
    cini(n);
    cinai(w,n);

    vi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u]++;
        adj[v]++;
    }

    vi adj2=adj;

    priority_queue<pii> q;
    for(int i=0; i<n; i++) 
        if(adj[i]>1)
            q.push({w[i], adj[i]});

    int ans=accumulate(all(w), 0LL);
    cout<<ans<<" ";
    int cnt=1;
    while(q.size() && cnt<n-1) {
    //for(int i=0; i<n-2; i++) {
        auto [ww,v]=q.top();
        q.pop();
        ans+=ww;
        cnt++;
        cout<<ans<<" ";
        v--;
        if(v>1)
            q.push({ww,v});
    }

    priority_queue<int> leafs;
    for(int i=0; i<n; i++) 
        if(adj2[i]==1)
            leafs.push(-w[i]);

    while(cnt<n-1) {
        assert(leafs.size()>0);
        int ww=-leafs.top();
        leafs.pop();
        ans-=ww;
        cout<<ans<<" ";
        cnt++;
    }

    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
