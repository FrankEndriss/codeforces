/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want our hero to fight in the last fight.
 *
 * So, there he will win if his oponent
 * is weaker on at least one to the both available 
 * maps. So, if any of the other players that would lose
 * that last fight can be the pre-last player, then ans=1.
 *
 * So there must be a path from current to all others
 * where each edge goes from high to low.
 *
 * How to check if such path exists foreach player?
 * We cannot dfs/bfs each one.
 *
 * Cycles:
 * If there is a cycle, each one of the cycle elements can be last one.
 *
 * ???
 * ****
 */
void solve() {
}

signed main() {
    solve();
}
