/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * An ordering allways exists if the number of cells
 * match each other.
 *
 * For n dominoes we got mi[rl][bw] cells at pos rl with color bw,
 * For all 4 cases it must be true that
 * mi[rl][bw] + mi[!rl][!bw] == n
 *
 * We can choose two coloring in each pair 
 * mi[0][?],mi[1][?]
 *
 * So, in a pair of {rl,bw}:{!rl,!bw} we have x unpainted
 * positions.  So 2^x posibilities to paint them.
 */
using mint=modint998244353;
void solve() {
    cini(n);
    vvi cnt(2, vi(2));
    for(int i=0; i<n; i++)  {
        cins(s);
        cnt[0][0]+=s[0]=='W';
        cnt[0][1]+=s[0]=='B';
        cnt[1][0]+=s[1]=='W';
        cnt[1][1]+=s[1]=='W';
    }

    mint ans=mint(2).pow(
            n-max(cnt[0][0],cnt[1][1])) * 
        mint(2).pow(
            n-max(cnt[0][1],cnt[1][0]));
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
