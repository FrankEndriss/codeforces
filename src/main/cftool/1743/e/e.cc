/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Again, scan the text very carefully.
 * WLOG consider the smaller t[i] to be the first laser.
 * The rules seem to say:
 * -it does never makes sense to wait longer than the second laser is loaded.
 * -both lasers have an implicit 'load factor', how fast the power builds up.
 * -if the 'load factor' of first is much better than second it might make sense
 *  to fire it before second is loaded.
 * -starting with empty lasers, it there is no choice until first is loaded.
 * -Then we want to shot, or wait for second to be loaded, no other choice useful.
 *
 * So we got two possible states at t[0]:
 * -laser first empty, h=h0-(p[0]-s) and l[1] filled by t[0]
 * -laser second filled by t[0]
 * So we go on a priorityQueue sorted by t[], and from each state
 * create the next possible ones.
 * State is allways { time, l[0], l[1], h }
 * First time h<=0 is ans.
 *
 * Unknown if simulation goes TLE or not.
 * But since h=max(5000) it should work.
 */
void solve() {
    cini(p1);
    cini(t1);
    cini(p2);
    cini(t2);
    cini(h);
    cini(s);

    using t4=tuple<int,int,int,int>;

    set<t4> q;
    q.emplace(0,0,0,h);

    while(q.size()) {
        auto it=q.begin();
        auto [t,l1,l2,hh]=*it;
        assert(l1<=t1);
        assert(l2<=t2);
        assert(hh>0);
        assert(t>=0);
        q.erase(it);
        //cerr<<"t="<<t<<" l1="<<l1<<" l2="<<l2<<" h="<<hh<<endl;

        if(l1==t1 && l2==t2) {    /* fire both */
            int hhh=hh-(p1+p2-s);
            if(hhh<=0) {
                cout<<t<<endl;
                return;
            }

            q.emplace((t+min(t1,t2)), min(t1,t2), min(t1,t2), hhh);
        } else if(l1==t1) {
            /* fire l1 if of use */
            if(p1>s) {
                int hhh=hh-max(0LL, p1-s);
                if(hhh<=0) {
                    cout<<t<<endl;
                    return;
                }
                int dur=min(t1, t2-l2);
                assert(dur>0);
                q.emplace((t+dur), dur, l2+dur, hhh);
            }

            /* fire none, wait for l2 */
            q.emplace((t+t2-l2), t1, t2, hh);
        } else if(l2==t2) {
            /* fire l2 if of use */
            if(p2>s) {
                int hhh=hh-max(0LL, p2-s);
                if(hhh<=0) {
                    cout<<t<<endl;
                    return;
                }
                int dur=min(t1-l1, t2);
                q.emplace((t+dur), l1+dur, dur, hhh);
            }

            /* fire none, wait for l1 */
            q.emplace((t+t1-l1), t1, t2, hh);
        } else { /* wait for next to be loaded */
            int dur=min(t1-l1, t2-l2);
            q.emplace((t+dur), l1+dur, l2+dur, hh);
        }
    }

    assert(false);
}

signed main() {
    solve();
}
