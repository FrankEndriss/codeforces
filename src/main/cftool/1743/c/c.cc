/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * greedy whenever it makes ans better move a lid.
 * ...but hard to get statement. Is there some pitfall in the text?
 *
 * No, its a dp.
 *
 * So, 
 * dp[i][0]=max val if s[i]=='0'
 * dp[i][1]=max val if s[i]=='1'
 */
const int INF=1e9;
void solve() {
    cini(n);
    cins(s);
    cinai(a,n);

    vvi dp(n, vi(2));

    if(s[0]=='1') {
        dp[0][1]=a[0];
        dp[0][0]=-INF;
    }

    for(int i=1; i<n; i++) {
        if(s[i]=='0') {
            dp[i][0]=max(dp[i-1][0], dp[i-1][1]);
            dp[i][1]=-INF;
        } else if(s[i]=='1') {
            dp[i][0]=dp[i-1][0]+a[i-1];
            dp[i][1]=max(dp[i-1][0], dp[i-1][1])+a[i];
        }
    }

    cout<<max(dp[n-1][0],dp[n-1][1])<<endl;
   
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
