/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Obiously we need to set the highest bit possible.
 * So choose the whole string (with leading 0s removed) as s1, makes the leftmost '1'
 * the highest bit.
 * As s2 we choose s1 right shift by some positions.
 *
 * So we want to choose the shift of s2 so that at most consecutive '0'
 * right of the leftmost '1' in s1 are covered.
 * Note that the first '0' allways can be covered, by one of the '1'
 * left of it, by shift==1 or more.
 * So we never shift more positions that that between the first '1' and the 
 * first '0' right of it since else it would not be covered.
 *
 * How to find the best number of shifts?
 * How does the random defintion blahblah helps?
 * Some FFT application?
 * Some brute force using bitsets?
 *
 * idk :/
 */
void solve() {
}

signed main() {
        solve();
}
