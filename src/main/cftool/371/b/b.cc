/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* bfs */
void solve() {
    cini(a);
    cini(b);

    map<int,int> mA;
    mA[a]=0;
    queue<int> q;
    q.push(a);
    while(q.size()) {
        int v=q.front();
        q.pop();

        for(int d : {
                    5, 3, 2
                }) {
            if(v%d==0) {
                int u=(v/d);
                auto it=mA.find(u);
                if(it==mA.end()) {
                    mA[u]=mA[v]+1;
                    q.push(u);
                }
            }
        }
    }

    map<int,int> mB;
    mB[b]=0;
    q.push(b);
    while(q.size()) {
        int v=q.front();
        q.pop();
        if(mA.find(v)!=mA.end()) {
            int ans=mA[v] + mB[v];
            cout<<ans<<endl;
            return;
        }

        for(int d : {
                    5, 3, 2
                }) {
            if(v%d==0) {
                int u=(v/d);
                auto it=mB.find(u);
                if(it==mB.end()) {
                    mB[u]=mB[v]+1;
                    q.push(u);
                }
            }
        }
    }
    cout<<-1<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

