/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=30;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/**
 * Squares have an even number of each prime factor.
 *
 * each a[i] can be represented by a bitmask telling
 * if ith prime is even or odd times in that number.
 *
 * Then we want to find the number of (nonempty) subsets with xor-sum == 0
 * How to do that?
 * There are 19 primes <=70, so ~5e5 possible xorsums.
 * We cannot iteaterate them at each position.
 * On the other hand, there can be not more than a view
 * primes included in a single number.
 * 2*3*5*7>70, so max 3 primefactors. 
 * But also, there are exactly (only) 70 combinations.
 * Note also that most a[i] are there a lot of times.
 * Build a freq array from a[], then combine numbers from it.
 *
 * Then we can iterate all combinations of 1,2 or 3 numbers in range 1..70, and
 * check if they exist, and the xor of the mask eq 0, and mult with the number
 * of other available pairs.
 * How to find the number of those available pairs?
 * Consider number m is there j times.
 * ...complected :/
 */
using mint=modint1000000007;
void solve() {
    vi m(71);
    for(int i=1; i<71; i++) {
        int ii=i;
        for(int j=0; pr[j]<=ii; pr[j]++) {
            while(ii%pr[j]==0) {
                m[i]^=(1<<j);
                ii/=pr[j];
            }
        }
    }

    cini(n);
    map<int,int> f;
    for(int i=0; i<n; i++) {
        cini(aux);
        f[m[aux]]++;
    }

    mint inv2=mint(2).inv();
    int pairs=0;
    for(int i=0; i<71; i++)
        pairs+=f[m[i]]*(f[m[i]]-1)/2;

    mint pairs2=mint(2).pow(pairs);
    /* one number */
    for(int i=0; i<71; i++) {
        if(f[m[i]]>0) {

        }
    }

}

signed main() {
    for(int i=0; pr[i]<=70; i++) {
        cerr<<i<<" "<<pr[i]<<endl;
    }
        solve();
}
