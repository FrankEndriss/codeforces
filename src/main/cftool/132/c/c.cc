/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Greedy.
 * Find positions of odd sized T blocks.
 * An odd sized T block has weight of Fs right of
 * that block until the start of the next such block.
 * Each block changes the direction, so the weight
 * of odd blocks is negative.
 *
 * If n>= number of odd sized blocks:
 *   change all odd sized block to even sized
 *   remove consecutive double TT by FF
 *   change last F remaining n times.
 * else we need to optimize the balance of the blocks.
 *
 * ******************
 * No, its dp. :/
 */
void solve() {
    cins(s);
    cini(n);

    function<int(string&)> count=[&](string& str) {
        int ans=0;
        int inc=1;
        for(char c : str) {
            if(c=='F')
                ans+=inc;
            else
                inc=-inc;
        }
        return abs(ans);
    };

    function<void(string&,int)> flip=[](string& str, int idx) {
        if(str[idx]=='F')
            str[idx]='T';
        else
            str[idx]='F';
    };

    // TODO
    cout<<count(s)<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
