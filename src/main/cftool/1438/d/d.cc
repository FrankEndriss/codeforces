/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* go like 
 * 0,1,2
 * 0,3,4
 * 0,5,6
 * ...
 * Then 0 is xorsum of all elements.
 * Then do the same again, which makes all elements be the xorsum of all.
 * What about even length?
 * -> From examples it seems that even is not possible.
 * !if all are same we need 0 operations, even if len is even!
 *
 * What about if even and some same elements?
 * For example a={ 1, 2, 4, 7 }
 * Obviously 1,2,3 works :/
 * ...If one element is xorsum of all others then we can ignore that element!
 * ...If there are some (odd sized) subsets of the array with same xorsum we can 
 * operate each one separate.
 * How to find if such partition exist?
 * -> Thats to complecated, there must be simpler construction.
 */
void solve() {
    cini(n);
    cinai(a,n);

    if(n%2==0) {
        bool allSame=true;
        for(int i=1; i<n; i++) 
            if(a[i]!=a[0])
                allSame=false;
        if(allSame) {
            cout<<"YES"<<endl;
            cout<<"0"<<endl;
            return;
        }





        cout<<"NO"<<endl;
        return;
    }

    cout<<"YES"<<endl;
    cout<<n-1<<endl;
    for(int j=0; j<2; j++)
        for(int i=2; i<n; i+=2) 
            cout<<"1 "<<i<<" "<<i+1<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
