/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Let x=mex-1, ie the highest number used in the 
 * interval with the lowest mex.
 * Then after using all numbers less than x in every
 * interval we can fill the remaining positions with x.
 *
 * The max possible mex is limited by the 
 * shortest interval because we cannot put
 * more elements there.
 *
 * So simply repeat that elements, then every interval
 * will get all of them.
 */
void solve() {
    cini(n);
    cini(m);
    int mi=1e9;
    for(int i=0; i<m; i++) {
        cini(l);
        cini(r);
        mi=min(mi, r-l+1);
    }
    cout<<mi<<endl;
    for(int i=0; i<n; i++)
        cout<<i%mi<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
