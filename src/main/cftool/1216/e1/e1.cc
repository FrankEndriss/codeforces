/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * There are several levels, we need to take care to split them
 * nicely, else we get brain cancer.
 *
 * First find length of a sequence of numbers from 1..n
 * this is the seq from 1..9,10..99,100..999,...100000..n
 * 10^1-1*1 + 10^2-10^1*2 + 10^3-10^2*3 +...
 *
 * 123456789101112131415161718192021222324252627282930
 *          x         x         x         x         x
 *
 *
 */
/*
void solve() {
    vi tenA(18);
    tenA[0]=0;
    tenA[1]=9;

    vi tenB(18);
    tenB[0]=1;
    tenB[1]=10;
    for(int i=2; i<18; i++) {
        tenA[i]=tenB[i-1]*9*i;
        tenB[i]=tenB[i-1]*10;
    }

    cini(q);
    for(int i=0; i<q; i++) {
        cini(k); k--;   // nums start at 1, not at 0
        int len=0;
        int idx=1;
        for(; i<18;idx++) {
            if(len+tenA[idx]>=k)
                break;
            len+=tenA[idx];
        }
        k-=len;
        int nums=k/idx;
        int cur=tenB[idx-1]+nums;
        ostringstream oss;
        oss<<cur;
        cout<<oss.str()[k%idx]<<endl;
    }
}
*/

/*
 * brute force
 */
const int INF=1e10;
void solve() {
    vi seq1(1);   // seq1[i]=idx of start of i in 123456789101112...
    vi seq2(1);   // seq2[i]=idx of start of ith seq1

    int idx=0;
    int idx2=0;
    int num=1;
    int len;

    while(idx2<INF) {
        if(num>=1e8)
            len=9;
        else if(num>=1e7)
            len=8;
        else if(num>=1e6)
            len=7;
        else if(num>=1e5)
            len=6;
        else if(num>=1e4)
            len=5;
        else if(num>=1e3)
            len=4;
        else if(num>=1e2)
            len=3;
        else if(num>=10)
            len=2;
        else 
            len=1;

        seq1.push_back(idx);
        seq2.push_back(idx2);
        idx+=len;
        idx2+=idx;
        num++;
    }
    //cerr<<"seq1.size()="<<seq1.size()<<" num="<<num<<endl;

    cini(q);
    while(q--) {
        cini(k);
        auto it=lower_bound(all(seq2), k);
        it--;
        k-=*it;

        it=lower_bound(all(seq1), k);
        it--;
        int i=distance(seq1.begin(), it);

        ostringstream oss;
        oss<<i;
        //cerr<<"num="<<oss.str()<<" i="<<i<<" k="<<k<<" *it="<<*it<<endl; 
        cout<<oss.str()[k-*it-1]<<endl;
    }
}

signed main() {
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
