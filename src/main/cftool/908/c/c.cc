/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we check O(n^2) where the disks stop.
 *
 * If two disk overlap, then the second disc stops at
 * a point dy above the first y.
 * let dx=abs(x1-x2), then
 * dx*dx+dy*dy=4*r*r
 * dy=sqrt(4*r*r-dx*dx)
 */
void solve() {
    cini(n);
    cini(r);
    
    cout<<setprecision(10)<<fixed;
    vector<pair<int,double>> d;  /* disks so far, <xCenter,yCenter> */
    for(int i=0; i<n; i++) {
        cini(x);
        pair<int,double> ans={ x, r};
        for(auto p : d) {
            int dx=abs(p.first-x);
            if(dx<=2*r) {
                double dy=sqrt(4*r*r-dx*dx);
                if(p.second+dy > ans.second)
                    ans.second=p.second+dy;
            }
        }
        d.push_back(ans);
        cout<<ans.second<<" ";
    }
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

