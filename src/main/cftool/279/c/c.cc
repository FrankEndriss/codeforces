/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * Note that single elements are allways ladders.
 * two elements are allways ladders, too.
 *
 * The only thing a ladder must not contain is
 * an up after an down.
 *
 * So, we need to know for every position:
 * -where is next down
 * -where is next up
 *
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    cinai(a,n);

    vi u(n, INF);
    vi d(n, INF);

    stack<int> stu;
    stack<int> std;

    int prev=a[0];
    for(int i=0; i<n; i++)  {
        if(a[i]<prev) {
            while(std.size()) {
                d[std.top()]=i;
                std.pop();
            }
        } else if(a[i]>prev) {
            while(stu.size()) {
                u[stu.top()]=i;
                stu.pop();
            }
        }

        prev=a[i];
        stu.push(i);
        std.push(i);
    }

    for(int i=0; i<m; i++) {
        cini(l); l--;
        cini(r); r--;

        int nextD=d[l];
        if(nextD>=r || u[nextD]>r) 
            cout<<"Yes"<<endl;
        else
            cout<<"No"<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
