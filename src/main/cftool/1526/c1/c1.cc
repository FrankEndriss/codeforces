/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


#include <atcoder/lazysegtree>
using namespace atcoder;


using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}

/* This is the neutral element */
const S INF=1e18;
S st_e() {
    return INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return x+f;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/* We drink all positives.
 * Each negative one can be drunk or not drunk.
 *
 * A negative a[i] can be used if the sum before it sum>=abs(a[i])
 * Drink (and check) the negs in non ascending order, smallest abs(a[i]) first?
 *
 * Consider the positives, they build a prefix sum.
 * Use lazy segment tree with min to maintain the prefix sums?
 */
void solve() {
    cini(n);
    cinai(a,n);
    vi b(n);
    b[0]=max(0LL,a[0]);
    for(int i=1; i<n; i++)
        b[i]=b[i-1]+max(a[i], 0LL);

    stree seg(b);

    vector<pii> neg;
    for(int i=0; i<n; i++) {
        if(a[i]<0)
            neg.emplace_back(a[i], i);
    }
    sort(all(neg), greater<pii>());

    int ans=0;
    for(size_t i=0; i<neg.size(); i++) {
        if(seg.prod(neg[i].second,n)+neg[i].first>=0) {
            ans++;
            seg.apply(neg[i].second, n, neg[i].first);
        }
    }
    cout<<n-(neg.size()-ans)<<endl;

}

signed main() {
    solve();
}
