/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


/* find if there is an articulation point in a component.
 * see https://cp-algorithms.com/graph/cutpoints.html */
vector<vector<int>> adj;
bool found;

vector<bool> visited;
vector<int> tin, low;
int timer;

void dfs(int v, int p = -1) {
    visited[v] = true;
    tin[v] = low[v] = timer++;
    int children=0;
    for (int to : adj[v]) {
        if (to == p)
            continue;
        if (visited[to]) {
            low[v] = min(low[v], tin[to]);
        } else {
            dfs(to, v);
            low[v] = min(low[v], low[to]);
            if (low[to] >= tin[v] && p!=-1) {
                /* cerr<<"found, v="<<v<<endl; */
                found=true;
            }
            ++children;
        }
    }
    if(p == -1 && children > 1) {
        /* cerr<<"found, v="<<v<<endl; */
        found=true;
    }
}

void find_cutpoints() {
    timer = 0;
    visited.assign(adj.size(), false);
    tin.assign(adj.size(), -1);
    low.assign(adj.size(), -1);
    for (size_t i = 0; i < adj.size(); ++i) {
        if (!visited[i])
            dfs (i);
    }
}

/*
 * There are 4 key fields, the two next to start and
 * the two next to treasure.
 * So with 2 blocks we can always block all paths.
 *
 * But we need to check if there exists a single cell which blocks
 * all possible paths!
 * How?
 *
 * We need to dfs twice, once from start, once from end, so that
 * the intersection of both is the set of cells in the path.
 *
 * Then two possible solutions:
 * 1. Find articulation point in the path.
 * 2. Store distance of every cell in dp. Then for each distance
 * there must exist two cells, else that cell is articualtion point.
 *
 * For soltion 1. see
 * https://cp-algorithms.com/graph/cutpoints.html
 * This works in O(n+m)
 */
void solve() {
    cini(n);
    cini(m);
    cinas(s,n);

    vi dx= {1,0};
    vi dy= {0,1};

    queue<pii> q;
    q.push({0,0});
    vvb dp(n, vb(m));
    dp[0][0]=true;

    while(q.size()) {
        const pii p=q.front();
        q.pop();
        for(int i=0; i<2; i++) {
            const int ni=p.first+dx[i];
            const int mi=p.second+dy[i];
            if( ni<n && mi<m) {
                if(s[ni][mi]=='.' && !dp[ni][mi]) {
                    dp[ni][mi]=true;
                    q.push({ni,mi});
                }
            }
        }
    }

    if(!dp[n-1][m-1]) { // there is no path
        cout<<0<<endl;
        return;
    }

    /* and again, fill dp2 this time */
    vvb dp2(n, vb(m));
    dp2[n-1][m-1]=true;
    q.push({n-1,m-1});

    while(q.size()) {
        const pii p=q.front();
        q.pop();
        for(int i=0; i<2; i++) {
            const int ni=p.first-dx[i];
            const int mi=p.second-dy[i];
            if( ni>=0 && mi>=0) {
                if(s[ni][mi]=='.' && !dp2[ni][mi]) {
                    dp2[ni][mi]=true;
                    q.push({ni,mi});
                }
            }
        }
    }


    /* we need to map i,j coordinates to vertex numbers starting at 0 */
    int gid=0;
    vvi vid(n, vi(m, -1));
    function<int(int,int)> getid=[&](int i, int j) {
        if(vid[i][j]>=0)
            return vid[i][j];
        else
            return vid[i][j]=gid++;
    };

    adj.resize(n*m);
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            if(dp[i][j] && dp2[i][j]) {
                for(int k=0; k<2; k++) {
                    int ni=i+dx[k];
                    int nj=j+dy[k];
                    if(ni<n && nj<m && dp[ni][nj] && dp2[ni][nj]) {
                        adj[getid(i,j)].push_back(getid(ni,nj));
                        adj[getid(ni,nj)].push_back(getid(i,j));
                    }
                }
            }
        }

    adj.resize(gid);

    found=false;
    find_cutpoints();

    if(found)
        cout<<1<<endl;
    else
        cout<<2<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
