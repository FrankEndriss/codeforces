/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int q(int idx) {
    cout<<"? "<<idx<<endl;
    int ans;
    cin>>ans;
    return ans;
}

/*
vi test={ 1, 3, 2, 4, 5 };
int q(int idx) {
    cerr<<"? "<<idx<<endl;
    return test[idx-1];
}
*/

/* Fucking casework...
 * Consider 3 indexes.
 * If middle is smallest, find mid of left half, then
 * that mid is smallest, so move all to left, or
 * that mid is bigger than m, the l=that mid.
 * Do same on right side.
 *
 * if left is smallest, move r to middle, and half
 * the bigger segment left or right of l.
 * if right is smallest, mor l to middle, and half
 * the bigger segment left or right of r.
 *
 */
const int INF=1e9;
void solve() {
    cini(n);

    vi a(n+2, -1);
    a[0]=INF;
    a[n+1]=INF;
    int l=0;
    int r=n+1;

    int m=(n+1)/2;
    a[m]=q(m);

    /* invariant is that m is smallest element of the three l,m,r */
    while(l+1<m || m+1<r) {
        assert(a[l]>a[m] && a[r]>a[m]);
        if(l+1<m) {
            int mid=(l+m)/2;
            a[mid]=q(mid);

            if(a[mid]>a[m]) {
                l=mid;
            } else {
                r=m;
                m=mid;
            }
        } else {
            assert(m+1<r);
            int mid=(m+r)/2;
            a[mid]=q(mid);
            if(a[mid]>a[m]) {
                r=mid;
            } else {
                l=m;
                m=mid;
            }
        }

    }
    cout<<"! "<<m<<endl;
}

signed main() {
    solve();
}
