/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to fetch all edgecases.
 * We want to fight the monster with strongest attack as last monster.
 */
void solve() {
    cini(A);
    cini(B);
    cini(n);
    vector<pii> ab(n);
    for(int i=0; i<n; i++)
        cin>>ab[i].first;
    for(int i=0; i<n; i++)
        cin>>ab[i].second;
    sort(all(ab));

    for(int i=0; i<n; i++) {
        if(B<=0) {
            cout<<"NO"<<endl;
            return;
        }

        int cnt=(ab[i].second+A-1)/A;    /* number of fights to kill current monster */

        B-=(ab[i].first*cnt);
        if(B+ab[i].first<=0) {
            cout<<"NO"<<endl;
            return;
        }
    }
    cout<<"YES"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
