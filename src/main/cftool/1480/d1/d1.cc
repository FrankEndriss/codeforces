/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Lets maximize a0 by using one number from each 
 * block of numbers in a.
 * Then maximize a1 by using one number from each
 * remaining block in a.
 *
 * Can this be sub optimum?
 * Consider 
 * 1 1 2 3 1 1 2 1 1
 * a0=1 2 3 1 2 1
 * a1=1
 *
 * Or better:
 * 1 2 1 2 1
 * 1 3 1
 * Ok, we need to use the single ones as splitters for
 * the other ones, ie but them alternating...or somehow.
 * How do we know which element belongs to which seq?
 * -> Put all double elements on both seqs.
 * -> Else we can move the last one on one seq to the other seq.
 *
 * What about a dp?
 * We would maintain a list of possible endings of a0 and a1, together
 * with the max number of elements...
 * But that is O(n^2) states.
 */
void solve() {
    cini(n);
    cinai(a,n);

    /* first compress the array */
    vector<pii> v1;
    v1.emplace_back(a[0], 0);
    for(int i=0; i<n; i++) {
        if(v1.back().first==a[i])
            v1.back().second++;
        else
            v1.emplace_back(a[i], 1);
    }

    vi a0;
    vi a1;
    for(size_t i=0; i<v1.size(); i++) {
        int a0back=a0.size()==0?-1:a0.back();
        int a1back=a1.size()==0?-1:a1.back();
        if(v1[i].second>1) {

           if(a0back!=v1[i].first && a1back!=v1[i].first) {
               a0.push_back(v1[i].first);
               a1.push_back(v1[i].first);
           } else if(a0back==v1[i].first && a1back!=v1[i].first) {
                if(a1.size()>0 && a1.back()!=a0back) {
                    a0.push_back(a1.back());
                    a1.pop_back();
                    a0.push_back(v1[i].first);
                    if(a1.size()==0 || a1.back()!=v1[i].first)
                        a1.push_back(v1[i].first);
                }
           } else if(a1back==v1[i].first && a0back!=v1[i].first) {
                if(a0.size()>0 && a0.back()!=a1back) {
                    a1.push_back(a0.back());
                    a0.pop_back();
                    a1.push_back(v1[i].first);
                    if(a0.size()==0 || a0.back()!=v1[i].first)
                        a0.push_back(v1[i].first);
                }
           }
        } else {
            if(v1[i].first!=a0back)
                a0.push_back(v1[i].first);
            else if(v1[i].first!=a1back)
                a1.push_back(v1[i].first);
        }
    }

    int ans=a0.size()+a1.size();
    cout<<ans<<endl;
}

signed main() {
    solve();
}
