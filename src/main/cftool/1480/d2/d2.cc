/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * What about the minimum?
 * 1 2 1 2
 * a0=1 1
 * a1=2 2
 * ...
 * idk
 */
void solve() {
    cini(n);
    cinai(a,n);

    /* first compress the array */
    vector<pii> v1;
    v1.emplace_back(a[0], 0);
    for(int i=0; i<n; i++) {
        if(v1.back().first==a[i])
            v1.back().second++;
        else
            v1.emplace_back(a[i], 1);
    }

    vector<pii> v2;
    for(size_t i=0; i<v1.size(); i++) {
        if(v1[i].second>1) 
            v2.emplace_back(v1[i].first, v1[i].second-1);
    }
    
    /* compress v2 */
    vector<pii> v3;
    if(v2.size()>0) {
        v3.emplace_back(v2[0].first, 0);
        for(size_t i=0; i<v2.size(); i++) {
            if(v3.back().first==v2[i].first)
                v3.back().second+=v2[i].second;
            else
                v3.emplace_back(v2[i].first, v2[i].second);
        }
    }
    int ans=v1.size()+v3.size();
    cout<<ans<<endl;
}

signed main() {
    solve();
}
