/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to find the shortest path including all vertex.
 * How? It is traveling salesman. O(2^n*n)
 * ***
 * If we start anywhere ans greedy choose the nearest next to
 * min/max, maybe that works.
 * ... It does not :/
 * ******
 * Given the first runner, how to find the order of all others?
 * We may choose the next slower or faster one.
 * But as a dp that would be O(n^3)
 * ...is it really, or is it O(n^2)
 * State is min cost for interval (x,y)
 * -> It is O(n^2), since we start with dp[i][j]=0 for i==j
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));

    vvi dp(n, vi(n, INF));

    for(int i=0; i<n; i++)
        dp[i][i]=0;


    for(int sz=2; sz<=n; sz++) {
        for(int l=0; l+sz<=n; l++) {
            int r=l+sz-1;
            dp[l][r]=min(dp[l][r-1]+abs(a[r]-a[l]), dp[l+1][r]+abs(a[r]-a[l]));
        }
    }

    cout<<dp[0][n-1]<<endl;
}

signed main() {
    solve();
}
