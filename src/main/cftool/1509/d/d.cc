/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* we can output s1[n/2],s2 if second half of s1 is a subseq of s2
 */
bool issub2(string &s1, string &s2) {
    assert(s1.size()==s2.size());
    size_t i1=s1.size()/2;
    for(size_t i=0; i1<s1.size() && i<s2.size(); i++) {
        if(s2[i]==s1[i1])
            i1++;
    }
    return i1>=s1.size();

}
/* We want to output s2,s1 and can do so
 * if the first half of s1 is a subseq of s2.
 */
bool issub(string &s1, string &s2) {
    assert(s1.size()==s2.size());
    size_t i1=0;
    for(size_t i=0; i<s2.size(); i++) {
        if(s2[i]==s1[i1])
            i1++;
    }
    return i1*2>=s1.size();
}

/*
 * find if first half of s[0] is substring of s[1] or s[2],
 * and same for the other pairs.
 * If there is such a pair, then choose those two, else no ans.
 * ***
 * we output s1[0,i], s1[i+1,2n], s2[j,2n]
 * where the middle part contains s2[0,j-1] as a subseq.
 * So, the middle part must be at least of size n.
 * ...
 * Some substring of len n of s1 must be subseq of s2.
 *
 */
void solve() {
    cini(n);
    cinas(s,3);

    /*
        for(int j=0; j<3; j++) {
            s[j]=string(t,' ');
            for(int i=0; i<t; i++) {
                s[j][i]=(char)('0'+(rand()%2));
            }
        }
        */

    bool ok=false;
    for(int i=0; i<3; i++) {
        for(int j=0; j<3; j++) {
            if(i==j)
                continue;

            if(issub(s[i], s[j])) {
                cout<<s[j];
                cout<<s[i].substr(s[i].size()/2);
                cout<<endl;
                return;
            }
            if(issub2(s[i], s[j])) {
                cout<<s[i].substr(0,s[i].size()/2);
                cout<<s[j];
                cout<<endl;
                return;
            }

        }
    }

    assert(false);

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
