/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* greedy
 * try decrease first digit by one, and increase all other to 9
 * else use n.
 */
void solve() {
    cini(n);
    vi d;

    if(n==1) {
        cout<<1<<endl;
        return;
    }

    vector<vi> ans; /* ans[i]=<dsum,ans> */
    int nn=n;
    int ans1=0;
    while(nn) {
        ans1+=nn%10;
        d.push_back(nn%10);
        nn/=10;
    }
    reverse(d.begin(), d.end());    /* biggest number first */
    ans.push_back(d);

    for(int i=0; i<d.size(); i++) {
        vi dd=d;
        if(i==0 || dd[i]>1) {
            dd[i]--;
            for(int j=i+1; j<d.size(); j++)
                dd[j]=9;
        }
        ans.push_back(dd);
    }

    sort(ans.begin(), ans.end(), [&](vi &a1, vi a2) {
        int s1=0;
        int s2=0;
        for(int i=0; i<a1.size(); i++) {
            s1+=a1[i];
            s2+=a2[i];
        }
        if(s1!=s2)
            return s2<s1;
        else
            return a2<a1;
    });

    bool first=true;
    for(int i=0; i<ans[0].size(); i++) {
        if(first && ans[0][i]==0)
            continue;
        first=false;
        cout<<ans[0][i];
    }
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS, SOLVE ONE AFTER THE OTHER
