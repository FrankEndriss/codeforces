/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vvd= vector<vd>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Let p(i,t)=prop that after t seconds there are i people in the box
 * Expected number of people in box after t seconds
 * E(t)= p(0,t)*0 + p(1,t)*1 + p(2,t)*2 + ... + p(n,t)*n
 *
 * So lets dp the probabilities.
 */
void solve() {
    cini(n);
    cind(p);
    cini(t);

    vvd dp(n+1, vd(t+1));
    dp[0][0]=1;

    for(int i=0; i<t; i++) {
        for(int j=0; j<=n; j++) {
            if(j==n) {
                dp[j][i+1]+=dp[j][i];
            } else {
                dp[j][i+1]+=dp[j][i]*(1-p);
                dp[j+1][i+1]+=dp[j][i]*p;
            }
        }
    }

    ld ans=0;
    for(int j=1; j<=n; j++) 
        ans+=dp[j][t]*j;

    cout<<ans<<endl;
}

signed main() {
        solve();
}
