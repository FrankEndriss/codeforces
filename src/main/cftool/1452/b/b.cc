/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* each boxs number must be divi by n-1.
 * no...
 *
 * We need to have y*(n-1) blocks in each box.
 * And it must be at least a[n-1] in each box.
 * no...
 *
 * The sum of all blocks is at least
 * a[n-1]*(n-1)
 */
void solve() {
    cini(n);
    vi a(n);
    int sum=0;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        sum+=a[i];
    }
    int ma=*max_element(all(a));
    int ssum=ma*(n-1);

    int ans=ssum-sum;
    while(ans<0)
        ans+=(n-1);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
