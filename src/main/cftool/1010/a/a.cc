/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* to land m tons we need y tons of fuel. so
 * m/(b[i]-1)==y
 */
void solve() {
    cini(n);
    cind(m);
    cinai(a, n);
    cinai(b, n);

    for(int i=0; i<n; i++) {
        if(a[i]<=1 || b[i]<=1) {
            cout<<-1<<endl;
            return;
        }
    }

    double ans=m;       // current weight

    /* land on earth */
    ans+=ans/(b[0]-1);


    for(int i=n-1; i>=0; i--) {
        /* start on planet i */
        ans+=ans/(a[i]-1);

        if(i>0) {
            /* land on planet i */
            ans+=ans/(b[i]-1);
        }
    }

    ans-=m;
    cout<<setprecision(12)<<fixed<<ans<<endl;
    
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

