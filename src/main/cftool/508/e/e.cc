#include <iostream>
#include <stack>
#include <vector>

using namespace std;

const int MAXN = 610;


/*
 * It seems that it is always optimal to close open brackets
 * as soon as possible.
 */
const int N=603;
void solve() {
    vector<int> p(N);
    vector<int> l(N);
    vector<int> r(N);

    int n;
    cin>>n;

    stack<int> st;
    string ans;

    for (int i=0; i<n; i++) {
        cin>>l[i]>>r[i];
        st.push(i);
        p[i]=(int)ans.size();
        ans+='(';

        while (!st.empty() && p[st.top()] + l[st.top()] <= ans.size()) {
            if (p[st.top()] + r[st.top()] < ans.size()) {
                cout<<"IMPOSSIBLE"<<endl;
                return;
            }
            ans+=')';
            st.pop();
        }
    }

    if (st.size())
        cout << "IMPOSSIBLE" << endl;
    else
        cout<<ans<<endl;

}

int main() {
    solve();
}
