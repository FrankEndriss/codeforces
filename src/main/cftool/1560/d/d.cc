/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Since ans has 30 bits it is allways possible to find a seq with
 * less than 60 steps. So brute force?
 *
 * Consider all powers of 2. Foreach x
 * create a prefix of x by removing digits.
 * add all remaining digits.
 * ans=min of all those tries.
 */
const int N=1e18+7;
void solve() {
    cini(n);
    vi dn;  /* digits of n */
    while(n>0) {
        dn.push_back(n%10);
        n/=10;
    }
    reverse(all(dn));  /* left to right */

    int ans=N;
    int kk=1;
    while(kk<N) {
        vi dkk; /* digits of kk */
        int kkk=kk;
        while(kkk>0) {
            dkk.push_back(kkk%10);
            kkk/=10;
        }
        reverse(all(dkk));  /* left to right */

        /* now find the longest prefix of dkk in dn */
        int idxKK=0;
        for(size_t i=0; i<dn.size(); i++) {
            if(idxKK<dkk.size() && dn[i]==dkk[idxKK])
                idxKK++;
        }
        int lans=dn.size()-idxKK;    /* number of removes */
        lans+=dkk.size()-idxKK;  /* number of adds */
        ans=min(ans, lans);

        kk*=2;
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
