/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int N=1e5+7;
//vi dp(N);  /* number of all numbers, i*i */
vi dp0(N); /* first number */
vi dp1(N); /* number of numbers starting at that col */

void init() {
    dp0[0]=1; dp0[1]=2;
    dp1[0]=1; dp1[1]=3;

    for(int i=3; i<=N; i++) {
        dp1[i-1]=i*2-1;
        dp0[i-1]=((i-1)*(i-1))+1;
    }
}

/**
 */
void solve() {
    cini(k);

    if(k==1)
        cout<<"1 1"<<endl;
    else if(k==2)
        cout<<"1 2"<<endl;
    else if(k==3)
        cout<<"2 2"<<endl;
    else if(k==4)
        cout<<"2 1"<<endl;
    else {
        auto it=upper_bound(all(dp0), k);
        int idx=distance(dp0.begin(), it)-1;    /* starting column */
        int ff=dp0[idx];                        /* first number in col */
        //cerr<<"k="<<k<<" idx="<<idx<<" ff="<<ff<<endl;
        assert(ff<=k);
        int cnt=dp1[idx]; /* number of numbers in col */
        assert(ff+cnt>k);

        int ansR=1;
        int rDiff=min(cnt/2, k-ff);
        ansR+=rDiff;
        ff+=rDiff;
        int ansC=idx+1;
        if(ff<k) {
            ansC+=ff;
            ansC-=k;
        }

        cout<<ansR<<" "<<ansC<<endl;
    }
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
