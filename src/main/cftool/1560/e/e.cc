/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


struct state {
    int len, link;
    map<char, int> next;

    int firstpos;
    bool is_clone;
    vector<int> inv_link;
};

/* see https://cp-algorithms.com/string/suffix-automaton.html */
struct SuffixAutomaton {
    int MAXLEN;
    vector<state> st;
    int sz, last;

    SuffixAutomaton(string str, int maxlen=-1) {
        MAXLEN=2*max((int)str.size(), maxlen);
        st=vector<state>(MAXLEN);
        init();
        for(char c : str) 
            extend(c);
    }

    /** init st to be the minimal suffix automaton, having only one state. */
    void init() {
        st[0].len = 0;
        st[0].link = -1;
        sz=1;
        last = 0;
    }

    /** Add a char to the automaton. */
    void extend(char c) {
        int cur = sz++;
        st[cur].len = st[last].len + 1;
        st[cur].firstpos=st[cur].len-1;
        st[cur].is_clone=false;
        int p = last;
        while (p != -1 && !st[p].next.count(c)) {
            st[p].next[c] = cur;
            p = st[p].link;
        }
        if (p == -1) {
            st[cur].link = 0;
        } else {
            int q = st[p].next[c];
            if (st[p].len + 1 == st[q].len) {
                st[cur].link = q;
            } else {
                int clone = sz++;
                st[clone].len = st[p].len + 1;
                st[clone].next = st[q].next;
                st[clone].link = st[q].link;
                st[clone].firstpos=st[q].firstpos;
                st[clone].is_clone=true;
                while (p != -1 && st[p].next[c] == q) {
                    st[p].next[c] = clone;
                    p = st[p].link;
                }
                st[q].link = st[cur].link = clone;
            }
        }
        last = cur;
    }

    /** @return len of longest prefix of p contained in s. 
     * @return node the node of the found state.
     * if len==p.size(), p is contained in s. */
    size_t prefix_len(string &p, int &node) {
        node=0;
        size_t pidx=0;
        while(pidx<p.size()) {
            auto it=st[node].next.find(p[pidx]);
            if(it==st[node].next.end())
                return pidx;
            node=it->second;
            pidx++;
        }
        return pidx;
    }

    /** checks if p is contained in s */
    bool contains(string &p) {
        int aux;
        return p.size()==prefix_len(p, aux);
    }

    int firstpos(string &p) {
        int v;
        int len=prefix_len(p, v);
        if(len==(int)p.size())
            return st[v].firstpos-p.size()+1;
        else 
            return -1;
    }

    /* Must not be called more than once. Creates
     * the inverted links of the states. */
    void collect_inv_link() {
        for(int v=1; v<sz; v++) 
            st[st[v].link].inv_link.push_back(v);
    }

    void all_occurences(const int v, const int P_length, vi &ans) {
        if(!st[v].is_clone)
            ans.push_back(st[v].firstpos - P_length + 1);
        for(int u : st[v].inv_link) 
            all_occurences(u, P_length, ans);
    }

    /* collect_inv_link() must be called before this function. 
     * @return push_back all indexes where p is contained to ans */
    void all_occurences(string &p, vi &ans) {
        ans.clear();
        int v;
        int len=prefix_len(p, v);
        if(len!=(int)p.size())
            return;

        all_occurences(v, p.size(), ans);
    }

    int cntDistinctSubstr(int node, vi &memo) {
        if(memo[node]>=0)
            return memo[node];
        int ans=1;
        for(auto ent : st[node].next)
            ans+=cntDistinctSubstr(ent.second, memo);

        return memo[node]=ans;
    }

    /* Count distinct substrings */
    int cntDistinctSubstr() {
        vi memo(MAXLEN, -1);
        return cntDistinctSubstr(0, memo)-1;
    }

};

/**
 * s is a prefix of t.
 * There is exactly one letter in that prefix that does
 * not occur in the later string, that is the first removed letter.
 * How to find that one letter, and the length of the prefix?
 *
 * Find the set of letters in t, and the idx of last occ.
 * The min of these idx is the first removed letter.
 * Then find, starting at idx of removed letter+1, the prefix.
 * If found then we got s.
 * Then find one letter after the other by iterating t.
 * ...
 * Rethink the implementation.
 * Once we got s0, then we do 
 * t=t-s0   // remove prefix
 * find first mismatch t[i]!=s0[i]
 *
 * How to avoid TLE?
 * Use a SuffixAutomaton?
 * -> to complecated :/
 *
 *****
 * The simple solution is to work from right to left, 
 * t.back() is last letter remove, and so on.
 */
const int INF=1e9;
void solve() {
    cins(t);
    cerr<<"t="<<t<<endl;

    SuffixAutomaton sa(t);

    vi idx(26,INF);
    for(size_t i=0; i<t.size(); i++)
        idx[t[i]-'a']=i;

    string s0;
    string s1;  /* s without first removed letter */
    vector<char> ans;

    int mi=*min_element(all(idx));
    for(int j=0; j<26; j++) {
        if(mi==idx[j]) {    /* 'a'+j is first removed letter */
            cerr<<"first removed="<<(char)('a'+j)<<endl;
            for(int k=0; s0.size()==0 && k<t.size(); k++) {
                if(k>idx[j]) {  /* check if current s1 matches at position t[k] */
                    int fpos=sa.firstpos(s1);
                    if(fpos==k || fpos==-1) {
                        s0=t.substr(0,k);
                    }
                } 
                if(t[k]!='a'+j)
                    s1+=t[k];
            }
        }
    }

    cerr<<"s0="<<s0<<endl;
    if(s0.size()==0) {
        cout<<-1<<endl;
    } else {
        string ansS0=s0;
        int toffs=0;
        while(toffs<t.size()) {
            bool ok=true;
            for(size_t i=0; i<s0.size(); i++) 
                if(i+toffs>=t.size() || t[i+toffs]!=s0[i])
                    ok=false;

            if(!ok) {
                cout<<-1<<endl;
                return;
            }

            toffs+=s0.size();

            for(size_t i=0; i<s0.size(); i++) {
                if(toffs+i>t.size() || t[toffs+i]!=s0[i]) {
                    ans.push_back(s0[i]);
                    break;
                }
            }

            string s;
            for(int i=0; i<s0.size(); i++) 
                if(s0[i]!=ans.back())
                    s+=s0[i];

            s0.swap(s);
        }

        cout<<ansS0<<" ";
        for(char c : ans) {
            cout<<c;
            cerr<<c<<endl;
        }
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
