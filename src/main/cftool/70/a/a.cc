/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MOD=1e6+3;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while (res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}


int gaus(int i) {
    if(i%2==0) 
        return mul(i/2, i+1);
    else
        return mul(i, (i+1)/2);
}

/* there are 2^(2*n) cells.
 * in first iteration cookie of k=2^n is placed, which has 2^n*(2^n+1)/2 cells.
 * 2nd iter 1 c of k=2^(n-1) is placed.
 * 3rd iter 2 c of k=2^(n-2) are placed.
 */
void solve() {
    cini(n);

    if(n<2) {
        cout<<1<<endl;
        return;
    }

    int ans=toPower(3, n-1);
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

