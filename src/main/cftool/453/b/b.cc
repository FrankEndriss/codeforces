/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int NN=44;

int vertex(int val, int col) {
    return col*NN+val;
}
int val(int vertex) {
    return vertex%NN;
}

/*
 * We create a set of primenumbers from 2 up to 60.
 * Turns out the size of this set is 17.
 *
 * Then we create a bitset of the primefactors for 
 * all numbers from 1 up to 60.
 *
 * Then we create a dp[pos][s] =
 * min diff at after pos numbers if used s primefactors.
 *
 * Then trackback ans from dp-table.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi pr;
    for(int i=2; i<60; i++) {
        int ispr=true;
        for(int j=2; ispr && j<i; j++) {
            if(i%j==0)
                ispr=false;
        }
        if(ispr)
            pr.push_back(i);
    }
    assert(pr.size()==17);

    vi pf(61);
    for(size_t i=1; i<pf.size(); i++) {
        int ii=i;
        int s=0;
        for(size_t j=0; j<pr.size(); j++) {
            while(ii%pr[j]==0) {
                ii/=pr[j];
                s|=(1LL<<j);
            }
        }
        pf[i]=s;
    }

    const int INF=1e9;
    const int S=1LL<<pr.size();
    vvi dp2(n, vi(1<<pr.size(), INF));
    vvi dp(n, vi(1<<pr.size(), INF));
    for(int i=1; i<=60; i++)  {
        int nval=abs(a[0]-i);
        if(dp[0][pf[i]]>nval) {
            dp[0][pf[i]]=nval;
            dp2[0][pf[i]]=i;
        }
    }

    for(int j=1; j<n; j++) {
/* todo loop over all unused bitsets */
        for(int i=1; i<=60; i++)  {
            int mask=(~pf[i]) & (S-1);
            for(int k=mask; ; k=((k-1)&mask)) {
                if(dp[j-1][k]==INF)
                    continue;

                int nval=dp[j-1][k]+abs(a[j]-i);
                if(dp[j][k|pf[i]]>nval) {
                    dp [j][k|pf[i]]=nval;
                    dp2[j][k|pf[i]]=i;
                }
                if(k==0)
                    break;
            }
        }
    }

    vi ans(n);
    auto it=min_element(all(dp[n-1]));
    int prevs=distance(dp[n-1].begin(), it);
    ans[n-1]=dp2[n-1][prevs];

    for(int i=n-2; i>=0; i--) {
//cerr<<"prevs="<<bitset<17>(prevs)<<" ans[i+1]="<<ans[i+1]<<" pf[ans[i+1]]="<<bitset<17>(pf[ans[i+1]])<<endl;
        assert((prevs&pf[ans[i+1]])==pf[ans[i+1]]);
        prevs^=pf[ans[i+1]];
        ans[i]=dp2[i][prevs];
    }

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
