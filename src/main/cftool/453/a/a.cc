/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Expected number of max dots eq
 * sum(i * numberOfWaysMaxIs(i)/numberOfAll)
 *
 * So, how to find the prob that max is i?
 * Consider foreach number ma=1..m that the first
 * toss shows that number (with 1/m prop), and
 * all other tosses show ma or less.
 */
void solve() {
    cini(m);    /* m faces */
    cini(n);    /* n tosses */

    double ans=pow(1.0/m, n);   /* prop that all tosses show 1 */
    double prev=ans;            /* for next ma, prop that tosses are less than ma */
    for(int ma=2; ma<=m; ma++) {
        double p=pow((double)ma/m, n);  /* prop that all tossses are ma or less */
        ans+=(p-prev)*ma;   
        prev=p;
    }
    cout<<ans<<endl;

}

signed main() {
        solve();
}
