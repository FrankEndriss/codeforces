/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we need to find a cirlce
 * where ABC are on it, and
 * distance from A to B is same as B to C
 * So, if two distances are same
 * then there is a solution.
 * NOTE if all three points are on one line, there is none.
 */
void solve() {
    cinai(a,6);
    vi d(3);
    d[0]=(a[0]-a[2])*(a[0]-a[2]) + (a[1]-a[3])*(a[1]-a[3]);
    d[1]=(a[2]-a[4])*(a[2]-a[4]) + (a[3]-a[5])*(a[3]-a[5]);
    d[2]=(a[0]-a[4])*(a[0]-a[4]) + (a[1]-a[5])*(a[1]-a[5]);

    int sl0x=a[2]-a[0];
    int sl0y=a[3]-a[1];
    int g=__gcd(sl0x, sl0y);
    sl0x/=g;
    sl0y/=g;

    int sl1x=a[4]-a[2];
    int sl1y=a[5]-a[3];
    g=__gcd(sl1x, sl1y);
    sl1x/=g;
    sl1y/=g;

    if(d[0]==d[1] && !(sl0x==sl1x && sl0y==sl1y))
        cout<<"Yes";
    else
        cout<<"No";

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

