/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Count pairs of bits.
 * start with lowest...somehow.
 *
 * We need to find the parity of sum of sums of two numbers.
 *
 * For first bit:
 * Consider b0 nums ending in 0, b1 in 1.
 * b0*b1
 *
 * Second bit
 * Consider number of numbers with endings
 * b00, b01, b10, b11
 * b00*b10 + b01*(b01-1) + b11*(b11-1)
 *
 * 2+xth bit
 * b00x, b01x, b10x, b11x
 * b00x*b10x + b01x*(b01x-1) + b11x*(b11x-1)
 *
 * ... turns out this is wrong.
 * assume 
 * 111
 * 001
 */
void solve() {
    cini(n);
    vvi b(4, vi(30));
    for(int i=0; i<n; i++) {
        cini(aux);

        if(aux&1)
            b[0][0]++;
        else
            b[2][0]++;

        int idx=1;
        while(aux) {
            b[aux&3][idx]++;
            aux>>=1;
            idx++;
        }
    }

    int ans=0;
    for(int i=0; i<30; i++) {
        int bit=((b[0][i]*(b[2][i]+b[3][i])) ^ (b[1][i]*(b[2][i]+b[1][i]-1)) ^ (b[3][i]*(b[3][i]-1)))&1;
        ans|=(bit<<i);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
