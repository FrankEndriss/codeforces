/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * First we need to find the number of bottels
 * by sorting a and summing volumes until sum(b[])
 * is reached.
 * let sumb=sum(b[])
 * let k=number of bottels
 *
 * Then we need to find the sets of k bottles with
 * sum(a[])>=sumb, and among those sets the one
 * with maximum of b[] in it.
 *
 * dp[j][k] = maximum amount of b in k bottels after considering current bottel and choosed j sum(a[])
 *
 * Then add one bottle after the other.
 * ans is max of dp[n-1][>=sumb]
 */
void solve() {
    cini(n);
    cinai(b,n);
    cinai(a,n);
    int sumb=accumulate(all(b), 0LL);

    vi aa=a;
    sort(all(aa),greater<int>());

    int k=0;
    int suma=0;
    while(k<n && suma<sumb)
        suma+=aa[k++];

    const int N=100*100+1;
    vvi dp(N, vi(k+1, -1));
    dp[0][0]=0;

    for(int i=0; i<n; i++) {
        for(int kk=k; kk>0; kk--) {
            for(int j=N-a[i]; j>=0; j--) {
                if(dp[j][kk-1]>=0)
                    dp[j+a[i]][kk]=max(dp[j+a[i]][kk], dp[j][kk-1]+b[i]);
            }
        }
    }

    int ans=0;
    for(int i=sumb; i<N; i++)
        ans=max(ans, dp[i][k]);

    cout<<k<<" "<<sumb-ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
