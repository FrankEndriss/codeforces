/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int query(vi& v) {
    cout<<"? "<<v.size()<<" ";
    for(int i : v)
        cout<<i<<" ";
    cout<<endl;
    cini(aux);
    return aux;
}

/*
 * pw is ordered k numbers.
 * All numbers except one are same as max(A[])
 * Obviously we need to binsearch the max 
 * value and the one other value.
 * How?
 *
 * We can ask our own subset of indices.
 * Ask once all indexes for max value.
 * Then use a mask to ask the subsets
 * with the bits set.
 *
 * What if the max(A[]) exists in more than one index?
 * If all answers are same max(A[]) exists in more
 * than one set hence the pw consists only of max(A[])
 * Else the pw has at some position the other selected
 * value.
 * All queries return max two different values.
 */
void solve() {
    cini(n);
    cini(k);
    vvi s(k+1);
    for(int i=1; i<=k; i++) {
        cini(c);
        for(int j=1; j<=c; j++) {
            cini(aux);
            s[i].push_back(aux);
        }
    }

    vi ini(n);
    iota(ini.begin(), ini.end(), 1);
    int ma=query(ini);

    int ma2=-1;
    vi mabits;
    for(int i=0; (1<<i)<=k; i++) {
        vi vq;
        for(int j=1; j<=k; j++) {
            if(j&(1<<i)) {
                for(int l : s[j])
                    vq.push_back(l);
            }
        }
        int q=query(vq);
        if(q!=ma) {
  //          assert(ma2==-1 || ma2==q);
            ma2=max(ma2,q);
        } else 
            mabits.push_back(i);
    }

    if(ma2==-1) {
        cout<<"! ";
        for(int i=0; i<k; i++)
            cout<<ma<<" ";
        cout<<endl;
    } else {
        // which set for ma? construct somehow from mabits...
        int idx=0;
        for(int b : mabits)
            idx|=(1<<b);

        cout<<"! ";
        for(int i=1; i<=k; i++) {
            if(i==idx)
                cout<<ma2<<" ";
            else
                cout<<ma<<" ";
        }
        cout<<endl;
    }
    cins(saux);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
