/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* dp
 * we can allways flip one of the three.
 *
 * SUBSEQ not SUBSTRING!
 * String must be of form (a1) 1111000 or (a2) 0001111
 */
void solve() {
    cins(s);

    vi dp0(s.size()+1);
    vi dp1(s.size()+1);
    for(size_t i=0; i<s.size(); i++) {
        dp0[i+1]=dp0[i];
        if(s[i]=='0')
            dp0[i+1]++;
        dp1[i+1]=dp1[i];
        if(s[i]=='1')
            dp1[i+1]++;
    }


    int ans=1e9;
    for(size_t i=0; i<s.size(); i++) {
/* number of 0s left of i + number of 1s right of i */
        int a1=dp0[i]+ dp1[s.size()]-dp1[i];
        int a2=dp1[i]+ dp0[s.size()]-dp0[i];
        ans=min(ans, min(a1,a2));
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
