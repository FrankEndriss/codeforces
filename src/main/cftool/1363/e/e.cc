/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * One solution is to shuffle the tree at root, fixing
 * all nodes with wrong value.
 * If this does not work then there is no sol.
 * So basically b[] must be a permutation of c[] to have a sol.
 *
 * Then, there might be groups of nodes being shuffeable in a 
 * subtree, with less cost than shuffling them throu root.
 *
 * Note that to make a node v correct value, we need to shuffle
 * the subtree including both positions, v and u 
 * where the fitting value c[v]==b[u]
 *
 * Viewed as a matching prob it is that each position v can be matched
 * with each position u if c[v]==b[u], and the cost of that 
 * match is min(a[lca(v,u)]..a[root])*2.
 * Or we can match a group of some vertex U such that the
 * list(b[U])==list(c[U]) for the cost of 
 * size(U)*min(a[lca(U)]..a[root])
 * Note that in that case such a group is independent of each other group.
 *
 * Note that a[i] and b[i] all are { 0, 1 }
 * So it never makes sense to fix only one of a set of two
 * shuffling nodes. Whatever we shuffle, the cost is allways the number of
 * shuffled nodes, and it does not make sense to shuffle anything else as
 * a set of nodes where afterwards all are fixed.
 *
 * dfs, we maintain the cheapest parent node starting at root.
 * foreach subtree, we match all wrong 0s with all wrong 1s, and update
 * count of nodes, and min cost ans.
 */
const int INF=1e18;
void solve() {
    cini(n);

    vi a(n);
    vi b(n);
    vi c(n);

    for(int i=0; i<n; i++)
        cin>>a[i]>>b[i]>>c[i];

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    struct data {
        data():cnt0(0),cnt1(0),cost(0) {
        }
        int cnt0;     /* Os and 1s in wrong positions */
        int cnt1;
        int cost;         /* costs so far */
    };

    /* vertex v from parent p and min cost mi per swap */
    function<data(int,int,int)> dfs=[&](int v, int p, int mi) {
        mi=min(mi, a[v]);
        //cerr<<"dfs, v="<<v<<" p="<<p<<" mi="<<mi<<" adj[v].size()="<<adj[v].size()<<endl;
        data ldata;
        if(b[v]!=c[v]) {
            if(b[v]==1)
                ldata.cnt1++;
            else
                ldata.cnt0++;
        }

        for(size_t i=0; i<adj[v].size(); i++) {
            if(adj[v][i]!=p) {
                data cdata=dfs(adj[v][i], v, mi);
                ldata.cnt0+=cdata.cnt0;
                ldata.cnt1+=cdata.cnt1;
                ldata.cost+=cdata.cost;
            }
        }

        int swcnt=min(ldata.cnt0, ldata.cnt1);
        ldata.cost+=mi*swcnt*2;
        ldata.cnt0-=swcnt;
        ldata.cnt1-=swcnt;
        //cerr<<"swapped swcnt="<<swcnt<<" at v="<<v<<" cost/swap="<<mi<<endl;
        return ldata;
    };

    data adata=dfs(0, -1, INF);
    if(adata.cnt0!=0 || adata.cnt1!=0) {
        cout<<-1<<endl;
        return;
    }

    cout<<adata.cost<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
