/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* if x is leaf first wins.
 * else at some point x is the middle of three vertex,
 * and the moving player looses.
 * ie when number gets 3 is loose position.
 *
 * WA testcase 3
 * There must be some 'trick' in the defintion of the tree... sic :/
 */
void solve() {
    cini(n);
    cini(x);
    set<int> adjX;
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        if(u==x)
            adjX.insert(v);
        else if(v==x)
            adjX.insert(u);
    }

    if(adjX.size()<=1) {
        cout<<"Ayush"<<endl;
        return;
    }
    if(n&1)
        cout<<"Ashish"<<endl;
    else
        cout<<"Ayush"<<endl;
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
