/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int mask(int i) {
    if(i>0)
        return (1<<(i-1))-1;
    //cout<<"mask="<<ret<<endl;
    return 1;
}

bool checkFini(int num, ostringstream &oss, int ans) {
//cout<<"checkFini, num="<<bitset<21>(num)<<endl;
    int i=1;
    while(i<100000000) {
        if(num==i-1) {
            cout<<ans<<endl;
            cout<<oss.str()<<endl;
            exit(0);
        }
        i<<=1;
    }
    return false;
}

int main() {
    int x;
    cin>>x;

    ostringstream oss;
    int ans=0;

    int i=20;
    /* we simply set all bits. */
    while(i>=0) {
        checkFini(x, oss, ans);

        bool bit_set =((1<<(i-1)) & x);

        if(bit_set) {
//cout<<"tbit (0-based) set="<<i<<endl;
            oss<<i<<" ";
            x^=mask(i+1);
//cout<<"aft op1 x="<<bitset<21>(x)<<endl;;
            ans++;
            checkFini(x, oss, ans);

            ans++;
            x++;
//cout<<"aft op2 x="<<bitset<21>(x)<<endl;;
        }
        i--;
    }

    assert(false);
}

