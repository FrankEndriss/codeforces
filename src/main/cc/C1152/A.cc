/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int main() {
int n, m;
    cin>>n>>m;
    int a[n];
    int aEven=0;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        if(a[i]%2==0)
            aEven++;
    }
    int bEven=0;
    int b[m];
    for(int i=0; i<m; i++) {
        cin>>b[i];
        if(b[i]%2==0)
            bEven++;
    }

    int ans=min(aEven, m-bEven) + min(n-aEven, bEven);
    cout<<ans<<endl;


}

