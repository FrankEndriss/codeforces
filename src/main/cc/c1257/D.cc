/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);    // monsters
    cinai(a,n); // power monsters

    cini(m);    // heros
    vector<pii> ps(m);  // power endurance
    for(int i=0; i<m; i++) {
        cin>>ps[i].first>>ps[i].second;
    }
    sort(ps.begin(), ps.end());

    vi maxS(m);     // maxS[i] == maximum endurance of all ps[j]  j>=i

    int dp=0;
    for(int j=m-1; j>=0; j--) {
        dp=max(dp, ps[j].second);
        maxS[j]=dp;
    }

    int ans=1;
    int cnt=0;
    int mamo=0;
    for(int i=0; i<n; i++) {
        // is there a hero which can defeat all monsters including a[i] ?
        cnt++;
        mamo=max(mamo, a[i]);
        auto it=lower_bound(ps.begin(), ps.end(), make_pair(mamo, 0));
        if(it==ps.end()) { // no hero can defeat a[i]
            cout<<-1<<endl;
            return;
        }

        int idx=distance(ps.begin(), it);
        if(maxS[idx]<cnt) { // no hero can, so fight best possible
            ans++;
            cnt=0;
            mamo=0;
            i--;
        }   
    }

    cout<<ans<<endl;
    
    
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

