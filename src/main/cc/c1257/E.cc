/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cinai(k,3);
    int n=0;
    n=k[0]+k[1]+k[2];
    vi pos(n);
    for(int i=0; i<3; i++)
        for(int j=0; j<k[i]; j++) {
            cini(x);
            pos[x-1]=i;
        }

    // make pos sorted with as less moves as possible
    // one move is change one number

    vi dp0(n);  // dp0[i]= number of moves needed to put sheets 0..i in 0th group
    dp0[0]=(pos[0]>0);
    for(int i=1; i<n; i++)
        dp0[i]=dp0[i-1]+(pos[i]>0);

    vi dp01(n);  // dp01[i]= number of moves needed to put sheets 0..i in 0th or 1th group
    dp01[0]=(pos[0]>1);
    for(int i=1; i<n; i++)
        dp01[i]=dp01[i-1]+(pos[i]>1);

    vi dp2(n);  // dp2[i]= number of moves needed to put sheets i..n-1 in 2th group
    dp2[n-1]=(pos[n-1]<2);
    for(int i=n-2; i>=0; i--)
        dp2[i]=dp2[i+1]+(pos[i]<2);
    
    vi dp21(n);  // dp21[i]= number of moves needed to put sheets i..n-1 in 2th or 1th group
    dp21[n-1]=(pos[n-1]<1);
    for(int i=n-2; i>=0; i--)
        dp21[i]=dp21[i+1]+(pos[i]<1);

    vi opt2(n);
    for(int i=0; i<n; i++) 
        opt2[i]=dp01[i]+dp2[i];

    vi opt0(n);
    for(int i=0; i<n; i++) 
        opt0[i]=dp21[i]+dp0[i];

/* find min opt0/opt2 pair */
    vi opt0min(n);
    opt0min[0]=opt0[0];
    for(int i=1; i<n; i++)
        opt0min[i]=min(opt0min[i-1], opt0[i]);
    vi opt2min(n);
    opt2min[n-1]=opt2[n-1];
    for(int i=n-2; i>=0; i--)
        opt2min[i]=min(opt2min[i+1], opt2[i]);

    int ans=INF;
    for(int i=0; i<n; i++) 
        ans=min(ans, opt0min[i]+opt2min[i]);

    cout<<ans<<endl;
    
}

