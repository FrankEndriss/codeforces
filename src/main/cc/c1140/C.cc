/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n, k;
    cin>>n>>k;
    vector<pair<int, int>> bl(n);
    fori(n) {
        int b, l;
        cin>>l>>b;
        bl[i].first=b;
        bl[i].second=l;
    }

    //by beauty desc
    sort(bl.begin(), bl.end(), [&](auto &bl1, auto &bl2) {
        return bl2.first < bl1.first;
    });

    // by first/length asc
    set<pair<int, int>> kset;

    ll ans=0;
    int addidx=0;
    ll sumkset=0;   // sum of lengths in kset
    for(int i=0; i<n; i++) {
        /** insert all with beauty bigger or equal */
        while(addidx<n && bl[addidx].first>=bl[i].first) {
            pair<int, int> p=make_pair(bl[addidx].second, addidx);
            if(kset.count(p)==0) {
                kset.insert(p);
                sumkset+=p.first;
            }
            addidx++;
        }

        pair<int, int> pi=make_pair(bl[i].second, i);
        int cnt=kset.count(pi);  // check if i is in set, because it must be in set
        while(kset.size()>k-1+cnt) {
            auto first=kset.begin();
            sumkset-=first->first;
            kset.erase(first);
        }

        if(cnt==0) {
            kset.insert(pi);
            sumkset+=pi.first;
        }
        assert(kset.size()<=k);

        ans=max(ans, sumkset*bl[i].first);
    }

    cout<<ans<<endl;

}

