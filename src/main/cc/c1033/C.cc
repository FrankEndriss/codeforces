
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<n; i++)
#define fauto(c, vec) for(auto c : vec)

int main() {
    int n;
    cin>>n;
    vector<int> a(n);
    fori(n)
    cin>>a[i];

    vector<int> dp(n);
    const int UNKNOWN=0;
    const int LOOSE=1;
    const int WIN=2;
    int unknown=n;

    while(unknown) {
        for(int i=0; i<n; i++) { // check field i
            if(dp[i]!=UNKNOWN)
                continue;
            int state[3]= {0};
            int idx=i;
            while(1) {
                idx+=a[i];
                if(idx>=n)
                    break;
                if(a[idx]>a[i])
                    state[dp[idx]]++;
            }
            idx=i;
            while(1) {
                idx-=a[i];
                if(idx<0)
                    break;
                if(a[idx]>a[i])
                    state[dp[idx]]++;
            }

            // a field is WIN if we can move to a LOOSE, and LOOSE if we cant move to LOOSE or UNKNOWN.
            if(state[LOOSE]>0) {
                dp[i]=WIN;
                unknown--;
            } else if(state[UNKNOWN]==0) {
                dp[i]=LOOSE;
                unknown--;
            }
        }
    }

    for(int i=0; i<n; i++)
        cout<<(dp[i]==WIN?"A":"B");
    cout<<endl;

}

