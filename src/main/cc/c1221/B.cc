/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);

    cini(n);

    vvb vis(n, vb(n));
    vvb col(n, vb(n));

    queue<pair<int, int>> q;
    q.push({0,0});
    vis[0][0]=true;
    
    while(!q.empty()) {
        auto p=q.front();
        q.pop();

        const pii px[] ={ 
         { p.first-2, p.second-1 }
        ,{ p.first-1, p.second-2 }
        ,{ p.first+1, p.second-2 }
        ,{ p.first+2, p.second-1 }
        ,{ p.first-2, p.second+1 }
        ,{ p.first-1, p.second+2 }
        ,{ p.first+1, p.second+2 }
        ,{ p.first+2, p.second+1 } };

        for(auto d : px) { 
            if(d.first>=0 && d.first<n && d.second>=0 && d.second<n && !vis[d.first][d.second]) {
                q.push({ d.first, d.second});
                col[d.first][d.second]=!col[p.first][p.second];
                vis[d.first][d.second]=true;
            }
        }
    }

    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            cout<<(col[i][j]?"W":"B");
        }
        cout<<endl;
    }
}

