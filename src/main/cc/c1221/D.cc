/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

typedef pair<ll, int> plli;

void solve() {
    cini(n);
    vll a(n);
    vll b(n);
    for(int i=0; i<n; i++) {
        cin>>a[i]>>b[i];
    }

    function<ll(int)> correctDown=[&](int idx) {
        ll ans=b[idx];
        if(idx>0 && a[idx-1]==a[idx]) {
            ans+=min(correctDown(idx-1), b[idx]);
        }
        return ans;
    };

    function<ll(int)> correctUp=[&](int idx) {
        ll ans=b[idx];
        if(idx<n-1 && a[idx+1]==a[idx]) {
            ans+=min(correctUp(idx+1), b[idx]);
        }
        return ans;
    };

    ll ans=0;
    for(int i=1; i<n; i++) {
        if(a[i-1]==a[i]) {
            ll v1=correctDown(i-1);
            ll v2=correctUp(i);
            if(v1<v2) {
                ans+=v1;
            } else {
                ans+=v2;
                i++;
            }
        }
    }

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

