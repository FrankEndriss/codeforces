/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cins(s);
    map<char,int> cnt;
    for(auto c : s) 
        cnt[c]++;
    int up=min(cnt['U'], cnt['D']);
    int le=min(cnt['L'], cnt['R']);

    if(up==0 && le==0) {
        cout<<"0"<<endl;
        return;
    } else if(up==0 || le==0) {
        if(up>0) {
            auto posU=s.find("U");
            auto posD=s.find("D");
            if(posU<posD)
                cout<<"2"<<endl<<"UD"<<endl;
            else
                cout<<"2"<<endl<<"DU"<<endl;
        }
        if(le>0) {
            auto posL=s.find("L");
            auto posR=s.find("R");
            if(posL<posR)
                cout<<"2"<<endl<<"LR"<<endl;
            else
                cout<<"2"<<endl<<"RL"<<endl;
        }
        return;
    }

    cout<<up*2+le*2<<endl;
    for(int i=0; i<up; i++)
        cout<<"U";
    for(int i=0; i<le; i++)
        cout<<"L";
    for(int i=0; i<up; i++)
        cout<<"D";
    for(int i=0; i<le; i++)
        cout<<"R";
    cout<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

