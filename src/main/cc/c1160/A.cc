/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 *  AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio = std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef const int cint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

#define sol_all

int manhatten(cint x1, cint y1, cint x2, cint y2) {
	return abs(x1 - x2) + abs(y1 - y2);
}

/* A job wich has to be done, comes from Input.*/
typedef struct job {
	int x, y;
	int duration;    // duration
	int workers;     // #workers
	int minStart;    // earliest schedule
	int maxEnd;      // latest complete

	int maxStart;   // latest schedule
	int minEnd;     // earliest possible end
} job;

const int START = 1;
const int ARRIVE = 2;
const int WORK = 3;
const int END = 4;

const string aLabel[] = { "", "start", "arrive", "work", "end" };

typedef int actionType;
typedef int workerId;
typedef int jobId;

typedef struct workeraction {
	const actionType action;  // ARRIVE or WORK; or START or END
	const int fromMoment;
	const int untilMoment;
	const jobId atLocation;

} workeraction;

/* A worker. */
typedef struct worker {
	//int x, y;               // current position
	int job;                // current job
	int freeAt;             // will have finished applied job on that point of time
	vector<workeraction> hist;  // history of what this man has done in his life.
} worker;

/* Scheduling of a job */
typedef struct sched {
	int job;
	int start;
	vector<workerId> workers;
} sched;

/* coordinates of basecamp, where infinite workers are available. */
int baseX, baseY;
const int BASEJOBID = -1;

vector<job> jobs;
int n;

bool byMaxStart(int i1, int i2) {
	return jobs[i1].maxStart < jobs[i2].maxStart;
}

bool byMinEnd(int i1, int i2) {
	return jobs[i1].minEnd < jobs[i2].minEnd;
}

bool byWorkers(int i1, int i2) {
	return jobs[i1].workers < jobs[i2].workers;
}

bool byWorkersDesc(int i1, int i2) {
	return byWorkers(i2, i1);
}

bool byDuration(int i1, int i2) {
	return jobs[i1].duration < jobs[i2].duration;
}

bool byDurationDesc(int i1, int i2) {
	return byDuration(i2, i1);
}

class WorkerFactory {

	/* Employees, identified by id/index. */
	vector<worker> empl;

	/** Calculates the costs if allocate would be called with that parameters.
	 Costs include employment + travel time of the workers, not the time spent working.
	 Note that there are extra costs because workers have to travel to base after
	 last job.
	 */
public:
	int allocationCosts(int job, int start) {
		return 42;
	}

	/** allocates workers for job job. */
	vector<workerId> allocate(int job, int start) {
cout<<"allocate job="<<job<<" start="<<start<<endl;
		vector<workerId> ret;
		// find workers available for the job.
		// this is workers able to travel in time to job location, sorted by min manhatten to job.
		vector<int> id(empl.size());
		for (uint i = 0; i < id.size(); i++)
			id[i] = i;
		sort(id.begin(), id.end(), [&](int i1, int i2) {  // smallest first
					return manhatten(jobs[i1].x, jobs[i1].y, jobs[job].x, jobs[job].y)<manhatten(jobs[i2].x, jobs[i2].y, jobs[job].x, jobs[job].y);
				});
		int allocated = 0;
		for (int widx : id) {
			int manh = manhatten(jobs[empl[widx].job].x, jobs[empl[widx].job].y, jobs[job].x, jobs[job].y);
			if (empl[widx].freeAt + manh <= start) {
				allocated++;
				ret.push_back(widx);
				empl[widx].job = job;
				empl[widx].freeAt = start + jobs[job].duration;
				empl[widx].hist.push_back( { ARRIVE, start, -1, job });
				empl[widx].hist.push_back( { WORK, start, start + jobs[job].duration, job });
			}
		}

		int employtime = start - manhatten(baseX, baseY, jobs[job].x, jobs[job].y);
		for (int i = allocated; i < jobs[job].workers; i++) {   // employ new workers
			ret.push_back(empl.size());
			empl.resize(empl.size() + 1);
			empl.back().freeAt = start + jobs[job].duration;
			empl.back().job = job;
			empl.back().hist.push_back( { START, employtime, -1, BASEJOBID });
			empl.back().hist.push_back( { ARRIVE, start, -1, job });
			empl.back().hist.push_back( { WORK, start, start + jobs[job].duration, job });
		}

		return ret;
	}

	/** puts the home travel into hist of all workers. */
	void endAll() {
		for (auto &worker : empl) {
			int dist = manhatten(baseX, baseY, jobs[worker.job].x, jobs[worker.job].y);
			worker.hist.push_back( { ARRIVE, worker.freeAt + dist, BASEJOBID });
		}
	}

	void dumpWorkplan() {
		for (auto &worker : empl) {
			for (auto &hent : worker.hist) {
				if (hent.action == WORK)
					cout << aLabel[hent.action] << " " << hent.fromMoment << " " << hent.untilMoment << " " << hent.atLocation + 2
							<< endl;
				else
					cout << aLabel[hent.action] << " " << hent.fromMoment << " " << hent.atLocation + 2 << endl;
			}
			cout << "end" << endl;
		}
	}

} workerFactory;

int main() {
	cin >> n;
	int x, y, duration, workers, minStart, maxEnd;

	cin >> baseX >> baseY >> duration >> workers >> minStart >> maxEnd;

	vector<int> idByStart(n - 1);
	vector<int> idByEnd(n - 1);
	vector<int> idByWorkers(n - 1);
	vector<int> idByDuration(n - 1);
	fori(n-1)
	{
		cin >> x >> y >> duration >> workers >> minStart >> maxEnd;
//cout<<"job x="<<x<<" y="<<y<<endl;
		int minEnd = minStart + duration;
		int maxStart = max(minStart, minEnd - duration);
		jobs.push_back( { x, y, duration, workers, minStart, maxEnd, maxStart, minEnd });
		idByStart[i] = i;
		idByEnd[i] = i;
		idByWorkers[i] = i;
		idByDuration[i] = i;
	}

	// TODO
	// Set minStart for all jobs so that it is garanteed that workers can
	// reach the job location in time start travaling at base.

	sort(idByStart.begin(), idByStart.end(), byMaxStart);
	sort(idByEnd.begin(), idByEnd.end(), byMinEnd);
	sort(idByWorkers.begin(), idByWorkers.end(), byWorkersDesc);
	sort(idByDuration.begin(), idByDuration.end(), byDurationDesc);
//cout<<"sorted"<<endl;

	/* Variable costs are determined by:
	 * - minimize overall number of workers (240 per worker) by minimize parallel jobs
	 * - minimize nothing-to-do time of workers by giving them first job as late as possible
	 * - minimize nothing-to-do time of workers by minimizing time between jobs
	 * - minimize travel costs (time+distance) by ordering jobs by location
	 * - minimize consts by denying jobs with negative balance (to far away etc)
	 */

#ifdef sol_all
	for (uint jobId = 0; jobId <= jobs.size(); jobId++) {
		workerFactory.allocate(jobId, jobs[jobId].maxStart);
	}
	workerFactory.endAll();
	workerFactory.dumpWorkplan();

#elif defined sol_sample
	/* Sample solution, do the biggest job, noting else. */
	int i = 0;

	while (true) {
		int jobIdx = idByDuration[i++];
		int dist = manhatten(baseX, baseY, jobs[jobIdx].x, jobs[jobIdx].y);
		int woks = jobs[jobIdx].workers;
		int beg = jobs[jobIdx].maxStart - dist;
		if (beg <= 0)
			continue;
		for (int i = 0; i < woks; i++) {
			cout << "start " << beg << " 1" << endl;
			cout << "arrive " << beg + dist << " " << jobIdx + 2 << endl;
			int wend = beg + dist + jobs[jobIdx].duration;
			cout << "work " << beg + dist << " " << wend << " " << jobIdx + 2 << endl;
			cout << "arrive " << wend + dist << " 1" << endl;
			cout << "end" << endl;
		}
		break;
	}

#endif

}

