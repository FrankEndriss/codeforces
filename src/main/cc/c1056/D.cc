
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<n; i++)
#define fauto(c, vec) for(auto c : vec)

ll dfs(int node, int parent, vector<int> &ret, vector<vector<int>> &tree) {
    ll count=0;
    fauto(c, tree[node]) 
        if(c!=parent && c!=node) {
            count+=dfs(c, node, ret, tree);
        }
    if(count==0)
        count++;
    ret.push_back(count);
    return count;
}

int main() {
int n;
    cin>>n;
    vector<vector<int>> tree(n+2, vector<int>());
    
    fori(n-1) {
        int p;
        cin>>p;
        tree[p].push_back(i+2);
        tree[i+2].push_back(p);
    }

    vector<int> ccount;
    dfs(1, -1, ccount, tree); // collect child counts in ccount

    sort(ccount.begin(), ccount.end());
    fauto(i, ccount) 
        cout<<i<<" ";
    cout<<endl;

}

