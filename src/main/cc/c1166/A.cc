/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int fak(int v) {
    if(v<2)
        return 0;
    if(v==2)
        return 1;
    return v-1+fak(v-1);
}

int main() {
int n;
    cin>>n;

    string s;
    vector<int> f(1+(int)'z'-'a');
    fori(n) {
        cin>>s;
        f[s[0]-'a']++;
    }

    int ans=0;
    for(auto x : f) {
        if(x>2) {
            int d1=x/2;
            int d2=x-d1;
//cout<<"d1="<<d1<<" d2="<<d2<<endl;
            ans+=(fak(d1)+fak(d2));
        }
    }

    cout<<ans<<endl;

    

}

