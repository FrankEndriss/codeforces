/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int k;
    cin>>k;

    char c[]={ 'a', 'e', 'i', 'o', 'u' };

    int n=-1, m=-1;
    for(int i=5; i*i<=k; i++) {
        if(k%i==0) {
            n=i;
            m=k/i;
            break;
        }
    }

    if(n==-1) {
        cout<<-1<<endl;
        return 0;
    }

    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            cout<<c[(i+j)%5];
        }
    }
    cout<<endl;

}

