/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n;
    cin>>n;
    vector<ll> a(n);
    fori(n)
        cin>>a[i];

for(int i=-9; i<=9; i++)  {
    for(int j=-9; j<=9; j++) {
        int aL=min(abs(i), abs(j));
        int aR=max(abs(j), abs(i));
        int vL=abs(min(i-j, i+j));
        int vR=abs(max(i-j, i+j));
        if(i==j)
            cout<< "X";
        else if(vL<=aL && vR>=aR)
            cout<< ".";
        else
            cout<<"O";
    }
    cout<<endl;
}
return 0;

/*
XOOOOOOOOOO
OXOOOOOOOOO
OOXOOOOOOOO
OOOXOOOOOOO
OOOOXOOOOOO
.....X.....
.....OX....
.....O.X...
....OOO.X..         -> x/2+1
....OOO..X.
...OOOOO..X         -> x/2+1
*/
    sort(a.begin(), a.end());

    ll ans=0;
    for(int i=0; i<n; i++) {
        if(a[i]>=0) {
            auto up=upper_bound(a.begin(), a.end(), (a[i]+1)/2);
            int countup=n-(up-a.begin())-1;
            if(a[i]==0)
                countup++;

            auto lo=lower_bound(a.begin(), a.end(), -(a[i]+1)/2);
            int countlo=lo-a.begin();

            ans+=countup;
            ans+=countlo;
//cout<<"a[i]="<<a[i]<<" countup="<<countup<<" countlo="<<countlo<<endl;
        }
    }

    cout<<ans<<endl;



}

