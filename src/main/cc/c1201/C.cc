/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinll(k);
    cinall(a, n);
    sort(a.begin(), a.end());

    int midx=n/2;
    int cidx=midx;
    while(k>0) {
        int cnt=cidx-midx+1;
        int inc=0;
        if(n>cidx+1) {
            inc=min(k, (a[cidx+1]-a[cidx])*cnt);
            cidx++;
            if(inc==0)
                continue;
        } else
            inc=k;
        int inc2=inc/cnt;
        if(inc2==0)
            break;
        k-=inc2*cnt;
        a[midx]+=inc2;
    }

    cout<<a[midx]<<endl;

}

