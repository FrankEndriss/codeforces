/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<pii> vpii;

const int INF=1e8;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    cini(k);
    cini(q);
    vvi t(n+1, { INF, INF });
    fori(k) {
        int tr, tc;
        cin>>tr>>tc;
        t[tr][0]=min(t[tr][0], tc);
        t[tr][1]=max(t[tr][1], tc);
    }

    cinai(b, q); // move up cols
    sort(b.begin(), b.end());

/* num steps starting at start moving up one row then to end */
    function<int(int, int)> shortestPath=[&](int start, int end) {
/* Two possibilities: 
 * Move left, then up, then to end. Or
 * Move right, then up, then to end.
 */
        auto it=lower_bound(b.begin(), b.end(), start);
        int ansL=INF;
        int ansR=INF;

        if(it!=b.begin()) {
            it--;
            int idxL=*it;
            ansL=abs(start-idxL)+1+abs(idxL-end);
            it++;
        }
        if(it!=b.end()) {
            int idxR=*it;
            ansR=abs(start-idxR)+1+abs(idxR-end);
        }
        return min(ansR, ansL);
    };

/* Now step up level by level, until last level reached. 
 * There are allways two possible paths, they can be same:
 * 1. shortest way to leftmost treasure, then move to the right
 * 2. shortest way to rightmost treasure, then move to the left
 * So, we need to maintain min steps for two positions per row.
 * In first level we only use the right one. (left=0, right=1)
 **/
    vvi dp(n+1, vi(2)); // dp[i][0]=min steps to collect all treasure up to level i ending on the left==0 of that rows treasures. right==1.
    dp[1][0]=INF;
    if(t[1][0]==INF) {
        t[1][0]=1;
        t[1][1]=1;
    }

    dp[1][1]=t[1][1]-1;

    int prevLevel=1;
    while(true) {
        int nextLevel=prevLevel+1;
        while(nextLevel<t.size() && t[nextLevel][0]==INF)
            nextLevel++;
        if(nextLevel==t.size())
            break;  // no more treasures

        int levelExtra=nextLevel-prevLevel-1;   // if level wo/ treasure

        dp[nextLevel][0]=min(
                dp[prevLevel][0]+shortestPath(t[prevLevel][0], t[nextLevel][0]),
                dp[prevLevel][1]+shortestPath(t[prevLevel][1], t[nextLevel][0]))
                    +abs(t[nextLevel][0]-t[nextLevel][1])
                    +levelExtra;
        dp[nextLevel][1]=min(
                dp[prevLevel][0]+shortestPath(t[prevLevel][0], t[nextLevel][1]),
                dp[prevLevel][1]+shortestPath(t[prevLevel][1], t[nextLevel][1]))
                    +abs(t[nextLevel][0]-t[nextLevel][1])
                    +levelExtra;
        prevLevel=nextLevel;
    }
    
    cout<<min(dp[prevLevel][0], dp[prevLevel][1])<<endl;
}

