/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    ll n;
    cin>>n;
    vector<pair<ll, ll>> id(2*n);
    fori(2*n) {
        ll a;
        cin>>a;
        id[i]={ a, i+1 };
    }
    sort(id.begin(), id.end());

    ll s=1;
    ll d=1;
    ll ans=0;
    for(ll i=1; i<=n; i++) {
        auto it=lower_bound(id.begin(), id.end(), make_pair((ll)i, 0LL));
        ll i1=it->second;
        it++;
        ll i2=it->second;

        ll sd=abs(i1-s) + abs(i2-d);
        ll ds=abs(i2-s) + abs(i1-d);
        if(sd<ds) {
            ans+=sd;
            s=i1;
            d=i2;
        } else {
            ans+=ds;
            s=i2;
            d=i1;
        }
    }
    cout<<ans<<endl;
}

