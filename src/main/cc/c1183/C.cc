/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int q;
    cin>>q;
    for(int i=0; i<q; i++) {
        ll k, n, a, b;
        cin>>k>>n>>a>>b;
    
        if(k>n*a) {
            cout<<n<<endl;
        } else if(k<=n*b) {
            cout<<-1<<endl;
        } else {
            ll save=a-b;
            ll res=k-n*b-1;
            ll ans=res/save;
            cout<<ans<<endl;
        }
    }

}

