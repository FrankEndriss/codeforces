/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

const int N=200001;

void solve() {
    int n;
    cin>>n;
    vector<int> f(n+1, 0);
    for(int i=0; i<n; i++) {
        int c;
        cin>>c;
        f[c]++;
    }
    sort(f.begin(), f.end(), greater<int>());

    int ans=0;
    int last=f[0];
    for(int j=0; j<n; j++) {
        if(f[j]>=last) {
            ans+=last;
            last--;
        } else {
            ans+=f[j];
            last=f[j]-1;
        }

        if(f[j]==0 || last==0)
            break;
    }

    cout<<ans<<endl;
}

/*
void solve() {
    int n;
    cin>>n;
    vector<pair<int, int>> f(n);
    
    for(int i=0; i<n; i++) {
        int c;
        cin>>c;
        f[c].first++;
        int c2;
        cin>>c2;
        if(c2==1)
            f[c].second++;
    }
    sort(f.begin(), f.end(), [&](pair<int, int> p1, pair<int, int> p2) {
        if(p2.first==p1.first)
            return p2.second<p1.second;
        return p2.first<p1.first;
    });

    int ans=0;
    int ans2=0;
    int last=f[0].first;
    for(int j=0; j<n; j++) {
        if(f[j].first>=last) {
            ans+=last;
            int idx=j;
            while(idx<n && f[idx].first>=last)
                idx++;

            sort(f.begin()+j, f.begin()+idx+1, [&](pair<int, int> p1, pair<int, int> p2) {
                return p2.second<p1.second;
            });

            ans2+=min(last, f[j].second);
            last--;
        } else {
            ans+=f[j].first;
            ans2+=f[j].second;
            last=f[j].first-1;
        }

        if(f[j].first==0 || last==0)
            break;
    }

    cout<<ans<<" "<<ans2<<endl;
}
*/


int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int q;
    cin>>q;
    for(int t=0; t<q; t++) {
        solve();
    }
}
