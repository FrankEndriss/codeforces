/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<vector<pii>> g(n+1);
    int firstu;
    int firstv;
    int firstw;
    fori(m) {
        int u, v, w;
        cin>>u>>v>>w;
        if(i==0) {
            firstu=u;
            firstv=v;
            firstw=w;
        }
        g[u].push_back({v, w});
        g[v].push_back({u, w});
    }

    int mi1=1e9;
    for(auto c : g[firstu])
        mi1=min(mi1, c.second);
    int mi2=1e9;
    for(auto c : g[firstv])
        mi2=min(mi2, c.second);
    int ma=max(mi1, mi2);   // max of both mins

    cout<<ma<<endl;

    // IDK

}

