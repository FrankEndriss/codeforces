/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<pii> vpii;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int ships, bases;
    cin>>ships>>bases;
    vi att(ships);
    fori(ships)
        cin>>att[i];

    vpii defgold(bases);
    fori(bases)
        cin>>defgold[i].first>>defgold[i].second;

    sort(defgold.begin(), defgold.end());
    vll dp(bases+1, 0);
    for(int i=1; i<=bases; i++)
        dp[i]=dp[i-1]+defgold[i-1].second;

    fori(ships) {
        auto it=upper_bound(defgold.begin(), defgold.end(), make_pair(att[i], (int)1e9+1));
        int idx=distance(defgold.begin(), it);
        cout<<dp[idx]<<endl;
    }

}

