/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, k, m, t;
    cin>>n>>k>>m>>t;

    k--;       // index of doctor 0-based
    int len=n; // len of list

    fori(t) {
        int evt, idx;
        cin>>evt>>idx;
        idx--;          // 0-based
        if(evt==1) {    // insert node before idx
            len++;
            if(idx<=k)
                k++;
        } else {        // split list after idx
            if(k<=idx) {
                len=idx+1;
            } else {
                len-=(idx+1);
                k-=(idx+1);
            }
        }
        cout<<len<<" "<<k+1<<endl;
    }

}

