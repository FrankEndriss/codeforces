/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    const int N=51;
    int n;
    cin>>n;
    vi x(n*4+1);
    vi y(n*4+1);
    vi fx(N);
    vi fy(N);

    int minx=1000;
    int miny=1000;
    int maxx=-1;
    int maxy=-1;
    fori(4*n+1) {
        cin>>x[i]>>y[i];
        fx[x[i]]++;
        fy[y[i]]++;
    } 

    /* upper x is biggest x with more than 1 f */
    for(int i=N-1; i>=0; i--)
        if(fx[i]>=n) {
            maxx=i;
            break;
        }
    for(int i=N-1; i>=0; i--)
        if(fy[i]>=n) {
            maxy=i;
            break;
        }
    for(int i=0; i<N; i++)
        if(fx[i]>=n) {
            minx=i;
            break;
        }
    for(int i=0; i<N; i++)
        if(fy[i]>=n) {
            miny=i;
            break;
        }
        
    fori(n*4+1) {
        if(((x[i]>=minx && x[i]<=maxx) && (y[i]==miny || y[i]==maxy))
            ||
            ((y[i]>=miny && y[i]<=maxy) && (x[i]==minx || x[i]==maxx)))
            ;
        else {
            cout<<x[i]<<" "<<y[i]<<endl;
            return 0;
        }
    }

}

