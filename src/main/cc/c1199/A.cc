
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, x, y;
    cin>>n>>x>>y;

    vi a(n);
    fori(n)
    cin>>a[i];

    const int INF=1e9+1;
    vi dpx(n);
    vi dpy(n);
    for(int i=0; i<n; i++) {
        int mi=INF;
        for(int j=max(0, i-x); j<i; j++)
            mi=min(mi, a[j]);
        dpx[i]=mi;

        mi=INF;
        for(int j=i+1; j<=i+y && j<n; j++)
            mi=min(mi, a[j]);
        dpy[i]=mi;
    }

    for(int i=0; i<n; i++) {
//cout<<"i="<<i<<" a[i]="<<a[i]<<" dpx[i]="<<dpx[i]<<" dpy[i]="<<dpy[i]<<endl;
        if(dpx[i]>a[i] && dpy[i]>a[i]) {
            cout<<i+1<<endl;
            return 0;
        }
    }
    assert(false);
}

