/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int INF=1e9;
int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, I;
    cin>>n>>I;
    vi a(n);
    map<int, int> f;
    fori(n) {
        cin>>a[i];
        f[a[i]]++;
    }

    I*=8;   // available storage in bits
    I/=n;   // max bits per sample
    if(I==0) {
        cout<<n<<endl;
        return 0;
    }

    int d=1<<I; // max diff
//cout<<"dbg, d="<<d<<endl;
/* We need to find the range i,j which is lte d, but max sum
 */
    vector<pii> vf;
    vi dp;
    for(auto ent : f) {
        vf.push_back({ent.first, ent.second});
    }


    int ma=0;
    fori(vf.size()) {
        int j=i;
        int sum=0;
        do {
            sum+=vf[j].second;
            j++;
        } while(j<vf.size() && vf[j].first-vf[i].first<d);
        ma=max(ma, sum);
    }
    
    cout<<n-ma<<endl;

}
