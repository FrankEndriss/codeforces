/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll atol(string &s) {
    istringstream iss(s);
    ll i;
    iss>>i;
    return i;
}

void atoli(string &s, vll &a) {
    istringstream iss(s);
    int i=0;
    while(iss.good())
        cin>>a[i++];
}

void solve() {
    string a="abc";
    int idx=0;
    cins(t);
    char prev=' ';
    for(int i=0; i<t.size(); i++) {
        if(t[i]=='?') {
            while(prev==a[idx] || (i+1<t.size() && t[i+1]==a[idx]))
                idx=(idx+1)%3;

            t[i]=a[idx];
        }
        prev=t[i];
    }

    prev=' ';
    for(int i=0; i<t.size(); i++) {
        if(prev==t[i]) {
            cout<<-1<<endl;
            return;
        }
        prev=t[i];
    }
    cout<<t<<endl;
    
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

