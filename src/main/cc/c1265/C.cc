/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    vi p;
    for(int i=0; i<n; i++) {
        cini(tmp);
        if(tmp==0)
            break;
        p.push_back(tmp);
    }
    int gma=n/2;

    if(p.size()<5) {
        cout<<"0 0 0"<<endl;
        return;
    }

    /* find min g and min s, give b to all others. Then check if enough medals. */
    int g=0;
    for(int i=1; i<p.size(); i++)  {
        if(p[i-1]!=p[i]) {
            g=i;
            break;
        }
    }
//cout<<"g="<<g<<endl;

    int s=0;
    for(int i=g; i<p.size() ; i++) {
        if(p[i-1]!=p[i] && s>g) {
            s=i-g;
            break;
        }
    }
//cout<<"s="<<s<<endl;

/*
        for(int i=g+s; i<p.size() ; i++) {
            if(p[i-1]!=p[i] && b>g) {
                break;
            }
            b++;
        }
*/

 //       sol.push_back(b);

//cout<<"b="<<b<<endl;

        if(g==0 || s<=g) {
            cout<<"0 0 0"<<endl;
            return;
        }

        vi sol;
        int b=0;
        int idx=g+s;
        int prev=-1;
        while(idx<p.size()) {
            if(p[idx]!=prev && (idx+1==p.size() || p[idx]!=p[idx+1]) && idx<=gma) {
                b=idx-(g+s+1);
            }
            prev=p[idx];
            idx++;
        }

    if(b==0) {
            cout<<"0 0 0"<<endl;
            return;
    }
    cout<<g<<" "<<s<<" "<<b<<endl;


}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

