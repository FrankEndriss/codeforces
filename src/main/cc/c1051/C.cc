/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>

const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n)   for (ll i=0; i<(n); i++)
#define fauto(c, vec)   for (auto c : vec)


int main() {
int n;
    cin>>n;
    vector<int> a(n);
    vector<int> freq(101);
    fori(n) {
        cin>>a[i];
        freq[a[i]]++;
    }

    int c1=0;
    int c3=0;
    fori(n) {
        if(freq[a[i]]==1) {
            c1++;
        } else if(freq[a[i]]>2) {
            c3++;
        }
    }

    /** 3 cases x in a 1x), 2x), 3ormoreX)
     * if number of 1x) is even, we need to distribute them 1:1.
     * if number of 1x) is odd, we need to distribute one 3ormoreX to B.
     * -> So we distribute first 1x) to B, second to A...
     * -> if 1)odd first of first 3) to B.
     */


    if(c1%2==1 && c3==0) {
        cout<<"NO"<<endl;
        return 0;
    }

    cout<<"YES"<<endl;

    bool toB=true;
    bool printC3=c1%2==1;
    string notC3="A";
    fori(n) {
        if(freq[a[i]]==1) {
            cout<<(toB?"B":"A");
            toB=!toB;
        } else if(freq[a[i]]>2 && printC3) {
            cout<<(toB?"B":"A");
            toB=!toB;
            notC3=(toB?"B":"A");
            printC3=false;
        } else
            cout<<notC3;
    }
    cout<<endl;
    exit(0);
}

