/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);    // rooms per floor
    cins(stair);
    int firstS=n+1;
    int lastS=-1;
    for(int i=0; i<n; i++) {
        if(stair[i]=='1') {
            firstS=min(firstS, i);
            lastS=i;
        }
    }

    if(firstS>n) { // no stairs
        cout<<n<<endl;
        return;
    }
    if(firstS==lastS) {
        int ans=max(firstS+1, n-firstS)*2;
        cout<<ans<<endl;
        return;
    }

    int ans=max(n-firstS, lastS+1)*2;
    cout<<ans<<endl;
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

