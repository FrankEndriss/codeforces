/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cinll(n);
    cinll(k);

    ll minK=n*(n+1)/2;
    if(k<minK) {
        cout<<-1<<endl;
        return 0;
    }

    vi ans(n);
    for(int i=0; i<n; i++)
        ans[i]=i;

    int lidx=0;
    for(int ridx=n-1; ridx>lidx && k>minK; ridx--) {
        if(k-minK>=ans[ridx]-ans[lidx]) {
            minK+=ans[ridx]-ans[lidx];
            swap(ans[ridx], ans[lidx]);
            lidx++;
        }
    }
    cout<<minK<<endl;
    for(int i=0; i<n; i++)
        cout<<i+1<<" ";
    cout<<endl;
    for(int i=0; i<n; i++) 
        cout<<ans[i]+1<<" ";
    cout<<endl;

}

