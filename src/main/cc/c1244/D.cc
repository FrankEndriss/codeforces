/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 **/

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int thirdColor(int colP, int colPP) {
    if(min(colP, colPP)>0) {
        return 0;
    } else if(max(colP, colPP)<2) {
        return 2;
    } else
        return 1;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);

    vvi cc(n, vi(3));
    for(int j=0; j<3; j++) {
    for(int i=0; i<n; i++) {
        cin>>cc[i][j];
    }
    }

    vvi tree(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    int aLeaf=-1;
    for(int i=0; i<n; i++) {
        if(tree[i].size()>2) {
            cout<<-1<<endl;
            return 0;
        }
        if(tree[i].size()==1 && aLeaf<0)
            aLeaf=i;
    }

    vi colarray(n);

    function<ll(int,int,int,int)> dfs=[&](int node, int parent, int colP, int colPP) {
        int col=thirdColor(colP, colPP);
//cout<<"dfs, colP="<<colP<<" colPP="<<colPP<<" col="<<col<<endl;
        colarray[node]=col;
        ll ans=cc[node][col];

        for(int chl : tree[node])
            if(chl!=parent)
                ans+=dfs(chl, node, col, colP);
        return ans;
    };

    const ll INF=1e18+7;
    ll ans=INF;
    vi ansarray(n);
    for(int c1=0; c1<3; c1++) {
        for(int c2=0; c2<3; c2++) {
            if(c1!=c2) {
                ll lans=dfs(aLeaf, -1, c1, c2);
//cout<<"lans="<<lans<<" colarray[0]="<<colarray[0]<<endl;
                if(lans<ans) {
                    ans=lans;
                    for(int i=0; i<n; i++) 
                        ansarray[i]=colarray[i];
                }
            }
        }
    }
    cout<<ans<<endl;
    for(int i=0; i<n; i++)
        cout<<ansarray[i]+1<<" ";
    cout<<endl;
}
