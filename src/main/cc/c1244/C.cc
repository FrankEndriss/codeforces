/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cinll(n);   // num games of team
    cinll(p);   // points
    cinll(w);   // points per win
    cinll(d);   // points per draw

    ll y=0;
    ll tmp=p;
    while(tmp>=0 && y<1000000) {
        if(tmp%w==0) {
            ll x=tmp/w;
            ll z=n-x-y;
            if(z>=0)
                cout<<x<<" "<<y<<" "<<z<<endl;
            else
                cout<<-1<<endl;
            return 0;
        }

        tmp-=d;
        y++;
    }
    cout<<-1<<endl;

}

