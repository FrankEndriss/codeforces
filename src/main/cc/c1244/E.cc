/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<ll, ll> pllll;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cinll(n);
    cinll(k);

    map<int, int> m;

    for(int i=0; i<n; i++) {
        cini(a);
        m[a]++;
    }
    vector<pllll> p;
    for(auto ent : m)
        p.push_back({ ent.first, ent.second });

    int lidx=0;
    int ridx=p.size()-1;
    while(lidx<ridx) {
        if(p[lidx].second<p[ridx].second) { // increment left
            ll neededL=p[lidx].second*(p[lidx+1].first-p[lidx].first);
            if(k>=neededL) {
                lidx++;
                p[lidx].second+=p[lidx-1].second;
                k-=neededL;
                continue;
            } else if(k>=p[lidx].second) {
                ll cnt=k/p[lidx].second;
                k%=p[lidx].second;
                p[lidx].first+=cnt;
            } else
                break;
        } else {         // decrement right
            ll neededR=p[ridx].second*(p[ridx].first-p[ridx-1].first);
            if(k>=neededR) {
                ridx--;
                p[ridx].second+=p[ridx+1].second;
                k-=neededR;
                continue;
            } else if(k>=p[ridx].second) {
                ll cnt=k/p[ridx].second;
                k%=p[ridx].second;
                p[ridx].first-=cnt;
            } else 
                break;
        }
    }

    cout<<p[ridx].first-p[lidx].first<<endl;
}

