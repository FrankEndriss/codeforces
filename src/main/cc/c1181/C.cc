/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;

    vector<string> b(n);
    fori(n)
    cin>>b[i];

    vector<vector<tuple<char, char, char>>> dpColors(n, vector<tuple<char, char, char>>(m));

    bool gBreak=false;
    function<bool(int, int, int)> checkPattern=[&](int row, int col, int pattern) {
#ifdef DEBUG
        cout<<"checkPattern, row="<<row<<" col="<<col<<" pattern="<<pattern<<endl;
#endif
        assert(row+pattern+pattern<n);
        for(int i=0; i<pattern; i++) {
            if(b[row][col]!=b[row+i][col]) {
                gBreak=true;
                return false;
            }
            if(b[row+pattern][col]!=b[row+pattern+i][col])
                return false;
            if(b[row+pattern+pattern][col]!=b[row+pattern+pattern+i][col])
                return false;
        }
        if(b[row][col]==b[row+pattern][col] ||
                b[row][col]==b[row+pattern+pattern][col] ||
                b[row+pattern][col]==b[row+pattern+pattern][col])
            return false;

#ifdef DEBUG
        cout<<"checkPattern==true, row="<<row<<" col="<<col<<endl;
#endif
        dpColors[row][col]= { b[row][col], b[row+pattern][col], b[row+pattern+pattern][col] };
        return true;
    };

    /* a line pattern has a size, 1, 2, 3... the number of same lines.
     * a col has at idx i at most one such pattern. */
    vector<vector<int>> dp(n, vector<int>(m));

    /* simple brute force */
    for(int row=0; row<n-2; row++) {
        for(int col=0; col<m; col++)  {
            for(int pattern=1; row+pattern*3<=n; pattern++) {
                gBreak=false;
                if(checkPattern(row, col, pattern)) {
                    dp[row][col]=pattern;
                    break;
                }
                if(gBreak)
                    break;
            }
        }
    }


    /* check patterns if same pattern in  col and col+1 */
    function<bool(int, int)> isSamePattern=[&](int row, int col) {
        bool ret= col<m-1 && dp[row][col]==dp[row][col+1] &&
                  dpColors[row][col]==dpColors[row][col+1];
#ifdef DEBUG
        cout<<"isSamePattern="<<ret<<" row="<<row<<" col="<<col<<endl;
#endif
        return ret;
    };

    function<ll(ll)> fak=[&fak](ll i) {
        if(i==1)
            return 1LL;
        else
            return i+fak(i-1);
    };

#ifdef DEBUG
    cout<<"searching multi col patterns"<<endl;
#endif

    ll ans=0;
    for(int row=0; row<n-2; row++) {
        for(int col=0; col<m; col++)  {
            int c=0;
            if(dp[row][col]>0) {
                c=1;
                while(col+c<n && isSamePattern(row, col+c-1))
                    c++;
                ans+=fak(c);
                col+=(c-1);
            }
        }
    }

    cout<<ans<<endl;
}

