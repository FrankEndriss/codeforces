/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

string sadd(string s1, string s2) {
#ifdef DEBUG
cout<<"sadd, s1="<<s1<<" s2="<<s2<<endl;
#endif

    string ans="";
    int digit;
    int uber=0;
    for(uint i=0; i<max(s1.size(), s2.size()); i++) {
        digit=uber;
        if(i<s1.size())
            digit+=(s1[s1.size()-i-1]-'0');
        if(i<s2.size())
            digit+=(s2[s2.size()-i-1]-'0');
    
        char c=('0'+(digit%10));
        ans+=c;
        uber=digit/10;
    }
    
    while(uber>0) {
        char c='0'+(uber%10);
        ans+=c;
        uber/=10;
    }
    reverse(ans.begin(), ans.end());
#ifdef DEBUG
cout<<"sadd, ret="<<ans<<endl;
#endif
    return ans;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int len;
    string n;
    cin>>len>>n;

    int minLen=len;
    vector<int> dp(len);    //length of longer half if split after i
    for(int i=0; i<len-1; i++) {
        if(n[i+1]!='0') {
            dp[i]=max(i+1, len-i-1);
            minLen=min(minLen, dp[i]);
        }
    }

    vector<int> sols;
    fori(len)
        if(dp[i]==minLen || dp[i]==minLen+1)
            sols.push_back(i);

#ifdef DEBUG
cout<<"minLen="<<minLen<<" sols.size()="<<sols.size()<<endl;
#endif

/* Find min sum of possible sols. */
    vector<string> ssols;
    for(int s : sols)
        ssols.push_back(sadd(n.substr(0, s+1), n.substr(s+1, len-s-1)));

    sort(ssols.begin(), ssols.end(), [](string &s1, string &s2) {
        if(s1.size()==s2.size())
            return s1<s2;
        else
            return s1.size()<s2.size();
    });


    cout<<ssols[0]<<endl;

}

