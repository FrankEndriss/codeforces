/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct Dsu {
    vector<int> p;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};

void solve() {
    cini(n);
    vi p(n);
    for(int i=0; i<n; i++) {
        cin>>p[i];
        p[i]--;
    }
    vi freq(n);     // freq per setid

    Dsu dsu(n);
    vi vis(n);
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;

        freq[i]=0;
        int next=i;
        do { 
            freq[i]++;
            vis[next]=true;
            next=p[next];
            dsu.union_sets(i, next);
        }while(next!=i);
    }
    for(int i=0; i<n; i++) 
        cout<<freq[dsu.find_set(i)]<<" ";
    cout<<endl;
    
}


int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

