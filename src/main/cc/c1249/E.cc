/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;

const int INF=1e9;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(c);
    cinai(a, n-1);
    cinai(b, n-1);

    vvll dp(n, vll(2));  // dp[i][0]=stairs, dp[i][1]=eleva
    dp[0][1]=c;
    
    cout<<"0 ";
    for(int i=1; i<n; i++) {    
        dp[i][0]=min(a[i-1]+dp[i-1][0], a[i-1]+dp[i-1][1]);
        dp[i][1]=min(b[i-1]+dp[i-1][1], b[i-1]+dp[i-1][0]+c);
        cout<<min(dp[i][0], dp[i][1])<<" ";
    }
    cout<<endl;
}

