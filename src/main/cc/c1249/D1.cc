/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(k);

    vector<pii> segs(n);
    for(int i=0; i<n; i++)
        cin>>segs[i].second>>segs[i].first;

    vi sid(n);
    iota(sid.begin(), sid.end(), 0);

    sort(sid.begin(), sid.end(), [&](int i1, int i2){
        return segs[i1]<segs[i2];
    });

    queue<pii> q;
    int maxL=-1;
    for(int i=0; i<k; i++) 
        q.push(segs[sid[i]]);

    vi ans;
    for(int i=k; i<n; i++) {
        pii lowest=q.front();
        if(lowest.first>=segs[sid[i]].second) {
            ans.push_back(sid[i]+1);
        } else {
            q.pop();
            q.push(segs[sid[i]]);
        }
    }
    cout<<ans.size()<<endl;
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;

}

