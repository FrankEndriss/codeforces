/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

vll tri;

void solve() {
    cinll(n);
//cout<<"solve(), n="<<n<<endl;
    ll x=n;

    vb vis(40, false);
    
    for(int i=39; i>=0; i--) {
        if(n>=tri[i]) {
            n-=tri[i];
            vis[i]=true;
        }
//cout<<"l1 n="<<n<<endl;
    }

    if(n==0) {   // biggest number smaller/eq n
        cout<<x<<endl;
        return;
    }

    n=x-n;
    for(int i=0; i<40; i++) {
        if(vis[i])
            continue;
        if(n+tri[i]>=x) {
            vis[i]=true;
            n+=tri[i];
            break;
        }
    }
//cout<<"after l2, n="<<n<<endl;
    if(n==x) {
        cout<<x<<endl;
        return;
    }

    for(int i=39; i>=0; i--) {
        if(!vis[i])
            continue;
        if(n-tri[i]>=x) {
            n-=tri[i];
//cout<<"l3, n="<<n<<endl;;
        }
    }

    cout<<n<<endl;

}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    ll num=1;
    do {
        tri.push_back(num);
        num*=3;
    }while(tri.size()<40);
    
    while(t--)
        solve();
}

