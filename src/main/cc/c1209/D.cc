/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    cini(n);
    cini(k);
    vi x(k);
    vi y(k);
    vi freq(n);
    set<int> unfeeded;

    for(int i=0; i<k; i++) {
        cin>>x[i]>>y[i];
        x[i]--;
        y[i]--;
        freq[x[i]]++;
        freq[y[i]]++;
        unfeeded.insert(i);
    }

    int ans=0;
    vb used(n); // eaten snacks

    /* first all guest with both unique flavours */
    for(auto it=unfeeded.begin(); it!=unfeeded.end(); it++) {
        int i=*it;
        if(freq[x[i]]==1 && freq[y[i]]==1)  {
            auto tmp=it;
            tmp++;
            unfeeded.erase(it);
            it=tmp;
            used[x[i]]=true;
            used[y[i]]=true;
        }
                if(it==unfeeded.end())
                    break;
    }

    /* guest with at least one unique flavour. 
    for(auto it=unfeeded.begin(); it!=unfeeded.end(); it++) {
        int i=*it;

        if(freq[x[i]]==1 || freq[y[i]]==1)  {
            auto tmp=it;
            tmp++;
            unfeeded.erase(it);
            it=tmp;
            used[x[i]]=true;
            used[y[i]]=true;
        }
                if(it==unfeeded.end())
                    break;
    }
*/

    /* all other guests, but the ones with only one available first. */
    while(unfeeded.size()) {

        bool found=false;

        /* find ones with only one available */
        for(auto it=unfeeded.begin(); it!=unfeeded.end(); it++) {
            int i=*it;

            if((!used[y[i]] && used[x[i]]) || (used[y[i]] && !used[x[i]]) ) {
                auto tmp=it;
                tmp++;
                unfeeded.erase(it);
                it=tmp;
                used[x[i]]=true;
                used[y[i]]=true;
                found=true;
            }
                if(it==unfeeded.end())
                    break;
        }

        /* find anyone */
        if(!found) {
            for(auto it=unfeeded.begin(); it!=unfeeded.end(); it++) {
                int i=*it;

                auto tmp=it;
                tmp++;
                unfeeded.erase(it);
                it=tmp;
                if((used[y[i]] && used[x[i]])) {
                    ans++;
                } else {
                    used[x[i]]=true;
                    used[y[i]]=true;
//                    break;
                }
                if(it==unfeeded.end())
                    break;
            }
        }
    }

    cout<<ans<<endl;

}

