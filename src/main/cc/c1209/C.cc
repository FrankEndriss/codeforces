/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);

    vi av(n);
    vi ans(n);
    vpii fila(10, { n+1, 0 });

    cins(s);
    for(int i=0; i<n; i++) {
        int a=s[i]-'0';
        av[i]=a;

        fila[a].first=min(fila[a].first, i);
        fila[a].second=max(fila[a].second, i);
    }

/* greedy: if there is some a[i]>a[j] with i<j then a[i] has to be in second group.
 */
    int mig2=10;
    for(int i=0; i<n; i++) {
        if(av[i]>mig2) {
            ans[i]=2;
            continue;
        }
        ans[i]=1;
        for(int j=0; j<av[i]; j++) {
            if(fila[j].second>i) {
                ans[i]=2;
                mig2=min(mig2, av[i]);
                break;
            }
        }
    }

    // check g2 
    bool possi=true;
    int prev=0;
    for(int i=0; i<n; i++) {
        if(ans[i]==1)
            continue;

        if(av[i]<prev) {
            possi=false;
            break;
        }
        prev=av[i];
    }

    if(!possi) {
        cout<<"-"<<endl;
    } else {
        for(int i=0; i<n; i++) {
            cout<<ans[i];
        }
        cout<<endl;
    }
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);    
    while(t--)
        solve();

}

