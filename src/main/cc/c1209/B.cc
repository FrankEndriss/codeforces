/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cins(s);
    vi a(n);
    vi b(n);
    const int MAX=150;
    vi dp(MAX);

    for(int i=0; i<n; i++) {
        cin>>a[i]>>b[i];

        if(s[i]=='1')
            for(int j=0; j<b[i]; j++)
                dp[j]++;

        int start=b[i];
        if(s[i]=='1')
            start+=a[i];

        for(int j=start; j<MAX; j+=(2*a[i])) 
            for(int k=0; k<a[i] && j+k<MAX; k++)
                dp[j+k]++;
    }

    int ans=0;
    for(int i=0; i<MAX; i++)
        ans=max(ans, dp[i]);

    cout<<ans<<endl;

}

