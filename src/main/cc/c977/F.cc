/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n;
    cin>>n;
    vector<int> a(n);
    fori(n)
        cin>>a[i];

    map<int, pair<int, int>> m;   // pair<len, idx>
    fori(n) {
        m[a[i]]=make_pair(m[a[i]-1].first+1, i);
    }

    int ans=0;
    int upperidx=0;
    for(auto ment : m) {
        int next=ment.second.first;
        if(next>ans) {
            ans=next;
            upperidx=ment.second.second;
        }
    }

    vector<int> vals;
    int val=a[upperidx];
    vals.push_back(upperidx);
    val--;
    for(int i=upperidx-1; i>=0; i--) {
        if(a[i]==val) {
            vals.push_back(i);
            val--;
        }
    }
    reverse(vals.begin(), vals.end());
    

    cout<<ans<<endl;
    fori(ans)
        cout<<vals[i]+1<<" ";
    cout<<endl;
}

