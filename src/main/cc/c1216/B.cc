/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinai(a, n);
    vi b(n);
    for(int i=0; i<n; i++) 
        b[i]=i;

    sort(b.begin(), b.end(), [&](int i1, int i2) {
        return a[i2]<a[i1];
    });

    ll ans=0;
    for(int x=0; x<n; x++) {
        ans+=a[b[x]]*x+1;
    }
    cout<<ans<<endl;
    for(int i=0; i<n; i++) 
        cout<<(b[i]+1)<<" ";
    cout<<endl;
}

