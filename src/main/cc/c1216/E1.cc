/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MAX=1e9+3;
vll len;
vll dp(10);  // dp[i]= highest index in block i

/* @return digit at pos k of seq "123456789101112..."  1 based. */
void dat(int k) {
//cout<<"dat k="<<k<<endl;
    int block=1;
    while(k>dp[block]) {
        k-=dp[block];
        block++;
//cout<<"dat2 k="<<k<<endl;
    }

    int ten=1;
    for(int i=1; i<block; i++)
        ten*=10;

    ll num=ten+(k-1)/block;
    int dig=(k-1)%block;
    
    /* num is a number of block digits, find dig-th of it. */
    int ans=-1;
    for(int i=0; i<block-dig; i++) {
        ans=num%10;
        num/=10;
    }

//cout<<"dat k="<<k<<" ans="<<ans<<endl;
    cout<<ans<<endl;
}

void solve() {
    cini(k);
    auto it=lower_bound(len.begin(), len.end(), k);
    it--;
    dat(k-(*it));
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(q);

/* size of blocks in dat() */
    dp[0]=0;
    dp[1]=9;
    ll num=100;
    for(ll i=2; i<=9; i++) {
        dp[i]=i*(num-num/10);
        num*=10;
    }

    const int MAX=1e9+7;

    ll len1=0;
    len.push_back(0);
    for(int i=1; i<=9; i++) {
        len1+=1;
        len.push_back(len.back()+len1);
    }
    for(int i=10; i<=99; i++) {
        len1+=2;
        len.push_back(len.back()+len1);
    }
    for(int i=100; i<=999; i++) {
        len1+=3;
        len.push_back(len.back()+len1);
    }
    for(int i=1000; i<=9999; i++) {
        len1+=4;
        len.push_back(len.back()+len1);
    }
//    for(int i=10000; i<=99999; i++) {
    while(len.back()<MAX) {
        len1+=5;
        len.push_back(len.back()+len1);
    }
/*
    for(int i=100000; i<=999999; i++) {
        len1+=6;
        len.push_back(len.back()+len1);
    }
    for(int i=1000000; i<=9999999; i++) {
        len1+=7;
        len.push_back(len.back()+len1);
    }
    for(int i=10000000; i<=99999999; i++)
        len.push_back(len.back()+8);
*/

    while(q--)
        solve();
}

