/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void inters(vll &rA, vll &rB, vll &res) {
    res[0]=max(rA[0], rB[0]);
    res[1]=max(rA[1], rB[1]);
    res[2]=min(rA[2], rB[2]);
    res[3]=min(rA[3], rB[3]);

    if(res[0]>=res[2] || res[1]>=res[3])
        res[0]=res[1]=res[2]=res[3]=0;
}

/* @return the size of the overlapping part of r1 and r2 */
ll overlap(vll &rA, vll &rB) {
    ll XA1=rA[0];
    ll YA1=rA[1];
    ll XA2=rA[2];
    ll YA2=rA[3];
    ll XB1=rB[0];
    ll YB1=rB[1];
    ll XB2=rB[2];
    ll YB2=rB[3];
    
    ll SI = max(0LL, min(XA2, XB2) - max(XA1, XB1)) * max(0LL, min(YA2, YB2) - max(YA1, YB1));
    return SI;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cinall(r1, 4);
    cinall(r2, 4);
    cinall(r3, 4);

    vll r4(4);
    inters(r2, r3, r4);

/* calc overlapping sizes and subtract */
    ll blov=overlap(r1, r4);
    ll wo1=overlap(r1, r2);
    ll wo2=overlap(r1, r3);

    

    ll warea=(r1[3]-r1[1])*(r1[2]-r1[0]);

    if((wo1+wo2-blov-warea ==0)) 
        cout<<"NO"<<endl;
    else
        cout<<"YES"<<endl;


}

