/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int c3(const int l, const int r) {
    int x=l;
    while(x%3)
        x++;
    if(r>=x)
        return 1+(r-x)/3;
    else
        return 0;
}

#define mod 1000000007

int pl(const int i1, const int i2) {
    return (i1+i2)%mod;
}
int mul(const int i1, const int i2) {
    return (int)((1LL * i1 * i2)%mod);
}

// https://codeforces.com/problemset/problem/1105/C
int main() {
int n, l, r;
    cin>>n>>l>>r;
    
    vector<vector<int>> dp(n+99, vector<int>(3));

    int c0=dp[0][0]=c3(l, r);
    int c1=dp[0][1]=c3(l+2, r+2);
    int c2=dp[0][2]=c3(l+1, r+1);
    for(int i=1; i<n; i++) {
        dp[i][0]=pl(mul(dp[i-1][0], c0), pl( mul(dp[i-1][1], c2), mul(dp[i-1][2], c1)));
        dp[i][1]=pl(mul(dp[i-1][1], c0), pl( mul(dp[i-1][2], c2), mul(dp[i-1][0], c1)));
        dp[i][2]=pl(mul(dp[i-1][2], c0), pl( mul(dp[i-1][0], c2), mul(dp[i-1][1], c1)));
    }
    int ans=dp[n-1][0];
    cout<<ans<<endl;

/* The number of possibilities at n-th position is (n-1)*(number of fitting ints).
 */
}

