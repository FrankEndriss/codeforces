
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n;
    cin>>n;
    string a, b;
    cin>>a;
    cin>>b;

    int ans=0;
    for(int i=0; i<n; i++) {
        if(a[i]!=b[i]) {
            if(i<n-1 && a[i]==b[i+1] && a[i+1]==b[i]) 
                i++;
            ans++;
        }
    }
    cout<<ans<<endl;

}

