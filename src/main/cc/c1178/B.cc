/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    string s;
    cin>>s;
    string t;
    for(int i=0;  i<s.size()-1; i++) {
        if(s[i]=='v' && s[i+1]=='v')
            t+='w';
        else if(s[i]=='o')
            t+='o';
    }

    vi os;
    vi ws;
    for(int i=0;  i<t.size(); i++) {
        if(t[i]=='w')
            ws.push_back(i);
        else
            os.push_back(i);
    }

    ll ans=0;
    int left=0;
    for(int oidx : os) {
        while(left<ws.size() && ws[left]<oidx)
            left++;
        ans+=(1LL*left*(ws.size()-left));
    }
    cout<<ans<<endl;
}

