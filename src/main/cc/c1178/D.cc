/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int primeAproxFactor=30;
const int MAXN=100000;
vector<int> pr(MAXN);

void init() {
    vector<bool> cmp(MAXN*primeAproxFactor, false);

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!cmp[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                cmp[j] = true;
                j += i;
            }
        }
        i++;
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
init();
    int n;
    cin>>n;

    int pidx=0;
    while(pr[pidx]<n)
        pidx++;
    
    cout<<pr[pidx]<<endl;
    for(int i=1; i<=n-1; i++) 
        cout<<i<<" "<<i+1<<endl;
    cout<<n<<" 1"<<endl;
    int i=1;
    int j=n;
    while(j<pr[pidx]) {
        cout<<i<<" "<<i+2<<endl;
        j++;
        i++;
        if(j<pr[pidx]) {
            cout<<i<<" "<<i+2<<endl;
            i+=3;
            j++;
        }
    }

}

