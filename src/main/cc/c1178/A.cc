/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vi a(n);
    int sum=0;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        sum+=a[i];
    }
    int maj=sum/2;
    while(maj<=sum/2)
        maj++;

    vi ans;
    ans.push_back(0);
    sum=a[0];
    for(int i=1; i<n; i++) {
        if(sum<maj && a[i]*2<=a[0]) {
            sum+=a[i];
            ans.push_back(i);
        }
    }
    if(sum<maj)
        cout<<"0"<<endl;
    else {
        cout<<ans.size()<<endl;
        for(int p : ans) 
            cout<<p+1<<" ";
        cout<<endl;
    }
}

