/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    string s;
    cin>>s;

/* find palindromic subsequence of min len s.size()/2.
 * No consequtive c in s.
 * Only a, b, c.
 *
 */
    if(s.size()==3) {
        cout<<s.substr(0, 1)<<endl;
        return 0;
    }
    

    int l=0;
    int r=s.size()-1;

    string ans;
    bool leftinc=true;
    while(l+1<r-1) {
        if(s[l]==s[r]) {
            ans+=s[l];
            l++;
            r--;
        } else if(s[l+1]==s[r]) {
            ans+=s[l+1];
            l+=2;
            r--;
        } else if(s[l]==s[r-1]) {
            ans+=s[l];
            l++;
            r-=2;
        } else if(s[l+1]==s[r-1]) {
            ans+=s[l+1];
            l+=2;
            r-=2;
        } else
            assert(false);
    }

    string lstr;
    lstr+=ans;
    lstr+=s[l];
    reverse(ans.begin(), ans.end());
    lstr+=ans;
    if(lstr.size()<s.size()/2) {
        cout<<"IMPOSSIBLE"<<endl;
    } else {
        cout<<lstr<<endl;
    }

}

