/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    string s;
    cin>>s;
    int n=(int)s.size();

    vector<int> a(n);
    fori(n)
        a[i]=(int)(s[i]-'0');

/* if s[i]%3==0 cut before it, no matter whatsever.
 * else, add s[i], if sum%3, cut after.
 * So after, zsum==0 or zsum%3!=0.
 */

/** following works, but I want to try dp solution
    int zsum=0;
    int lenz=0;
    int ans=0;

    fori(n) {
        if(a[i]%3==0) {
            ans++;
            zsum=0;
            lenz=0;
        } else  {
            zsum+=a[i];
            lenz++;
            if(zsum%3==0 || lenz==3) {
                ans++;
                zsum=0;
                lenz=0;
            }
        }
    }
*/
    vector<int> dp(n+4);  // max count of 3d up to index idx
    fori(n) {
        dp[i+3]=dp[i+2];
        if(a[i]%3==0)
            dp[i+3]=max(dp[i+3], dp[i+2]+1);

        if(i>0 && (a[i]+a[i-1])%3==0)
            dp[i+3]=max(dp[i+3], dp[i+1]+1);

        if(i>1)
            dp[i+3]=max(dp[i+3], dp[i]+1);
    }

    cout<<dp[n+2]<<endl;
}

