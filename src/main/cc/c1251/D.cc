/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);    // n is odd
    cinll(s);
//cout<<"solve for n="<<n<<" s="<<s<<endl;
    vector<pii> sal(n);
    ll mi=0;
    for(int i=0; i<n; i++)  {
        cin>>sal[i].first>>sal[i].second;
        mi+=sal[i].first;
    }

    const int INF=1e9;
    sort(sal.begin(), sal.end(), [&](pii p1, pii p2) {
        if(p1.second==p2.second)
            return p1.first<p2.first;
        return p1.second<p2.second;
    });
    ll ans=sal[n/2].second;     // max r of r-median, we can never go over that value.
//cout<<" ma ans="<<ans<<endl;

    ll opt= s-mi;       // optional money
    sort(sal.begin(), sal.end());
    /* now provide the optional money to the medians */

    ll med=sal[n/2].first;
    int cnt=1;
    for(int i=n/2+1; i<n; i++) {
        int next=sal[i].first;
        ll bonus=min((opt/cnt)*cnt, (next-med)*cnt);
        med+=bonus/cnt;
        opt-=bonus;
        if(med<next)
            break;
        cnt++;
    }
    if(med==sal[n-1].first)
        med+=(opt/cnt);

    cout<<min(med, ans)<<endl;

}


int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

