/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    vector<pii> pm(n);
    for(int i=0; i<n; i++)
        cin>>pm.first>>pm.second;

    sort(pm.begin(), pm.end());     // by cost

    vi pid(n);
    iota(pid.begin(), pid.end(), 0);
    sort(pid.begin(), pid.end(), [&](int i1, int i2) {  // by inlueziability
        if(pm[i1].second==pm[i2].second)
            return pm[i1].first<pm[i2].first;

        return pm[i1].second<pm[i2].second;
    });

    /* greedy.
     * By cheapest voters until we get the min m for free, take what we get for free
     * and go on by more of the cheapest... But do not by the ones we get for free.
     */
    vb vis(n);
    int midx=0;
    int bought=0;
    ll cost=0;

    function<void()> getfree=[&]() {
        while(midx<n && pm[pid[midx]]<=bought) {
            vis[pid[midx]]=true;
            bought++;
            midx++;
        }
    }

/* number of voters we have to buy to get the next one for free */
    int needBuy=pm[pid[midx]].second-(midx+1);
/* which one to buy ??? 
 * How to find if buying one is cheaper than buying the next instead?
 **/


    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;
        cost+=pm[i];

        // need to buy pm[i]
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

