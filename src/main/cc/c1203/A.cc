
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cinai(a, n);

    bool ordup=false;
    bool orddo=false;
    if(n<=2) {
        ordup=true;
        orddo=true;
    } else {
        ordup=a[0]==a[1]-1 || a[1]==a[2]-1;
        orddo=a[0]==a[1]+1 || a[1]==a[2]+1;
    }
    if(ordup) {
        for(int i=0; i<n-1; i++) {
            if(a[i]+1==a[i+1] || (a[i]==n && a[i+1]==1))
                ; // ok
            else {
                cout<<"NO"<<endl;
                return;
            }
        }
        if(a[n-1]+1==a[0] || (a[n-1]==n && a[0]==1))
            ; // ok
        else {
                cout<<"NO"<<endl;
                return;
        }
    }

    if(orddo) {
        for(int i=0; i<n-1; i++) {
            if(a[i]-1==a[i+1] || (a[i]==1 && a[i+1]==n))
                ; // ok
            else {
                cout<<"NO"<<endl;
                return;
            }
        }
        if(a[n-1]-1==a[0] || (a[n-1]==1 && a[0]==n))
            ; // ok
        else {
                cout<<"NO"<<endl;
                return;
        }
    }
    cout<<"YES"<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);   
    cini(t);
    while(t--)
        solve();
}

