/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(r);

    vi a(n);
    vi b(n);

    vi posb;
    vi negb;

    for(int i=0; i<n; i++) {
        cin>>a[i]>>b[i];
        if(b[i]>=0)
            posb.push_back(i);
        else
            negb.push_back(i);
    }

    sort(posb.begin(), posb.end(), [&](int i1, int i2) {
        return a[i1]<a[i2];
    });

    int ans=0;
    for(int i=0; i<posb.size(); i++) {
        if(r<a[posb[i]]) {
            break;
        }
        ans++;
        r+=b[posb[i]];
    }
cout<<"dbg ans="<<ans<<endl;

/* how to solve most negb?
 * Among the possible negb, choose the cheapest one.
 * Repeat.
 * TODO that does not work... we need more kind of knapsack or the like
 */
    sort(negb.begin(), negb.end(), [&](int i1, int i2) {
        return a[i1]<a[i2];
    });
    while(true)  {
        while(negb.size()>0 && a[negb.back()]>r)
            negb.pop_back();
        if(negb.size()==0)
            break;
        auto it=max_element(negb.begin(), negb.end(), [&](int i1, int i2) {
            return b[i1]<b[i2];
        });
        ans++;
        r+=b[*it];  
        negb.erase(it);
    }
    cout<<ans<<endl;
}

