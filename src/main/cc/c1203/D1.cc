/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cins(s);   
    cins(t);
    if(s==t) {
        cout<<0<<endl;
        return 0;
    }

/* we need to find the leftmost part of t[0,i]
 * and the rightmost t[i+1,n] for any i;
 * ans is the max gap between them.
 */

    int ans=0;
    for(int i=0; i<t.size(); i++) { // len of left part== i
        /* match left part in s from left */
        int s1idx=0;
        int t1idx=0;
        while(t1idx<i) {
            while(s[s1idx]!=t[t1idx])
                s1idx++;
            t1idx++;
        }

        int s2idx=s.size()-1;
        int t2idx=t.size()-1;
        while(t2idx>=i) {
            while(s[s2idx]!=t[t2idx])
                s2idx--;
            t2idx--;
        }
        ans=max(ans, s2idx-s1idx);
    }
    cout<<ans<<endl;
}

