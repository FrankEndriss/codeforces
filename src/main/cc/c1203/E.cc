/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    map<int, int> f;
    for(int i=0; i<n; i++) {
        cini(a);
        f[a]++;
    }

    set<int> s;
    for(auto ent : f) {
        if(ent.second==1) {
            s.insert(ent.first);
        }
        if(ent.second==2) {
            if(ent.first>1 && s.count(ent.first-1)==0) {
                s.insert(ent.first-1);
                s.insert(ent.first);
            } else {
                s.insert(ent.first);
                s.insert(ent.first+1);
            }
        }
        if(ent.second>2) {
            if(ent.first>1)
                s.insert(ent.first-1);
            s.insert(ent.first);
            s.insert(ent.first+1);
        }
    }
    cout<<s.size()<<endl;
}

