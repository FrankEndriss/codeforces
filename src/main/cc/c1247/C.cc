/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(p);

/** repeated summands are allowed.
 * if p is negative.
 * -> how can we find that a number is not possible?
 *  We can allways choose some huge 2^^x, and find some multiple so that any sum 
 *  can be build. So for p<-1 a solution must allways exist.
 *  And for -1, 0, too, since the smalles values are 1 which is good for any sum.
 */
    if(p>=0) {
    int ans=0;
    for(int i=29; i>=0; i--) {
        int pnum=p+(1<<i);
        if(pnum<=0)
            break;

        if(n>=pnum) {
            ans++;
            n-=(pnum);
        }
        if(n==0)
            break;
    }
    if(n!=0)
        cout<<-1<<endl;
    else
        cout<<ans<<endl;
    } else {
        // p<0
    }
    

}

