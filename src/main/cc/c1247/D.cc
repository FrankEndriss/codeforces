/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll INF=1LL*1e5*1e5+7;

ll xpk(int x, int k) {
    ll ans=1;
    for(int i=0; i<k; i++) {
        ans*=x;
        if(ans>=INF)
            return INF;
    }
    return ans;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(k);
    cinai(a,n);
    set<pii> s; // s[j]= { a[i],i }
    for(int i=0; i<n; i++)
        s.insert({a[i],i});

    if(k>31) {
        cout<<0<<endl;
        return 0;
    }
    
// k==2 ???
    vll p;
    for(int x=1; xpk(x,k)<INF; x++) {
        p.push_back(xpk(x,k));
    }

/* What about p.size()>10...
 * That is for k=2 or k=3. k=4 -> p.size()=15
 * What to do for k=2 ???
 */

    ll ans=0;
// a[i]*a[j] in p
    for(int i=0; i<n; i++) {
        for(ll px : p) {
            if(px%a[i]!=0)
                continue;

            ll jsearch=px/a[i];

            auto it=s.upper_bound({jsearch,-1});
            while(it!=s.end() && it->first==jsearch) {
                if(it->second>i)
                    ans++;
                it++;
            }
        }
    }

    cout<<ans<<endl;

}

