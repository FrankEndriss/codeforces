/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

void solve() {
    string s, t, p;
    cin>>s>>t>>p;

    for(int idx=0; idx<t.size(); idx++) {
        if(s.size()>idx && s[idx]==t[idx])
            continue;

        auto pos=p.find(t[idx]);
        if(pos==string::npos) {
            cout<<"NO"<<endl;
            return;
        }
        s.insert(idx, 1, t[idx]);
        p.erase(pos, 1);
    }
    if(s==t)
            cout<<"YES"<<endl;
    else
            cout<<"NO"<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}
