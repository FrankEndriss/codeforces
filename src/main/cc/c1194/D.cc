/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

const string A="Alice";
const string B="Bob";

void solve() {
    ll n, k;
    cin>>n>>k;

    if(n==0) {
        cout<<B<<endl;
        return;
    }

    if(n==1 || n==2 || n==k) {
        cout<<A<<endl;
        return;
    } else if(n<k) {
        cout<<B<<endl;
        return;
    }

    if(k%3!=0) {
        n%=3;
        if(n==2)
            cout<<B<<endl;
        else
            cout<<A<<endl;
        return;
    }

    if(n==k || (n-k)%(k+1)==0) {
        cout<<A<<endl;
    }
    n%=3;
    if(n==2)
        cout<<B<<endl;
    else
        cout<<A<<endl;
    return;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

