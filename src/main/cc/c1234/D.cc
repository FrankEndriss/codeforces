/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

//#define DEBUG

const int INF=1e9+7;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cins(s);
    cini(q);

    vector<set<int>> dp('z'-'a'+1);
    for(int i=0; i<='z'-'a'; i++) {
        dp[i].insert(INF);
    }

    for(size_t i=0; i<s.size(); i++)
        dp[s[i]-'a'].insert((int)i);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(idx);
            idx--;
            cins(str);

            dp[s[idx]-'a'].erase(idx);
            s[idx]=str[0];
            dp[s[idx]-'a'].insert(idx);

        } else {
            cini(l);
            l--;
            cini(r);
            r--;
#ifdef DEBUG
cout<<"l="<<l<<" r="<<r<<endl;
cout<<s.substr(l, r-l+1)<<endl;
#endif

            int ans=0;
            for(size_t j=0; j<dp.size(); j++) {
                //int val=*lower_bound(dp[j].begin(), dp[j].end(), l);
                int val=*dp[j].lower_bound(l);
                //if(it!=dp[j].end()) {
                    if(val<=r)
                        ans++;
                //}
            }
            cout<<ans<<endl;
        }
    }

}

