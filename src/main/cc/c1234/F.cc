/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cins(s);

    const int N=100001;
    vvi dp(N, vi(20));
    vi dp2(N);  // dp2[i]=length of longtest distinct substring starting at i

    for(size_t i=0; i<s.size(); i++) {
        dp[i][0]=(1<<(s[i]-'a'));
        dp2[i]=1;
        for(size_t j=1; j<20; j++) {
            if(i+j<s.size()) {
                dp[i][j]=dp[i][j-1]|(1<<(s[i+j]-'a'));
                if(dp[i][j]==dp[i][j-1])
                    break;
                dp2[i]++;
            }
        }
    }

/*
    for(size_t i=0; i<s.size(); i++) {
        for(int j=dp2[i]-1; j>=0; j--) {    
            for(size_t k=i+j; k<s.size(); k++) {
                for(int k2=0; k2<20-j; k2++) {
                    if(dp[i][j] & dp[k][k2]==0) {
                        int x=dp[i][j]|dp[k][k2];
                        if(x>ma) {
                            ma=x;
                            idx1=i;
                            len1=j;
                            idx2=k;
                            len2=k;
                        }
                    }
                }
            }
        }
    } 
*/
    vector<pii> dp3(N);
    queue<pii> q;
    int val=0;
    val=(1<<(s[0]-'a'));
    q.push({ val, 0 });
    for(size_t i=1; i<s.size(); i++) {
        val=(1<<(s[i]-'a'));
        while(q.front()&val) {
            pii f=q.front();
            dp3[f.first]=i-q.size();
            q.pop();
        }
    }
    

    

}

