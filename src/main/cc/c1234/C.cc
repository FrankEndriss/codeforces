/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int DRIGHT=1;
const int DDOWN=2;
const int DUP=3;
const int DLEFT=4;

//#define DEBUG

/* return direction of pip out if dir
 * of in is din.
 */
int getDir(int din, int pip, int row) {
#ifdef DEBUG
cout<<"getDir din="<<din<<" pip="<<pip<<endl;
#endif

    if(pip==1 || pip==2) {
        return din;
    } else if(pip==3 || pip==4) {
        if(din==DRIGHT) {
            if(row==0)
                return DDOWN;
            else
                return DUP;
        }
        else if(din==DDOWN || din==DUP)
            return DRIGHT;
    } else if(pip==5 || pip==6) {
        if(din==DRIGHT) {
            if(row==0)
                return DDOWN;
            else
                return DUP;
        } else if(din==DDOWN || din==DUP)
            return DRIGHT;
    }
    assert(false);
}


void solve() {
    cini(n);
#ifdef DEBUG
cout<<"solve(), n="<<n<<endl;
#endif
    vi dp(n);
    vvi a(n, vi(2));
    cins(s1);
    cins(s2);

    for(int i=0; i<n; i++)
        a[i][0]=s1[i]-'0';

    for(int i=0; i<n; i++)
        a[i][1]=s2[i]-'0';


    int row=0;
    int i=0;
    bool ok=false;
    int ndir=DRIGHT;
    while(true) {
#ifdef DEBUG
cout<<"while i="<<i<<" row="<<row<<" n="<<n<<" dp.size()="<<dp.size()<<endl;
#endif
        if(dp[i]>3)
            break;  // endless loop
        dp[i]++;

        ndir=getDir(ndir, a[i][row], row);
        if(ndir==DDOWN)
            row++;
        else if(ndir==DUP)
            row--;
        else if(ndir==DRIGHT)
            i++;

#ifdef DEBUG
cout<<"i="<<i<<" ndir="<<ndir<<" row="<<row<<endl;
#endif
        if(row<0 || row>1)
            break;

        if(i==n) {
            if(row==1)
                ok=true;
            break;
        }
    }
    if(ok)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(q);
    while(q--)
        solve();
}

