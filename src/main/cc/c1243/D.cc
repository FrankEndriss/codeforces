/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);    // num weight 1

/* we need to find the nodes part of components of 0-edges.
 * All other need to be connected by 1-edges.
 * So, weight of MST is number of 0-components minus 1.
 */
    vector<vector<int>> ones(n);
    for(int i=0; i<m; i++) {
        cini(a);
        cini(b);
        a--;
        b--;
        if(b>a)
            ones[a].push_back(b);
        if(a>b)
            ones[b].push_back(a);
    }
    for(int i=0; i<n; i++) {
        sort(ones[i].begin(), ones[i].end());
    }

    vb vis(n, false);
    function<void(int)> dfs=[&](int node) {
        vis[node]=true;

        size_t idx=0;
        bool canExit=false;
        for(int i=node+1; i<n; i++) {
            if(idx<ones[node].size() && ones[node][idx]==i) {
                if(!vis[i])
                    canExit=true;
                idx++;
            } else {
                if(!vis[i]) {
                    dfs(i);
                    if(canExit)
                        return;
                }
            }
        }
    };

    int ans=0;
    for(int i=0; i<n; i++) {
        if(!vis[i]) {
            ans++;
            dfs(i);
        }
    }
    cout<<ans-1<<endl;
}

