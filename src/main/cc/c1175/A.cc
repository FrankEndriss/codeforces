/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    ll t, n, k;
    cin>>t;

    while(t--) {
        cin>>n>>k;
        ll ans=0;
        while(n>=k) {
//cout<<"n="<<n<<" ans="<<ans<<endl;
            if(n%k==0) {
                n/=k;
                ans++;
            } else {
                ll d=n%k;
                n-=d;
                ans+=d;
            }
        }

        ans+=n;
        cout<<ans<<endl;
    }



}

