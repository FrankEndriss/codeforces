/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)


int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n;
    cin>>n;

    struct sent {
        ll adds;
        ll loop;
    };

    stack<sent> st;
    st.push({ 0, 1 });
    for(int i=0; i<n; i++) {
        string s;
        cin>>s;
        if(s=="add")
            st.top().adds++;
        else if(s=="for") {
            int loops;
            cin>>loops;
            st.push({ 0, loops });
        } else if(s=="end") {
            sent ent=st.top();
//cout<<"end; adds="<<ent.adds<<" loop="<<ent.loop<<endl;
            st.pop();
            st.top().adds+=ent.adds*ent.loop;
        }
        if(st.top().adds>=2LL<<31) {
            cout<<"OVERFLOW!!!"<<endl;
            return 0;
        }
    }

    cout<<st.top().adds<<endl;
    return 0;


}

