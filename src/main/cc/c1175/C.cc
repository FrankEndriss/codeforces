/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

void solve() {
    int n, k;
    cin>>n>>k;
    vector<ll> a(n);
    fori(n)
        cin>>a[i];

    sort(a.begin(), a.end());

/* simply find the mid of k+1 consecutive points. */
    ll minD=(a[k]-a[0]+1)/2;
    ll minX=(a[k]+a[0])/2;
    for(int i=k+1; i<n; i++) {
        ll d=(a[i]-a[i-k]+1)/2;
        if(d<minD) {
            minX=(a[i-k]+a[i])/2;
            minD=d;
        }
    }

    cout<<minX<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--) {
        solve();
    }
}

