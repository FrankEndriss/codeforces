/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, m;
    cin>>n>>m;
    vector<int> a(m), b(m), id(m);
    set<pair<int, int>> ab;
    fori(m) {
        int a, b;
        cin>>a>>b;
        if(a>b) {
            int tmp=a;
            a=b;
            b=tmp;
        }
        ab.insert(make_pair(a, b));
    }

    list<int> l;    // rotations
    fori(n-1) {
        if(n%(i+1)==0)
            l.push_back(i+1);
    }

    for(auto &seg : ab) {
        for(auto it=l.begin(); it!=l.end(); ) {
            /* find if segment m exists rotated by positions *it.  if not, remove *it from l. */
            int a=(seg.first+*it-1)%n+1;
            int b=(seg.second+*it-1)%n+1;
            if(a>b) {
                int tmp=a;
                a=b;
                b=tmp;
            }
//cout<<"check rot="<<*it<<endl;
            if(ab.count(make_pair(a, b))==0) {
//cout<<"erase rot="<<*it<<endl;
                it=l.erase(it);
            } else
                ++it;
        }
    }
    bool ans=l.size()>0;
    cout<<(ans?"Yes":"No")<<endl;
    
}

