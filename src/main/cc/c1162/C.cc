/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n, k;
    cin>>n>>k;
    vector<int> x(k);
    unordered_set<int> o;
    set<pair<int, int>> lr;

    fori(k)
        cin>>x[i];

    for(int i=1; i<=n; i++) {
        if(i>1)
            lr.insert(make_pair(i, i-1));
        lr.insert(make_pair(i, i));
        if(i<n)
            lr.insert(make_pair(i, i+1));
    }

    /* Note A cant stay on X fields, but can move all other moves. First and last only one adj. */
    ll ans=0;
    forn(j, k) {
        int i=x[j];
        auto p2=make_pair(i, i);
        lr.erase(p2);

        if(o.count(i-1)) {
            auto p1=make_pair(i-1, i);
            lr.erase(p1);
        }

        if(o.count(i+1)) {
            auto p3=make_pair(i+1, i);
            lr.erase(p3);
        }

        o.insert(i);
    }
    ans=lr.size();

    cout<<ans<<endl;
}

