/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<vector<bool>> grid(n, vector<bool>(m, false)); /* marked cells, start 0,0 */
    set<pair<int, int>> moves;  /* made moves */

    /* greedy, try to jump to the farthest cell allways, manhattan dist. */
    vector<int> freeN(n, m);
    vector<int> freeM(m, n);
    vector<pair<int, int>> ans;
    ans.push_back({ 0, 0});

    grid[0][0]=true;
    freeN[0]--;
    freeM[0]--;
    int c=1;
    int currN=0;
    int currM=0;
    while(c<n*m) {
        int nextN=0;
        int nDir=0;
        if(currN<=n/2) {
            nDir=-1;
            for(nextN=n-1; nextN>=0; nextN--)
                if(freeN[nextN]>0)
                    break;
            assert(nextN>=0);
        } else {
            nDir=1;
            for(nextN=0; nextN<n; nextN++)
                if(freeN[nextN]>0)
                    break;
            assert(nextN<n);
        }

        int nextM=0;
        int mDir=0;
        if(currM<=m/2) {
            nextM=m-1;
            mDir=-1;
        } else {
            nextM=0;
            mDir=1;
        }
        while(true) {
            if(nextN<0 || nextN>=n || nextM<0 || nextM>=m) {
                cout<<-1<<endl;
                return 0;
            }
            assert(nextN>=0);
            assert(nextN<n);
            assert(nextM>=0);
            assert(nextM<m);
            pair<int, int> lmove= { nextN-currN, nextM-currM };
            if((!grid[nextN][nextM]) && moves.count(lmove)==0) {
#ifdef DEBUG
cout<<"nextN="<<nextN<<" nextM="<<nextM<<" m.f="<<lmove.first<<" m.s="<<lmove.second<<endl;
#endif
                moves.insert(lmove);
                ans.push_back({ nextN, nextM});
                freeN[nextN]--;
                freeM[nextM]--;
                grid[nextN][nextM]=true;
                c++;
                break;
            } else {
                nextM+=mDir;
                if(nextM<0) {
                    nextM=m-1;
                    nextN+=nDir;
                } else if(nextM==m) {
                    nextM=0;
                    nextN+=nDir;
                }
            }
        }
        currN=nextN;
        currM=nextM;
    }

    for(uint i=0; i<ans.size(); i++)
        cout<<ans[i].first+1<<" "<<ans[i].second+1<<endl;
    

}

