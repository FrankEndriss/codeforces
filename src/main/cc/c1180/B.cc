/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> a(n);
    int cneg=0;


    fori(n) {
        cin>>a[i];
        if(a[i]<0)
            cneg++;
        else if(a[i]==0) {
            a[i]=-1;
            cneg++;
        }
    }

    if(n==1) {
        if(a[0]<0)
            a[0]=-a[0]-1;
        cout<<a[0]<<endl;
        return 0;
    }

/* if there is a even number of elements, swap all to negativ.
 * else do not swap the biggest one.
 */
    int notSwap=-1;
    if(n%2) {
        int mx=0;
        fori(n) {
            if(a[i]>mx) {
                mx=a[i];
                notSwap=i;
            }
        }
    }

    for(int i=0; i<n; i++) {
        if(a[i]>0 && i!=notSwap) {
            a[i]=-a[i]-1;
        }
    }

    fori(n)
        cout<<a[i]<<" ";
    cout<<endl;

}

