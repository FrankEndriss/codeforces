/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, q;
    cin>>n>>q;
    deque<int> a;
    int mx=-1;
    int mxidx=-1;
    fori(n) {
        int x;
        cin>>x;
        a.push_back(x);
        if(x>mx) {
            mx=x;
            mxidx=i;
        }
    }

    int c=0;
    vector<pair<int, int>> start;

    while(a.front()!=mx) {
#ifdef DEBUG
        cout<<"a.front()="<<a.front()<<endl;
#endif
        int A=a.front();
        a.pop_front();
        int B=a.front();
        a.pop_front();
        start.push_back({A, B});
        if(A>B) {
            a.push_front(A);
            a.push_back(B);
        } else {
            a.push_front(B);
            a.push_back(A);
        }
        c++;
    }

#ifdef DEBUG
    cout<<"c="<<c<<" mxidx="<<mxidx<<endl;
#endif
    assert(c==mxidx);

    vector<int> v;
    while(a.size()>0) {
        int x=a.front();
        a.pop_front();
        v.push_back(x);
    }

    /* after c operations mx Element is at front. Then the order does not change any more. */
    fori(q) {
        ll m;
        cin>>m;
        m--;

        if(m<c) {
            cout<<start[(int)m].first<<" "<<start[(int)m].second<<endl;
        } else {
            m-=c;
            m%=(n-1);
            int A=v[0];
            int B=v[(int)m+1];
            cout<<A<<" "<<B<<endl;
        }
    }
}

