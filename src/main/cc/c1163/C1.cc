/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}

int main() {
int n;
    vector<int> x(n), y(n);
    vector<map<int, vector<int>>> pitch;   // pitches of lines from p[i] to all other points
        // note that two lines throug p[i] with same pitch is same line

    fori(n)
        cin>>x[i]>>y[i];

    /* 1. Find number of lines, and pitch of each line. */
    for(int i=0; i<n; i++)   {
        for(int j=i+1; j<n; j++)  {
            int pit=fraction(y[j]-y[i], x[j]-x[i]);
            pitch[i][pit].push_back(j);
            pitch[j][pit].push_back(i);
        }
    }

    unordered_map<int, int> lines;    // maps pitches to number of lines whit this pitch
    vector<vector<bool>> vis(n, vector<bool>(n, false));
    fori(n) {
        for(auto p : pitch[i]) {
            int pit=p.first;
            vector<int> points=p.second;
        }
    }

    

    /* 2. Calc number of crossings from numbers of lines and number of parallel lines. */

}

