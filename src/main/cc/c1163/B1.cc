/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n;
    cin>>n;
    vector<int> u(n);
    vector<int> f(10);
    map<int, int> freqs; // maps a frequency to the number of ribbons with that freq
    
    int ans=0;
    fori(n) {
        cin>>u[i];
        if(freqs.count(f[u[i]])>0) {
            freqs[f[u[i]]]--;
            if(freqs[f[u[i]]]==0)
                freqs.erase(f[u[i]]);
        }
        f[u[i]]++;
        freqs[f[u[i]]]++;

//cout<<"i="<<i<<" freqs.size()="<<freqs.size()<<endl;
        if(freqs.size()==2) {
            int f1=(*freqs.begin()).second;
            auto sec=freqs.begin();
            sec++;
            int f2=(*sec).second;
            if(f1==1 || f2==1)
                ans=i;
        }
    }

    cout<<ans+1<<endl;

}

