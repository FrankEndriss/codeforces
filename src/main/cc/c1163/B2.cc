/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n;
    cin>>n;
    vector<int> f(100001);      // freq of color i
    vector<int> ff(100001);     // freq of freq i
    set<int> ffset;   // set of distinct value s in ff, ie existing frequencies
    
    int ans=0;
    fori(n) {
        int u;
        cin>>u;

        if(ff[f[u]]>0) {
            ff[f[u]]--;
            if(ff[f[u]]==0)
                ffset.erase(f[u]);
        }

        f[u]++;
        ff[f[u]]++;
        ffset.insert(f[u]);

//cout<<"i="<<i<<" ffset.size()="<<ffset.size()<<endl;
        if(ffset.size()==2) {
            int f1=(*ffset.begin());
            auto sec=ffset.begin(); sec++;
            int f2=(*sec);
            int fCount1=ff[f1]; // count of colors wich occur f1 times
            int fCount2=ff[f2];
//cout<<"f1="<<f1<<" fCount1="<<fCount1<<" f2="<<f2<<" fCount2="<<fCount2<<endl;
            if((f1==1 && fCount1==1) || (f2==1 && fCount2==1) ) // Every color has the same occurrence of cats, except for exactly one color which has the occurrence of 1
                ans=i;
            else if((f1-f2==1 && fCount1==1) || (f2-f1==1 && fCount2==1)) 
                ans=i;
        } else if(ffset.size()==1) {
            auto f1=(*ffset.begin());
            if(ff[f1]==1)     // 1. only one color in this streak
                ans=i;
            else if(f1==1)  // 2. All appeared colors in this streak have the occurrence of 1 (i.e. every color has exactly 1 cat with that color).
                ans=i;
        }

//cases
// 1. one frequency, its == 1
// 2. two frequencies, one of them == 1
// 3. two frequencies, one of them == other+1
    }

    cout<<ans+1<<endl;

}

