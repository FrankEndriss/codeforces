/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, m;
    cin>>n>>m;
    vector<int> b(n);
    vector<int> g(m);
    fori(n)
        cin>>b[i];
    fori(m)
        cin>>g[i];

    if(*min_element(g.begin(), g.end())<*max_element(b.begin(), b.end())) {
        cout<<"-1"<<endl;
        return 0;
    }

    sort(g.begin(), g.end());
    reverse(g.begin(), g.end());

    sort(b.begin(), b.end());
    reverse(b.begin(), b.end());

    ll ans=0;
    int maxB=b[0];
    for(int i=0; i<n; i++) {
        // boy i presented min b[i] sweets to any girl!
        ans+=b[i]*m;

        int extra=0;
        if(i<m) { // need to satisfy girl[i]
            if(g[i]>b[i])
                extra=g[i]-b[i];
/* this is not correct, because g[i] could have got the sweets from previous boy, too. See last example. */
        }
        ans+=extra;
    }
    
    cout<<ans<<endl;

}

