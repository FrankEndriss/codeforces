/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, k;
    cin>>n>>k;

    if(n==k) {
        fori(n)
            cout<<"1";
        cout<<endl;
        return 0;
    }
    
    if(n/2+1==k) {
        bool flip=true;
        fori(n) {
            cout<<(flip?"1":"0");
            flip=!flip;
        }
        return 0;
    }
    if(k>n/2) {
        exit(1);
    }

    string muniqs="1";
    fori(k-1)
        muniqs+="0";

    string ans=muniqs.substr(0, k-1)+muniqs;

    while(ans.length()<n) {
        ans="0"+ans;
    }
    cout<<ans<<endl;

}

