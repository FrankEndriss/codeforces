/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n;
    cin>>n;
string s;
    cin>>s;


    int ans=0;
    for(auto c : s) {
        if(c=='-')
            ans=max(0, ans-1);
        else
            ans++;
    }

    cout<<ans<<endl;

}

