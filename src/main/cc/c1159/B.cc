/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n; cin>>n;
    //int miEle=1000000000;
    //int miIdx=-1;
    int ans=1000000000;
    fori(n) {
        int a;
        cin>>a;
        int maInd=max(i, n-i-1);
        ans=min(ans, a/maInd);
    }

    cout<<ans<<endl;

}

