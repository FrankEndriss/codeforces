/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    int z[]= { 4, 8, 15, 16, 23, 42 };

    vector<int> a(n);
    //vector<int> f(6);
    fori(n) {
        cin>>a[i];
        for(int j=0; j<6; j++) {
            if(a[i]==z[j]) {
                a[i]=j;
                break;
            }
        }
        //f[a[i]]++;
    }

/*
    int ma=n;   // max repititions  of whole 6 elements
    for(int i=0; i<6; i++)
        ma=min(ma, f[i]);
*/

    vector<int> f2(6); // count how much seqs we can build
    for(int i=0; i<n; i++) {
        if(a[i]==0)
            f2[0]++;
        else if(f2[a[i]-1]>f2[a[i]]) {
            f2[a[i]]++;
        }
    }

    int ans=n-6*f2[5];
    cout<<ans<<endl;

}

