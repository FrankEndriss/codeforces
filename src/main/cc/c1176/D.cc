/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

const int primeAproxFactor=30;
const int MAXN=200002;
vector<int> pr(MAXN);

void init() {
    vector<bool> cmp(MAXN*primeAproxFactor, false);

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!cmp[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                cmp[j] = true;
                j += i;
            }
        }
        i++;
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    init();
    int n;
    cin>>n;
    vector<int> p(2*n);
    fori(2*n)
    cin>>p[i];

    /* We need to find pairs.
     * (Any prime, pr[prime])
     * (notprime, biggestDivisor)
     *
     * Sort, start with biggest
     * 1. match primes
     * 2. Find matching divisor
     */

    sort(p.begin(), p.end());
    vector<bool> vis(2*n, false);

    for(int i=2*n-1; i>=0; i--) {
        if(vis[i])
            continue;

        vis[i]=true;

        auto it=lower_bound(pr.begin(), pr.end(), p[i]);
        if(*it==p[i]) { // p[i] is constructed prime
            int otherPr=distance(pr.begin(), it)+1; // ...and was matched by this one
            int idx=distance(p.begin(), lower_bound(p.begin(), p.end(), otherPr));
            assert(otherPr==p[idx]);
            while(vis[idx]) {
                idx++;
                assert(otherPr==p[idx]);
            }
            vis[idx]=true;
            cout<<p[idx]<<" ";
            continue;
        } // else p[i] is not prime

        cout<<p[i]<<" ";

        // find biggest divisor and mark it
        int j=i-1;
        for( ; j>=0; j--) {
            if(!vis[j] && p[i]%p[j]==0) {
                vis[j]=true;
                break;
            }
        }
    }
    cout<<endl;


}

