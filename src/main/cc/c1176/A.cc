/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n;
    cin>>n;

    fori(n) {
        int ans=0;
        ll x;
        cin>>x;
        while(true) {
            if(x%2==1 && x%3==0) {
                ans++;
                x*=2;
                x/=3;
            } else if(x%2==0) {
                ans++;
                x/=2;
            } else if(x%5==0) {
                ans++;
                x*=4;
                x/=5;
            } else
                break;
        }

        if(x==1)
            cout<<ans<<endl;
        else
            cout<<-1<<endl;
    }

}

