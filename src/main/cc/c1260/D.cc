/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(m);    // # soldiers
    cini(n);    // boss position
    cini(k);    // # traps
    cini(t);    // max time
    cinai(a,m); // soldiers agility

/* Disarming a trap costs time if r is bigger than l, since we need
 * to move to r, then back to l, then we can move the squad over 
 * the trap.
 * So, since we need to disarm _all_ traps up to a certein level,
 * we need to sort the traps by d desc.
 * Then sum the time it needs to disarm the traps.
 * Result is a max level of armed traps.
 * We can use all soldiers with d >= that level.
 *
 * Note: disarming is more complicated. We can disarm more than one trap 
 * with one run to a r of a trap, since on that way there could be another
 * r.
 * Or, if disarming one trap it could be useful to make few more steps 
 * right to disarm another one, too.
 * How to model that?
 */
    vector<pair<int,pii>> traps(k);  // d,{l,r}

    for(int i=0; i<k; i++)
        cin>>traps[i].second.first>>traps[i].second.second>>traps[i].first;

    sort(traps.begin(), traps.end(), greater<pair<int,pii>>());

    t-=(n+1);   // time needed to move the squad to the boss
    int maxTrap=0;
    for(int i=0; i<k; i++) {
        if(traps[i].second.second*2>t)
            maxTrap=max(maxTrap, traps[i].first);
    }

    int ans=0;
    for(int i=0; i<m; i++)
        if(a[i]>=maxTrap)
            ans++;

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t=1;
    while(t--)
        solve();
}

