/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    for(int i=0; i<n; i++) {
        cini(c);
        cini(sum);
        int ra=min(c,sum);  // # radiators
        int su=sum/ra;      // # min sections per radiator
        int cnt=0;
        if(su*ra < sum)
            cnt=sum%ra;     // # cnt radiators with one more section
        ll ans=0;
        for(int j=0; j<cnt; j++)
            ans+=(su+1)*(su+1);
        for(int j=cnt; j<ra; j++)
            ans+=su*su;

        cout<<ans<<endl;
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t=1;
    while(t--)
        solve();
}

