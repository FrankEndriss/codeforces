/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

template <typename E>
class SegmentTree {
private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

public:
/** SegmentTree. Note that you can hold your data in your own storage and give
 * an Array of indices to a SegmentTree.
 * @param beg, end The initial data.
 * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
 * @param pCumul The cumulative function to create the "sum" of two nodes.
**/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=(int)distance(beg, end);
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Delta-Updates all elements in interval(idxL, idxR]. 
    * iE add 42 to all elements from 4 to inclusive 7: 
    * update(4, 8, 42, [](int i1, int i2) { return i1+i2; });
    */
    void updateRange(int pIdxL, int pIdxR, function<E (E)> apply) {
        // Upate bottom to top
        pIdxL+=treeDataOffset;
        pIdxR+=treeDataOffset;
        for(int i=pIdxL; i<pIdxR; i++)
            tree[i]=apply(tree[i]);

        while (pIdxL != ROOT) {
            pIdxL=parent(pIdxL);
            pIdxR=max(pIdxL, parent(pIdxR-1));
            for(int i=pIdxL; i<=pIdxR; i++)
                tree[i]=cumul(tree[leftChild(i)], tree[rightChild(i)]);
        }
    }

    /** Updates the data at dataIdx to value. */
    void update(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

void solve() {
    int n;
    cin>>n;
    vector<int> a(n), b(n);
    vector<stack<int>> fa(n+1);
    fori(n) {
        cin>>a[i];
        fa[a[i]].push(i);
    }
    fori(n) {
        cin>>b[i];
    }

    SegmentTree<int> segtree(a.begin(), a.end(), 0, [](int i1, int i2) {
        return max(i1, i2);
    });

    /* 
     * for any number in b, starting right, find the "next" left one in a.
     * Check if that one can be moved to current position.
     * Mark it as used.
     * repeat.
     */
    vector<bool> used(n, false);
    for(int i=n-1; i>=0; i--) {
        if(fa[b[i]].size()==0) { // permutations of a dont match b
            cout<<"NO"<<endl;
            return;
        }
        int from=fa[b[i]].top();
        fa[b[i]].pop();

        int max=segtree.get(from, n);
        if(max>b[i]) {
            cout<<"NO"<<endl;
            return;
        }

        segtree.update(from, 0);
    }

    cout<<"YES"<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

