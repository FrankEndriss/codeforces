/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    string s;
    cin>>s;
    int m;
    cin>>m;

    vector<int> f[256];
    for(int i=0; i<s.size(); i++)
        f[s[i]].push_back(i);


    fori(m) {
        string t;
        cin>>t;

        vector<int> idx(256);
        int mx=0;
        for(int j=0; j<t.size(); j++) {
            mx=max(mx, f[t[j]][idx[(int)t[j]]]);
            idx[(int)t[j]]++;
        }
        cout<<mx+1<<endl;
    }

}

