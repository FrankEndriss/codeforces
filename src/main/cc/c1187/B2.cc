#define _CRT_SECURE_NO_WARNINGS
 
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <random>
#include <cmath>
#include <algorithm>
#include <functional>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <unordered_map>
#include <queue>
#include <deque>
#include <stack>
#include <list>
#include <array>
#include <valarray>
#include <cstring>
#include <bitset>
#include <numeric>
#include <cassert>
 
using namespace std;
 
#define X first
#define Y second
#define all(a) a.begin(), a.end()
#define forn(i, n) for(int i = 0; i < int(n); i++) 
 
typedef long double ld;
typedef long long ll;
 
const int INF = 1e9 + 7;
const ld eps = 1e-9;
 
const int A = 26;
 
vector<int> pos[A];
 
int main()
{
  int n;
  cin >> n;
 
  string s;
  cin >> s;
 
  forn(i, n) 
  {
    pos[s[i] - 'a'].push_back(i);
  }
 
  int m;
  cin >> m;
 
  forn(_, m) 
  {
    string t;
    cin >> t;
 
    int ans = -1;
    vector<int> cnt(A);
    for (char c : t)
    {
      cnt[c - 'a']++;
      int id = upper_bound(all(pos[c - 'a']), ans) - pos[c - 'a'].begin();
      if (id >= cnt[c - 'a'])
      {
        continue;
      }
      else 
      {
        ans = pos[c - 'a'][id];
      }
    }
 
    cout << ans + 1 << '\n';
  }
 
}
