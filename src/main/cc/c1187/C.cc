/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;

    vector<pair<int, int>> sorts;
    vector<pair<int, int>> unsorts;

    //vector<bool> sorted(n, false);
    fori(m) {
        int t, l, r;
        cin>>t>>l>>r;

        if(t==1) {
//            for(int j=l; j<=r; j++)
//                sorted[j-1]=true;
            sorts.push_back({ l, r });
        } else {
            unsorts.push_back({ l, r });
        }
    }

    sort(sorts.begin(), sorts.end());

    for(auto p : unsorts) {
        // check if fully inside any sort
        for(int i=0; i<sorts.size(); i++) {
            if(p.first>=sorts[i].first && p.second<=sorts[i].second) {
                cout<<"NO"<<endl;
                return 0;
            }
        }
    }

    /* checks if idx and idx+1 must be sorted */
    function<bool(int)> isSort=[&](int idx) {
        for(int i=0; i<sorts.size(); i++) {
            if(sorts[i].first<=idx && sorts[i].second>=idx+1)
                return true;
            else if(sorts[i].first>=idx+1)
                break;
        }
        return false;
    };

    vector<bool> needSort(n);  // notSorted[i]== i and i+1 need not to be sorted
    for(int i=1; i<n; i++)
        needSort[i]=isSort(i);

    for(auto p : unsorts) {
        // check if fully inside any sort
        for(int i=0; i<sorts.size(); i++) {
            if(p.first>=sorts[i].first && p.second<=sorts[i].second) {
                cout<<"NO"<<endl;
                return 0;
            }
        }

        bool ok=false;
        for(int i=p.first; i<p.second; i++) {
            if(!needSort[i]) {
                ok=true;
                break;
            }
        }
        if(!ok) {
            cout<<"NO"<<endl;
            return 0;
        }
    }


    
    vector<int> ans;
    int val=100000;
    ans.push_back(val);

    for(int i=1; i<n; i++) {
        if(needSort[i])
            val++;
        else
            val--;
        ans.push_back(val);
    }

    cout<<"YES\n";
    fori(n)
        cout<<ans[i]<<" ";

}

