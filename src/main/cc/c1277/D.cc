/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cinas(s,n);

    vvi cnt(2, vi(2));  // cnt[begin=0;end=1][0] cnt of string with "0" at begin/end
    
    set<string> rev;
    vi dif;    // start/end differs

    for(int i=0; i<n; i++) {
        string s2=s[i];
        reverse(s2.begin(), s2.end());
        rev.insert(s2);

        if(s[i][0]!=s[i].back())
            dif.push_back(i);

        cnt[s[i][0]-'0'][s[i].back()-'0']++;
    }

/*
cout<<"cnt[0][0]="<<cnt[0][0]<<endl;
cout<<"cnt[0][1]="<<cnt[0][1]<<endl;
cout<<"cnt[1][0]="<<cnt[1][0]<<endl;
cout<<"cnt[1][1]="<<cnt[1][1]<<endl;
*/

/* ends and begins must not differ by more than one */

/* we can interchange cnt[1][0] and cnt[0][1]  by one reversing */
/* 
 * 10 00 01 11
 * 00 01 11 10
 * ...
 * 10 00 01 1x10 if 11 is empty
 */

    int minRev=abs(cnt[1][0]-cnt[0][1]);    // parity switch
    minRev/=2;

    if(cnt[0][0] && cnt[1][1] && (cnt[0][1]+cnt[1][0])==0) {
        cout<<-1<<endl;
        return;
    }

//cout<<"minRev="<<minRev<<endl;
    if(minRev>dif.size()) {
        cout<<-1<<endl;
        return;
    }

    vi ans;
    for(int i : dif) {
        if(minRev==0)
            break;

        if(!rev.count(s[i])) {
            ans.push_back(i);
            minRev--;
        }
    }

    if(minRev>0)
        cout<<-1<<endl;
    else {
        cout<<ans.size()<<endl;
        for(int i : ans) 
            cout<<i+1<<" ";
        cout<<endl;
        //for(string str : ans)
        //    cout<<str<<endl;
    }
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

