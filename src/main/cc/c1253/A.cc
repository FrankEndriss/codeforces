/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9;

void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);



    int i=0;
    while(i<n && b[i]==a[i])
        i++;

    if(i<n) {
        int d=b[i]-a[i];
        while(d>0 && i<n && b[i]-a[i]==d)
            i++;
    }

    while(i<n && b[i]==a[i])
        i++;

    if(i==n)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}


int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

