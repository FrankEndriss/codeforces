/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct Dsu {
    int cnt;
    vector<int> p;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
        cnt=size;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            cnt--;
        }
    }
};

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    

/* this is about components.
 * All nodes of an interval must be in one component.
 * If not then we must connect those two components.
 */

    cini(n);
    cini(m);
    vvi tree(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;        
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    Dsu dsu(n);
    vi comp(n, -1);
    int c;

    function<void(int,int)> dfs=[&](int node, int parent) {
        comp[node]=c;
        dsu.union_sets(c,node);
        for(int chl : tree[node])
            if(chl!=parent && comp[chl]==-1)
                dfs(chl, node);
    };

    for(int i=0; i<n; i++)  {
        if(comp[i]==-1) {
            c=i;
            dfs(i, -1);
        }
    }

/* find number of overlaps */

    int ma=comp[0];
    int ans=dsu.cnt;
    int maU=0;
    for(int i=0; i<n; i++) {
        if(comp[i]<ma) {
            for(int j=max(maU, comp[i]); j<ma; j++) {
                dsu.union_sets(ma,j);
                maU=j;
            }
        }

        if(comp[i]>ma) {
            ma=comp[i];
            maU=0;
        }
    }
    ans-=dsu.cnt;
    cout<<ans<<endl;
}

