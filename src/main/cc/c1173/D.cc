/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

const int MOD=998244353;

ll fak(ll i) {
    ll ret=1;
    while(i>1) {
        ret*=i;
        ret%=MOD;
        i--;
    }
    return ret;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> tree[n+1];
    fori(n-1) {
        int u, v;
        cin>>u>>v;
        tree[u].push_back(v);   
        tree[v].push_back(u);
    }

/* All permutations are n!
 * If there is a perm with crossing two edges,
 * that perm can be mirrored (how often?) and each mirror version
 * can be rotated n times.
 * So these perms have to be subtracted.
 * How can we cross edges?
 * All edges with a distance of at least one, (ie do not connect
 * the same vertex), can be crossed.
 *
 * One can interchange the positions of all children of one node!
 * And that for every node!
 */
    ll ans=1;
    for(int i=1; i<=n; i++) {
        if(tree[i].size()>0) {
            ans*=fak(tree[i].size());
            ans%=MOD;
        }
    }
    ans*=n;
    ans%=MOD;

    cout<<ans<<endl;

}

