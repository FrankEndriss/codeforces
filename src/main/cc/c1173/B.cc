/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

/* check if manhattan distance is bigger than value diff */
bool ok(int i, int j, pair<int, int> pi, pair<int, int> pj) {
    return abs(pi.first-pj.first) + abs(pi.second-pj.second) >= abs(i-j);
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;

/* Note that if we can place the maximum diff pair then we can place all pairs,
 * since we could place all pieces into one row, one after the other.
 * So, min(m) == n/2;
 */
    
    int m=1;
    while(m+m-1<n)
        m++;

    cout<<m<<endl;
    for(int ans=1; ans<=m; ans++)
        cout<<"1 "<<ans<<endl;
    for(int ans=2; ans<=n-m+1; ans++)
        cout<<ans<<" "<<m<<endl;

}

