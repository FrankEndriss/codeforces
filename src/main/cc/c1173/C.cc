/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    set<int> a, a2;
    vector<int> b(n), posB(n+1);
    queue<int> b2;
    fori(n) {
        int x;
        cin>>x;
        if(x!=0) {
            a.insert(x);
            a2.insert(x);
        }
    }
    
    fori(n) {
        cin>>b[i];
        posB[b[i]]=(int)i+1; // positions 1 based
        b2.push(b[i]);
    }

/* In worst case, we need to take all cards, then play all cards in order.
 * How much can we shorten this?
 * 
 * Case 1: There is an sorted postfix in b. So we can simply play all cards
 * in order. Then number of moves is n-(len_sorted_postfix).
 * All numbers must be available. Just check if this is possible.
 */
    int postlen=1;
    int pos1=posB[1]; 
    int card=2;
    for(int i=pos1+1; i<=n; i++) {
        if(posB[card]==i) {
            card++;
            postlen++;
        } else {
            postlen=0;
            break;
        }
    }
    int ans=-1;
    int minpos=1;
    if(postlen>0) {
        ans=0;
        for(int i=postlen+1; i<=n; i++) {
            if(a2.count(i)>0 || posB[i]<minpos) {
                ans++;
                minpos++;
            } else {
                ans=-1;
                break;
            }
        }
    }

    if(ans>=0) {
        cout<<ans<<endl;
        return 0;
    }
    
    // Case 2

/* We need to take (or have on hand) card 1, then card 2, then card 3...
 * So, min moves is max of: pos(card 1)+n, pos(card2)-1+n, pos(card3)-2+n, ...
 */
    ans=n;
    for(int i=1; i<=n; i++) {
        ans=max(ans, posB[i]+n-(i-1));
    }
    
    cout<<ans<<endl;

}

