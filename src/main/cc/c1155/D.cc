/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

typedef struct subarret {
    ll ret;
    int lidx;
    int ridx;
} subarret;


int main() {
    int n, x;
    cin>>n>>x;
    vector<ll> a(n);
    for(auto &i : a)
        cin>>i;

    vector<vector<ll>> dp(n+99, vector<ll>(3));

    ll ans=-1;
    for(int i=0; i<n; i++)  {
        dp[i+1][0]=max(0LL, dp[i][0]+a[i]);       /* running sum since last 0 */
        dp[i+1][1]=max(0LL, max(dp[i+1][0], dp[i][1]+a[i]*x));
        dp[i+1][2]=max(0LL, max(dp[i+1][1], dp[i][2] + a[i]));
        ans=max(ans, *max_element(dp[i+1].begin(), dp[i+1].begin()+3));
    }

//cout<<"log dp[n][2]="<<dp[n][2]<<endl;
    cout<<ans<<endl;
    return 0;
}

