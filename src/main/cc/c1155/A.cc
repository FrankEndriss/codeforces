
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int main() {
int n;
string s;
    cin>>n>>s;
    int m=0;
    for(uint i=0; i<s.size(); i++) {
        int c=s[i];
        if(c<m) {
            cout<<"YES"<<endl;
            cout<<i<<" "<<i+1<<endl;
            return 0;
        }
        m=c;
    }
    cout<<"NO"<<endl;
}

