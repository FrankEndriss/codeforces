/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

typedef struct node {
    struct node *p;
    int o;  // idx of open brace
    int c;  // idx of close brace
    vector<struct node*> ch;
} node;

int main() {
int n, q;
    cin>>n>>q;
    string braces;
    cin>>braces;

    vector<node*> nodeidx(n);

    node root;
    root.o=0;
    root.p=NULL;

    node *curr=&root;
    for(uint i=0; i<braces.size(); i++) {
        if(braces[i]=='(') {
            node *nn=new node();
            nodeidx[i]=nn;
            nn->p=curr;
            nn->o=i;
            curr->ch.push_back(nn);
            curr=nn;
        } else {
            curr->c=i;
            nodeidx[i]=curr;
            curr=curr->p;
        }
    }

    int longestPath=42; // TODO

    fori(q) {
        int i1, i2;
        cin>>i1>>i2;
        if(braces[i1]==braces[i2]) {
                // no change
        } else if(braces[i2]=='(') 
            auto tmp=i1;
            i1=i2;
            i2=tmp;
        } 

        // open2close(i1); '(' node wird vom parent entfernt und an den parent-parent angehangen,
        node *ni1=nodeidx[i1];
        node *ni1p=ni1->p;
        ni1p->ch.erase(find(ni1));
        ni1p->p->ch.push_back(ni1);


        close2open(i2); // ')' vom parent-parent wird das naechste child an i2 angehangen.

        auto tmp=braces[i1];
        braces[i2]=braces[i1];
        braces[i1]=tmp;
        cout<<longestPath<<endl;
    }

    exit(0);
}

