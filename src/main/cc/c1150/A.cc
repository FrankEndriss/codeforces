
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

int main() {
    int n, m, r;
    cin>>n>>m>>r;
    vector<int> s(n);
    fori(n)
    cin>>s[i];

    sort(s.begin(), s.end());   // by price

    vector<int> b(m);
    fori(m)
    cin>>b[i];
    sort(b.begin(), b.end(), [&](int i1, int i2) {
        return i2<i1;
    });


    if(s[0]<b[0]) {
        int cnt=0;
        while(r>=s[0]) {
            cnt++;
            r-=s[0];
        }
        while(cnt--)
            r+=b[0];
    }

    cout<<r<<endl;
}

