/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

const int MAXN=100000;
const int primeAproxFactor=30;

vector<int> pr(MAXN);
void init() {
    vector<bool> cmp(MAXN*primeAproxFactor, false);

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!cmp[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                cmp[j] = true;
                j += i;
            }
        }
        i++;
    }
}

int main() {
    init();
    int n;
    cin>>n;
    int cnt[3]={0};
    fori(n) {
        int tmp;
        cin>>tmp;
        cnt[tmp]++;
    }

    int nextpr=0;
    int sum=0;
    fori(n) {
//cout<<"log, p="<<pr[nextpr]<<" sum="<<sum<<" cnt[1]="<<cnt[1]<<" cnt[2]="<<cnt[2]<<endl;
        int oval=0;
        if(cnt[1]==0)
            oval=2;
        else if(cnt[2]==0)
            oval=1;
        else {
            if(pr[nextpr]-sum > 1)
                oval=2;
            else
                oval=1;
        }
        sum+=oval;
        cnt[oval]--;
        cout<<oval<<" ";
        while(sum>=pr[nextpr])
            nextpr++;
    }
    cout<<endl;
    
}

