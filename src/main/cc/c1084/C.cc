/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<n; i++)
#define fauto(c, vec) for(auto c : vec)

int main() {
string s;
    cin>>s;
    /** Split to groups of 'a' separated by one or more 'b'. Like Stars and bars. */
    ll mode=0;
    ll a=0;
    vector<ll> stars;
    for(auto c : s) {
        if(mode==0 && c=='a') {
            a++;
            mode=1;
        } else if(mode==1) {
            if(c=='a')
                a++;
            else if(c=='b') {
                stars.push_back(a);
                a=0;
                mode=0;
            }
        }
    }
    if(a>0)
        stars.push_back(a);

    const int mod=1000000007;
    ll ans=1;
    // on every bucket of a we can choose any one of the a or none, which is sizeof(bucket)+1 possibilities.
    // But the complete empty seq is not allowed, so -1 in the end.
    for(auto m : stars)
        ans=(ans*(m+1))%mod;

    ans--;
    cout<<ans<<endl;
}

