/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vvi h(2, vi(n));
    vvll dp(2, vll(n));
    fori(n)
        cin>>h[0][i];
    fori(n)
        cin>>h[1][i];

/* dp[k][i] = max H on students rigth of i, if start choosing on row k */
    dp[0][n-1]=h[0][n-1];
    dp[1][n-1]=h[1][n-1];
    for(int i=n-2; i>=0; i--) {
        dp[0][i]=max(dp[1][i+1]+h[0][i], dp[0][i+1]);
        dp[1][i]=max(dp[0][i+1]+h[1][i], dp[1][i+1]);
    }

    cout<<max(dp[0][0], dp[1][0]);
}

