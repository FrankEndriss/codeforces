/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<vll> vvll;
typedef vector<vvll> vvvll;

const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vi a(n);
    fori(n) {
        cin>>a[i];
    }

    const int left=0;
    const int right=1;
/* dp[d][n][first]== if a[n] shuffeld with a d+1 digit number, and a[n] is first, then this
 * will be the result.
 */
    vvvi dp(10, vvi(n, vi(2)));
    vi f(10);        
    fori(n) {
        for(int d=0; d<10; d++) { 
            int num=a[i];
            int multLeft=10;
            int multRight=1;
            for(int k=0; k<=d && num>0; k++) {
                dp[d][i][left]=pl(dp[d][i][left], mul(num%10, multLeft));
                dp[d][i][right]=pl(dp[d][i][right], mul(num%10, multRight));
                multLeft=mul(multLeft, 100);
                multRight=mul(multRight, 100);
                num/=10;
            }
            while(num>0) {
                dp[d][i][left]=pl(dp[d][i][left], mul(num%10, multLeft));
                dp[d][i][right]=pl(dp[d][i][right], mul(num%10, multRight));
                multLeft=mul(multLeft, 10);
                multRight=mul(multRight, 10);
                num/=10;
            }
        }

        int k=0;
        ll num=10;
        while(true) {
            if(num>a[i]) {
                f[k]++;
                break;
            }
            num*=10;
            k++;
        }
    }

    int ans=0;
    fori(n) {
        for(int d=0; d<10; d++) {
            ans=pl(ans, mul(dp[d][i][left], f[d]));
            ans=pl(ans, mul(dp[d][i][right], f[d]));
        }
    }

    cout<<ans<<endl;

}

