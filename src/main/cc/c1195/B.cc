/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, k;
    cin>>k>>n;

    ll l=1, r=k;
    while(l<r-1) {
        ll puts=(l+r)/2;
        ll candies=puts*(puts+1)/2-(k-puts);
    
        if(candies>n)
            r=puts;
        else
            l=puts;
    }
//--cout<<"l="<<l<<" k="<<k<<" n="<<n<<endl;
    ll candies=l*(l+1)/2-(k-l);
    assert(candies==n);
    cout<<k-l<<endl;

}

