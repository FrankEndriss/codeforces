/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

pair<int, int> p(int i1, int i2) {
    return make_pair(min(i1, i2), max(i1, i2));
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<pair<int, int>> ab(m);
    vector<int> abid(m);
    vector<int> f(n+1);
    vector<int> fid(n+1);

    fori(m) {
        int a, b;
        cin>>a>>b;
        ab[i]={a, b};
        f[a]++;
        f[b]++;
        abid[i]=i;
    }
    fori(n+1)
        fid[i]=(int)i;

    if(m<=2) {
        cout<<"YES"<<endl;
        return 0;
    }

    sort(fid.begin()+1, fid.end(), [&](int i1, int i2) {
        return f[i2]<f[i1];
    });

    sort(abid.begin(), abid.end(), [&](int i1, int i2) {
        return f[ab[i1].first]+f[ab[i1].second]<f[ab[i2].first]+f[ab[i2].second];
    });

    bool ok=false;
    for(int x=1; x<=n; x++)  {
        int fini=true;
        for(int y=x+1; y<=n && f[fid[x]]+f[fid[y]]>=m; y++) {
            fini=false;

            int ans=0;
            fori(m) {
                if(fid[x]==ab[abid[i]].first 
                    || fid[y]==ab[abid[i]].first
                    || fid[x]==ab[abid[i]].second
                    || fid[y]==ab[abid[i]].second)
                        ans++;
                else
                    break;
            }
            if(ans==m) {
                ok=true;
                break;
            }
        }
        if(ok || fini)
            break;
    }

    if(ok)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

