/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
string s;
    cin>>s;

    ll ans=0;
    for(ll x=0; x<s.size(); x++) {
        /* find first matching index */
        ll lk=0;
        for(ll k=1; x+2*k<s.size(); k++) {
            if(s[x]==s[x+k] && s[x]==s[x+k*2]) {
                lk=k;
                break;
            }
        }
        if(lk>0) {
//cout<<"x="<<x<<" lk="<<lk<<endl;
            ans+=(s.size()-(x+2*lk));
        }
    }

    cout<<ans<<endl;

/* TODO, above code is not correct.
 * We need to consider pairs of (x, x+k*2)
 * If only one such pair exists, ans=x*n-(x+k*2), ie number of ints before
 * the range mult by number of ints after the range.
 * Usually more than one range exists, so we have to combine them...
 */

}

