/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
#define	Unique(v)	v.erase(unique(all(v)),v.end())

using namespace std;

typedef unsigned int uint;
typedef long long ll;

#define N 100001
ll a[N];
ll b[N];
int pos[N];
int n;

/** dissatisfaction of student s at pos p */
ll dis(int s, int p) {
	ll ret= (a[s] * p) + (b[s]*(n-p-1));
	return ret;
}

int main() {
	cin>>n;
	for(int i=0; i<n; i++) {
		cin>>a[i]>>b[i];
		pos[i]=i;
	}

	sort(pos, pos+n, [](const int s1, const int s2){
		return a[s1]-b[s1]>a[s2]-b[s2];
	});
	
	ll ans=0;
	for(int i=0; i<n; i++)
		ans+=dis(pos[i], i);
	cout<<ans<<endl;
}

