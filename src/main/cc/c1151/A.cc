
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
#define	Unique(v)	v.erase(unique(all(v)),v.end())

using namespace std;

typedef unsigned int uint;
typedef long long ll;

int main() {
int n;
	cin>>n;
string s;
	cin>>s;
string genome="ACTG";
vector<int> ops;


	for(uint i=0; i<s.length()-3; i++) {
		string su=s.substr(i, 4);
		int o=0;
		for(int j=0; j<4; j++) {
			int dist=min(abs(su[j]-genome[j]), abs(abs(su[j]-genome[j])-26));
			o+=dist;
		}
		ops.push_back(o);
	}

	int ans=*min_element(ops.begin(), ops.end());
	cout<<ans<<endl;
	return 0;
}

