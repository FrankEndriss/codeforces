/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(x);
    vector<pii> blows(n);
    int maSingle=0;
    int maDiff=-1e9;
    for(int i=0; i<n; i++) {
        cin>>blows[i].first>>blows[i].second;
        maSingle=max(maSingle, blows[i].first);
        maDiff=max(maDiff, blows[i].first-blows[i].second);
    }

    if(x<=maSingle) {
        cout<<1<<endl;
        return;
    }

    if(maDiff<=0) {
        cout<<-1<<endl;
        return;
    }

    x-=maSingle;
    int ans=1+(x+maDiff-1)/maDiff;

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

