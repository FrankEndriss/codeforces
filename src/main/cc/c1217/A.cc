/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(s);
    cini(i);
    cini(free);

    if(free==0) {
        if(s>i)
            cout<<1<<endl;
        else
            cout<<0<<endl;
        return;
    }

    if(s+free<=i) {
        cout<<0<<endl;
        return;
    }

    int minS=max(s, (s+i+free)/2);
    while(minS<=(s+i+free)-minS)
        minS++;

    int maxS=s+free;
    int ans=max(0, maxS-minS+1);

    cout<<ans<<endl;
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

