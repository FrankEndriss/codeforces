
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<vector<int>> tree(n+1);
    fori(n) {
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    vector<int> level(n+1);
    vector<vector<int>> nodesOfLevel(n+1);
    function<void(int, int, int)> dfs=[&](int v, int p, int l) {
        level[v]=l;
        nodesOfLevel[l].push_back(v);
        for(int c : tree[v])
            if(c!=p)
                dfs(c, v, l+1);
    };
    dfs(1, -1, 0);

    int remc=0;
    vector<bool> rem(n+1, false);
    function<void(int, int)> dfsRem=[&](int v, int p) {
        if(rem[v])
            return;

        remc++;
        rem[v]=true;
        for(int c : tree[v])
            if(c!=p)
                dfsRem(c, v);
    };

    int ans;
    cout<<"d 1"<<endl;
    int xlevel;
    cin>>xlevel;
    
    int root=1;
    int rootlevel=0;
    while(remc<n-1) {
        if(level-rootlevel==1) {
            cout<<"s "<<root<<endl;
            cin>>ans;
            cout<<"! "<<ans<<endl;
        }
        // ... ???
    }
    

}

