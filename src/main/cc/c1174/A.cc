/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> a(2*n);
    fori(2*n) 
        cin>>a[i];
    sort(a.begin(), a.end());

    bool ans=(a[0]==a[2*n-1]);
    
    if(ans)
        cout<<"-1"<<endl;
    else {
        fori(2*n)
            cout<<a[i]<<" ";
        cout<<endl;
    }

    return 0;
}

