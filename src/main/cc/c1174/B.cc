/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;

    vector<int> a(n);
    int ods=0;
    int evs=0;
    fori(n) {
        cin>>a[i];
        if(a[i]%2==0) {
            evs++;
        } else {
            ods++;
        }
    }

    if(evs>0 && ods>0)
        sort(a.begin(), a.end());

    fori(n) {
            cout<<a[i]<<" ";
    }
    cout<<endl;
        

}

