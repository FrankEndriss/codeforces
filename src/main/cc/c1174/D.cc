/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    ll n, x;
    cin>>n>>x;

    /* we need to create the list as the prefix
     * xor list of the final list.
     * So, the prefix list simply uses any value once,
     * and xors it with x.
     *
     * note that x can be bigger than 2<<(n-1), in the case
     * we can ignore x.
     */

    vector<int> preans;
    for(int i=1; i<2<<(n-1); i++) {
        preans.push_back(i^x);
    }
    fori(
        lbit<<=1;

        vector<int> ans;

    for(int i=1; i<2<<(n-1); i++) {
    if((i&lbit)==0)
            ans.push_back(i);
    }

    cout<<ans.size()<<endl;

        for(int i : ans)
        cout<<i<<" ";
        cout<<endl;

}

