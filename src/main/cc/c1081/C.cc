/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

const int mod=998244353;

int nok[2001][2001];

int main() {
    int n, m, k;
    cin>>n>>m>>k;

    int ans=m;
    /* Since there are k color changes, and the first brick can have any color, ans= m*((m-1)^^k) */
    for(int i=0; i<k; i++) {
        ll tmp=1LL * ans * (m-1);
        ans=(int)(tmp%mod);
    }

    /* Since n can be greater than k+1, the number of bricks (next to next) of any of these
     * colors can vary.
     * Number of ways is: How many distributions of n-k-1 items on k+1 buckets are possible?
     * see https://en.wikipedia.org/wiki/Stars_and_bars_(combinatorics)
     */

    if(k>0) {
        for(int i=1; i<2000; i++) {
            nok[i][0]=1;
            nok[i][i]=1;
            for(int j=1; j<2000; j++) {
                if(i!=j)
                    nok[i][j]=(nok[max(1, i-1)][j] + nok[max(1, i-1)][max(0, j-1)])%mod;
            }
        }

        ll tmp=1LL * ans * nok[n-1][k];
        ans=(int)(tmp%mod);
    }
    cout<<ans<<endl;
    return 0;
}

