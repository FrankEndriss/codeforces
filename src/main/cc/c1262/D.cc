/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    vi a(n);
    map<int,vi> f;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        f[a[i]].push_back(i);
    }

    cini(m);
    for(int i=0; i<m; i++) {
        cini(k);
        cini(pos);
        // ans[i].first=number, ans[i].second=times
        vector<pii> ans;  
        for(auto it=f.rbegin(); it!=f.rend() && k; it++) {
            int cnt=min(k, (int)it->second.size());
            ans.push_back({it->first, cnt});
            k-=cnt;
        }

/* Now we add the numbers allways using the next
 * position in a.
 *
 * Binary search?
 * let cidx be the checked idx of a.
 * We find the biggest idx<=cidx in ans, and the number at that idx.
 * We check if there are exactly pos numbers <= cidx in our ans.
 * if yes, found.
 *
 * How to check.
 * For all entries in ans find the number of nums left of cidx.
 * ...this is to slow, ans can be size up to n :/
 */
        int lo=0;
        int hi=n;

        while(lo+1<hi) {
            int cidx=(lo+hi)/2;
            int cnt=0;
            for(size_t j=0; j<ans.size(); j++) {
                vi v=f[ans[j].first];
                auto it=lower_bound(v.begin(), v.end(), cidx);
                int d=distance(v.begin(), it);
                d=min(d, ans[j].second);
                cnt+=d;
            }

            if(cnt==pos) {
...
            }
            
        }


/*
        for(int i=1; ; i++) {
            if(i==pos) {
                auto lans=q.top();
                cout<<lans.second.first<<endl;
                break;
            } else {
                auto val=q.top();
                q.pop();
                if(val.second.second>1) {
                    val.second.second--;
                    val.first.second++;
                    val.first.first=-f[val.second.first][val.first.second];
                    q.push(val);
                }
            }
        }
*/
    }
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t=1;
    while(t--)
        solve();
}

