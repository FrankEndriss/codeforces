/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    cini(k);

    vector<pii> obs(k);
    vector<pii> obs2(k);
    
    for(int i=0; i<k; i++) {
        cin>>obs[i].first>>obs[i].second;
        obs[i].first--;
        obs[i].second--;
        obs2[i]=obs[i];
    }

    sort(obs.begin(), obs.end());

    function<bool(pii,pii)> scmp=[&](pii p1, pii p2) {
        if(p1.second==p2.second)
            return p1.first<p2.first;
        return p1.second<p2.second;
    };
    sort(obs2.begin(), obs2.end(), scmp);

/* Observations:
 * 1. n,m 1e5, ie to much for any dfs.
 * 2. Free fields must be one component, else false;
 * 3. Doll cannot reverse direction, so at most one onway may exist, except of start.
 * 4. In oneways, left turn must not exist.
 * 5. All other constructs are ok?
 * 6. How to detect oneways? -> Fields with 3 borders are ends of oneways.
 * 7. How to check if one component? -> if obs form way from one border to another border
 * this cuts out a part of the grid. But only if there are free fields in that part.
 *
 * Solution:
 * 8. Since every free field must be visited _exactly_ once, the path must be spiral.
 *    How to check if spiral possible?
 *    There must not be any obstacle in first row (now free field after last
 *    visitable field of first row, so max col in first row must be max col of
 *    any row.).
 *    Then rotate everything by 90 degrees, and same.
 *    And so on.
 * 9. Note that if we hit any obstacle, fields are "covered" by that obstacle.
 *    Find the hitted one, and check for covered fields.
 *
 * NOTE:
 * Forgett about "covered" fields. We simply run in spirals as far as possible, counting fields.
 * If we got all field, ok, else not ok.
 */
    int dir=0;  // Right
    int rmin=0;
    int rmax=n;
    int cmin=-1;
    int cmax=m;

    ll vis=1;
    pii pos={ 0, 0 };
    while(true) {   // when are we finished?
        if(vis+k==n*m)
            break;
//cout<<"pos= ("<<pos.first<<","<<pos.second<<")"<<endl;

        pii oldPos=pos;
        if(dir==0) {    // Right
            auto it=lower_bound(obs.begin(), obs.end(), pos);
            if(it==obs.end() || it->first!=pos.first)  // ok
                pos={ pos.first, cmax-1 };
            else {
                pos={ pos.first, it->second-1 };
            }
            dir=1;
            cmax=pos.second;
        } else if(dir==1) { // Down
            auto it=lower_bound(obs2.begin(), obs2.end(), pos, scmp);
            if(it==obs2.end() || it->second!=pos.second)  // ok
                pos={ rmax-1, pos.second };
            else {
                pos={ it->first-1, pos.second };
            }
            dir=2;
            rmax=pos.first;
        } else if(dir==2) { // Left
            auto it=upper_bound(obs.begin(), obs.end(), make_pair(pos.first, cmin));
            if(it==obs.end() || it->first!=pos.first)  // ok
                pos={ pos.first, cmin+1 };
            else {
                pos={ pos.first, it->second+1 };
            }
            dir=3;
            cmin=pos.second;
        } else if(dir==3) { // Up
            auto it=upper_bound(obs2.begin(), obs2.end(), make_pair(rmin, pos.second), scmp);
            if(it==obs2.end() || it->second!=pos.second)  // ok
                pos={ rmin+1, pos.second };
            else { 
                pos={ it->first+1, pos.second };
            }
            dir=0;
            rmin=pos.first;
        }
        if(oldPos==pos)
            break;

        vis+=(abs(oldPos.first-pos.first)+abs(oldPos.second-pos.second));
        if(vis+k==n*m)
            break;
    }


    if((vis+k)==n*m)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

