/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vll a(n);
    fori(n)
        cin>>a[i];
    sort(a.begin(), a.end());

    if(n<4) {
        ll sum=a[0]+a[1]+a[2];
        ll ma=max(a[0], max(a[1], a[2]));
        if(ma>=sum-ma) {
            cout<<"NO"<<endl;
        } else {
            cout<<"YES"<<endl;
            cout<<a[0]<<" "<<a[1]<<" "<<a[2]<<endl;
        }
        return 0;
    }

    if(a[n-2]+a[n-3]<=a[n-1]) {
        cout<<"NO"<<endl;
        return 0;
    }
    cout<<"YES"<<endl;
    for(int i=0; i<n-3; i++)
        cout<<a[i]<<" ";
    cout<<a[n-2]<<" "<<a[n-1]<<" "<<a[n-3]<<endl;
}

