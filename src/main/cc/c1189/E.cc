/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

#define DEBUG

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n, p, k;
    cin>>n>>p>>k;
    vll a(n);
    vll dp(n);
    fori(n) {
        cin>>a[i];
        dp[i]=(a[i]*a[i]+a[i])%p;
    }
    sort(dp.begin(), dp.end());

    ll ans=0;
    for(int i=0; i<n-1; i++) {
        ll match=k-dp[i];
        if(match<0)
            match+=p;
        auto it1=lower_bound(dp.begin()+i+1, dp.end(), match);
        auto it2=upper_bound(dp.begin()+i+1, dp.end(), match);
        ans+=distance(it1, it2);
    }
    cout<<ans<<endl;
}

