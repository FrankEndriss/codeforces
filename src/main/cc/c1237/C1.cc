/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    vector<pair<pair<int,int>, pair<int,int>>> p(n);
    map<pair<int,int>, vi> simiXY;
    map<pair<int,int>, vi> simiXZ;
    map<pair<int,int>, vi> simiYZ;

    for(int i=0; i<n; i++) {
        cin>>p[i].first.first>>p[i].first.second>>p[i].second.first;
        p[i].second.second=i;
    }
    sort(p.begin(), p.end());

    for(int i=0; i<n; i++) {
        simiXY[p[i].first].push_back(i);
        simiXZ[make_pair(p[i].first.first, p[i].second.first)].push_back(i);
        simiYZ[make_pair(p[i].first.second, p[i].second.first)].push_back(i);
    }

    vb vis(n, false); // vis[i] <-> p[i]
    int cnt=0;
    for(auto &m : {
                simiXY, simiYZ, simiXZ
            }) {
        cnt++;
//cout<<"map: "<<cnt<<endl;
        for(auto ent : m) {
            int first=-1;
            for(size_t i=0; i<ent.second.size(); i++) {
                if(!vis[ent.second[i]]) {
                    if(first<0) {
                        first=ent.second[i];
                    } else {
                        cout<<p[first].second.second+1<<" "<<p[ent.second[i]].second.second+1<<endl;
                        vis[first]=true;
                        vis[ent.second[i]]=true;
                        first=-1;
                    }
                }
            }
        }
    }

//cout<<"lin"<<endl;
    int first=-1;
    for(int i=0; i<n; i++) {
        if(!vis[i]) {
//cout<<"!vis[i], i="<<i+1<<endl;
            vis[i]=true;
            if(first<0)
                first=p[i].second.second;
            else {
                cout<<first+1<<" "<<p[i].second.second+1<<endl;
                first=-1;
            }
        }
    }
}

