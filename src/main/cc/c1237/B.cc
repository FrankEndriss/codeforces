/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    vi a(n);
    vi apos(n);
    for(int i=0; i<n; i++) {
        cin>>a[i];
        a[i]--;
        apos[a[i]]=i;
    }
    vi b(n);
    for(int i=0; i<n; i++) {
        cin>>b[i];
        b[i]--;
    }

/* For car b[i] we need to find any car with pos b[i-x] (ie before)
 * has a position in a _after_ b[i]. ie
 * For all b[i] check if: apos[b[i]]<apos[b[i-x]]
 *
 * Since we cant check for all, we check the min apos[b[x]] only, which is enough.
 */
    int ans=0;
    int minpos=n+1;
    for(int i=n-1; i>=0; i--) {  
        if(apos[b[i]]>minpos)
            ans++;
        minpos=min(minpos, apos[b[i]]);
    }
    cout<<ans<<endl;
}

