/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    vector<pii> c(n);
    vi a(n);
    for(int i=0; i<n; i++) {
        cin>>c[i].first;
        c[i].second=i;
        a[i]=c[i].first;
    }
    sort(a.begin(), a.end());
    sort(c.begin(), c.end());

    /* All teams have 3,4 or 5 members. */

    const int INF=1e9;
    vvi dp(3, vi(n, INF));   // dp[i]=opt sol up to idx i
    vi dp2(n);  // size of last team in optimal solution, 3 or 4 or 5

    dp[0][2]=a[2]-a[0];
    dp[1][3]=a[3]-a[0];
    dp[2][4]=a[4]-a[0];
    dp2[2]=3;
    dp2[3]=4;
    dp2[4]=5;
    for(int i=5; i<n; i++) {
        for(int j=0; j<3; j++) {
            dp[0][i]=min(dp[0][i], dp[j][i-3]+a[i]-a[i-2]);
            for(int j=0; j<3; j++)
                dp[1][i]=min(dp[1][i], dp[j][i-4]+a[i]-a[i-3]);
            for(int j=0; j<3; j++)
                dp[2][i]=min(dp[2][i], dp[j][i-5]+a[i]-a[i-4]);
        }
    }

    stack<int> st;
    int idx=n-1;
    while(idx>0) {
        int val=INF;
        int mi;
        for(int i=0; i<3; i++)
            if(dp[i][idx]<val) {
                mi=i;
                val=dp[i][idx];
            }
        st.push(mi+3);  // teamsize
        idx-=(mi+3);
    }

    int ans=INF;
    for(int i=0; i<3; i++)
        ans=min(ans, dp[i][n-1]);
    cout<<ans<<" "<<st.size()<<endl;
    idx=0;
    int cnt=1;
    while(st.size()) {
        int i=st.top();
        st.pop();
        for(int j=0; j<i; j++, idx++)
            a[c[idx].second]=cnt;
        cnt++;
    }

    for(int i=0; i<n; i++)
        cout<<a[i]<<" ";
    cout<<endl;

}

