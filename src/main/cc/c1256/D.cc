/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cinll(n);
    cinll(k);
    cins(s);
    vi pos;
    for(size_t i=0; i<s.size(); i++)
        if(s[i]=='0')
            pos.push_back(i);

    size_t i0=0;
    for(int i=0; i<s.size(); i++) {
        if(s[i]=='1') { 
            /* find next available '0' */
            for(size_t j=i0; j<pos.size(); j++) {
                if(pos[j]>i) {
                    if(pos[j]-i<=k) {
                        swap(s[i], s[pos[j]]);
                        k-=pos[j]-i;
                        i0=j+1;
                    }
                    break;
                } else
                    i0=j;
            }
        }
    }
    cout<<s<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

