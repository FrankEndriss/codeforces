/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    vi a(n);
    vi pos(n);
    for(int i=0; i<n; i++) {
        cini(x);
        a[i]=x-1;
        pos[x-1]=i;
    }

    int perms=n-1;
    function<void(int,int)> mswap=[&](int l, int r) {
        for(int i=r; i>l; i--) {
            swap(a[i], a[i-1]);
            pos[a[i-1]]--;
            pos[a[i]]++;
            perms--;
        }
    };

    int maswap=0;
    for(int i=0; i<n; i++) {
        /* find smallest number we can move to position maswap */
        int mi=1e9;
        int midx=-1;
#ifdef DEBUG
cout<<"try i="<<i<<endl;
#endif
        maswap=max(maswap, i);
        for(int j=maswap+1; j<n; j++) { // positions right of maswap
            if(j-maswap<=perms && a[j]<mi && a[j]<a[maswap]) {
#ifdef DEBUG
cout<<"possible swap at j="<<j<<endl;
#endif
                mi=a[j];
                midx=j;
            }
        }

        if(midx>maswap)  {
#ifdef DEBUG
cout<<"do swap, maswap="<<maswap<<" midx="<<midx<<endl;
#endif
            mswap(maswap, midx);
            maswap=midx;
        }
    }

    for(int i=0; i<n; i++)
        cout<<a[i]+1<<" ";
    cout<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

