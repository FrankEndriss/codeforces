/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;


/* number of inversion & 1 */
bool parity(const string& str) {
    bool res = true;
    for (int i = 0; i < str.size(); ++i) {
        for (int j = 0; j < i; ++j) {
            if (str[i] < str[j]) res ^= 1;
        }
    }
    return res;
}

void solve() {
    cini(n);
    cins(s);
    cins(t);
    vi fs(256);
    vi ft(256);

    for(size_t i=0; i<n; i++)  {
        fs[s[i]]++;
        ft[t[i]]++;
    }
    bool has2=false;
    for(int i=0; i<256; i++)  {
        if(fs[i]!=ft[i]) {
            cout<<"NO"<<endl;
            return;
        }
        if(fs[i]>1)
            has2=true;
    }

    if(has2) {
        cout<<"YES"<<endl;
        return;
    }
    vi pos(256);
    for(int i=0; i<n; i++)
        pos[s[i]]=i;

/* Now find the parity of the permutation.
 * if its even ans=="YES"
 * see https://en.wikipedia.org/wiki/Parity_of_a_permutation
 */

    if(parity(s)==parity(t))
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    /* Observation.
     * If at least one letters freq is >1 then any flip can be done in both
     * strings.
     * How to find if the number of flips is odd?
     * -> Count number of diffing positions.
     */
    cini(t);
    while(t--)
        solve();

}

