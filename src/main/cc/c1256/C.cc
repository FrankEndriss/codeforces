/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    cini(d);
    cinai(c,m);
    
    vi pos(m);
    pos[0]=1;
/* first place the platforms most possibly left. */
    for(int i=1; i<m; i++)
        pos[i]=pos[i-1]+c[i-1];
        
/* Then move, from right to left, all platforms as much needed
 * positions to the right that one can jump from one to another.
 * If a platform need not to be moved we are finished.
 * If first platform from left is reachable thats ans, else -1.
 */
    
    int rpos=n+1;
    for(int i=m-1; i>=0; i--) {
//cout<<"rpos="<<rpos<<endl;
        int nrpos=rpos-d-(c[i]-1);
//cout<<"i="<<i<<" d="<<d<<" c[i]="<<c[i]<<" nrpos="<<nrpos<<endl;
        if(nrpos>pos[i])
            pos[i]=nrpos;
        else
            break;
        rpos=nrpos;
    }

/* Output result */
    if(pos[0]<=d) {
        cout<<"YES"<<endl;
        int idx=1;
        for(int i=0; i<m; i++) {
            while(idx<pos[i]) {
                cout<<"0"<<" ";
                idx++;
            }
            for(int j=0; j<c[i]; j++){
                cout<<i+1<<" ";
                idx++;
            }
        }
        while(idx<n+1) {
            cout<<"0 ";
            idx++;
        }
        cout<<endl;
    } else
        cout<<"NO"<<endl;
        


}

