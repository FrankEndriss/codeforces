/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;

void solve() {
    cini(n);
    cini(a);
    cini(b);
    cins(s);

    ll INF=1e18;

    vvll dp(n+1, vll(2));    // dp[i][j]== min cost for pipeline untile seg i on hight j
    dp[0][0]=b;
    dp[0][1]=INF;

    for(int i=1; i<=n; i++) {
        dp[i][1]=min(dp[i-1][0]+a*2+b*2, dp[i-1][1]+a+b*2);
        if(s[i-1]=='0') {
            dp[i][0]=min(dp[i-1][0]+a+b, dp[i-1][1]+a*2+b*2);
        } else
            dp[i][0]=INF;
    }

    cout<<dp[n][0]<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

