/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);

    vvi a(n, vi(m));
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++)
            cin>>a[i][j];
    }

    function<bool(int, int)> isLUp=[&](int x, int y) {
        return a[x][y] && x+1<n && y+1<m && a[x+1][y] && a[x][y+1] && a[x+1][y+1];
    };
    function<bool(int, int)> isRUp=[&](int x, int y) {
        return a[x][y] && x-1>=0 && y+1<m && a[x-1][y] && a[x][y+1] && a[x-1][y+1];
    };
    function<bool(int, int)> isLDown=[&](int x, int y) {
        return a[x][y] && x+1<n && y-1>=0 && a[x+1][y] && a[x][y-1] && a[x+1][y-1];
    };
    function<bool(int, int)> isRDown=[&](int x, int y) {
        return a[x][y] && x-1>=0 && y-1>=0 && a[x-1][y] && a[x][y-1] && a[x-1][y-1];
    };

    int cnt=0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(a[i][j])
                cnt++;

            if( !a[i][j] ||
                (a[i][j] && (isLUp(i,j) || isRUp(i,j) || isLDown(i,j) || isRDown(i,j))))
                ;
            else {
                cout<<"-1"<<endl;
                return 0;
            }
        }
    }
    cout<<cnt<<endl;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(!a[i][j])
                continue;
            if(isLUp(i,j))
                cout<<i+1<<" "<<j+1<<endl;
            else if(isRUp(i,j)) 
                cout<<i<<" "<<j+1<<endl;
            else if(isLDown(i,j))
                cout<<i+1<<" "<<j<<endl;
            else if(isRDown(i,j))
                cout<<i<<" "<<j<<endl;
            else
                assert(false);
        }
    }

}

