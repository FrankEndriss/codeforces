
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int num[100];
int prd[100];

int main() {
int n;
	cin>>n;
int digits=0;
	prd[0]=1;
	while(n) {
		num[digits++]=n%10;
		n/=10;
		prd[0]*=num[digits-1];
	}

	for(int i=1; i<digits; i++) {
		prd[i]=1;
		// decrease num[i] by 1, set num[i-x] to 9
		for(int j=0; j<digits; j++) {
			int m=num[j];
			if(j<i) 
				m=9;
			else if(j==i)
				m=num[j]-1;
			if(m>0) // ignore 0s is ok here
				prd[i]*=m;
		}
	}

	int ans=-1;
	for(int i=0; i<digits; i++)
		ans=max(ans, prd[i]);
	cout<<ans<<endl;
}

