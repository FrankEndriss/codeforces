
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

int root;


typedef struct node {
	int parent;
	vector<int> childs;
	int respect;
} node;

// nodes matching the respect criteria and should be removed
// NOTE we do _not_ need a queue here, vector would be sufficient,
// because we fill that list once, then finish.
priority_queue<int, vector<int>, greater<int> > prioQ;

node tree[100001];

/** Checks node for removal, called after adding childs. */
void checkNode(int node) {
	if(node<=0)
		exit(2);
	if(node==root)
		return;
	if(tree[node].respect==1) {
		int lrespect=1;
		for(uint i=0; i<tree[node].childs.size(); i++) {
			if(tree[tree[node].childs[i]].respect==0) {
				lrespect=0;
				break;
			}
		}
		if(lrespect) {
			prioQ.push(node);
		}
	} else {
	}
}

/** Records if node matches the respect criteria, hence must be removed. */
void dfsMatch(int node) {
	checkNode(node);
	for(uint i=0; i<tree[node].childs.size(); i++)
		dfsMatch(tree[node].childs[i]);
}

int main() {
int n;
	cin>>n;
	for(int i=1; i<=n; i++) {
		cin>>tree[i].parent>>tree[i].respect;
		if(tree[i].parent==-1)
			root=i;
		else 
			tree[tree[i].parent].childs.push_back(i);
	}

	dfsMatch(root);

	int printed=0;
	while(!prioQ.empty()) {
		printed=1;
		int node=prioQ.top();
		prioQ.pop();
		cout<<node<<" ";
	}
	if(!printed)
		cout<<-1;
	cout<<endl;
}

