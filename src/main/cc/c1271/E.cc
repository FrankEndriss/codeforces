/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* y is contained if n>=y y*2 is in list
 * Let x=n..1;
 * x=y -> k=1
 * x=y/2
 **/
void solve() {
    cinll(n);
    cinll(k);

    const int M=100000000;
    vll memo(M,-1);
/* Number of times x is in lists for given n */
    function<ll(ll)> cnt=[&](ll x) {
//cout<<"cnt, x="<<x<<" n="<<n<<endl;
        if(x==0 || x>n)
            return 0LL;
        else if(x==1)
            return n;
        else if(x==2)
            return n-1;
        else {
            if(x<M && memo[x]>=0)
                return memo[x];

            ll ret;
            if(x&1) {
                ret= 1+cnt(x*2);
            } else { 
                ret= 1+cnt(x+1)+cnt(x*2);
            }
            
            if(x<M)
                memo[x]=ret;
            return ret;
        }
    };

/*
    for(ll i=M; i>0; i--) {
        if(i%100==0)
            cout<<"precalc "<<i<<endl;
        cnt(i);
    }
*/

    ll lo=1;
    ll hi=n+1;
    if(hi%2==0)
        hi++;

    while(lo+2<hi) {
        ll mid=(lo+hi)/2;
        ll c=cnt(mid);
cout<<"path("<<mid<<")="<<c<<endl;
        if(c>=k) {
            lo=mid;
            if(lo%2==0)
                lo--;
        } else {
            hi=mid;
            if(hi%2==0)
                hi--;
        }
    }
    ll ans1=lo;

    lo=0;
    hi=n+1;
    if(hi%2)
        hi++;

    while(lo+2<hi) {
        ll mid=(lo+hi)/2;
        if(mid&1)
            mid--;
        ll c=cnt(mid);
cout<<"path("<<mid<<")="<<c<<endl;
        if(c>=k)
            lo=mid;
        else
            hi=mid;
    }
    ll ans2=lo;

    cout<<max(ans1,ans2)<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

