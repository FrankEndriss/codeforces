/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cinll(sx);
    cinll(sy);

    //vll q1(4);   // left-up, right-up, right-down, left-down; pos y goes up
    vll q(4);    // up, right, down, left

    for(int i=0; i<n; i++) {
        cinll(px);
        cinll(py);
        px-=sx;
        py-=sy;
        if(py>0)
            q[0]++;
        if(px>0)
            q[1]++;
        if(py<0)
            q[2]++;
        if(px<0)
            q[3]++;
    }
    vector<pair<ll,pair<ll,ll>>> ans;
    ans.push_back({q[0], { sx, sy+1 }});
    ans.push_back({q[1], { sx+1, sy }});
    ans.push_back({q[2], { sx, sy-1 }});
    ans.push_back({q[3], { sx-1, sy }});

    sort(ans.begin(), ans.end());
    auto a=ans.back();
    cout<<a.first<<endl;
    cout<<a.second.first<<" "<<a.second.second<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

