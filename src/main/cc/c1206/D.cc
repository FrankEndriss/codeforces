/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);

    vvll a(63);
    vll aval(n);

    for(int i=0; i<n; i++) {
        cin>>aval[i];
        for(int j=0; j<=62; j++)
            if(aval[i]&(1<<j))
                a[j].push_back(i);
    }

    vi color(n);
    vi parent(n);
    int cycle_end, cycle_start;

    queue<int> q;
    function<bool()> bfs=[&]() {
        int node=q.front();
        q.pop();
        if(color[node]!=0)
            return false;
//cout<<"dbg node="<<node<<endl;

        color[node]=1;

        for(int j=0; j<=62; j++)  {
            if(aval[node]&(1<<j)) {
                for(int c : a[j]) {
                    if(c!=node && c!=parent[node]) {
                        if(color[c]==0) {
                            parent[c]=node;
                            q.push(c);
                        } else if(color[c]==1) {
                            cycle_end=node;
                            cycle_start=c;
//cout<<"dbg bfs true, start="<<cycle_start<<" cylcle_end="<<cycle_end<<endl;
                            return true;
                        }
                    }
                }
            }
        }
        //color[node]=2;
        return false;
    };

    const int INF=1e9+7;
    cycle_start=-1;
    int ans=INF;

    color.assign(n, 0);
    parent.assign(n, -1);
    cycle_start=-1;

    for(int i=0; i<n; i++) {
        if(color[i]==0) {
            while(q.size())
                q.pop();

            q.push(i);
            while(q.size()) {
                if(bfs()) {
//for(int i=0; i<n; i++) {
//cout<<"parent["<<i<<"]="<<parent[i]<<endl;
//}
                    set<int> cycle;
                    while(cycle_start>=0 && cycle.count(cycle_start)==0) {     
                        cycle.insert(cycle_start);
                        cycle_start=parent[cycle_start];
                    }
                    while(cycle_end>=0 && cycle.count(cycle_end)==0) {     
                        cycle.insert(cycle_end);
                        cycle_end=parent[cycle_end];
                    }

                    ans=min(ans, (int)cycle.size());
                    break;
                }
            }
        }
        cycle_start=-1;
/*
*/
    }

    if(ans==INF) 
        cout<<-1<<endl;
    else
        cout<<ans<<endl;

}

