/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);

    vi a(n);
    vi x(n);
    for(int i=0; i<n; i++) {
        a[i]=i;
        cout<<bitset<8>(a[i])<<" ";
        x[i]^=a[i];
    }
    cout<<endl;

    for(int j=1; j<n; j++) {
        for(int i=0; i<n; i++) {
            a[i]+=n;
            //a[i]%=(n*n);
            cout<<bitset<8>(a[i])<<" ";
            x[i]^=a[i];
        }
        cout<<endl;
    }

cout<<"xor:"<<endl;
    for(int i=0; i<n; i++) {
        cout<<bitset<8>(x[i])<<" ";
    }
    cout<<endl;
}

