/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinai(a, n);

    map<int, vi> idx;
    for(int i=0; i<n; i++) {
        idx[a[i]].push_back(i);
    }

    int leftmostSecond=n;
    int leftmostFirst=n;
    int rightmostPrelast=0;
    int rightmostLast=0;
    int la=-1;
    int ra=-1;

    bool zero=true;
    for(auto ent : idx) {
        if(ent.second.size()<2)
            continue;

        zero=false;
        if(ent.second[0]<leftmostFirst) {
            leftmostFirst=ent.second[0];
            la=ent.first;
        }
        if(ent.second[1]<leftmostSecond) {
            leftmostSecond=ent.second[1];
        }

        if(ent.second[ent.second.size()-1] > rightmostLast) {
            rightmostLast=ent.second[ent.second.size()-1];
            ra=ent.first;
        }
        if(ent.second[ent.second.size()-2] > rightmostPrelast) {
            rightmostPrelast=ent.second[ent.second.size()-2];
        }
    }

cout<<"lF="<<leftmostFirst<<" lS="<<leftmostSecond<<" rPL="<<rightmostPrelast<<" rL="<<rightmostLast<<endl;
    if(zero)
        cout<<0<<endl;
    else {
        if(la==ra) {
            int a1=abs(rightmostPrelast-leftmostFirst);
            int a2=abs(rightmostLast-leftmostSecond);
            cout<<min(a1, a2)+1<<endl;
        } else {
            /* case leftmostFirst and rightmostLast stays */
            int left1=min(leftmostSecond, idx[ra][0]);
            int right1=max(rightmostPrelast, idx[la][idx[la].size()-1]);
            int a1=abs(right1-left1);
            cout<<a1+1<<endl;
        }
    }

}

