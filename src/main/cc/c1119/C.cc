/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

int A[501][501];
int B[501][501];
int main() {
// all rows, all cols diff of count must be even
int n, m; cin>>n>>m;
	for(int r=0; r<n; r++)
		for(int c=0; c<m; c++)
			cin>>A[r][c];
	for(int r=0; r<n; r++)
		for(int c=0; c<m; c++)
			cin>>B[r][c];

int rD[501];
	for(int r=0; r<n; r++) {
		rD[r]=0;
		for(int c=0; c<m; c++) {
			if(A[r][c]!=B[r][c])
				rD[r]++;
		}
	}
int cD[501];
	for(int c=0; c<m; c++) {
		cD[c]=0;
		for(int r=0; r<n; r++) {
			if(A[r][c]!=B[r][c])
				cD[c]++;
		}
	}

	for(int r=0; r<n; r++)
		if(rD[r]%2) {
			cout<<"No"<<endl;
			return 0;
		}
	for(int c=0; c<m; c++)
		if(cD[c]%2) {
			cout<<"No"<<endl;
			return 0;
		}
	cout<<"Yes"<<endl;
}

