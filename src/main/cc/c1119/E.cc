/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned int uint;

int main() {
int n; cin>>n;
        vector<int> a(n);
	for(auto &i : a)
		cin>>i;

// possible tri are:
// 1. 3 of same size
// 2. 2 of same size and 1 smaller
//
   /* Start at smallest. every a[i] match as much smaller to two of size a[i]. If no more smaller avail then
    * remove es much triple as possible.
    */
	
	long long ans=0;
        int smallest=0;
	for(int i=0; i<n; i++) {
            while(a[i]>=2) {
                while(a[smallest]==0 && smallest<n-1)
                    smallest++;
                if(smallest<i) {
                    a[smallest]--;
                    a[i]-=2;
                    ans++;
                } else
                    break;
            }
            ans+=a[i]/3;
            a[i]%=3;
        }
	cout<<ans<<endl;
}

