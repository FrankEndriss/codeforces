
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

int h[1001];

int main() {
int n, sz; cin>>n>>sz;
	for(int i=0; i<n; i++)
		cin>>h[i];

	for(int k=1; k<=n; k++) {
//cout<<"log trying k="<<k<<endl;
		sort(h, h+k);
		int hig=0;
		for(int i=k; i>=0; i--) {
			if((k-i)%2==1) { // ignore odd bottles
				hig+=h[i];
//cout<<"log bottle["<<i<<"].h="<<h[i]<<" hig="<<hig<<" sz="<<sz<<endl;
				if(hig>sz) {
					cout<<k-1<<endl;
					return 0;
				}
			}
		}
	}
//cout<<"log ans=n"<<endl;
	cout<<n<<endl;

}

