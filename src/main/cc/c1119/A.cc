
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int a[300003];
int dp[2][1<<20];

int main() {
    int n;
    cin>>n;
    for(int i=0; i<n; i++)
        cin>>a[i];

    dp[1][0]=1; /* Because if x==0 a funny pair is (0, i) for even number of elements, which are at odd i caused by 0 based indexing. */
    int x=0;
    ll ans=0;
    for(int i=0; i<n; i++) {
        x^=a[i];
        ans+=dp[i%2][x];
        dp[i%2][x]++;
    }
    cout<<ans<<endl;


}

