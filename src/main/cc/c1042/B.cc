/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

set<int> abc;
set<int> ab;
set<int> ac;
set<int> bc;
set<int> a;
set<int> b;
set<int> c;

int main() {
int n;
    cin>>n;
    for(int i=0; i<n; i++) {
        int p;
        cin>>p;
        string s;
        cin>>s;
        sort(s.begin(), s.end());
        if(s=="A")
            a.insert(p);
        else if(s=="B")
            b.insert(p);
        else if(s=="C")
            c.insert(p);
        else if(s=="AB")
            ab.insert(p);
        else if(s=="AC")
            ac.insert(p);
        else if(s=="BC")
            bc.insert(p);    
        else if(s=="ABC")
            abc.insert(p);
        else
            assert(false);
    }

    const int INF=10000000;
    int fABC=abc.size()>0?*abc.begin():INF;
    int fAB=ab.size()>0?*ab.begin():INF;
    int fAC=ac.size()>0?*ac.begin():INF;
    int fBC=bc.size()>0?*bc.begin():INF;
    int fA=a.size()>0?*a.begin():INF;
    int fB=b.size()>0?*b.begin():INF;
    int fC=c.size()>0?*c.begin():INF;
    
    vector<int> mi;
    mi.push_back(fABC);
    mi.push_back(fAB+fAC);
    mi.push_back(fAB+fBC);
    mi.push_back(fAB+fC);
    mi.push_back(fAC+fB);
    mi.push_back(fAC+fBC);
    mi.push_back(fAC+fAB);
    mi.push_back(fBC+fA);
    mi.push_back(fBC+fAC);
    mi.push_back(fBC+fAB);
    mi.push_back(fA+fB+fC);
    int ans=*min_element(mi.begin(), mi.end());
    if(ans>=INF)
        ans=-1;
    cout<<ans<<endl;
}

