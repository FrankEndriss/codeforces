/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, m;
    cin>>n>>m;

    vector<int> a(n);
    int maBench=0;
    int miBench=1000000001;
    fori(n) {   
        cin>>a[i];
        maBench=max(maBench, a[i]);
        miBench=min(miBench, a[i]);
    }

    int maK=maBench+m;

    while(m>0) {    // brute force distribute persons to benches, starting at emptiest
        for(int i=0; i<n; i++) {
            if(a[i]==miBench) {
                a[i]++;
                m--;
                if(m==0) 
                    break;
            }
        }
        miBench++;
    }

    int miK=*max_element(a.begin(), a.end());
    cout<<miK<<" "<<maK<<endl;

}

