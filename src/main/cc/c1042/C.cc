/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    uint n;
    cin>>n;
    vector<int> a(n);
    
/* We need to consider 0s, and negative values.
 * First, multiply all 0s, and if number of negative values is odd, remove biggest negative number, too.
 * Then multiply all others.
 * Cornercase: There are no others.
 */
    int negcount=0;
    int maxneg=-1000000001;
    int maxnegidx=-1;
    vector<int> rem;
    vector<int> neg;
    vector<int> pos;
    fori(n) {
        cin>>a[i];
        if(a[i]==0) {
            rem.push_back(i);
        } else if(a[i]<0) {
            neg.push_back(i);
            negcount++;
            if(a[i]>maxneg) {
                maxneg=a[i];
                maxnegidx=i;
            }
        } else 
            pos.push_back(i);
    }
    vector<int> notrem;
    if(negcount%2==1)
        rem.push_back(maxnegidx);
    else if(negcount>0)
        notrem.push_back(maxnegidx);

    for(auto i : neg)
        if(i!=maxnegidx)
            notrem.push_back(i);

    for(auto i : pos)
        notrem.push_back(i);

    // TODO
    // 1. remove all in rem
    // 2. multiply all others.
    sort(rem.begin(), rem.end());
    for(int i=1; i<rem.size(); i++)
        cout<<1<<" "<<rem[i-1]+1<<" "<<rem[i]+1<<endl;
    if(rem.size()>0 && rem.size()<n)
        cout<<2<<" "<<rem[rem.size()-1]+1<<endl;

    if(rem.size()==n)
        return 0;

    sort(notrem.begin(), notrem.end());

    if(notrem.size()<=1)
        return 0;

    for(int i=1; i<notrem.size(); i++)
        cout<<1<<" "<<notrem[i-1]+1<<" "<<notrem[i]+1<<endl;
}

