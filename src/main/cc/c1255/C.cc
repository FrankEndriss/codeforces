/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    vvi a(n-2, vi(3));
    vvi f(n);

    for(int i=0; i<n-2; i++)
        for(int j=0; j<3; j++) {
            cin>>a[i][j];
            a[i][j]--;
            f[a[i][j]].push_back(i);
        }

#ifdef DEBUG
cout<<"parsed"<<endl;
#endif

    // true if two numbers match
    function<bool(int,int)> match2=[&](int i1, int i2) {
        int ans=0;
        for(int i=0; i<3; i++)
            for(int j=0; j<3; j++) {
                if(a[i1][i]==a[i2][j])
                    ans++;
            }

        return ans==2;
    };

    int start=-1;
    for(int i=0; i<n-2 && start<0; i++)
        for(int j=0; j<3; j++)
            if(f[a[i][j]].size()==1)
                start=i;

#ifdef DEBUG
cout<<"found start="<<start<<endl;
#endif
    assert(start>=0);


    /* return first member of i1 which is not in i2 */
    function<int(int,int)> notin=[&](int i1, int i2) {
        for(int i=0; i<3; i++) {
            bool found=false;
            for(int j=0; j<3; j++) {
                if(a[i1][i]==a[i2][j])
                    found=true;
            }
            if(!found)
                return a[i1][i];
        }
        assert(false);
    };

    function<int(int,int)> match1=[&](int i1, int i2) {
        for(int i=0; i<3; i++) {
            for(int j=0; j<3; j++) {
                if(a[i1][i]==a[i2][j])
                    return a[i1][i];
            }
        }
        assert(false);
    };

/** @return match2 of cur which is not last. */
    function<int(int,int)> next=[&](int cur, int last) {
        for(int i=0; i<3; i++)
            for(int k : f[a[cur][i]])
                if(k!=last && k!=cur && match2(cur, k))
                    return k;
        return -1;
    };

    int pprev=-1;
    int prev=-1;
    do {
        int ne=next(start, prev);

        if(ne>=0)
            cout<<notin(start, ne)+1<<" ";
        else {   // last
            int fi=match1(start, pprev);
            cout<<fi+1<<" ";
            int last=notin(start, prev);
            for(int k=0; k<3; k++) {
                if(a[start][k]!=fi && a[start][k]!=last)
                    cout<<a[start][k]+1<<" ";
            }
            cout<<last+1<<endl;
        }
        pprev=prev;
        prev=start;
        start=ne;
    }while(start>=0);
    cout<<endl;
    
}

