/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(m);
    cinai(a,n);

    if(n<3 || m<n) {
        cout<<-1<<endl;
        return;
    }

    ll ans=0;
    int mi1=1e9;
    int mi2=1e9;
    int miI1=-1;
    int miI2=-1;
    for(int i=0; i<n; i++) {
        ans+=(a[i]*2);

        if(a[i]<mi1) {
            miI2=miI1;
            miI1=i;
            mi2=mi1;
            mi1=a[i];
        } else if(a[i]<mi2) {
            mi2=a[i];
            miI2=i;
        }
    }

    ans+=((mi1+mi2)*(m-n));

    cout<<ans<<endl;
    int cnt=0;
        for(int i=0; i<n && cnt<m; i++) {
            cout<<i+1<<" "<<((i+1)%n)+1<<endl;
            cnt++;
        }

    for(int i=cnt; i<m; i++) 
        cout<<miI1+1<<" "<<miI2+1<<endl;
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

