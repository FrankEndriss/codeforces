/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int N=1e5+7;
vi x(N);

void solve() {
    cini(n);
    cini(r);
    cinai(a, n);
    int ma=-1;
    for(int i=0; i<n; i++) {
        x[a[i]]++;
        ma=max(ma, a[i]);
    }
    int ans=0;
    int left=0;
    for(int i=ma; i>left; i--) {
        if(x[i]>0) {
            ans++;
            left+=r;
        }
    }
    cout<<ans<<endl;

    for(int i=0; i<n;i++)   // reset to zero for next testcase
        x[a[i]]=0;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(q);
    while(q--)
        solve();
}

