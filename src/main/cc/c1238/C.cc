/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(h);
    cini(n);
    cinai(p, n);
    //p.push_back(0);

    //int cur=h;
    int ans=0;

    vi cons;
    int cnt=1;
    for(size_t i=0; i<n-1; i++) {
        if(p[i]==p[i+1]+1) {
            cnt++;
        } else {
            cons.push_back(cnt);
            cnt=1;
        }
    }
    cons.push_back(cnt);
    for(int i : cons) {
        if((i&1)==0)
            ans++;
    }

    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(q);
    while(q--)
        solve();

}

