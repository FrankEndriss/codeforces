/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll gaus(ll n) {
    return (n*(n+1))/2;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cins(s);

    vi comp;
    int cnt=1;
    for(int i=1; i<n; i++) {
        if(s[i]==s[i-1])
            cnt++;
        else {
            comp.push_back(cnt);
            cnt=1;
        }
    }
    comp.push_back(cnt);

    /* count all substrings minus
     * those starting xor ending with one rep
     */
    ll ans=gaus(n-1);
    if(comp.size()>1) {
        for(size_t i=0; i<comp.size(); i++) {
            if(i==0||i==comp.size()-1)
                ans-=comp[i];
            else {
                ans-=(2*comp[i]);
                ans++;
            }
        }
        ans++;  // for first and last double count
    }

    cout<<ans<<endl;

}

