/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    cins(s);

    vvi f(m, vi(m));

    for(int i=0; i<n-1; i++) {
        int c1=s[i];
        int c2=s[i+1];
        if(c1!=c2) {
            f[c1-'a'][c2-'a']++;
            f[c2-'a'][c1-'a']++;
        }
    }
//cout<<"counted f"<<endl;

    /* find max pair */
    int ma=-1;
    int mac1;
    int mac2;
    for(int i=0; i<m; i++) {
        for(int j=i+1; j<m; j++) {
            if(f[i][j]>ma) {
                mac1=i;
                mac2=j;
            }
        }
    }
//cout<<"found max pair,mac1="<<mac1<<" mac2="<<mac2<<endl;

    /* start with max pair */
    vb used(m, false);
    used[mac1]=true;
    used[mac2]=true;

    list<int> kb;
    kb.push_front(mac1);
    kb.push_back(mac2);

    /* extend to left and right */
    while(kb.size()<m) {
        int left=kb.front();        
        int right=kb.back();
//cout<<" left="<<left<<" right="<<right<<endl;

        int maleft=-1;
        int maleftidx=-1;
        for(int i=0; i<m; i++) {
            if(!used[i] && f[left][i]>maleft)  {
                maleft=f[left][i];
                maleftidx=i;
            }
        }

        int maright=-1;
        int marightidx=-1;
        for(int i=0; i<m; i++) {
            if(!used[i] && f[right][i]>maright)  {
                maright=f[right][i];
                marightidx=i;
            }
        }
        
        if(maleft>maright) {
            kb.push_front(maleftidx);
            used[maleftidx]=true;
        } else {
            kb.push_back(marightidx);
            used[marightidx]=true;
        }
    }

//cout<<"index positions"<<endl;
    vi pos(m);
    int idx=0;
    for(auto it=kb.begin(); it!=kb.end(); it++) {
        pos[*it]=idx;
        idx++;
    }

//cout<<"calc ans"<<endl;
    ll ans=0;
    for(size_t i=0; i<s.size()-1; i++)
        ans+=abs(pos[s[i]-'a']-pos[s[i+1]-'a']);
    
    cout<<ans<<endl;

}

