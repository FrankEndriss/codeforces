/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    int n, k;
    cin>>n>>k;
    vi a(n);
    ll sum=0;
    vi ans;
    int lastIdx=-1;
    fori(n) {
        cin>>a[i];
    }

    fori(n) {
        if(k==1)
            break;

        sum+=a[i];

        if(sum&1) {
            ans.push_back(i);
            lastIdx=i;
            k--;
            sum=0;
        }
    }
    sum=0;
    for(int i=lastIdx+1; i<n; i++) {
        sum+=a[i];
    }
    if(sum&1) {
        cout<<"YES"<<endl;
        for(int i=0; i<ans.size(); i++) {
            cout<<ans[i]+1<<" ";
        }
        cout<<n<<endl;
    } else {
        cout<<"NO"<<endl;
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    fori(n)
        solve();

}

