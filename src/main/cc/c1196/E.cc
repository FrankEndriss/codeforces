/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    int b, w;
    cin>>b>>w;

    if((b==1 || w==1) && max(b, w)>4)
        cout<<"NO"<<endl;
    else if(min(b, w)*3<max(b, w)-1) {
        cout<<"NO"<<endl;
    } else {
        cout<<"YES"<<endl;
        int curr=1;
        bool isWhite=false;
        if(w>b) {
            curr=2;
            isWhite=true;
        }

        while(w>0 || b>0) {
            cout<<"2 "<<curr<<endl;
            if(isWhite) {
                w--;
                if(b>w) {
                    cout<<"1 "<<curr<<endl;
                    b--;
                }
                if(b>w) {
                    cout<<"3 "<<curr<<endl;
                    b--;
                }
            } else {
                b--;
                if(w>b) {
                    cout<<"1 "<<curr<<endl;
                    w--;
                }
                if(w>b) {
                    cout<<"3 "<<curr<<endl;
                    w--;
                }
            }
            curr++;
            isWhite=!isWhite;
        }
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

