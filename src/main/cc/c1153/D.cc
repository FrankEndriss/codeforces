/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

#define N 300001
#define ROOT 0
int ops[N];
vector<int> tree[N];
int num_min;
int num_max;

int main() {
uint n;
	cin>>n;
	for(uint i=0; i<n; i++)
		cin>>ops[i];
	for(uint i=0; i<n-1; i++) {
		int p;
		cin>>p;
		tree[p-1].push_back(i);
	}

	num_min=1;
	num_max=0;
	for(uint i=0; i<n; i++)
		if(tree[i].size()==0)
			num_max++;

// TODO
	dfs(ROOT, ops[ROOT]);

}

