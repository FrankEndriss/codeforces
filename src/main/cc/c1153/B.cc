/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

#define N 101
int front[N];
int idxFront[N];
int leftV[N];
int idxLeft[N];
int out[N][N];

int main() {
int n, m, h;
	cin>>n>>m>>h;
	for(int i=0; i<m; i++) {
		cin>>front[i];
	}

	for(int i=0; i<n; i++) {
		cin>>leftV[i];
	}

	for(int i=0; i<n; i++) {
		for(int j=0; j<m; j++) {
			int top;
			cin>>top;
			if(top)
				cout<<min(front[j], leftV[i])<<" ";
			else 
				cout<<0<<" ";
		}	
		cout<<endl;
	}


}

