/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define N 101
int s[N];
int d[N];
int m[N];

bool mcmp(int i1, int i2) {
	return m[i1]<m[i2];
}

int main() {
int n, t;
	cin>>n>>t;
	for(int i=0; i<n; i++)
		cin>>s[i]>>d[i];

	/* find number of minutes the next bus arrives after t */
	int mVal;
	int mIdx=-1;
	for(int i=0; i<n; i++) {
		int m;
		if(t<=s[i])
			m=s[i]-t;
		else {
			int mod=((t-s[i])%d[i]);
			if(mod>0)
				m=d[i]-mod;
			else 
				m=0;
		}
		if(mIdx<0 || m<mVal) {
			mIdx=i;
			mVal=m;
		}
	}

	cout<<mIdx+1<<endl;
}

