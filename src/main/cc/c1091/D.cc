/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

#define MOD 998244353

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

int main() {
    int n;
    cin>>n;
    vector<int> fak(n+1);

/* There are N subarrs identical to the permutations.
 * Then we need to add all subarrs where the lexicographical next subarr starts with the same prefix as the current one. 
 * For each prefix we have to add one.
 * The number of permutations of len N is N!
 * So, the number of pairs with prefix len 1 we need to add is N! - n
 * So, the number of pairs with prefix len 2 we need to add is N! - n*n-1
 * So, the number of pairs with prefix len 3 we need to add is N! - n*(n-1)*(n-2)
 * ...
 */
    fak[1]=1;
    for(int i=2; i<=n; i++)
        fak[i]=mul(fak[i-1], i);

    int prod=n;
    int ans=fak[n];
    for(int i=1; i<n; i++) {
        ans=pl(ans, pl(fak[n], -prod));
        prod=mul(prod, n-i);
    }
    cout<<ans<<endl;
}

