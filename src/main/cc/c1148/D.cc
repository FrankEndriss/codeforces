/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
int n;
    cin>>n;

    vector<pair<int, int>> ab(n);
    vector<int> fstBiggerByA;
    vector<int> fstBiggerByB;
    vector<int> secBiggerByA;
    vector<int> secBiggerByB;

    fori(n) {
        int a, b;
        cin>>a>>b;
        ab.push_back({a, b});

        if(a>b) {
            fstBiggerByA.push_back(i);
            fstBiggerByB.push_back(i);
        } else {
            secBiggerByA.push_back(i);
            secBiggerByB.push_back(i);
        }
    }

    function<bool(int,int)> byAdesc=[&](int i1, int i2) {
        return ab[i2].first<ab[i1].first;
    };
    function<bool(int,int)> byBdesc=[&](int i1, int i2) {
        return ab[i2].second<ab[i1].second;
    };

    sort(fstBiggerByA.begin(), fstBiggerByA.end(), byA);
    sort(fstBiggerByB.begin(), fstBiggerByB.end(), byB);

    sort(secBiggerByA.begin(), secBiggerByA.end(), byA);
    sort(secBiggerByB.begin(), secBiggerByB.end(), byB);

/* start with a>b. (sol 2) */
    int ans1;
    int idxa=0;
    int idxb=0;
    while(idxa<n && idxb<<n) {
/* We need to use the first fst where the b is smaller than
 * the biggest a of sec.
 * So we use fst sort by B desc, and sec sort by A desc.
 *
 * Then we need to use the first sec where a smaller than the given B
 * ... sic
 */
        
        
    }

    int ans2;

}

