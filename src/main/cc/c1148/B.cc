/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m, ta, tb, k;
    cin>>n>>m>>ta>>tb>>k;
    vector<ll> a(n);
    fori(n)
        cin>>a[i];
    vector<ll> b(m);
    fori(m)
        cin>>b[i];

    if(k>=n || k>=m) {
        cout<<-1<<endl;
        return 0;
    }

    ll ans=0;
    for(int i=0; i<=k; i++) {
        ll arrB=a[i]+ta;
        auto it=lower_bound(b.begin(), b.end(), arrB);
        if(it==b.end()) {
            cout<<-1<<endl;
            return 0;
        }
        int idx=distance(b.begin(), it);
        if(idx+k-i >=m) {
            cout<<-1<<endl;
            return 0;
        }
        ans=max(ans, b[idx+k-i]+tb);
    }
    cout<<ans<<endl;

}

