/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

const int N=300001;
int n;
vector<pair<int, int>> swaps;
vector<int> p(N);
vector<int> pos(N);

bool isPossibleSwap(int i, int j) {
    return 2*abs(i-j)>=n;
}

void swap2(int i, int j) {
#ifdef DEBUG
    cout<<"swap "<<i<<" "<<j<<endl;
#endif
    if(i==j)
        return;

    assert(isPossibleSwap(i, j));

    swaps.push_back({i, j});
    int tmp=p[i];
    p[i]=p[j];
    p[j]=tmp;

    pos[p[i]]=i;
    pos[p[j]]=j;
}

int tmppos(int i) {
    if(i<n/2)
        return n-1;
    else
        return 0;
}

void doSwap(int pos1, int pos2) {
#ifdef DEBUG
cout<<"doSwap, pos1="<<pos1<<" "<<pos2<<endl;
    if(isPossibleSwap(pos1, pos2)) {
        swap2(pos1, pos2);
        return;
    }
    int pos3=tmppos(pos2);
    doSwap(pos2, pos3);
    doSwap(pos3, pos1);
    doSwap(pos3, pos2);
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cin>>n;
    fori(n) {
        cin>>p[i];
        p[i]--;
        pos[p[i]]=i;
    }

    for(int i=0; i<n/2; i++) {
        int pos1=n/2-1-i;
        int pos2=pos[pos1];

        doSwap(pos1, pos2);

        pos1=n/2+i;
        pos2=pos[pos2];
        doSwap(pos1, pos2);
    }

    cout<<swaps.size()<<endl;
    for(auto ans : swaps) {
        cout<<ans.first+1<<" "<<ans.second+1<<" ";
    }
    cout<<endl;
}

