/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);

    vector<set<int>> tree(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        tree[u].insert(v);
        tree[v].insert(u);
    }

    set<int> v[3];
    vvb pos(n, vb(3, true));

    for(int node=0; node<n; node++) {
        if(tree[node].size()==0) {  /* no unconnected node */
            cout<<-1<<endl;
            return 0;
        }

        for(int c : tree[node]) {
            for(int i=0; i<3; i++) 
                if(v[i].count(c)) {
                    pos[node][i]=false;
                }
        }

        int group=0;
        while(group<3 && !pos[node][group])
            group++;

        if(group==3) {
            cout<<-1<<endl;
            return 0;
        }

        v[group].insert(node);
        for(int c : tree[node])
            pos[c][group]=false;
    }

/* double check if all connections exist */
    for(int group=0; group<3; group++) {
        for(int node : v[group]) {
            for(int g2=0; g2<3; g2++) {
                if(g2!=group) {
                    /* all group members must be children of node */
                    for(int member : v[g2]) {
                        if(!tree[node].count(member)) {
                            cout<<-1<<endl;
                            return 0;
                        }
                    }
                }
            }
        }
    }

    if(v[0].size()==0 || v[1].size()==0 || v[2].size()==0) {
                cout<<-1<<" ";
                return 0;
    }

/* output ans */
    for(int i=0; i<n; i++) {
        for(int g=0; g<3; g++) {
            if(v[g].count(i)) {
                cout<<g+1<<" ";
                break;
            }
        }
    }
    cout<<endl;

}

