/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(h);
    cini(w);

    vvi g(h, vi(w));

// 1==black
// 2==white
    for(int i=0; i<h; i++) {
        cini(r);
        for(int j=0; j<r; j++) {
            g[i][j]=1;
        }
        if(r<w)
            g[i][r]=2;
    }
    bool impossible=false;
    for(int i=0; i<w; i++) {
        cini(c);
        for(int j=0; j<c; j++) {
            if(g[j][i]==2) {
                impossible=true;
            } else {
                g[j][i]=1;
            }
        }
        if(c<h) {
            if(g[c][i]==1)
                impossible=true;
            else
                g[c][i]=2;
        }
    }

    if(impossible) {
        cout<<0<<endl;
        return 0;
    }

    int cnt=0;
    for(int i=0; i<h; i++)
        for(int j=0; j<w; j++) 
            if(g[i][j]==0)
                cnt++;

    const int MOD=1e9+7;
    ll ans=1;
    for(int i=0; i<cnt; i++) {
        ans*=2;
        ans%=MOD;
    }

    cout<<ans<<endl;

}

