/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

bool check(int i) {
    vb b(10);
    while(i) {
        int d=i%10;
        if(b[d])
            return true;
        b[d]=true;
        i/=10;
    }

    return false;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(l);
    cini(r);

    for(int i=l; i<=r; i++) {
        if(!check(i)) {
            cout<<i<<endl;
            return 0;
        }
    }

    cout<<-1<<endl;
}

