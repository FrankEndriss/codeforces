/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int primeAproxFactor=30;
const int MAXN=100000;
vector<int> pr(MAXN);

void init() {
    vector<bool> cmp(MAXN*primeAproxFactor, false);

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!cmp[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                cmp[j] = true;
                j += i;
            }
        }
        i++;
    }
}

ll g(ll x, ll p) {
    ll ret=1;
    ll ans=1;
    while(ans<x) {
        ans*=p;
        if(x%ans==0)
            ret=ans;
    }
    return ret;
}

const int MOD=1e9+7;

ll f(ll x, ll y) {
    ll ans=1;
    for(int i=0; pr[i]*pr[i]<=x; i++) {
        if(x%pr[i]==0)  {
            ans*=g(y, pr[i]);
            ans%=MOD;
        }
    }
    return ans;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
init();

    // fuck you.. assholes
    
    
    

}

