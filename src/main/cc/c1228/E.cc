/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define MOD 1000000007

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int fak(int n) {
    if(n==1)
        return 1;

    return mul(n, n-1);
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(k);

/* We need to find the number of patterns for
 * grid n*n,
 * and multiply by freeFields^^k
 *
 * faculty(n) * n*(n-1)^^k
 * But, there are soltions double counted...
 * How to get rid of them?
 */

    int f=fak(n);
    int free=mul(n, n-1);
    int p=toPower(k, free);
    p--;
    if(p<0)
        p+=MOD;
    ll ans=mul(f, p);
    ans++;
    ans%=MOD;
    cout<<ans<<endl;
}

