/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n;
    cin>>n;
    vector<int> a(n);
    fori(n)
        cin>>a[i];

    int m=1;
    int ans=1;
    for(int i=1; i<n; i++) {
        if(a[i]>a[i-1] && a[i-1]*2>=a[i]) {
            m++;
            ans=max(ans, m);
        } else 
            m=1;
    }

    cout<<ans<<endl;

}

