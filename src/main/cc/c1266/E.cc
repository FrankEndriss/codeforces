/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* Obs
 * 1. If we add/remove a milestone the number of turn changes by 
 * not more than one, since only one resource can be earned 
 * per milestone.
 * 2. That resource is earned if a[Sj] >= Tj
 */
void solve() {
    cini(n);
    cinai(a,n);
    ll sum=0;
    for(int i : a)
        sum+=i;

    map<pii,int> ms;
    cini(q);
    for(int i=0; i<q; i++) {
        cini(s);
        s--;
        cini(t);
        cini(u);
        u--;
        pii key={s,t};

        auto it=ms.find(key);
        if(it!=ms.end()) {
            int lu=it->second;
            if(a[key.first]>=key.second) 
                sum++;
            ms.erase(it);
        }

        if(u>=0) {
            ms[key]=u;
            if(a[key.first]>=key.second)
                sum--;
        }
        cout<<sum<<endl;
    }
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

