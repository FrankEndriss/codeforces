/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(m);

    vector<map<int,ll>> tree(n);
    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        cini(d);
        tree[u][v]+=d;
    }

    for(int i=0; i<n; i++) {
        for(auto &ent : tree[i]) {
            if(ent.first==i)
                continue;

            ll mi=min(ent.second, tree[ent.first][i]);
            if(mi>0) {
                ent.second-=mi;
                tree[ent.first][i]-=mi;
                continue;
            }

            for(auto &ent2 : tree[ent.first])  {
                if(ent2.second==0 || ent2.first==i)
                    continue;

                ll mi=min(ent.second, ent2.second);
                if(mi>0) {
                    ent.second-=mi;
                    ent2.second-=mi;
                    tree[i][ent2.first]+=mi;
                }
            }
        }
    }

    vvi ans(3);
    for(int i=0; i<n; i++) {
        for(auto ent : tree[i]) 
            if(ent.first!=i && ent.second!=0) {
                ans[0].push_back(i+1);
                ans[1].push_back(ent.first+1);
                ans[2].push_back(ent.second);
            }
    }
    cout<<ans[0].size()<<endl;
    for(int i=0; i<ans[0].size(); i++) {
        for(int j=0; j<3; j++)
                cout<<ans[j][i]<<" ";
        cout<<endl;
    }
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

