/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(q);
    for(int i=0; i<q; i++) {
        cini(n);

        int ans=0;
        if(n==2)
            ans=2;
        else {
            n%=4;
            if(n==1)
                ans=1;
            else if(n==2)
                ans=0;
            else if(n==3)
                ans=1;
            else
                ans=0;
        }

        cout<<ans<<endl;
    }

}

