/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct edge {
    int u;
    int v;
    int w;
};

void solve() {
    cini(n);
    cini(k);
    vi cols(n); // cols[i]=colors of vertex i so far
    vector<edge> vedge(n-1);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        cini(w);
        u--;
        v--;

        vedge[i]={ u, v, w };
    }

    sort(vedge.begin(), vedge.end(), [&](edge e1, edge e2) {
        return e2.w<e1.w;   // biggest first
    });

    ll ans=0;
    for(int i=0; i<n-1; i++) {
        if(cols[vedge[i].u]<k && cols[vedge[i].v]<k) {
            ans+=vedge[i].w;
            cols[vedge[i].u]++;
            cols[vedge[i].v]++;
        }
    }

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(q);
    while(q--)
        solve();
}

