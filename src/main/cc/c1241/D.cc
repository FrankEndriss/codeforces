/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI=3.1415926535897932384626433;
typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    //vi a(n);
    vector<pii> mima(n, { -1, -1 });
    for(int i=0; i<n; i++) {
        cini(x);
        x--;
        //a[i]=x;
        if(mima[x].first<0)
            mima[x].first=i;
        mima[x].second=i;
    }

/* we search the longest l,r for which all elements in a
 * are pre-sorted.
 * Because these do not need to be sorted.
 * Pre-sorted is, if (for l<r) max(l)<min(r)
 * So index min/max index for all elements
 */
    
    int ncnt=0;
    int ma=1;
    int cnt=1;
    int i=0;
    while(mima[i].first<0)
        i++;

    pii prev=mima[i];
    for(; i<n; i++) {
        if(mima[i].first<0)
            continue;
        ncnt++;

        if(mima[i].first>prev.second) {
            cnt++;
            ma=max(ma, cnt);
        } else
            cnt=1;

        prev=mima[i];
    }
//cout<<"ncnt="<<ncnt<<" ma="<<ma<<endl;

    cout<<ncnt-ma<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);


    cini(q);
    while(q--)
        solve();
}

