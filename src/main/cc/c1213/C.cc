/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(q);
    for(int i=0; i<q; i++) {
        cinll(n);
        cinll(m);

        vb d(10);
        int cnt=0;
        ll dig=m%10;
        ll ansPerRnd=0;
        while(!d[dig]) {
            d[dig]=true;
            ansPerRnd+=dig;
            cnt++;
            dig+=m;
            dig%=10;
        }
        ll mPerRnd=cnt*m;
        ll ans=ansPerRnd*(n/mPerRnd);
        ll n2=(n/mPerRnd)*mPerRnd;
        while(n2<=n) {
            ans+=n2%10;
            n2+=m;
        }

        cout<<ans<<endl;
    }

}

