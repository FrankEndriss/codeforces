
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

int a[20];
int b[20];

int check(int lIdx, int rIdx) {
	for(int i=lIdx; i<rIdx; i++)
		b[i]=a[i];
	sort(b+lIdx, b+rIdx);
	int same=1;
	for(int i=lIdx; i<rIdx; i++)
		if(b[i]!=a[i])
			same=false;
	if(same)
		return rIdx-lIdx;
	else
		return max(check(lIdx, lIdx+(rIdx-lIdx)/2), check(lIdx+(rIdx-lIdx)/2, rIdx));
}

int main() {
int n;
	cin>>n;
	for(int i=0; i<n; i++) {
		cin>>a[i];
	}
	int ans=check(0, n);
	cout<<ans<<endl;
}

