
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

int a[11];
int main() {
int n, sum=0;
	cin>>n;

	for(int i=0;i<n; i++) {
		cin>>a[i];
		sum+=a[i];
	}

	int ans=0;
	for(int i=0; i<10; i++) 
		if(sum & (1<<i))
			ans++;
	cout<<ans<<endl;
}

