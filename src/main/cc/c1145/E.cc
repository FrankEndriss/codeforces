
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

int e[]={ 1, 0, 0, 1, 1, 0 };
#define E 6

int main() {
	for(int i=20; i<50; i++) 
		cout<<e[i%E]<<endl;
}

