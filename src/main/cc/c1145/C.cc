/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

int main() {
int n, ans=0;
	cin>>n;
	// bitwise not and rightmost set bit set, too
	ans=~n & 15;
	for(int i=0; i<4; i++) {
		if(n & (1<<i)) {
			ans|=(1<<i);
			break;
		}
	}
	cout<<ans<<endl;
}

