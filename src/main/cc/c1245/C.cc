/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define MOD 1000000007

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}


int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

const int N=100000;
vector<int> fac(N);

void initFac(int mod=MOD) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
}

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    initFac();
    cins(s);

    vi dp;
    int cons=1;
    
    for(size_t i=0; i<s.size(); i++) {
        if(s[i]=='w' || s[i]=='m') {
            cout<<0<<endl;  
            return 0;
        }
        if(i==0)
            continue;

        if(s[i]==s[i-1] && (s[i]=='n' || s[i]=='u')) {
            cons++;
        } else {
            if(cons>1)
                dp.push_back(cons);
            cons=1;
        }
    }
    if(cons>1)
        dp.push_back(cons);

    int ans=1;
    for(int i : dp) {
#ifdef DEBUG
cout<<"dp, i="<<i<<endl;
#endif

/* we got a seq of i items, and want to group them into groups of 1 or 2.
 * How many group combinations are possible?
 * 2: 2
 * 3: 3
 * 4: 5
 * 6:... 111111 21111 12111 11211 11121 11112
 *              2211 2121 2112 1221 1212 1122
 *              222
 *              -> 13
 */
        int lans=1;
        for(int j=1; j<=i/2; j++) { // number of 2s
            lans+=nCr(i-j, j);
            lans%=MOD;
        }
        ans=mul(ans, lans);
    }

    cout<<ans<<endl;
}

