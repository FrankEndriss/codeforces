/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(aR);    // rock
    cini(aP);    // paper
    cini(aS);    // sciss
    cins(s);    // RPS bob

    int bR=0;
    int bP=0;
    int bS=0;
    for(size_t i=0; i<s.size(); i++) {
        if(s[i]=='R')
            bR++;
        else if(s[i]=='P')
            bP++;
        else
            bS++;
    }
#ifdef DEBUG
cout<<"aR="<<aR<<" aS="<<aS<<" bS="<<bS<<" bR="<<bR<<endl;
#endif

    int aWin=0;
    aWin+=min(aR,bS);
    aWin+=min(aP,bR);
    aWin+=min(aS,bP);

#ifdef DEBUG
cout<<"aWin="<<aWin<<" n="<<n<<endl;
#endif

    if(aWin*2>=n) {
        string ans=s;   // copy
        for(size_t i=0; i<s.size(); i++) {
            ans[i]='.';
        }
        for(size_t i=0; i<s.size(); i++) {
            if(s[i]=='R') {
                if(aP) {
                    ans[i]='P';
                    aP--;
                }
            }
            if(s[i]=='P') {
                if(aS) {
                    ans[i]='S';
                    aS--;
                }
            }
            if(s[i]=='S') {
                if(aR) {
                    ans[i]='R';
                    aR--;
                }
            }
        }
        for(size_t i=0; i<ans.size(); i++) {
            if(ans[i]=='.') {
                if(aP) {
                    ans[i]='P';
                    aP--;
                } else if(aS) {
                    ans[i]='S';
                    aS--;
                } else
                    ans[i]='R';
            }
        }

        cout<<"YES"<<endl;
        cout<<ans<<endl;

    } else {
        cout<<"NO"<<endl;
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

