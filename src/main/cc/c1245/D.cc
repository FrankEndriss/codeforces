/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

vector<int> parent, lrank;

void make_set(int v) {
    parent[v] = v;
    lrank[v] = 0;
}

int find_set(int v) {
    if (v == parent[v])
        return v;
    return parent[v] = find_set(parent[v]);
}

void union_sets(int a, int b) {
    a = find_set(a);
    b = find_set(b);
    if (a != b) {
        if (lrank[a] < lrank[b])
            swap(a, b);
        parent[b] = a;
        if (lrank[a] == lrank[b])
            lrank[a]++;
    }
}

struct Edge {
    int u, v;
    ll weight;
    bool operator<(Edge const& other) {
        return weight < other.weight;
    }
};

int n;
vector<Edge> edges;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    vi x(n+1);
    vi y(n+1);
    for(int i=1; i<=n; i++)
        cin>>x[i]>>y[i];

    vll c(n+1);
    for(int i=1; i<=n; i++)
        cin>>c[i];
    vll k(n+1);
    for(int i=1; i<=n; i++)
        cin>>k[i];

    /* cost of connecting city i to city j */
    function<ll(int,int)> concost=[&](int i, int j) {
            return (abs(x[i]-x[j])+abs(y[i]-y[j])) * (k[i]+k[j]);
    };

    /* Greedy:
     * minimum spanning tree!
     * Then power station in cheapest city.
     */

/** create all Edges */
    for(int i=1; i<n; i++)
        for(int j=i+1; j<=n; j++)
            edges.push_back({ i, j, concost(i,j) });

/** Add special edges for build power stations */
    for(int i=1; i<=n; i++)
        edges.push_back({ i, 0, c[i] });
    

/* Kruskal */
    ll cost = 0;
    vector<Edge> result;
    parent.resize(n+1);
    lrank.resize(n+1);
    for (int i = 0; i < n+1; i++)
        make_set(i);

    sort(edges.begin(), edges.end());

    for (Edge e : edges) {
        if (find_set(e.u) != find_set(e.v)) {
            cost += e.weight;
            result.push_back(e);
            union_sets(e.u, e.v);
        }
    }

    vi powers;
    vector<pii> cons;

    ll ans=0;
    for(size_t i=0; i<result.size(); i++) {
        ans+=result[i].weight;

        if(result[i].v==0)
            powers.push_back(result[i].u);
        else
            cons.push_back({result[i].u,result[i].v});
    }
    cout<<ans<<endl;
    cout<<powers.size()<<endl;
    for(int a : powers)
        cout<<a<<" ";
    cout<<endl;
    cout<<cons.size()<<endl;
    for(pii p : cons)
        cout<<p.first<<" "<<p.second<<endl;
}

