/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, z;
    cin>>n>>z;
    vector<int> x(n);
    vector<int> id(n);
    fori(n) {
        cin>>x[i];
        id[i]=i;
    }
    sort(x.begin(), x.end());

    int lidx=n/2;
    int i=0;
    int ans=0;
    while(lidx<n && i<n/2) {
        while(lidx<n && x[lidx]-x[i]<z)
            lidx++;
        if(lidx>=n)
            break;
//cout<<"matched "<<x[i]<<":"<<x[lidx]<<endl;
        lidx++;
        i++;
        ans++;
    }

    cout<<ans<<endl;
    

}

