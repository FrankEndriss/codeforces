/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int t;
    cin>>t;
    fori(t) {
        string s;
        cin>>s;

        vector<int> letters(s.size());
        vector<int> freq(256);
        fori(s.size()) {
            letters.push_back(s[i]);
            freq[s[i]]++;
        }

        char currLetter=0;
        ostringstream oss;
        bool ok=true;
        fori(s.size()) {
            // find allowed letter with max freq, and max neigbours :/
            int ma=0;
            int maNei=0;
            int maIdx=-1;
            fori(256) {
                int numNei=0;
                if(i>='a' && i<='z')
                    numNei+=freq[i-1]+freq[i+1];

                if(freq[i]>=1 && abs(currLetter-i)!=1 && (ma<freq[i] || ( ma==freq[i] && numNei>maNei))) {
                    ma=freq[i];
                    maIdx=i;
                    maNei=numNei;
                }
            }
            if(maIdx<=0)  {
                ok=false;
                break;
            }

            string x="x";
            x[0]=(char)maIdx;
            freq[maIdx]--;
            currLetter=maIdx;
            oss<<x;
        }
        if(ok)
            cout<<oss.str();
        else
            cout<<"No answer";
        cout<<endl;
    }
    return 0;
}

