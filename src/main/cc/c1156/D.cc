/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int n;
vector<pair<int, int>> tree[200001];

vector<unordered_map<int, int>> memo(200001);

int dfs(int node, int parent, bool pathHas1) {
    const int key=parent+ (pathHas1?1000000:0);
    const int ans=memo[node][key];
    if(ans>0)
        return ans;

    ll ret=1;
    for(auto pc : tree[node]) {
        if(pc.first!=parent && ( pc.second==1 || !pathHas1 )) {
            ret+=dfs(pc.first, node, pathHas1 || pc.second==1);
        }
    }

    memo[node][key]=ret;
    return ret;
}

int main() {
    cin>>n;

    for(int i=0; i<n-1; i++) {
        int u, v, c;
        cin>>u>>v>>c;
        u--; v--;
        tree[u].push_back({ v, c });
        tree[v].push_back({ u, c });
    }
    ll ans=0;
    fori(n) 
        ans+=dfs(i, -1, false)-1;

    cout<<ans<<endl;
}

