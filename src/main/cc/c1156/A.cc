
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n;
    cin>>n;
    vector<int> a(n);
    fori(n) {
        cin>>a[i];
    }

    int ans=0;
    fori(n-1) {
        if(a[i]+a[i+1]==5) {
            cout<<"Infinite"<<endl;
            return 0;
        } else if(a[i]+a[i+1]==3) {
            ans+=3;
        } else
            ans+=4;

        // triangle in circle in square
        if(i+2<n && a[i]==3 && a[i+1]==1 && a[i+2]==2)
            ans--;
    }

    cout<<"Finite"<<endl;
    cout<<ans<<endl;
    return 0;
}

