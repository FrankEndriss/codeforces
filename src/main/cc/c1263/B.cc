
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int ch1(int i) {
                int d0=i%10;
                int d1=(d0+1)%10;
                return i-d0+d1;
}

void solve() {
    cini(n);
    cinai(a,n);
    vi b=a;

    for(int i=0; i<n; i++) {
        while(true) {
            int br=true;
            for(int j=0; j<n; j++) {
                if(i==j)
                    continue;
                if(a[i]==a[j]) {
                    a[i]=ch1(a[i]);
                    br=false;
                    break;
                }
            }
            if(br)
                break;
        }
    }

    int ans=0;
    for(int i=0; i<n; i++)
        if(b[i]!=a[i])
            ans++;
    
    cout<<ans<<endl;
    for(int i=0; i<n; i++) {
            stack<int> st;
        for(int j=0; j<4; j++) {
            st.push(a[i]%10);
            a[i]/=10;
        }
        while(st.size()) {
            int k=st.top();
            st.pop();
            cout<<k;
        }
        cout<<endl;
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

