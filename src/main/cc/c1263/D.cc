/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct Dsu {
    vector<int> p;
    int cnt;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
        cnt=size;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        if(p[v]!=v)
            cnt++;
        p[v] = v;
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            cnt--;
        }
    }
};

void solve() {
    cini(n);
    vvi m(256);
    for(int i=0; i<n; i++) {
        cins(s);
        for(char c : s) {
            if(m[c].size()==0 || m[c].back()!=i)
                m[c].push_back(i);
        }
    }

    Dsu dsu(n);
    for(size_t i=0; i<m.size(); i++)
        for(size_t j=1; j<m[i].size(); j++)
            dsu.union_sets(m[i][0], m[i][j]);

    cout<<dsu.cnt<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t=1;
    while(t--)
        solve();
}

