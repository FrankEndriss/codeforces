/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cins(in);
    int pos=0;
    int bal=0;  // balance of paras
    int ncnt=0; // count of nesting
    set<int> ob; // positions of open brackets
    set<int> cb; // positions of close brackets
    int depnestopen;    // position of deepest nested open bracket
    string s=" ";
    for(char c : in) {
        if(c=='R') {
            pos++;
            while(s.size()<pos)
                s+=" ";
        } else if(c=='L') {
            if(pos>0)
                pos--;
        } else if(c=='(') {
            if(s[pos]=='(') {
                ;
            } else if(s[pos]==')') {
                cb.erase(pos);
                bal+=2;
            } else
                bal++;
            s[pos]=c;
            ob.insert(pos);
        } else if(c==')') {
            if(s[pos]=='(') {
                bal-=2;
                ob.erase(pos);
            } else if(s[pos]==')')
                ;
            else
                bal--;
            s[pos]=c;
            cb.insert(pos);
        } else {
            if(s[pos]=='(') {
                bal--;
                ob.erase(pos);
            } else if(s[pos]==')') {
                bal++;
                cb.erase(pos);
            }
            s[pos]=c;
        }
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t=1;
    while(t--)
        solve();
}

