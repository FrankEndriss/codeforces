/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n;
    cin>>n;

    vector<int> a(n);
    fori(n)
        cin>>a[i];

    sort(a.begin(), a.end());
    int ans=0;
    int idx=0;
    for(int i=1; i<=n; i++) {
        if(idx>=n)
            break;
        if(a[idx]>=i) {
//cout<<"A: a[idx]="<<a[idx]<<" i="<<i<<endl;
            ans++;
            idx++;
        } else {
            while(idx<n && a[idx]<i)
                idx++;
            if(idx<n)
                ans++;
            idx++;
//cout<<"B: idx="<<idx<<endl;
        }
    }

    cout<<ans<<endl;

}

