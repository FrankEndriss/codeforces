/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, x, y;
    cin>>n>>x>>y;
string s;
    cin>>s;

    
    int i=(int)s.size()-1;
    int c=0;
    int ans=0;
    while(true) {
        if(c!=x && c!=y && s[i]=='1')
            ans++;

        if((c==x || c==y) && s[i]=='0')
            ans++;

        c++;

        if(c>=x && c>=y)
            break;
        i--;
    }

    cout<<ans<<endl;

}

