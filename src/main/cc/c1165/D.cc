/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

void solve() {
    int n;
    cin>>n;
    vector<int> d;
    fori(n) {
        int x;
        cin>>x;
        d.push_back(x);
    }
    sort(d.begin(), d.end());

    // what if product is to big???
    /* We need to check if all divisors of all divisors are in d.
    */

    set<int> dset;
    fori(n)
        dset.insert(d[i]);

    ll ans=1LL*d[0]*d[d.size()-1];
    // so, check if a divisor is missing
    for(int j=2; 1LL*j*j<=ans; j++)  {
        if(ans%j==0) {
            if(!binary_search(d.begin(), d.end(), j)) {
                cout<<-1<<endl;
                return;
            }
            if(j!=ans/j) {
                if(!binary_search(d.begin(), d.end(), ans/j)) {
                    cout<<-1<<endl;
                    return;
                }
            }
            dset.erase(j);
            dset.erase(ans/j);
        }
    }


    if(dset.size()>0)
        ans=-1;

    cout<<ans<<endl;


}

int main() {
    int t;
    cin>>t;
    while(t--)
        solve();

}

