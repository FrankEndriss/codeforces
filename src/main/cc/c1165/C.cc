/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n;
    cin>>n;
string s;
    cin>>s;

    int ans=0;
    string ans2;
    bool odd=true;
    for(int i=0; i<n; i++) {
        if(odd && i<n-1 && s[i]==s[i+1])
            ans++;
        else {
            odd=!odd;
            ans2+=s[i];
        }
    }
    if((s.size()-ans)%2==1) {
        ans++;
        if(ans2.size()>0)
            ans2=ans2.substr(0, ans2.size()-1);
        else
            ans2="";
    }

    cout<<ans<<endl;
    cout<<ans2<<endl;

}

