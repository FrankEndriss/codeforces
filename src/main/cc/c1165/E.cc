/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

#define mod 998244353

int mul(const int v1, const int v2) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int inv(const int x) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}

int pl(int v1, int v2) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}


void solve() {
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++)
            for(int k=i; k<=j; k++)
                ans+=a[k]*b[k];

}
int main() {
    int n;
    cin>>n;
    vector<int> a(n);
    vector<int> b(n);

    fori(n)
        cin>>a[i];
    fori(n)
        cin>>b[i];

    sort(b.begin(), b.end());
    reverse(b.begin(), b.end());
    vector<int> c(n);
    for(int i=n-1; i>=0; i--) {
        c[i]=b[i];
        if(i<n-1)
            c[i]+=c[i+1];
    }
    //reverse(c.begin(), c.end());

    int ans=0;
    for(int i=0; i<n; i++) {
cout<<"mul a[i]="<<a[i]<<" c[i]="<<c[i]<<endl;
        int m=mul(a[i], c[i]);
        ans=pl(ans, m);
    }

    cout<<ans<<endl;
}

