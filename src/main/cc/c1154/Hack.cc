
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

int n;
set <int> a;
int main() {
    cin >> n;
    for(int i = 1; i <= n; ++i) {
        int k;
        cin >> k;
        a.insert(k);
    }
    if(a.size() == 1) {
        cout << 0;
        return 0;
    }
    int num1 = 0, num2 = 0, num3 = 0;
    if(a.size() == 2) {
        num1 = *a.begin();
        a.erase(*a.begin());
        num2 = *a.begin();
        if((num2 - num1) % 2 != 0) {
            cout << num2 - num1;
        }
        else cout << (num2 - num1) / 2;
        return 0;
    }
    if(a.size() == 3)  {
        num1 = *a.begin();
        a.erase(*a.begin());
        num2 = *a.begin();
        a.erase(*a.begin());
        num3 = *a.begin();
        if(num2 - num1 == num3 - num2) {
            cout << num2 - num1;
        }
        else {
            cout << -1;
        }
        return 0;
    }
    cout << -1;
}
