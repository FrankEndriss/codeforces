
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define N 1000001
ll a[N];

bool acmp(const int id1, const int id2) {
    return a[id1]<a[id2];
}

int main() {
    int n;
    cin>>n;
    int id[n];
    for(int i=0; i<n; i++) {
        cin>>a[i];
        id[i]=i;
    }
    sort(id, id+n, acmp);

    ll ans=10000000000000001;
    int ansI=-1;
    int ansJ=-1;
    for(int i=0; i<n-1; i++)  {
        if(ans<a[id[i]])
            break;
        for(int j=i+1; j<n; j++) {
            if(ans<=a[id[j]])
                break;

            ll val=(a[id[i]]/__gcd(a[id[i]], a[id[j]]))*a[id[j]];
            if(val<ans) {
                ans=val;
                ansI=id[i];
                ansJ=id[j];
            }
        }
    }
    ansI++;
    ansJ++;

    cout<< min(ansI, ansJ)<< " "<< max(ansI, ansJ)<<endl;
}

