
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int main() {
	ll a, b, c;
	cin>>a>>b>>c;

// monday to sunday
int fish[]={ 1, 0, 0, 1, 0, 0, 1 };
int rabb[]={ 0, 1, 0, 0, 0, 1, 0 };
int chic[]={ 0, 0, 1, 0, 1, 0, 0 };

int ans[7];
	for(int i=0; i<7; i++) {
		ll dFish=7*(a/3);
		int rFish=a%3;
		int j=i;
		while(true) {
			rFish-=fish[j%7];
			if(rFish>=0)
				dFish++;
			else 
				break;
			j++;
		}

		ll dRabb=7*(b/2);
		int rRabb=b%2;
		j=i;
		while(true) {
			rRabb-=rabb[j%7];
			if(rRabb>=0)
				dRabb++;
			else 
				break;
			j++;
		}
		
		ll dChic=7*(c/2);
		int rChic=c%2;
		j=i;
		while(true) {
			rChic-=chic[j%7];
			if(rChic>=0)
				dChic++;
			else	
				break;
			j++;
		}

		ans[i]=min(dFish, min(dRabb, dChic));
	}

	int an=-1;
	for(int i=0; i<7;i++) {
		if(ans[i]>an)
			an=ans[i];
	}
	cout<<an<<endl;
}

