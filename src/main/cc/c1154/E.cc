/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

#define N 200001 

int main() {
int n, k;
	cin>>n>>k;
        set<pair<int, int>> unchoosen;
        vector<int> a(n);
        vector<int> left(n);
        vector<int> right(n);

	for(int i=0; i<n; i++) {
		cin>>a[i];
                unchoosen.insert({ -a[i], i});
                left[i]=i-1;
                right[i]=i+1;
	}

        vector<int> ans(n);
        int team=1;
        while(unchoosen.size()>0) {
            auto lead=unchoosen.begin();
            int leadidx=lead->second;

            vector<int> rem;

            int idx=leadidx;
            fori(k+1) {
                rem.push_back(idx);
                idx=left[idx];
                if(idx<0)
                    break;
            }

            idx=right[leadidx];
            fori(k) {
                if(idx>=n)
                    break;
                rem.push_back(idx);
                idx=right[idx];
            }

            for(int r : rem) {
                unchoosen.erase({ -a[r], r });
                int nodeleft=left[r];
                int noderight=right[r];
                if(noderight<n)
                    left[noderight]=left[r];
                if(nodeleft>=0)
                    right[nodeleft]=right[r];

                ans[r]=team;
//cout<<"choosen idx="<<r<<" team="<<team<<endl;
            }

            if(team==1)
                team=2;
            else
                team=1;
        }

        fori(n)
            cout<<ans[i];
        cout<<endl;

}

