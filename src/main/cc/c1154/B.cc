
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define N 101
int a[N];
int n;
int main() {
    cin>>n;
    set<int> s;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        s.insert(a[i]);
    }
    if(s.size()>3) {
        cout<<"-1"<<endl;
        return 0;
    }

    vector<int> v;
    for(int i : s)
        v.push_back(i);
    sort(v.begin(), v.end());
    if(v.size()==3) {
        if(v[1]-v[0]!=v[2]-v[1]) {
            cout<<"-1"<<endl;
            return 0;
        } else
            cout<<v[1]-v[0]<<endl;
    } else if(v.size()==2) {
        int ans=v[1]-v[0];
        if(ans%2==0)
            ans/=2;
        cout<<ans<<endl;
    } else
        cout<<0<<endl;

}

