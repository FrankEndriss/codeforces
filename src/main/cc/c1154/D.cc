/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int s[200001];

int main() {
int n, b, a;
	cin>>n>>b>>a;
	for(int i=0; i<n; i++)
		cin>>s[i];

	const int aC=a;	// Capacity
	// simple strategy: Allways use the a, but load a if possible

	int ans=0;
	for(int i=0; i<n && (a>0 || b>0); i++) {
		if(s[i] && b>0 && a<aC) { // with sun
			a++;
			b--;
		} else if(a>0)
			a--;
		else
			b--;
				
		ans++;
	}
	cout<<ans;
}

