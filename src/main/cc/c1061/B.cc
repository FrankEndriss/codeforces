/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n, m;
    cin>>n>>m;
    vector<int> a(n);
    fori(n)
        cin>>a[i];
    sort(a.begin(), a.end());


    ll ans=0;
    int uncov=a[n-1];   // uncovered rows so far
    for(int i=n-1; i>=0; i--) {
        int cov;    // number of blocks remaining in col[i], ie covering rows
        if(i>0)
            cov=max(1, uncov-a[i-1]);
        else
            cov=max(1, uncov);
        ans+=a[i]-cov;
        uncov-=cov;
    }

    cout<<ans<<endl;

}

