/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

void divs(int a, vector<int> &ret) {
    ret.push_back(1);
    if(a>1)
        ret.push_back(a);

    for(int i=2; i*i<=a; i++) {
        if(a%i==0) {
            ret.push_back(i);
            int i2=a/i;
            if(i2!=i)
                ret.push_back(i2);
        }
    }
    sort(ret.begin(), ret.end());
    reverse(ret.begin(), ret.end());
}

int main() {
int n;
    cin>>n;

/*
    vector<ll> dp(n, n); 
 dp[x][y]=number of possibilities of len i at position j
        if(a[i]%j==0)
            dp[i][j]=dp[i-1][j] + dp[i-1][j-1];
        else
            dp[i][j]=dp[i-1][j];
*/
    vector<ll> dp(1000001);
    dp[0]=1;

    const int MOD=1000000007;
    for(int i=1; i<=n; i++) {
        int a;
        cin>>a;
        vector<int> d;
        divs(a, d);

        for(auto j : d) {
            dp[j]+=dp[j-1];
            dp[j]%=MOD;
        }
    }
    ll ans=0;
    for(int i=1; i<=n; i++)
        ans+=dp[i];
    ans%=MOD;

    cout<<ans<<endl;
    return 0;
}

