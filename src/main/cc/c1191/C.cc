/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n, m, k;
    cin>>n>>m>>k;
    vll p(m);
    fori(m)
        cin>>p[i];

    int ans=0;
    int idx=0;
    while(idx<m) {
        ans++;
        ll page=(p[idx]-1-idx)/k;
        int lidx=idx;
        do {
            idx++;
        } while(idx<m && (p[idx]-1-lidx)/k==page);
    }

    cout<<ans<<endl;

}

