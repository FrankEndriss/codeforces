/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    string s1, s2, s3;
    cin>>s1>>s2>>s3;

    int i1, i2, i3;
    i1=s1[0]-'0';
    i2=s2[0]-'0';
    i3=s3[0]-'0';

/* koutsu or two of a koutsu */
    if(s1==s2 && s2==s3) {
        cout<<0<<endl;
        return 0;
    } else if(s1==s2 || s2==s3 || s1==s3) {
        cout<<1<<endl;
        return 0;
    }

/* full shuntsu */
    if(s1[1]==s2[1] && s2[1]==s3[1]) {
        vi v({ i1, i2, i3});
        sort(v.begin(), v.end());
        if(v[0]==v[1]-1 && v[1]==v[2]-1) {
            cout<<0<<endl;
            return 0;
        }
    }

/* two of a shuntsu */
    if(s1[1]==s2[1] && (abs(i1-i2)==1 || abs(i1-i2)==2)) {
        cout<<1<<endl;
        return 0;
    }
    if(s2[1]==s3[1] && (abs(i2-i3)==1 || abs(i2-i3)==2)) {
        cout<<1<<endl;
        return 0;
    }
    if(s1[1]==s3[1] && (abs(i1-i3)==1 || abs(i1-i3)==2)) {
        cout<<1<<endl;
        return 0;
    }

    cout<<2<<endl;
    return 0;

}

