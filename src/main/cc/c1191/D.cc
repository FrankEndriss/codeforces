/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vi a(n);
    map<int, int> ms;
    ll sum=0;
    fori(n) {
        cin>>a[i];
        ms[a[i]]++;
        sum+=a[i];
    }

    if(sum==0) {
        cout<<"cslnb"<<endl;
        return 0;
    }
    if(n==1) {
        if(sum%2==1) {
            cout<<"sjfnb"<<endl;
        } else {
            cout<<"cslnb"<<endl;
        }
        return 0;
    }

    /* check if first move leads to loose the game */
    int two=0;
    int theTwo=-1;
    for(auto ent : ms) {
        if(ent.second>2) {
            cout<<"cslnb"<<endl;
            return 0;
        }

        if(ent.second==2) {
            two++;
            theTwo=ent.first;
        }

        if(two>1) {
            cout<<"cslnb"<<endl;
            return 0;
        }
    }
    if(two==1) {
        if(ms[theTwo-1]>0) {
            cout<<"cslnb"<<endl;
            return 0;
        }
    }
    if(ms[0]>1) {
        cout<<"cslnb"<<endl;
        return 0;
    }

    /* count possible moves */
    ll lpos=((n-1)*n)/2;     // num stones in loosing position
    assert(sum>=lpos);
    if((sum-lpos)%2==1) {
        cout<<"sjfnb"<<endl;
    } else {
        cout<<"cslnb"<<endl;
    }

    return 0;
}

