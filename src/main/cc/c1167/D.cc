/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n;
    cin>>n;
    string rbs;
    cin>>rbs;

    int ropen=0;
    int bopen=0;
    for(auto c : rbs) {
        if(c=='(') {
            if(ropen<=bopen) {
                ropen++;
                cout<<0;
            } else {
                bopen++;
                cout<<1;
            }
        } else {
            if(ropen>=bopen) {
                ropen--;
                cout<<0;
            } else {
                bopen--;
                cout<<1;
            }
        }
    }
    cout<<endl;
}

