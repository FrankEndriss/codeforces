/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    vector<int> n{ 4, 8, 15, 16, 23, 42 };

    int a, b, c, d;
    cout<<"? 1 2"<<endl;
    cin>>a;
    cout<<"? 2 3"<<endl;
    cin>>b;
    cout<<"? 3 4"<<endl;
    cin>>c;
    cout<<"? 4 5"<<endl;
    cin>>d;

    vector<int> ans(6);
    fori(6)
        ans[i]=i;

    do {
        if( n[ans[0]]*n[ans[1]]==a &&
            n[ans[1]]*n[ans[2]]==b &&
            n[ans[2]]*n[ans[3]]==c &&
            n[ans[3]]*n[ans[4]]==d) {
                break;
        }
    }while(next_permutation(ans.begin(), ans.end()));

    cout<<"! ";
    fori(6)
        cout<<n[ans[i]]<<" ";
    cout<<endl;
}

