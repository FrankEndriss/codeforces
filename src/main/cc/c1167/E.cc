/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, x;
    cin>>n>>x;
    vector<int> minPos(x, -1);  // leftmost position of a 
    vector<int> maxPos(x, -1);  // rightmost position of a
    vector<int> sumMax(x, 0);  // rightmost position of any t<=a

/* If any x+k is left of x we need to remove the range (x,x+k) to make the result sorted. Else we can remove none, any or all.
 * So, we need to find the min and max positions of all x.
 * Then find the range we need to remove in all cases.
 * Then calculate the number of remaining pairs.
 *
 * ...Waaaah..its more complicated:
 * A "1" left of a "4" can be made sorted by removing the 1 _or_ the 4 (or both).
 * So, for any number we need to find the _range_ of smaller ones right of it.
 * Then, we can make the result sorted by removing the number, or the smaller range.
 * We need to combine these OR-pairs ... somehow.
 */
    fori(n) {
        int a;
        cin>>a;
        
        if(minPos[a]==-1)
            minPos[a]=i;
        maxPos[a]=i;
    }
    for(int i=1; i<x; i++)
        sumMax[i]=max(sumMax[i-1], maxPos[i]);

    int minRem=n+2;
    int maxRem=-1;
    for(int i=1; i<x; i++) {
        if(minPos[i]<sumMax[i]) {
            minRem=min(minRem, i);
            maxRem=max(maxRem, i);
// TODO find the min and max of all n<i right of minPos[i]
        }
    }

    cout<<"minRem="<<minRem<<" maxRem"<<maxRem<<endl;

/* Now do the combinatorics. */
    int ans=-1

/* Still missing: case 2,3,1, pair(2,3); */
    cout<<ans<<endl;

    

}

