
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

int A[200001];
int n, c2;

int main() {
	cin>>n;
	for(int i=0; i<n; i++) {
		int x;
		cin>>x;
		A[x]++;
		if(A[x]==2)
			c2++;
		else if(A[x]>2) {
			cout<<"NO"<<endl;
			return 0;
		}
	}
	cout<<"YES"<<endl;
	cout<<c2<<endl;
	for(int i=0; i<200001; i++) {
		if(A[i]==2)
			cout<<i<<" ";
	}
	cout<<endl;
	cout<<n-c2<<endl;
	for(int i=200000; i>=0; i--) {
		if(A[i]>0)
			cout<<i<<" ";
	}
	cout<<endl;
}

