
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

vector<int> even;
vector<int> odd;
int main() {
int n, x;
	cin>>n;
	for(int i=0; i<n; i++)  {
		cin>>x;
		if(x&1)
			odd.push_back(x);
		else
			even.push_back(x);
	}
	sort(odd.begin(), odd.end());
	sort(even.begin(), even.end());

	int oddRemain=odd.size()-even.size()-1;
	int evenRemain=even.size()-odd.size()-1;

	int ans=-1;
		int oddSum=0;
		for(int i=0; i<oddRemain; i++)
			oddSum+=odd[i];
		int evenSum=0; 
		for(int i=0; i<evenRemain; i++)
			evenSum+=even[i];
		ans=max(oddSum, evenSum);
	cout<<ans<<endl;
	
}

