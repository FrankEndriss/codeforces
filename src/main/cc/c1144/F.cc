
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

vector<int> tree[200001];
int color[200001];
#define BLACK 1
#define WHITE 2

pair<int, int> edges[200001];

bool col(int v, int c) {
	if(color[v]==BLACK || color[v]==WHITE)
		return color[v]==c;
	color[v]=c;
	int nC=0;
	if(c==WHITE)
		nC=BLACK;
	else
		nC=WHITE;
	for(uint i=0; i<tree[v].size(); i++)
		if(!col(tree[v][i], nC))
			return false;

	return true;
}

int main() {
int n, m;
	cin>>n>>m;
	for(int i=0; i<m; i++) {
		int u, v;
		cin>>u>>v;
		edges[i]=make_pair(u, v);
		tree[u].push_back(v);
		tree[v].push_back(u);
	}
	if(!col(edges[0].first, BLACK)) {
		cout<<"NO"<<endl;
		return 0;
	}
	cout<<"YES"<<endl;
	for(int i=0; i<m; i++)
		cout<<color[edges[i].first]-1;
	cout<<endl;
}

