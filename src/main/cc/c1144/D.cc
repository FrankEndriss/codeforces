

#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
typedef unsigned int uint;

#define MAX 200001
int A[MAX];
int f[MAX];

int main() {
int n;
	cin>>n;
	for(int i=1; i<=n; i++) {
		cin>>A[i];
		f[A[i]]++;
	}
	int maxFreq=0;
	int mFvalue=-1;
	for(int i=0; i<MAX; i++)
		if(f[i]>maxFreq) {
			maxFreq=f[i];
			mFvalue=i;
		}
	// find idx of mFvalue
	int mFidx=-1;
	for(int i=1; i<MAX; i++) {
		if(A[i]==mFvalue) {
			mFidx=i;
			break;
		}
	}

	cout<<n-maxFreq<<endl;
	for(int i=mFidx-1; i>0; i--)  {
		if(A[i]<mFvalue)
			cout<<"1 "<<i<<" "<<i+1<<endl;
		else if(A[i]>mFvalue)
			cout<<"2 "<<i<<" "<<i+1<<endl;
	}
	for(int i=mFidx; i<n; i++)  {
		if(A[i+1]<mFvalue)
			cout<<"1 "<<i+1<<" "<<i<<endl;
		else if(A[i+1]>mFvalue)
			cout<<"2 "<<i+1<<" "<<i<<endl;
	}
}

