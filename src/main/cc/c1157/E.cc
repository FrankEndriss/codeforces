/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n;
    cin>>n;
    vector<int> a(n);
    fori(n)
        cin>>a[i];

    multiset<int> b;
    fori(n) {
        int x;
        cin>>x;
        b.insert(x);
    }

/* (a[i]+b[i])%n is min if a[i]+b[i]==0 or a[i]+b[i]==n;
 * a[i]=0 -> 0, 1, 2
 * a[i]=1 -> n-1, 0, 1, 2,...
 * a[i]=2 -> n-2, n-1, 0, 1, 2...
 * How to search b[i] efficient? multiset!
 */
    vector<int> ans(n);
    fori(n) {
        /* find best match for a[i] */
        int match=a[i]==0?0:n-a[i];
        auto it=b.lower_bound(match);
        if(it==b.end())
            it=b.begin();

        ans[i]=*it;
        b.erase(it);
    }

    fori(n)
        cout<<(a[i]+ans[i])%n<<" ";
    cout<<endl;
}

