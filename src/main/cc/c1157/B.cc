
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<n; i++)
#define fauto(c, vec) for(auto c : vec)

int main() {
int n;
    cin>>n;
    string a;
    vector<int> b(10);
    cin>>a;

    fori(9)
        cin>>b[i+1];

    bool first=true;
    fori(n) {
        int z=a[i]-'0';
        if(first && b[z]>z) {
            first=false;
            while(b[z]>=z ) {
                cout<<b[z];
                i++;
                if(i==n)
                    break;
                z=a[i]-'0';
            }
        }
        if(i<n)
            cout<<a[i];
    }
    cout<<endl;

}

