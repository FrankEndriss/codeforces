/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<n; i++)
#define fauto(c, vec) for(auto c : vec)

int main() {
int n;
    cin>>n;
    vector<int> a(n);
    fori(n)
        cin>>a[i];

    ostringstream oss;
    int l=0, r=n-1;
    int last=min(a[0], a[n-1])-1;
    int ans=0;
    fori(n) {
        // use the smallest one of the available
        if(a[l]==a[r] && a[l]>last) {
            /* check which end is longer increasing. */
            int llen=0;
            int lidx=l;
            int llast=last;
            while(lidx<r && a[lidx]>llast) {
                llen++;
                llast=a[lidx];
                lidx++;
            }
            int rlen=0;
            int ridx=r;
            int rlast=last;
            while(ridx>l && a[ridx]>rlast) {
                rlen++;
                rlast=a[ridx];
                ridx--;
            }
            if(llen>rlen) {
                oss<<"L";
                last=a[l];
                l++;
            } else {
                oss<<"R";
                last=a[r];
                r--;
            }
            ans++;

        } else if(a[l]>last && (a[r]<=last || a[l]<a[r])) {
            oss<<"L";
            last=a[l];
            l++;
            ans++;
        } else if(a[r]>last) {
            oss<<"R";
            last=a[r];
            r--;
            ans++;
        } 
    }
    cout<<ans<<endl;
    cout<<oss.str()<<endl;
}

