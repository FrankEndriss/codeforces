
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define fauto(c, vec) for(auto c : vec)

//#define DEBUG

int yes(vector<ll> &v) {
    cout<<"YES"<<endl;
    fori(v.size()) {
        cout<<v[i]<<" ";
    }
    cout<<endl;
    return 0;
}

void dump(vector<ll> &v) {
    cout<<"log v=";
    fori(v.size())
        cout<<v[i]<<" ";
    cout<<endl;
}

int main() {
ll n, k;
    cin>>n>>k;
    
    // min x+1
    // max x*2
    // sum(a[i])==n
    // a[i]>0
    
    /** biggest possible sum when starting at i must be greater or eq to n. */
    ll start=1;
    while(k<30 && start<<k <= n)
        start<<=1;

    /* start with smallest possible. */
    vector<ll> a(k);
    ll s=0;
    for(ll i=0; i<k; i++) {
        a[i]=start+i;
        s+=a[i];
    }
    if(s>n) {
        cout<<"NO"<<endl;
        return 0;
    }

    ll diff=n-s;
    ll lastDiff=diff;
#ifdef DEBUG
dump(a);
#endif
    while(diff) {
#ifdef DEBUG
cout<<"while diff="<<diff<<endl;
#endif
        if(k==2 && a[0]==1 && diff>1) {
            a[1]++;
            diff--;
            a[0]++;
            diff--;
            continue;
        }

        for(ll i=k-1; i>0; i--) {
            ll inc=min(2*a[i-1]-a[i], diff);
            a[i]+=inc;
#ifdef DEBUG
dump(a);
#endif
            diff-=inc;
            if(diff==0)
                return yes(a);
        }
        ll inc=min(diff, a[1]-a[0]-1);
        a[0]+=inc;
#ifdef DEBUG
dump(a);
#endif
        diff-=inc;
        if(diff==0)
                return yes(a);
        if(diff==lastDiff) {
            cout<<"NO"<<endl;
            return 0;
        }
        lastDiff=diff;
    }
    return yes(a);
}

