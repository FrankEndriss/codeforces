/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n, k;
    cin>>n>>k;

    int d = 2*k + 1;
    int ans=n/d;
    if(n%d>0)
        ans++;

    int r= n%d;
    if(r==0)
        r=2*k;

    cout << ans << endl;
    for(int i=1+r/2; i<=n; i+=d)
        cout << i <<" ";
    cout << endl;
    return 0;
}

