
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int main() {
string t;
    cin>>t;
string t2;
    for(auto c : t) {
        if(c!='a')
            t2+=c;
    }

    /* t2 is t1 without a
    */
    if(t2.size()%2==1) {
        cout<<":("<<endl;
        return 0;
    }

    bool t2Ok= t2.substr(0, t2.size()/2) == t2.substr(t2.size()/2, t2.size()/2);
    bool tOk= t.substr(t.size()-t2.size()/2, t2.size()/2) == t2.substr(0, t2.size()/2);
    string s=t2.substr(0, t2.size()/2);
    string ans;
    if( t2Ok && tOk)
        ans=t.substr(0, t.size()-s.size());
    else 
        ans=":(";

    cout<<ans<<endl;
}

