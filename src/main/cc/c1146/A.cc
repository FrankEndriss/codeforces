
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int main() {
string s;
    cin>>s;
    uint c=0;
    for(uint i=0; i<s.length(); i++)
        if(s[i]=='a')
            c++;
    uint ans=min(c*2-1, (uint)(s.length()));
    cout<<ans<<endl;

}

