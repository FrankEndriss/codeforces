/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int main() {
int n, q;
    cin>>n>>q;
    vector<int> a(n);
    vector<char> s(q);
    vector<int> x(q);

    for(auto &c : a)
        cin>>c;

    for(int i=0; i<q; i++) {
        string str;
        cin>>str>>x[i];
        s[i]=str[0];
    }
    
    for(int i=0; i<n; i++) {
        if(a[i]==0)
            continue;
        
        int la=abs(a[i]);
        int j;
        int flip=0;
        for(j=q-1; j>=0; j--) {
            if(abs(x[j])<la) {
                if(s[j]=='>') {
                    a[i]=-abs(a[i]);
                } else {
                    a[i]=abs(a[i]);
                }
//cout<<"set a[i] at j="<<j<<" to a[i]="<<a[i]<<endl;
                j++;
                break;
            } else if(abs(x[j])==la) {
                if(s[j]=='<' && x[j]>0)  {
                    a[i]=abs(a[i]);
                    j++;
                    break;
                } else if(s[j]=='>' && x[j]<0) {
                    a[i]=-abs(a[i]);
                    j++;
                    break;
                }
            } else {
                if(s[j]=='<' && x[j]>0)
                    flip++;
                else if(s[j]=='>' && x[j]<0)
                    flip++;
            }
        }
//cout<<"i="<<i<<" j="<<j<<" flip="<<flip<<endl;
        if(flip%2==1)
            a[i]=-a[i];

/*
        for(; j<q; j++)  {
            if( ((s[j]=='<') && (a[i]<x[j])) || ((s[j]=='>') && (a[i]>x[j]) )) {
//cout<<"flip a[i], i="<<i<< " a[i]="<<a[i]<<endl;
                a[i]=-a[i];
            }
        }
*/
    }

    for(int i=0; i<n; i++) 
        cout<<a[i]<<" ";
    cout<<endl;


}

