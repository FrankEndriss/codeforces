/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vvi adj(n);
	for (int i = 0; i < n - 1; i++) {
		cini(u);
		cini(v);
		u--;
		v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	cout << "parsed" << endl;

	vi p(n);
	vi lvl(n);
	int mxlvlnode = -1;
	int mxlvl = -1;
	function<int(int, int)> depth = [&](int node, int parent) {
		p[node] = parent;
		int plevel = 0;
		if (parent >= 0)
			plevel = lvl[parent];
		lvl[node] = plevel + 1;
		if (lvl[node] > mxlvl) {
			mxlvl = lvl[node];
			mxlvlnode = node;
		}

		int ans = 0;
		for (int chl : adj[node]) {
			if (chl == parent)
				continue;
			ans = max(ans, depth(chl, node));
		}

		return ans + 1;
	};

	int root;
	for (root = 0; root < n; root++) {
		if (adj[root].size() > 1)
			break;
	}

	cout << "found root=" << root << endl;

	vector<pii> d0;
	for (int i = 0; i < adj[root].size(); i++) {
		d0.emplace_back(depth(adj[root][i], root), i);
	}

	sort(d0.begin(), d0.end(), greater<pii>());

	cout << "deepest subtrees are: " << adj[root][d0[0].second] << " " << adj[root][d0[1].second] << endl;

	mxlvl = -1;
	mxlvlnode = -1;
	depth(adj[root][d0[0].second], root);
	int ans1 = mxlvlnode;
	int ansCnt = lvl[ans1];

	cout << "subnode1=" << ans1 << endl;

	mxlvl = -1;
	mxlvlnode = -1;
	depth(adj[root][d0[1].second], root);
	int ans2 = mxlvlnode;
	ansCnt += lvl[ans2];
	ansCnt++;

	cout << "subnode2=" << ans2 << endl;
	cout << "ansCnt=" << ansCnt << endl;

	/* now find deepest path on all nodes on way from ans1 to ans2 */
	int maxdepth = -1;
	int maxdepthnode = -1;

	int node = ans1;
	vi pp = p;
	while (node != root) {
		int nnode = pp[node];
		for (int chl : adj[nnode]) {
			if (chl != node && chl != pp[nnode] && chl != root) {
				lvl[chl] = 0;
				mxlvl = -1;
				mxlvlnode = -1;
				int ldepth = depth(chl, nnode);
				if (ldepth > maxdepth) {
					maxdepth = ldepth;
					maxdepthnode = mxlvlnode;
				}
			}
		}
		node = nnode;
	}

	node = ans2;
	while (node != root) {
		int nnode = pp[node];
		for (int chl : adj[nnode]) {
			if (chl != node && chl != pp[nnode] && chl != root) {
				lvl[chl] = 0;
				mxlvl = -1;
				mxlvlnode = -1;
				int ldepth = depth(chl, nnode);
				if (ldepth > maxdepth) {
					maxdepth = ldepth;
					maxdepthnode = mxlvlnode;
				}
			}
		}
		node = nnode;
	}
	ansCnt += maxdepth;
	cout << ansCnt << endl;
	cout << ans1 << " " << ans2 << " " << maxdepthnode << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

