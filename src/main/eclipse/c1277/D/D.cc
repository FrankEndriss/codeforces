/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vvi cnt(2, vi(2));
	vvvi idx(2, vvi(2));
	vvvi rev(2, vvi(2));

	set<string> str;
	vs strlist;
	for (int i = 0; i < n; i++) {
		cins(s);
		str.insert(s);
		strlist.push_back(s);
		cnt[s[0] == '1'][s.back() == '1']++;
		idx[s[0] == '1'][s.back() == '1'].push_back(i + 1);
	}

	for (int i = 0; i < n; i++) {
		string t = strlist[i];
		reverse(t.begin(), t.end());
		if (str.count(t) == 0)
			rev[strlist[i][0] == '1'][strlist[i].back() == '1'].push_back(i + 1);
	}

	/* we can concat all(cnt[0][0]), cnt[0][1],cnt[1][0],...,cnt[0][1], all(cnt[1][1])
	 * How to find min changes?
	 * No need to change 00 or 11, they simply get attached whereever possible.
	 * 01 and 10. The count must not differ by more than one, so we need to convert
	 * (cnt[0][1]+cnt[1][0])/2 - min(cnt[0][1], cnt[1][0])
	 * Corner case: If no 01 or 10 exist, but 00 _and_ 11, there is no solution.
	 */

	if (cnt[0][1] + cnt[1][0] == 0 && cnt[0][0] > 0 && cnt[1][1] > 0) {
		cout << -1 << endl;
		return;
	}

	int ans = (cnt[0][1] + cnt[1][0]) / 2 - min(cnt[0][1], cnt[1][0]);

	if (cnt[0][1] < cnt[1][0] && ans > rev[1][0].size()) {
		cout << -1 << endl;
	}
	if (cnt[0][1] > cnt[1][0] && ans > rev[0][1].size()) {
		cout << -1 << endl;
	}

	cout << ans << endl;
	for (int i = 0; i < ans; i++) {
		if (cnt[0][1] < cnt[1][0])
			cout << rev[1][0][i] << " ";
		else
			cout << rev[0][1][i] << " ";
	}
	cout << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

