/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(m);
	set<pii> occ;
	set<int> trees;
	for (int i = 0; i < n; i++) {
		cini(x);
		trees.insert(x);
		occ.insert( { x, x });
	}

	auto it = occ.begin();
	while (it != occ.end()) {
		pii cur = *it;
		it++;

		if (it != occ.end()) {
			pii next = *it;
			if (cur.second == next.first - 1) {
				occ.erase(cur);
				occ.erase(next);
				occ.insert( { cur.first, next.second });
				it = occ.find( { cur.first, next.second });
			}
		}
	}

	/* nearest distance... sic :/ */
	function<int(int)> ndist = [&](int x) {
		auto it = trees.lower_bound(x);
		if (it == trees.end()) {
			it--;
			return abs(x - *it);
		} else {
			int x1 = *it;
			if (it != trees.begin()) {
				it--;
				int x2 = *it;
				x1 = min(abs(x - x1), abs(x - x2));
			} else
				x1 = abs(x - x1);
			return x1;
		}
	};

	it = occ.begin();
	bool first = true;
	ll ans = 0;
	while (m) {
		pii cur = *it;
		it++;

		if (first) {
			occ.erase(cur);
			cur.first--;
			occ.insert(cur);
			it = occ.find(cur);
			it++;
			m--;
			ans += ndist(cur.first);
			first = false;
		}

		if (it == occ.end()) {
			if (m) {
				occ.erase(cur);
				cur.second++;
				occ.insert(cur);
				m--;
				ans += ndist(cur.second);
			}
			it = occ.begin();
			first = true;
		} else if (m) {
			pii next = *it;
			if (cur.second + 1 < next.first && m) {
				occ.erase(cur);
				cur.second++;
				occ.insert(cur);
				m--;
				ans += ndist(cur.second);
				if (next.first - 1 > cur.second && m) {
					occ.erase(next);
					next.first--;
					occ.insert(next);
					m--;
					ans += ndist(next.first);
					it = occ.find(next);
				}
				if (cur.second == next.first - 1) {
					occ.erase(cur);
					occ.erase(next);
					occ.insert( { cur.first, next.second });
					it = occ.find( { cur.first, next.second });
				}
			}
		}
	}

	cout << ans << endl;
	for (pii p : occ) {
		for (int i = p.first; i <= p.second; i++) {
			if (trees.count(i) == 0)
				cout << i << " ";
		}
	}
	cout << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

