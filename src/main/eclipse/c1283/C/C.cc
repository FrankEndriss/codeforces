/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vi f(n);
	set<int> free;
	for (int i = 0; i < n; i++) {
		cin >> f[i];
		free.insert(i + 1);
	}
	for (int i = 0; i < n; i++) {
		if (f[i] != 0)
			free.erase(f[i]);
	}
	//cout << "parsed" << endl;

	for (int i = 0; i < n && free.size() > 3; i++) {
		if (f[i] == 0) {
			auto it = free.begin();
			if (*it == i + 1)
				it++;

			f[i] = *it;
			free.erase(it);
		}
	}

	//cout << "free.size=" << free.size() << endl;

	vi idx1(free.size());
	copy(free.begin(), free.end(), idx1.begin());

	//cout << "copied" << endl;

	vi pidx(free.size());
	for (size_t i = 0; i < free.size(); i++)
		pidx[i] = i;

	do {
		//cout << "next perm" << endl;
		bool ok = true;
		int idx = 0;
		for (int i = 0; i < n; i++) {
			if (f[i] == 0) {
				if (idx1[pidx[idx]] == i + 1) {
					ok = false;
					break;
				}
				idx++;
			}
		}
		if (ok) {
			idx = 0;
			for (int i = 0; i < n; i++) {
				if (f[i] == 0) {
					//cout << "idx=" << idx << endl;
					f[i] = idx1[pidx[idx]];
					idx++;
				}
			}
			break;
		}
	} while (next_permutation(pidx.begin(), pidx.end()));

	for (int a : f)
		cout << a << " ";
	cout << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

