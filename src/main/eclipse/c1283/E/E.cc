/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vi x(n + 2);
	for (int i = 1; i <= n; i++) {
		cini(aux);
		x[aux]++;
	}

	int micnt = 0;
	for (int i = 1; i <= n; i++) {
		// case 101
		// case 110
		// case 111
		if (x[i] > 0) {
			micnt++;
			i += 2;
		}
	}

	int segl = 0;
	int segr = 0;
	while (segl < n + 2) {
		int segcnt = 0;
		while (segl <= n && x[segl] == 0)
			segl++;
		segr = segl;
		while (segr <= n + 1 && x[segr] > 0) {
			segcnt += x[segr];
			segr++;
		}
		segcnt -= (segr - segl);
		if (x[segl - 1] == 0 && segcnt) {
			x[segl - 1]++;
			segcnt--;
		}
		if (segr < n + 2 && segcnt) {
			x[segr]++;
		}
		segl = segr + 1;
	}
	int macnt = 0;
	for (int i : x)
		if (i)
			macnt++;

	cout << micnt << " " << macnt << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

