/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(s);
	vi t;
	t.push_back(-s - 1);
	for (int i = 0; i < n; i++) {
		cini(h);
		cini(m);
		t.push_back(h * 60 + m);
	}
	sort(t.begin(), t.end());

	for (int i = 1; i < n + 1; i++) {
		if (t[i] - t[i - 1] > 2 * s + 1) {
			int t0 = t[i - 1] + s + 1;
			cout << t0 / 60 << " " << t0 % 60 << endl;
			return;
		}
	}
	int t0 = t.back() + s + 1;
	cout << t0 / 60 << " " << t0 % 60 << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

