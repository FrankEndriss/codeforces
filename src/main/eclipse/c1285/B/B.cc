/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cinall(a, n);
	ll asum = 0;
	ll asum1 = 0;
	ll lsum1 = 0;
	ll asum2 = 0;
	ll lsum2 = 0;
	for (int i = 0; i < n; i++) {
		asum += a[i];
		if (i > 0) {
			if (lsum1 + a[i] > 0)
				lsum1 += a[i];
			else
				lsum1 = 0;
			asum1 = max(asum1, lsum1);
		}
		if (i + 1 < n) {
			if (lsum2 + a[i] > 0)
				lsum2 += a[i];
			else
				lsum2 = 0;
			asum2 = max(asum2, lsum2);
		}
	}

	if (asum > asum1 && asum > asum2) {
		cout << "YES" << endl;
	} else {
		cout << "NO" << endl;
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

