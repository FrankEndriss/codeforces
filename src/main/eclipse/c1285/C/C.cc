/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void divs(ll x, vll &f) {
	for (ll i = 2; i * i <= x; i++) {
		if (x % i == 0) {
			f.push_back(i);
			if (i != x / i)
				f.push_back(x / i);
		}
	}
}

ll lcm(ll a, ll b) {
	ll g = __gcd(a, b);
	a /= g;
	return a * b;
}

void solve() {
	cinll(x);
	vll f;
	divs(x, f);
	sort(f.begin(), f.end(), greater<ll>());

	ll ans = x;
	for (int i = 0; i < f.size(); i++) {
		ll f1 = f[i];
		ll f2 = x / f1;
		if (f1 != f2 && lcm(f1, f2) == x)
			ans = min(ans, max(f1, f2));
	}
	cout << ans << " " << x / ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

