/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* we need to find the segment with max number of
 * 1 subsegments.
 * This is, segments within the segment where number
 * of intersecting segments is 1, and where left and
 * right are subsegments with intersect bg 1.
 *
 * Sooo... we cut all segments into subsegments, with each
 * holding a ref to the orig segment.
 * Then count the subseqs of len 1, in buckets of
 * orig segment.
 *
 * We need to count subseqs val1==1 and val2>1
 * ans=min(val1,val2) over all orig segments
 */
struct event {
	event(int i1, int i2, int i3) :
			oidx(i1), idx(i2), val(i3) {

	}
	int oidx;	// index into p, orig segment
	int idx;	// index -INF..INF
	int val; 	// -1, +1
};

void solve() {
	cini(n);
	vector<pii> p(n);
	vector<event> evt;
	for (int i = 0; i < n; i++) {
		cin >> p[i].first >> p[i].second;
		if (p[i].first > p[i].second)
			swap(p[i].first, p[i].second);

		evt.emplace_back(i, p[i].first, 1);
		evt.emplace_back(i, p[i].second + 1, -1);
	}

	sort(evt.begin(), evt.end(), [&](event &e1, event &e2) {
		if (e1.idx == e2.idx)
			return e2.val < e1.val;
		else
			return e1.idx < e2.idx;
	});

	vector<pii> cnt(n);	// <val==1,val>1>
	int inter = 0;
	int prev = -1;
	stack<event> st;	// <idx, oidx>
	for (size_t i = 0; i < evt.size(); i++) {
		inter += evt[i].val;

		if (evt[i].val > 0) {
			st.push(evt[i]);
			while (i + 1 < evt.size() && evt[i].idx == evt[i + 1].idx && evt[i].val > 0) {
				i++;
				inter += evt[i].val;
				st.push(evt[i]);
			}
			if (inter == 1)
				cnt[evt[i].oidx].first++;
			else if (inter > 1)
				cnt[evt[i].oidx].second++;
		} else {
			event e = st.top();
			st.pop();
		}
	}

	int ans = 0;
	for (int i = 0; i < n; i++) {
		ans = max(ans, min(cnt[i].first, cnt[i].second));
	}
	cout << n - ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

