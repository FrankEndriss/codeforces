/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cinai(a, n);

	/* we search for i<j pair a[i],a[j] where abs(a[i]-a[j]) >= abs(j-i)
	 * for a[i]>a[j] | case ma
	 * a[i]-a[j] >= j-i
	 * a[i]-a[j]+i-j >= 0
	 * a[i]+i-j-a[j] >= 0
	 *
	 * for a[i]<a[j] | case mi
	 * a[j]-a[i] >= j-i
	 * a[j]-a[i]-j+i >= 0
	 * i-a[i] + a[j]-j>=0
	 **/
	int mi = -1e9;
	int mip = -1;
	int ma = -1e9;
	int map = -1;
	int l = -1, r = -1;

	for (int i = 0; i < n; i++) {
		if (ma - a[i] - i > 0) {
			l = map;
			r = i;
			//cout << "break ma" << endl;
			break;
		}

		//cout << "i=" << i << " a[i]-mi-i=" << a[i] - mi - i << endl;
		if (mi + a[i] - i > 0) { /* second bigger */
			l = mip;
			r = i;
			//cout << "break mi" << endl;
			break;
		}

		if (i - a[i] > mi) {
			mi = i - a[i];
			mip = i;
			//cout << "new mi=" << mi << " mip=" << mip << endl;
		}

		if (a[i] + i > ma) {
			ma = a[i] + i;
			map = i;
			//cout << "new ma=" << ma << " map=" << map << endl;
		}

	}

	if (l < 0) {
		cout << "NO" << endl;
	} else {
		cout << "YES" << endl;
		cout << l + 1 << " " << r + 1 << endl;
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

