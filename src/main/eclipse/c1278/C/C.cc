/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);

	int diff = 0;
	vi vl;
	vi vr;
	for (int i = 0; i < 2 * n; i++) {
		cini(a);
		if (a == 1)
			diff++;
		else
			diff--;
		if (i < n)
			vl.push_back(a);
		else
			vr.push_back(a);
	}

	reverse(vl.begin(), vl.end());

	/* we need to make ones == twos, so have to eat diff(ones,twos) more of the kind
	 * which has more available.
	 */
	if (diff < 0) {
		int rev[] = { 0, 2, 1 };
		for (int i = 0; i < n; i++) {
			vl[i] = rev[vl[i]];
			vr[i] = rev[vr[i]];
		}
		diff = -diff;
	}

	if (diff == 0) {
		cout << 0 << endl;
	} else {
#ifdef DEBUG
		cout << "diff=" << diff << endl;
#endif
		vi dpl(n + 1);
		dpl[0] = 0;
		for (int i = 0; i < n; i++) {
			int d;
			if (vl[i] == 1)
				d = 1;
			else
				d = -1;
			dpl[i + 1] = dpl[i] + d;
		}

		vi dpr(n + 1);
		dpr[0] = 0;
		for (int i = 0; i < n; i++) {
			int d;
			if (vr[i] == 1)
				d = 1;
			else
				d = -1;
			dpr[i + 1] = dpr[i] + d;
		}

		int mal = -1;
		int mar = -1;
		vector<pii> mapr;
		vector<pii> mapl;
		for (int i = 0; i <= n; i++) {
			if (dpl[i] > mal) {
				mal = dpl[i];
				mapl.push_back( { mal, i });
#ifdef DEBUG
				cout << "mapl.push, mal=" << mal << " i=" << i << endl;
#endif
			}

			if (dpr[i] > mar) {
				mar = dpr[i];
				mapr.push_back( { mar, i });
#ifdef DEBUG
				cout << "mapr.push, mal=" << mar << " i=" << i << endl;
#endif
			}
		}
		/* now find best pair mapl/mapr */
		int lidx = 0;
		while (lidx + 1 < mapl.size() && mapl[lidx].first < diff) {
			lidx++;
		}
		int ridx = 0;
		while (ridx + 1 < mapr.size() && mapl[lidx].first + mapr[ridx].first < diff)
			ridx++;

#ifdef DEBUG
		cout << "mapl[lidx].first=" << mapl[lidx].first << " mapl[ridx].first=" << mapl[ridx].first << endl;
#endif
		assert(mapl[lidx].first + mapr[ridx].first == diff);

		int ans = 1e9;
		while (lidx >= 0 && ridx < mapr.size()) {
			ans = min(ans, mapl[lidx].second + mapr[ridx].second);
			lidx--;
			ridx++;
		}

		cout << ans << endl;
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

