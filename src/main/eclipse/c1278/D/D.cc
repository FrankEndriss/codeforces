/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll atol(string &s) {
	istringstream iss(s);
	ll i;
	iss >> i;
	return i;
}

void atoli(string &s, vll &a) {
	istringstream iss(s);
	int i = 0;
	while (iss.good())
		cin >> a[i++];
}

void solve() {
	cini(n)
	vector<pii> seg(n);
	for (int i = 0; i < n; i++) {
		cin >> seg[i].first >> seg[i].second;
	}
	sort(seg.begin(), seg.end());

	vvi tree(n);
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n && seg[j].first < seg[i].second; j++) {
			if (seg[j].second > seg[i].second) {
#ifdef DEBUG
				cout << "tree: u=" << i << " v=" << j << endl;
#endif
				tree[i].push_back(j);
				tree[j].push_back(i);
			}
		}
	}

	/* dfs detect loops. If no loops and all nodes one component, graph is tree */
	vb vis(n);
	function<bool(int, int)> dfs = [&](int node, int parent) {
		if (vis[node])
			return true;
		vis[node] = true;
		for (int chl : tree[node]) {
			if (chl == parent)
				continue;
			if (dfs(chl, node))
				return true;
		}

		return false;
	};

	bool loop = dfs(0, -1);
	bool onecomp = true;
	for (bool b : vis)
		if (!b)
			onecomp = false;

#ifdef DEBUG
	cout << "loop=" << loop << " onecomp=" << onecomp << endl;
#endif
	if (!loop && onecomp)
		cout << "YES" << endl;
	else
		cout << "NO" << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

