/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int primeAproxFactor = 20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN = 1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN * primeAproxFactor, false);

void init() {

	ll c = 0;
	ll i = 2;
	while (i < MAXN * primeAproxFactor && c < MAXN) {
		if (!notpr[i]) {
			pr[c] = i;
			c++;
			ll j = 1LL * i * i;
			while (j < primeAproxFactor * MAXN) {
				notpr[j] = true;
				j += i;
			}
		}
		i++;
	}
}

void solve() {
	cini(n);
	for (int i = 4; i < n; i++) {
		int ni = n - i;
		if (notpr[i] && notpr[ni]) {
			cout << i << " " << ni << endl;
			return;
		}
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	init();
	solve();
}

