/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define MOD 998244353

int mul(const int v1, const int v2, int mod = MOD) {
	return (int) ((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod = MOD) {
	int res = 1;
	while (p != 0) {
		if (p & 1)
			res = mul(res, a, mod);
		p >>= 1;
		a = mul(a, a, mod);
	}
	return res;
}

int pl(int v1, int v2, int mod = MOD) {
	int res = v1 + v2;

	if (res < 0)
		res += mod;

	else if (res >= mod)
		res -= mod;

	return res;
}

int inv(const int x, const int mod = MOD) {
	return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
	return mul(zaehler, inv(nenner));
}

const int N = 1e6 + 3;

/* 7/8 */
void solve() {
	cinll(n);
	vvi a(n);
	vvi f(N + 1);	// f[i]=freq of item i

	int maItem = 0;
	for (int i = 0; i < n; i++) {
		cinll(k);

		for (int j = 0; j < k; j++) {
			cini(aux);
			maItem = max(maItem, aux);
			a[i].push_back(aux);
			f[aux].push_back(i);
		}
	}

	const int fracn2 = mul(fraction(1, n), fraction(1, n));
	vi fsum(N + 1);
	for (int i = 1; i <= maItem; i++) {
		for (int x : f[i]) {
			int lfrac = fraction(1, a[x].size());
			lfrac = mul(lfrac, fracn2);
			fsum[i] = pl(fsum[i], lfrac);
		}
	}

	int ans = 0;

	for (int z = 0; z < n; z++) {
		for (int item : a[z]) {
			ans = pl(ans, fsum[item]);
		}
		//cout << "x=" << x << " z=" << z << " cnt=" << cnt << " a[x].size()=" << a[x].size() << endl;
	}

	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

