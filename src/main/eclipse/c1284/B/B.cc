/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	ll cntHasA = 0;
	vb hasA(n);
	vi fi(n);
	vi la(n);
	vi fi2;
	vi hasNotA;
	for (int i = 0; i < n; i++) {
		cini(l);
		vi a(l);

		int mi = 1e9;
		int ma = 0;

		for (int j = 0; j < l; j++) {
			cin >> a[j];
			mi = min(mi, a[j]);
			ma = max(ma, a[j]);

			if (j > 0 && a[j] > a[j - 1])
				hasA[i] = true;
		}
		fi[i] = ma;
		la[i] = mi;

		if (hasA[i]) {
			cntHasA++;
		} else {
			hasNotA.push_back(i);
			fi2.push_back(fi[i]);
		}
	}

	/* sort max values of seqs */
	sort(fi2.begin(), fi2.end());

#ifdef DEBUG
	for (int i = 0; i < hasNotA.size(); i++) {
		cout << "hasNotA, i=" << i << " idx=" << hasNotA[i] << " la[idx]=" << la[hasNotA[i]] << endl;
	}
	cout << "f2= ";
	for (int i : fi2)
		cout << i << " ";
	cout << endl;
#endif

	ll ans = cntHasA * n * 2 - (cntHasA * cntHasA);
	for (size_t i = 0; i < hasNotA.size(); i++) {
		auto it = upper_bound(fi2.begin(), fi2.end(), la[hasNotA[i]]);
		int dist = distance(it, fi2.end());
#ifdef DEBUG
		cout << "dist=" << dist << " " << endl;
#endif
		ans += dist;
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

