/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int MOD = 0;

int pl(int v1, int v2) {
	int res = v1 + v2;

	if (res < 0)
		res += MOD;

	else if (res >= MOD)
		res -= MOD;

	return res;
}

int mul(const int v1, const int v2) {
	return (int) ((1LL * v1 * v2) % MOD);
}

const int N = 250007;
vector<int> fac(N);

void initFac() {
	fac[1] = 1;
	for (int i = 2; i < N; i++) {
		fac[i] = mul(fac[i - 1], i);
	}
}

void solve() {
	cini(n);
	cin >> MOD;

	if (n == 1) {
		cout << 1 << endl;
		return;
	}

	initFac();

	int ans = mul(fac[n], n);	// all permutations * subseq of len 1
	ans = pl(ans, fac[n]);	// all permutations subseq of len n

	/* add number of times there is a subseq of len len with len consecutive numbers in it */
	for (int len = 2; len < n; len++) {
		int lans = n - len + 1; /* start positions of seq of len len */
		lans = mul(lans, fac[len]); /* permutations off same consecutive numbers */
		lans = mul(lans, n - len + 1); /* different first numbers */
		lans = mul(lans, fac[n - len]); /* number of times that happens */
		ans = pl(ans, lans);
	}

	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

