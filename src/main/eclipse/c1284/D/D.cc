/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
/** stolen from tourist, Problem D, https://codeforces.com/contest/1208/standings/page/1 */
template<typename T>
class fenwick {
public:
	vector<T> fenw;
	int n;

	fenwick(int _n) :
			n(_n) {
		fenw.resize(n);
	}

	void modify(int x, T v) {
		while (x < n) {
			fenw[x] += v;
			x |= (x + 1);
		}
	}

	/* get sum of range (0,x), including x */
	T get(int x) {
		T v { };
		while (x >= 0) {
			v += fenw[x];
			x = (x & (x + 1)) - 1;
		}
		return v;
	}
};

template<typename T>
class fenwick2d {
public:
	vector<fenwick<T>> fenw2d;
	int n, m;

	fenwick2d(int x, int y) :
			n(x), m(y) {
		fenw2d.resize(n, fenwick<int>(m));
	}

	void modify(int x, int y, T v) {
		while (x < n) {
			fenw2d[x].modify(y, v);
			x |= (x + 1);
		}
	}

	T get(int x, int y) {   // range 0..x/y, including x/y
		x = min(x, n - 1);
		y = min(y, m - 1);
		T v { };
		while (x >= 0) {
			v += fenw2d[x].get(y);
			x = (x & (x + 1)) - 1;
		}
		return v;
	}
};

bool overlap(pii f, pii s) {
	return max(f.first, s.first) <= min(f.second, s.second);
}

const int INF = 1e9;
/* Let lect be sx,ex
 * We search for a pair of { lect1,lect2 } so that it overlaps in a ^ b. ie exactly once.
 * So, every set of overlaps of lectX must be same in both venues.
 * If not, that lectX has at least on lectY for where there is such a diff.
 *
 * Note: That fenwick2d based sol does not work :/
 */
void solve() {
	cini(n);
	vector<pair<pii, pii>> l(n);

	fenwick2d<int> fenA(n, n);
	fenwick2d<int> fenB(n, n);

	for (int i = 0; i < n; i++) {
		cin >> l[i].first.first >> l[i].first.second >> l[i].second.first >> l[i].second.second;
		fenA.modify(l[i].first.first, l[i].first.second, 1);
		fenB.modify(l[i].second.first, l[i].second.second, 1);
	}

	for (int i = 0; i < n; i++) {
		int ovA = fenA.get(INF, l[i].first.second) - fenA.get(l[i].first.second - 1, l[i].first.first);
		int ovB = fenB.get(INF, l[i].second.second) - fenB.get(l[i].second.second - 1, l[i].second.first);
#ifdef DEBUG
		cout << "lect i=" << i << " ovA=" << ovA << " ovB=" << ovB << endl;
#endif

		if (ovA != ovB) {
			/* find number of overlapping lects in a and in b */
			/* brute force */
			for (int j = i + 1; j < n; j++) {
				if (overlap(l[i].first, l[j].first) != overlap(l[i].second, l[j].second)) {
					cout << "match found!! i=" << i << " j=" << j << endl;
				}
			}
		}
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

