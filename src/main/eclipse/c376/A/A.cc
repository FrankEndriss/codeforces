/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cins(s);
	int mid = -1;
	for (int i = 0; i < s.size(); i++) {
		if (s[i] == '^')
			mid = i;
	}
	assert(mid >= 0);

	ll l = 0, r = 0;
	for (int i = 1; i < s.size(); i++) {
		int lidx = mid - i;
		if (lidx >= 0 && s[lidx] >= '1' && s[lidx] <= '9')
			l += i * (s[lidx] - '0');

		int ridx = mid + i;
		if (ridx < s.size() && s[ridx] >= '1' && s[ridx] <= '9')
			r += i * (s[ridx] - '0');
	}
	if (l > r)
		cout << "left" << endl;
	else if (l < r)
		cout << "right" << endl;
	else
		cout << "balance" << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

