/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD = 1e9 + 7;

int pl(int i1, int i2) {
	int ans = i1 + i2;
	while (ans < 0)
		ans += MOD;
	while (ans >= MOD)
		ans -= MOD;
	return ans;
}
void solve() {
	cini(x);
	cins(s);
	ll slen = s.size();
	for (int idx = 0; idx < x; idx++) {
//		cout << "idx=" << idx << " s=" << s << " slen=" << slen << endl;
		int mul = s[idx] - '1';

		ll tlen = pl(slen, -(idx + 1)) * mul;
		tlen %= MOD;
//		cout << "tlen=" << tlen << endl;
		slen = pl(slen, tlen);
		slen %= MOD;

		if (s.size() < x) {
			string t = s.substr(idx + 1);
			for (int i = 0; i < mul; i++) {
				if (s.size() < x)
					s += t;
			}
		}
	}
	cout << slen << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

