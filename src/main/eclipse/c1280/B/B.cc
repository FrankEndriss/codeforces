/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(r);
	cini(c);
	vi cnt(2);
	cinas(s, r);

	set<int> rp;	// rows with P
	set<int> cp;	// cols with P
	set<int> ra;
	set<int> ca;
	for (int i = 0; i < r; i++)
		for (int j = 0; j < c; j++) {
			if (s[i][j] == 'P') {
				cnt[0]++;
				rp.insert(i);
				cp.insert(j);
			} else {
				ra.insert(i);
				ca.insert(j);
				cnt[1]++;
			}
		}

	if (cnt[0] == 0) {
		cout << "0" << endl;
		return;
	} else if (cnt[1] == 0) {
		cout << "MORTAL" << endl;
		return;
	}

	/* We need to check if we can change all P in one move.
	 * That is, if one border row or col is completly A.
	 */
	bool oneshot = false;
	bool p1 = true;
	bool p2 = true;
	for (int i = 0; i < r; i++) {
		if (s[i][0] != 'A')
			p1 = false;
		if (s[i][c - 1] != 'A')
			p2 = false;
	}
	if (p1 || p2)
		oneshot = true;
	p1 = true;
	p2 = true;
	for (int j = 0; j < c; j++) {
		if (s[0][j] != 'A')
			p1 = false;
		if (s[r - 1][j] != 'A')
			p2 = false;
	}
	if (p1 || p2)
		oneshot = true;

	if (oneshot) {
		cout << 1 << endl;
		return;
	}

	/* check if any (other) whole row/col is A,
	 * then we need two moves.
	 */
	bool twoshot = false;
	for (int j = 0; j < c; j++) {
		p1 = true;
		for (int i = 0; i < r; i++) {
			if (s[i][j] != 'A')
				p1 = false;
		}
		if (p1)
			twoshot = true;
	}
	for (int i = 0; i < r; i++) {
		p1 = true;
		for (int j = 0; j < c; j++) {
			if (s[i][j] != 'A')
				p1 = false;
		}
		if (p1)
			twoshot = true;
	}

	if (twoshot) {
		cout << 2 << endl;
		return;
	}

	/* or any corner, two moves needed, too. */
	if (s[0][0] == 'A' || s[0][c - 1] == 'A' || s[r - 1][0] == 'A' || s[r - 1][c - 1] == 'A') {
		cout << 2 << endl;
		return;
	}

	/* one field at the border makes three moves possible */
	if (ra.count(0) || ra.count(r - 1) || ca.count(0) || ca.count(c - 1)) {
		cout << 3 << endl;
		return;
	}
	cout << 4 << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

