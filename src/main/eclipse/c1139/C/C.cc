/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
struct Dsu {
	vector<int> p;
	vector<int> s;
	/* initializes each (0..size-1) to single set */
	Dsu(int size) {
		p.resize(size);
		s.resize(size, 1);
		for (int i = 0; i < size; i++)
			p[i] = i;
	}

	/* finds set representative for member v */
	int find_set(int v) {
		if (v == p[v])
			return v;
		return p[v] = find_set(p[v]);
	}

	/* combines the sets of two members.
	 * Use the bigger set as param a for optimized performance.  */
	void union_sets(int a, int b) {
		a = find_set(a);
		b = find_set(b);
		if (a != b) {
			p[b] = a;
			s[a] += s[b];
		}
	}
};
#define MOD 1000000007

int mul(const int v1, const int v2, int mod = MOD) {
	return (int) ((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod = MOD) {
	int res = 1;
	while (p != 0) {
		if (p & 1)
			res = mul(res, a, mod);
		p >>= 1;
		a = mul(a, a, mod);
	}
	return res;
}

int pl(int v1, int v2, int mod = MOD) {
	int res = v1 + v2;

	if (res < 0)
		res += mod;

	else if (res >= mod)
		res -= mod;

	return res;
}

/* build components by red edges, let
 * a[] be the size of these m components.
 * Then
 * ans= n^k - a[0]^k - a[1]^k - ... - a[m]^k
 */
void solve() {
	cini(n);
	cini(k);
	Dsu dsu(n);
	for (int i = 0; i < n - 1; i++) {
		cini(u);
		u--;
		cini(v);
		v--;
		cini(x);
		if (x == 0)
			dsu.union_sets(u, v);
	}

	set<int> red;
	for (int i = 0; i < n; i++)
		red.insert(dsu.find_set(i));

	int ans = toPower(n, k);
	for (int r : red) {
		int num = toPower(dsu.s[r], k);
		ans = pl(ans, -num);
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

