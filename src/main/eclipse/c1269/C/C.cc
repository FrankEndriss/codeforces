/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(k);
	cins(s);
	vi a;
	for (char c : s)
		a.push_back(c - '0');

	vi b = a;

	for (int i = 0; i < n; i++) {
		if (i + k < n)
			b[i + k] = b[i];
	}
#ifdef DEBUG
	cout << "dbg b=";
	for (int i : b)
		cout << i;
	cout << endl;
#endif

	bool need_add = false;
	for (int i = 0; i < n; i++) {
		if (b[i] > a[i])
			break;
		else if (b[i] < a[i])
			need_add = true;
	}
	int carry = 0;
	if (need_add) {
		vi chgIdx;
		b[k - 1]++;
		chgIdx.push_back(k - 1);

		for (int i = k - 1; i >= 0; i--) {
			if (b[i] == 10) {
				b[i] = 0;
				if (i > 0) {
					b[i - 1]++;
					chgIdx.push_back(i - 1);
				} else
					carry = 1;
			}
		}
		for (int idx : chgIdx) {
			for (int j = idx + k; j < n; j += k)
				b[j] = b[idx];
		}
	}

	if (carry)
		cout << n + 1 << endl;
	else
		cout << n << endl;

	if (carry)
		cout << 1;
	for (size_t i = 0; i < b.size(); i++)
		cout << b[i];
	cout << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

