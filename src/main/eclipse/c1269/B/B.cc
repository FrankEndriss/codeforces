/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(m);
	cinai(a, n);
	cinai(b, n);

	set<int> diffs;
	map<int, int> fa;
	map<int, int> fb;
	for (int i = 0; i < n; i++) {
		int d = b[i] - a[0];
		if (d < 0)
			d += m;
		diffs.insert(d);

		fa[a[i]]++;
		fb[b[i]]++;
	}

	/* check all possible diffs */
	int ans = 1e9 + 7;
	for (int d : diffs) {
		bool ok = true;
		for (auto ent : fa) {
			if (fb[(ent.first + d) % m] != ent.second) {
				ok = false;
				break;
			}
		}
		if (ok) {
			ans = d;
			break;
		}
	}

	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

