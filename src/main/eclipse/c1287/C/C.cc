/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* we have gaps with same parity at begin and end,
 * and differentparity.
 * if same parity, and enough avail, we can fill a gap without compl.
 * So, we need to sort gaps by size.
 * Gaps with diff parity:
 * They produce 1 compl, independend of what we are doing.
 * So, we first need to fill same-par-gaps by size,
 * smallest first.
 * First and last gap are like same-par-gap.
 */
void solve() {
	cini(n);
	vb vis(n + 1);
	vi avail(2);
	vi g(n + 1, -1);

	vector<pii> gaps; 	// same par gaps

	int last = -1; // last set position
	for (int i = 1; i <= n; i++) {
		cini(aux);
		if (aux == 0)
			continue;

		vis[aux] = true;
		g[i] = aux & 1;

		if (last == -1 && i > 1) {
			gaps.push_back( { 1, i - 1 });
		} else if (last < i - 1) {
			if (g[last] == g[i]) {
				gaps.push_back( { last + 1, i - 1 });
			}
		}
		last = i;
	}

	for (int i = 1; i <= n; i++) {
		if (!vis[i])
			avail[i & 1]++;
	}

	sort(gaps.begin(), gaps.end(), [&](pii p1, pii p2) {
		return p1.second - p1.first < p2.second - p2.first;
	});

	/** fill smallest gaps */
	for (size_t i = 0; i < gaps.size(); i++) {
		int gpar;
		if (gaps[i].first == 1)
			gpar = g[gaps[i].second];
		else
			gpar = g[gaps[i].first];

		int gsize = gaps[i].second - gaps[i].first;
		if (gsize <= avail[gpar]) {
			for (int j = gaps[i].first + 1; j < gaps[i].second; j++) {
				g[j] = gpar;
				avail[gpar]--;
			}
		}
	}

	if (g[1] == -1) {
		auto first = *min_element(gaps.begin(), gaps.end());
		int fill = !g[first.second];
		for (int i = 1; i < first.second; i++) {
			if (avail[fill]) {
				g[i] = fill;
				avail[fill]--;
			} else {
				g[i] = !fill;
				avail[!fill]--;
			}
		}
	}

	/* fill other gaps */
	for (int i = 2; i <= n; i++) {
		if (avail[g[i - 1]]) {
			g[i] = g[i - 1];
			avail[g[i - 1]]--;
		} else {
			g[i] = !g[i - 1];
			avail[!g[i - 1]]--;
		}
	}

	assert(avail[0] == 0);
	assert(avail[1] == 0);
	int ans = 0;
//cout << g[1];
	for (int i = 2; i <= n; i++) {
		//	cout << g[i];

		if (g[i] != g[i - 1])
			ans++;
	}
//cout << endl;
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

