/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

string nc2i(char c1, char c2) {
	if (c1 == 'S') {
		if (c2 == 'E')
			return "T";
		else
			return "E";
	} else if (c1 == 'E') {
		if (c2 == 'S')
			return "T";
		else
			return "S";
	} else if (c1 == 'T') {
		if (c2 == 'E')
			return "S";
		else
			return "E";
	} else
		assert(false);
}

void solve() {
	cini(n);
	cini(k);
	vs s(n);

	vvi f(k, vi(3));

	for (int i = 0; i < n; i++) {
		cin >> s[i];
	}
	sort(s.begin(), s.end());

	ll ans = 0;
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n - 1; j++) {
			string se;

			for (int l = 0; l < k; l++) {
				if (s[i][l] == s[j][l])
					se += s[i][l];
				else
					se += nc2i(s[i][l], s[j][l]);
			}

			/*
			 cout << "s[i]=" << s[i] << endl;
			 cout << "s[j]=" << s[j] << endl;
			 cout << "  se=" << se << endl;
			 cout << endl;
			 */
			/*
			 auto it = lower_bound(s.begin(), s.end(), se);
			 if (it != s.end()) {
			 */
			if (binary_search(s.begin() + j + 1, s.end(), se)) {
				//int idx = distance(s.begin(), it);
				//if (idx > j)
				ans++;
			}
		}
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

