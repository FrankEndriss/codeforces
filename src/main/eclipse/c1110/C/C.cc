/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

map<int, int> memo;

int bdev(int n) {
	auto it = memo.find(n);
	if (it != memo.end())
		return it->second;

	for (int i = 2; i * i <= n; i++) {
		if (n % i == 0)
			return n / i;
	}

	return 1;
}

void solve() {
	cini(a);
	ll num = 0;
	ll bit = 1;
	int b = 0;
	while (num < a) {
		if ((a & bit) == 0) {
			b |= bit;
		}
		num |= bit;
		bit <<= 1;
	}
	if (b == 0) {
		// TODO find biggest divisor of a and use that as value for b
		// memoize. There cannot be more that 25 different a for which
		// b is zero.
		b = bdev(a);
	}

	cout << __gcd(a ^ b, a & b) << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

