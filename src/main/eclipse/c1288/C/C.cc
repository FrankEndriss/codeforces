/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD = 1e9 + 7;

void solve() {
	cini(n);
	cini(m);
	m *= 2;

	/* find number of arrays of len 2*m where a[i]<=a[i+1] for all positions i
	 */
	vvi dp(m, vi(n, -1));	// dp[i][j]= number of possibilities at idx i if last number was j

	function<int(int, int)> pos = [&](int idx, int prev) {
		if (prev == n)
			return 1;
		if (idx == m - 1)
			return n - prev + 1;

		if (dp[idx][prev] >= 0)
			return dp[idx][prev];

		int ans = 0;
		for (int i = prev; i <= n; i++) {
			ans += pos(idx + 1, i);
			ans %= MOD;
		}
		dp[idx][prev] = ans;
		return ans;
	};
	int ans = pos(0, 1);
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

