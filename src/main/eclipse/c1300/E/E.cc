/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* for all a[i] from left to right
 * we need to find the smallest possible outcome for that position.
 * wich is the smallest average of all postfixes.
 * but, if we can make at least one of them smaller...
 * So, we need to do this recursive for all postfixes.
 *
 * We allways "want" to make the last one biggest.
 * ...
 * for a postfix,  we want to know how much element should be used
 * it is either:
 * 1. dont use it at all
 * 2. use first element
 * 3. use all elements
 * blah...
 *
 * We try simple impl:
 * from left to right,
 * Choose smallest possible average for d[i],
 * repeat...
 * BUT: That is n^2, since we need to find
 * smallest sum/index for all positions right of
 * the curent one in each loop. sic.
 */
void solve() {
	cini(n);
	vector<double> d(n);
	vector<double> sum(n);

	vector<pair<double, int>> ans;
	double mi = 1e18;
	int miidx = 0;
	for (int i = 0; i < n; i++) {
		cini(a);
		d[i] = a;
		sum[i] += d[i];
		if (i > 0)
			sum[i] += sum[i - 1];
		const double val = sum[i] / (i + 1);
		if (val < mi) {
			mi = val;
			miidx = i;
		}
	}

	ans.emplace_back(mi, miidx);

	while (true) {
		// todo... idk
	}

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

