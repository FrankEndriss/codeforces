/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct fract {
	ll f, s;

	fract(ll counter, ll denominator) :
			f(counter), s(denominator) {
		norm();
	}

	bool operator<(const fract &rhs) {
		return f * rhs.s < s * rhs.f;
	}

	fract& operator+=(const fract &rhs) {
		ll g = __gcd(this->s, rhs.s);
		ll lcm = this->s * (rhs.s / g);
		this->f = this->f * (lcm / this->s) + rhs.f * (lcm / rhs.s);
		this->s = lcm;
		norm();
		return *this;
	}

	friend fract operator+(fract lhs, const fract &rhs) {
		lhs += rhs;
		return lhs;
	}

	friend fract operator-(fract lhs, const fract &rhs) {
		lhs += fract(-rhs.f, rhs.s);
		return lhs;
	}

	fract& operator*=(const fract &rhs) {
		this->f *= rhs.f;
		this->s *= rhs.s;
		norm();
		return *this;
	}

	friend fract operator*(fract lhs, const fract &rhs) {
		lhs *= rhs;
		return lhs;
	}

	friend fract operator/(fract lhs, const fract &rhs) {
		return lhs * fract(rhs.s, rhs.f);
	}

	void norm() {
		if (s == 0) {
			f = -1;
			return;
		}
		int sign = ((this->f < 0) + (this->s < 0)) % 2;
		sign = -sign;
		if (sign == 0)
			sign = 1;
		ll g = __gcd(abs(this->f), abs(this->s));
		if (g != 0) {
			this->f = (abs(this->f) * sign) / g;
			this->s = abs(this->s) / g;
		}
	}
};

/* P is similar if number of sides is even and all opposite sides
 * are parallel.
 */
void solve() {
	cini(n);
	vector<pair<ll, ll>> p(n);
	for (int i = 0; i < n; i++) {
		cin >> p[i].first >> p[i].second;
	}

	if (n & 1) {
		cout << "NO" << endl;
		return;
	} else {
		for (int i1 = 0; i1 < n; i1++) {
			int i2 = (i1 + 1) % n;
			fract f1(p[i1].first - p[i2].first, p[i1].second - p[i2].second);
			double len1 = hypot(p[i1].first - p[i2].first, p[i1].second - p[i2].second);

			int ii1 = (i1 + n / 2) % n;
			int ii2 = (ii1 + 1) % n;
			fract f2(p[ii2].first - p[ii1].first, p[ii2].second - p[ii1].second);
			double len2 = hypot(p[ii1].first - p[ii2].first, p[ii1].second - p[ii2].second);

			//cout << "i1=" << i1 << " i2=" << i2 << " ii1=" << ii1 << " ii2=" << ii2 << endl;
			//cout << "f1.f=" << f1.f << " != f2.f=" << f2.f << endl;

			if (f1.f != f2.f || f1.s != f2.s || len1 != len2) {
				cout << "NO" << endl;
				return;
			}
		}
		cout << "YES" << endl;
	}

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

