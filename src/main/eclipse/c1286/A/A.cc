/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<vvvi> vvvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vi a(n);
	vi av(2);
	av[0] = n / 2;
	av[1] = (n + 1) / 2;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
		if (a[i] > 0) {
			a[i] %= 2;
			av[a[i]]--;
		} else
			a[i] = -1;
	}

	/* dp[i][j][k][l]= min score starting at idx i with j 0 available and k 1 available and last number was l */
	vvvvi dp(n, vvvi(n / 2 + 2, vvi(n / 2 + 2, vi(2, -1))));

	const int INF = 1e9;
	function<int(int, int, int, int)> score = [&](int idx, int i0, int i1, int prev) {
		if (idx == n)
			return 0;

		const int dpval = dp[idx][i0][i1][prev];
		if (dpval >= 0)
			return dpval;

		if (a[idx] >= 0) {
			int ans = 0;
			if (a[idx] != prev)
				ans++;
			return ans + score(idx + 1, i0, i1, a[idx]);
		} else {
			int ans1 = INF;
			if (i0 > 0) {
				ans1 = score(idx + 1, i0 - 1, i1, 0);
				if (prev == 1)
					ans1++;
			}
			int ans2 = INF;
			if (i1 > 0) {
				ans2 = score(idx + 1, i0, i1 - 1, 1);
				if (prev == 0)
					ans2++;
			}
			int ans = min(ans1, ans2);
			dp[idx][i0][i1][prev] = ans;
			return ans;
		}
	};

	int ans0 = score(0, av[0], av[1], 0);
	int ans1 = score(0, av[0], av[1], 1);
	cout << min(ans0, ans1) << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

