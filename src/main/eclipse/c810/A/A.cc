/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void mygetline(string &s) {
	getline(cin, s);
	while (s.size() && s.back() == 13)
		s.pop_back();
}

ll atol(string &s) {
	istringstream iss(s);
	ll i;
	iss >> i;
	return i;
}

void atoli(string &s, vll &a) {
	istringstream iss(s);
	int i = 0;
	while (iss.good())
		cin >> a[i++];
}

void solve() {
	cini(n);
	cini(k);
	double sum = 0;
	int miss = 0;
	for (int i = 0; i < n; i++) {
		cini(aux);
		miss += k - aux;
	}

	int ans = max(0, miss * 2 - n);
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

