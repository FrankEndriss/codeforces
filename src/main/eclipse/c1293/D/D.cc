/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* It seems that we can collect us subarray from
 * the nodes.
 * How to construct?
 * Find nearest node, go there.
 * Find number how much we collect if going r.
 * Tyr left[nearest] and go right
 * ...
 *
 * dp time etc if collect first node,
 * and if not-collect first node,
 * same second... and so on.
 * ignore paths with bad result.
 *
 * Start at first reacheable node.
 */

void solve() {
	cinll(x0);
	cinll(y0);	// first constructed node
	cinll(ax);
	cinll(ay);	// construction rule, note 2 <= ax,bx <=100
	cinll(bx);
	cinll(by);

	cinll(xs);
	cinll(ys);
	cinll(t);

#define dist(x1, y1, x2, y2) (abs(x1-x2)+abs(y1-y2))
	/* find first collectable node */
	ll xn = x0;
	ll yn = y0;

	ll d = 0;
	const ll INF = 1e18;
	vector<pair<ll, ll>> rnodes;	 // reachable nodes, max 50 or so

	for (int i = 0; d < INF; i++) {
		d = dist(xs, ys, xn, yn);
		if (d <= t) {
			rnodes.emplace_back(xn, yn);
		}

		if (INF / ax < xn || INF / ay < yn)	 // overflow
			break;

		xn = xn * ax + bx;
		yn = yn * ay + by;
	}

	/* so following paths are possible:
	 * start -> node[x] -> node[x+1] -> node[x+2]...
	 * start -> node[x] -> node[x-1] -> node[x-2]...
	 * simply check all, cant be that much.
	 */

	int ans = 0;
	for (size_t i = 0; i < rnodes.size(); i++) {
		int lans = 1;
		ll t0 = t;	 // available time
		t0 -= dist(xs, ys, rnodes[i].first, rnodes[i].second);

		/* go downwards */
		for (int j = i - 1; j >= 0; j--) {
			ll d = dist(rnodes[j + 1].first, rnodes[j + 1].second, rnodes[j].first, rnodes[j].second);
			if (t0 >= d) {
				t0 -= d;
				lans++;
			} else
				break;
		}
		ans = max(ans, lans);

		/* go upwards */
		t0 = t;
		t0 -= dist(xs, ys, rnodes[i].first, rnodes[i].second);
		lans = 1;
		for (int j = i + 1; j < rnodes.size(); j++) {
			ll d = dist(rnodes[j - 1].first, rnodes[j - 1].second, rnodes[j].first, rnodes[j].second);
			if (t0 >= d) {
				t0 -= d;
				lans++;
			} else
				break;
		}
		ans = max(ans, lans);
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

