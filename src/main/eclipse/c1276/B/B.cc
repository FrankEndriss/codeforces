/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct Dsu {
	vector<int> p;
	vector<int> s;
	/* initializes each (0..size-1) to single set */
	Dsu(int size) {
		p.resize(size);
		s.resize(size, 1);
		for (int i = 0; i < size; i++)
			p[i] = i;
	}

	/* finds set representative for member v */
	int find_set(int v) {
		if (v == p[v])
			return v;
		return p[v] = find_set(p[v]);
	}

	/* combines the sets of two members.
	 * Use the bigger set as param a for optimized performance.  */
	void union_sets(int a, int b) {
		a = find_set(a);
		b = find_set(b);
		if (a != b) {
			p[b] = a;
			s[a] += s[b];
		}
	}
};

/* we remove a and b from the graph, and build components.
 * All pairs of cities in different components are
 * counted.
 * Build components by dsu, count set sizes in dsu.
 */
void solve() {
	cini(n);
	cini(m);
	cini(a);
	cini(b);
	vvi adj(n);
	for (int i = 0; i < m; i++) {
		cini(u);
		cini(v);
		if (u != a && u != b && v != a && v != b) {
			u--;
			v--;
			adj[u].push_back(v);
			adj[v].push_back(u);
		}
	}

	Dsu dsu(n);

	vb vis(n);
	int root;
	function<void(int)> dfs = [&](int node) {
		vis[node] = true;

		dsu.union_sets(root, node);
		for (int chl : adj[node]) {
			if (!vis[chl])
				dfs(chl);
		}
	};

	vi sets;
	for (int i = 0; i < n; i++) {
		if (dsu.find_set(i) == i) {
			root = i;
			vis.assign(n, false);
			sets.push_back(i);
			dfs(i);
		}
	}

	ll ans = 0;
	for (size_t i = 0; i < sets.size(); i++) {
		for (size_t j = i + 1; j < sets.size(); j++) {
			ans += dsu.s[sets[i]] * dsu.s[sets[j]];
		}
	}
	cout << ans << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

