/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF = 1e9;

void solve() {
	cini(n);
	cini(p);
	cini(k);
	cinai(a, n);
	sort(a.begin(), a.end());

	/* In one action we buy cheapest good for a[0], or k goods for a[k-1].
	 * In both cases we bought all goods from left, never leave on untouched. */

	vi dp(n, INF); // dp[i]=min price for first i goods
	dp[0] = a[0];
	dp[k - 1] = a[k - 1];

	for (int i = 1; i < n; i++) {
		dp[i] = min(dp[i], dp[i - 1] + a[i]);
		if (i >= k) {
			dp[i] = min(dp[i], dp[i - k] + a[i]);
		}
	}

	/* biggest index with dp[i]<=p */
	int ans = -1;
	for (int i = 0; i < n; i++)
		if (dp[i] <= p)
			ans = i;

	cout << ans + 1 << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

