/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n); // problems
	cini(T); // minutes

	cini(a); // t easy
	cini(b); // t hard

#ifdef DEBUG
	cout << "n=" << n << " T=" << T << " a=" << a << " b=" << b << endl;
#endif

	vi ishard(n);
	ll acnt = 0;
	ll bcnt = 0;
	for (int i = 0; i < n; i++) {
		bool aux;
		cin >> aux;
		if (aux) {
			ishard[i] = b;
			bcnt++;
		} else {
			ishard[i] = a;
			acnt++;
		}
	}

	cinai(t, n);
	vi id(n);
	iota(id.begin(), id.end(), 0);
	sort(id.begin(), id.end(), [&](int i1, int i2) {
		return t[i1] < t[i2];
	});

	/* For every minute before a problem becomes mandantory calculate the points
	 * he gets if leaving the contest at that point of time.
	 * max of that is ans.
	 */
	ll ans = 0;
	ll tsum = 0;
	for (int i = 0; i < n; i++) {
		int t0 = t[id[i]] - 1;
		if (tsum <= t0) {
			ll tfree = t0 - tsum;	// time to solve non mandantory problems
			ll lans = i;	// all mandantory probs
			ll aprobs = min(acnt, tfree / a); // additional a probs
			lans += aprobs;
			tfree -= aprobs * a;
			ll bprobs = min(bcnt, tfree / b);	// additional b probs
			lans += bprobs;
			ans = max(ans, lans);
		}

		tsum += ishard[id[i]];
		if (ishard[id[i]] == a)
			acnt--;
		else
			bcnt--;
	}

	if (tsum <= T) {
		ans = n;
	}

	cout << ans << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

