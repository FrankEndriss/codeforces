/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void mygetline(string &s) {
	getline(cin, s);
	while (s.back() == 13)
		s.pop_back();
}

void solve() {
	cini(a);
	cini(b);
	cini(c);
	cini(r);

	if (r < 0)
		r = -r;

	if (a > b)
		swap(a, b);

	int i1 = c - r;
	int i2 = c + r;
	/* intersection i1,i2 X a,b is coverage */
	int isect = max(0, min(b, i2) - max(i1, a));
	int ans = max(0, b - a - isect);
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

