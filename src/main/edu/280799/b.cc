
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


template<typename E>
struct SegmentTreeLazy {
    vector<E> data;
    vector<E> lazyInc;
    vector<bool> lazyIncB;  /* flag if there is a pending lazyInc in node */

    E neutral;
    int n;

    function<E(E,E)> plus;

    SegmentTreeLazy(int _n, E _neutral) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2, 0);
        lazyIncB.resize(n*2);
        neutral=_neutral;
    }

    /* pushes down the pending updates to the vertex node 
     * If both lazies are set then the inc was later, so both
     * have to be pushed.
     * But first the set, which erases all previously set 
     * and inc on the child nodes, then the inc.
     * */
    void pushlazy(int node, int sL, int sR) {
        if(lazyIncB[node]) {
            //cerr<<"pushlazy Inc, node="<<node<<" sL="<<sL<<" sR="<<sR<<endl;
            if(sL!=sR) {
                int mid=(sL+sR)/2;
                rangeInc(node*2, sL   , mid, sL   , mid);
                rangeInc(node*2+1, mid+1, sR , mid+1, sR);
            }
            lazyIncB[node]=false;
        }
    }

    int q1(int k) {
        //cerr<<"q1, k="<<k<<endl;
        return q1(1, 0, n-1, k);
    }
    
    int q1(int node, int sL, int sR, int k) {
        //cerr<<"q1 node="<<node<<" sL="<<sL<<" sR="<<sR<<" k="<<k<<endl;
        assert(k>0);
        if(sL==sR) {
            assert(k==1);
            assert(data[node]==1);
            return sL;
        }

        pushlazy(node, sL, sR);

        int mid=(sL+sR)/2;
        if(data[node*2]>=k)
            return q1(node*2, sL, mid, k);
        else
            return q1(node*2+1, mid+1, sR, k-data[node*2]);
    }

    /* increment all position in (l,r) by inc */
    void rangeInc(int node, int sL, int sR, int l, int r) {
        if (r < sL || l > sR)
            return;
        //cerr<<"rangeInc node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<endl;

        if(l<=sL && r>=sR) {
            lazyIncB[node]=!lazyIncB[node];
            data[node]=sR-sL+1-data[node];
            //cerr<<"rangeInc did set node="<<data[node]<<endl;
            return;
        }

        pushlazy(node, sL, sR);

        int mid = (sL+sR)/2;
        rangeInc(node*2  , sL   , mid, l, r);
        rangeInc(node*2+1, mid+1, sR , l, r);
        data[node]=plus(data[node*2], data[node*2+1]);
    }

    /* increment by inc at positions (l,r) */
    void rangeInc(int l, int r) {
        rangeInc(1, 0, n-1, l, r);
    }
};

/*
 * maintain segment with sum.
 * every segment holds the number of 1 in that segment.
 * modifiy rangeInc() accordingly.
 */
void solve() {
    cini(n);
    cini(m);

    SegmentTreeLazy<int> seg(n, 0);

    seg.plus=[](int i1, int i2) { return i1+i2; };

    for(int i=0; i<m; i++) {
        cini(t);
        if(t==1) {
            cini(l);
            cini(r);
            seg.rangeInc(l,r-1);
        } else if(t==2) {
            cini(k);
            cout<<seg.q1(k+1)<<endl;
        }
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
