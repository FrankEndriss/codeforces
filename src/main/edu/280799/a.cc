/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;



template<typename E>
struct SegmentTreeLazy {
    vector<E> data;
    vector<E> lazySet;
    vector<bool> lazySetB;  /* flag if there is a pending lazySet in node */

    E neutral;
    int n;

    function<E(E,E)> plus;
    function<E(E,int)> mult;

    SegmentTreeLazy(int _n, E _neutral) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2, _neutral);
        lazySet.resize(n*2, _neutral);
        lazySetB.resize(n*2);
        neutral=_neutral;
    }

    /* pushes down the pending updates to the vertex node
     * If both lazies are set then the inc was later, so both
     * have to be pushed.
     * But first the set, which erases all previously set
     * and inc on the child nodes, then the inc.
     * */
    void pushlazy(int node, int sL, int sR) {
        if(lazySetB[node]) {
            //cerr<<"pushlazy Set, node="<<node<<" sL="<<sL<<" sR="<<sR<<endl;

            if(sL!=sR) {
                int mid=(sL+sR)/2;
                rangeSet(node*2, sL, mid, sL, mid, lazySet[node]);
                rangeSet(node*2+1, mid+1, sR, mid+1, sR, lazySet[node]);
            }

            lazySetB[node]=false;
            lazySet[node]=neutral;
        }
    }

    /* @return accumulative (l,r), both inclusive, top down */
    E query(int node, int sL, int sR, int l, int r) {
        if (r < sL || l > sR)
            return neutral;
        //cerr<<"query, node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" data[node]="<<data[node]<<endl;

        if (l<=sL && r>=sR)
            return data[node];

        pushlazy(node, sL, sR);

        int mid = (sL+sR)/2;
        return plus(query(node*2, sL, mid, l, r), query(node*2+1, mid+1, sR, l, r));
    }
    E query(int l, int r) {
        return query(1, 0, n-1, l, r);
    }

    /* set all position in (l,r) to val */
    void rangeSet(int node, int sL, int sR, int l, int r, E val) {
        if (r < sL || l > sR)
            return;
        //cerr<<"rangeSet node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" val="<<val<<endl;

        if(l<=sL && r>=sR) {
            lazySet[node]=val;
            lazySetB[node]=true;
            data[node]=mult(val, sR-sL+1);
            //cerr<<"rangeSet did set node="<<data[node]<<endl;
            return;
        }

        pushlazy(node, sL, sR);

        int mid = (sL+sR)/2;
        rangeSet(node*2  , sL   , mid, l, r, val);
        rangeSet(node*2+1, mid+1, sR , l, r, val);
        data[node]=plus(data[node*2], data[node*2+1]);
    }

    /* set val at positions (l,r) */
    void rangeSet(int l, int r, E val) {
        rangeSet(1, 0, n-1, l, r, val);
    }
};

const int INF=1e18;

void solve() {
    cini(n);
    cini(m);

    struct maseg {
        int ma;     /* maximum subarray */
        int pref;   /* maximum prefix */
        int post;   /* maximum postfix */
        int sum;    /* sum of all elements in segment */
        maseg(int i=0):ma(max(0LL,i)),pref(max(0LL,i)),post(max(0LL,i)),sum(i) {
        };
    };

    maseg neutral(0);
    SegmentTreeLazy<maseg> seg(n, neutral);

    seg.plus=[](maseg i1, maseg i2) {
        maseg ans(0);
        ans.sum=i1.sum+i2.sum;
        ans.pref=max(i1.pref, i1.sum+i2.pref);
        ans.post=max(i1.post+i2.sum, i2.post);
        ans.ma=max(max(i1.ma, i2.ma), i1.post+i2.pref);
        return ans;
    };

    seg.mult=[](maseg i1, int ix) {
        maseg ans(i1.sum*ix);
        return ans;
    };

    for(int i=0; i<m; i++) {
        cini(l);
        cini(r);
        cini(v);
        //cerr<<"qu l="<<l<<" r="<<r<<" v="<<v<<endl;
        maseg val(v);
        seg.rangeSet(l,r-1,val);
        cout<<seg.query(0,n-1).ma<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
