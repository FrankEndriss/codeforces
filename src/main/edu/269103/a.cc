
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<signed, signed>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<signed>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* brute force */
void solve1() {
    cins(s);
    s+="$";

    vi id(s.size());
    iota(all(id), 0);

    sort(all(id), [&](int i1, int i2) {

        if(i1==i2)
            return false;

        for(size_t i=0; i<s.size(); i++) {
            const char c1=s[(i1+i)%s.size()];
            const char c2=s[(i2+i)%s.size()];
            if(c1!=c2)
                return c1<c2;
        }
        assert(false);
    });

    for(int i : id)
        cout<<i<<" ";
    cout<<endl;
}

/* n logn^2 */
void solve2() {
    cins(s);
    s+="$";

    const signed n=(signed)s.size();

    vi id(n);
    iota(all(id), 0);
    vi eqc(n);
    vector<pii> eqp(n);

    /* first pass, the equivalence classes are simply the same
     * as the chars at the positions.
     **/
    for(int i=0; i<n; i++)
        eqc[i]=(signed)s[i];

    /* other passes */
    for(signed len=1; len<n; len<<=1) {

        for(signed i=0; i<n; i++) {
            eqp[i].first=eqc[i];
            eqp[i].second=eqc[(i+len)%n];
        }

        /* compare the strings starting at i1/i2 by
         * comparing the current eqc of first
         * half and second half.
         * Note that it could be more efficient if we sort
         * the elements belonging to one eq-class separate.
         * ...somehow :/
         **/
        sort(all(id), [&](int i1, int i2) {
            return eqp[i1]<eqp[i2];
        });

        /* and assign the new eqc */
        eqc[id[0]]=0;
        for(signed i=1; i<n; i++) {
            if(eqp[id[i]]==eqp[id[i-1]])
                eqc[id[i]]=eqc[id[i-1]];
            else
                eqc[id[i]]=eqc[id[i-1]]+1;
        }
    }

    for(auto i : id)
        cout<<i<<" ";
    cout<<endl;
}


signed main() {
    solve2();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
