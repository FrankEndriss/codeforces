
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


template<typename E>
struct SegmentTreeLazy {
    vector<E> data;
    vector<E> lazySet;
    vector<bool> lazySetB;  /* flag if there is a pending lazySet in node */

    E neutral;
    int n;

    function<E(E,E)> plus;
    function<E(E,int)> mult;

    SegmentTreeLazy(int _n, E _neutral) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2, _neutral);
        lazySet.resize(n*2, _neutral);
        lazySetB.resize(n*2);
        neutral=_neutral;
    }

    /* bulk update in O(n) */
    template<typename Iterator>
    void init(Iterator beg, Iterator end) {
        for(int idx=n; beg!=end; beg++, idx++)
            data[idx]=*beg;

        for(int idx=n-1; idx>=1; idx--)
            data[idx]=plus(data[idx*2], data[idx*2+1]);
    }

    /* pushes down the pending updates to the vertex node 
     * If both lazies are set then the inc was later, so both
     * have to be pushed.
     * But first the set, which erases all previously set 
     * and inc on the child nodes, then the inc.
     * */
    void pushlazy(int node, int sL, int sR) {
        if(lazySetB[node]) {
            //cerr<<"pushlazy Set, node="<<node<<" sL="<<sL<<" sR="<<sR<<endl;

            if(sL!=sR) {
                int mid=(sL+sR)/2;
                rangeSet(node*2, sL   , mid, sL   , mid, lazySet[node]);
                rangeSet(node*2+1, mid+1, sR , mid+1, sR , lazySet[node]);
            }

            lazySetB[node]=false;
            lazySet[node]=neutral;
        }
    }

    /* @return accumulative (l,r), both inclusive, top down */
    E query(int node, int sL, int sR, int l, int r) {
        if (r < sL || l > sR)
            return neutral;
        //cerr<<"query, node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" data[node]="<<data[node]<<endl;

        if (l<=sL && r>=sR)
            return data[node];

        pushlazy(node, sL, sR);

        int mid = (sL+sR)/2;
        return plus(query(node*2, sL, mid, l, r), query(node*2+1, mid+1, sR, l, r));
    }
    E query(int l, int r) {
        return query(1, 0, n-1, l, r);
    }

    /* set all position in (l,r) to val */
    void rangeSet(int node, int sL, int sR, int l, int r, E val) {
        if (r < sL || l > sR)
            return;
        //cerr<<"rangeSet node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" val="<<val<<endl;

        if(l<=sL && r>=sR) {
            lazySet[node]=val;
            lazySetB[node]=true;
            data[node]=mult(val, sR-sL+1);
            //cerr<<"rangeSet did set node="<<data[node]<<endl;
            return;
        }

        pushlazy(node, sL, sR);

        int mid = (sL+sR)/2;
        rangeSet(node*2  , sL   , mid, l, r, val);
        rangeSet(node*2+1, mid+1, sR , l, r, val);
        data[node]=plus(data[node*2], data[node*2+1]);
    }

    /* set val at positions (l,r) */
    void rangeSet(int l, int r, E val) {
        rangeSet(1, 0, n-1, l, r, val);
    }

};

struct bw {
    int size;   /* size of segment */
    int cntB;   /* cnt black cells on whole segment */
    int cntS;   /* cnt black segments on whole segment */
    int pref;   /* cnt black cells on left end of segment */
    int post;   /* cnt black cells on right end of segment */
    bw(int _size=0, int _cntB=0, int _cntS=0, int _pref=0, int _post=0):size(_size),cntB(_cntB),cntS(_cntS),pref(_pref),post(_post) {
    }
};

const int N=5e5+5;
void solve() {
    cini(n);

    bw neutral(1LL,0LL,0LL,0LL,0LL);
    SegmentTreeLazy<bw> seg(N*2, neutral);

    seg.plus=[](bw sL, bw sR) {
        bw ans;

        ans.size=sL.size+sR.size;
        ans.cntB=sL.cntB+sR.cntB;

        ans.cntS=sL.cntS+sR.cntS;
        if(sL.post>0 && sR.pref>0)
            ans.cntS--;

        ans.pref=sL.pref;
        if(sL.pref==sL.size)
            ans.pref+=sR.pref;
        ans.post=sR.post;
        if(sR.post==sR.size)
            ans.post+=sL.post;
        return ans;
    };

    seg.mult=[](bw s, int ix) {
        bw ans;
        ans.size=s.size*ix;
        ans.cntB=s.cntB*ix;

        if(s.pref==s.size && s.pref>0)
            ans.cntS=1;
        else if(s.pref>0)
            ans.cntS=ix;
        else
            ans.cntS=0;

        if(s.pref==s.size)
            ans.pref=ans.size;
        else
            ans.pref=s.pref;

        if(s.post==s.size)
            ans.post=ans.size;
        else
            ans.post=s.post;
        return ans;
    };

    for(int i=0; i<n; i++) {
        char col;
        cin>>col;
        cini(l);
        l+=N;
        cini(len);
        int cnt=(col=='B');
        bw data(1,cnt,cnt,cnt,cnt);
        seg.rangeSet(l, l+len-1, data);
        bw ans=seg.query(0,2*N-1);
        cout<<ans.cntS<<" "<<ans.cntB<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
