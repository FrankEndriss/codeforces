/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


template<typename E>
struct SegmentTreeLazy {
    vector<E> data;
    vector<pii> lazyInc;    /* <valueOfFirstIncrement,incrementOfIncrement> */
    vector<bool> lazyIncB;  /* flag if there is a pending lazyInc in node */

    E neutral;
    int n;

    function<E(E,E)> plus;

    SegmentTreeLazy(int _n, E _neutral) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2, _neutral);
        lazyInc.resize(n*2, {0LL, 0LL});
        lazyIncB.resize(n*2);
        neutral=_neutral;
    }

    /* bulk update in O(n) */
    template<typename Iterator>
    void init(Iterator beg, Iterator end) {
        for(int idx=n; beg!=end; beg++, idx++)
            data[idx]=*beg;

        for(int idx=n-1; idx>=1; idx--)
            data[idx]=plus(data[idx*2], data[idx*2+1]);
    }

    void pushlazy(int node, int sL, int sR) {
        if(lazyIncB[node]) {
            //cerr<<"pushlazy Inc, node="<<node<<" sL="<<sL<<" sR="<<sR<<endl;
            if(sL!=sR) {
                int mid=(sL+sR)/2;
                rangeInc(node*2  , sL   , mid, sL   , mid, lazyInc[node]);
                rangeInc(node*2+1, mid+1, sR , mid+1, sR ,
                        { lazyInc[node].first+((mid-sL+1)*lazyInc[node].second), lazyInc[node].second});
            }
            lazyIncB[node]=false;
            lazyInc[node]={ neutral, neutral };
        }
    }

    /* @return first position > val */
    int q1(int val) {
        return q1(1, 0, n-1, val);
    }
    int q1(int node, int sL, int sR, int val) {
        cerr<<"q1 node="<<node<<" sL="<<sL<<" sR="<<sR<<" val="<<val<<endl;
        if(sL==sR) {
            assert(data[node]>val || sL==n-1);
            if(data[node]>val)
                return sL;
            else
                return n;
        }
        pushlazy(node, sL, sR);
        int mid=(sL+sR)/2;
        if(data[node*2]>val)
            return q1(node*2, sL, mid, val);
        else
            return q1(node*2+1, mid+1, sR, val);
    }

    /* @return accumulative (l,r), both inclusive, top down */
    E query(int node, int sL, int sR, int l, int r) {
        if (r < sL || l > sR)
            return neutral;
        //cerr<<"query, node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" data[node]="<<data[node]<<endl;

        if (l<=sL && r>=sR)
            return data[node];

        pushlazy(node, sL, sR);

        int mid = (sL+sR)/2;
        return plus(query(node*2, sL, mid, l, r), query(node*2+1, mid+1, sR, l, r));
    }
    E query(int l, int r) {
        return query(1, 0, n-1, l, r);
    }

    void rangeInc(int node, int sL, int sR, int l, int r, pii inc) {
        if (r < sL || l > sR)
            return;
        cerr<<"rangeInc node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<endl;

        if(l<=sL && r>=sR) {
            int first=inc.first+(sL-l)*inc.second;
            int cnt=sR-sL+1;

            lazyInc[node].first+=first;
            lazyInc[node].second+=inc.second;
            lazyIncB[node]=true;
            data[node]=data[node]+ first*cnt + (cnt*(cnt-1)*inc.second)/2;
            //cerr<<"rangeInc did set node="<<data[node]<<endl;
            return;
        }

        pushlazy(node, sL, sR);

        int mid = (sL+sR)/2;
        rangeInc(node*2  , sL   , mid, l, r, inc);
        rangeInc(node*2+1, mid+1, sR , l, r, inc);
        data[node]=plus(data[node*2], data[node*2+1]);
    }

    /* increment by inc at positions (l,r) */
    void rangeInc(int l, int r, int a, int d) {
        rangeInc(1, 0, n-1, l, r, {a, d});
    }
};

/* SegmentTreeLazy with modified range incrementation.
 * Instead of maintaining a single value not propagated so far
 * we maintain a range of values, and propagate that.
 * The inrementation range is <firstVal,incOfInc>, so we can add
 * two such incrementation ranges by simply adding the two parts.
 *
 * F:
 * It is polynomial range upates, (same as b.cc),
 * but two updates per operation.
 * The queries are binary search.
 * Segment tree operation is max.
 *
 * ... It is not rangeInc, it is rangeSet!
 **/

void solve() {
    cini(n);

    SegmentTreeLazy<int> seg(n+1, 0);
    seg.plus=[](int i1, int i2) { return max(i1,i2); };

    while(true) {
        char c; cin>>c;
        if(c=='I') {
            cini(l);
            cini(r);
            cini(d);
            int oval1=seg.query(l-1,l-1);
            int oval2=seg.query(l,l);
            int first=d-(oval2-oval1);
            //... TODO

            seg.rangeInc(l, r, first, d);
            if(r<=n)
                seg.rangeInc(r+1, n-1, d*(r-l+1), 0);
        } else if(c=='Q') {
            cini(val);
            cout<<seg.q1(val)<<endl;
        } else if(c=='E') {
            break;
        } else assert(false);
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
