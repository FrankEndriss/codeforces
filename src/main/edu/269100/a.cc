
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* brute force */
void solve1() {
    cins(s);
    s+="$";

    vi id(s.size());
    iota(all(id), 0);
    /*
    for(int i : id)
        cerr<<"i="<<i<<" ";
    cerr<<endl;
    */
    sort(all(id), [&](int i1, int i2) {
        //cerr<<"i1="<<i1<<" i2="<<i2<<endl;

        if(i1==i2)
            return false;

        for(size_t i=0; i<s.size(); i++) {
            const char c1=s[(i1+i)%s.size()];
            const char c2=s[(i2+i)%s.size()];
            if(c1!=c2)
                return c1<c2;
        }
        assert(false);
    });

    for(int i : id)
        cout<<i<<" ";
    cout<<endl;
}

/* n logn^2 */
void solve2() {
    cins(s);
    s+="$";

    const int n=s.size();
    s=s+s;  /* DEBUG */
    s=s+s;  /* DEBUG */

    vi id(n);
    iota(all(id), 0);

    vi eqc(n);

    /* first pass, the equivalence classes are simply the same
     * as the chars at the positions.
     **/
    for(int i=0; i<n; i++)
        eqc[i]=(int)s[i];

    /* other passes */
    for(int len=1; len<n; len<<=1) {
        //cerr<<"kth pass, len="<<len<<endl;

        /* compare the strings starting at i1/i2 by
         * comparing the current eqc of first
         * half and second half. */
        sort(all(id), [&](int i1, int i2) {
            //cerr<<" i1="<<i1<<" i2="<<i2<<endl;
            const pii e1= {eqc[i1], eqc[(i1+len)%n]};
            const pii e2= {eqc[i2], eqc[(i2+len)%n]};
            return e1<e2;
        });

        /* DEBUG
        cerr<<"after sort"<<endl;
        for(int i=0; i<n; i++) {
            cerr<<s.substr(id[i],len)<<" "<<s.substr(id[i]+len, len)<<"  "
                <<eqc[id[i]]<<" "<<eqc[(id[i]+len)%n]<<endl;
        }
        */

        /* and assign the new eqc */
        vi eqc2(n);
        for(int i=1; i<n; i++) {
            const pii e1= {eqc[id[i  ]], eqc[(id[i  ]+len)%n]};
            const pii e2= {eqc[id[i-1]], eqc[(id[i-1]+len)%n]};

            assert(e1>=e2);

            if(e1==e2)
                eqc2[id[i]]=eqc2[id[i-1]];
            else {
                //cerr<<"e1!=e2, e1.f="<<e1.first<<" e2.f="<<e2.first<<" e1.s="<<e1.second<<" e2.s="<<e2.second<<endl;
                eqc2[id[i]]=eqc2[id[i-1]]+1;
            }
        }
        eqc.swap(eqc2);
        //cerr<<"after asing eqc"<<endl;

    }

    sort(all(id), [&](int i1, int i2) {
        return eqc[i1]<eqc[i2];
    });


    for(auto i : id)
        cout<<i<<" ";
    cout<<endl;
}


signed main() {
    solve2();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
