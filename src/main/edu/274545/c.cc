/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

ostream& operator<<(ostream& os, pii p) {
    cout<<p.first<<" "<<p.second;
    return os;
}

/* E must support noarg constructor and operator+(E) and operator+=(E) and operator*(int)
 * Implementation:
 * We maintain lazy updates to the tree of two sorts:
 * increment and set.
 * Each time we access a node we first check if there are pending updates.
 * If yes then we first execute the pending updates.
 *
 * If we execute an update of type set we overwrite all previous made updates.
 * If we execute an update of type inc we add the increment to a previos set
 * update, or the node data.
 * */
template<typename E>
struct SegmentTree {
    vector<E> data;
    vector<E> lazyInc;
    vector<bool> lazyIncB;  /* flag if there is a pending lazyInc in node */
    vector<E> lazySet;
    vector<bool> lazySetB;  /* flag if there is a pending lazySet in node */

    E neutral;
    int n;

    function<E(E,E)> plus;
    function<E(E,int)> mult;

    SegmentTree(int _n) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2);
        lazyInc.resize(n*2);
        lazyIncB.resize(n*2);
        lazySet.resize(n*2);
        lazySetB.resize(n*2);
        neutral=data[0];
    }

    /* bulk update in O(n) */
    template<typename Iterator>
    void init(Iterator beg, Iterator end) {
        for(int idx=n; beg!=end; beg++, idx++)
            data[idx]=*beg;

        for(int idx=n-1; idx>=1; idx--)
            data[idx]=plus(data[idx*2], data[idx*2+1]);
    }

    /* pushes down the pending updates to the vertex node */
    void pushlazy(int node, int sL, int sR) {
        if(lazySetB[node]) {
            //cerr<<"pushlazy Set, node="<<node<<" sL="<<sL<<" sR="<<sR<<endl;
            data[node]=mult(lazySet[node], sR-sL+1);

            if(sL!=sR) {    /* there are no newer pending updates down the tree, so overwrite all */
                lazySetB[node*2]=true;
                lazySet[node*2]=lazySet[node];
                lazyIncB[node*2]=false;

                lazySetB[node*2+1]=false;
                lazySet[node*2+1]=lazySet[node];
                lazyIncB[node*2+1]=false;
            }

            lazySetB[node]=false;
            lazyIncB[node]=false;
        }

        if(lazyIncB[node]) {
            //cerr<<"pushlazy Inc, node="<<node<<" sL="<<sL<<" sR="<<sR<<endl;
            data[node]=plus(data[node], mult(lazyInc[node], sR-sL+1));
            if(sL!=sR) {
                if(lazySetB[node*2])
                    lazySet[node*2]=plus(lazySet[node*2], lazyInc[node]);
                else {
                    lazyInc[node*2]=plus(lazyInc[node*2], lazyInc[node]);
                    lazyIncB[node*2]=true;
                }

                if(lazySetB[node*2+1])
                    lazySet[node*2+1]=plus(lazySet[node*2+1], lazyInc[node]);
                else {
                    lazyInc[node*2+1]=plus(lazyInc[node*2+1], lazyInc[node]);
                    lazyIncB[node*2+1]=true;
                }
            }
        }
    }

    /* @return accumulative (l,r), both inclusive, top down */
    E query(int node, int sL, int sR, int l, int r) {
        if (r < sL || l > sR)
            return neutral;

        //cerr<<"query, node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" data[node]="<<data[node]<<endl;

        pushlazy(node, sL, sR);

        if (l<=sL && r>=sR)
            return data[node];

        int mid = (sL+sR)/2;
        return plus(query(node*2, sL, mid, l, r), query(node*2+1, mid+1, sR, l, r));
    }
    E query(int l, int r) {
        return query(1, 0, n-1, l, r);
    }

    /* set all position in (l,r) to val */
    void rangeSet(int node, int sL, int sR, int l, int r, E val) {
        if (r < sL || l > sR)
            return;
        //cerr<<"rangeSet node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" val="<<val<<endl;

        pushlazy(node, sL, sR);

        if(l<=sL && r>=sR) {
            lazySet[node]=val;
            lazySetB[node]=true;
            data[node]=mult(val, sR-sL+1);
            return;
        }

        int mid = (sL+sR)/2;
        rangeSet(node*2, sL, mid, l, r, val);
        rangeSet(node*2+1, mid+1, sR, l, r, val);
        data[node]=plus(data[node*2], data[node*2+1]);
    }

    /* increment all position in (l,r) by inc */
    void rangeInc(int node, int sL, int sR, int l, int r, E inc) {
        if (r < sL || l > sR)
            return;

        pushlazy(node, sL, sR);

        if(l<=sL && r>=sR) {
            lazyInc[node]=plus(lazyInc[node], inc);
            data[node]=plus(data[node], mult(inc, sR-sL+1));
            return;
        }

        int mid = (sL+sR)/2;
        rangeInc(node*2, sL, mid, l, r, inc);
        rangeInc(node*2+1, mid+1, sR, l, r, inc);
        data[node]=plus(data[node*2], data[node*2+1]);
    }

    /* accumulate x at position idx */
    void inc(int idx, E x) {
        rangeInc(1, 0, n-1, idx, idx, x);
    }

    /* set x at position idx */
    void set(int idx, E x) {
        rangeSet(1, 0, n-1, idx, idx, x);
    }
};

const int INF=1e9;

/* We maintain a fat lazy segment tree wich supports
 * both types of lazy propagation, increase and assign.
 * See https://cses.fi/book/book.pdf chapter 28 page 257ff
 *
 * We go from left to right through the array.
 *
 * The first time we encounter a number we
 * put the position into a mapping.
 *
 * The second time we mark the position of the 
 * first time in segtree with a 1.
 *
 * Then we can query the intervall, and find the 
 * sum as the numbers of other intervals wich have
 * been closed within the current interval.
 */
void solve() {
    cini(n);

    SegmentTree<int> seg(2*n);

    seg.plus=[](int i1, int i2) { return i1+i2; };
    seg.mult=[](int i1, int i2) { return i1*i2; };

    vi ans(n);
    vi pos(n, -1);
    for(int i=0; i<2*n; i++) {
        cini(a);
        a--;
        if(pos[a]<0)
            pos[a]=i;
        else {
            ans[a]=seg.query(pos[a]+1, i);
            seg.set(pos[a], 1);
        }
    }
    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;

}

signed main() {
    solve();
}
