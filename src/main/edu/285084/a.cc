
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=2e9+7;
void solve() {
    cini(n);
    cini(k);
    k++;    /* numbering starts from zero */

    vi sl(n);
    vi sr(n);
    for(int i=0; i<n; i++) {
        cin>>sl[i]>>sr[i];
    }

    int l=-INF;
    int r=INF;
    int ans=-1;
    while(l+1<r) {
        int mid=(l+r)/2;
        /* count how much smaller numbers there are, and
         * how much of mid.
         * Then calc if number at position k is smaller
         * or bigger than mid.
         */

        int lt=0;
        int eq=0;
        for(int i=0; i<n; i++) {
            if(sl[i]<mid)
                lt+=min(mid, sr[i]+1)-sl[i];

            if(sl[i]<=mid && sr[i]>=mid)
                eq++;
        }

        //cerr<<"mid="<<mid<<" lt="<<lt<<" eq="<<eq<<endl;
        if(lt+eq<k) {
            l=mid;
        } else {
            if(eq>0)
                ans=mid;
            r=mid;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
