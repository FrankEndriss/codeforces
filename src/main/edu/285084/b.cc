
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We search smallest number with 
 * k+1 lteq table entries.
 */
void solve() {
    cini(n);
    cini(k);

    int l=0;
    int r=n*n;

    while(l+1<r) {
        int mid=(l+r)/2;

        /* find number of pairs with product lteq mid */
        int cnt=0;
        for(int i=1; i<n; i++) {
            int div=mid/i;
            cnt+=div;
        }

        cerr<<"mid="<<mid<<" cnt="<<cnt<<endl;
        if(cnt<k)
            l=mid;
        else
            r=mid;
    }
    cout<<r<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
