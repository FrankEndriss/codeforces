
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e9+3;
/* binsearch for minimum max(edgevalue).
 * On each mid do bfs maintaining max edgevalue
 * in dp, and parent array.
 */
void solve() {
    cini(n);
    cini(m);
    cini(d);

    vector<vector<pii>> adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        cini(c);
        adj[u].emplace_back(v, c);
    }

    vi ans;

    int l=0;
    int r=1e9;
    while(l+1<r) {
        int mid=(l+r)/2;

        vi dp(n, INF);
        vi p(n, -1);
        dp[0]=0;
        queue<int> q;
        q.push(0);

        while(q.size()) {
            int v=q.front();
            if(dp[v]>d)
                break;
            q.pop();
            for(pii c : adj[v]) {
                if(c.second<=mid && dp[c.first]>dp[v]+1) {
                    dp[c.first]=dp[v]+1;
                    p[c.first]=v;
                    if(c.first==n-1)
                        break;
                    q.push(c.first);
                }
            }
        }

        if(p[n-1]>=0 && dp[n-1]<=d) {
            ans.clear();
            int v=n-1;
            while(v>=0) {
                ans.push_back(v);
                v=p[v];
            }
            r=mid;
        } else
            l=mid;
    }

    if(ans.size()<2)
        cout<<-1<<endl;
    else {
        cout<<ans.size()-1<<endl;
        while(ans.size())  {
            cout<<ans.back()+1<<" ";
            ans.pop_back();
        }
        cout<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
