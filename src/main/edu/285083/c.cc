
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e9+3;
/* binsearch for minimum distance.
 * On each try to put all cows into stalls
 * keeping mid distance.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    int l=0;
    int r=1e9;
    while(l+1<r) {
        int mid=(l+r)/2;

        int cnt=1;
        int prev=a[0];
        for(int i=1; i<n; i++) {
            if(a[i]-prev>=mid) {
                cnt++;
                prev=a[i];
            }
        }

        //cerr<<"mid="<<mid<<" cnt="<<cnt<<endl;
        if(cnt>=k)
            l=mid;
        else
            r=mid;
    }
    cout<<l<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
