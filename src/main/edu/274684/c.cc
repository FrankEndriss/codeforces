
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


#include <atcoder/segtree>
using namespace atcoder;

const int N=41;

/* freq of elements, and number of inversions in segment. */
struct S {
    int f[N]={ 0 };
    //vector<int> f;
    ll inv=0;
};

S st_op(S a, S b) {

    /* count the new inversion that are created from left to right */
    ll ninv=0;
    ll sumR=b.f[0];
    for(int j=1; j<N; j++)  {
        ninv+=a.f[j]*sumR;
        sumR+=b.f[j];
    }

    for(int i=0; i<N; i++)
        a.f[i]+=b.f[i];

    a.inv=a.inv+b.inv+ninv;

    return a;
}

S st_e() {
    S a;
    return a;
}

using stree=segtree<S, st_op, st_e>;

/*
 * Maintain freq of elements and number of inversion per segment
 * in a segment tree.
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    vector<S> data(n);

    for(int i=0; i<n; i++)
        data[i].f[a[i]]++;

    stree seg(data);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(x); x--;
            cini(y); y--;

            S s=seg.prod(x,y+1);
            cout<<s.inv<<endl;

        } else if(t==2) {
            cini(x); x--;
            cini(y);
            S aa;
            aa.f[y]=1;

            seg.set(x,aa);
        }
    }

}

signed main() {
    solve();
}
