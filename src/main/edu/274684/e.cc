
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * This looks like an lazy segment tree.
 * What to maintain, how?
 * Assume there are no initial buildings. Then we need to know
 * Can we simply maintain the freq of hights per segment?
 * Then an earthquake removes all hights<p.
 * And the update increases one hight by one.
 *
 * Current version does not work, TLE.
 * Maintaining the freq of hights per segment seems to be like O(n^2)
 *
 * ***
 * Can we take advantage of reordering the queries?
 * It is that each building has a life-span, from beeing build until
 * destroyed.
 * So, each earthquake destroys all buildings in area and life-span
 * active at event time of earthquake.
 *
 * ***
 * How to take advantage of compressing the hights?
 * Note that each building is build and destroyed only once.
 * What about a simple 2D-map? first dim is hight, second is position.
 * Then, on each operation, we do what is written in statement.
 * No, we would have to go throug the lists, most likely without
 * removeing buildings because they are in another segment.
 * So it would be O(n^2)
 */
void solve() {
    cini(n);
    cini(m);

    for(int i=0; i<m; i++) {
        cini(t);
        if(t==1) {
            cini(idx);
            cini(h);
            S d(1,h);
            seg.set(idx,d);
        } else if(t==2) {
            cini(l);
            cini(r);
            cini(p);
            S ans=seg.prod(l,r);
            int sum=0;
            for(auto ent : ans) {
                if(ent<=p)
                    sum++;
            }

            seg.apply(l,r,p);
            cout<<sum<<endl;
        } else
            assert(false);
    }
}


signed main() {
    solve();
}
