/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* 
 * We have sum(a[i]) students, and want 
 * to make x concils of students, where
 * each concil has k members, and
 * each student of a concil originates from different group.
 *
 * How to check if x is possible or not?
 * Obviously if x*k>sum(a[i]) it is not possible.
 * Then we collect x times k student from 
 * biggest to smallest a[i].
 * That might be possible or not.
 */
void solve() {
    cins(s);
    cini(nb);
    cini(ns);
    cini(nc);
    cini(pb);
    cini(ps);
    cini(pc);
    cini(money);

    vi a(3);
    for(char c : s) {
        if(c=='B')
            a[0]++;
        else if(c=='S')
            a[1]++;
        else if(c=='C')
            a[2]++;
        else assert(false);
    }

    int l=0; 
    int r=1e13+3;

    while(l+1<r) {
        int mid=(l+r)/2;
        //cerr<<"l="<<l<<" r="<<r<<" mid="<<mid<<endl;

        int p0=max(0LL, ((a[0]*mid)-nb)*pb);
        int p1=max(0LL, ((a[1]*mid)-ns)*ps);
        int p2=max(0LL, ((a[2]*mid)-nc)*pc);

        if(p0+p1+p2<=money)
            l=mid;
        else
            r=mid;
    }
    cout<<l<<endl;
}

signed main() {
    solve();
}

