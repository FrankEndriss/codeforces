/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* 
 * We have sum(a[i]) students, and want 
 * to make x concils of students, where
 * each concil has k members, and
 * each student of a concil originates from different group.
 *
 * How to check if x is possible or not?
 * Obviously if x*k>sum(a[i]) it is not possible.
 * Then we collect x times k student from 
 * biggest to smallest a[i].
 * That might be possible or not.
 */
void solve() {
    cini(k);
    cini(n);
    cinai(a,n);

    int l=0; 
    int r=1e16;

    while(l+1<r) {
        int mid=(l+r)/2;
        //cerr<<"l="<<l<<" r="<<r<<" mid="<<mid<<endl;

        int cnt=0;
        for(int i=0; i<n; i++) 
            cnt+=min(mid, a[i]);

        if(cnt>=mid*k)
            l=mid;
        else
            r=mid;

    }
    cout<<l<<endl;
}

signed main() {
    solve();
}

