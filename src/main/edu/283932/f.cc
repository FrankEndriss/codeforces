/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* binsearch a number.
 * On each number do check in O(t.size())
 * p can be created from t if p exist
 * as a subsequence in t.
 */
void solve() {
    cins(t);
    cins(p);
    cinai(a,t.size());

    vector<size_t> pos(t.size());
    for(size_t i=0; i<t.size(); i++)
        pos[a[i]-1]=i;
    
    size_t l=0; 
    size_t r=t.size()+1;

    while(l+1<r) {
        size_t mid=(l+r)/2;
        //cerr<<"l="<<l<<" r="<<r<<" mid="<<mid<<endl;

        size_t ti=0;
        bool ok=true;
        for(size_t pi=0; ok && pi<p.size(); pi++) {
            while(ti<t.size() && (pos[ti]<mid || t[ti]!=p[pi]))
                ti++;

            if(ti==t.size())
                ok=false;
            ti++;
        }

        if(ok)
            l=mid;
        else
            r=mid;

    }
    cout<<l<<endl;
}

signed main() {
    solve();
}

