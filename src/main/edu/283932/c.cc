/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* n ropes, we need to create k pieces.
 * find max possible length of pieces.
 */
void solve() {
    cini(n);
    cini(x);
    cini(y);

    if(x>y)
        swap(x,y);

    int l=-1; 
    int r=1e10;

    while(l+1<r) {
        int mid=(l+r)/2;
        //cerr<<"l="<<l<<" r="<<r<<" mid="<<mid<<endl;
        int cp1=mid/x;
        int cp2=max(0LL, mid-x)/y;
        if(cp1+cp2<n)
            l=mid;
        else
            r=mid;

    }
    cout<<r<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
