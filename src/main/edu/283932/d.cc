/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* m ballons
 * binsearch on number of minutes.
 */
void solve() {
    cini(m);
    cini(n);

    vi t(n);
    vi z(n);
    vi y(n);
    for(int i=0; i<n; i++)
        cin>>t[i]>>z[i]>>y[i];

    /* max number of ballons assi i can inflate in mi minutes */
    auto infl=[&](int i, int mi)->int {
        int cycl=z[i]*t[i]+y[i];
        int cyclCnt=mi/cycl;
        int after=mi%cycl;
        if(after<t[i]) {
            if(cyclCnt>0) {
                cyclCnt--;
                after+=cycl;
            }
        }
        int ans=cyclCnt*z[i];
        ans+=min(after/t[i], z[i]);
        return ans;
    };

    int l=-1; 
    int r=1e10;

    while(l+1<r) {
        int mid=(l+r)/2;
        //cerr<<"l="<<l<<" r="<<r<<" mid="<<mid<<endl;

        int cnt=0;  /* number of inflated ballons */
        for(int i=0; i<n; i++)
            cnt+=infl(i, mid);

        if(cnt<m)
            l=mid;
        else
            r=mid;

    }
    cout<<r<<endl;

    for(int i=0; i<n; i++) {
        int cnt=infl(i,r);
        cout<<min(m,cnt)<<" ";
        m-=cnt;
    }
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
