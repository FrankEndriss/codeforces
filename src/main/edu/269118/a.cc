
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<signed, signed>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<signed>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* n logn^2 */
void solve2() {
    cins(s);
    s+="$";

    const signed n=(signed)s.size();

    vi id(n);
    iota(all(id), 0);
    vi eqc(n);
    vector<pii> eqp(n);

    /* first pass, the equivalence classes are simply the same
     * as the chars at the positions.
     **/
    for(int i=0; i<n; i++)
        eqc[i]=(signed)s[i];

    /* other passes */
    for(signed len=1; len<n; len<<=1) {

        for(signed i=0; i<n; i++) {
            eqp[i].first=eqc[i];
            eqp[i].second=eqc[(i+len)%n];
        }

        /* compare the strings starting at i1/i2 by
         * comparing the current eqc of first
         * half and second half.
         * Note that it could be more efficient if we sort
         * the elements belonging to one eq-class separate.
         * ...somehow :/
         **/
        sort(all(id), [&](int i1, int i2) {
            return eqp[i1]<eqp[i2];
        });

        /* and assign the new eqc */
        eqc[id[0]]=0;
        for(signed i=1; i<n; i++) {
            if(eqp[id[i]]==eqp[id[i-1]])
                eqc[id[i]]=eqc[id[i-1]];
            else
                eqc[id[i]]=eqc[id[i-1]]+1;
        }
    }

    cini(k);
    for(int i=0; i<k; i++) {
        cins(t);

        /* find any suffix string eq t by binsearch first one bgeq t */
        int l=0;
        int r=n;
        bool hit=false;
        while(!hit && l+1<r) {
            int mid=(l+r)/2;
            hit=true;
            for(size_t j=0; j<t.size(); j++) {
                if(t[j]<s[(j+id[mid])%n]) {
                    r=mid;
                    hit=false;
                    break;
                } else if(t[j]>s[(j+id[mid])%n]) {
                    l=mid;
                    hit=false;
                    break;
                }
            }
            if(hit)
                break;
        }
        if(hit)
            cout<<"Yes"<<endl;
        else
            cout<<"No"<<endl;
    }
}


signed main() {
    solve2();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
