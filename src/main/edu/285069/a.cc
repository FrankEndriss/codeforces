
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* binsearch for max average value.
 * foreach check if such segment exists.
 * let b[] be prefix sums.
 * (p[r+1]-p[l])/(r+1-l) >=mid
 * (p[r+1]-p[l]>=mid*(r+1-l)
 * -> subtract mid from both sides.
 *  p[r+1]-p[l]>=0
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(d);
    cinai(a,n);

    int ansL=-1;
    int ansR=-1;

    double l=0; 
    double r=1e7+7;

    while(l+.0000001<r) {
        double mid=(l+r)/2;

        vd pre(n+1);
        //pre[0]=(double)a[0]-mid;
        vector<pair<double,int>> mi(n+1, {(double)INF, -1});
        mi[0]={0.0,0};

        for(int i=1; i<=n; i++) {
            pre[i]=pre[i-1]+(double)a[i-1]-mid;
            mi[i]=min(mi[i-1], {pre[i],i});
        }

        int ok=false;
        for(int i=d; i<=n; i++) 
            if(mi[i-d].first<=pre[i]) {
                ok=true;
                ansL=mi[i-d].second;
                ansR=i-1;
            }

        if(ok)
            l=mid;
        else 
            r=mid;
    }

    //cerr<<"max mean="<<l<<endl;
    cout<<ansL+1<<" "<<ansR+1<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
