
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * For all paths of len x we need to check
 * the one with smallest avg mean.
 * But how comes BS into play?
 * How to construct all paths?
 *
 * BS for min mean?
 * We would need to check if there exists a path
 * with mean smaller mid in O(n)
 * -> Impossible
 *
 **********
 * Can we construct all paths (and min means) for
 * all pathlenths?
 * Foreach pathlen there cannot be more than n endpoints,
 * and on average much less.
 * Try simply bfs all paths.
 * How to trackback the path?
 * ... We would need to find the min mean in each step...
 * That does not work out :/
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    vector<vector<pii>> adj(n);
    for(int i=0; i<n; i++) {
        cini(u); u--;
        cini(v); v--;
        cini(c);
        adj[u].emplace_back(v,c);
    }

    struct qent {
        int v, len, sum;
        qent(int _v, int _len, int _sum):v(_v), len(_len), sum(_sum) {
        }
    };
    queue<qent> q;
    q.push({0,0,0});

    vector<pii> ans;

    vi dp(n+1, INF); /* dp[i] = min sum for path of len i */
    dp[0]=0;

    while(q.size()) {
        qent cur=q.front();
        q.pop();
        if(cur.sum!=dp[cur.len])
            continue;

        if(cur.v==n-1) {
            if(ans.size()>0 && ans.back().first==cur.len && ans.back().second<cur.sum)
                ans.back().second=cur.sum;
            else
                ans.emplace_back(cur.len, cur.sum);

            continue;
        }

        for(auto chl : adj[cur.v]) {
            if(cur.sum+chl.second<dp[chl.first]) {
                dp[chl.first]=cur.sum+chl.second;
                q.emplace(chl.first, cur.len+1, cur.sum+chl.second);
            }
        }
    }

    for(size_t i=0; i<ans.size(); i++) {
        cerr<<"len="<<ans[i].first<<" sum="<<ans[i].second<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
