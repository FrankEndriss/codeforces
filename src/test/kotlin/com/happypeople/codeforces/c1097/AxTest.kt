package com.happypeople.codeforces.c1097

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class AxTest {

    @Before
    fun setUp() {
        Ax.printLog=true
    }

    @Test
    fun run() {
        Ax.inputStr="AS\n" +
                "2H 4C TH JH AD\n"
        Ax().run()
    }

    @Test
    fun run2() {
        Ax.inputStr="2H\n" +
                "3D 4C AC KD AS\n"
        Ax().run()
    }
}