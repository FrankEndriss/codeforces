package com.happypeople.codeforces.c1097

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="3\n" +
                "10\n" +
                "20\n" +
                "30\n"
        B().run()
    }

    @Test
    fun run2() {
        B.inputStr="3\n" +
                "10\n" +
                "10\n" +
                "10\n"
        B().run()
    }
}