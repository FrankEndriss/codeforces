package com.happypeople.codeforces.c1097

import org.junit.Before
import org.junit.Test

class CTest {

    @Before
    fun setUp() {
        C.printLog = true
    }

    @Test
    fun run() {
        C.inputStr = "7\n" +
                ")())\n" +
                ")\n" +
                "((\n" +
                "((\n" +
                "(\n" +
                ")\n" +
                ")\n"
        C().run()
        // out: 2
    }

    @Test
    fun run2() {
        C.inputStr = "4\n" +
                "(\n" +
                "((\n" +
                "(((\n" +
                "(())"
        C().run()
    }

}