package com.happypeople.codeforces.c1100

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class FTest {

    @Before
    fun setUp() {
        F.printLog=true
    }

    @Test
    fun run() {
        F.inputStr="4\n" +
                "7 2 3 4\n" +
                "3\n" +
                "1 4\n" +
                "2 3\n" +
                "1 3"
        F().run()
    }

    @Test
    fun run2() {
        F.inputStr="5\n" +
                "12 14 23 13 7\n" +
                "15\n" +
                "1 1\n" +
                "1 2\n" +
                "1 3\n" +
                "1 4\n" +
                "1 5\n" +
                "2 2\n" +
                "2 3\n" +
                "2 4\n" +
                "2 5\n" +
                "3 3\n" +
                "3 4\n" +
                "3 5\n" +
                "4 4\n" +
                "4 5\n" +
                "5 5\n"
        F().run()
    }
}