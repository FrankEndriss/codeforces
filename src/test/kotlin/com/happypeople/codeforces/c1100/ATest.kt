package com.happypeople.codeforces.c1100

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ATest {

    @Before
    fun setUp() {
        A.printLog=true
    }

    @Test
    fun run() {
        A.inputStr="14 3\n" +
                "-1 1 -1 -1 1 -1 -1 1 -1 -1 1 -1 -1 1"
        A().run()
    }

    @Test
    fun run2() {
        A.inputStr="4 2\n" +
                "1 1 -1 1"
        A().run()
    }
}