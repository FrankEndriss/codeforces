package com.happypeople.codeforces.c1100

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="3 11\n" +
                "2 3 1 2 2 2 3 2 2 3 1"
        B().run()
    }

    @Test
    fun run2() {
        B.inputStr="4 8\n" +
                "4 1 3 3 2 3 3 3\n"
        B().run()
    }
}