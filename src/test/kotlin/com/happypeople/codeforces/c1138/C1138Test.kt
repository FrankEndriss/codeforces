package com.happypeople.codeforces.c1138

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test
import java.lang.Math.random
import java.util.*

class C1138Test {

    @Before
    fun setUp() {
        C1138.printLog=true
    }

    @Test
    fun run() {
        C1138.inputStr="2 3\n" +
                "1 2 1\n" +
                "2 1 2"
        C1138().run()
    }

    @Test
    fun run2() {
        C1138.inputStr="2 2\n" +
                "1 2\n" +
                "3 4"
        C1138().run()
    }

    @Test
    fun run4() {
        val ra= Random(43L)
        val n=5
        val m=5
        val sb=StringBuffer("$n $m\n")
        for(i in 1..n) {
            for (j in 1..m) {
                val r=(10+ra.nextDouble()*10).toInt()
                sb.append("$r ")
            }
            sb.append("\n")
        }
        println(sb)

        C1138.inputStr=sb.toString()
        C1138().run()
    }

    @Test
    fun run3() {
        val sb=StringBuffer("1000 1000\n")
        var r=0
        for(i in 1..1000) {
            for (j in 1..1000) {
                r=(random()*10000).toInt()
                sb.append(" " + r)
            }
            sb.append("\n")
        }
        C1138.inputStr=sb.toString()
        val start=System.currentTimeMillis()
        C1138().run()
        val needed=System.currentTimeMillis()-start
        println("needed: $needed ms")
    }
}