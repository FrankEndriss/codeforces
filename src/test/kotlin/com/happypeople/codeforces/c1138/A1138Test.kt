package com.happypeople.codeforces.c1138

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1138Test {

    @Before
    fun setUp() {
        A1138.printLog=true
    }

    @Test
    fun run() {
        A1138.inputStr="7\n" +
                "2 2 2 1 1 2 2"
        A1138().run()
    }

    @Test
    fun run2() {
        A1138.inputStr="6\n" +
                "1 2 1 2 1 2"
        A1138().run()
    }

    @Test
    fun run3() {
        A1138.inputStr="9\n" +
                "2 2 1 1 1 2 2 2 2"
        A1138().run()
    }
}

