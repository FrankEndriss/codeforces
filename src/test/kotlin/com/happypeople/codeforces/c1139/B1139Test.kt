package com.happypeople.codeforces.c1139

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1139Test {

    @Before
    fun setUp() {
        B1139.printLog=true
    }

    @Test
    fun run() {
        B1139.inputStr="5\n" +
                "1 2 1 3 6"
        B1139().run()
    }

    @Test
    fun run2() {
        B1139.inputStr="5\n" +
                "3 2 5 4 10"
        B1139().run()
    }

    @Test
    fun run3() {
        B1139.inputStr="4\n" +
                "1 1 1 1"
        B1139().run()
    }
}