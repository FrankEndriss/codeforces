package com.happypeople.codeforces.c1139

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1139Test {

    @Before
    fun setUp() {
        A1139.printLog=true
    }

    @Test
    fun run() {
        A1139.inputStr="4\n" +
                "1234"
        A1139().run()
    }

    @Test
    fun run2() {
        A1139.inputStr="4\n" +
                "2244"
        A1139().run()
    }
}