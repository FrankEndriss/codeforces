package com.happypeople.codeforces.c1139

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1139Test {

    @Before
    fun setUp() {
        C1139.printLog=true
    }

    @Test
    fun run() {
        C1139.inputStr="4 4\n" +
                "1 2 1\n" +
                "2 3 1\n" +
                "3 4 1"
        C1139().run()
    }

    @Test
    fun run2() {
        C1139.inputStr="4 6\n" +
                "1 2 0\n" +
                "1 3 0\n" +
                "1 4 0"
        C1139().run()
    }

    @Test
    fun run3() {
        C1139.inputStr="3 5\n" +
                "1 2 1\n" +
                "2 3 0"
        C1139().run()
    }
}