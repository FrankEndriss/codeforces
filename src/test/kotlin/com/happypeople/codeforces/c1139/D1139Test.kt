package com.happypeople.codeforces.c1139

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1139Test {

    @Before
    fun setUp() {
        D1139.printLog=true
    }

    @Test
    fun run() {
        D1139.inputStr="42"
        D1139().run()
    }
}