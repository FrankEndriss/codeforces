package com.happypeople.codeforces.c1136

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1136Test {

    @Before
    fun setUp() {
        C1136.printLog=true
    }

    @Test
    fun run4() {
        C1136.inputStr="3 2\n"+
                "1 2\n" +
                "3 4\n" +
                "5 6\n" +
                "1 2\n" +
                "3 5\n" +
                "4 6\n"
        C1136().run()
    }
    @Test
    fun run3() {
        C1136.inputStr="2 3\n"+
                "1 2 3\n" +
                "4 5 6\n" +
                "1 2 3\n" +
                "4 5 6\n"
        C1136().run()
    }

    @Test
    fun run() {
        C1136.inputStr="3 3\n" +
                "1 2 3\n" +
                "4 5 6\n" +
                "7 8 9\n" +
                "1 4 7\n" +
                "2 5 6\n" +
                "3 8 9"
        C1136().run()
    }

    @Test
    fun run2() {
        C1136.inputStr="2 2\n" +
                "4 4\n" +
                "4 5\n" +
                "5 4\n" +
                "4 4"
        C1136().run()
    }
}