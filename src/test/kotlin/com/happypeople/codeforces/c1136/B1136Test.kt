package com.happypeople.codeforces.c1136

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1136Test {

    @Before
    fun setUp() {
        B1136.printLog=true
    }

    @Test
    fun run() {
        B1136.inputStr="4 3\n"
        B1136().run()
    }
}