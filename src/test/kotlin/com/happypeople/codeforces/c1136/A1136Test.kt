package com.happypeople.codeforces.c1136

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1136Test {

    @Before
    fun setUp() {
        A1136.printLog=true
    }

    @Test
    fun run() {
        A1136.inputStr="3\n" +
                "1 3\n" +
                "4 7\n" +
                "8 11\n" +
                "4"
        A1136().run()
    }
}