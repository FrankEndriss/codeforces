package com.happypeople.codeforces.c1170

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1170Test {

    @Before
    fun setUp() {
        C1170.printLog=true
    }

    @Test
    fun run() {
        C1170.inputStr="5\n" +
                "-+--+\n" +
                "-+++\n" +
                "--------\n" +
                "-+--+-\n" +
                "-\n" +
                "+\n" +
                "--\n" +
                "---\n" +
                "+++\n" +
                "+++\n"
        C1170().run()
    }
}