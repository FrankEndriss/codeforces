package com.happypeople.codeforces.c1170
import org.junit.Assert.*;

import com.happypeople.codeforces.c1170.A;
import org.junit.Before
import org.junit.Test


class A1170Test {

    @Before
    fun setUp() {
        A.printLog=true
    }

    @Test
    fun run() {

        A.inputStr="3\n"+
        "123 13\n"+
        "2 2\n"+
        "2000000000 2000000000";

        A().run()
    }
}