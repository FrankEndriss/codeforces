package com.happypeople.codeforces.c1170

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1170Test {

    @Before
    fun setUp() {
        B1170.printLog=true
    }

    @Test
    fun run() {
        B1170.inputStr="8\n" +
                "3 1 4 1 5 9 2 6"
        B1170().run()
    }
}