package com.happypeople.codeforces.c1170

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class F1170Test {

    @Before
    fun setUp() {
        F1170.printLog=true
    }

    @Test
    fun run() {
        F1170.inputStr="6 6 7\n" +
                "6 15 16 20 1 5"
        F1170().run()
    }

    @Test
    fun run2() {
        F1170.inputStr="6 3 1\n" +
                "4 8 15 16 23 42"
        F1170().run()
    }
}