package com.happypeople.codeforces.c1170

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1170Test {

    @Before
    fun setUp() {
        D1170.printLog=true
    }

    @Test
    fun run() {
        D1170.inputStr="12\n" +
                "3 2 1 1 7 2 4 -1 3 -1 4 -1"
        D1170().run()
    }
}