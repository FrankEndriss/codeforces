package com.happypeople.codeforces.c1102

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1102Test {

    @Before
    fun setUp() {
        D1102.printLog=true
    }

    @Test
    fun run() {
        D1102.inputStr="6\n" +
                "120110\n"
        D1102().run()
    }
}