package com.happypeople.codeforces.c1102

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="4 2\n" +
                "1 2 2 3"
        B().run()
    }

    @Test
    fun run2() {
        B.inputStr="5 4\n" +
                "3 2 1 2 3"
        B().run()
    }

    @Test
    fun run3() {
        B.inputStr="5 5\n" +
                "4 2 2 2 4"
        B().run()
    }
}