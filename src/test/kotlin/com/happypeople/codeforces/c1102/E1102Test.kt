package com.happypeople.codeforces.c1102

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class E1102Test {

    @Before
    fun setUp() {
        E1102.printLog=true
    }

    @Test
    fun run() {
        E1102.inputStr="5\n" +
                "1 2 1 2 3\n"
        E1102().run()
    }

    @Test
    fun run2() {
        E1102.inputStr="4\n" +
                "1 3 3 7"
        E1102().run()
    }
}