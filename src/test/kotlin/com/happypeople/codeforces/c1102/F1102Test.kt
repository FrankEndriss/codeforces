package com.happypeople.codeforces.c1102

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class F1102Test {

    @Before
    fun setUp() {
        F1102.printLog=true
    }

    @Test
    fun run() {
        F1102.inputStr="4 2\n" +
                "9 9\n" +
                "10 8\n" +
                "5 3\n" +
                "4 3\n"
        F1102().run()
    }
}