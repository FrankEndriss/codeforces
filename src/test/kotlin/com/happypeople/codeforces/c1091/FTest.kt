package com.happypeople.codeforces.c1091

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class FTest {

    @Before
    fun setUp() {
        F.printLog=true
    }

    @Test
    fun run1() {
        F.inputStr="1\n"+
        "10\n"+
        "G\n"
        F().run()
    }
}