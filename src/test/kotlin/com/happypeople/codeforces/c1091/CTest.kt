package com.happypeople.codeforces.c1091

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class CTest {

    @Before
    fun setUp() {
        C.printLog=true
    }

    @Test
    fun run() {
        C.inputStr="16\n"
        C().run()
    }
}