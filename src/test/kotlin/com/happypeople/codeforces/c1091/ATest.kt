package com.happypeople.codeforces.c1091

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class ATest {

    @Before
    fun setUp() {
        A.printLog=true
    }

    @Test
    fun run() {
        A.inputStr="8 13 9\n"
        A().run()
    }
}