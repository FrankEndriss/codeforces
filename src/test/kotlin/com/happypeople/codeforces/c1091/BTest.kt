package com.happypeople.codeforces.c1091

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="2\n"+
                "2 5\n"+
                "-6 4\n"+
                "7 -2\n"+
                "-1 -3\n"
        B().run()
    }

    @Test
    fun run1() {
        B.inputStr="4\n"+
        "2 2\n"+
        "8 2\n"+
        "-7 0\n"+
        "-2 6\n"+
        "1 -14\n"+
        "16 -12\n"+
        "11 -18\n"+
        "7 -14\n"
        B().run()
    }
}