package com.happypeople.codeforces.c1056

import org.junit.Test

class CTest {
    @Test
    fun testRun1() {
        C.inputStr = "3 1\n" +
                "1 2 3 4 5 6\n" +
                "2 6\n" +
                "1\n" +
                "2\n" +
                "4\n" +
                "1\n"
        C().run()
    }

    @Test
    fun testRun2() {
        C.inputStr = "3 1\n" +
                "1 2 3 4 5 6\n" +
                "1 5\n" +
                "2\n" +
                "6\n" +
                "1\n" +
                "3\n"
        C().run()
    }
}