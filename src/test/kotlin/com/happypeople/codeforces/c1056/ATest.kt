package com.happypeople.codeforces.c1056

import org.junit.Test

class ATest {

    @Test
    fun testRun() {
        A.inputStr = "3\n" +
                "3 1 4 6\n" +
                "2 1 4\n" +
                "5 10 5 6 4 1\n"

        A().run()
    }
}