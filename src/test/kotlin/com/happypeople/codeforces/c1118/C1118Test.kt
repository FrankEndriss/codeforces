package com.happypeople.codeforces.c1118

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1118Test {

    @Before
    fun setUp() {
        C1118.printLog=true
    }

    @Test
    fun run() {
        C1118.inputStr="4\n" +
                "1 8 8 1 2 2 2 2 2 2 2 2 1 8 8 1\n"
        C1118().run()
    }

    @Test
    fun run2() {
        C1118.inputStr="3\n" +
                "1 1 1 1 1 3 3 3 3"
        C1118().run()
    }
}