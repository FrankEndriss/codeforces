package com.happypeople.codeforces.c1118

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1118Test {

    @Before
    fun setUp() {
        A1118.printLog=true
    }

    @Test
    fun run() {
        A1118.inputStr="4\n" +
                "10 1 3\n" +
                "7 3 2\n" +
                "1 1000 1\n" +
                "1000000000000 42 88"
        A1118().run()
    }
}