package com.happypeople.codeforces.c1118

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1118Test {

    @Before
    fun setUp() {
        B1118.printLog=true
    }

    @Test
    fun run() {
        B1118.inputStr="9\n" +
                "2 3 4 2 2 3 2 2 4"
        B1118().run()
    }
}