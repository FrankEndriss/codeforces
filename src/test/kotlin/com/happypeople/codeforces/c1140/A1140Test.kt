package com.happypeople.codeforces.c1140

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1140Test {

    @Before
    fun setUp() {
        A1140.printLog=true
    }

    @Test
    fun run() {
        A1140.inputStr="9\n" +
                "1 3 3 6 7 6 8 8 9"
        A1140().run()
    }
}