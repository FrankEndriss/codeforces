package com.happypeople.codeforces.c1140

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1140Test {

    @Before
    fun setUp() {
        D1140.printLog=true
    }

    @Test
    fun run() {
        D1140.inputStr="3"
        D1140().run()
    }

    @Test
    fun run2() {
        D1140.inputStr="4"
        D1140().run()
    }
}