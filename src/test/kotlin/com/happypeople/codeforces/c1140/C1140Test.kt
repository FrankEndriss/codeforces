package com.happypeople.codeforces.c1140

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1140Test {

    @Before
    fun setUp() {
        C1140.printLog=true
    }

    @Test
    fun run() {
        C1140.inputStr="4 3\n" +
                "4 7\n" +
                "15 1\n" +
                "3 6\n" +
                "6 8"
        C1140().run()
    }

    @Test
    fun run2() {
        C1140.inputStr="5 3\n" +
                "12 31\n" +
                "112 4\n" +
                "100 100\n" +
                "13 55\n" +
                "55 50"
        C1140().run()
    }
}