package com.happypeople.codeforces.c1140

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1140Test {

    @Before
    fun setUp() {
        B1140.printLog = true
    }

    @Test
    fun run() {
        B1140.inputStr = "4\n" +
                "2\n" +
                "<>\n" +
                "3\n" +
                "><<\n" +
                "1\n" +
                ">\n" +
                "9\n" +
                "<<<<><<>>"

        B1140().run()
    }
}