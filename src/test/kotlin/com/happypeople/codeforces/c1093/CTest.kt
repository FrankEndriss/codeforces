package com.happypeople.codeforces.c1093

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class CTest {

    @Before
    fun setUp() {
        C.printLog=true
    }

    @Test
    fun run() {
        C.inputStr="6\n" +
                "2 1 2"
        C().run()
    }

    @Test
    fun run2() {
        C.inputStr="4\n" +
                "5 6"
        C().run()
    }

    @Test
    fun run5() {
        C.inputStr="12\n" +
                "4 5 4 4 3 4\n"
        C().run()
    }
}