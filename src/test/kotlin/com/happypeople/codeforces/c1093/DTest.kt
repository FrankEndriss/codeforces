package com.happypeople.codeforces.c1093

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class DTest {

    @Before
    fun setUp() {
        D.printLog=true
    }

    @Test
    fun run() {
        D.inputStr="2\n" +
                "2 1\n" +
                "1 2\n" +
                "4 6\n" +
                "1 2\n" +
                "1 3\n" +
                "1 4\n" +
                "2 3\n" +
                "2 4\n" +
                "3 4"
        D().run()
    }

    @Test
    fun run2() {
        D.inputStr="12\n" +
                "8 7\n" +
                "2 3\n" +
                "3 4\n" +
                "4 5\n" +
                "5 2\n" +
                "6 7\n" +
                "7 8\n" +
                "8 6\n" +
                "1 0\n" +
                "2 1\n" +
                "1 2\n" +
                "3 3\n" +
                "1 2\n" +
                "2 3\n" +
                "1 3\n" +
                "3 2\n" +
                "2 3\n" +
                "3 1\n" +
                "4 4\n" +
                "1 2\n" +
                "2 3\n" +
                "3 4\n" +
                "4 1\n" +
                "4 4\n" +
                "1 2\n" +
                "2 3\n" +
                "3 1\n" +
                "4 1\n" +
                "6 9\n" +
                "1 4\n" +
                "1 5\n" +
                "1 6\n" +
                "2 4\n" +
                "2 5\n" +
                "2 6\n" +
                "3 4\n" +
                "3 5\n" +
                "3 6\n" +
                "100000 0\n" +
                "1000 5\n" +
                "55 56\n" +
                "56 57\n" +
                "57 58\n" +
                "58 59\n" +
                "59 55\n" +
                "11 11\n" +
                "1 2\n" +
                "2 3\n" +
                "3 4\n" +
                "4 5\n" +
                "5 6\n" +
                "6 7\n" +
                "7 8\n" +
                "8 9\n" +
                "9 10\n" +
                "10 11\n" +
                "11 1\n" +
                "10 12\n" +
                "1 2\n" +
                "2 3\n" +
                "3 4\n" +
                "4 1\n" +
                "1 5\n" +
                "5 6\n" +
                "6 7\n" +
                "7 1\n" +
                "1 8\n" +
                "8 9\n" +
                "9 10\n" +
                "10 1"
        D().run()
    }

    @Test
    fun test22() {
        val str=StringBuffer("")
        (1..300000).forEach {
            str.append("1 0\n")
        }
        println("starting...")
        D.inputStr="300000\n"+str
        D().run()
    }
}