package com.happypeople.codeforces.c1093

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import java.lang.Math.random

class ETest {

    @Before
    fun setUp() {
        E.printLog=true
    }

    @Test
    fun run() {
        E.inputStr="6 7\n" +
                "5 1 4 2 3 6\n" +
                "2 5 3 1 4 6\n" +
                "1 1 2 4 5\n" +
                "2 2 4\n" +
                "1 1 2 4 5\n" +
                "1 2 3 3 5\n" +
                "1 1 6 1 2\n" +
                "2 4 1\n" +
                "1 4 4 1 3"
        E().run()
    }

    @Test
    fun big() {
        val queries=10000
        val sb=StringBuffer("200000 $queries\n")
        for(i in 1..200000) {
            val r=random()*200000+1
            sb.append(""+(r.toInt())+" ")
        }
        sb.append("\n")
        for(i in 1..200000) {
            val r=random()*200000+1
            sb.append(""+(r.toInt())+" ")
        }
        sb.append("\n")
        for(i in 1..queries) {
            sb.append("1 ")
            val r1=(random()*200000).toInt()+1
            val r2=(random()*200000).toInt()+1
            val r3=(random()*200000).toInt()+1
            val r4=(random()*200000).toInt()+1
            if(r1<r2)
                sb.append("$r1 $r2 ")
            else
                sb.append("$r2 $r1 ")
            if(r3<r4)
                sb.append("$r3 $r4 ")
            else
                sb.append("$r4 $r3 ")
            sb.append("\n")
        }
        E.inputStr=sb.toString()
        E().run()

    }
}