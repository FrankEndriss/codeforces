package com.happypeople.codeforces.c1104

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CTest {

    @Before
    fun setUp() {
        C.printLog=true
    }

    @Test
    fun run() {
        C.inputStr="010"
        C().run()
    }
}