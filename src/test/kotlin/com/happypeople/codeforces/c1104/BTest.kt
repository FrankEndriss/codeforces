package com.happypeople.codeforces.c1104

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="abacaba"
        B().run()
    }

    @Test
    fun run2() {
        B.inputStr="abba"
        B().run()
    }

    @Test
    fun run3() {
        B.inputStr="iiq"
        B().run()
    }
}