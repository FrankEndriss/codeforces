package com.happypeople.codeforces.c1105

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class BTest {

    @Before
    fun setUp() {
        B.printLog =true
    }

    @Test
    fun run() {
        B.inputStr ="8 2\n" +
                "aaacaabb"
        B().run()
    }

    @Test
    fun run2() {
        B.inputStr ="2 1\n" +
                "ab\n"
        B().run()
    }

    @Test
    fun run3() {
        B.inputStr ="4 2\n" +
                "abab\n"
        B().run()
    }
}