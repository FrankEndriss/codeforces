package com.happypeople.codeforces.c1105

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ATest {

    @Before
    fun setUp() {
        A.printLog =true
    }

    @Test
    fun run() {
        A.inputStr ="3\n" +
                "10 1 4"
        A().run()
    }

    @Test
    fun run2() {
        A.inputStr ="5\n" +
                "1 1 2 2 3"
        A().run()
    }
}