package com.happypeople.codeforces.c1105

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CTest {

    @Before
    fun setUp() {
        C.printLog =true
    }

    @Test
    fun run() {
        C.inputStr ="2 1 3\n"
        C().run()
    }

    @Test
    fun run2() {
        C.inputStr ="3 2 2\n"
        C().run()
    }

    @Test
    fun run3() {
        C.inputStr ="9 9 99\n"
        C().run()
    }
}