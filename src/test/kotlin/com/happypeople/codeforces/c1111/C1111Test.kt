package com.happypeople.codeforces.c1111

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1111Test {

    @Before
    fun setUp() {
        C1111.printLog=true
    }

    @Test
    fun run1() {
        C1111.inputStr="2 2 1 2\n" +
                "1 3"
        C1111().run()
    }

    @Test
    fun run2() {
        C1111.inputStr="3 2 1 2\n" +
                "1 7"
        C1111().run()
    }
}