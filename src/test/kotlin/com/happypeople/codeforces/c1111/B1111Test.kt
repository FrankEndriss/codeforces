package com.happypeople.codeforces.c1111

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1111Test {

    @Before
    fun setUp() {
        B1111.printLog=true
    }

    @Test
    fun run1() {
        B1111.inputStr="2 4 6\n" +
                "4 7"
        B1111().run()
    }
    @Test
    fun run2() {
        B1111.inputStr="4 2 6\n" +
                "1 3 2 3"
        B1111().run()
    }
}