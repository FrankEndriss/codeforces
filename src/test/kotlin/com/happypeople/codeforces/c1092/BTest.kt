package com.happypeople.codeforces.c1092

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="6\n" +
                "5 10 2 3 14 5"
        B().run()
    }
}