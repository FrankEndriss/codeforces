package com.happypeople.codeforces.c1092

import org.junit.Assert.*
import org.junit.Before

import org.junit.Test

class CTest {

    @Before
    fun setUp() {
        C.printLog=true
    }

    @Test
    fun run() {
        C.inputStr="5\n" +
                "ba\n" +
                "a\n" +
                "abab\n" +
                "a\n" +
                "aba\n" +
                "baba\n" +
                "ab\n" +
                "aba"
        C().run()
    }

    @Test
    fun run2() {
        C.inputStr="3\n" +
                "a\n" +
                "aa\n" +
                "aa\n" +
                "a"
        C().run()
    }

    @Test
    fun run3() {
        C.inputStr="2\n" +
                "a\n" +
                "c"
        C().run()
    }
}