package com.happypeople.codeforces.c1092

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ATest {

    @Before
    fun setUp() {
        A.printLog=true
    }

    @Test
    fun run() {
        A.inputStr="3\n" +
                "7 3\n" +
                "4 4\n" +
                "6 2"
        A().run()
    }
}