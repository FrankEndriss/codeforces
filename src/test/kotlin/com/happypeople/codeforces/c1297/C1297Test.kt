package com.happypeople.codeforces.c1297

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1297Test {

    @Before
    fun setUp() {
    }

    @Test
    fun run() {
        C1297.inputStr="5\n" +
                "5\n" +
                "1 -1 1 -1 1\n" +
                "2\n" +
                "11 1\n" +
                "3\n" +
                "5 -3 4\n" +
                "3\n" +
                "5 3 -4\n" +
                "5\n" +
                "-1 0 3 -3 0\n"
        C1297().run()
    }
}