package com.happypeople.codeforces.c1297

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1297Test {

    @Before
    fun setUp() {
    }

    @Test
    fun run() {
        A1297.inputStr="9\n" +
                "999\n" +
                "123\n" +
                "0\n" +
                "1782\n" +
                "31415926\n" +
                "1500\n" +
                "999999\n" +
                "35499710\n" +
                "2000000000"
        A1297().run()
    }
}