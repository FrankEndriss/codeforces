package com.happypeople.codeforces.c1297

import org.junit.Assert.*

import org.junit.Test

class B1297Test {

    @Test
    fun run() {
        B1297.inputStr="5\n" +
                "1\n" +
                "1 1\n" +
                "3\n" +
                "2 1000000000\n" +
                "2 500000000\n" +
                "500000002 1000000000\n" +
                "3\n" +
                "1 2\n" +
                "3 4\n" +
                "1 4\n" +
                "2\n" +
                "4 11\n" +
                "4 9\n" +
                "3\n" +
                "1 5\n" +
                "10 10\n" +
                "1 5\n"
        B1297().run()
    }
}