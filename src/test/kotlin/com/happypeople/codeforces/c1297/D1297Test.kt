package com.happypeople.codeforces.c1297

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1297Test {

    @Before
    fun setUp() {
    }

    @Test
    fun run() {
        D1297.inputStr="5\n" +
                "4 1\n" +
                "3 1 4 2\n" +
                "2 3\n" +
                "10 2\n" +
                "4 1000000000\n" +
                "987654321 1000000000 999999999 500000000\n" +
                "8 9\n" +
                "5 6 1 8 3 4 2 7\n" +
                "6 1\n" +
                "6 3 1 8 5 9\n"
        D1297().run()
    }
}