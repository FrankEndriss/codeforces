package com.happypeople.codeforces.c1121

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1121Test {

    @Before
    fun setUp() {
        B1121.printLog=true
    }

    @Test
    fun run() { // 3
        B1121.inputStr="8\n" +
                "1 8 3 11 4 9 2 7"
        B1121().run()
    }

    @Test
    fun run2() { // 2
        B1121.inputStr="7\n" +
                "3 1 7 11 9 2 12"
        B1121().run()
    }
}