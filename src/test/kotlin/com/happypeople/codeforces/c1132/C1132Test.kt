package com.happypeople.codeforces.c1132

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1132Test {

    @Before
    fun setUp() {
        C1132.printLog=true
    }

    @Test
    fun run() {
        C1132.inputStr="7 5\n" +
                "1 4\n" +
                "4 5\n" +
                "5 6\n" +
                "6 7\n" +
                "3 5"
        C1132().run()
    }

    @Test
    fun run2() {
        C1132.inputStr="4 3\n" +
                "1 1\n" +
                "2 2\n" +
                "3 4"
        C1132().run()
    }

    @Test
    fun run3() {
        C1132.inputStr="4 4\n" +
                "1 1\n" +
                "2 2\n" +
                "2 3\n" +
                "3 4\n"
        C1132().run()
    }
}