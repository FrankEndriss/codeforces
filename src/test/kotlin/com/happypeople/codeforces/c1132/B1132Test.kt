package com.happypeople.codeforces.c1132

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1132Test {

    @Before
    fun setUp() {
        B1132.printLog=true
    }

    @Test
    fun run() {
        B1132.inputStr="7\n" +
                "7 1 3 1 4 10 8\n" +
                "2\n" +
                "3 4"
        B1132().run()
    }
}