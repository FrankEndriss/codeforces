package com.happypeople.codeforces.c1132

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1132Test {

    @Before
    fun setUp() {
        A1132.printLog=true
    }

    @Test
    fun run() {
        A1132.inputStr="3\n" +
                "1\n" +
                "4\n" +
                "3"
        A1132().run()
    }

    @Test
    fun run2() {
        A1132.inputStr="0\n" +
                "0\n" +
                "0\n" +
                "0"
        A1132().run()
    }

    @Test
    fun run3() {
        A1132.inputStr="1\n" +
                "2\n" +
                "3\n" +
                "4"
        A1132().run()
    }
}