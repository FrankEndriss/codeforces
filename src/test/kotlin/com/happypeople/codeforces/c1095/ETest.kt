package com.happypeople.codeforces.c1095

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class ETest {

    @Before
    fun setUp() {
        E.printLog=true
    }

    @Test
    fun run() {
        E.inputStr="6\n" +
                "(((())"
        E().run()
    }

    @Test
    fun run2() {
        E.inputStr="6\n" +
                "(()())"
        E().run()
    }

    @Test
    fun run3() {
        E.inputStr="8\n" +
                "())))((("
        E().run()
    }

    @Test
    fun run4() {
        E.inputStr="10\n" +
                "(())((((((())())))()"
        E().run()
    }
}