package com.happypeople.codeforces.c1095

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run1() {
        B.inputStr="2\n1 100000"
        B().run()
    }

    @Test
    fun run2() {
        B.inputStr="4\n1 3 3 7"
        B().run()
    }
}