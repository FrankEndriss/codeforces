package com.happypeople.codeforces.c1095

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class CTest {

    @Before
    fun setUp() {
        C.printLog=true
    }

    @Test
    fun run() {
        C.inputStr="9 4\n"
        C().run()
    }

    @Test
    fun run2() {
        C.inputStr="8 1\n"
        C().run()
    }

    @Test
    fun run3() {
        C.inputStr="5 1\n"
        C().run()
    }

    @Test
    fun run4() {
        C.inputStr="3 7\n"
        C().run()
    }

    @Test
    fun run5() {
        C.inputStr="10 10\n"
        C().run()
    }
}