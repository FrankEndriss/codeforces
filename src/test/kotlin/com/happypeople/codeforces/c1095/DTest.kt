package com.happypeople.codeforces.c1095

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class DTest {

    @Before
    fun setUp() {
        D.printLog=true
    }

    @Test
    fun run() {
        D.inputStr="5\n" +
                "3 5\n" +
                "1 4\n" +
                "4 2\n" +
                "1 5\n" +
                "2 3"
        D().run()
    }

    @Test
    fun run2() {
        D.inputStr="3\n" +
                "2 3\n" +
                "3 1\n" +
                "1 2"
        D().run()
    }
}