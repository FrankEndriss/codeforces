package com.happypeople.codeforces.c1095

import org.junit.Before
import org.junit.Test
import java.lang.Math.random

class FTest {

    @Before
    fun setUp() {
        F.printLog = true
    }

    @Test
    fun run() {
        F.inputStr = "3 2\n" +
                "1 3 3\n" +
                "2 3 5\n" +
                "2 1 1"
        F().run()
    }

    @Test
    fun run2() {
        F.inputStr = "4 0\n" +
                "1 3 3 7"
        F().run()
    }

    @Test
    fun run3() {
        F.inputStr = "5 4\n" +
                "1 2 3 4 5\n" +
                "1 2 8\n" +
                "1 3 10\n" +
                "1 4 7\n" +
                "1 5 15"
        F().run()
    }

    @Test
    fun run5() {
        F.inputStr = "10 1\n" +
                "2 2 9 7 2 3 7 7 1 1\n" +
                "5 8 4"
        F().run()
    }

    @Test
    fun run6() {
        F.inputStr = "10 10\n" +
                "3 4 5 10 10 9 5 4 7 8\n" +
                "1 5 5\n" +
                "10 3 18\n" +
                "1 7 20\n" +
                "7 8 20\n" +
                "10 4 8\n" +
                "10 2 17\n" +
                "9 4 2\n" +
                "7 5 16\n" +
                "1 8 17\n" +
                "7 10 18"
        F().run()
    }

    @Test
    fun run7() {
        F.printLog = false
        val str = (1..200000).map { it }.joinToString(" ")
        F.inputStr = "200000 0\n" + str + "\n"
        F().run()
    }

    @Test
    fun run8() {
        F.printLog = false
        val str = (1..200000).map {
            val r = random() * 200000
            "" + (r.toInt())
        }.joinToString(" ")

        val offers = (1..100000).map {
            val x = (random() * 200000).toInt()
            val y = (random() * 200000).toInt()
            val w = (random() * 200000).toInt()
            "$x $y $w"
        }.joinToString("\n")
        F.inputStr = "200000 100000\n" + str + "\n" + offers
        F().run()
    }
}