package com.happypeople.codeforces.c1080

import org.junit.Test

import org.junit.Assert.*

class BTest {

    @Test
    fun run() {
        B.inputStr="5\n"+
        "1 3\n"+
        "2 5\n"+
            "5 5\n"+
            "4 4\n"+
        "2 3\n"
        B().run()
    }
}