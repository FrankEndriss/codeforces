package com.happypeople.codeforces.c1298

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1298Test {

    @Before
    fun setUp() {
        A1298.printLog=true
    }

    @Test
    fun run() {
        A1298.inputStr="3 6 5 4\n"
        A1298().run()
    }
}