package com.happypeople.codeforces.c1298

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1298Test {

    @Before
    fun setUp() {
        B1298.printLog=true
    }

    @Test
    fun run() {
        B1298.inputStr="6\n" +
                "1 5 5 1 6 1\n"
        B1298().run()
    }
}