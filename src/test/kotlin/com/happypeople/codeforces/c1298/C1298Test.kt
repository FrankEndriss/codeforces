package com.happypeople.codeforces.c1298

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1298Test {

    @Before
    fun setUp() {
        C1298.printLog=true
    }

    @Test
    fun run() {
        C1298.inputStr="6\n" +
                "xxxiii\n"
        C1298().run()
    }
}