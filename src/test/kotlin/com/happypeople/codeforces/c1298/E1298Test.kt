package com.happypeople.codeforces.c1298

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class E1298Test {

    @Before
    fun setUp() {
        E1298.printLog=true
    }

    @Test
    fun run() {
        E1298.inputStr="4 2\n" +
                "10 4 10 15\n" +
                "1 2\n" +
                "4 3\n"
        E1298().run()
    }

    @Test
    fun run2() {
        E1298.inputStr="10 4\n" +
                "5 4 1 5 4 3 7 1 2 5\n" +
                "4 6\n" +
                "2 1\n" +
                "10 8\n" +
                "3 5\n"
        E1298().run()
    }
}