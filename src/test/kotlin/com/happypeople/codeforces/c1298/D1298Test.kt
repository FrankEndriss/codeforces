package com.happypeople.codeforces.c1298

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1298Test {

    @Before
    fun setUp() {
        D1298.printLog=true
    }

    @Test
    fun run() {
        D1298.inputStr="3 5\n" +
                "2 1 -3\n"
        D1298().run()
    }

    @Test
    fun run2() {
        D1298.inputStr="2 4\n" +
                "-1 1\n"
        D1298().run()
    }

    @Test
    fun run3() {
        D1298.inputStr="4 10\n" +
                "2 4 1 2\n"
        D1298().run()
    }
}