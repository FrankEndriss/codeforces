package com.happypeople.codeforces.c1298

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class F1_1298Test {

    @Before
    fun setUp() {
        F1_1298.printLog=true
    }

    @Test
    fun run() {
        F1_1298.inputStr="5 4\n" +
                "2 4 5 3 1\n"
        F1_1298().run()
    }

    @Test
    fun run2() {
        F1_1298.inputStr="5 5\n" +
                "1 2 3 4 5\n"
        F1_1298().run()
    }

    @Test
    fun run3() {
        F1_1298.inputStr="15 8\n" +
                "1 15 2 14 3 13 4 8 12 5 11 6 10 7 9\n"
        F1_1298().run()
    }
}