package com.happypeople.codeforces.c1101

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="|[a:b:|]\n"
        B().run()
    }

    @Test
    fun run2() {
        B.inputStr="|]:[|:]\n"
        B().run()
    }

    @Test
    fun run3() {
        B.inputStr="x[[x:]|:xx|||xx:x]x\n"
        B().run()
    }
}