package com.happypeople.codeforces.c1101

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ATest {

    @Before
    fun setUp() {
        A.printLog=true
    }

    @Test
    fun run() {
        A.inputStr="5\n" +
                "2 4 2\n" +
                "5 10 4\n" +
                "3 10 1\n" +
                "1 2 3\n" +
                "4 6 5"
        A().run()
    }
}