package com.happypeople.codeforces.c1108

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class DTest {

    @Before
    fun setUp() {
        D.printLog=true
    }

    @Test
    fun run() {
        D.inputStr="9\n" +
                "RBGRRBRGG"
        D().run()
    }

    @Test
    fun run2() {
        D.inputStr="8\n" +
                "BBBGBRRR"
        D().run()
    }

    @Test
    fun run3() {
        D.inputStr="13\n" +
                "BBRRRRGGGGGRR"
        D().run()
    }
}