package com.happypeople.codeforces.c1108

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ATest {

    @Before
    fun setUp() {
        A.printLog=true
    }

    @Test
    fun run() {
        A.inputStr="5\n" +
                "1 2 1 2\n" +
                "2 6 3 4\n" +
                "2 4 1 3\n" +
                "1 2 1 3\n" +
                "1 4 5 8"
        A().run();
    }
}