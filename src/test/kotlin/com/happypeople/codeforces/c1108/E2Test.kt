package com.happypeople.codeforces.c1108

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import java.lang.Math.random

class E2Test {

    @Before
    fun setUp() {
        E2.printLog=true
    }

    @Test
    fun run() {
        val str=StringBuffer("")
        for(i in 1..10000) {
            str.append(" "+(random()*2000-1000).toInt())
        }
        str.append("\n")
        E2.inputStr="10000 0\n"+str

        val ts=System.currentTimeMillis()
        E2().run()
        val needed=System.currentTimeMillis()-ts
        println("needed: $needed")
    }
}