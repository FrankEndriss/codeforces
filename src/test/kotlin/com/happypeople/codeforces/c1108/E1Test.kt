package com.happypeople.codeforces.c1108

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class E1Test {

    @Before
    fun setUp() {
        E1.printLog=true
    }

    @Test
    fun run() {
        E1.inputStr="5 4\n" +
                "2 -2 3 1 2\n" +
                "1 3\n" +
                "4 5\n" +
                "2 5\n" +
                "1 3"
        E1().run()
    }

    @Test
    fun run2() {
        E1.inputStr="5 4\n" +
                "2 -2 3 1 4\n" +
                "3 5\n" +
                "3 4\n" +
                "2 4\n" +
                "2 5"
        E1().run()
    }

    @Test
    fun run3() {
        E1.inputStr="1 0\n" +
                "1000000"
        E1().run()
    }

    @Test
    fun run13() {
        E1.inputStr="2 1\n" +
                "5 8\n" +
                "1 1"
        E1().run()
    }
}