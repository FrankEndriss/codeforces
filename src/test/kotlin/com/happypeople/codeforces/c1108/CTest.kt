package com.happypeople.codeforces.c1108

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CTest {

    @Before
    fun setUp() {
        C.printLog=true
    }

    @Test
    fun run() {
        C.inputStr="7\n" +
                "RGBGRBB"
        C().run()
    }

    @Test
    fun run2() {
        C.inputStr = "3\n" +
                "BRB"
        C().run()
    }
}