package com.happypeople.codeforces.c1108

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="10\n" +
                "10 2 8 1 2 4 1 20 4 5"
        B().run()
    }
}