package com.happypeople.codeforces.c1113

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1113Test {

    @Before
    fun setUp() {
        B1113.printLog=true
    }

    @Test
    fun run() {
        B1113.inputStr="5\n" +
                "1 2 3 4 5"
        B1113().run()
    }

    @Test
    fun run2() {
        B1113.inputStr="4\n" +
                "4 2 4 4"
        B1113().run()
    }

    @Test
    fun run3() {
        B1113.inputStr="5\n" +
                "2 4 2 3 7"
        B1113().run()
    }
}