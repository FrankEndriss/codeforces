package com.happypeople.codeforces.c1113

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1113Test {

    @Before
    fun setUp() {
        D1113.printLog=true
    }

    @Test
    fun run() {
        D1113.inputStr="nolon\n"
        D1113().run()
    }

    @Test
    fun run2() {
        D1113.inputStr="otto"
        D1113().run()
    }

    @Test
    fun run3() {
        D1113.inputStr="qqqq\n"
        D1113().run()
    }

    @Test
    fun run4() {
        D1113.inputStr="kinnikkinnik"
        D1113().run()
    }
}