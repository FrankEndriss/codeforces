package com.happypeople.codeforces.c1113

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1113Test {

    @Before
    fun setUp() {
        C1113.printLog=true
    }

    @Test
    fun run() {
        C1113.inputStr="5\n" + // 1
                "1 2 3 4 5"
        C1113().run()
    }

    @Test
    fun run2() {
        C1113.inputStr="6\n" +  // 3
                "3 2 2 3 7 6"
        C1113().run()
    }

    @Test
    fun run3() {
        C1113.inputStr="3\n" +
                "42 4 2"
        C1113().run()
    }
}