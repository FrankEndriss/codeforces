package com.happypeople.codeforces.c1113

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1113Test {

    @Before
    fun setUp() {
        A1113.printLog=true
    }

    @Test
    fun run() {
        A1113.inputStr="4 2"
        A1113().run()
    }
    @Test
    fun run2() {
        A1113.inputStr="7 6"
        A1113().run()
    }
    @Test
    fun run3() {
        A1113.inputStr="8 3"
        A1113().run()
    }
}

