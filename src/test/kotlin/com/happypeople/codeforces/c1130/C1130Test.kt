package com.happypeople.codeforces.c1130

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1130Test {

    @Before
    fun setUp() {
        C1130.printLog=true
    }

    @Test
    fun run() {
        C1130.inputStr="5\n" +
                "1 1\n" +
                "5 5\n" +
                "00001\n" +
                "11111\n" +
                "00111\n" +
                "00110\n" +
                "00110"
        C1130().run()
    }

    @Test
    fun run2() {
        C1130.inputStr="3\n" +
                "1 3\n" +
                "3 1\n" +
                "010\n" +
                "101\n" +
                "010"
        C1130().run()
    }
}