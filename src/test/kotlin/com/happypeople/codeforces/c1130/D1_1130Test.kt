package com.happypeople.codeforces.c1130

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1_1130Test {

    @Before
    fun setUp() {
        D1_1130.printLog=true
    }

    @Test
    fun run() {
        D1_1130.inputStr="5 7\n" +
                "2 4\n" +
                "5 1\n" +
                "2 3\n" +
                "3 4\n" +
                "4 1\n" +
                "5 3\n" +
                "3 5"
        D1_1130().run()
    }

    @Test
    fun run2() {
        D1_1130.inputStr="2 3\n" +
                "1 2\n" +
                "1 2\n" +
                "1 2"
        D1_1130().run()
    }
}