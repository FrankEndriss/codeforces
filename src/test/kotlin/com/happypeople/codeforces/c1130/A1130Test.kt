package com.happypeople.codeforces.c1130

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1130Test {

    @Before
    fun setUp() {
        A1130.printLog=true
    }

    @Test
    fun run() {
        A1130.inputStr="5\n" +
                "10 0 -7 2 6"
        A1130().run()
    }

    @Test
    fun run2() {
        A1130.inputStr="7\n" +
                "0 0 1 -1 0 0 2"
        A1130().run()
    }
}