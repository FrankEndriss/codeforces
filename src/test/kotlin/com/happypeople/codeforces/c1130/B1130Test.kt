package com.happypeople.codeforces.c1130

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1130Test {

    @Before
    fun setUp() {
        B1130.printLog=true
    }

    @Test
    fun run() {
        B1130.inputStr="3\n" +
                "1 1 2 2 3 3"
        B1130().run()
    }

    @Test
    fun run2() {
        B1130.inputStr="2\n" +
                "2 1 1 2"
        B1130().run()
    }

    @Test
    fun run3() {
        B1130.inputStr="4\n" +
                "4 1 3 2 2 3 1 4"
        B1130().run()
    }
}