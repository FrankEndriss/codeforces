package com.happypeople.codeforces.c1110

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1110Test {

    @Before
    fun setUp() {
        C1110.printLog=true
    }

    @Test
    fun run() {
        C1110.inputStr="4\n" +
                "2\n" +
                "3\n" +
                "5\n" +
                "7"
        C1110().run()
    }
}