package com.happypeople.codeforces.c1110

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1110Test {

    @Before
    fun setUp() {
        A1110.printLog=true
    }

    @Test
    fun run() {
        A1110.inputStr="10 9\n" +
                "1 2 3 4 5 6 7 8 9"
        A1110().run()
    }

    @Test
    fun run1() {
        A1110.inputStr="13 3\n" +
                "3 2 7"
        A1110().run()
    }

    @Test
    fun run2() {
        A1110.inputStr="99 5\n" +
                "32 92 85 74 4"
        A1110().run()
    }
}