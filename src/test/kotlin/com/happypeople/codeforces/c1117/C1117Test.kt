package com.happypeople.codeforces.c1117

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1117Test {

    @Before
    fun setUp() {
        C1117.printLog=true
    }

    @Test
    fun run() {
        C1117.inputStr="0 0\n" +
                "4 6\n" +
                "3\n" +
                "UUU"
        C1117().run()
    }

    @Test
    fun run2() {
        C1117.inputStr="0 3\n" +
                "0 0\n" +
                "3\n" +
                "UDD"
        C1117().run()
    }

    @Test
    fun run3() {
        C1117.inputStr="0 0\n" +
                "0 1\n" +
                "1\n" +
                "L"
        C1117().run()
    }
}