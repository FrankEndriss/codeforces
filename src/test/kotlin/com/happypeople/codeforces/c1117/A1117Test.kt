package com.happypeople.codeforces.c1117

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1117Test {

    @Before
    fun setUp() {
        A1117.printLog=true
    }

    @Test
    fun run() {
        A1117.inputStr="5\n" +
                "6 1 6 6 0"
        A1117().run()
    }

    @Test
    fun run2() {
        A1117.inputStr="8\n" +
                "6 1 6 6 7 7 7 0"
        A1117().run()
    }
}