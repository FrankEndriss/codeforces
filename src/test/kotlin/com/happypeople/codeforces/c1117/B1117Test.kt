package com.happypeople.codeforces.c1117

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1117Test {

    @Before
    fun setUp() {
        B1117.printLog=true
    }

    @Test
    fun run() {
        B1117.inputStr="6 9 2\n" +
                "1 3 3 7 4 2"
        B1117().run()
    }

    @Test
    fun run2() {
        B1117.inputStr="3 1000000000 1\n" +
                "1000000000 987654321 1000000000"
        B1117().run()
    }

    @Test
    fun run3() {
        B1117.inputStr="6 12 2\n" +
                "1 3 3 7 4 2"
        B1117().run()
    }

}