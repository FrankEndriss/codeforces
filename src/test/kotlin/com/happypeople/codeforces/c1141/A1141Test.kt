package com.happypeople.codeforces.c1141

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1141Test {

    @Before
    fun setUp() {
        A1141.printLog=true
    }

    @Test
    fun run() {
        A1141.inputStr="120 51840\n"
        A1141().run()
    }

    @Test
    fun run2() {
        A1141.inputStr="42 42\n"
        A1141().run()
    }

    @Test
    fun run3() {
        A1141.inputStr="48 72\n"
        A1141().run()
    }

    @Test
    fun run4() {
        val x=(1 shl 29) -1

        A1141.inputStr="1 $x\n"
        A1141().run()
    }
}