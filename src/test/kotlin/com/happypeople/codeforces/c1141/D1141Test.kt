package com.happypeople.codeforces.c1141

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1141Test {

    @Before
    fun setUp() {
        D1141.printLog=true
    }

    @Test
    fun run() {
        D1141.inputStr="10\n" +
                "codeforces\n" +
                "dodivthree"
        D1141().run()
    }

    @Test
    fun run2() {
        D1141.inputStr="7\n" +
                "abaca?b\n" +
                "zabbbcc"
        D1141().run()
    }

    @Test
    fun run3() {
        D1141.inputStr="9\n" +
                "bambarbia\n" +
                "hellocode"
        D1141().run()
    }

    @Test
    fun run4() {
        D1141.inputStr="10\n" +
                "code??????\n" +
                "??????test"
        D1141().run()
    }
}