package com.happypeople.codeforces.c1141

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1141Test {

    @Before
    fun setUp() {
        C1141.printLog=true
    }

    @Test
    fun run() {
        C1141.inputStr="3\n" +
                "-2 1"
        C1141().run()
    }

    @Test
    fun run2() {
        C1141.inputStr="5\n" +
                "1 1 1 1"
        C1141().run()
    }

    @Test
    fun run3() {
        C1141.inputStr="4\n" +
                "-1 2 2"
        C1141().run()
    }
}