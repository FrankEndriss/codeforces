package com.happypeople.codeforces.c1141

import com.happypeople.codeforces.c1114.B1114
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1141Test {

    @Before
    fun setUp() {
        B1141.printLog=true
    }

    @Test
    fun run() {
        B1141.inputStr="5\n" +
                "1 0 1 0 1"
        B1141().run()
    }

    @Test
    fun run2() {
        B1141.inputStr="6\n" +
                "0 1 0 1 1 0"
        B1141().run()
    }

    @Test
    fun run3() {
        B1141.inputStr="7\n" +
                "1 0 1 1 1 0 1"
        B1141().run()
    }

    @Test
    fun run4() {
        B1141.inputStr="3\n" +
                "0 0 0"
        B1141().run()
    }

    @Test
    fun run5() {
        B1141.inputStr="3\n" +
                "1 1 1"
        B1141().run()
    }
}