package com.happypeople.codeforces.c1141

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class E1141Test {

    @Before
    fun setUp() {
        E1141.printLog=true
    }

    @Test
    fun run() {
        E1141.inputStr="1000 6\n" +
                "-100 -200 -300 125 77 -4"
        E1141().run()
    }

    @Test
    fun run2() {
        E1141.inputStr="1000000000000 5\n" +
                "-1 0 0 0 0"
        E1141().run()
    }

    @Test
    fun run3() {
        E1141.inputStr="10 4\n" +
                "-3 -6 5 4"
        E1141().run()
    }

    @Test
    fun run4() {
        E1141.inputStr="2 3\n" +
                "0 -1 1"
        E1141().run()
    }

    @Test
    fun run8() {
        E1141.inputStr="1 2\n" +
                "-1 1"
        E1141().run()
    }

    @Test
    fun run12() {
        E1141.inputStr="10 10\n" +
                "-4 -10 -4 -2 5 2 0 3 0 0"
        E1141().run()
    }
}