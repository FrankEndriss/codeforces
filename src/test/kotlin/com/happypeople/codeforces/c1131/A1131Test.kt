package com.happypeople.codeforces.c1131

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1131Test {

    @Before
    fun setUp() {
        A1131.printLog=true
    }

    @Test
    fun run1() {
        A1131.inputStr="2 1 2 1"
        A1131().run()
    }

    @Test
    fun run2() {
        A1131.inputStr="2 2 1 2"
        A1131().run()
    }

}