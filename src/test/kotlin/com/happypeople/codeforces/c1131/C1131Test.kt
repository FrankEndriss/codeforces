package com.happypeople.codeforces.c1131

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1131Test {

    @Before
    fun setUp() {
        C1131.printLog=true
    }

    @Test
    fun run() {
        C1131.inputStr="5\n" +
                "2 1 1 3 2"
        C1131().run()
    }

    @Test
    fun run2() {
        C1131.inputStr="3\n" +
                "30 10 20\n"
        C1131().run()
    }

    @Test
    fun run3() {
        C1131.inputStr="10\n" +
                "0 1 2 3 4 5 6 7 8 9\n"
        C1131().run()
    }
}