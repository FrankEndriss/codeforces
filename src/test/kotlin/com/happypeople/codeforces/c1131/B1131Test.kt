package com.happypeople.codeforces.c1131

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1131Test {

    @Before
    fun setUp() {
        B1131.printLog=true
    }

    @Test
    fun run() {
        B1131.inputStr="3\n" +
                "2 0\n" +
                "3 1\n" +
                "3 4"
        B1131().run()
    }

    @Test
    fun run2() {
        B1131.inputStr="1\n" +
                "5 4"
        B1131().run()
    }

    @Test
    fun run3() {
        B1131.inputStr="4\n" +
                "0 0\n" +
        "1 1\n" +
                "3 3\n" +
                "4 3\n"
        B1131().run()
    }
}