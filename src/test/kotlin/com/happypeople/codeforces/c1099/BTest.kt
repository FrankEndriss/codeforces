package com.happypeople.codeforces.c1099

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class BTest {

    @Before
    fun setUp() {
        B.printLog=true
    }

    @Test
    fun run() {
        B.inputStr="15"
        B().run()
    }

    @Test
    fun run1() {
        B.inputStr="1"
        B().run()
    }

    @Test
    fun run2() {
        B.inputStr="2"
        B().run()
    }
}