package com.happypeople.codeforces.c1099

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class CTest {

    @Before
    fun setUp() {
        C.printLog=true
    }

    @Test
    fun run() {
        C.inputStr="hw?ap*yn?eww*ye*ar\n" +
                "12\n"
        C().run()
    }

    @Test
    fun run4() {
        C.inputStr="hw?ap*yn?eww?ye?ar\n" +
                "12\n"
        C().run()
    }

    @Test
    fun run2() {
        C.inputStr="ababb\n" +
                "5\n"
        C().run()
    }

    @Test
    fun run3() {
        C.inputStr="ab?a\n" +
                "1\n"
        C().run()
    }

    @Test
    fun run5() {
        C.inputStr="\n" +
                "1\n"
        C().run()

    }

    @Test
    fun run6() {
        C.inputStr="abc?\n"+
                "4\n"
        C().run()
    }
}