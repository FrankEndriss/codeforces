package com.happypeople.codeforces.c1099

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class ATest {

    @Before
    fun setUp() {
        A.printLog=true
    }

    @Test
    fun run() {
        A.inputStr="4 3\n" +
                "1 1\n" +
                "1 2"
        A().run()
    }
    @Test
    fun run2() {
        A.inputStr="4 3\n" +
                "9 2\n" +
                "0 1"
        A().run()
    }
}