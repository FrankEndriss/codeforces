package com.happypeople.codeforces.c1114

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1114Test {

    @Before
    fun setUp() {
        D1114.printLog=true
    }

    @Test
    fun run() {
        D1114.inputStr="4\n" +
                "5 2 2 1"
        D1114().run()
    }

    @Test
    fun run2() {
        D1114.inputStr="8\n" +
                "4 5 2 2 1 3 5 5"
        D1114().run()
    }

    @Test
    fun run3() {
        D1114.inputStr="1\n" +
                "4"
        D1114().run()
    }
}