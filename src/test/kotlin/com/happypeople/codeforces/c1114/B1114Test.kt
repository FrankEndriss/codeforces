package com.happypeople.codeforces.c1114

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1114Test {

    @Before
    fun setUp() {
        B1114.printLog=false
    }

    @Test
    fun run() {
        B1114.inputStr="9 2 3\n" +
                "5 2 5 2 4 1 1 3 2"
        B1114().run()
    }

    @Test
    fun run2() {
        B1114.inputStr="6 1 4\n" +
                "4 1 3 2 2 3"
        B1114().run()
    }

    @Test
    fun run3() {
        B1114.inputStr="2 1 2\n" +
                "-1000000000 1000000000"
        B1114().run()
    }
}