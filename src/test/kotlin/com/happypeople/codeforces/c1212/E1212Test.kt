package com.happypeople.codeforces.c1212

import E1212
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class E1212Test {

    @Before
    fun setUp() {
        E1212.printLog=true
    }

    @Test
    fun run() {
        E1212.inputStr="3\n" +
                "10 50\n" +
                "2 100\n" +
                "5 30\n" +
                "3\n" +
                "4 6 9\n"
        E1212().run()
    }
}