package com.happypeople.codeforces.c1212

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1212Test {

    @Before
    fun setUp() {
        C1212.printLog=true
    }

    @Test
    fun run() {
        C1212.inputStr="7 2\n"+
                "3 7 5 1 10 3 20"
        C1212().run()
    }
}