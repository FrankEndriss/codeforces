package com.happypeople.codeforces.c1212

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1212Test {

    @Before
    fun setUp() {
        D1212.printLog=true
    }

    @Test
    fun run() {
        D1212.inputStr="2\n"+
        "1000000000000000000 3000000000000000000"
        D1212().run()
    }
}