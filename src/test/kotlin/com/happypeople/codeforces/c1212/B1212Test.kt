package com.happypeople.codeforces.c1212

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1212Test {

    @Before
    fun setUp() {
        B1212.printLog=true;
    }

    @Test
    fun run() {
        B1212.inputStr="7\n"+
            "ABACABA"
        B1212().run()
    }
}