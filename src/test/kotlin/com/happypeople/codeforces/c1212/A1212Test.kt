package com.happypeople.codeforces.c1212

import org.junit.Before
import org.junit.Test

class A1212Test {
    @Before
    fun setUp() {
        A1212.printLog=true
    }

    @Test
    fun run() {
        A1212.inputStr="512 4\n"
        A1212().run()
    }

}