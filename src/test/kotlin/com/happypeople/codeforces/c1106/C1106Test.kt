package com.happypeople.codeforces.c1106

import org.junit.Before
import org.junit.Test

class C1106Test {

    @Before
    fun setUp() {
        C1106.printLog=true
    }

    @Test
    fun run() {
        C1106.inputStr="4\n" +
                "8 5 2 3"
        C1106().run()
    }
}