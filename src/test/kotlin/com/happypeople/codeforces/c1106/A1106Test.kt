package com.happypeople.codeforces.c1106

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1106Test {

    @Before
    fun setUp() {
        A1106.printLog=true
    }

    @Test
    fun run2() {
        A1106.inputStr="6\n" +
                "......\n" +
                "X.X.X.\n" +
                ".X.X.X\n" +
                "X.X.X.\n" +
                ".X.X.X\n" +
                "......"
        A1106().run()
    }
    @Test
    fun run() {
        A1106.inputStr="5\n" +
                ".....\n" +
                ".XXX.\n" +
                ".XXX.\n" +
                ".XXX.\n" +
                "....."
        A1106().run()
    }
}