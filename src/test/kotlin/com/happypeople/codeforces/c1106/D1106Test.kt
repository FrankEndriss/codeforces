package com.happypeople.codeforces.c1106

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1106Test {

    @Before
    fun setUp() {
        D1106.printLog=true
    }

    @Test
    fun run2() {
        D1106.inputStr="10 10\n" +
                "1 4\n" +
                "6 8\n" +
                "2 5\n" +
                "3 7\n" +
                "9 4\n" +
                "5 6\n" +
                "3 4\n" +
                "8 10\n" +
                "8 9\n" +
                "1 10"
        D1106().run()
    }
    @Test
    fun run() {
        D1106.inputStr="5 5\n" +
                "1 4\n" +
                "3 4\n" +
                "5 4\n" +
                "3 2\n" +
                "1 5"
        D1106().run()
    }
}