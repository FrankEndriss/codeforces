package com.happypeople.codeforces.c1106

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1106Test {

    @Before
    fun setUp() {
        B1106.printLog=true
    }

    @Test
    fun run() {
        B1106.inputStr="8 5\n" +
                "8 6 2 1 4 5 7 5\n" +
                "6 3 3 2 6 2 3 2\n" +
                "2 8\n" +
                "1 4\n" +
                "4 7\n" +
                "3 4\n" +
                "6 10"
        B1106().run()
    }
    @Test
    fun run3() {
        B1106.inputStr = "6 6\n" +
                "6 6 6 6 6 6\n" +
                "6 66 666 6666 66666 666666\n" +
                "1 6\n" +
                "2 13\n" +
                "3 6\n" +
                "4 11\n" +
                "5 6\n" +
                "6 6"
        B1106().run()
    }
}