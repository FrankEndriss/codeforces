package com.happypeople.codeforces.c1133

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class B1133Test {

    @Before
    fun setUp() {
        B1133.printLog=true
    }

    @Test
    fun run() {
        B1133.inputStr="7 2\n" +
                "1 2 2 3 2 4 10"
        B1133().run()
    }

    @Test
    fun run2() {
        B1133.inputStr="8 2\n" +
                "1 2 2 3 2 4 6 10"
        B1133().run()
    }

    @Test
    fun run3() {
        B1133.inputStr="7 3\n" +
                "1 2 2 3 2 4 5"
        B1133().run()
    }
}