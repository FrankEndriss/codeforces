package com.happypeople.codeforces.c1133

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class F2_1133Test {

    @Before
    fun setUp() {
        F2_1133.printLog=true
    }

    @Test
    fun run() {
        F2_1133.inputStr="4 5 1\n" +
                "1 2\n" +
                "1 3\n" +
                "1 4\n" +
                "2 3\n" +
                "3 4"
        F2_1133().run()
    }
}