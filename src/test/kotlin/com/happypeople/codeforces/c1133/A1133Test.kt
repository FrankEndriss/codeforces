package com.happypeople.codeforces.c1133

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class A1133Test {

    @Before
    fun setUp() {
        A1133.printLog=true
    }

    @Test
    fun run() {
        A1133.inputStr="10:00\n" +
                "11:00"
        A1133().run()
    }

    @Test
    fun run2() {
        A1133.inputStr="01:02\n" +
                "03:02"
        A1133().run()
    }
}