package com.happypeople.codeforces.c1133

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class C1133Test {

    @Before
    fun setUp() {
        C1133.printLog=true
    }

    @Test
    fun run() {
        C1133.inputStr="8\n"+
                "1 2 3  5 6 7  9 10"
        C1133().run()
    }

    @Test
    fun run2() {
        C1133.inputStr="6\n" +
                "1 10 17 12 15 2"
        C1133().run()
    }
}