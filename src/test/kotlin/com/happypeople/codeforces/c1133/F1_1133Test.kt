package com.happypeople.codeforces.c1133

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class F1_1133Test {

    @Before
    fun setUp() {
        F1_1133.printLog=true
    }

    @Test
    fun run() {
        F1_1133.inputStr="5 5\n" +
                "1 2\n" +
                "2 3\n" +
                "3 5\n" +
                "4 3\n" +
                "1 5"
        F1_1133().run()
    }

    @Test
    fun run2() {
        F1_1133.inputStr="4 6\n" +
                "1 2\n" +
                "1 3\n" +
                "1 4\n" +
                "2 3\n" +
                "2 4\n" +
                "3 4"
        F1_1133().run()
    }
}