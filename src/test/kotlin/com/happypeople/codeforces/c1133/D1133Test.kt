package com.happypeople.codeforces.c1133

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class D1133Test {

    @Before
    fun setUp() {
        D1133.printLog=true
    }

    @Test
    fun run() {
        D1133.inputStr="5\n" +
                "1 2 3 4 5\n" +
                "2 4 7 11 3"
        D1133().run()
    }

    @Test
    fun run2() {
        D1133.inputStr="3\n" +
                "13 37 39\n" +
                "1 2 3"
        D1133().run()
    }

    @Test
    fun run3() {
        D1133.inputStr="4\n" +
                "0 0 0 0\n" +
                "1 2 3 4"
        D1133().run()
    }

    @Test
    fun run4() {
        D1133.inputStr="3\n" +
                "1 2 -1\n" +
                "-6 -12 6"
        D1133().run()
    }
}