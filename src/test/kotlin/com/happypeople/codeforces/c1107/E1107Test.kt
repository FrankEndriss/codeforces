package com.happypeople.codeforces.c1107

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class E1107Test {

    @Before
    fun setUp() {
        E1107.printLog=true
    }

    @Test
    fun run() {
        E1107.inputStr="7\n" +
                "1101001\n" +
                "3 4 9 100 1 2 3"
        E1107().run()
    }

    @Test
    fun run2() {
        E1107.inputStr="5\n" +
                "10101\n" +
                "3 10 15 15 15"
        E1107().run()
    }

    @Test
    fun run4() {
        E1107.inputStr="30\n" +
                "010010010010010010010010010010\n" +
                "1 1 1000000000 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1"
        E1107().run()
    }

    @Test
    fun run5() {
        E1107.inputStr="93\n" +
                "010001011000011111111111001100111110111111101010010100011101101111101010101110101101010010100\n" +
                "44 77 155 1 219 284 32 146 56 31 213 253 333 206 146 305 178 257 125 3 512 455 433 108 541 112 219 1 609 682 585 764 86 317 216 959 37 212 861 599 1176 1147 316 930 876 259 613 266 448 1845 1280 1570 2389 2799 3100 784 3392 542 3140 3371 1871 4723 680 812 334 7639 8168 6537 1652 8174 1807 9420 9013 117 2250 23272 2881 75 23885 4608 13637 49693 38953 255 91290 61084 116748 191611 103509 268545 15046 620814 493 123\n"
        E1107().run()
    }

}