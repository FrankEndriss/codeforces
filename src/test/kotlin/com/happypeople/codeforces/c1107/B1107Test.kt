package com.happypeople.codeforces.c1107

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class B1107Test {

    @Before
    fun setUp() {
    }

    @Test
    fun run() {
        B1107.inputStr="3\n" +
                "1 5\n" +
                "5 2\n" +
                "3 1"
        B1107().run()
    }
}