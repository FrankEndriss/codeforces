package com.happypeople.codeforces.c1107

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

internal class A1107Test {

    @Before
    fun setUp() {
        A1107.printLog=true
    }

    @Test
    fun run() {
        A1107.inputStr="4\n" +
                "6\n" +
                "654321\n" +
                "4\n" +
                "1337\n" +
                "2\n" +
                "33\n" +
                "4\n" +
                "2122"
        A1107().run()
    }

    @Test
    fun run1() {
    }
}