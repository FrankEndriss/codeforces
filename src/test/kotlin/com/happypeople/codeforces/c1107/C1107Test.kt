package com.happypeople.codeforces.c1107

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class C1107Test {

    @Before
    fun setUp() {
        C1107.printLog=true
    }

    @Test
    fun run() {
        C1107.inputStr="7 3\n" +
                "1 5 16 18 7 2 10\n" +
                "baaaaca"
        C1107().run()
    }

    @Test
    fun run2() {
        C1107.inputStr="5 5\n" +
                "2 4 1 3 1000\n" +
                "aaaaa"
        C1107().run()
    }

    @Test
    fun run3() {
        C1107.inputStr = "8 1\n" +
                "10 15 2 1 4 8 15 16\n" +
                "qqwweerr\n"
        C1107().run()
    }
}